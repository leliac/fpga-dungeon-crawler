----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/09/2017 10:47:09 AM
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top is
    Port ( clk : in STD_LOGIC;
           btn : in STD_LOGIC_VECTOR (3 downto 0);
           sw : in STD_LOGIC_VECTOR (0 downto 0);
           --led : out STD_LOGIC_VECTOR (3 downto 0);
           vga_r : out STD_LOGIC_VECTOR (4 downto 0);
           vga_g : out STD_LOGIC_VECTOR (5 downto 0);
           vga_b : out STD_LOGIC_VECTOR (4 downto 0);
           vga_hs : out STD_LOGIC;
           vga_vs : out STD_LOGIC);
end top;

architecture Behavioral of top is

component tilemanager is
    Port ( clock : in STD_LOGIC;
           we : in STD_LOGIC;
           x_r : in NATURAL range 0 to 80;
           y_r : in NATURAL range 0 to 60;
           x_w : in NATURAL range 0 to 80;
           y_w : in NATURAL range 0 to 60;
           in_tile : in STD_LOGIC_VECTOR(7 downto 0);
           out_tile : out STD_LOGIC_VECTOR(7 downto 0));
end component;

component renderer_v2 is
    Port ( addr_X : out INTEGER range 0 to 81;
          addr_Y : out INTEGER range 0 to 63;
          pixel_bus : out STD_LOGIC_VECTOR(15 downto 0);
          boundary_X : in INTEGER range 0 to 81; --may embed boundary calculation
          boundary_Y : in INTEGER range 0 to 63;
          this_X : in INTEGER range 0 to 81; --player position
          this_Y : in INTEGER range 0 to 63;
          mode : in STD_LOGIC; --whether to print the finder
          canput : in STD_LOGIC; --to set the color of the finder
          screen_X : in INTEGER range 0 to 900; --calculated inside the vga module
          screen_Y : in INTEGER range 0 to 530;
          tile_id : in STD_LOGIC_VECTOR(7 downto 0); --size subject to change
          clk: in STD_LOGIC;
          output_enable : in STD_LOGIC;
          pixel_clk: in STD_LOGIC);
end component;

component vga_connector is
    Port ( pixel_bus : in STD_LOGIC_VECTOR (15 downto 0);
           clock : in STD_LOGIC;
           vga_r : out STD_LOGIC_VECTOR (4 downto 0);
           vga_g : out STD_LOGIC_VECTOR (5 downto 0);
           vga_b : out STD_LOGIC_VECTOR (4 downto 0);
           vga_hs : out STD_LOGIC;
           vga_vs : out STD_LOGIC;
           h_cnt : out INTEGER range 0 to 900; --can be used by other modules
           v_cnt : out INTEGER range 0 to 530;
           render_enable: out STD_LOGIC;
           anim_enable: out STD_LOGIC);
end component;

component boundaryWriter is
    Port ( fr_clk : in STD_LOGIC;
           ch_x : in INTEGER range 0 to 80;
           ch_y : in INTEGER range 0 to 60;
           boundary_X : out INTEGER range 0 to 80 := 0;
           boundary_Y : out INTEGER range 0 to 60 := 0);
end component;

component buttReceiver is
    Port ( buttons : in STD_LOGIC_VECTOR (7 downto 0);
           clock : in STD_LOGIC;
           moves : out STD_LOGIC_VECTOR (7 downto 0));
end component;

component clk_gen is
    Port( clk_in : in STD_LOGIC;
          clk_out : out STD_LOGIC);
end component;

component chasingAi is
    Port ( clock : in STD_LOGIC;
           enable : in STD_LOGIC;
           targetX : in INTEGER range 0 to 80;
           targetY : in INTEGER range 0 to 60;
           thisX : in INTEGER range 0 to 80;
           thisY : in INTEGER range 0 to 60;
           thisType : in STD_LOGIC_VECTOR(2 downto 0); --indicates the type of Ai to compute (should be defined)
           canShot : in STD_LOGIC; --may be taken from entity array
           isFree : in STD_LOGIC; --received from collisionChecker
           ready : in STD_LOGIC;
           dirToTest :out STD_LOGIC_VECTOR (3 downto 0) := (others=>'0');
           c_enable : out STD_LOGIC := '0';
           testX : out INTEGER range 0 to 80; --to pass to collisionChecker
           testY : out INTEGER range 0 to 60;
           command : out INTEGER range 0 to 7;
           damage : out INTEGER range 0 to 7;
           direction : out STD_LOGIC_VECTOR (3 downto 0) := (others=>'0'));
end component;

component collisionChecker is
    Port ( clock : in STD_LOGIC;
           enable : in STD_LOGIC;
           x_in : in INTEGER range 0 to 80;
           y_in : in INTEGER range 0 to 60;
           dir : in STD_LOGIC_VECTOR (3 downto 0);
           x_out : out INTEGER range 0 to 80; -- sent to
           y_out : out INTEGER range 0 to 60; -- tilemap
           tile : in STD_LOGIC_VECTOR (7 downto 0);
           check : out STD_LOGIC := '0'; --1 if free
           ready : out STD_LOGIC := '1'); --1 if the result can be used
end component;

component player_manager is
    Port ( clock : in STD_LOGIC;
           enable : in STD_LOGIC;
           buttons : in STD_LOGIC_VECTOR (7 downto 0);
           mode : in STD_LOGIC;
           isFree : in STD_LOGIC;
           ready : in STD_LOGIC;
           dirToTest : out STD_LOGIC_VECTOR (3 downto 0);
           direction : out STD_LOGIC_VECTOR (3 downto 0);
           canput : out STD_LOGIC; --used to color the target of the mob-spawner
           command : out INTEGER range 0 to 7);
end component;

component buttProc is
    Port ( buttons : in STD_LOGIC_VECTOR (7 downto 0); --from board
           clock : in STD_LOGIC; --may be used as enabler (not system clock)
           enable : in STD_LOGIC;
           mode : in STD_LOGIC;
           isFree : in STD_LOGIC;
           ready : in STD_LOGIC;
           dirToTest : out STD_LOGIC_VECTOR (3 downto 0) := (others=>'0');
           coll_ena : out STD_LOGIC := '0';
           direction : out STD_LOGIC_VECTOR (3 downto 0) := (others=>'0');
           canput : out STD_LOGIC := '0'; --to color the target of the mob-spawner
           damage : out INTEGER range 0 to 7;
           command : out INTEGER range 0 to 7);
end component;

signal pbus : STD_LOGIC_VECTOR (15 downto 0) := (others => '0');
signal h_clk,clk5,r_ena,w_ena,addr_ch,new_tile : STD_LOGIC := '0';
signal tile_in,tile_out,tmpButt : STD_LOGIC_VECTOR (7 downto 0) := (others=>'0');
signal x,y : INTEGER range 0 to 800 := 0; --screen coordinates
signal x_w,y_w,x_r,y_r,bnd_x,bnd_y : INTEGER range 0 to 80 := 0;
signal x_in,y_in,x_out,y_out,addrX,addrY : INTEGER range 0 to 80 := 0;
signal chX,chY,oldX,oldY : INTEGER range 0 to 80 := 20;
signal mobX,mobY,MoldX,MoldY,mtX,mtY : INTEGER range 0 to 80 := 5;
signal tmpdir,testDir,dir,tmpdirP: STD_LOGIC_VECTOR (3 downto 0);
signal btt : STD_LOGIC_VECTOR (3 downto 0);
signal butt_ena,bnd_ena,ai_ena,c_enaP,c_enaM,c_ena : STD_LOGIC := '0';
signal pl_ena, mob_ena : STD_LOGIC := '0';
signal check,ready,canput : STD_LOGIC := '0';

begin

  rend : renderer_v2 port map(boundary_X=>bnd_x, boundary_Y=>bnd_y, tile_id=>tile_out,
         this_x=>chX, this_Y=>chY, mode=>sw(0), canput=>canput,
         clk=>h_clk,screen_X=>x, screen_Y=>y, pixel_clk=>clk5, 
         output_enable=>r_ena, pixel_bus=>pbus, addr_X=>addrX, addr_Y=>addrY);

  vga_clock : clk_gen port map(clk_in=>clk, clk_out=>clk5);

  output : vga_connector port map(pixel_bus=>pbus, clock=>clk5, h_cnt=>x, v_cnt=>y,
          vga_r=>vga_r, vga_g=>vga_g, vga_b=>vga_b, vga_hs=>vga_hs, vga_vs=>vga_vs,
          render_enable=>r_ena);

  tm : tilemanager port map(clock=>h_clk, we=>w_ena, x_r=>x_r, y_r=>y_r, 
       x_w=>x_w, y_w=>y_w, in_tile=>tile_in, out_tile=>tile_out);
       
  bnd : boundaryWriter port map(fr_clk=>bnd_ena,ch_x=>chX, ch_y=>chY, 
                       boundary_X=>bnd_x, boundary_Y=>bnd_y);

  checkDir : chasingAi port map(clock=>h_clk, targetX=>chX, targetY=>chY, thisX=>mobX, 
            canshot=>'0', enable=>ai_ena, ready=>ready, thisType=> "000", thisY=>mobY,
            isFree=>check, dirToTest=>tmpdir, direction=>dir, c_enable=>c_enaM,
            testX=>mtX, testY=>mtY);

  collision : collisionChecker port map(clock=>h_clk, enable=>c_ena,
              dir=>testDir, x_in=>x_in, y_in=>y_in, x_out=>x_out, y_out=>y_out, 
              tile=> tile_out, check=>check, ready=>ready);

              
  bp : buttProc port map(buttons(7 downto 4)=>"0000", buttons(3 downto 0)=>btn, 
                     clock=>h_clk, enable=>butt_ena, mode=>sw(0), isFree=>check, ready=>ready,
                     dirToTest=>tmpdirP, coll_ena=>c_enaP, direction=>btt, canput=>canput);
  
  half_clk : process(clk)
  begin
    if rising_edge(clk) then
      h_clk <= not h_clk;
    end if;
  end process;
  
  testing : process(h_clk)
  variable cnt : NATURAL := 0;
  begin
    if falling_edge(h_clk) then
      if cnt>=3 and cnt<27 then
        w_ena <= '1';
        if cnt=18 then
          tile_in <= "00000110"; --door
        else
          tile_in <= "00000010"; --wall
        end if;
        x_w <= 10; y_w <= cnt;
      elsif x=20 and y=502 and sw(0)='1' then
        w_ena <= '1';
        if btt(1) = '1' then --left
          tile_in <= "00000101"; --wizard
        else
          tile_in <= "00000100";
        end if;
        x_w <= chX; y_w <= chY;
      elsif x=10 and y=502 and sw(0)='1' then
        w_ena <= '1';
        tile_in <= "00000000"; --floor
        x_w <= oldX; y_w <= oldY;
      
      elsif x=40 and y=502 then
        w_ena <= '1';
        if dir(1) = '1' then --left
          tile_in <= "00001000"; --mob
        else
          tile_in <= "00001001";
        end if;
        x_w <= mobX; y_w <= mobY;
      elsif x=30 and y=502 then
        w_ena <= '1';
        tile_in <= "00000000";
        x_w <= MoldX; y_w <= MoldY;
      else
        w_ena <= '0';
      end if;
    end if;
    if rising_edge(h_clk) then
      case y is
        when 10 =>
          if x=10 then
            butt_ena <= '1'; --check for pressing
          elsif x=15 then
            pl_ena <= '1'; --computes movement
          end if;
        when 18 => 
          if x=10 then
            ai_ena <= '1'; --computes the direction
          elsif x=100 then
            mob_ena <= '1'; --computes movement
          end if; 
        when 475 => bnd_ena <= '1'; --start computing boundary
        when others => --disable all
          butt_ena <= '0';
          bnd_ena <= '0';
          ai_ena <= '0';
          pl_ena <= '0';
          mob_ena <= '0';
      end case;
      if cnt<1000 then
        cnt := cnt + 1;
      end if;
    end if;
  end process;
  
  tm_r_mux : process(addrX,addrY,x_out,y_out)
  begin
    if butt_ena='1' or ai_ena='1' then --when needed to check for collisions
      x_r <= x_out; y_r <= y_out; --used by collision
    else
      x_r <= addrX; y_r <= addrY; --used by renderer
    end if;
  end process;
  
  coll_mux : process(chX,chY,tmpdirP,mtX,mtY,tmpdir,c_enaP,c_enaM)
  begin
    if butt_ena='1' then --hero values
      x_in<=chX; y_in<=chY;
      testDir<=tmpdirP; --check these values
      c_ena<=c_enaP;
    else --mob values
      x_in<=mtX; y_in<=mtY;
      testDir<=tmpdir; --check these values
      c_ena<=c_enaM;
    end if;
  end process;
  
  pl_anim : process(pl_ena)
  variable step : INTEGER range 0 to 15 := 0;
  begin
    if rising_edge(pl_ena) then
      oldX<=chX; oldY<=chY;
      chX<=chX; chY<=chY;
      if step=0 then --do it only once
        if btt(3)='1' then --right
          chx <= chx + 1;
        elsif btt(1)='1' then --left
          chx <= chx - 1;
        end if;
        if btt(2)='1' then --up
          chy <= chy - 1;
        elsif btt(0)='1' then --down
          chy <= chy + 1;
        end if;
      end if;
      step:=(step+1) mod 10;
    end if;
  end process;
  
  mob_anim : process(mob_ena)
  variable step : INTEGER range 0 to 15 := 0;
  begin
    if rising_edge(mob_ena) and false then
      mobX<=mobX; mobY<=mobY;
      MoldX<=mobX; MoldY<=mobY;
      if step=0 then --do it only once
        if dir(3)='1' then --right
          mobx <= (mobx + 1);
        elsif dir(1)='1' then --left
          mobx <= (mobx - 1);
        end if;
        if dir(2)='1' then --up
          moby <= (moby - 1);
        elsif dir(0)='1' then --down
          moby <= (moby + 1);
        end if;
      end if;
      step:=(step+1) mod 10;
    end if;
  end process;

end Behavioral;
