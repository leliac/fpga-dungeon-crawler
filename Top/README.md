All existing modules to be IPfied in Vivado 2017 and merged together in a single project here

On top
- make sure that the paths of the ip projects axiDataFetcher and rendVgaTmBoot are added to the ip repositories
- resolve all possible path errors

On upgrade of project rendVgaTmBoot
- implement (optional)
- repackage ip
then, on top project
- tools -> report ip status (refresh ip catalog if needed)
- upgrade  ips
- generate bitstream
- export hardware (including bitstream)
- launch sdk to test with board

Sdk warning: due to a bug, in order to get rid of some errors in project 'init', you may need to
- right click init_bsp project -> board support package settings
- disable library xilffs
- right click init_bsp project -> board support package settings
- enable library xilffs

To prepare microsd, in sdk
- right click project 'init' -> generate boot image
- the three required files should be selected automatically, confirm overwrite of previous boot image
- copy generated file .../init/bootimage/BOOT.bin to fat-formatted microsd
- copy desired 'maps.bin' and 'sprites.bin' files to microsd
