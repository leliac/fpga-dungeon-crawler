// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.1 (lin64) Build 1846317 Fri Apr 14 18:54:47 MDT 2017
// Date        : Fri Jun  9 14:39:59 2017
// Host        : surprise running 64-bit Linux Mint 18.1 Serena
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_axiDataFetcher_0_1_sim_netlist.v
// Design      : design_1_axiDataFetcher_0_1
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axiDataFetcher
   (m_axi_bready,
    dataPacket,
    m_axi_wvalid,
    m_axi_araddr,
    m_axi_arlen,
    m_axi_arsize,
    m_axi_arburst,
    m_axi_arvalid,
    m_axi_rready,
    fetching,
    m_axi_wstrb,
    m_axi_rvalid,
    m_axi_wready,
    dataType,
    m_axi_bvalid,
    m_axi_aresetn,
    dataId,
    m_axi_aclk,
    m_axi_rdata,
    m_axi_rlast,
    ena,
    m_axi_arready);
  output m_axi_bready;
  output [31:0]dataPacket;
  output m_axi_wvalid;
  output [31:0]m_axi_araddr;
  output [7:0]m_axi_arlen;
  output [0:0]m_axi_arsize;
  output [0:0]m_axi_arburst;
  output m_axi_arvalid;
  output m_axi_rready;
  output fetching;
  output [0:0]m_axi_wstrb;
  input m_axi_rvalid;
  input m_axi_wready;
  input dataType;
  input m_axi_bvalid;
  input m_axi_aresetn;
  input [7:0]dataId;
  input m_axi_aclk;
  input [31:0]m_axi_rdata;
  input m_axi_rlast;
  input ena;
  input m_axi_arready;

  wire \I_CMD_STATUS_MODULE/sig_cmd_empty_reg ;
  wire \I_CMD_STATUS_MODULE/sig_push_cmd_reg ;
  wire bus2ip_mst_cmdack;
  wire bus2ip_mst_cmplt;
  wire [7:0]dataId;
  wire [31:0]dataPacket;
  wire dataType;
  wire ena;
  wire fetching;
  wire [20:0]ip2bus_mst_addr;
  wire [13:8]ip2bus_mst_length_Reg;
  wire ip2bus_mstrd_req;
  wire m_axi_aclk;
  wire [31:0]m_axi_araddr;
  wire [0:0]m_axi_arburst;
  wire m_axi_aresetn;
  wire [7:0]m_axi_arlen;
  wire m_axi_arready;
  wire [0:0]m_axi_arsize;
  wire m_axi_arvalid;
  wire m_axi_bready;
  wire m_axi_bvalid;
  wire [31:0]m_axi_rdata;
  wire m_axi_rlast;
  wire m_axi_rready;
  wire m_axi_rvalid;
  wire m_axi_wready;
  wire [0:0]m_axi_wstrb;
  wire m_axi_wvalid;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axi_master_burst axiMasterBurst
       (.Q({ip2bus_mst_length_Reg[13],ip2bus_mst_length_Reg[8]}),
        .bus2ip_mst_cmdack(bus2ip_mst_cmdack),
        .bus2ip_mst_cmplt(bus2ip_mst_cmplt),
        .dataPacket(dataPacket),
        .fetching(fetching),
        .\ip2bus_mst_addr_reg[20] (ip2bus_mst_addr),
        .ip2bus_mstrd_req(ip2bus_mstrd_req),
        .m_axi_aclk(m_axi_aclk),
        .m_axi_araddr(m_axi_araddr),
        .m_axi_arburst(m_axi_arburst),
        .m_axi_aresetn(m_axi_aresetn),
        .m_axi_arlen(m_axi_arlen),
        .m_axi_arready(m_axi_arready),
        .m_axi_arsize(m_axi_arsize),
        .m_axi_arvalid(m_axi_arvalid),
        .m_axi_bready(m_axi_bready),
        .m_axi_bvalid(m_axi_bvalid),
        .m_axi_rdata(m_axi_rdata),
        .m_axi_rlast(m_axi_rlast),
        .m_axi_rready(m_axi_rready),
        .m_axi_rvalid(m_axi_rvalid),
        .m_axi_wready(m_axi_wready),
        .m_axi_wstrb(m_axi_wstrb),
        .m_axi_wvalid(m_axi_wvalid),
        .sig_cmd_empty_reg(\I_CMD_STATUS_MODULE/sig_cmd_empty_reg ),
        .sig_push_cmd_reg(\I_CMD_STATUS_MODULE/sig_push_cmd_reg ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axiDataFetcher_controller controller
       (.Q({ip2bus_mst_length_Reg[13],ip2bus_mst_length_Reg[8]}),
        .bus2ip_mst_cmdack(bus2ip_mst_cmdack),
        .bus2ip_mst_cmplt(bus2ip_mst_cmplt),
        .dataId(dataId),
        .dataType(dataType),
        .ena(ena),
        .ip2bus_mstrd_req(ip2bus_mstrd_req),
        .m_axi_aclk(m_axi_aclk),
        .m_axi_aresetn(m_axi_aresetn),
        .sig_cmd_empty_reg(\I_CMD_STATUS_MODULE/sig_cmd_empty_reg ),
        .\sig_cmd_mst_addr_reg[20] (ip2bus_mst_addr),
        .sig_push_cmd_reg(\I_CMD_STATUS_MODULE/sig_push_cmd_reg ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axiDataFetcher_controller
   (ip2bus_mstrd_req,
    sig_push_cmd_reg,
    Q,
    \sig_cmd_mst_addr_reg[20] ,
    dataId,
    m_axi_aclk,
    dataType,
    sig_cmd_empty_reg,
    bus2ip_mst_cmdack,
    bus2ip_mst_cmplt,
    ena,
    m_axi_aresetn);
  output ip2bus_mstrd_req;
  output sig_push_cmd_reg;
  output [1:0]Q;
  output [20:0]\sig_cmd_mst_addr_reg[20] ;
  input [7:0]dataId;
  input m_axi_aclk;
  input dataType;
  input sig_cmd_empty_reg;
  input bus2ip_mst_cmdack;
  input bus2ip_mst_cmplt;
  input ena;
  input m_axi_aresetn;

  wire [14:5]A;
  wire [1:0]Q;
  wire \axiMasterBurst/I_RESET_MODULE/sig_combined_reset ;
  wire bus2ip_mst_cmdack;
  wire bus2ip_mst_cmplt;
  wire [7:0]dataId;
  wire dataType;
  wire ena;
  wire enaReg;
  wire enaReg_i_1_n_0;
  wire ip2bus_mst_addr3_i_10_n_0;
  wire ip2bus_mst_addr3_i_11_n_0;
  wire ip2bus_mst_addr3_i_12_n_0;
  wire ip2bus_mst_addr3_i_2_n_0;
  wire ip2bus_mst_addr3_i_2_n_1;
  wire ip2bus_mst_addr3_i_2_n_2;
  wire ip2bus_mst_addr3_i_2_n_3;
  wire ip2bus_mst_addr3_i_3_n_0;
  wire ip2bus_mst_addr3_i_3_n_1;
  wire ip2bus_mst_addr3_i_3_n_2;
  wire ip2bus_mst_addr3_i_3_n_3;
  wire ip2bus_mst_addr3_i_4_n_0;
  wire ip2bus_mst_addr3_i_5_n_0;
  wire ip2bus_mst_addr3_i_6_n_0;
  wire ip2bus_mst_addr3_i_7_n_0;
  wire ip2bus_mst_addr3_i_8_n_0;
  wire ip2bus_mst_addr3_i_9_n_0;
  wire ip2bus_mst_addr3_n_100;
  wire ip2bus_mst_addr3_n_101;
  wire ip2bus_mst_addr3_n_102;
  wire ip2bus_mst_addr3_n_103;
  wire ip2bus_mst_addr3_n_104;
  wire ip2bus_mst_addr3_n_105;
  wire ip2bus_mst_addr3_n_85;
  wire ip2bus_mst_addr3_n_86;
  wire ip2bus_mst_addr3_n_87;
  wire ip2bus_mst_addr3_n_88;
  wire ip2bus_mst_addr3_n_89;
  wire ip2bus_mst_addr3_n_90;
  wire ip2bus_mst_addr3_n_91;
  wire ip2bus_mst_addr3_n_92;
  wire ip2bus_mst_addr3_n_93;
  wire ip2bus_mst_addr3_n_94;
  wire ip2bus_mst_addr3_n_95;
  wire ip2bus_mst_addr3_n_96;
  wire ip2bus_mst_addr3_n_97;
  wire ip2bus_mst_addr3_n_98;
  wire ip2bus_mst_addr3_n_99;
  wire \ip2bus_mst_addr[13]_i_1_n_0 ;
  wire \ip2bus_mst_addr[14]_i_1_n_0 ;
  wire \ip2bus_mst_addr[16]_i_1_n_0 ;
  wire \ip2bus_mst_addr[19]_i_1_n_0 ;
  wire \ip2bus_mst_length_Reg[13]_i_2_n_0 ;
  wire ip2bus_mstrd_req;
  wire ip2bus_mstrd_req_i_1_n_0;
  wire m_axi_aclk;
  wire m_axi_aresetn;
  wire [13:8]out0;
  wire [20:0]p_2_in;
  wire sig_cmd_empty_reg;
  wire [20:0]\sig_cmd_mst_addr_reg[20] ;
  wire sig_push_cmd_reg;
  wire [1:0]state;
  wire \state[0]_i_1_n_0 ;
  wire \state[1]_i_1_n_0 ;
  wire \state[1]_i_2_n_0 ;
  wire NLW_ip2bus_mst_addr3_CARRYCASCOUT_UNCONNECTED;
  wire NLW_ip2bus_mst_addr3_MULTSIGNOUT_UNCONNECTED;
  wire NLW_ip2bus_mst_addr3_OVERFLOW_UNCONNECTED;
  wire NLW_ip2bus_mst_addr3_PATTERNBDETECT_UNCONNECTED;
  wire NLW_ip2bus_mst_addr3_PATTERNDETECT_UNCONNECTED;
  wire NLW_ip2bus_mst_addr3_UNDERFLOW_UNCONNECTED;
  wire [29:0]NLW_ip2bus_mst_addr3_ACOUT_UNCONNECTED;
  wire [17:0]NLW_ip2bus_mst_addr3_BCOUT_UNCONNECTED;
  wire [3:0]NLW_ip2bus_mst_addr3_CARRYOUT_UNCONNECTED;
  wire [47:21]NLW_ip2bus_mst_addr3_P_UNCONNECTED;
  wire [47:0]NLW_ip2bus_mst_addr3_PCOUT_UNCONNECTED;
  wire [3:0]NLW_ip2bus_mst_addr3_i_1_CO_UNCONNECTED;
  wire [3:1]NLW_ip2bus_mst_addr3_i_1_O_UNCONNECTED;

  LUT5 #(
    .INIT(32'hAB000000)) 
    enaReg_i_1
       (.I0(enaReg),
        .I1(state[0]),
        .I2(state[1]),
        .I3(ena),
        .I4(m_axi_aresetn),
        .O(enaReg_i_1_n_0));
  FDRE enaReg_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(enaReg_i_1_n_0),
        .Q(enaReg),
        .R(1'b0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-13 {cell *THIS*}}" *) 
  DSP48E1 #(
    .ACASCREG(0),
    .ADREG(1),
    .ALUMODEREG(0),
    .AREG(0),
    .AUTORESET_PATDET("NO_RESET"),
    .A_INPUT("DIRECT"),
    .BCASCREG(0),
    .BREG(0),
    .B_INPUT("DIRECT"),
    .CARRYINREG(0),
    .CARRYINSELREG(0),
    .CREG(1),
    .DREG(1),
    .INMODEREG(0),
    .MASK(48'h3FFFFFFFFFFF),
    .MREG(0),
    .OPMODEREG(0),
    .PATTERN(48'h000000000000),
    .PREG(0),
    .SEL_MASK("MASK"),
    .SEL_PATTERN("PATTERN"),
    .USE_DPORT("FALSE"),
    .USE_MULT("MULTIPLY"),
    .USE_PATTERN_DETECT("NO_PATDET"),
    .USE_SIMD("ONE48")) 
    ip2bus_mst_addr3
       (.A({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,A,dataId[0],1'b0,1'b0,1'b0,1'b0}),
        .ACIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .ACOUT(NLW_ip2bus_mst_addr3_ACOUT_UNCONNECTED[29:0]),
        .ALUMODE({1'b0,1'b0,1'b0,1'b0}),
        .B({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b1,1'b1,1'b1,1'b0,1'b0}),
        .BCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .BCOUT(NLW_ip2bus_mst_addr3_BCOUT_UNCONNECTED[17:0]),
        .C({1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1,1'b1}),
        .CARRYCASCIN(1'b0),
        .CARRYCASCOUT(NLW_ip2bus_mst_addr3_CARRYCASCOUT_UNCONNECTED),
        .CARRYIN(1'b0),
        .CARRYINSEL({1'b0,1'b0,1'b0}),
        .CARRYOUT(NLW_ip2bus_mst_addr3_CARRYOUT_UNCONNECTED[3:0]),
        .CEA1(1'b0),
        .CEA2(1'b0),
        .CEAD(1'b0),
        .CEALUMODE(1'b0),
        .CEB1(1'b0),
        .CEB2(1'b0),
        .CEC(1'b0),
        .CECARRYIN(1'b0),
        .CECTRL(1'b0),
        .CED(1'b0),
        .CEINMODE(1'b0),
        .CEM(1'b0),
        .CEP(1'b0),
        .CLK(1'b0),
        .D({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .INMODE({1'b0,1'b0,1'b0,1'b0,1'b0}),
        .MULTSIGNIN(1'b0),
        .MULTSIGNOUT(NLW_ip2bus_mst_addr3_MULTSIGNOUT_UNCONNECTED),
        .OPMODE({1'b0,1'b0,1'b0,1'b0,1'b1,1'b0,1'b1}),
        .OVERFLOW(NLW_ip2bus_mst_addr3_OVERFLOW_UNCONNECTED),
        .P({NLW_ip2bus_mst_addr3_P_UNCONNECTED[47:21],ip2bus_mst_addr3_n_85,ip2bus_mst_addr3_n_86,ip2bus_mst_addr3_n_87,ip2bus_mst_addr3_n_88,ip2bus_mst_addr3_n_89,ip2bus_mst_addr3_n_90,ip2bus_mst_addr3_n_91,ip2bus_mst_addr3_n_92,ip2bus_mst_addr3_n_93,ip2bus_mst_addr3_n_94,ip2bus_mst_addr3_n_95,ip2bus_mst_addr3_n_96,ip2bus_mst_addr3_n_97,ip2bus_mst_addr3_n_98,ip2bus_mst_addr3_n_99,ip2bus_mst_addr3_n_100,ip2bus_mst_addr3_n_101,ip2bus_mst_addr3_n_102,ip2bus_mst_addr3_n_103,ip2bus_mst_addr3_n_104,ip2bus_mst_addr3_n_105}),
        .PATTERNBDETECT(NLW_ip2bus_mst_addr3_PATTERNBDETECT_UNCONNECTED),
        .PATTERNDETECT(NLW_ip2bus_mst_addr3_PATTERNDETECT_UNCONNECTED),
        .PCIN({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .PCOUT(NLW_ip2bus_mst_addr3_PCOUT_UNCONNECTED[47:0]),
        .RSTA(1'b0),
        .RSTALLCARRYIN(1'b0),
        .RSTALUMODE(1'b0),
        .RSTB(1'b0),
        .RSTC(1'b0),
        .RSTCTRL(1'b0),
        .RSTD(1'b0),
        .RSTINMODE(1'b0),
        .RSTM(1'b0),
        .RSTP(1'b0),
        .UNDERFLOW(NLW_ip2bus_mst_addr3_UNDERFLOW_UNCONNECTED));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-9 {cell *THIS*} {string 8x7}}" *) 
  CARRY4 ip2bus_mst_addr3_i_1
       (.CI(ip2bus_mst_addr3_i_2_n_0),
        .CO({NLW_ip2bus_mst_addr3_i_1_CO_UNCONNECTED[3:2],A[14],NLW_ip2bus_mst_addr3_i_1_CO_UNCONNECTED[0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_ip2bus_mst_addr3_i_1_O_UNCONNECTED[3:1],A[13]}),
        .S({1'b0,1'b0,1'b1,ip2bus_mst_addr3_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    ip2bus_mst_addr3_i_10
       (.I0(dataId[1]),
        .I1(dataId[3]),
        .O(ip2bus_mst_addr3_i_10_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    ip2bus_mst_addr3_i_11
       (.I0(dataId[0]),
        .I1(dataId[2]),
        .O(ip2bus_mst_addr3_i_11_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    ip2bus_mst_addr3_i_12
       (.I0(dataId[1]),
        .O(ip2bus_mst_addr3_i_12_n_0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-9 {cell *THIS*} {string 8x7}}" *) 
  CARRY4 ip2bus_mst_addr3_i_2
       (.CI(ip2bus_mst_addr3_i_3_n_0),
        .CO({ip2bus_mst_addr3_i_2_n_0,ip2bus_mst_addr3_i_2_n_1,ip2bus_mst_addr3_i_2_n_2,ip2bus_mst_addr3_i_2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,dataId[5:3]}),
        .O(A[12:9]),
        .S({ip2bus_mst_addr3_i_5_n_0,ip2bus_mst_addr3_i_6_n_0,ip2bus_mst_addr3_i_7_n_0,ip2bus_mst_addr3_i_8_n_0}));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-9 {cell *THIS*} {string 8x7}}" *) 
  CARRY4 ip2bus_mst_addr3_i_3
       (.CI(1'b0),
        .CO({ip2bus_mst_addr3_i_3_n_0,ip2bus_mst_addr3_i_3_n_1,ip2bus_mst_addr3_i_3_n_2,ip2bus_mst_addr3_i_3_n_3}),
        .CYINIT(1'b0),
        .DI({dataId[2:0],1'b0}),
        .O(A[8:5]),
        .S({ip2bus_mst_addr3_i_9_n_0,ip2bus_mst_addr3_i_10_n_0,ip2bus_mst_addr3_i_11_n_0,ip2bus_mst_addr3_i_12_n_0}));
  LUT1 #(
    .INIT(2'h2)) 
    ip2bus_mst_addr3_i_4
       (.I0(dataId[7]),
        .O(ip2bus_mst_addr3_i_4_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    ip2bus_mst_addr3_i_5
       (.I0(dataId[6]),
        .O(ip2bus_mst_addr3_i_5_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    ip2bus_mst_addr3_i_6
       (.I0(dataId[5]),
        .I1(dataId[7]),
        .O(ip2bus_mst_addr3_i_6_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    ip2bus_mst_addr3_i_7
       (.I0(dataId[4]),
        .I1(dataId[6]),
        .O(ip2bus_mst_addr3_i_7_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    ip2bus_mst_addr3_i_8
       (.I0(dataId[3]),
        .I1(dataId[5]),
        .O(ip2bus_mst_addr3_i_8_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    ip2bus_mst_addr3_i_9
       (.I0(dataId[2]),
        .I1(dataId[4]),
        .O(ip2bus_mst_addr3_i_9_n_0));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \ip2bus_mst_addr[0]_i_1 
       (.I0(ip2bus_mst_addr3_n_105),
        .I1(dataType),
        .I2(state[1]),
        .O(p_2_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \ip2bus_mst_addr[10]_i_1 
       (.I0(ip2bus_mst_addr3_n_95),
        .I1(dataType),
        .I2(state[1]),
        .O(p_2_in[10]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \ip2bus_mst_addr[11]_i_1 
       (.I0(ip2bus_mst_addr3_n_94),
        .I1(dataType),
        .I2(state[1]),
        .O(p_2_in[11]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \ip2bus_mst_addr[12]_i_1 
       (.I0(ip2bus_mst_addr3_n_93),
        .I1(dataType),
        .I2(state[1]),
        .O(p_2_in[12]));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'h54)) 
    \ip2bus_mst_addr[13]_i_1 
       (.I0(state[1]),
        .I1(ip2bus_mst_addr3_n_92),
        .I2(dataType),
        .O(\ip2bus_mst_addr[13]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'h54)) 
    \ip2bus_mst_addr[14]_i_1 
       (.I0(state[1]),
        .I1(ip2bus_mst_addr3_n_91),
        .I2(dataType),
        .O(\ip2bus_mst_addr[14]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \ip2bus_mst_addr[15]_i_1 
       (.I0(ip2bus_mst_addr3_n_90),
        .I1(dataType),
        .I2(state[1]),
        .O(p_2_in[15]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'h54)) 
    \ip2bus_mst_addr[16]_i_1 
       (.I0(state[1]),
        .I1(ip2bus_mst_addr3_n_89),
        .I2(dataType),
        .O(\ip2bus_mst_addr[16]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \ip2bus_mst_addr[17]_i_1 
       (.I0(ip2bus_mst_addr3_n_88),
        .I1(dataType),
        .I2(state[1]),
        .O(p_2_in[17]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \ip2bus_mst_addr[18]_i_1 
       (.I0(ip2bus_mst_addr3_n_87),
        .I1(dataType),
        .I2(state[1]),
        .O(p_2_in[18]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'h54)) 
    \ip2bus_mst_addr[19]_i_1 
       (.I0(state[1]),
        .I1(ip2bus_mst_addr3_n_86),
        .I2(dataType),
        .O(\ip2bus_mst_addr[19]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \ip2bus_mst_addr[1]_i_1 
       (.I0(ip2bus_mst_addr3_n_104),
        .I1(dataType),
        .I2(state[1]),
        .O(p_2_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \ip2bus_mst_addr[20]_i_1 
       (.I0(ip2bus_mst_addr3_n_85),
        .I1(dataType),
        .I2(state[1]),
        .O(p_2_in[20]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \ip2bus_mst_addr[2]_i_1 
       (.I0(ip2bus_mst_addr3_n_103),
        .I1(dataType),
        .I2(state[1]),
        .O(p_2_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \ip2bus_mst_addr[3]_i_1 
       (.I0(ip2bus_mst_addr3_n_102),
        .I1(dataType),
        .I2(state[1]),
        .O(p_2_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \ip2bus_mst_addr[4]_i_1 
       (.I0(ip2bus_mst_addr3_n_101),
        .I1(dataType),
        .I2(state[1]),
        .O(p_2_in[4]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \ip2bus_mst_addr[5]_i_1 
       (.I0(ip2bus_mst_addr3_n_100),
        .I1(dataType),
        .I2(state[1]),
        .O(p_2_in[5]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \ip2bus_mst_addr[6]_i_1 
       (.I0(ip2bus_mst_addr3_n_99),
        .I1(dataType),
        .I2(state[1]),
        .O(p_2_in[6]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \ip2bus_mst_addr[7]_i_1 
       (.I0(ip2bus_mst_addr3_n_98),
        .I1(dataType),
        .I2(state[1]),
        .O(p_2_in[7]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \ip2bus_mst_addr[8]_i_1 
       (.I0(ip2bus_mst_addr3_n_97),
        .I1(dataType),
        .I2(state[1]),
        .O(p_2_in[8]));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \ip2bus_mst_addr[9]_i_1 
       (.I0(ip2bus_mst_addr3_n_96),
        .I1(dataType),
        .I2(state[1]),
        .O(p_2_in[9]));
  FDRE \ip2bus_mst_addr_reg[0] 
       (.C(m_axi_aclk),
        .CE(\ip2bus_mst_length_Reg[13]_i_2_n_0 ),
        .D(p_2_in[0]),
        .Q(\sig_cmd_mst_addr_reg[20] [0]),
        .R(\axiMasterBurst/I_RESET_MODULE/sig_combined_reset ));
  FDRE \ip2bus_mst_addr_reg[10] 
       (.C(m_axi_aclk),
        .CE(\ip2bus_mst_length_Reg[13]_i_2_n_0 ),
        .D(p_2_in[10]),
        .Q(\sig_cmd_mst_addr_reg[20] [10]),
        .R(\axiMasterBurst/I_RESET_MODULE/sig_combined_reset ));
  FDRE \ip2bus_mst_addr_reg[11] 
       (.C(m_axi_aclk),
        .CE(\ip2bus_mst_length_Reg[13]_i_2_n_0 ),
        .D(p_2_in[11]),
        .Q(\sig_cmd_mst_addr_reg[20] [11]),
        .R(\axiMasterBurst/I_RESET_MODULE/sig_combined_reset ));
  FDRE \ip2bus_mst_addr_reg[12] 
       (.C(m_axi_aclk),
        .CE(\ip2bus_mst_length_Reg[13]_i_2_n_0 ),
        .D(p_2_in[12]),
        .Q(\sig_cmd_mst_addr_reg[20] [12]),
        .R(\axiMasterBurst/I_RESET_MODULE/sig_combined_reset ));
  FDRE \ip2bus_mst_addr_reg[13] 
       (.C(m_axi_aclk),
        .CE(\ip2bus_mst_length_Reg[13]_i_2_n_0 ),
        .D(\ip2bus_mst_addr[13]_i_1_n_0 ),
        .Q(\sig_cmd_mst_addr_reg[20] [13]),
        .R(\axiMasterBurst/I_RESET_MODULE/sig_combined_reset ));
  FDRE \ip2bus_mst_addr_reg[14] 
       (.C(m_axi_aclk),
        .CE(\ip2bus_mst_length_Reg[13]_i_2_n_0 ),
        .D(\ip2bus_mst_addr[14]_i_1_n_0 ),
        .Q(\sig_cmd_mst_addr_reg[20] [14]),
        .R(\axiMasterBurst/I_RESET_MODULE/sig_combined_reset ));
  FDRE \ip2bus_mst_addr_reg[15] 
       (.C(m_axi_aclk),
        .CE(\ip2bus_mst_length_Reg[13]_i_2_n_0 ),
        .D(p_2_in[15]),
        .Q(\sig_cmd_mst_addr_reg[20] [15]),
        .R(\axiMasterBurst/I_RESET_MODULE/sig_combined_reset ));
  FDRE \ip2bus_mst_addr_reg[16] 
       (.C(m_axi_aclk),
        .CE(\ip2bus_mst_length_Reg[13]_i_2_n_0 ),
        .D(\ip2bus_mst_addr[16]_i_1_n_0 ),
        .Q(\sig_cmd_mst_addr_reg[20] [16]),
        .R(\axiMasterBurst/I_RESET_MODULE/sig_combined_reset ));
  FDRE \ip2bus_mst_addr_reg[17] 
       (.C(m_axi_aclk),
        .CE(\ip2bus_mst_length_Reg[13]_i_2_n_0 ),
        .D(p_2_in[17]),
        .Q(\sig_cmd_mst_addr_reg[20] [17]),
        .R(\axiMasterBurst/I_RESET_MODULE/sig_combined_reset ));
  FDRE \ip2bus_mst_addr_reg[18] 
       (.C(m_axi_aclk),
        .CE(\ip2bus_mst_length_Reg[13]_i_2_n_0 ),
        .D(p_2_in[18]),
        .Q(\sig_cmd_mst_addr_reg[20] [18]),
        .R(\axiMasterBurst/I_RESET_MODULE/sig_combined_reset ));
  FDRE \ip2bus_mst_addr_reg[19] 
       (.C(m_axi_aclk),
        .CE(\ip2bus_mst_length_Reg[13]_i_2_n_0 ),
        .D(\ip2bus_mst_addr[19]_i_1_n_0 ),
        .Q(\sig_cmd_mst_addr_reg[20] [19]),
        .R(\axiMasterBurst/I_RESET_MODULE/sig_combined_reset ));
  FDRE \ip2bus_mst_addr_reg[1] 
       (.C(m_axi_aclk),
        .CE(\ip2bus_mst_length_Reg[13]_i_2_n_0 ),
        .D(p_2_in[1]),
        .Q(\sig_cmd_mst_addr_reg[20] [1]),
        .R(\axiMasterBurst/I_RESET_MODULE/sig_combined_reset ));
  FDRE \ip2bus_mst_addr_reg[20] 
       (.C(m_axi_aclk),
        .CE(\ip2bus_mst_length_Reg[13]_i_2_n_0 ),
        .D(p_2_in[20]),
        .Q(\sig_cmd_mst_addr_reg[20] [20]),
        .R(\axiMasterBurst/I_RESET_MODULE/sig_combined_reset ));
  FDRE \ip2bus_mst_addr_reg[2] 
       (.C(m_axi_aclk),
        .CE(\ip2bus_mst_length_Reg[13]_i_2_n_0 ),
        .D(p_2_in[2]),
        .Q(\sig_cmd_mst_addr_reg[20] [2]),
        .R(\axiMasterBurst/I_RESET_MODULE/sig_combined_reset ));
  FDRE \ip2bus_mst_addr_reg[3] 
       (.C(m_axi_aclk),
        .CE(\ip2bus_mst_length_Reg[13]_i_2_n_0 ),
        .D(p_2_in[3]),
        .Q(\sig_cmd_mst_addr_reg[20] [3]),
        .R(\axiMasterBurst/I_RESET_MODULE/sig_combined_reset ));
  FDRE \ip2bus_mst_addr_reg[4] 
       (.C(m_axi_aclk),
        .CE(\ip2bus_mst_length_Reg[13]_i_2_n_0 ),
        .D(p_2_in[4]),
        .Q(\sig_cmd_mst_addr_reg[20] [4]),
        .R(\axiMasterBurst/I_RESET_MODULE/sig_combined_reset ));
  FDRE \ip2bus_mst_addr_reg[5] 
       (.C(m_axi_aclk),
        .CE(\ip2bus_mst_length_Reg[13]_i_2_n_0 ),
        .D(p_2_in[5]),
        .Q(\sig_cmd_mst_addr_reg[20] [5]),
        .R(\axiMasterBurst/I_RESET_MODULE/sig_combined_reset ));
  FDRE \ip2bus_mst_addr_reg[6] 
       (.C(m_axi_aclk),
        .CE(\ip2bus_mst_length_Reg[13]_i_2_n_0 ),
        .D(p_2_in[6]),
        .Q(\sig_cmd_mst_addr_reg[20] [6]),
        .R(\axiMasterBurst/I_RESET_MODULE/sig_combined_reset ));
  FDRE \ip2bus_mst_addr_reg[7] 
       (.C(m_axi_aclk),
        .CE(\ip2bus_mst_length_Reg[13]_i_2_n_0 ),
        .D(p_2_in[7]),
        .Q(\sig_cmd_mst_addr_reg[20] [7]),
        .R(\axiMasterBurst/I_RESET_MODULE/sig_combined_reset ));
  FDRE \ip2bus_mst_addr_reg[8] 
       (.C(m_axi_aclk),
        .CE(\ip2bus_mst_length_Reg[13]_i_2_n_0 ),
        .D(p_2_in[8]),
        .Q(\sig_cmd_mst_addr_reg[20] [8]),
        .R(\axiMasterBurst/I_RESET_MODULE/sig_combined_reset ));
  FDRE \ip2bus_mst_addr_reg[9] 
       (.C(m_axi_aclk),
        .CE(\ip2bus_mst_length_Reg[13]_i_2_n_0 ),
        .D(p_2_in[9]),
        .Q(\sig_cmd_mst_addr_reg[20] [9]),
        .R(\axiMasterBurst/I_RESET_MODULE/sig_combined_reset ));
  LUT1 #(
    .INIT(2'h1)) 
    \ip2bus_mst_length_Reg[13]_i_1 
       (.I0(m_axi_aresetn),
        .O(\axiMasterBurst/I_RESET_MODULE/sig_combined_reset ));
  LUT3 #(
    .INIT(8'h4A)) 
    \ip2bus_mst_length_Reg[13]_i_2 
       (.I0(state[0]),
        .I1(bus2ip_mst_cmdack),
        .I2(state[1]),
        .O(\ip2bus_mst_length_Reg[13]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \ip2bus_mst_length_Reg[13]_i_3 
       (.I0(dataType),
        .I1(state[1]),
        .O(out0[13]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \ip2bus_mst_length_Reg[8]_i_1 
       (.I0(state[1]),
        .I1(dataType),
        .O(out0[8]));
  FDRE \ip2bus_mst_length_Reg_reg[13] 
       (.C(m_axi_aclk),
        .CE(\ip2bus_mst_length_Reg[13]_i_2_n_0 ),
        .D(out0[13]),
        .Q(Q[1]),
        .R(\axiMasterBurst/I_RESET_MODULE/sig_combined_reset ));
  FDRE \ip2bus_mst_length_Reg_reg[8] 
       (.C(m_axi_aclk),
        .CE(\ip2bus_mst_length_Reg[13]_i_2_n_0 ),
        .D(out0[8]),
        .Q(Q[0]),
        .R(\axiMasterBurst/I_RESET_MODULE/sig_combined_reset ));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT1 #(
    .INIT(2'h1)) 
    ip2bus_mstrd_req_i_1
       (.I0(state[1]),
        .O(ip2bus_mstrd_req_i_1_n_0));
  FDRE ip2bus_mstrd_req_reg
       (.C(m_axi_aclk),
        .CE(\ip2bus_mst_length_Reg[13]_i_2_n_0 ),
        .D(ip2bus_mstrd_req_i_1_n_0),
        .Q(ip2bus_mstrd_req),
        .R(\axiMasterBurst/I_RESET_MODULE/sig_combined_reset ));
  LUT2 #(
    .INIT(4'h8)) 
    sig_cmd_full_reg_i_2
       (.I0(ip2bus_mstrd_req),
        .I1(sig_cmd_empty_reg),
        .O(sig_push_cmd_reg));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'h60)) 
    \state[0]_i_1 
       (.I0(state[0]),
        .I1(\state[1]_i_2_n_0 ),
        .I2(m_axi_aresetn),
        .O(\state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT4 #(
    .INIT(16'h6A00)) 
    \state[1]_i_1 
       (.I0(state[1]),
        .I1(\state[1]_i_2_n_0 ),
        .I2(state[0]),
        .I3(m_axi_aresetn),
        .O(\state[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hCAF0CAF0CAFFCAF0)) 
    \state[1]_i_2 
       (.I0(bus2ip_mst_cmdack),
        .I1(bus2ip_mst_cmplt),
        .I2(state[0]),
        .I3(state[1]),
        .I4(ena),
        .I5(enaReg),
        .O(\state[1]_i_2_n_0 ));
  FDRE \state_reg[0] 
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(\state[0]_i_1_n_0 ),
        .Q(state[0]),
        .R(1'b0));
  FDRE \state_reg[1] 
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(\state[1]_i_1_n_0 ),
        .Q(state[1]),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axi_master_burst
   (m_axi_wvalid,
    m_axi_arsize,
    m_axi_arburst,
    m_axi_arvalid,
    m_axi_wstrb,
    sig_cmd_empty_reg,
    bus2ip_mst_cmplt,
    bus2ip_mst_cmdack,
    m_axi_bready,
    m_axi_rready,
    fetching,
    dataPacket,
    m_axi_araddr,
    m_axi_arlen,
    m_axi_aclk,
    sig_push_cmd_reg,
    ip2bus_mstrd_req,
    m_axi_rvalid,
    m_axi_wready,
    m_axi_bvalid,
    m_axi_aresetn,
    m_axi_rlast,
    m_axi_arready,
    m_axi_rdata,
    Q,
    \ip2bus_mst_addr_reg[20] );
  output m_axi_wvalid;
  output [0:0]m_axi_arsize;
  output [0:0]m_axi_arburst;
  output m_axi_arvalid;
  output [0:0]m_axi_wstrb;
  output sig_cmd_empty_reg;
  output bus2ip_mst_cmplt;
  output bus2ip_mst_cmdack;
  output m_axi_bready;
  output m_axi_rready;
  output fetching;
  output [31:0]dataPacket;
  output [31:0]m_axi_araddr;
  output [7:0]m_axi_arlen;
  input m_axi_aclk;
  input sig_push_cmd_reg;
  input ip2bus_mstrd_req;
  input m_axi_rvalid;
  input m_axi_wready;
  input m_axi_bvalid;
  input m_axi_aresetn;
  input m_axi_rlast;
  input m_axi_arready;
  input [31:0]m_axi_rdata;
  input [1:0]Q;
  input [20:0]\ip2bus_mst_addr_reg[20] ;

  wire \I_ADDR_CNTL/sig_rd_addr_valid_reg0 ;
  wire I_CMD_STATUS_MODULE_n_29;
  wire I_CMD_STATUS_MODULE_n_30;
  wire I_CMD_STATUS_MODULE_n_31;
  wire I_CMD_STATUS_MODULE_n_32;
  wire I_CMD_STATUS_MODULE_n_33;
  wire I_CMD_STATUS_MODULE_n_37;
  wire [1:0]\I_MSTR_PCC/p_1_in ;
  wire [14:8]\I_MSTR_PCC/sig_btt_cntr0 ;
  wire \I_MSTR_PCC/sig_calc_error_reg0 ;
  wire I_RD_WR_CNTRL_MODULE_n_22;
  wire I_RD_WR_CNTRL_MODULE_n_23;
  wire I_RD_WR_CNTRL_MODULE_n_24;
  wire \I_READ_STREAM_SKID_BUF/p_0_in2_in ;
  wire \I_READ_STREAM_SKID_BUF/sig_data_reg_out_en ;
  wire [1:0]Q;
  wire bus2ip_mst_cmdack;
  wire bus2ip_mst_cmplt;
  wire [31:0]dataPacket;
  wire fetching;
  wire [20:0]\ip2bus_mst_addr_reg[20] ;
  wire ip2bus_mstrd_req;
  wire m_axi_aclk;
  wire [31:0]m_axi_araddr;
  wire [0:0]m_axi_arburst;
  wire m_axi_aresetn;
  wire [7:0]m_axi_arlen;
  wire m_axi_arready;
  wire [0:0]m_axi_arsize;
  wire m_axi_arvalid;
  wire m_axi_bready;
  wire m_axi_bvalid;
  wire [31:0]m_axi_rdata;
  wire m_axi_rlast;
  wire m_axi_rready;
  wire m_axi_rvalid;
  wire m_axi_wready;
  wire [0:0]m_axi_wstrb;
  wire m_axi_wvalid;
  wire sig_cmd2all_doing_read;
  wire sig_cmd2pcc_cmd_valid;
  wire sig_cmd_empty_reg;
  wire [20:2]sig_cmd_mst_addr;
  wire sig_cmd_mstrd_req;
  wire sig_cmd_mstrd_req0;
  wire sig_doing_read_reg;
  wire sig_llink2cmd_rd_busy;
  wire sig_llink2rd_allow_addr_req;
  wire sig_pcc2all_calc_err;
  wire sig_pcc2data_calc_error;
  wire sig_pcc_taking_command;
  wire sig_push_cmd_reg;
  wire sig_push_status1_out;
  wire sig_rd2llink_strm_tvalid;
  wire sig_rd_discontinue;
  wire sig_rd_llink_enable;
  wire sig_rdwr2llink_int_err;
  wire [4:4]sig_rsc2stat_status;
  wire sig_rsc2stat_status_valid;
  wire sig_rst2cmd_stat_reset;
  wire sig_rst2llink_reset;
  wire sig_rst2rdwr_cntlr_reset;
  wire sig_status_reg_empty;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axi_master_burst_cmd_status I_CMD_STATUS_MODULE
       (.D(\I_MSTR_PCC/p_1_in ),
        .\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[1] ({I_RD_WR_CNTRL_MODULE_n_23,I_RD_WR_CNTRL_MODULE_n_24}),
        .Q(sig_cmd_mst_addr),
        .SR(sig_cmd_mstrd_req0),
        .bus2ip_mst_cmdack(bus2ip_mst_cmdack),
        .\ip2bus_mst_addr_reg[20] (\ip2bus_mst_addr_reg[20] ),
        .\ip2bus_mst_length_Reg_reg[13] (Q),
        .ip2bus_mstrd_req(ip2bus_mstrd_req),
        .m_axi_aclk(m_axi_aclk),
        .out(sig_rst2cmd_stat_reset),
        .sig_btt_cntr0({\I_MSTR_PCC/sig_btt_cntr0 [14:13],\I_MSTR_PCC/sig_btt_cntr0 [11],\I_MSTR_PCC/sig_btt_cntr0 [9:8]}),
        .\sig_btt_cntr_reg[14] ({I_CMD_STATUS_MODULE_n_29,I_CMD_STATUS_MODULE_n_30,I_CMD_STATUS_MODULE_n_31,I_CMD_STATUS_MODULE_n_32,I_CMD_STATUS_MODULE_n_33}),
        .sig_calc_error_reg0(\I_MSTR_PCC/sig_calc_error_reg0 ),
        .sig_calc_error_reg_reg(I_CMD_STATUS_MODULE_n_37),
        .sig_cmd2all_doing_read(sig_cmd2all_doing_read),
        .sig_cmd2pcc_cmd_valid(sig_cmd2pcc_cmd_valid),
        .sig_cmd_cmplt_reg_reg_0(bus2ip_mst_cmplt),
        .sig_cmd_empty_reg(sig_cmd_empty_reg),
        .sig_cmd_mstrd_req(sig_cmd_mstrd_req),
        .sig_doing_read_reg(sig_doing_read_reg),
        .sig_llink2cmd_rd_busy(sig_llink2cmd_rd_busy),
        .sig_pcc2all_calc_err(sig_pcc2all_calc_err),
        .sig_pcc2data_calc_error(sig_pcc2data_calc_error),
        .sig_pcc_taking_command(sig_pcc_taking_command),
        .sig_push_cmd_reg(sig_push_cmd_reg),
        .sig_push_status1_out(sig_push_status1_out),
        .sig_rd_addr_valid_reg0(\I_ADDR_CNTL/sig_rd_addr_valid_reg0 ),
        .sig_rd_llink_enable(sig_rd_llink_enable),
        .sig_rdwr2llink_int_err(sig_rdwr2llink_int_err),
        .sig_rsc2stat_status(sig_rsc2stat_status),
        .sig_rsc2stat_status_valid(sig_rsc2stat_status_valid),
        .sig_status_reg_empty(sig_status_reg_empty));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axi_master_burst_rd_llink I_RD_LLINK_ADAPTER
       (.E(\I_READ_STREAM_SKID_BUF/sig_data_reg_out_en ),
        .fetching(fetching),
        .m_axi_aclk(m_axi_aclk),
        .out(sig_rst2llink_reset),
        .sig_cmd2all_doing_read(sig_cmd2all_doing_read),
        .sig_doing_read_reg(sig_doing_read_reg),
        .sig_last_reg_out_reg(I_RD_WR_CNTRL_MODULE_n_22),
        .sig_llink2cmd_rd_busy(sig_llink2cmd_rd_busy),
        .sig_llink2rd_allow_addr_req(sig_llink2rd_allow_addr_req),
        .sig_m_valid_dup_reg(\I_READ_STREAM_SKID_BUF/p_0_in2_in ),
        .sig_m_valid_out_reg(sig_rd2llink_strm_tvalid),
        .sig_rd_discontinue(sig_rd_discontinue),
        .sig_rdwr2llink_int_err(sig_rdwr2llink_int_err));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axi_master_burst_rd_wr_cntlr I_RD_WR_CNTRL_MODULE
       (.D(\I_MSTR_PCC/p_1_in ),
        .E(\I_READ_STREAM_SKID_BUF/sig_data_reg_out_en ),
        .\GEN_ADDR_32.sig_addr_cntr_lsh_reg[1] ({I_RD_WR_CNTRL_MODULE_n_23,I_RD_WR_CNTRL_MODULE_n_24}),
        .Q(sig_cmd_mst_addr),
        .SR(sig_cmd_mstrd_req0),
        .dataPacket(dataPacket),
        .m_axi_aclk(m_axi_aclk),
        .m_axi_araddr(m_axi_araddr),
        .m_axi_arburst(m_axi_arburst),
        .m_axi_arlen(m_axi_arlen),
        .m_axi_arready(m_axi_arready),
        .m_axi_arsize(m_axi_arsize),
        .m_axi_arvalid(m_axi_arvalid),
        .m_axi_bready(m_axi_bready),
        .m_axi_bvalid(m_axi_bvalid),
        .m_axi_rdata(m_axi_rdata),
        .m_axi_rlast(m_axi_rlast),
        .m_axi_rready(m_axi_rready),
        .m_axi_rvalid(m_axi_rvalid),
        .m_axi_wready(m_axi_wready),
        .m_axi_wstrb(m_axi_wstrb),
        .m_axi_wvalid(m_axi_wvalid),
        .out(sig_rst2rdwr_cntlr_reset),
        .\sig_btt_cntr_reg[14] ({\I_MSTR_PCC/sig_btt_cntr0 [14:13],\I_MSTR_PCC/sig_btt_cntr0 [11],\I_MSTR_PCC/sig_btt_cntr0 [9:8]}),
        .sig_calc_error_reg0(\I_MSTR_PCC/sig_calc_error_reg0 ),
        .sig_cmd2all_doing_read(sig_cmd2all_doing_read),
        .sig_cmd2pcc_cmd_valid(sig_cmd2pcc_cmd_valid),
        .\sig_cmd_mst_length_reg[13] (I_CMD_STATUS_MODULE_n_37),
        .sig_cmd_mstrd_req(sig_cmd_mstrd_req),
        .sig_cmd_mstrd_req_reg({I_CMD_STATUS_MODULE_n_29,I_CMD_STATUS_MODULE_n_30,I_CMD_STATUS_MODULE_n_31,I_CMD_STATUS_MODULE_n_32,I_CMD_STATUS_MODULE_n_33}),
        .sig_cmd_reset_reg_reg(sig_rst2cmd_stat_reset),
        .sig_doing_read_reg(sig_doing_read_reg),
        .sig_llink2cmd_rd_busy(sig_llink2cmd_rd_busy),
        .sig_llink2rd_allow_addr_req(sig_llink2rd_allow_addr_req),
        .sig_llink_busy_reg(sig_rd2llink_strm_tvalid),
        .sig_llink_busy_reg_0(I_RD_WR_CNTRL_MODULE_n_22),
        .sig_llink_reset_reg_reg(sig_rst2llink_reset),
        .sig_m_valid_out_reg(\I_READ_STREAM_SKID_BUF/p_0_in2_in ),
        .sig_pcc2all_calc_err(sig_pcc2all_calc_err),
        .sig_pcc2data_calc_error(sig_pcc2data_calc_error),
        .sig_pcc_taking_command(sig_pcc_taking_command),
        .sig_push_cmd_reg(sig_push_cmd_reg),
        .sig_push_status1_out(sig_push_status1_out),
        .sig_rd_addr_valid_reg0(\I_ADDR_CNTL/sig_rd_addr_valid_reg0 ),
        .sig_rd_discontinue(sig_rd_discontinue),
        .sig_rd_llink_enable(sig_rd_llink_enable),
        .sig_rsc2stat_status(sig_rsc2stat_status),
        .sig_rsc2stat_status_valid(sig_rsc2stat_status_valid),
        .sig_status_reg_empty(sig_status_reg_empty));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axi_master_burst_reset I_RESET_MODULE
       (.\INFERRED_GEN.cnt_i_reg[0] (sig_rst2rdwr_cntlr_reset),
        .m_axi_aclk(m_axi_aclk),
        .m_axi_aresetn(m_axi_aresetn),
        .out(sig_rst2cmd_stat_reset),
        .sig_rd_error_reg_reg(sig_rst2llink_reset));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axi_master_burst_addr_cntl
   (out,
    sig_addr2stat_cmd_fifo_empty,
    m_axi_arsize,
    m_axi_arburst,
    m_axi_arvalid,
    m_axi_araddr,
    m_axi_arlen,
    sig_push_addr_reg1_out,
    m_axi_aclk,
    sig_pcc2data_calc_error,
    sig_pcc2addr_burst,
    sig_rd_addr_valid_reg0,
    sig_rdwr_reset_reg_reg,
    sig_cmd2all_doing_read,
    m_axi_arready,
    Q,
    \sig_xfer_len_reg_reg[7] );
  output out;
  output sig_addr2stat_cmd_fifo_empty;
  output [0:0]m_axi_arsize;
  output [0:0]m_axi_arburst;
  output m_axi_arvalid;
  output [31:0]m_axi_araddr;
  output [7:0]m_axi_arlen;
  input sig_push_addr_reg1_out;
  input m_axi_aclk;
  input sig_pcc2data_calc_error;
  input [0:0]sig_pcc2addr_burst;
  input sig_rd_addr_valid_reg0;
  input sig_rdwr_reset_reg_reg;
  input sig_cmd2all_doing_read;
  input m_axi_arready;
  input [31:0]Q;
  input [7:0]\sig_xfer_len_reg_reg[7] ;

  wire [31:0]Q;
  wire m_axi_aclk;
  wire [31:0]m_axi_araddr;
  wire [0:0]m_axi_arburst;
  wire [7:0]m_axi_arlen;
  wire m_axi_arready;
  wire [0:0]m_axi_arsize;
  wire m_axi_arvalid;
  wire sig_addr2stat_calc_error;
  wire sig_addr2stat_cmd_fifo_empty;
  wire sig_addr_reg_full;
  wire sig_cmd2all_doing_read;
  wire sig_next_addr_reg0;
  wire [0:0]sig_pcc2addr_burst;
  wire sig_pcc2data_calc_error;
  (* RTL_KEEP = "true" *) (* equivalent_register_removal = "no" *) wire sig_posted_to_axi;
  (* RTL_KEEP = "true" *) (* equivalent_register_removal = "no" *) wire sig_posted_to_axi_2;
  wire sig_push_addr_reg1_out;
  wire sig_rd_addr_valid_reg0;
  wire sig_rdwr_reset_reg_reg;
  wire [7:0]\sig_xfer_len_reg_reg[7] ;

  assign out = sig_posted_to_axi;
  FDSE #(
    .INIT(1'b0)) 
    sig_addr_reg_empty_reg
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(1'b0),
        .Q(sig_addr2stat_cmd_fifo_empty),
        .S(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    sig_addr_reg_full_reg
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(sig_push_addr_reg1_out),
        .Q(sig_addr_reg_full),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    sig_calc_error_reg_reg
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(sig_pcc2data_calc_error),
        .Q(sig_addr2stat_calc_error),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_addr_reg_reg[0] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(Q[0]),
        .Q(m_axi_araddr[0]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_addr_reg_reg[10] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(Q[10]),
        .Q(m_axi_araddr[10]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_addr_reg_reg[11] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(Q[11]),
        .Q(m_axi_araddr[11]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_addr_reg_reg[12] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(Q[12]),
        .Q(m_axi_araddr[12]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_addr_reg_reg[13] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(Q[13]),
        .Q(m_axi_araddr[13]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_addr_reg_reg[14] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(Q[14]),
        .Q(m_axi_araddr[14]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_addr_reg_reg[15] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(Q[15]),
        .Q(m_axi_araddr[15]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_addr_reg_reg[16] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(Q[16]),
        .Q(m_axi_araddr[16]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_addr_reg_reg[17] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(Q[17]),
        .Q(m_axi_araddr[17]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_addr_reg_reg[18] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(Q[18]),
        .Q(m_axi_araddr[18]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_addr_reg_reg[19] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(Q[19]),
        .Q(m_axi_araddr[19]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_addr_reg_reg[1] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(Q[1]),
        .Q(m_axi_araddr[1]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_addr_reg_reg[20] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(Q[20]),
        .Q(m_axi_araddr[20]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_addr_reg_reg[21] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(Q[21]),
        .Q(m_axi_araddr[21]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_addr_reg_reg[22] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(Q[22]),
        .Q(m_axi_araddr[22]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_addr_reg_reg[23] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(Q[23]),
        .Q(m_axi_araddr[23]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_addr_reg_reg[24] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(Q[24]),
        .Q(m_axi_araddr[24]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_addr_reg_reg[25] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(Q[25]),
        .Q(m_axi_araddr[25]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_addr_reg_reg[26] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(Q[26]),
        .Q(m_axi_araddr[26]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_addr_reg_reg[27] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(Q[27]),
        .Q(m_axi_araddr[27]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_addr_reg_reg[28] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(Q[28]),
        .Q(m_axi_araddr[28]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_addr_reg_reg[29] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(Q[29]),
        .Q(m_axi_araddr[29]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_addr_reg_reg[2] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(Q[2]),
        .Q(m_axi_araddr[2]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_addr_reg_reg[30] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(Q[30]),
        .Q(m_axi_araddr[30]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_addr_reg_reg[31] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(Q[31]),
        .Q(m_axi_araddr[31]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_addr_reg_reg[3] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(Q[3]),
        .Q(m_axi_araddr[3]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_addr_reg_reg[4] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(Q[4]),
        .Q(m_axi_araddr[4]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_addr_reg_reg[5] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(Q[5]),
        .Q(m_axi_araddr[5]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_addr_reg_reg[6] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(Q[6]),
        .Q(m_axi_araddr[6]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_addr_reg_reg[7] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(Q[7]),
        .Q(m_axi_araddr[7]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_addr_reg_reg[8] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(Q[8]),
        .Q(m_axi_araddr[8]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_addr_reg_reg[9] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(Q[9]),
        .Q(m_axi_araddr[9]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_burst_reg_reg[0] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(sig_pcc2addr_burst),
        .Q(m_axi_arburst),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_len_reg_reg[0] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(\sig_xfer_len_reg_reg[7] [0]),
        .Q(m_axi_arlen[0]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_len_reg_reg[1] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(\sig_xfer_len_reg_reg[7] [1]),
        .Q(m_axi_arlen[1]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_len_reg_reg[2] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(\sig_xfer_len_reg_reg[7] [2]),
        .Q(m_axi_arlen[2]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_len_reg_reg[3] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(\sig_xfer_len_reg_reg[7] [3]),
        .Q(m_axi_arlen[3]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_len_reg_reg[4] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(\sig_xfer_len_reg_reg[7] [4]),
        .Q(m_axi_arlen[4]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_len_reg_reg[5] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(\sig_xfer_len_reg_reg[7] [5]),
        .Q(m_axi_arlen[5]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_len_reg_reg[6] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(\sig_xfer_len_reg_reg[7] [6]),
        .Q(m_axi_arlen[6]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_len_reg_reg[7] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(\sig_xfer_len_reg_reg[7] [7]),
        .Q(m_axi_arlen[7]),
        .R(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_next_size_reg_reg[1] 
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(1'b1),
        .Q(m_axi_arsize),
        .R(sig_next_addr_reg0));
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    sig_posted_to_axi_2_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_push_addr_reg1_out),
        .Q(sig_posted_to_axi_2),
        .R(sig_rdwr_reset_reg_reg));
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    sig_posted_to_axi_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_push_addr_reg1_out),
        .Q(sig_posted_to_axi),
        .R(sig_rdwr_reset_reg_reg));
  LUT5 #(
    .INIT(32'hBAAAAAAA)) 
    sig_rd_addr_valid_reg_i_1
       (.I0(sig_rdwr_reset_reg_reg),
        .I1(sig_addr2stat_calc_error),
        .I2(sig_cmd2all_doing_read),
        .I3(m_axi_arready),
        .I4(sig_addr_reg_full),
        .O(sig_next_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    sig_rd_addr_valid_reg_reg
       (.C(m_axi_aclk),
        .CE(sig_push_addr_reg1_out),
        .D(sig_rd_addr_valid_reg0),
        .Q(m_axi_arvalid),
        .R(sig_next_addr_reg0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axi_master_burst_cmd_status
   (sig_cmd2pcc_cmd_valid,
    sig_cmd_mstrd_req,
    sig_cmd_empty_reg,
    sig_cmd2all_doing_read,
    sig_cmd_cmplt_reg_reg_0,
    sig_status_reg_empty,
    sig_rdwr2llink_int_err,
    bus2ip_mst_cmdack,
    D,
    Q,
    \sig_btt_cntr_reg[14] ,
    sig_push_status1_out,
    sig_rd_addr_valid_reg0,
    sig_rd_llink_enable,
    sig_calc_error_reg_reg,
    out,
    m_axi_aclk,
    SR,
    sig_push_cmd_reg,
    ip2bus_mstrd_req,
    sig_calc_error_reg0,
    \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[1] ,
    sig_btt_cntr0,
    sig_rsc2stat_status_valid,
    sig_pcc2data_calc_error,
    sig_doing_read_reg,
    sig_pcc2all_calc_err,
    sig_pcc_taking_command,
    sig_rsc2stat_status,
    sig_llink2cmd_rd_busy,
    \ip2bus_mst_length_Reg_reg[13] ,
    \ip2bus_mst_addr_reg[20] );
  output sig_cmd2pcc_cmd_valid;
  output sig_cmd_mstrd_req;
  output sig_cmd_empty_reg;
  output sig_cmd2all_doing_read;
  output sig_cmd_cmplt_reg_reg_0;
  output sig_status_reg_empty;
  output sig_rdwr2llink_int_err;
  output bus2ip_mst_cmdack;
  output [1:0]D;
  output [18:0]Q;
  output [4:0]\sig_btt_cntr_reg[14] ;
  output sig_push_status1_out;
  output sig_rd_addr_valid_reg0;
  output sig_rd_llink_enable;
  output sig_calc_error_reg_reg;
  input out;
  input m_axi_aclk;
  input [0:0]SR;
  input sig_push_cmd_reg;
  input ip2bus_mstrd_req;
  input sig_calc_error_reg0;
  input [1:0]\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[1] ;
  input [4:0]sig_btt_cntr0;
  input sig_rsc2stat_status_valid;
  input sig_pcc2data_calc_error;
  input sig_doing_read_reg;
  input sig_pcc2all_calc_err;
  input sig_pcc_taking_command;
  input [0:0]sig_rsc2stat_status;
  input sig_llink2cmd_rd_busy;
  input [1:0]\ip2bus_mst_length_Reg_reg[13] ;
  input [20:0]\ip2bus_mst_addr_reg[20] ;

  wire [1:0]D;
  wire [1:0]\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[1] ;
  wire [18:0]Q;
  wire [0:0]SR;
  wire bus2ip_mst_cmdack;
  wire [20:0]\ip2bus_mst_addr_reg[20] ;
  wire [1:0]\ip2bus_mst_length_Reg_reg[13] ;
  wire ip2bus_mstrd_req;
  wire m_axi_aclk;
  wire out;
  wire [4:0]sig_btt_cntr0;
  wire [4:0]\sig_btt_cntr_reg[14] ;
  wire sig_calc_error_reg0;
  wire sig_calc_error_reg_reg;
  wire sig_cmd2all_doing_read;
  wire sig_cmd2pcc_cmd_valid;
  wire sig_cmd_cmplt_reg_i_1_n_0;
  wire sig_cmd_cmplt_reg_reg_0;
  wire sig_cmd_empty_reg;
  wire sig_cmd_empty_reg_i_1_n_0;
  wire [1:0]sig_cmd_mst_addr;
  wire [0:0]sig_cmd_mst_be;
  wire [13:8]sig_cmd_mst_length;
  wire sig_cmd_mstrd_req;
  wire sig_cmdack_reg_i_1_n_0;
  wire sig_doing_read_reg;
  wire sig_doing_read_reg_i_1_n_0;
  wire sig_init_reg1;
  wire sig_init_reg2;
  wire sig_int_error_pulse_reg_i_1_n_0;
  wire sig_llink2cmd_rd_busy;
  wire sig_pcc2all_calc_err;
  wire sig_pcc2data_calc_error;
  wire sig_pcc_taking_command;
  wire sig_push_cmd_reg;
  wire sig_push_status1_out;
  wire sig_rd_addr_valid_reg0;
  wire sig_rd_llink_enable;
  wire sig_rdwr2llink_int_err;
  wire [0:0]sig_rsc2stat_status;
  wire sig_rsc2stat_status_valid;
  wire sig_status_reg_empty;
  wire sig_status_reg_empty_i_1_n_0;
  wire sig_status_reg_full;
  wire sig_status_reg_full_i_1_n_0;

  LUT4 #(
    .INIT(16'h8F80)) 
    \GEN_ADDR_32.sig_addr_cntr_lsh[0]_i_1 
       (.I0(sig_cmd_mst_be),
        .I1(sig_cmd_mst_addr[0]),
        .I2(sig_calc_error_reg0),
        .I3(\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[1] [0]),
        .O(D[0]));
  LUT4 #(
    .INIT(16'h8F80)) 
    \GEN_ADDR_32.sig_addr_cntr_lsh[1]_i_1 
       (.I0(sig_cmd_mst_be),
        .I1(sig_cmd_mst_addr[1]),
        .I2(sig_calc_error_reg0),
        .I3(\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[1] [1]),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h8F80)) 
    \sig_btt_cntr[11]_i_1 
       (.I0(sig_cmd_mst_length[8]),
        .I1(sig_cmd_mst_be),
        .I2(sig_calc_error_reg0),
        .I3(sig_btt_cntr0[2]),
        .O(\sig_btt_cntr_reg[14] [2]));
  LUT4 #(
    .INIT(16'h8F80)) 
    \sig_btt_cntr[13]_i_1 
       (.I0(sig_cmd_mst_be),
        .I1(sig_cmd_mst_length[13]),
        .I2(sig_calc_error_reg0),
        .I3(sig_btt_cntr0[3]),
        .O(\sig_btt_cntr_reg[14] [3]));
  LUT4 #(
    .INIT(16'h8F80)) 
    \sig_btt_cntr[14]_i_1 
       (.I0(sig_cmd_mstrd_req),
        .I1(sig_cmd_mst_be),
        .I2(sig_calc_error_reg0),
        .I3(sig_btt_cntr0[4]),
        .O(\sig_btt_cntr_reg[14] [4]));
  LUT4 #(
    .INIT(16'h8F80)) 
    \sig_btt_cntr[8]_i_1 
       (.I0(sig_cmd_mst_length[8]),
        .I1(sig_cmd_mst_be),
        .I2(sig_calc_error_reg0),
        .I3(sig_btt_cntr0[0]),
        .O(\sig_btt_cntr_reg[14] [0]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h8F80)) 
    \sig_btt_cntr[9]_i_1 
       (.I0(sig_cmd_mst_length[8]),
        .I1(sig_cmd_mst_be),
        .I2(sig_calc_error_reg0),
        .I3(sig_btt_cntr0[1]),
        .O(\sig_btt_cntr_reg[14] [1]));
  LUT6 #(
    .INIT(64'h0F1FFFFF0F1F0000)) 
    sig_calc_error_reg_i_1
       (.I0(sig_cmd_mst_length[13]),
        .I1(sig_cmd_mstrd_req),
        .I2(sig_cmd_mst_be),
        .I3(sig_cmd_mst_length[8]),
        .I4(sig_calc_error_reg0),
        .I5(sig_pcc2all_calc_err),
        .O(sig_calc_error_reg_reg));
  LUT5 #(
    .INIT(32'h00000070)) 
    sig_cmd_cmplt_reg_i_1
       (.I0(sig_llink2cmd_rd_busy),
        .I1(sig_cmd2all_doing_read),
        .I2(sig_status_reg_full),
        .I3(out),
        .I4(sig_cmd_cmplt_reg_reg_0),
        .O(sig_cmd_cmplt_reg_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sig_cmd_cmplt_reg_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_cmd_cmplt_reg_i_1_n_0),
        .Q(sig_cmd_cmplt_reg_reg_0),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFFFF22F2)) 
    sig_cmd_empty_reg_i_1
       (.I0(sig_cmd_empty_reg),
        .I1(ip2bus_mstrd_req),
        .I2(sig_init_reg1),
        .I3(sig_init_reg2),
        .I4(sig_cmd_cmplt_reg_reg_0),
        .O(sig_cmd_empty_reg_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sig_cmd_empty_reg_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_cmd_empty_reg_i_1_n_0),
        .Q(sig_cmd_empty_reg),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    sig_cmd_full_reg_reg
       (.C(m_axi_aclk),
        .CE(sig_push_cmd_reg),
        .D(sig_push_cmd_reg),
        .Q(sig_cmd2pcc_cmd_valid),
        .R(SR));
  FDRE \sig_cmd_mst_addr_reg[0] 
       (.C(m_axi_aclk),
        .CE(sig_push_cmd_reg),
        .D(\ip2bus_mst_addr_reg[20] [0]),
        .Q(sig_cmd_mst_addr[0]),
        .R(SR));
  FDRE \sig_cmd_mst_addr_reg[10] 
       (.C(m_axi_aclk),
        .CE(sig_push_cmd_reg),
        .D(\ip2bus_mst_addr_reg[20] [10]),
        .Q(Q[8]),
        .R(SR));
  FDRE \sig_cmd_mst_addr_reg[11] 
       (.C(m_axi_aclk),
        .CE(sig_push_cmd_reg),
        .D(\ip2bus_mst_addr_reg[20] [11]),
        .Q(Q[9]),
        .R(SR));
  FDRE \sig_cmd_mst_addr_reg[12] 
       (.C(m_axi_aclk),
        .CE(sig_push_cmd_reg),
        .D(\ip2bus_mst_addr_reg[20] [12]),
        .Q(Q[10]),
        .R(SR));
  FDRE \sig_cmd_mst_addr_reg[13] 
       (.C(m_axi_aclk),
        .CE(sig_push_cmd_reg),
        .D(\ip2bus_mst_addr_reg[20] [13]),
        .Q(Q[11]),
        .R(SR));
  FDRE \sig_cmd_mst_addr_reg[14] 
       (.C(m_axi_aclk),
        .CE(sig_push_cmd_reg),
        .D(\ip2bus_mst_addr_reg[20] [14]),
        .Q(Q[12]),
        .R(SR));
  FDRE \sig_cmd_mst_addr_reg[15] 
       (.C(m_axi_aclk),
        .CE(sig_push_cmd_reg),
        .D(\ip2bus_mst_addr_reg[20] [15]),
        .Q(Q[13]),
        .R(SR));
  FDRE \sig_cmd_mst_addr_reg[16] 
       (.C(m_axi_aclk),
        .CE(sig_push_cmd_reg),
        .D(\ip2bus_mst_addr_reg[20] [16]),
        .Q(Q[14]),
        .R(SR));
  FDRE \sig_cmd_mst_addr_reg[17] 
       (.C(m_axi_aclk),
        .CE(sig_push_cmd_reg),
        .D(\ip2bus_mst_addr_reg[20] [17]),
        .Q(Q[15]),
        .R(SR));
  FDRE \sig_cmd_mst_addr_reg[18] 
       (.C(m_axi_aclk),
        .CE(sig_push_cmd_reg),
        .D(\ip2bus_mst_addr_reg[20] [18]),
        .Q(Q[16]),
        .R(SR));
  FDRE \sig_cmd_mst_addr_reg[19] 
       (.C(m_axi_aclk),
        .CE(sig_push_cmd_reg),
        .D(\ip2bus_mst_addr_reg[20] [19]),
        .Q(Q[17]),
        .R(SR));
  FDRE \sig_cmd_mst_addr_reg[1] 
       (.C(m_axi_aclk),
        .CE(sig_push_cmd_reg),
        .D(\ip2bus_mst_addr_reg[20] [1]),
        .Q(sig_cmd_mst_addr[1]),
        .R(SR));
  FDRE \sig_cmd_mst_addr_reg[20] 
       (.C(m_axi_aclk),
        .CE(sig_push_cmd_reg),
        .D(\ip2bus_mst_addr_reg[20] [20]),
        .Q(Q[18]),
        .R(SR));
  FDRE \sig_cmd_mst_addr_reg[2] 
       (.C(m_axi_aclk),
        .CE(sig_push_cmd_reg),
        .D(\ip2bus_mst_addr_reg[20] [2]),
        .Q(Q[0]),
        .R(SR));
  FDRE \sig_cmd_mst_addr_reg[3] 
       (.C(m_axi_aclk),
        .CE(sig_push_cmd_reg),
        .D(\ip2bus_mst_addr_reg[20] [3]),
        .Q(Q[1]),
        .R(SR));
  FDRE \sig_cmd_mst_addr_reg[4] 
       (.C(m_axi_aclk),
        .CE(sig_push_cmd_reg),
        .D(\ip2bus_mst_addr_reg[20] [4]),
        .Q(Q[2]),
        .R(SR));
  FDRE \sig_cmd_mst_addr_reg[5] 
       (.C(m_axi_aclk),
        .CE(sig_push_cmd_reg),
        .D(\ip2bus_mst_addr_reg[20] [5]),
        .Q(Q[3]),
        .R(SR));
  FDRE \sig_cmd_mst_addr_reg[6] 
       (.C(m_axi_aclk),
        .CE(sig_push_cmd_reg),
        .D(\ip2bus_mst_addr_reg[20] [6]),
        .Q(Q[4]),
        .R(SR));
  FDRE \sig_cmd_mst_addr_reg[7] 
       (.C(m_axi_aclk),
        .CE(sig_push_cmd_reg),
        .D(\ip2bus_mst_addr_reg[20] [7]),
        .Q(Q[5]),
        .R(SR));
  FDRE \sig_cmd_mst_addr_reg[8] 
       (.C(m_axi_aclk),
        .CE(sig_push_cmd_reg),
        .D(\ip2bus_mst_addr_reg[20] [8]),
        .Q(Q[6]),
        .R(SR));
  FDRE \sig_cmd_mst_addr_reg[9] 
       (.C(m_axi_aclk),
        .CE(sig_push_cmd_reg),
        .D(\ip2bus_mst_addr_reg[20] [9]),
        .Q(Q[7]),
        .R(SR));
  FDRE \sig_cmd_mst_be_reg[0] 
       (.C(m_axi_aclk),
        .CE(sig_push_cmd_reg),
        .D(1'b1),
        .Q(sig_cmd_mst_be),
        .R(SR));
  FDRE \sig_cmd_mst_length_reg[13] 
       (.C(m_axi_aclk),
        .CE(sig_push_cmd_reg),
        .D(\ip2bus_mst_length_Reg_reg[13] [1]),
        .Q(sig_cmd_mst_length[13]),
        .R(SR));
  FDRE \sig_cmd_mst_length_reg[8] 
       (.C(m_axi_aclk),
        .CE(sig_push_cmd_reg),
        .D(\ip2bus_mst_length_Reg_reg[13] [0]),
        .Q(sig_cmd_mst_length[8]),
        .R(SR));
  FDRE sig_cmd_mstrd_req_reg
       (.C(m_axi_aclk),
        .CE(sig_push_cmd_reg),
        .D(ip2bus_mstrd_req),
        .Q(sig_cmd_mstrd_req),
        .R(SR));
  LUT4 #(
    .INIT(16'h0008)) 
    sig_cmdack_reg_i_1
       (.I0(sig_cmd_empty_reg),
        .I1(ip2bus_mstrd_req),
        .I2(bus2ip_mst_cmdack),
        .I3(out),
        .O(sig_cmdack_reg_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sig_cmdack_reg_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_cmdack_reg_i_1_n_0),
        .Q(bus2ip_mst_cmdack),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h000000E2)) 
    sig_doing_read_reg_i_1
       (.I0(sig_cmd2all_doing_read),
        .I1(sig_pcc_taking_command),
        .I2(sig_cmd_mstrd_req),
        .I3(out),
        .I4(sig_cmd_cmplt_reg_reg_0),
        .O(sig_doing_read_reg_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sig_doing_read_reg_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_doing_read_reg_i_1_n_0),
        .Q(sig_cmd2all_doing_read),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    sig_init_reg1_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(1'b1),
        .Q(sig_init_reg1),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    sig_init_reg2_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_init_reg1),
        .Q(sig_init_reg2),
        .R(out));
  LUT6 #(
    .INIT(64'h00000000E2000000)) 
    sig_int_error_pulse_reg_i_1
       (.I0(sig_rdwr2llink_int_err),
        .I1(sig_push_status1_out),
        .I2(sig_rsc2stat_status),
        .I3(sig_llink2cmd_rd_busy),
        .I4(sig_cmd2all_doing_read),
        .I5(out),
        .O(sig_int_error_pulse_reg_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sig_int_error_pulse_reg_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_int_error_pulse_reg_i_1_n_0),
        .Q(sig_rdwr2llink_int_err),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h2)) 
    sig_llink_busy_i_2
       (.I0(sig_cmd2all_doing_read),
        .I1(sig_doing_read_reg),
        .O(sig_rd_llink_enable));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h2)) 
    sig_rd_addr_valid_reg_i_3
       (.I0(sig_cmd2all_doing_read),
        .I1(sig_pcc2data_calc_error),
        .O(sig_rd_addr_valid_reg0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h80)) 
    sig_rd_sts_reg_full_i_2
       (.I0(sig_rsc2stat_status_valid),
        .I1(sig_status_reg_empty),
        .I2(sig_cmd2all_doing_read),
        .O(sig_push_status1_out));
  LUT6 #(
    .INIT(64'hF2FFF2F2FFFFF2F2)) 
    sig_status_reg_empty_i_1
       (.I0(sig_init_reg1),
        .I1(sig_init_reg2),
        .I2(sig_cmd_cmplt_reg_reg_0),
        .I3(sig_cmd2all_doing_read),
        .I4(sig_status_reg_empty),
        .I5(sig_rsc2stat_status_valid),
        .O(sig_status_reg_empty_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sig_status_reg_empty_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_status_reg_empty_i_1_n_0),
        .Q(sig_status_reg_empty),
        .R(out));
  LUT6 #(
    .INIT(64'h00000000F2222222)) 
    sig_status_reg_full_i_1
       (.I0(sig_status_reg_full),
        .I1(sig_cmd_cmplt_reg_reg_0),
        .I2(sig_cmd2all_doing_read),
        .I3(sig_status_reg_empty),
        .I4(sig_rsc2stat_status_valid),
        .I5(out),
        .O(sig_status_reg_full_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sig_status_reg_full_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_status_reg_full_i_1_n_0),
        .Q(sig_status_reg_full),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axi_master_burst_fifo
   (sig_init_reg2_reg_0,
    sig_s_ready_out_reg,
    m_axi_bready,
    out,
    m_axi_aclk,
    m_axi_bvalid,
    sig_s_ready_dup_reg);
  output sig_init_reg2_reg_0;
  output sig_s_ready_out_reg;
  output m_axi_bready;
  input out;
  input m_axi_aclk;
  input m_axi_bvalid;
  input sig_s_ready_dup_reg;

  wire \GEN_OMIT_STORE_FORWARD.I_DATA_CNTL_STATUS_FIFO/sig_init_reg2 ;
  wire m_axi_aclk;
  wire m_axi_bready;
  wire m_axi_bvalid;
  wire out;
  wire sig_inhibit_rdy_n;
  wire sig_inhibit_rdy_n_i_1_n_0;
  wire sig_init_done;
  wire sig_init_done_i_1_n_0;
  wire sig_init_reg2_reg_0;
  wire sig_s_ready_dup_reg;
  wire sig_s_ready_out_reg;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_srl_fifo_f \USE_SRL_FIFO.I_SYNC_FIFO 
       (.m_axi_aclk(m_axi_aclk),
        .m_axi_bready(m_axi_bready),
        .m_axi_bvalid(m_axi_bvalid),
        .out(out),
        .sig_inhibit_rdy_n(sig_inhibit_rdy_n));
  LUT2 #(
    .INIT(4'hE)) 
    sig_inhibit_rdy_n_i_1
       (.I0(sig_init_done),
        .I1(sig_inhibit_rdy_n),
        .O(sig_inhibit_rdy_n_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sig_inhibit_rdy_n_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_inhibit_rdy_n_i_1_n_0),
        .Q(sig_inhibit_rdy_n),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT4 #(
    .INIT(16'h0008)) 
    sig_init_done_i_1
       (.I0(sig_init_reg2_reg_0),
        .I1(\GEN_OMIT_STORE_FORWARD.I_DATA_CNTL_STATUS_FIFO/sig_init_reg2 ),
        .I2(sig_init_done),
        .I3(out),
        .O(sig_init_done_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sig_init_done_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_init_done_i_1_n_0),
        .Q(sig_init_done),
        .R(1'b0));
  FDSE #(
    .INIT(1'b0)) 
    sig_init_reg2_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_init_reg2_reg_0),
        .Q(\GEN_OMIT_STORE_FORWARD.I_DATA_CNTL_STATUS_FIFO/sig_init_reg2 ),
        .S(out));
  FDRE #(
    .INIT(1'b0)) 
    sig_init_reg_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(out),
        .Q(sig_init_reg2_reg_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT2 #(
    .INIT(4'hE)) 
    sig_s_ready_dup_i_1__1
       (.I0(sig_init_reg2_reg_0),
        .I1(sig_s_ready_dup_reg),
        .O(sig_s_ready_out_reg));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axi_master_burst_pcc
   (sig_pcc2addr_burst,
    sig_pcc2data_cmd_cmplt,
    sig_pcc2data_calc_error,
    sig_xfer_calc_err_reg_reg_0,
    sig_pcc2data_eof,
    sig_pcc2data_sequential,
    \sig_btt_cntr_reg[14]_0 ,
    sig_pcc2data_cmd_valid,
    S,
    SR,
    \GEN_ADDR_32.sig_addr_cntr_msh_reg[0]_0 ,
    DI,
    \GEN_ADDR_32.sig_addr_cntr_msh_reg[7]_0 ,
    \GEN_ADDR_32.sig_addr_cntr_msh_reg[15]_0 ,
    \GEN_ADDR_32.sig_addr_cntr_msh_reg[11]_0 ,
    sig_pcc_taking_command,
    sig_push_addr_reg1_out,
    sig_last_dbeat_reg,
    \sig_next_len_reg_reg[7] ,
    \GEN_ADDR_32.sig_addr_cntr_lsh_reg[1]_0 ,
    \sig_next_addr_reg_reg[31] ,
    out,
    m_axi_aclk,
    O,
    \GEN_ADDR_32.sig_addr_cntr_msh_reg[7]_1 ,
    \GEN_ADDR_32.sig_addr_cntr_msh_reg[11]_1 ,
    \GEN_ADDR_32.sig_addr_cntr_msh_reg[15]_1 ,
    \sig_cmd_mst_length_reg[13] ,
    Q,
    sig_cmd2pcc_cmd_valid,
    sig_cmd_reset_reg_reg,
    sig_push_cmd_reg,
    sig_cmd_mstrd_req,
    sig_rdc2pcc_cmd_ready,
    sig_cmd2all_doing_read,
    sig_addr2stat_cmd_fifo_empty,
    sig_llink2rd_allow_addr_req,
    D,
    sig_cmd_mstrd_req_reg);
  output [0:0]sig_pcc2addr_burst;
  output sig_pcc2data_cmd_cmplt;
  output sig_pcc2data_calc_error;
  output sig_xfer_calc_err_reg_reg_0;
  output sig_pcc2data_eof;
  output sig_pcc2data_sequential;
  output [4:0]\sig_btt_cntr_reg[14]_0 ;
  output sig_pcc2data_cmd_valid;
  output [3:0]S;
  output [0:0]SR;
  output \GEN_ADDR_32.sig_addr_cntr_msh_reg[0]_0 ;
  output [0:0]DI;
  output [3:0]\GEN_ADDR_32.sig_addr_cntr_msh_reg[7]_0 ;
  output [3:0]\GEN_ADDR_32.sig_addr_cntr_msh_reg[15]_0 ;
  output [3:0]\GEN_ADDR_32.sig_addr_cntr_msh_reg[11]_0 ;
  output sig_pcc_taking_command;
  output sig_push_addr_reg1_out;
  output sig_last_dbeat_reg;
  output [7:0]\sig_next_len_reg_reg[7] ;
  output [1:0]\GEN_ADDR_32.sig_addr_cntr_lsh_reg[1]_0 ;
  output [31:0]\sig_next_addr_reg_reg[31] ;
  input out;
  input m_axi_aclk;
  input [3:0]O;
  input [3:0]\GEN_ADDR_32.sig_addr_cntr_msh_reg[7]_1 ;
  input [3:0]\GEN_ADDR_32.sig_addr_cntr_msh_reg[11]_1 ;
  input [3:0]\GEN_ADDR_32.sig_addr_cntr_msh_reg[15]_1 ;
  input \sig_cmd_mst_length_reg[13] ;
  input [18:0]Q;
  input sig_cmd2pcc_cmd_valid;
  input sig_cmd_reset_reg_reg;
  input sig_push_cmd_reg;
  input sig_cmd_mstrd_req;
  input sig_rdc2pcc_cmd_ready;
  input sig_cmd2all_doing_read;
  input sig_addr2stat_cmd_fifo_empty;
  input sig_llink2rd_allow_addr_req;
  input [1:0]D;
  input [4:0]sig_cmd_mstrd_req_reg;

  wire [1:0]D;
  wire [0:0]DI;
  wire \FSM_sequential_sig_pcc_sm_state[0]_i_1_n_0 ;
  wire \FSM_sequential_sig_pcc_sm_state[0]_i_2_n_0 ;
  wire \FSM_sequential_sig_pcc_sm_state[1]_i_1_n_0 ;
  wire \FSM_sequential_sig_pcc_sm_state[2]_i_1_n_0 ;
  wire \GEN_ADDR_32.sig_addr_cntr_incr_imreg[0]_i_1_n_0 ;
  wire \GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_1_n_0 ;
  wire \GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_2_n_0 ;
  wire \GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_3_n_0 ;
  wire \GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_4_n_0 ;
  wire \GEN_ADDR_32.sig_addr_cntr_incr_imreg[1]_i_1_n_0 ;
  wire \GEN_ADDR_32.sig_addr_cntr_incr_imreg[2]_i_1_n_0 ;
  wire \GEN_ADDR_32.sig_addr_cntr_incr_imreg[3]_i_1_n_0 ;
  wire \GEN_ADDR_32.sig_addr_cntr_incr_imreg[3]_i_2_n_0 ;
  wire \GEN_ADDR_32.sig_addr_cntr_incr_imreg[4]_i_1_n_0 ;
  wire \GEN_ADDR_32.sig_addr_cntr_incr_imreg[4]_i_2_n_0 ;
  wire \GEN_ADDR_32.sig_addr_cntr_incr_imreg[5]_i_1_n_0 ;
  wire \GEN_ADDR_32.sig_addr_cntr_incr_imreg[5]_i_2_n_0 ;
  wire \GEN_ADDR_32.sig_addr_cntr_incr_imreg[6]_i_1_n_0 ;
  wire \GEN_ADDR_32.sig_addr_cntr_incr_imreg[7]_i_1_n_0 ;
  wire \GEN_ADDR_32.sig_addr_cntr_incr_imreg[7]_i_2_n_0 ;
  wire \GEN_ADDR_32.sig_addr_cntr_incr_imreg[8]_i_1_n_0 ;
  wire \GEN_ADDR_32.sig_addr_cntr_incr_imreg[8]_i_2_n_0 ;
  wire \GEN_ADDR_32.sig_addr_cntr_incr_imreg[9]_i_1_n_0 ;
  wire \GEN_ADDR_32.sig_addr_cntr_incr_imreg[9]_i_2_n_0 ;
  wire \GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ;
  wire [1:0]\GEN_ADDR_32.sig_addr_cntr_lsh_reg[1]_0 ;
  wire \GEN_ADDR_32.sig_addr_cntr_msh[0]_i_1_n_0 ;
  wire [15:0]\GEN_ADDR_32.sig_addr_cntr_msh_reg ;
  wire \GEN_ADDR_32.sig_addr_cntr_msh_reg[0]_0 ;
  wire [3:0]\GEN_ADDR_32.sig_addr_cntr_msh_reg[11]_0 ;
  wire [3:0]\GEN_ADDR_32.sig_addr_cntr_msh_reg[11]_1 ;
  wire [3:0]\GEN_ADDR_32.sig_addr_cntr_msh_reg[15]_0 ;
  wire [3:0]\GEN_ADDR_32.sig_addr_cntr_msh_reg[15]_1 ;
  wire [3:0]\GEN_ADDR_32.sig_addr_cntr_msh_reg[7]_0 ;
  wire [3:0]\GEN_ADDR_32.sig_addr_cntr_msh_reg[7]_1 ;
  wire \GEN_ADDR_32.sig_adjusted_addr_incr_reg[3]_i_2_n_0 ;
  wire \GEN_ADDR_32.sig_adjusted_addr_incr_reg[3]_i_3_n_0 ;
  wire \GEN_ADDR_32.sig_adjusted_addr_incr_reg[3]_i_4_n_0 ;
  wire \GEN_ADDR_32.sig_adjusted_addr_incr_reg[3]_i_5_n_0 ;
  wire \GEN_ADDR_32.sig_adjusted_addr_incr_reg[7]_i_2_n_0 ;
  wire \GEN_ADDR_32.sig_adjusted_addr_incr_reg[7]_i_3_n_0 ;
  wire \GEN_ADDR_32.sig_adjusted_addr_incr_reg[7]_i_4_n_0 ;
  wire \GEN_ADDR_32.sig_adjusted_addr_incr_reg[7]_i_5_n_0 ;
  wire \GEN_ADDR_32.sig_adjusted_addr_incr_reg[9]_i_2_n_0 ;
  wire \GEN_ADDR_32.sig_adjusted_addr_incr_reg[9]_i_3_n_0 ;
  wire \GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[3]_i_1_n_0 ;
  wire \GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[3]_i_1_n_1 ;
  wire \GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[3]_i_1_n_2 ;
  wire \GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[3]_i_1_n_3 ;
  wire \GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[7]_i_1_n_0 ;
  wire \GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[7]_i_1_n_1 ;
  wire \GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[7]_i_1_n_2 ;
  wire \GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[7]_i_1_n_3 ;
  wire \GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[9]_i_1_n_3 ;
  wire \GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[0] ;
  wire \GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[1] ;
  wire \GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[2] ;
  wire \GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[3] ;
  wire \GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[4] ;
  wire \GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[5] ;
  wire \GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[6] ;
  wire \GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[7] ;
  wire \GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[8] ;
  wire \GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[9] ;
  wire \GEN_ADDR_32.sig_first_xfer_i_1_n_0 ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg[11]_i_2_n_0 ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg[11]_i_3_n_0 ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg[11]_i_4_n_0 ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg[11]_i_5_n_0 ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg[15]_i_2_n_0 ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg[15]_i_3_n_0 ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg[15]_i_4_n_0 ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg[15]_i_5_n_0 ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg[3]_i_2_n_0 ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg[3]_i_3_n_0 ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg[3]_i_4_n_0 ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg[3]_i_5_n_0 ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg[7]_i_2_n_0 ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg[7]_i_3_n_0 ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg[7]_i_4_n_0 ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg[7]_i_5_n_0 ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[11]_i_1_n_0 ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[11]_i_1_n_1 ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[11]_i_1_n_2 ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[11]_i_1_n_3 ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[15]_i_1_n_1 ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[15]_i_1_n_2 ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[15]_i_1_n_3 ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[3]_i_1_n_0 ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[3]_i_1_n_1 ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[3]_i_1_n_2 ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[3]_i_1_n_3 ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[7]_i_1_n_0 ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[7]_i_1_n_1 ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[7]_i_1_n_2 ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[7]_i_1_n_3 ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[10] ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[11] ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[12] ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[13] ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[14] ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[2] ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[3] ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[4] ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[5] ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[6] ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[7] ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[8] ;
  wire \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[9] ;
  wire [3:0]O;
  wire [18:0]Q;
  wire [3:0]S;
  wire [0:0]SR;
  wire m_axi_aclk;
  wire out;
  wire p_0_in;
  wire [15:2]p_1_in;
  wire p_1_in2_in;
  wire [9:0]sel0;
  wire sig_addr2stat_cmd_fifo_empty;
  wire [10:0]sig_addr_cntr_incr_imreg;
  wire [9:0]sig_adjusted_addr_incr;
  wire [19:0]sig_btt_cntr0;
  wire sig_btt_cntr0_carry__0_i_1_n_0;
  wire sig_btt_cntr0_carry__0_i_2_n_0;
  wire sig_btt_cntr0_carry__0_i_3_n_0;
  wire sig_btt_cntr0_carry__0_i_4_n_0;
  wire sig_btt_cntr0_carry__0_n_0;
  wire sig_btt_cntr0_carry__0_n_1;
  wire sig_btt_cntr0_carry__0_n_2;
  wire sig_btt_cntr0_carry__0_n_3;
  wire sig_btt_cntr0_carry__1_i_1_n_0;
  wire sig_btt_cntr0_carry__1_i_2_n_0;
  wire sig_btt_cntr0_carry__1_i_3_n_0;
  wire sig_btt_cntr0_carry__1_i_4_n_0;
  wire sig_btt_cntr0_carry__1_n_0;
  wire sig_btt_cntr0_carry__1_n_1;
  wire sig_btt_cntr0_carry__1_n_2;
  wire sig_btt_cntr0_carry__1_n_3;
  wire sig_btt_cntr0_carry__2_i_1_n_0;
  wire sig_btt_cntr0_carry__2_i_2_n_0;
  wire sig_btt_cntr0_carry__2_i_3_n_0;
  wire sig_btt_cntr0_carry__2_i_4_n_0;
  wire sig_btt_cntr0_carry__2_n_0;
  wire sig_btt_cntr0_carry__2_n_1;
  wire sig_btt_cntr0_carry__2_n_2;
  wire sig_btt_cntr0_carry__2_n_3;
  wire sig_btt_cntr0_carry__3_i_1_n_0;
  wire sig_btt_cntr0_carry__3_i_2_n_0;
  wire sig_btt_cntr0_carry__3_i_3_n_0;
  wire sig_btt_cntr0_carry__3_i_4_n_0;
  wire sig_btt_cntr0_carry__3_n_1;
  wire sig_btt_cntr0_carry__3_n_2;
  wire sig_btt_cntr0_carry__3_n_3;
  wire sig_btt_cntr0_carry_i_1_n_0;
  wire sig_btt_cntr0_carry_i_2_n_0;
  wire sig_btt_cntr0_carry_i_3_n_0;
  wire sig_btt_cntr0_carry_i_4_n_0;
  wire sig_btt_cntr0_carry_n_0;
  wire sig_btt_cntr0_carry_n_1;
  wire sig_btt_cntr0_carry_n_2;
  wire sig_btt_cntr0_carry_n_3;
  wire \sig_btt_cntr[0]_i_1_n_0 ;
  wire \sig_btt_cntr[10]_i_1_n_0 ;
  wire \sig_btt_cntr[12]_i_1_n_0 ;
  wire \sig_btt_cntr[15]_i_1_n_0 ;
  wire \sig_btt_cntr[16]_i_1_n_0 ;
  wire \sig_btt_cntr[17]_i_1_n_0 ;
  wire \sig_btt_cntr[18]_i_1_n_0 ;
  wire \sig_btt_cntr[19]_i_1_n_0 ;
  wire \sig_btt_cntr[1]_i_1_n_0 ;
  wire \sig_btt_cntr[2]_i_1_n_0 ;
  wire \sig_btt_cntr[3]_i_1_n_0 ;
  wire \sig_btt_cntr[4]_i_1_n_0 ;
  wire \sig_btt_cntr[5]_i_1_n_0 ;
  wire \sig_btt_cntr[6]_i_1_n_0 ;
  wire \sig_btt_cntr[7]_i_1_n_0 ;
  wire [4:0]\sig_btt_cntr_reg[14]_0 ;
  wire sig_btt_eq_b2mbaa1;
  wire sig_btt_eq_b2mbaa1_carry_i_1_n_0;
  wire sig_btt_eq_b2mbaa1_carry_i_2_n_0;
  wire sig_btt_eq_b2mbaa1_carry_i_3_n_0;
  wire sig_btt_eq_b2mbaa1_carry_i_4_n_0;
  wire sig_btt_eq_b2mbaa1_carry_i_5_n_0;
  wire sig_btt_eq_b2mbaa1_carry_i_6_n_0;
  wire sig_btt_eq_b2mbaa1_carry_n_1;
  wire sig_btt_eq_b2mbaa1_carry_n_2;
  wire sig_btt_eq_b2mbaa1_carry_n_3;
  wire sig_btt_lt_b2mbaa1;
  wire sig_btt_lt_b2mbaa1_carry__0_i_1_n_0;
  wire sig_btt_lt_b2mbaa1_carry__0_i_2_n_0;
  wire sig_btt_lt_b2mbaa1_carry__0_i_3_n_0;
  wire sig_btt_lt_b2mbaa1_carry__0_i_4_n_0;
  wire sig_btt_lt_b2mbaa1_carry__0_i_5_n_0;
  wire sig_btt_lt_b2mbaa1_carry__0_n_3;
  wire sig_btt_lt_b2mbaa1_carry_i_1_n_0;
  wire sig_btt_lt_b2mbaa1_carry_i_2_n_0;
  wire sig_btt_lt_b2mbaa1_carry_i_3_n_0;
  wire sig_btt_lt_b2mbaa1_carry_i_4_n_0;
  wire sig_btt_lt_b2mbaa1_carry_i_5_n_0;
  wire sig_btt_lt_b2mbaa1_carry_i_6_n_0;
  wire sig_btt_lt_b2mbaa1_carry_i_7_n_0;
  wire sig_btt_lt_b2mbaa1_carry_i_8_n_0;
  wire sig_btt_lt_b2mbaa1_carry_i_9_n_0;
  wire sig_btt_lt_b2mbaa1_carry_n_0;
  wire sig_btt_lt_b2mbaa1_carry_n_1;
  wire sig_btt_lt_b2mbaa1_carry_n_2;
  wire sig_btt_lt_b2mbaa1_carry_n_3;
  wire [9:0]sig_btt_residue_slice;
  wire \sig_byte_change_minus1/i__n_0 ;
  wire sig_calc_error_pushed;
  wire sig_calc_error_pushed_i_1_n_0;
  wire sig_cmd2addr_valid_i_1_n_0;
  wire sig_cmd2all_doing_read;
  wire sig_cmd2data_valid_i_1_n_0;
  wire sig_cmd2dre_valid_i_1_n_0;
  wire sig_cmd2dre_valid_reg_n_0;
  wire sig_cmd2pcc_cmd_valid;
  wire \sig_cmd_mst_length_reg[13] ;
  wire sig_cmd_mstrd_req;
  wire [4:0]sig_cmd_mstrd_req_reg;
  wire sig_cmd_reset_reg_reg;
  wire sig_first_xfer;
  wire sig_input_burst_type_reg;
  wire sig_input_burst_type_reg_i_1_n_0;
  wire sig_input_eof_reg;
  wire sig_input_eof_reg_i_1_n_0;
  wire sig_input_reg_empty;
  wire sig_input_reg_empty_i_1_n_0;
  wire sig_last_dbeat_i_7_n_0;
  wire sig_last_dbeat_reg;
  wire sig_ld_xfer_reg;
  wire sig_ld_xfer_reg_i_1_n_0;
  wire sig_llink2rd_allow_addr_req;
  wire [31:0]\sig_next_addr_reg_reg[31] ;
  wire [7:0]\sig_next_len_reg_reg[7] ;
  wire sig_parent_done;
  wire sig_parent_done_i_1_n_0;
  wire [0:0]sig_pcc2addr_burst;
  wire sig_pcc2addr_cmd_valid;
  wire sig_pcc2data_calc_error;
  wire sig_pcc2data_cmd_cmplt;
  wire sig_pcc2data_cmd_valid;
  wire sig_pcc2data_eof;
  wire sig_pcc2data_sequential;
  (* RTL_KEEP = "yes" *) wire [2:0]sig_pcc_sm_state;
  wire sig_pcc_taking_command;
  wire [15:0]sig_predict_addr_lsh_im3_in;
  wire sig_push_addr_reg1_out;
  wire sig_push_cmd_reg;
  wire sig_push_xfer_reg15_out;
  wire sig_rdc2pcc_cmd_ready;
  wire sig_sm_halt_ns;
  wire sig_sm_halt_reg;
  wire sig_sm_ld_calc1_reg;
  wire sig_sm_ld_calc1_reg_ns;
  wire sig_sm_ld_calc2_reg;
  wire sig_sm_ld_calc2_reg_ns;
  wire sig_sm_pop_input_reg;
  wire sig_sm_pop_input_reg_ns;
  wire sig_xfer_addr_reg0;
  wire [14:0]sig_xfer_address;
  wire sig_xfer_calc_err_reg_reg_0;
  wire sig_xfer_cmd_cmplt_reg_i_1_n_0;
  wire sig_xfer_eof_reg0;
  wire sig_xfer_is_seq_reg_i_1_n_0;
  wire sig_xfer_is_seq_reg_i_2_n_0;
  wire sig_xfer_is_seq_reg_i_3_n_0;
  wire sig_xfer_is_seq_reg_i_4_n_0;
  wire sig_xfer_is_seq_reg_i_5_n_0;
  wire sig_xfer_is_seq_reg_i_6_n_0;
  wire sig_xfer_is_seq_reg_i_7_n_0;
  wire \sig_xfer_len_reg[0]_i_1_n_0 ;
  wire \sig_xfer_len_reg[1]_i_1_n_0 ;
  wire \sig_xfer_len_reg[2]_i_1_n_0 ;
  wire \sig_xfer_len_reg[3]_i_1_n_0 ;
  wire \sig_xfer_len_reg[4]_i_1_n_0 ;
  wire \sig_xfer_len_reg[5]_i_1_n_0 ;
  wire \sig_xfer_len_reg[6]_i_1_n_0 ;
  wire \sig_xfer_len_reg[7]_i_1_n_0 ;
  wire sig_xfer_reg_empty;
  wire sig_xfer_reg_empty_i_3_n_0;
  wire [3:1]\NLW_GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[9]_i_1_CO_UNCONNECTED ;
  wire [3:2]\NLW_GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[9]_i_1_O_UNCONNECTED ;
  wire [3:3]\NLW_GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[15]_i_1_CO_UNCONNECTED ;
  wire [3:3]NLW_sig_btt_cntr0_carry__3_CO_UNCONNECTED;
  wire [3:0]NLW_sig_btt_eq_b2mbaa1_carry_O_UNCONNECTED;
  wire [3:0]NLW_sig_btt_lt_b2mbaa1_carry_O_UNCONNECTED;
  wire [3:2]NLW_sig_btt_lt_b2mbaa1_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_sig_btt_lt_b2mbaa1_carry__0_O_UNCONNECTED;

  LUT6 #(
    .INIT(64'h00330033B833B8FF)) 
    \FSM_sequential_sig_pcc_sm_state[0]_i_1 
       (.I0(\FSM_sequential_sig_pcc_sm_state[0]_i_2_n_0 ),
        .I1(sig_pcc_sm_state[0]),
        .I2(sig_push_xfer_reg15_out),
        .I3(sig_pcc_sm_state[2]),
        .I4(\GEN_ADDR_32.sig_addr_cntr_msh_reg[0]_0 ),
        .I5(sig_pcc_sm_state[1]),
        .O(\FSM_sequential_sig_pcc_sm_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \FSM_sequential_sig_pcc_sm_state[0]_i_2 
       (.I0(sig_parent_done),
        .I1(sig_calc_error_pushed),
        .O(\FSM_sequential_sig_pcc_sm_state[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h5A5A1A5A4A4A0A4A)) 
    \FSM_sequential_sig_pcc_sm_state[1]_i_1 
       (.I0(sig_pcc_sm_state[1]),
        .I1(sig_pcc_sm_state[2]),
        .I2(sig_pcc_sm_state[0]),
        .I3(sig_parent_done),
        .I4(sig_calc_error_pushed),
        .I5(\GEN_ADDR_32.sig_addr_cntr_msh_reg[0]_0 ),
        .O(\FSM_sequential_sig_pcc_sm_state[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h0040)) 
    \FSM_sequential_sig_pcc_sm_state[1]_i_2 
       (.I0(sig_xfer_calc_err_reg_reg_0),
        .I1(sig_cmd2pcc_cmd_valid),
        .I2(sig_input_reg_empty),
        .I3(sig_sm_halt_reg),
        .O(\GEN_ADDR_32.sig_addr_cntr_msh_reg[0]_0 ));
  LUT4 #(
    .INIT(16'h3C8C)) 
    \FSM_sequential_sig_pcc_sm_state[2]_i_1 
       (.I0(sig_calc_error_pushed),
        .I1(sig_pcc_sm_state[2]),
        .I2(sig_pcc_sm_state[0]),
        .I3(sig_pcc_sm_state[1]),
        .O(\FSM_sequential_sig_pcc_sm_state[2]_i_1_n_0 ));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_sig_pcc_sm_state_reg[0] 
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(\FSM_sequential_sig_pcc_sm_state[0]_i_1_n_0 ),
        .Q(sig_pcc_sm_state[0]),
        .R(out));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_sig_pcc_sm_state_reg[1] 
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(\FSM_sequential_sig_pcc_sm_state[1]_i_1_n_0 ),
        .Q(sig_pcc_sm_state[1]),
        .R(out));
  (* KEEP = "yes" *) 
  FDRE \FSM_sequential_sig_pcc_sm_state_reg[2] 
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(\FSM_sequential_sig_pcc_sm_state[2]_i_1_n_0 ),
        .Q(sig_pcc_sm_state[2]),
        .R(out));
  LUT4 #(
    .INIT(16'hB888)) 
    \GEN_ADDR_32.sig_addr_cntr_incr_imreg[0]_i_1 
       (.I0(sig_btt_residue_slice[0]),
        .I1(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_2_n_0 ),
        .I2(sig_first_xfer),
        .I3(sig_xfer_address[0]),
        .O(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h15)) 
    \GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_1 
       (.I0(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_2_n_0 ),
        .I1(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_3_n_0 ),
        .I2(sig_first_xfer),
        .O(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT5 #(
    .INIT(32'h00000002)) 
    \GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_2 
       (.I0(sig_btt_lt_b2mbaa1),
        .I1(sel0[0]),
        .I2(sel0[8]),
        .I3(sel0[9]),
        .I4(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_4_n_0 ),
        .O(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_3 
       (.I0(sig_xfer_address[9]),
        .I1(sig_xfer_address[8]),
        .I2(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[7]_i_2_n_0 ),
        .I3(sig_xfer_address[6]),
        .I4(sig_xfer_address[7]),
        .O(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_4 
       (.I0(sig_xfer_is_seq_reg_i_5_n_0),
        .I1(sel0[2]),
        .I2(sel0[3]),
        .I3(sel0[1]),
        .O(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h8BB88888)) 
    \GEN_ADDR_32.sig_addr_cntr_incr_imreg[1]_i_1 
       (.I0(sig_btt_residue_slice[1]),
        .I1(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_2_n_0 ),
        .I2(sig_xfer_address[1]),
        .I3(sig_xfer_address[0]),
        .I4(sig_first_xfer),
        .O(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h888BBBB888888888)) 
    \GEN_ADDR_32.sig_addr_cntr_incr_imreg[2]_i_1 
       (.I0(sig_btt_residue_slice[2]),
        .I1(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_2_n_0 ),
        .I2(sig_xfer_address[0]),
        .I3(sig_xfer_address[1]),
        .I4(sig_xfer_address[2]),
        .I5(sig_first_xfer),
        .O(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[2]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hB888)) 
    \GEN_ADDR_32.sig_addr_cntr_incr_imreg[3]_i_1 
       (.I0(sig_btt_residue_slice[3]),
        .I1(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_2_n_0 ),
        .I2(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[3]_i_2_n_0 ),
        .I3(sig_first_xfer),
        .O(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'h5556)) 
    \GEN_ADDR_32.sig_addr_cntr_incr_imreg[3]_i_2 
       (.I0(sig_xfer_address[3]),
        .I1(sig_xfer_address[0]),
        .I2(sig_xfer_address[1]),
        .I3(sig_xfer_address[2]),
        .O(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[3]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT4 #(
    .INIT(16'hB888)) 
    \GEN_ADDR_32.sig_addr_cntr_incr_imreg[4]_i_1 
       (.I0(sig_btt_residue_slice[4]),
        .I1(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_2_n_0 ),
        .I2(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[4]_i_2_n_0 ),
        .I3(sig_first_xfer),
        .O(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT5 #(
    .INIT(32'h55555556)) 
    \GEN_ADDR_32.sig_addr_cntr_incr_imreg[4]_i_2 
       (.I0(sig_xfer_address[4]),
        .I1(sig_xfer_address[2]),
        .I2(sig_xfer_address[1]),
        .I3(sig_xfer_address[0]),
        .I4(sig_xfer_address[3]),
        .O(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[4]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hB888)) 
    \GEN_ADDR_32.sig_addr_cntr_incr_imreg[5]_i_1 
       (.I0(sig_btt_residue_slice[5]),
        .I1(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_2_n_0 ),
        .I2(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[5]_i_2_n_0 ),
        .I3(sig_first_xfer),
        .O(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h5555555555555556)) 
    \GEN_ADDR_32.sig_addr_cntr_incr_imreg[5]_i_2 
       (.I0(sig_xfer_address[5]),
        .I1(sig_xfer_address[3]),
        .I2(sig_xfer_address[0]),
        .I3(sig_xfer_address[1]),
        .I4(sig_xfer_address[2]),
        .I5(sig_xfer_address[4]),
        .O(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[5]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFF600060)) 
    \GEN_ADDR_32.sig_addr_cntr_incr_imreg[6]_i_1 
       (.I0(sig_xfer_address[6]),
        .I1(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[7]_i_2_n_0 ),
        .I2(sig_first_xfer),
        .I3(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_2_n_0 ),
        .I4(sig_btt_residue_slice[6]),
        .O(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF00001E001E00)) 
    \GEN_ADDR_32.sig_addr_cntr_incr_imreg[7]_i_1 
       (.I0(sig_xfer_address[6]),
        .I1(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[7]_i_2_n_0 ),
        .I2(sig_xfer_address[7]),
        .I3(sig_first_xfer),
        .I4(sig_btt_residue_slice[7]),
        .I5(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_2_n_0 ),
        .O(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \GEN_ADDR_32.sig_addr_cntr_incr_imreg[7]_i_2 
       (.I0(sig_xfer_address[5]),
        .I1(sig_xfer_address[3]),
        .I2(sig_xfer_address[0]),
        .I3(sig_xfer_address[1]),
        .I4(sig_xfer_address[2]),
        .I5(sig_xfer_address[4]),
        .O(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[7]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hF022)) 
    \GEN_ADDR_32.sig_addr_cntr_incr_imreg[8]_i_1 
       (.I0(sig_first_xfer),
        .I1(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[8]_i_2_n_0 ),
        .I2(sig_btt_residue_slice[8]),
        .I3(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_2_n_0 ),
        .O(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[8]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hAAA9)) 
    \GEN_ADDR_32.sig_addr_cntr_incr_imreg[8]_i_2 
       (.I0(sig_xfer_address[8]),
        .I1(sig_xfer_address[7]),
        .I2(sig_xfer_address[6]),
        .I3(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[7]_i_2_n_0 ),
        .O(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[8]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hF808)) 
    \GEN_ADDR_32.sig_addr_cntr_incr_imreg[9]_i_1 
       (.I0(sig_first_xfer),
        .I1(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[9]_i_2_n_0 ),
        .I2(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_2_n_0 ),
        .I3(sig_btt_residue_slice[9]),
        .O(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[9]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT5 #(
    .INIT(32'h55555556)) 
    \GEN_ADDR_32.sig_addr_cntr_incr_imreg[9]_i_2 
       (.I0(sig_xfer_address[9]),
        .I1(sig_xfer_address[8]),
        .I2(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[7]_i_2_n_0 ),
        .I3(sig_xfer_address[6]),
        .I4(sig_xfer_address[7]),
        .O(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[9]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_incr_imreg_reg[0] 
       (.C(m_axi_aclk),
        .CE(sig_sm_ld_calc1_reg),
        .D(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[0]_i_1_n_0 ),
        .Q(sig_addr_cntr_incr_imreg[0]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_incr_imreg_reg[10] 
       (.C(m_axi_aclk),
        .CE(sig_sm_ld_calc1_reg),
        .D(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_1_n_0 ),
        .Q(sig_addr_cntr_incr_imreg[10]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_incr_imreg_reg[1] 
       (.C(m_axi_aclk),
        .CE(sig_sm_ld_calc1_reg),
        .D(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[1]_i_1_n_0 ),
        .Q(sig_addr_cntr_incr_imreg[1]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_incr_imreg_reg[2] 
       (.C(m_axi_aclk),
        .CE(sig_sm_ld_calc1_reg),
        .D(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[2]_i_1_n_0 ),
        .Q(sig_addr_cntr_incr_imreg[2]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_incr_imreg_reg[3] 
       (.C(m_axi_aclk),
        .CE(sig_sm_ld_calc1_reg),
        .D(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[3]_i_1_n_0 ),
        .Q(sig_addr_cntr_incr_imreg[3]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_incr_imreg_reg[4] 
       (.C(m_axi_aclk),
        .CE(sig_sm_ld_calc1_reg),
        .D(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[4]_i_1_n_0 ),
        .Q(sig_addr_cntr_incr_imreg[4]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_incr_imreg_reg[5] 
       (.C(m_axi_aclk),
        .CE(sig_sm_ld_calc1_reg),
        .D(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[5]_i_1_n_0 ),
        .Q(sig_addr_cntr_incr_imreg[5]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_incr_imreg_reg[6] 
       (.C(m_axi_aclk),
        .CE(sig_sm_ld_calc1_reg),
        .D(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[6]_i_1_n_0 ),
        .Q(sig_addr_cntr_incr_imreg[6]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_incr_imreg_reg[7] 
       (.C(m_axi_aclk),
        .CE(sig_sm_ld_calc1_reg),
        .D(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[7]_i_1_n_0 ),
        .Q(sig_addr_cntr_incr_imreg[7]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_incr_imreg_reg[8] 
       (.C(m_axi_aclk),
        .CE(sig_sm_ld_calc1_reg),
        .D(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[8]_i_1_n_0 ),
        .Q(sig_addr_cntr_incr_imreg[8]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_incr_imreg_reg[9] 
       (.C(m_axi_aclk),
        .CE(sig_sm_ld_calc1_reg),
        .D(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[9]_i_1_n_0 ),
        .Q(sig_addr_cntr_incr_imreg[9]),
        .R(out));
  LUT6 #(
    .INIT(64'hFFFFEFFF00002000)) 
    \GEN_ADDR_32.sig_addr_cntr_lsh[10]_i_1 
       (.I0(Q[8]),
        .I1(sig_xfer_calc_err_reg_reg_0),
        .I2(sig_cmd2pcc_cmd_valid),
        .I3(sig_input_reg_empty),
        .I4(sig_sm_halt_reg),
        .I5(\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[10] ),
        .O(p_1_in[10]));
  LUT6 #(
    .INIT(64'hFFFFEFFF00002000)) 
    \GEN_ADDR_32.sig_addr_cntr_lsh[11]_i_1 
       (.I0(Q[9]),
        .I1(sig_xfer_calc_err_reg_reg_0),
        .I2(sig_cmd2pcc_cmd_valid),
        .I3(sig_input_reg_empty),
        .I4(sig_sm_halt_reg),
        .I5(\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[11] ),
        .O(p_1_in[11]));
  LUT6 #(
    .INIT(64'hFFFFEFFF00002000)) 
    \GEN_ADDR_32.sig_addr_cntr_lsh[12]_i_1 
       (.I0(Q[10]),
        .I1(sig_xfer_calc_err_reg_reg_0),
        .I2(sig_cmd2pcc_cmd_valid),
        .I3(sig_input_reg_empty),
        .I4(sig_sm_halt_reg),
        .I5(\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[12] ),
        .O(p_1_in[12]));
  LUT6 #(
    .INIT(64'hFFFFEFFF00002000)) 
    \GEN_ADDR_32.sig_addr_cntr_lsh[13]_i_1 
       (.I0(Q[11]),
        .I1(sig_xfer_calc_err_reg_reg_0),
        .I2(sig_cmd2pcc_cmd_valid),
        .I3(sig_input_reg_empty),
        .I4(sig_sm_halt_reg),
        .I5(\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[13] ),
        .O(p_1_in[13]));
  LUT6 #(
    .INIT(64'hFFFFEFFF00002000)) 
    \GEN_ADDR_32.sig_addr_cntr_lsh[14]_i_1 
       (.I0(Q[12]),
        .I1(sig_xfer_calc_err_reg_reg_0),
        .I2(sig_cmd2pcc_cmd_valid),
        .I3(sig_input_reg_empty),
        .I4(sig_sm_halt_reg),
        .I5(\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[14] ),
        .O(p_1_in[14]));
  LUT4 #(
    .INIT(16'hEAAA)) 
    \GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1 
       (.I0(\GEN_ADDR_32.sig_addr_cntr_msh_reg[0]_0 ),
        .I1(sig_xfer_reg_empty),
        .I2(sig_ld_xfer_reg),
        .I3(sig_input_burst_type_reg),
        .O(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFEFFF00002000)) 
    \GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_2 
       (.I0(Q[13]),
        .I1(sig_xfer_calc_err_reg_reg_0),
        .I2(sig_cmd2pcc_cmd_valid),
        .I3(sig_input_reg_empty),
        .I4(sig_sm_halt_reg),
        .I5(p_0_in),
        .O(p_1_in[15]));
  LUT6 #(
    .INIT(64'hFFFFEFFF00002000)) 
    \GEN_ADDR_32.sig_addr_cntr_lsh[2]_i_1 
       (.I0(Q[0]),
        .I1(sig_xfer_calc_err_reg_reg_0),
        .I2(sig_cmd2pcc_cmd_valid),
        .I3(sig_input_reg_empty),
        .I4(sig_sm_halt_reg),
        .I5(\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[2] ),
        .O(p_1_in[2]));
  LUT6 #(
    .INIT(64'hFFFFEFFF00002000)) 
    \GEN_ADDR_32.sig_addr_cntr_lsh[3]_i_1 
       (.I0(Q[1]),
        .I1(sig_xfer_calc_err_reg_reg_0),
        .I2(sig_cmd2pcc_cmd_valid),
        .I3(sig_input_reg_empty),
        .I4(sig_sm_halt_reg),
        .I5(\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[3] ),
        .O(p_1_in[3]));
  LUT6 #(
    .INIT(64'hFFFFEFFF00002000)) 
    \GEN_ADDR_32.sig_addr_cntr_lsh[4]_i_1 
       (.I0(Q[2]),
        .I1(sig_xfer_calc_err_reg_reg_0),
        .I2(sig_cmd2pcc_cmd_valid),
        .I3(sig_input_reg_empty),
        .I4(sig_sm_halt_reg),
        .I5(\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[4] ),
        .O(p_1_in[4]));
  LUT6 #(
    .INIT(64'hFFFFEFFF00002000)) 
    \GEN_ADDR_32.sig_addr_cntr_lsh[5]_i_1 
       (.I0(Q[3]),
        .I1(sig_xfer_calc_err_reg_reg_0),
        .I2(sig_cmd2pcc_cmd_valid),
        .I3(sig_input_reg_empty),
        .I4(sig_sm_halt_reg),
        .I5(\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[5] ),
        .O(p_1_in[5]));
  LUT6 #(
    .INIT(64'hFFFFEFFF00002000)) 
    \GEN_ADDR_32.sig_addr_cntr_lsh[6]_i_1 
       (.I0(Q[4]),
        .I1(sig_xfer_calc_err_reg_reg_0),
        .I2(sig_cmd2pcc_cmd_valid),
        .I3(sig_input_reg_empty),
        .I4(sig_sm_halt_reg),
        .I5(\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[6] ),
        .O(p_1_in[6]));
  LUT6 #(
    .INIT(64'hFFFFEFFF00002000)) 
    \GEN_ADDR_32.sig_addr_cntr_lsh[7]_i_1 
       (.I0(Q[5]),
        .I1(sig_xfer_calc_err_reg_reg_0),
        .I2(sig_cmd2pcc_cmd_valid),
        .I3(sig_input_reg_empty),
        .I4(sig_sm_halt_reg),
        .I5(\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[7] ),
        .O(p_1_in[7]));
  LUT6 #(
    .INIT(64'hFFFFEFFF00002000)) 
    \GEN_ADDR_32.sig_addr_cntr_lsh[8]_i_1 
       (.I0(Q[6]),
        .I1(sig_xfer_calc_err_reg_reg_0),
        .I2(sig_cmd2pcc_cmd_valid),
        .I3(sig_input_reg_empty),
        .I4(sig_sm_halt_reg),
        .I5(\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[8] ),
        .O(p_1_in[8]));
  LUT6 #(
    .INIT(64'hFFFFEFFF00002000)) 
    \GEN_ADDR_32.sig_addr_cntr_lsh[9]_i_1 
       (.I0(Q[7]),
        .I1(sig_xfer_calc_err_reg_reg_0),
        .I2(sig_cmd2pcc_cmd_valid),
        .I3(sig_input_reg_empty),
        .I4(sig_sm_halt_reg),
        .I5(\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[9] ),
        .O(p_1_in[9]));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_lsh_reg[0] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(D[0]),
        .Q(sig_xfer_address[0]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_lsh_reg[10] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(p_1_in[10]),
        .Q(sig_xfer_address[10]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_lsh_reg[11] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(p_1_in[11]),
        .Q(sig_xfer_address[11]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_lsh_reg[12] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(p_1_in[12]),
        .Q(sig_xfer_address[12]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_lsh_reg[13] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(p_1_in[13]),
        .Q(sig_xfer_address[13]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_lsh_reg[14] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(p_1_in[14]),
        .Q(sig_xfer_address[14]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_lsh_reg[15] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(p_1_in[15]),
        .Q(p_1_in2_in),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_lsh_reg[1] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(D[1]),
        .Q(sig_xfer_address[1]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_lsh_reg[2] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(p_1_in[2]),
        .Q(sig_xfer_address[2]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_lsh_reg[3] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(p_1_in[3]),
        .Q(sig_xfer_address[3]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_lsh_reg[4] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(p_1_in[4]),
        .Q(sig_xfer_address[4]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_lsh_reg[5] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(p_1_in[5]),
        .Q(sig_xfer_address[5]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_lsh_reg[6] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(p_1_in[6]),
        .Q(sig_xfer_address[6]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_lsh_reg[7] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(p_1_in[7]),
        .Q(sig_xfer_address[7]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_lsh_reg[8] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(p_1_in[8]),
        .Q(sig_xfer_address[8]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_lsh_reg[9] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(p_1_in[9]),
        .Q(sig_xfer_address[9]),
        .R(out));
  LUT6 #(
    .INIT(64'hBAAAAAAAAAAAAAAA)) 
    \GEN_ADDR_32.sig_addr_cntr_msh[0]_i_1 
       (.I0(\GEN_ADDR_32.sig_addr_cntr_msh_reg[0]_0 ),
        .I1(p_0_in),
        .I2(p_1_in2_in),
        .I3(sig_input_burst_type_reg),
        .I4(sig_ld_xfer_reg),
        .I5(sig_xfer_reg_empty),
        .O(\GEN_ADDR_32.sig_addr_cntr_msh[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_msh_reg[0] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_msh[0]_i_1_n_0 ),
        .D(O[0]),
        .Q(\GEN_ADDR_32.sig_addr_cntr_msh_reg [0]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_msh_reg[10] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_msh[0]_i_1_n_0 ),
        .D(\GEN_ADDR_32.sig_addr_cntr_msh_reg[11]_1 [2]),
        .Q(\GEN_ADDR_32.sig_addr_cntr_msh_reg [10]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_msh_reg[11] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_msh[0]_i_1_n_0 ),
        .D(\GEN_ADDR_32.sig_addr_cntr_msh_reg[11]_1 [3]),
        .Q(\GEN_ADDR_32.sig_addr_cntr_msh_reg [11]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_msh_reg[12] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_msh[0]_i_1_n_0 ),
        .D(\GEN_ADDR_32.sig_addr_cntr_msh_reg[15]_1 [0]),
        .Q(\GEN_ADDR_32.sig_addr_cntr_msh_reg [12]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_msh_reg[13] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_msh[0]_i_1_n_0 ),
        .D(\GEN_ADDR_32.sig_addr_cntr_msh_reg[15]_1 [1]),
        .Q(\GEN_ADDR_32.sig_addr_cntr_msh_reg [13]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_msh_reg[14] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_msh[0]_i_1_n_0 ),
        .D(\GEN_ADDR_32.sig_addr_cntr_msh_reg[15]_1 [2]),
        .Q(\GEN_ADDR_32.sig_addr_cntr_msh_reg [14]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_msh_reg[15] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_msh[0]_i_1_n_0 ),
        .D(\GEN_ADDR_32.sig_addr_cntr_msh_reg[15]_1 [3]),
        .Q(\GEN_ADDR_32.sig_addr_cntr_msh_reg [15]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_msh_reg[1] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_msh[0]_i_1_n_0 ),
        .D(O[1]),
        .Q(\GEN_ADDR_32.sig_addr_cntr_msh_reg [1]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_msh_reg[2] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_msh[0]_i_1_n_0 ),
        .D(O[2]),
        .Q(\GEN_ADDR_32.sig_addr_cntr_msh_reg [2]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_msh_reg[3] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_msh[0]_i_1_n_0 ),
        .D(O[3]),
        .Q(\GEN_ADDR_32.sig_addr_cntr_msh_reg [3]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_msh_reg[4] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_msh[0]_i_1_n_0 ),
        .D(\GEN_ADDR_32.sig_addr_cntr_msh_reg[7]_1 [0]),
        .Q(\GEN_ADDR_32.sig_addr_cntr_msh_reg [4]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_msh_reg[5] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_msh[0]_i_1_n_0 ),
        .D(\GEN_ADDR_32.sig_addr_cntr_msh_reg[7]_1 [1]),
        .Q(\GEN_ADDR_32.sig_addr_cntr_msh_reg [5]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_msh_reg[6] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_msh[0]_i_1_n_0 ),
        .D(\GEN_ADDR_32.sig_addr_cntr_msh_reg[7]_1 [2]),
        .Q(\GEN_ADDR_32.sig_addr_cntr_msh_reg [6]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_msh_reg[7] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_msh[0]_i_1_n_0 ),
        .D(\GEN_ADDR_32.sig_addr_cntr_msh_reg[7]_1 [3]),
        .Q(\GEN_ADDR_32.sig_addr_cntr_msh_reg [7]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_msh_reg[8] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_msh[0]_i_1_n_0 ),
        .D(\GEN_ADDR_32.sig_addr_cntr_msh_reg[11]_1 [0]),
        .Q(\GEN_ADDR_32.sig_addr_cntr_msh_reg [8]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_addr_cntr_msh_reg[9] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_msh[0]_i_1_n_0 ),
        .D(\GEN_ADDR_32.sig_addr_cntr_msh_reg[11]_1 [1]),
        .Q(\GEN_ADDR_32.sig_addr_cntr_msh_reg [9]),
        .R(out));
  LUT4 #(
    .INIT(16'hB888)) 
    \GEN_ADDR_32.sig_adjusted_addr_incr_reg[3]_i_2 
       (.I0(sig_btt_residue_slice[3]),
        .I1(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_2_n_0 ),
        .I2(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[3]_i_2_n_0 ),
        .I3(sig_first_xfer),
        .O(\GEN_ADDR_32.sig_adjusted_addr_incr_reg[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h888BBBB888888888)) 
    \GEN_ADDR_32.sig_adjusted_addr_incr_reg[3]_i_3 
       (.I0(sig_btt_residue_slice[2]),
        .I1(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_2_n_0 ),
        .I2(sig_xfer_address[0]),
        .I3(sig_xfer_address[1]),
        .I4(sig_xfer_address[2]),
        .I5(sig_first_xfer),
        .O(\GEN_ADDR_32.sig_adjusted_addr_incr_reg[3]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h0DFDF808)) 
    \GEN_ADDR_32.sig_adjusted_addr_incr_reg[3]_i_4 
       (.I0(sig_first_xfer),
        .I1(sig_xfer_address[0]),
        .I2(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_2_n_0 ),
        .I3(sig_btt_residue_slice[1]),
        .I4(sig_xfer_address[1]),
        .O(\GEN_ADDR_32.sig_adjusted_addr_incr_reg[3]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h1DC0)) 
    \GEN_ADDR_32.sig_adjusted_addr_incr_reg[3]_i_5 
       (.I0(sig_first_xfer),
        .I1(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_2_n_0 ),
        .I2(sig_btt_residue_slice[0]),
        .I3(sig_xfer_address[0]),
        .O(\GEN_ADDR_32.sig_adjusted_addr_incr_reg[3]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF00001E001E00)) 
    \GEN_ADDR_32.sig_adjusted_addr_incr_reg[7]_i_2 
       (.I0(sig_xfer_address[6]),
        .I1(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[7]_i_2_n_0 ),
        .I2(sig_xfer_address[7]),
        .I3(sig_first_xfer),
        .I4(sig_btt_residue_slice[7]),
        .I5(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_2_n_0 ),
        .O(\GEN_ADDR_32.sig_adjusted_addr_incr_reg[7]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hFF600060)) 
    \GEN_ADDR_32.sig_adjusted_addr_incr_reg[7]_i_3 
       (.I0(sig_xfer_address[6]),
        .I1(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[7]_i_2_n_0 ),
        .I2(sig_first_xfer),
        .I3(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_2_n_0 ),
        .I4(sig_btt_residue_slice[6]),
        .O(\GEN_ADDR_32.sig_adjusted_addr_incr_reg[7]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hB888)) 
    \GEN_ADDR_32.sig_adjusted_addr_incr_reg[7]_i_4 
       (.I0(sig_btt_residue_slice[5]),
        .I1(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_2_n_0 ),
        .I2(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[5]_i_2_n_0 ),
        .I3(sig_first_xfer),
        .O(\GEN_ADDR_32.sig_adjusted_addr_incr_reg[7]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hB888)) 
    \GEN_ADDR_32.sig_adjusted_addr_incr_reg[7]_i_5 
       (.I0(sig_btt_residue_slice[4]),
        .I1(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_2_n_0 ),
        .I2(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[4]_i_2_n_0 ),
        .I3(sig_first_xfer),
        .O(\GEN_ADDR_32.sig_adjusted_addr_incr_reg[7]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'hF808)) 
    \GEN_ADDR_32.sig_adjusted_addr_incr_reg[9]_i_2 
       (.I0(sig_first_xfer),
        .I1(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[9]_i_2_n_0 ),
        .I2(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_2_n_0 ),
        .I3(sig_btt_residue_slice[9]),
        .O(\GEN_ADDR_32.sig_adjusted_addr_incr_reg[9]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hF022)) 
    \GEN_ADDR_32.sig_adjusted_addr_incr_reg[9]_i_3 
       (.I0(sig_first_xfer),
        .I1(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[8]_i_2_n_0 ),
        .I2(sig_btt_residue_slice[8]),
        .I3(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_2_n_0 ),
        .O(\GEN_ADDR_32.sig_adjusted_addr_incr_reg[9]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[0] 
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_adjusted_addr_incr[0]),
        .Q(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[0] ),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[1] 
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_adjusted_addr_incr[1]),
        .Q(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[1] ),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[2] 
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_adjusted_addr_incr[2]),
        .Q(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[2] ),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[3] 
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_adjusted_addr_incr[3]),
        .Q(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[3] ),
        .R(out));
  CARRY4 \GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[3]_i_1_n_0 ,\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[3]_i_1_n_1 ,\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[3]_i_1_n_2 ,\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[3]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,sig_xfer_address[1:0]}),
        .O(sig_adjusted_addr_incr[3:0]),
        .S({\GEN_ADDR_32.sig_adjusted_addr_incr_reg[3]_i_2_n_0 ,\GEN_ADDR_32.sig_adjusted_addr_incr_reg[3]_i_3_n_0 ,\GEN_ADDR_32.sig_adjusted_addr_incr_reg[3]_i_4_n_0 ,\GEN_ADDR_32.sig_adjusted_addr_incr_reg[3]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[4] 
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_adjusted_addr_incr[4]),
        .Q(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[4] ),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[5] 
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_adjusted_addr_incr[5]),
        .Q(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[5] ),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[6] 
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_adjusted_addr_incr[6]),
        .Q(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[6] ),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[7] 
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_adjusted_addr_incr[7]),
        .Q(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[7] ),
        .R(out));
  CARRY4 \GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[7]_i_1 
       (.CI(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[3]_i_1_n_0 ),
        .CO({\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[7]_i_1_n_0 ,\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[7]_i_1_n_1 ,\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[7]_i_1_n_2 ,\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(sig_adjusted_addr_incr[7:4]),
        .S({\GEN_ADDR_32.sig_adjusted_addr_incr_reg[7]_i_2_n_0 ,\GEN_ADDR_32.sig_adjusted_addr_incr_reg[7]_i_3_n_0 ,\GEN_ADDR_32.sig_adjusted_addr_incr_reg[7]_i_4_n_0 ,\GEN_ADDR_32.sig_adjusted_addr_incr_reg[7]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[8] 
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_adjusted_addr_incr[8]),
        .Q(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[8] ),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[9] 
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_adjusted_addr_incr[9]),
        .Q(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[9] ),
        .R(out));
  CARRY4 \GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[9]_i_1 
       (.CI(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[7]_i_1_n_0 ),
        .CO({\NLW_GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[9]_i_1_CO_UNCONNECTED [3:1],\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[9]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[9]_i_1_O_UNCONNECTED [3:2],sig_adjusted_addr_incr[9:8]}),
        .S({1'b0,1'b0,\GEN_ADDR_32.sig_adjusted_addr_incr_reg[9]_i_2_n_0 ,\GEN_ADDR_32.sig_adjusted_addr_incr_reg[9]_i_3_n_0 }));
  LUT6 #(
    .INIT(64'h000000000EEEEEEE)) 
    \GEN_ADDR_32.sig_first_xfer_i_1 
       (.I0(sig_first_xfer),
        .I1(\GEN_ADDR_32.sig_addr_cntr_msh_reg[0]_0 ),
        .I2(sig_input_burst_type_reg),
        .I3(sig_ld_xfer_reg),
        .I4(sig_xfer_reg_empty),
        .I5(out),
        .O(\GEN_ADDR_32.sig_first_xfer_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_first_xfer_reg 
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(\GEN_ADDR_32.sig_first_xfer_i_1_n_0 ),
        .Q(sig_first_xfer),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h2)) 
    \GEN_ADDR_32.sig_predict_addr_lsh_imreg[11]_i_2 
       (.I0(sig_xfer_address[11]),
        .O(\GEN_ADDR_32.sig_predict_addr_lsh_imreg[11]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \GEN_ADDR_32.sig_predict_addr_lsh_imreg[11]_i_3 
       (.I0(sig_xfer_address[10]),
        .I1(sig_addr_cntr_incr_imreg[10]),
        .O(\GEN_ADDR_32.sig_predict_addr_lsh_imreg[11]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \GEN_ADDR_32.sig_predict_addr_lsh_imreg[11]_i_4 
       (.I0(sig_xfer_address[9]),
        .I1(sig_addr_cntr_incr_imreg[9]),
        .O(\GEN_ADDR_32.sig_predict_addr_lsh_imreg[11]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \GEN_ADDR_32.sig_predict_addr_lsh_imreg[11]_i_5 
       (.I0(sig_xfer_address[8]),
        .I1(sig_addr_cntr_incr_imreg[8]),
        .O(\GEN_ADDR_32.sig_predict_addr_lsh_imreg[11]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \GEN_ADDR_32.sig_predict_addr_lsh_imreg[15]_i_2 
       (.I0(p_1_in2_in),
        .O(\GEN_ADDR_32.sig_predict_addr_lsh_imreg[15]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \GEN_ADDR_32.sig_predict_addr_lsh_imreg[15]_i_3 
       (.I0(sig_xfer_address[14]),
        .O(\GEN_ADDR_32.sig_predict_addr_lsh_imreg[15]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \GEN_ADDR_32.sig_predict_addr_lsh_imreg[15]_i_4 
       (.I0(sig_xfer_address[13]),
        .O(\GEN_ADDR_32.sig_predict_addr_lsh_imreg[15]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \GEN_ADDR_32.sig_predict_addr_lsh_imreg[15]_i_5 
       (.I0(sig_xfer_address[12]),
        .O(\GEN_ADDR_32.sig_predict_addr_lsh_imreg[15]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \GEN_ADDR_32.sig_predict_addr_lsh_imreg[3]_i_2 
       (.I0(sig_xfer_address[3]),
        .I1(sig_addr_cntr_incr_imreg[3]),
        .O(\GEN_ADDR_32.sig_predict_addr_lsh_imreg[3]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \GEN_ADDR_32.sig_predict_addr_lsh_imreg[3]_i_3 
       (.I0(sig_xfer_address[2]),
        .I1(sig_addr_cntr_incr_imreg[2]),
        .O(\GEN_ADDR_32.sig_predict_addr_lsh_imreg[3]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \GEN_ADDR_32.sig_predict_addr_lsh_imreg[3]_i_4 
       (.I0(sig_xfer_address[1]),
        .I1(sig_addr_cntr_incr_imreg[1]),
        .O(\GEN_ADDR_32.sig_predict_addr_lsh_imreg[3]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \GEN_ADDR_32.sig_predict_addr_lsh_imreg[3]_i_5 
       (.I0(sig_xfer_address[0]),
        .I1(sig_addr_cntr_incr_imreg[0]),
        .O(\GEN_ADDR_32.sig_predict_addr_lsh_imreg[3]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \GEN_ADDR_32.sig_predict_addr_lsh_imreg[7]_i_2 
       (.I0(sig_xfer_address[7]),
        .I1(sig_addr_cntr_incr_imreg[7]),
        .O(\GEN_ADDR_32.sig_predict_addr_lsh_imreg[7]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \GEN_ADDR_32.sig_predict_addr_lsh_imreg[7]_i_3 
       (.I0(sig_xfer_address[6]),
        .I1(sig_addr_cntr_incr_imreg[6]),
        .O(\GEN_ADDR_32.sig_predict_addr_lsh_imreg[7]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \GEN_ADDR_32.sig_predict_addr_lsh_imreg[7]_i_4 
       (.I0(sig_xfer_address[5]),
        .I1(sig_addr_cntr_incr_imreg[5]),
        .O(\GEN_ADDR_32.sig_predict_addr_lsh_imreg[7]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \GEN_ADDR_32.sig_predict_addr_lsh_imreg[7]_i_5 
       (.I0(sig_xfer_address[4]),
        .I1(sig_addr_cntr_incr_imreg[4]),
        .O(\GEN_ADDR_32.sig_predict_addr_lsh_imreg[7]_i_5_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[0] 
       (.C(m_axi_aclk),
        .CE(sig_sm_ld_calc2_reg),
        .D(sig_predict_addr_lsh_im3_in[0]),
        .Q(\GEN_ADDR_32.sig_addr_cntr_lsh_reg[1]_0 [0]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[10] 
       (.C(m_axi_aclk),
        .CE(sig_sm_ld_calc2_reg),
        .D(sig_predict_addr_lsh_im3_in[10]),
        .Q(\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[10] ),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[11] 
       (.C(m_axi_aclk),
        .CE(sig_sm_ld_calc2_reg),
        .D(sig_predict_addr_lsh_im3_in[11]),
        .Q(\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[11] ),
        .R(out));
  CARRY4 \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[11]_i_1 
       (.CI(\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[7]_i_1_n_0 ),
        .CO({\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[11]_i_1_n_0 ,\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[11]_i_1_n_1 ,\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[11]_i_1_n_2 ,\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[11]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,sig_xfer_address[10:8]}),
        .O(sig_predict_addr_lsh_im3_in[11:8]),
        .S({\GEN_ADDR_32.sig_predict_addr_lsh_imreg[11]_i_2_n_0 ,\GEN_ADDR_32.sig_predict_addr_lsh_imreg[11]_i_3_n_0 ,\GEN_ADDR_32.sig_predict_addr_lsh_imreg[11]_i_4_n_0 ,\GEN_ADDR_32.sig_predict_addr_lsh_imreg[11]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[12] 
       (.C(m_axi_aclk),
        .CE(sig_sm_ld_calc2_reg),
        .D(sig_predict_addr_lsh_im3_in[12]),
        .Q(\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[12] ),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[13] 
       (.C(m_axi_aclk),
        .CE(sig_sm_ld_calc2_reg),
        .D(sig_predict_addr_lsh_im3_in[13]),
        .Q(\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[13] ),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[14] 
       (.C(m_axi_aclk),
        .CE(sig_sm_ld_calc2_reg),
        .D(sig_predict_addr_lsh_im3_in[14]),
        .Q(\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[14] ),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[15] 
       (.C(m_axi_aclk),
        .CE(sig_sm_ld_calc2_reg),
        .D(sig_predict_addr_lsh_im3_in[15]),
        .Q(p_0_in),
        .R(out));
  CARRY4 \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[15]_i_1 
       (.CI(\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[11]_i_1_n_0 ),
        .CO({\NLW_GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[15]_i_1_CO_UNCONNECTED [3],\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[15]_i_1_n_1 ,\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[15]_i_1_n_2 ,\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[15]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(sig_predict_addr_lsh_im3_in[15:12]),
        .S({\GEN_ADDR_32.sig_predict_addr_lsh_imreg[15]_i_2_n_0 ,\GEN_ADDR_32.sig_predict_addr_lsh_imreg[15]_i_3_n_0 ,\GEN_ADDR_32.sig_predict_addr_lsh_imreg[15]_i_4_n_0 ,\GEN_ADDR_32.sig_predict_addr_lsh_imreg[15]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[1] 
       (.C(m_axi_aclk),
        .CE(sig_sm_ld_calc2_reg),
        .D(sig_predict_addr_lsh_im3_in[1]),
        .Q(\GEN_ADDR_32.sig_addr_cntr_lsh_reg[1]_0 [1]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[2] 
       (.C(m_axi_aclk),
        .CE(sig_sm_ld_calc2_reg),
        .D(sig_predict_addr_lsh_im3_in[2]),
        .Q(\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[2] ),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[3] 
       (.C(m_axi_aclk),
        .CE(sig_sm_ld_calc2_reg),
        .D(sig_predict_addr_lsh_im3_in[3]),
        .Q(\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[3] ),
        .R(out));
  CARRY4 \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[3]_i_1_n_0 ,\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[3]_i_1_n_1 ,\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[3]_i_1_n_2 ,\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[3]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(sig_xfer_address[3:0]),
        .O(sig_predict_addr_lsh_im3_in[3:0]),
        .S({\GEN_ADDR_32.sig_predict_addr_lsh_imreg[3]_i_2_n_0 ,\GEN_ADDR_32.sig_predict_addr_lsh_imreg[3]_i_3_n_0 ,\GEN_ADDR_32.sig_predict_addr_lsh_imreg[3]_i_4_n_0 ,\GEN_ADDR_32.sig_predict_addr_lsh_imreg[3]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[4] 
       (.C(m_axi_aclk),
        .CE(sig_sm_ld_calc2_reg),
        .D(sig_predict_addr_lsh_im3_in[4]),
        .Q(\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[4] ),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[5] 
       (.C(m_axi_aclk),
        .CE(sig_sm_ld_calc2_reg),
        .D(sig_predict_addr_lsh_im3_in[5]),
        .Q(\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[5] ),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[6] 
       (.C(m_axi_aclk),
        .CE(sig_sm_ld_calc2_reg),
        .D(sig_predict_addr_lsh_im3_in[6]),
        .Q(\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[6] ),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[7] 
       (.C(m_axi_aclk),
        .CE(sig_sm_ld_calc2_reg),
        .D(sig_predict_addr_lsh_im3_in[7]),
        .Q(\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[7] ),
        .R(out));
  CARRY4 \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[7]_i_1 
       (.CI(\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[3]_i_1_n_0 ),
        .CO({\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[7]_i_1_n_0 ,\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[7]_i_1_n_1 ,\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[7]_i_1_n_2 ,\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(sig_xfer_address[7:4]),
        .O(sig_predict_addr_lsh_im3_in[7:4]),
        .S({\GEN_ADDR_32.sig_predict_addr_lsh_imreg[7]_i_2_n_0 ,\GEN_ADDR_32.sig_predict_addr_lsh_imreg[7]_i_3_n_0 ,\GEN_ADDR_32.sig_predict_addr_lsh_imreg[7]_i_4_n_0 ,\GEN_ADDR_32.sig_predict_addr_lsh_imreg[7]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[8] 
       (.C(m_axi_aclk),
        .CE(sig_sm_ld_calc2_reg),
        .D(sig_predict_addr_lsh_im3_in[8]),
        .Q(\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[8] ),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg[9] 
       (.C(m_axi_aclk),
        .CE(sig_sm_ld_calc2_reg),
        .D(sig_predict_addr_lsh_im3_in[9]),
        .Q(\GEN_ADDR_32.sig_predict_addr_lsh_imreg_reg_n_0_[9] ),
        .R(out));
  LUT5 #(
    .INIT(32'hAAAA8AAA)) 
    i__carry__0_i_1
       (.I0(\GEN_ADDR_32.sig_addr_cntr_msh_reg [7]),
        .I1(sig_sm_halt_reg),
        .I2(sig_input_reg_empty),
        .I3(sig_cmd2pcc_cmd_valid),
        .I4(sig_xfer_calc_err_reg_reg_0),
        .O(\GEN_ADDR_32.sig_addr_cntr_msh_reg[7]_0 [3]));
  LUT6 #(
    .INIT(64'hFFFFEFFF00002000)) 
    i__carry__0_i_2
       (.I0(sig_cmd_mstrd_req),
        .I1(sig_xfer_calc_err_reg_reg_0),
        .I2(sig_cmd2pcc_cmd_valid),
        .I3(sig_input_reg_empty),
        .I4(sig_sm_halt_reg),
        .I5(\GEN_ADDR_32.sig_addr_cntr_msh_reg [6]),
        .O(\GEN_ADDR_32.sig_addr_cntr_msh_reg[7]_0 [2]));
  LUT5 #(
    .INIT(32'hAAAA8AAA)) 
    i__carry__0_i_3
       (.I0(\GEN_ADDR_32.sig_addr_cntr_msh_reg [5]),
        .I1(sig_sm_halt_reg),
        .I2(sig_input_reg_empty),
        .I3(sig_cmd2pcc_cmd_valid),
        .I4(sig_xfer_calc_err_reg_reg_0),
        .O(\GEN_ADDR_32.sig_addr_cntr_msh_reg[7]_0 [1]));
  LUT6 #(
    .INIT(64'hFFFFEFFF00002000)) 
    i__carry__0_i_4
       (.I0(Q[18]),
        .I1(sig_xfer_calc_err_reg_reg_0),
        .I2(sig_cmd2pcc_cmd_valid),
        .I3(sig_input_reg_empty),
        .I4(sig_sm_halt_reg),
        .I5(\GEN_ADDR_32.sig_addr_cntr_msh_reg [4]),
        .O(\GEN_ADDR_32.sig_addr_cntr_msh_reg[7]_0 [0]));
  LUT5 #(
    .INIT(32'hAAAA8AAA)) 
    i__carry__1_i_1
       (.I0(\GEN_ADDR_32.sig_addr_cntr_msh_reg [11]),
        .I1(sig_sm_halt_reg),
        .I2(sig_input_reg_empty),
        .I3(sig_cmd2pcc_cmd_valid),
        .I4(sig_xfer_calc_err_reg_reg_0),
        .O(\GEN_ADDR_32.sig_addr_cntr_msh_reg[11]_0 [3]));
  LUT5 #(
    .INIT(32'hAAAA8AAA)) 
    i__carry__1_i_2
       (.I0(\GEN_ADDR_32.sig_addr_cntr_msh_reg [10]),
        .I1(sig_sm_halt_reg),
        .I2(sig_input_reg_empty),
        .I3(sig_cmd2pcc_cmd_valid),
        .I4(sig_xfer_calc_err_reg_reg_0),
        .O(\GEN_ADDR_32.sig_addr_cntr_msh_reg[11]_0 [2]));
  LUT5 #(
    .INIT(32'hAAAA8AAA)) 
    i__carry__1_i_3
       (.I0(\GEN_ADDR_32.sig_addr_cntr_msh_reg [9]),
        .I1(sig_sm_halt_reg),
        .I2(sig_input_reg_empty),
        .I3(sig_cmd2pcc_cmd_valid),
        .I4(sig_xfer_calc_err_reg_reg_0),
        .O(\GEN_ADDR_32.sig_addr_cntr_msh_reg[11]_0 [1]));
  LUT5 #(
    .INIT(32'hAAAA8AAA)) 
    i__carry__1_i_4
       (.I0(\GEN_ADDR_32.sig_addr_cntr_msh_reg [8]),
        .I1(sig_sm_halt_reg),
        .I2(sig_input_reg_empty),
        .I3(sig_cmd2pcc_cmd_valid),
        .I4(sig_xfer_calc_err_reg_reg_0),
        .O(\GEN_ADDR_32.sig_addr_cntr_msh_reg[11]_0 [0]));
  LUT5 #(
    .INIT(32'hAAAA8AAA)) 
    i__carry__2_i_1
       (.I0(\GEN_ADDR_32.sig_addr_cntr_msh_reg [15]),
        .I1(sig_sm_halt_reg),
        .I2(sig_input_reg_empty),
        .I3(sig_cmd2pcc_cmd_valid),
        .I4(sig_xfer_calc_err_reg_reg_0),
        .O(\GEN_ADDR_32.sig_addr_cntr_msh_reg[15]_0 [3]));
  LUT5 #(
    .INIT(32'hAAAA8AAA)) 
    i__carry__2_i_2
       (.I0(\GEN_ADDR_32.sig_addr_cntr_msh_reg [14]),
        .I1(sig_sm_halt_reg),
        .I2(sig_input_reg_empty),
        .I3(sig_cmd2pcc_cmd_valid),
        .I4(sig_xfer_calc_err_reg_reg_0),
        .O(\GEN_ADDR_32.sig_addr_cntr_msh_reg[15]_0 [2]));
  LUT5 #(
    .INIT(32'hAAAA8AAA)) 
    i__carry__2_i_3
       (.I0(\GEN_ADDR_32.sig_addr_cntr_msh_reg [13]),
        .I1(sig_sm_halt_reg),
        .I2(sig_input_reg_empty),
        .I3(sig_cmd2pcc_cmd_valid),
        .I4(sig_xfer_calc_err_reg_reg_0),
        .O(\GEN_ADDR_32.sig_addr_cntr_msh_reg[15]_0 [1]));
  LUT5 #(
    .INIT(32'hAAAA8AAA)) 
    i__carry__2_i_4
       (.I0(\GEN_ADDR_32.sig_addr_cntr_msh_reg [12]),
        .I1(sig_sm_halt_reg),
        .I2(sig_input_reg_empty),
        .I3(sig_cmd2pcc_cmd_valid),
        .I4(sig_xfer_calc_err_reg_reg_0),
        .O(\GEN_ADDR_32.sig_addr_cntr_msh_reg[15]_0 [0]));
  LUT6 #(
    .INIT(64'hFFFFEFFF00002000)) 
    i__carry_i_1
       (.I0(Q[14]),
        .I1(sig_xfer_calc_err_reg_reg_0),
        .I2(sig_cmd2pcc_cmd_valid),
        .I3(sig_input_reg_empty),
        .I4(sig_sm_halt_reg),
        .I5(\GEN_ADDR_32.sig_addr_cntr_msh_reg [0]),
        .O(DI));
  LUT6 #(
    .INIT(64'hFFFFEFFF00002000)) 
    i__carry_i_2
       (.I0(Q[17]),
        .I1(sig_xfer_calc_err_reg_reg_0),
        .I2(sig_cmd2pcc_cmd_valid),
        .I3(sig_input_reg_empty),
        .I4(sig_sm_halt_reg),
        .I5(\GEN_ADDR_32.sig_addr_cntr_msh_reg [3]),
        .O(S[3]));
  LUT6 #(
    .INIT(64'hFFFFEFFF00002000)) 
    i__carry_i_3
       (.I0(Q[16]),
        .I1(sig_xfer_calc_err_reg_reg_0),
        .I2(sig_cmd2pcc_cmd_valid),
        .I3(sig_input_reg_empty),
        .I4(sig_sm_halt_reg),
        .I5(\GEN_ADDR_32.sig_addr_cntr_msh_reg [2]),
        .O(S[2]));
  LUT6 #(
    .INIT(64'hFFFFEFFF00002000)) 
    i__carry_i_4
       (.I0(Q[15]),
        .I1(sig_xfer_calc_err_reg_reg_0),
        .I2(sig_cmd2pcc_cmd_valid),
        .I3(sig_input_reg_empty),
        .I4(sig_sm_halt_reg),
        .I5(\GEN_ADDR_32.sig_addr_cntr_msh_reg [1]),
        .O(S[1]));
  LUT6 #(
    .INIT(64'h555555555C555555)) 
    i__carry_i_5
       (.I0(\GEN_ADDR_32.sig_addr_cntr_msh_reg [0]),
        .I1(Q[14]),
        .I2(sig_sm_halt_reg),
        .I3(sig_input_reg_empty),
        .I4(sig_cmd2pcc_cmd_valid),
        .I5(sig_xfer_calc_err_reg_reg_0),
        .O(S[0]));
  CARRY4 sig_btt_cntr0_carry
       (.CI(1'b0),
        .CO({sig_btt_cntr0_carry_n_0,sig_btt_cntr0_carry_n_1,sig_btt_cntr0_carry_n_2,sig_btt_cntr0_carry_n_3}),
        .CYINIT(1'b1),
        .DI(sig_btt_residue_slice[3:0]),
        .O(sig_btt_cntr0[3:0]),
        .S({sig_btt_cntr0_carry_i_1_n_0,sig_btt_cntr0_carry_i_2_n_0,sig_btt_cntr0_carry_i_3_n_0,sig_btt_cntr0_carry_i_4_n_0}));
  CARRY4 sig_btt_cntr0_carry__0
       (.CI(sig_btt_cntr0_carry_n_0),
        .CO({sig_btt_cntr0_carry__0_n_0,sig_btt_cntr0_carry__0_n_1,sig_btt_cntr0_carry__0_n_2,sig_btt_cntr0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(sig_btt_residue_slice[7:4]),
        .O(sig_btt_cntr0[7:4]),
        .S({sig_btt_cntr0_carry__0_i_1_n_0,sig_btt_cntr0_carry__0_i_2_n_0,sig_btt_cntr0_carry__0_i_3_n_0,sig_btt_cntr0_carry__0_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    sig_btt_cntr0_carry__0_i_1
       (.I0(sig_btt_residue_slice[7]),
        .I1(sig_addr_cntr_incr_imreg[7]),
        .O(sig_btt_cntr0_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    sig_btt_cntr0_carry__0_i_2
       (.I0(sig_btt_residue_slice[6]),
        .I1(sig_addr_cntr_incr_imreg[6]),
        .O(sig_btt_cntr0_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    sig_btt_cntr0_carry__0_i_3
       (.I0(sig_btt_residue_slice[5]),
        .I1(sig_addr_cntr_incr_imreg[5]),
        .O(sig_btt_cntr0_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    sig_btt_cntr0_carry__0_i_4
       (.I0(sig_btt_residue_slice[4]),
        .I1(sig_addr_cntr_incr_imreg[4]),
        .O(sig_btt_cntr0_carry__0_i_4_n_0));
  CARRY4 sig_btt_cntr0_carry__1
       (.CI(sig_btt_cntr0_carry__0_n_0),
        .CO({sig_btt_cntr0_carry__1_n_0,sig_btt_cntr0_carry__1_n_1,sig_btt_cntr0_carry__1_n_2,sig_btt_cntr0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({sel0[1:0],sig_btt_residue_slice[9:8]}),
        .O({\sig_btt_cntr_reg[14]_0 [2],sig_btt_cntr0[10],\sig_btt_cntr_reg[14]_0 [1:0]}),
        .S({sig_btt_cntr0_carry__1_i_1_n_0,sig_btt_cntr0_carry__1_i_2_n_0,sig_btt_cntr0_carry__1_i_3_n_0,sig_btt_cntr0_carry__1_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    sig_btt_cntr0_carry__1_i_1
       (.I0(sel0[1]),
        .O(sig_btt_cntr0_carry__1_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    sig_btt_cntr0_carry__1_i_2
       (.I0(sel0[0]),
        .I1(sig_addr_cntr_incr_imreg[10]),
        .O(sig_btt_cntr0_carry__1_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    sig_btt_cntr0_carry__1_i_3
       (.I0(sig_btt_residue_slice[9]),
        .I1(sig_addr_cntr_incr_imreg[9]),
        .O(sig_btt_cntr0_carry__1_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    sig_btt_cntr0_carry__1_i_4
       (.I0(sig_btt_residue_slice[8]),
        .I1(sig_addr_cntr_incr_imreg[8]),
        .O(sig_btt_cntr0_carry__1_i_4_n_0));
  CARRY4 sig_btt_cntr0_carry__2
       (.CI(sig_btt_cntr0_carry__1_n_0),
        .CO({sig_btt_cntr0_carry__2_n_0,sig_btt_cntr0_carry__2_n_1,sig_btt_cntr0_carry__2_n_2,sig_btt_cntr0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI(sel0[5:2]),
        .O({sig_btt_cntr0[15],\sig_btt_cntr_reg[14]_0 [4:3],sig_btt_cntr0[12]}),
        .S({sig_btt_cntr0_carry__2_i_1_n_0,sig_btt_cntr0_carry__2_i_2_n_0,sig_btt_cntr0_carry__2_i_3_n_0,sig_btt_cntr0_carry__2_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    sig_btt_cntr0_carry__2_i_1
       (.I0(sel0[5]),
        .O(sig_btt_cntr0_carry__2_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    sig_btt_cntr0_carry__2_i_2
       (.I0(sel0[4]),
        .O(sig_btt_cntr0_carry__2_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    sig_btt_cntr0_carry__2_i_3
       (.I0(sel0[3]),
        .O(sig_btt_cntr0_carry__2_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    sig_btt_cntr0_carry__2_i_4
       (.I0(sel0[2]),
        .O(sig_btt_cntr0_carry__2_i_4_n_0));
  CARRY4 sig_btt_cntr0_carry__3
       (.CI(sig_btt_cntr0_carry__2_n_0),
        .CO({NLW_sig_btt_cntr0_carry__3_CO_UNCONNECTED[3],sig_btt_cntr0_carry__3_n_1,sig_btt_cntr0_carry__3_n_2,sig_btt_cntr0_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,sel0[8:6]}),
        .O(sig_btt_cntr0[19:16]),
        .S({sig_btt_cntr0_carry__3_i_1_n_0,sig_btt_cntr0_carry__3_i_2_n_0,sig_btt_cntr0_carry__3_i_3_n_0,sig_btt_cntr0_carry__3_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    sig_btt_cntr0_carry__3_i_1
       (.I0(sel0[9]),
        .O(sig_btt_cntr0_carry__3_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    sig_btt_cntr0_carry__3_i_2
       (.I0(sel0[8]),
        .O(sig_btt_cntr0_carry__3_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    sig_btt_cntr0_carry__3_i_3
       (.I0(sel0[7]),
        .O(sig_btt_cntr0_carry__3_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    sig_btt_cntr0_carry__3_i_4
       (.I0(sel0[6]),
        .O(sig_btt_cntr0_carry__3_i_4_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    sig_btt_cntr0_carry_i_1
       (.I0(sig_btt_residue_slice[3]),
        .I1(sig_addr_cntr_incr_imreg[3]),
        .O(sig_btt_cntr0_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    sig_btt_cntr0_carry_i_2
       (.I0(sig_btt_residue_slice[2]),
        .I1(sig_addr_cntr_incr_imreg[2]),
        .O(sig_btt_cntr0_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    sig_btt_cntr0_carry_i_3
       (.I0(sig_btt_residue_slice[1]),
        .I1(sig_addr_cntr_incr_imreg[1]),
        .O(sig_btt_cntr0_carry_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    sig_btt_cntr0_carry_i_4
       (.I0(sig_btt_residue_slice[0]),
        .I1(sig_addr_cntr_incr_imreg[0]),
        .O(sig_btt_cntr0_carry_i_4_n_0));
  LUT5 #(
    .INIT(32'hAAAA8AAA)) 
    \sig_btt_cntr[0]_i_1 
       (.I0(sig_btt_cntr0[0]),
        .I1(sig_sm_halt_reg),
        .I2(sig_input_reg_empty),
        .I3(sig_cmd2pcc_cmd_valid),
        .I4(sig_xfer_calc_err_reg_reg_0),
        .O(\sig_btt_cntr[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA8AAA)) 
    \sig_btt_cntr[10]_i_1 
       (.I0(sig_btt_cntr0[10]),
        .I1(sig_sm_halt_reg),
        .I2(sig_input_reg_empty),
        .I3(sig_cmd2pcc_cmd_valid),
        .I4(sig_xfer_calc_err_reg_reg_0),
        .O(\sig_btt_cntr[10]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA8AAA)) 
    \sig_btt_cntr[12]_i_1 
       (.I0(sig_btt_cntr0[12]),
        .I1(sig_sm_halt_reg),
        .I2(sig_input_reg_empty),
        .I3(sig_cmd2pcc_cmd_valid),
        .I4(sig_xfer_calc_err_reg_reg_0),
        .O(\sig_btt_cntr[12]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA8AAA)) 
    \sig_btt_cntr[15]_i_1 
       (.I0(sig_btt_cntr0[15]),
        .I1(sig_sm_halt_reg),
        .I2(sig_input_reg_empty),
        .I3(sig_cmd2pcc_cmd_valid),
        .I4(sig_xfer_calc_err_reg_reg_0),
        .O(\sig_btt_cntr[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA8AAA)) 
    \sig_btt_cntr[16]_i_1 
       (.I0(sig_btt_cntr0[16]),
        .I1(sig_sm_halt_reg),
        .I2(sig_input_reg_empty),
        .I3(sig_cmd2pcc_cmd_valid),
        .I4(sig_xfer_calc_err_reg_reg_0),
        .O(\sig_btt_cntr[16]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA8AAA)) 
    \sig_btt_cntr[17]_i_1 
       (.I0(sig_btt_cntr0[17]),
        .I1(sig_sm_halt_reg),
        .I2(sig_input_reg_empty),
        .I3(sig_cmd2pcc_cmd_valid),
        .I4(sig_xfer_calc_err_reg_reg_0),
        .O(\sig_btt_cntr[17]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA8AAA)) 
    \sig_btt_cntr[18]_i_1 
       (.I0(sig_btt_cntr0[18]),
        .I1(sig_sm_halt_reg),
        .I2(sig_input_reg_empty),
        .I3(sig_cmd2pcc_cmd_valid),
        .I4(sig_xfer_calc_err_reg_reg_0),
        .O(\sig_btt_cntr[18]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'hAAAA8AAA)) 
    \sig_btt_cntr[19]_i_1 
       (.I0(sig_btt_cntr0[19]),
        .I1(sig_sm_halt_reg),
        .I2(sig_input_reg_empty),
        .I3(sig_cmd2pcc_cmd_valid),
        .I4(sig_xfer_calc_err_reg_reg_0),
        .O(\sig_btt_cntr[19]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA8AAA)) 
    \sig_btt_cntr[1]_i_1 
       (.I0(sig_btt_cntr0[1]),
        .I1(sig_sm_halt_reg),
        .I2(sig_input_reg_empty),
        .I3(sig_cmd2pcc_cmd_valid),
        .I4(sig_xfer_calc_err_reg_reg_0),
        .O(\sig_btt_cntr[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA8AAA)) 
    \sig_btt_cntr[2]_i_1 
       (.I0(sig_btt_cntr0[2]),
        .I1(sig_sm_halt_reg),
        .I2(sig_input_reg_empty),
        .I3(sig_cmd2pcc_cmd_valid),
        .I4(sig_xfer_calc_err_reg_reg_0),
        .O(\sig_btt_cntr[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA8AAA)) 
    \sig_btt_cntr[3]_i_1 
       (.I0(sig_btt_cntr0[3]),
        .I1(sig_sm_halt_reg),
        .I2(sig_input_reg_empty),
        .I3(sig_cmd2pcc_cmd_valid),
        .I4(sig_xfer_calc_err_reg_reg_0),
        .O(\sig_btt_cntr[3]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA8AAA)) 
    \sig_btt_cntr[4]_i_1 
       (.I0(sig_btt_cntr0[4]),
        .I1(sig_sm_halt_reg),
        .I2(sig_input_reg_empty),
        .I3(sig_cmd2pcc_cmd_valid),
        .I4(sig_xfer_calc_err_reg_reg_0),
        .O(\sig_btt_cntr[4]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA8AAA)) 
    \sig_btt_cntr[5]_i_1 
       (.I0(sig_btt_cntr0[5]),
        .I1(sig_sm_halt_reg),
        .I2(sig_input_reg_empty),
        .I3(sig_cmd2pcc_cmd_valid),
        .I4(sig_xfer_calc_err_reg_reg_0),
        .O(\sig_btt_cntr[5]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA8AAA)) 
    \sig_btt_cntr[6]_i_1 
       (.I0(sig_btt_cntr0[6]),
        .I1(sig_sm_halt_reg),
        .I2(sig_input_reg_empty),
        .I3(sig_cmd2pcc_cmd_valid),
        .I4(sig_xfer_calc_err_reg_reg_0),
        .O(\sig_btt_cntr[6]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA8AAA)) 
    \sig_btt_cntr[7]_i_1 
       (.I0(sig_btt_cntr0[7]),
        .I1(sig_sm_halt_reg),
        .I2(sig_input_reg_empty),
        .I3(sig_cmd2pcc_cmd_valid),
        .I4(sig_xfer_calc_err_reg_reg_0),
        .O(\sig_btt_cntr[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_btt_cntr_reg[0] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(\sig_btt_cntr[0]_i_1_n_0 ),
        .Q(sig_btt_residue_slice[0]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \sig_btt_cntr_reg[10] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(\sig_btt_cntr[10]_i_1_n_0 ),
        .Q(sel0[0]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \sig_btt_cntr_reg[11] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(sig_cmd_mstrd_req_reg[2]),
        .Q(sel0[1]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \sig_btt_cntr_reg[12] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(\sig_btt_cntr[12]_i_1_n_0 ),
        .Q(sel0[2]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \sig_btt_cntr_reg[13] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(sig_cmd_mstrd_req_reg[3]),
        .Q(sel0[3]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \sig_btt_cntr_reg[14] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(sig_cmd_mstrd_req_reg[4]),
        .Q(sel0[4]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \sig_btt_cntr_reg[15] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(\sig_btt_cntr[15]_i_1_n_0 ),
        .Q(sel0[5]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \sig_btt_cntr_reg[16] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(\sig_btt_cntr[16]_i_1_n_0 ),
        .Q(sel0[6]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \sig_btt_cntr_reg[17] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(\sig_btt_cntr[17]_i_1_n_0 ),
        .Q(sel0[7]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \sig_btt_cntr_reg[18] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(\sig_btt_cntr[18]_i_1_n_0 ),
        .Q(sel0[8]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \sig_btt_cntr_reg[19] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(\sig_btt_cntr[19]_i_1_n_0 ),
        .Q(sel0[9]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \sig_btt_cntr_reg[1] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(\sig_btt_cntr[1]_i_1_n_0 ),
        .Q(sig_btt_residue_slice[1]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \sig_btt_cntr_reg[2] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(\sig_btt_cntr[2]_i_1_n_0 ),
        .Q(sig_btt_residue_slice[2]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \sig_btt_cntr_reg[3] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(\sig_btt_cntr[3]_i_1_n_0 ),
        .Q(sig_btt_residue_slice[3]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \sig_btt_cntr_reg[4] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(\sig_btt_cntr[4]_i_1_n_0 ),
        .Q(sig_btt_residue_slice[4]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \sig_btt_cntr_reg[5] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(\sig_btt_cntr[5]_i_1_n_0 ),
        .Q(sig_btt_residue_slice[5]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \sig_btt_cntr_reg[6] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(\sig_btt_cntr[6]_i_1_n_0 ),
        .Q(sig_btt_residue_slice[6]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \sig_btt_cntr_reg[7] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(\sig_btt_cntr[7]_i_1_n_0 ),
        .Q(sig_btt_residue_slice[7]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \sig_btt_cntr_reg[8] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(sig_cmd_mstrd_req_reg[0]),
        .Q(sig_btt_residue_slice[8]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \sig_btt_cntr_reg[9] 
       (.C(m_axi_aclk),
        .CE(\GEN_ADDR_32.sig_addr_cntr_lsh[15]_i_1_n_0 ),
        .D(sig_cmd_mstrd_req_reg[1]),
        .Q(sig_btt_residue_slice[9]),
        .R(out));
  CARRY4 sig_btt_eq_b2mbaa1_carry
       (.CI(1'b0),
        .CO({sig_btt_eq_b2mbaa1,sig_btt_eq_b2mbaa1_carry_n_1,sig_btt_eq_b2mbaa1_carry_n_2,sig_btt_eq_b2mbaa1_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_sig_btt_eq_b2mbaa1_carry_O_UNCONNECTED[3:0]),
        .S({sig_btt_eq_b2mbaa1_carry_i_1_n_0,sig_btt_eq_b2mbaa1_carry_i_2_n_0,sig_btt_eq_b2mbaa1_carry_i_3_n_0,sig_btt_eq_b2mbaa1_carry_i_4_n_0}));
  LUT6 #(
    .INIT(64'h55555556AAAAAAA8)) 
    sig_btt_eq_b2mbaa1_carry_i_1
       (.I0(sig_xfer_address[9]),
        .I1(sig_xfer_address[8]),
        .I2(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[7]_i_2_n_0 ),
        .I3(sig_xfer_address[6]),
        .I4(sig_xfer_address[7]),
        .I5(sig_btt_residue_slice[9]),
        .O(sig_btt_eq_b2mbaa1_carry_i_1_n_0));
  LUT3 #(
    .INIT(8'h28)) 
    sig_btt_eq_b2mbaa1_carry_i_2
       (.I0(sig_btt_eq_b2mbaa1_carry_i_5_n_0),
        .I1(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[8]_i_2_n_0 ),
        .I2(sig_btt_residue_slice[8]),
        .O(sig_btt_eq_b2mbaa1_carry_i_2_n_0));
  LUT6 #(
    .INIT(64'h000000005556AAA9)) 
    sig_btt_eq_b2mbaa1_carry_i_3
       (.I0(sig_xfer_address[3]),
        .I1(sig_xfer_address[0]),
        .I2(sig_xfer_address[1]),
        .I3(sig_xfer_address[2]),
        .I4(sig_btt_residue_slice[3]),
        .I5(sig_btt_eq_b2mbaa1_carry_i_6_n_0),
        .O(sig_btt_eq_b2mbaa1_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h0208041020804001)) 
    sig_btt_eq_b2mbaa1_carry_i_4
       (.I0(sig_btt_residue_slice[0]),
        .I1(sig_btt_residue_slice[1]),
        .I2(sig_xfer_address[2]),
        .I3(sig_xfer_address[1]),
        .I4(sig_xfer_address[0]),
        .I5(sig_btt_residue_slice[2]),
        .O(sig_btt_eq_b2mbaa1_carry_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'h06606009)) 
    sig_btt_eq_b2mbaa1_carry_i_5
       (.I0(sig_xfer_address[7]),
        .I1(sig_btt_residue_slice[7]),
        .I2(sig_xfer_address[6]),
        .I3(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[7]_i_2_n_0 ),
        .I4(sig_btt_residue_slice[6]),
        .O(sig_btt_eq_b2mbaa1_carry_i_5_n_0));
  LUT5 #(
    .INIT(32'hF99F9FF6)) 
    sig_btt_eq_b2mbaa1_carry_i_6
       (.I0(sig_xfer_address[5]),
        .I1(sig_btt_residue_slice[5]),
        .I2(sig_xfer_address[4]),
        .I3(sig_btt_lt_b2mbaa1_carry_i_9_n_0),
        .I4(sig_btt_residue_slice[4]),
        .O(sig_btt_eq_b2mbaa1_carry_i_6_n_0));
  CARRY4 sig_btt_lt_b2mbaa1_carry
       (.CI(1'b0),
        .CO({sig_btt_lt_b2mbaa1_carry_n_0,sig_btt_lt_b2mbaa1_carry_n_1,sig_btt_lt_b2mbaa1_carry_n_2,sig_btt_lt_b2mbaa1_carry_n_3}),
        .CYINIT(1'b0),
        .DI({sig_btt_lt_b2mbaa1_carry_i_1_n_0,sig_btt_lt_b2mbaa1_carry_i_2_n_0,sig_btt_lt_b2mbaa1_carry_i_3_n_0,sig_btt_lt_b2mbaa1_carry_i_4_n_0}),
        .O(NLW_sig_btt_lt_b2mbaa1_carry_O_UNCONNECTED[3:0]),
        .S({sig_btt_lt_b2mbaa1_carry_i_5_n_0,sig_btt_lt_b2mbaa1_carry_i_6_n_0,sig_btt_lt_b2mbaa1_carry_i_7_n_0,sig_btt_lt_b2mbaa1_carry_i_8_n_0}));
  CARRY4 sig_btt_lt_b2mbaa1_carry__0
       (.CI(sig_btt_lt_b2mbaa1_carry_n_0),
        .CO({NLW_sig_btt_lt_b2mbaa1_carry__0_CO_UNCONNECTED[3:2],sig_btt_lt_b2mbaa1,sig_btt_lt_b2mbaa1_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,sig_btt_lt_b2mbaa1_carry__0_i_1_n_0,sig_btt_lt_b2mbaa1_carry__0_i_2_n_0}),
        .O(NLW_sig_btt_lt_b2mbaa1_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,sig_btt_lt_b2mbaa1_carry__0_i_3_n_0,sig_btt_lt_b2mbaa1_carry__0_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    sig_btt_lt_b2mbaa1_carry__0_i_1
       (.I0(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_3_n_0 ),
        .O(sig_btt_lt_b2mbaa1_carry__0_i_1_n_0));
  LUT5 #(
    .INIT(32'h045145D3)) 
    sig_btt_lt_b2mbaa1_carry__0_i_2
       (.I0(sig_btt_residue_slice[9]),
        .I1(sig_btt_lt_b2mbaa1_carry__0_i_5_n_0),
        .I2(sig_xfer_address[8]),
        .I3(sig_xfer_address[9]),
        .I4(sig_btt_residue_slice[8]),
        .O(sig_btt_lt_b2mbaa1_carry__0_i_2_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    sig_btt_lt_b2mbaa1_carry__0_i_3
       (.I0(sig_xfer_address[9]),
        .I1(sig_xfer_address[8]),
        .I2(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[7]_i_2_n_0 ),
        .I3(sig_xfer_address[6]),
        .I4(sig_xfer_address[7]),
        .O(sig_btt_lt_b2mbaa1_carry__0_i_3_n_0));
  LUT5 #(
    .INIT(32'h60060960)) 
    sig_btt_lt_b2mbaa1_carry__0_i_4
       (.I0(sig_xfer_address[9]),
        .I1(sig_btt_residue_slice[9]),
        .I2(sig_xfer_address[8]),
        .I3(sig_btt_lt_b2mbaa1_carry__0_i_5_n_0),
        .I4(sig_btt_residue_slice[8]),
        .O(sig_btt_lt_b2mbaa1_carry__0_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'h01)) 
    sig_btt_lt_b2mbaa1_carry__0_i_5
       (.I0(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[7]_i_2_n_0 ),
        .I1(sig_xfer_address[6]),
        .I2(sig_xfer_address[7]),
        .O(sig_btt_lt_b2mbaa1_carry__0_i_5_n_0));
  LUT5 #(
    .INIT(32'h11141774)) 
    sig_btt_lt_b2mbaa1_carry_i_1
       (.I0(sig_btt_residue_slice[7]),
        .I1(sig_xfer_address[7]),
        .I2(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[7]_i_2_n_0 ),
        .I3(sig_xfer_address[6]),
        .I4(sig_btt_residue_slice[6]),
        .O(sig_btt_lt_b2mbaa1_carry_i_1_n_0));
  LUT5 #(
    .INIT(32'h001706EE)) 
    sig_btt_lt_b2mbaa1_carry_i_2
       (.I0(sig_btt_lt_b2mbaa1_carry_i_9_n_0),
        .I1(sig_xfer_address[4]),
        .I2(sig_btt_residue_slice[4]),
        .I3(sig_btt_residue_slice[5]),
        .I4(sig_xfer_address[5]),
        .O(sig_btt_lt_b2mbaa1_carry_i_2_n_0));
  LUT6 #(
    .INIT(64'h000155541115777C)) 
    sig_btt_lt_b2mbaa1_carry_i_3
       (.I0(sig_btt_residue_slice[3]),
        .I1(sig_xfer_address[2]),
        .I2(sig_xfer_address[1]),
        .I3(sig_xfer_address[0]),
        .I4(sig_xfer_address[3]),
        .I5(sig_btt_residue_slice[2]),
        .O(sig_btt_lt_b2mbaa1_carry_i_3_n_0));
  LUT4 #(
    .INIT(16'h1474)) 
    sig_btt_lt_b2mbaa1_carry_i_4
       (.I0(sig_btt_residue_slice[1]),
        .I1(sig_xfer_address[1]),
        .I2(sig_xfer_address[0]),
        .I3(sig_btt_residue_slice[0]),
        .O(sig_btt_lt_b2mbaa1_carry_i_4_n_0));
  LUT5 #(
    .INIT(32'h06606009)) 
    sig_btt_lt_b2mbaa1_carry_i_5
       (.I0(sig_xfer_address[7]),
        .I1(sig_btt_residue_slice[7]),
        .I2(sig_xfer_address[6]),
        .I3(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[7]_i_2_n_0 ),
        .I4(sig_btt_residue_slice[6]),
        .O(sig_btt_lt_b2mbaa1_carry_i_5_n_0));
  LUT5 #(
    .INIT(32'h01686801)) 
    sig_btt_lt_b2mbaa1_carry_i_6
       (.I0(sig_btt_residue_slice[4]),
        .I1(sig_btt_lt_b2mbaa1_carry_i_9_n_0),
        .I2(sig_xfer_address[4]),
        .I3(sig_btt_residue_slice[5]),
        .I4(sig_xfer_address[5]),
        .O(sig_btt_lt_b2mbaa1_carry_i_6_n_0));
  LUT6 #(
    .INIT(64'h0606066060606009)) 
    sig_btt_lt_b2mbaa1_carry_i_7
       (.I0(sig_xfer_address[3]),
        .I1(sig_btt_residue_slice[3]),
        .I2(sig_xfer_address[2]),
        .I3(sig_xfer_address[1]),
        .I4(sig_xfer_address[0]),
        .I5(sig_btt_residue_slice[2]),
        .O(sig_btt_lt_b2mbaa1_carry_i_7_n_0));
  LUT4 #(
    .INIT(16'h6009)) 
    sig_btt_lt_b2mbaa1_carry_i_8
       (.I0(sig_xfer_address[1]),
        .I1(sig_btt_residue_slice[1]),
        .I2(sig_btt_residue_slice[0]),
        .I3(sig_xfer_address[0]),
        .O(sig_btt_lt_b2mbaa1_carry_i_8_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    sig_btt_lt_b2mbaa1_carry_i_9
       (.I0(sig_xfer_address[3]),
        .I1(sig_xfer_address[0]),
        .I2(sig_xfer_address[1]),
        .I3(sig_xfer_address[2]),
        .O(sig_btt_lt_b2mbaa1_carry_i_9_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \sig_byte_change_minus1/i_ 
       (.I0(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[4] ),
        .I1(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[2] ),
        .I2(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[0] ),
        .I3(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[1] ),
        .I4(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[3] ),
        .I5(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[5] ),
        .O(\sig_byte_change_minus1/i__n_0 ));
  LUT4 #(
    .INIT(16'hFF80)) 
    sig_calc_error_pushed_i_1
       (.I0(sig_xfer_calc_err_reg_reg_0),
        .I1(sig_ld_xfer_reg),
        .I2(sig_xfer_reg_empty),
        .I3(sig_calc_error_pushed),
        .O(sig_calc_error_pushed_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sig_calc_error_pushed_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_calc_error_pushed_i_1_n_0),
        .Q(sig_calc_error_pushed),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    sig_calc_error_reg_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(\sig_cmd_mst_length_reg[13] ),
        .Q(sig_xfer_calc_err_reg_reg_0),
        .R(out));
  LUT6 #(
    .INIT(64'h000000002EEEEEEE)) 
    sig_cmd2addr_valid_i_1
       (.I0(sig_push_xfer_reg15_out),
        .I1(sig_pcc2addr_cmd_valid),
        .I2(sig_cmd2all_doing_read),
        .I3(sig_addr2stat_cmd_fifo_empty),
        .I4(sig_llink2rd_allow_addr_req),
        .I5(out),
        .O(sig_cmd2addr_valid_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sig_cmd2addr_valid_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_cmd2addr_valid_i_1_n_0),
        .Q(sig_pcc2addr_cmd_valid),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h000000EA)) 
    sig_cmd2data_valid_i_1
       (.I0(sig_pcc2data_cmd_valid),
        .I1(sig_xfer_reg_empty),
        .I2(sig_ld_xfer_reg),
        .I3(sig_rdc2pcc_cmd_ready),
        .I4(out),
        .O(sig_cmd2data_valid_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sig_cmd2data_valid_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_cmd2data_valid_i_1_n_0),
        .Q(sig_pcc2data_cmd_valid),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h00000080)) 
    sig_cmd2dre_valid_i_1
       (.I0(sig_ld_xfer_reg),
        .I1(sig_xfer_reg_empty),
        .I2(sig_first_xfer),
        .I3(sig_cmd2dre_valid_reg_n_0),
        .I4(out),
        .O(sig_cmd2dre_valid_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sig_cmd2dre_valid_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_cmd2dre_valid_i_1_n_0),
        .Q(sig_cmd2dre_valid_reg_n_0),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hAAAAAAAAABAAAAAA)) 
    sig_cmd_full_reg_i_1
       (.I0(sig_cmd_reset_reg_reg),
        .I1(sig_push_cmd_reg),
        .I2(sig_calc_error_pushed),
        .I3(sig_cmd2pcc_cmd_valid),
        .I4(sig_input_reg_empty),
        .I5(sig_sm_halt_reg),
        .O(SR));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT4 #(
    .INIT(16'h0040)) 
    sig_doing_read_reg_i_2
       (.I0(sig_calc_error_pushed),
        .I1(sig_cmd2pcc_cmd_valid),
        .I2(sig_input_reg_empty),
        .I3(sig_sm_halt_reg),
        .O(sig_pcc_taking_command));
  LUT5 #(
    .INIT(32'h0000000E)) 
    sig_input_burst_type_reg_i_1
       (.I0(sig_input_burst_type_reg),
        .I1(\GEN_ADDR_32.sig_addr_cntr_msh_reg[0]_0 ),
        .I2(out),
        .I3(sig_sm_pop_input_reg),
        .I4(sig_calc_error_pushed),
        .O(sig_input_burst_type_reg_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sig_input_burst_type_reg_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_input_burst_type_reg_i_1_n_0),
        .Q(sig_input_burst_type_reg),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h0000000E)) 
    sig_input_eof_reg_i_1
       (.I0(sig_input_eof_reg),
        .I1(\GEN_ADDR_32.sig_addr_cntr_msh_reg[0]_0 ),
        .I2(out),
        .I3(sig_sm_pop_input_reg),
        .I4(sig_calc_error_pushed),
        .O(sig_input_eof_reg_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sig_input_eof_reg_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_input_eof_reg_i_1_n_0),
        .Q(sig_input_eof_reg),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFFFFFFF2)) 
    sig_input_reg_empty_i_1
       (.I0(sig_input_reg_empty),
        .I1(\GEN_ADDR_32.sig_addr_cntr_msh_reg[0]_0 ),
        .I2(out),
        .I3(sig_sm_pop_input_reg),
        .I4(sig_calc_error_pushed),
        .O(sig_input_reg_empty_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sig_input_reg_empty_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_input_reg_empty_i_1_n_0),
        .Q(sig_input_reg_empty),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    sig_last_dbeat_i_4
       (.I0(\sig_next_len_reg_reg[7] [0]),
        .I1(\sig_next_len_reg_reg[7] [3]),
        .I2(\sig_next_len_reg_reg[7] [1]),
        .I3(sig_last_dbeat_i_7_n_0),
        .O(sig_last_dbeat_reg));
  LUT4 #(
    .INIT(16'hFFFE)) 
    sig_last_dbeat_i_7
       (.I0(\sig_next_len_reg_reg[7] [7]),
        .I1(\sig_next_len_reg_reg[7] [2]),
        .I2(\sig_next_len_reg_reg[7] [5]),
        .I3(\sig_next_len_reg_reg[7] [4]),
        .O(sig_last_dbeat_i_7_n_0));
  LUT6 #(
    .INIT(64'h0000000000FF4040)) 
    sig_ld_xfer_reg_i_1
       (.I0(sig_pcc_sm_state[2]),
        .I1(sig_pcc_sm_state[0]),
        .I2(sig_pcc_sm_state[1]),
        .I3(sig_xfer_reg_empty),
        .I4(sig_ld_xfer_reg),
        .I5(out),
        .O(sig_ld_xfer_reg_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sig_ld_xfer_reg_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_ld_xfer_reg_i_1_n_0),
        .Q(sig_ld_xfer_reg),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0000000000002AEA)) 
    sig_parent_done_i_1
       (.I0(sig_parent_done),
        .I1(sig_ld_xfer_reg),
        .I2(sig_xfer_reg_empty),
        .I3(sig_xfer_is_seq_reg_i_1_n_0),
        .I4(\GEN_ADDR_32.sig_addr_cntr_msh_reg[0]_0 ),
        .I5(out),
        .O(sig_parent_done_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sig_parent_done_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_parent_done_i_1_n_0),
        .Q(sig_parent_done),
        .R(1'b0));
  LUT4 #(
    .INIT(16'h8000)) 
    sig_rd_addr_valid_reg_i_2
       (.I0(sig_pcc2addr_cmd_valid),
        .I1(sig_cmd2all_doing_read),
        .I2(sig_addr2stat_cmd_fifo_empty),
        .I3(sig_llink2rd_allow_addr_req),
        .O(sig_push_addr_reg1_out));
  LUT4 #(
    .INIT(16'h3803)) 
    sig_sm_halt_reg_i_1
       (.I0(sig_calc_error_pushed),
        .I1(sig_pcc_sm_state[0]),
        .I2(sig_pcc_sm_state[1]),
        .I3(sig_pcc_sm_state[2]),
        .O(sig_sm_halt_ns));
  FDSE #(
    .INIT(1'b0)) 
    sig_sm_halt_reg_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_sm_halt_ns),
        .Q(sig_sm_halt_reg),
        .S(out));
  LUT6 #(
    .INIT(64'h0040004000404440)) 
    sig_sm_ld_calc1_reg_i_1
       (.I0(sig_pcc_sm_state[1]),
        .I1(sig_pcc_sm_state[0]),
        .I2(\GEN_ADDR_32.sig_addr_cntr_msh_reg[0]_0 ),
        .I3(sig_pcc_sm_state[2]),
        .I4(sig_calc_error_pushed),
        .I5(sig_parent_done),
        .O(sig_sm_ld_calc1_reg_ns));
  FDRE #(
    .INIT(1'b0)) 
    sig_sm_ld_calc1_reg_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_sm_ld_calc1_reg_ns),
        .Q(sig_sm_ld_calc1_reg),
        .R(out));
  LUT3 #(
    .INIT(8'h02)) 
    sig_sm_ld_calc2_reg_i_1
       (.I0(sig_pcc_sm_state[1]),
        .I1(sig_pcc_sm_state[2]),
        .I2(sig_pcc_sm_state[0]),
        .O(sig_sm_ld_calc2_reg_ns));
  FDRE #(
    .INIT(1'b0)) 
    sig_sm_ld_calc2_reg_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_sm_ld_calc2_reg_ns),
        .Q(sig_sm_ld_calc2_reg),
        .R(out));
  LUT5 #(
    .INIT(32'h00002000)) 
    sig_sm_pop_input_reg_i_1
       (.I0(sig_pcc_sm_state[2]),
        .I1(sig_pcc_sm_state[1]),
        .I2(sig_pcc_sm_state[0]),
        .I3(sig_parent_done),
        .I4(sig_calc_error_pushed),
        .O(sig_sm_pop_input_reg_ns));
  FDRE #(
    .INIT(1'b0)) 
    sig_sm_pop_input_reg_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_sm_pop_input_reg_ns),
        .Q(sig_sm_pop_input_reg),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_addr_reg_reg[0] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(sig_xfer_address[0]),
        .Q(\sig_next_addr_reg_reg[31] [0]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_addr_reg_reg[10] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(sig_xfer_address[10]),
        .Q(\sig_next_addr_reg_reg[31] [10]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_addr_reg_reg[11] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(sig_xfer_address[11]),
        .Q(\sig_next_addr_reg_reg[31] [11]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_addr_reg_reg[12] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(sig_xfer_address[12]),
        .Q(\sig_next_addr_reg_reg[31] [12]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_addr_reg_reg[13] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(sig_xfer_address[13]),
        .Q(\sig_next_addr_reg_reg[31] [13]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_addr_reg_reg[14] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(sig_xfer_address[14]),
        .Q(\sig_next_addr_reg_reg[31] [14]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_addr_reg_reg[15] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(p_1_in2_in),
        .Q(\sig_next_addr_reg_reg[31] [15]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_addr_reg_reg[16] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(\GEN_ADDR_32.sig_addr_cntr_msh_reg [0]),
        .Q(\sig_next_addr_reg_reg[31] [16]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_addr_reg_reg[17] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(\GEN_ADDR_32.sig_addr_cntr_msh_reg [1]),
        .Q(\sig_next_addr_reg_reg[31] [17]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_addr_reg_reg[18] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(\GEN_ADDR_32.sig_addr_cntr_msh_reg [2]),
        .Q(\sig_next_addr_reg_reg[31] [18]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_addr_reg_reg[19] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(\GEN_ADDR_32.sig_addr_cntr_msh_reg [3]),
        .Q(\sig_next_addr_reg_reg[31] [19]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_addr_reg_reg[1] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(sig_xfer_address[1]),
        .Q(\sig_next_addr_reg_reg[31] [1]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_addr_reg_reg[20] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(\GEN_ADDR_32.sig_addr_cntr_msh_reg [4]),
        .Q(\sig_next_addr_reg_reg[31] [20]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_addr_reg_reg[21] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(\GEN_ADDR_32.sig_addr_cntr_msh_reg [5]),
        .Q(\sig_next_addr_reg_reg[31] [21]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_addr_reg_reg[22] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(\GEN_ADDR_32.sig_addr_cntr_msh_reg [6]),
        .Q(\sig_next_addr_reg_reg[31] [22]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_addr_reg_reg[23] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(\GEN_ADDR_32.sig_addr_cntr_msh_reg [7]),
        .Q(\sig_next_addr_reg_reg[31] [23]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_addr_reg_reg[24] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(\GEN_ADDR_32.sig_addr_cntr_msh_reg [8]),
        .Q(\sig_next_addr_reg_reg[31] [24]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_addr_reg_reg[25] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(\GEN_ADDR_32.sig_addr_cntr_msh_reg [9]),
        .Q(\sig_next_addr_reg_reg[31] [25]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_addr_reg_reg[26] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(\GEN_ADDR_32.sig_addr_cntr_msh_reg [10]),
        .Q(\sig_next_addr_reg_reg[31] [26]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_addr_reg_reg[27] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(\GEN_ADDR_32.sig_addr_cntr_msh_reg [11]),
        .Q(\sig_next_addr_reg_reg[31] [27]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_addr_reg_reg[28] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(\GEN_ADDR_32.sig_addr_cntr_msh_reg [12]),
        .Q(\sig_next_addr_reg_reg[31] [28]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_addr_reg_reg[29] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(\GEN_ADDR_32.sig_addr_cntr_msh_reg [13]),
        .Q(\sig_next_addr_reg_reg[31] [29]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_addr_reg_reg[2] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(sig_xfer_address[2]),
        .Q(\sig_next_addr_reg_reg[31] [2]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_addr_reg_reg[30] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(\GEN_ADDR_32.sig_addr_cntr_msh_reg [14]),
        .Q(\sig_next_addr_reg_reg[31] [30]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_addr_reg_reg[31] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(\GEN_ADDR_32.sig_addr_cntr_msh_reg [15]),
        .Q(\sig_next_addr_reg_reg[31] [31]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_addr_reg_reg[3] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(sig_xfer_address[3]),
        .Q(\sig_next_addr_reg_reg[31] [3]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_addr_reg_reg[4] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(sig_xfer_address[4]),
        .Q(\sig_next_addr_reg_reg[31] [4]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_addr_reg_reg[5] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(sig_xfer_address[5]),
        .Q(\sig_next_addr_reg_reg[31] [5]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_addr_reg_reg[6] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(sig_xfer_address[6]),
        .Q(\sig_next_addr_reg_reg[31] [6]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_addr_reg_reg[7] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(sig_xfer_address[7]),
        .Q(\sig_next_addr_reg_reg[31] [7]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_addr_reg_reg[8] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(sig_xfer_address[8]),
        .Q(\sig_next_addr_reg_reg[31] [8]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_addr_reg_reg[9] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(sig_xfer_address[9]),
        .Q(\sig_next_addr_reg_reg[31] [9]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    sig_xfer_calc_err_reg_reg
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(sig_xfer_calc_err_reg_reg_0),
        .Q(sig_pcc2data_calc_error),
        .R(sig_xfer_addr_reg0));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'hB)) 
    sig_xfer_cmd_cmplt_reg_i_1
       (.I0(sig_xfer_calc_err_reg_reg_0),
        .I1(sig_xfer_is_seq_reg_i_1_n_0),
        .O(sig_xfer_cmd_cmplt_reg_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sig_xfer_cmd_cmplt_reg_reg
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(sig_xfer_cmd_cmplt_reg_i_1_n_0),
        .Q(sig_pcc2data_cmd_cmplt),
        .R(sig_xfer_addr_reg0));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'h2)) 
    sig_xfer_eof_reg_i_1
       (.I0(sig_input_eof_reg),
        .I1(sig_xfer_is_seq_reg_i_1_n_0),
        .O(sig_xfer_eof_reg0));
  FDRE #(
    .INIT(1'b0)) 
    sig_xfer_eof_reg_reg
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(sig_xfer_eof_reg0),
        .Q(sig_pcc2data_eof),
        .R(sig_xfer_addr_reg0));
  LUT6 #(
    .INIT(64'hF0F0F0FFEEEEEEEE)) 
    sig_xfer_is_seq_reg_i_1
       (.I0(sig_xfer_is_seq_reg_i_2_n_0),
        .I1(\GEN_ADDR_32.sig_addr_cntr_incr_imreg[10]_i_3_n_0 ),
        .I2(sig_xfer_is_seq_reg_i_3_n_0),
        .I3(sig_btt_eq_b2mbaa1),
        .I4(sig_btt_lt_b2mbaa1),
        .I5(sig_xfer_is_seq_reg_i_4_n_0),
        .O(sig_xfer_is_seq_reg_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFEFFFF)) 
    sig_xfer_is_seq_reg_i_2
       (.I0(sel0[1]),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(sig_xfer_is_seq_reg_i_5_n_0),
        .I4(sel0[0]),
        .I5(sig_xfer_is_seq_reg_i_6_n_0),
        .O(sig_xfer_is_seq_reg_i_2_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    sig_xfer_is_seq_reg_i_3
       (.I0(sel0[1]),
        .I1(sel0[3]),
        .I2(sel0[2]),
        .I3(sig_xfer_is_seq_reg_i_5_n_0),
        .I4(sig_xfer_is_seq_reg_i_6_n_0),
        .I5(sel0[0]),
        .O(sig_xfer_is_seq_reg_i_3_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    sig_xfer_is_seq_reg_i_4
       (.I0(sig_btt_residue_slice[2]),
        .I1(sig_btt_residue_slice[8]),
        .I2(sig_btt_residue_slice[0]),
        .I3(sig_btt_residue_slice[3]),
        .I4(sig_xfer_is_seq_reg_i_7_n_0),
        .O(sig_xfer_is_seq_reg_i_4_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    sig_xfer_is_seq_reg_i_5
       (.I0(sel0[7]),
        .I1(sel0[6]),
        .I2(sel0[5]),
        .I3(sel0[4]),
        .O(sig_xfer_is_seq_reg_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'hE)) 
    sig_xfer_is_seq_reg_i_6
       (.I0(sel0[9]),
        .I1(sel0[8]),
        .O(sig_xfer_is_seq_reg_i_6_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    sig_xfer_is_seq_reg_i_7
       (.I0(sig_btt_residue_slice[7]),
        .I1(sig_btt_residue_slice[1]),
        .I2(sig_btt_residue_slice[6]),
        .I3(sig_btt_residue_slice[9]),
        .I4(sig_btt_residue_slice[4]),
        .I5(sig_btt_residue_slice[5]),
        .O(sig_xfer_is_seq_reg_i_7_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sig_xfer_is_seq_reg_reg
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(sig_xfer_is_seq_reg_i_1_n_0),
        .Q(sig_pcc2data_sequential),
        .R(sig_xfer_addr_reg0));
  LUT3 #(
    .INIT(8'hE1)) 
    \sig_xfer_len_reg[0]_i_1 
       (.I0(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[1] ),
        .I1(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[0] ),
        .I2(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[2] ),
        .O(\sig_xfer_len_reg[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hFE01)) 
    \sig_xfer_len_reg[1]_i_1 
       (.I0(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[2] ),
        .I1(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[0] ),
        .I2(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[1] ),
        .I3(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[3] ),
        .O(\sig_xfer_len_reg[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'hFFFE0001)) 
    \sig_xfer_len_reg[2]_i_1 
       (.I0(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[3] ),
        .I1(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[1] ),
        .I2(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[0] ),
        .I3(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[2] ),
        .I4(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[4] ),
        .O(\sig_xfer_len_reg[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFE00000001)) 
    \sig_xfer_len_reg[3]_i_1 
       (.I0(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[4] ),
        .I1(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[2] ),
        .I2(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[0] ),
        .I3(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[1] ),
        .I4(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[3] ),
        .I5(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[5] ),
        .O(\sig_xfer_len_reg[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \sig_xfer_len_reg[4]_i_1 
       (.I0(\sig_byte_change_minus1/i__n_0 ),
        .I1(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[6] ),
        .O(\sig_xfer_len_reg[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hE1)) 
    \sig_xfer_len_reg[5]_i_1 
       (.I0(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[6] ),
        .I1(\sig_byte_change_minus1/i__n_0 ),
        .I2(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[7] ),
        .O(\sig_xfer_len_reg[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'hFE01)) 
    \sig_xfer_len_reg[6]_i_1 
       (.I0(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[7] ),
        .I1(\sig_byte_change_minus1/i__n_0 ),
        .I2(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[6] ),
        .I3(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[8] ),
        .O(\sig_xfer_len_reg[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'hFFFE0001)) 
    \sig_xfer_len_reg[7]_i_1 
       (.I0(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[8] ),
        .I1(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[6] ),
        .I2(\sig_byte_change_minus1/i__n_0 ),
        .I3(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[7] ),
        .I4(\GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg_n_0_[9] ),
        .O(\sig_xfer_len_reg[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_len_reg_reg[0] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(\sig_xfer_len_reg[0]_i_1_n_0 ),
        .Q(\sig_next_len_reg_reg[7] [0]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_len_reg_reg[1] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(\sig_xfer_len_reg[1]_i_1_n_0 ),
        .Q(\sig_next_len_reg_reg[7] [1]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_len_reg_reg[2] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(\sig_xfer_len_reg[2]_i_1_n_0 ),
        .Q(\sig_next_len_reg_reg[7] [2]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_len_reg_reg[3] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(\sig_xfer_len_reg[3]_i_1_n_0 ),
        .Q(\sig_next_len_reg_reg[7] [3]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_len_reg_reg[4] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(\sig_xfer_len_reg[4]_i_1_n_0 ),
        .Q(\sig_next_len_reg_reg[7] [4]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_len_reg_reg[5] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(\sig_xfer_len_reg[5]_i_1_n_0 ),
        .Q(\sig_next_len_reg_reg[7] [5]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_len_reg_reg[6] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(\sig_xfer_len_reg[6]_i_1_n_0 ),
        .Q(\sig_next_len_reg_reg[7] [6]),
        .R(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_xfer_len_reg_reg[7] 
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(\sig_xfer_len_reg[7]_i_1_n_0 ),
        .Q(\sig_next_len_reg_reg[7] [7]),
        .R(sig_xfer_addr_reg0));
  LUT6 #(
    .INIT(64'hBABBBABBBABBBABA)) 
    sig_xfer_reg_empty_i_1
       (.I0(out),
        .I1(sig_xfer_reg_empty_i_3_n_0),
        .I2(sig_rdc2pcc_cmd_ready),
        .I3(sig_pcc2data_cmd_valid),
        .I4(sig_pcc2addr_cmd_valid),
        .I5(sig_cmd2dre_valid_reg_n_0),
        .O(sig_xfer_addr_reg0));
  LUT2 #(
    .INIT(4'h8)) 
    sig_xfer_reg_empty_i_2
       (.I0(sig_xfer_reg_empty),
        .I1(sig_ld_xfer_reg),
        .O(sig_push_xfer_reg15_out));
  LUT6 #(
    .INIT(64'h88F8F8F8F8F8F8F8)) 
    sig_xfer_reg_empty_i_3
       (.I0(sig_ld_xfer_reg),
        .I1(sig_xfer_reg_empty),
        .I2(sig_pcc2addr_cmd_valid),
        .I3(sig_cmd2all_doing_read),
        .I4(sig_addr2stat_cmd_fifo_empty),
        .I5(sig_llink2rd_allow_addr_req),
        .O(sig_xfer_reg_empty_i_3_n_0));
  FDSE #(
    .INIT(1'b0)) 
    sig_xfer_reg_empty_reg
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(1'b0),
        .Q(sig_xfer_reg_empty),
        .S(sig_xfer_addr_reg0));
  FDRE #(
    .INIT(1'b0)) 
    sig_xfer_type_reg_reg
       (.C(m_axi_aclk),
        .CE(sig_push_xfer_reg15_out),
        .D(sig_input_burst_type_reg),
        .Q(sig_pcc2addr_burst),
        .R(sig_xfer_addr_reg0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axi_master_burst_rd_llink
   (sig_rd_discontinue,
    sig_llink2cmd_rd_busy,
    sig_llink2rd_allow_addr_req,
    E,
    fetching,
    out,
    sig_rdwr2llink_int_err,
    m_axi_aclk,
    sig_last_reg_out_reg,
    sig_m_valid_dup_reg,
    sig_m_valid_out_reg,
    sig_cmd2all_doing_read,
    sig_doing_read_reg);
  output sig_rd_discontinue;
  output sig_llink2cmd_rd_busy;
  output sig_llink2rd_allow_addr_req;
  output [0:0]E;
  output fetching;
  input out;
  input sig_rdwr2llink_int_err;
  input m_axi_aclk;
  input sig_last_reg_out_reg;
  input sig_m_valid_dup_reg;
  input sig_m_valid_out_reg;
  input sig_cmd2all_doing_read;
  input sig_doing_read_reg;

  wire [0:0]E;
  wire \I_WR_LLINK_ADAPTER/sig_wr_error_reg ;
  wire fetching;
  wire m_axi_aclk;
  wire out;
  wire sig_allow_rd_requests_i_1_n_0;
  wire sig_cmd2all_doing_read;
  wire sig_doing_read_reg;
  wire sig_last_reg_out_reg;
  wire sig_llink2cmd_rd_busy;
  wire sig_llink2rd_allow_addr_req;
  wire sig_m_valid_dup_reg;
  wire sig_m_valid_out_reg;
  wire sig_rd_discontinue;
  wire sig_rd_discontinue_i_1_n_0;
  wire sig_rdwr2llink_int_err;

  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'hEA)) 
    fetching_INST_0
       (.I0(sig_m_valid_out_reg),
        .I1(sig_rd_discontinue),
        .I2(sig_llink2cmd_rd_busy),
        .O(fetching));
  LUT5 #(
    .INIT(32'h0000EE0E)) 
    sig_allow_rd_requests_i_1
       (.I0(sig_llink2rd_allow_addr_req),
        .I1(sig_llink2cmd_rd_busy),
        .I2(sig_cmd2all_doing_read),
        .I3(sig_doing_read_reg),
        .I4(out),
        .O(sig_allow_rd_requests_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sig_allow_rd_requests_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_allow_rd_requests_i_1_n_0),
        .Q(sig_llink2rd_allow_addr_req),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \sig_data_reg_out[31]_i_1 
       (.I0(sig_llink2cmd_rd_busy),
        .I1(sig_m_valid_dup_reg),
        .O(E));
  FDRE #(
    .INIT(1'b0)) 
    sig_llink_busy_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_last_reg_out_reg),
        .Q(sig_llink2cmd_rd_busy),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h00000F20)) 
    sig_rd_discontinue_i_1
       (.I0(sig_rdwr2llink_int_err),
        .I1(\I_WR_LLINK_ADAPTER/sig_wr_error_reg ),
        .I2(sig_llink2cmd_rd_busy),
        .I3(sig_rd_discontinue),
        .I4(out),
        .O(sig_rd_discontinue_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sig_rd_discontinue_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_rd_discontinue_i_1_n_0),
        .Q(sig_rd_discontinue),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    sig_rd_error_reg_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_rdwr2llink_int_err),
        .Q(\I_WR_LLINK_ADAPTER/sig_wr_error_reg ),
        .R(out));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axi_master_burst_rd_status_cntl
   (sig_rsc2stat_status_valid,
    sig_rsc2rdc_ready,
    sig_rsc2stat_status,
    m_axi_aclk,
    sig_rdc2rsc_valid,
    sig_rdc2rsc_calc_err,
    sig_push_status1_out,
    out);
  output sig_rsc2stat_status_valid;
  output sig_rsc2rdc_ready;
  output [0:0]sig_rsc2stat_status;
  input m_axi_aclk;
  input sig_rdc2rsc_valid;
  input sig_rdc2rsc_calc_err;
  input sig_push_status1_out;
  input out;

  wire m_axi_aclk;
  wire out;
  wire sig_push_status1_out;
  wire sig_rd_sts_interr_reg_i_1_n_0;
  wire sig_rd_sts_reg_empty_i_1_n_0;
  wire sig_rd_sts_reg_full_i_1_n_0;
  wire sig_rdc2rsc_calc_err;
  wire sig_rdc2rsc_valid;
  wire sig_rsc2rdc_ready;
  wire [0:0]sig_rsc2stat_status;
  wire sig_rsc2stat_status_valid;

  LUT6 #(
    .INIT(64'h000000000000EAAA)) 
    sig_rd_sts_interr_reg_i_1
       (.I0(sig_rsc2stat_status),
        .I1(sig_rsc2rdc_ready),
        .I2(sig_rdc2rsc_valid),
        .I3(sig_rdc2rsc_calc_err),
        .I4(sig_push_status1_out),
        .I5(out),
        .O(sig_rd_sts_interr_reg_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sig_rd_sts_interr_reg_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_rd_sts_interr_reg_i_1_n_0),
        .Q(sig_rsc2stat_status),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT4 #(
    .INIT(16'hFFF2)) 
    sig_rd_sts_reg_empty_i_1
       (.I0(sig_rsc2rdc_ready),
        .I1(sig_rdc2rsc_valid),
        .I2(sig_push_status1_out),
        .I3(out),
        .O(sig_rd_sts_reg_empty_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sig_rd_sts_reg_empty_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_rd_sts_reg_empty_i_1_n_0),
        .Q(sig_rsc2rdc_ready),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT5 #(
    .INIT(32'h000000EA)) 
    sig_rd_sts_reg_full_i_1
       (.I0(sig_rsc2stat_status_valid),
        .I1(sig_rsc2rdc_ready),
        .I2(sig_rdc2rsc_valid),
        .I3(sig_push_status1_out),
        .I4(out),
        .O(sig_rd_sts_reg_full_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sig_rd_sts_reg_full_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_rd_sts_reg_full_i_1_n_0),
        .Q(sig_rsc2stat_status_valid),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axi_master_burst_rd_wr_cntlr
   (sig_pcc2data_calc_error,
    sig_pcc2all_calc_err,
    sig_m_valid_out_reg,
    sig_llink_busy_reg,
    m_axi_wvalid,
    m_axi_arsize,
    m_axi_arburst,
    m_axi_arvalid,
    sig_doing_read_reg,
    \sig_btt_cntr_reg[14] ,
    m_axi_wstrb,
    sig_rsc2stat_status_valid,
    sig_rsc2stat_status,
    m_axi_bready,
    SR,
    sig_calc_error_reg0,
    sig_pcc_taking_command,
    m_axi_rready,
    sig_llink_busy_reg_0,
    \GEN_ADDR_32.sig_addr_cntr_lsh_reg[1] ,
    dataPacket,
    m_axi_araddr,
    m_axi_arlen,
    out,
    m_axi_aclk,
    sig_rd_addr_valid_reg0,
    sig_cmd2all_doing_read,
    \sig_cmd_mst_length_reg[13] ,
    Q,
    sig_cmd2pcc_cmd_valid,
    sig_llink2cmd_rd_busy,
    m_axi_rvalid,
    m_axi_wready,
    m_axi_bvalid,
    sig_cmd_reset_reg_reg,
    sig_push_cmd_reg,
    sig_cmd_mstrd_req,
    D,
    sig_llink2rd_allow_addr_req,
    m_axi_rlast,
    sig_status_reg_empty,
    m_axi_arready,
    m_axi_rdata,
    sig_rd_llink_enable,
    sig_rd_discontinue,
    sig_llink_reset_reg_reg,
    sig_push_status1_out,
    E,
    sig_cmd_mstrd_req_reg);
  output sig_pcc2data_calc_error;
  output sig_pcc2all_calc_err;
  output sig_m_valid_out_reg;
  output sig_llink_busy_reg;
  output m_axi_wvalid;
  output [0:0]m_axi_arsize;
  output [0:0]m_axi_arburst;
  output m_axi_arvalid;
  output sig_doing_read_reg;
  output [4:0]\sig_btt_cntr_reg[14] ;
  output [0:0]m_axi_wstrb;
  output sig_rsc2stat_status_valid;
  output [0:0]sig_rsc2stat_status;
  output m_axi_bready;
  output [0:0]SR;
  output sig_calc_error_reg0;
  output sig_pcc_taking_command;
  output m_axi_rready;
  output sig_llink_busy_reg_0;
  output [1:0]\GEN_ADDR_32.sig_addr_cntr_lsh_reg[1] ;
  output [31:0]dataPacket;
  output [31:0]m_axi_araddr;
  output [7:0]m_axi_arlen;
  input out;
  input m_axi_aclk;
  input sig_rd_addr_valid_reg0;
  input sig_cmd2all_doing_read;
  input \sig_cmd_mst_length_reg[13] ;
  input [18:0]Q;
  input sig_cmd2pcc_cmd_valid;
  input sig_llink2cmd_rd_busy;
  input m_axi_rvalid;
  input m_axi_wready;
  input m_axi_bvalid;
  input sig_cmd_reset_reg_reg;
  input sig_push_cmd_reg;
  input sig_cmd_mstrd_req;
  input [1:0]D;
  input sig_llink2rd_allow_addr_req;
  input m_axi_rlast;
  input sig_status_reg_empty;
  input m_axi_arready;
  input [31:0]m_axi_rdata;
  input sig_rd_llink_enable;
  input sig_rd_discontinue;
  input sig_llink_reset_reg_reg;
  input sig_push_status1_out;
  input [0:0]E;
  input [4:0]sig_cmd_mstrd_req_reg;

  wire [1:0]D;
  wire [0:0]E;
  wire [1:0]\GEN_ADDR_32.sig_addr_cntr_lsh_reg[1] ;
  wire \GEN_OMIT_STORE_FORWARD.I_DATA_CNTL_STATUS_FIFO/sig_init_reg ;
  wire I_MSTR_PCC_n_12;
  wire I_MSTR_PCC_n_13;
  wire I_MSTR_PCC_n_14;
  wire I_MSTR_PCC_n_15;
  wire I_MSTR_PCC_n_18;
  wire I_MSTR_PCC_n_19;
  wire I_MSTR_PCC_n_20;
  wire I_MSTR_PCC_n_21;
  wire I_MSTR_PCC_n_22;
  wire I_MSTR_PCC_n_23;
  wire I_MSTR_PCC_n_24;
  wire I_MSTR_PCC_n_25;
  wire I_MSTR_PCC_n_26;
  wire I_MSTR_PCC_n_27;
  wire I_MSTR_PCC_n_28;
  wire I_MSTR_PCC_n_29;
  wire I_MSTR_PCC_n_30;
  wire I_MSTR_PCC_n_33;
  wire I_RD_DATA_CNTL_n_4;
  wire I_RD_DATA_CNTL_n_5;
  wire I_READ_STREAM_SKID_BUF_n_4;
  wire I_WR_STATUS_CNTLR_n_1;
  wire [18:0]Q;
  wire [0:0]SR;
  wire [31:0]dataPacket;
  wire \i_/i_/i__carry__0_n_0 ;
  wire \i_/i_/i__carry__0_n_1 ;
  wire \i_/i_/i__carry__0_n_2 ;
  wire \i_/i_/i__carry__0_n_3 ;
  wire \i_/i_/i__carry__0_n_4 ;
  wire \i_/i_/i__carry__0_n_5 ;
  wire \i_/i_/i__carry__0_n_6 ;
  wire \i_/i_/i__carry__0_n_7 ;
  wire \i_/i_/i__carry__1_n_0 ;
  wire \i_/i_/i__carry__1_n_1 ;
  wire \i_/i_/i__carry__1_n_2 ;
  wire \i_/i_/i__carry__1_n_3 ;
  wire \i_/i_/i__carry__1_n_4 ;
  wire \i_/i_/i__carry__1_n_5 ;
  wire \i_/i_/i__carry__1_n_6 ;
  wire \i_/i_/i__carry__1_n_7 ;
  wire \i_/i_/i__carry__2_n_1 ;
  wire \i_/i_/i__carry__2_n_2 ;
  wire \i_/i_/i__carry__2_n_3 ;
  wire \i_/i_/i__carry__2_n_4 ;
  wire \i_/i_/i__carry__2_n_5 ;
  wire \i_/i_/i__carry__2_n_6 ;
  wire \i_/i_/i__carry__2_n_7 ;
  wire \i_/i_/i__carry_n_0 ;
  wire \i_/i_/i__carry_n_1 ;
  wire \i_/i_/i__carry_n_2 ;
  wire \i_/i_/i__carry_n_3 ;
  wire \i_/i_/i__carry_n_4 ;
  wire \i_/i_/i__carry_n_5 ;
  wire \i_/i_/i__carry_n_6 ;
  wire \i_/i_/i__carry_n_7 ;
  wire m_axi_aclk;
  wire [31:0]m_axi_araddr;
  wire [0:0]m_axi_arburst;
  wire [7:0]m_axi_arlen;
  wire m_axi_arready;
  wire [0:0]m_axi_arsize;
  wire m_axi_arvalid;
  wire m_axi_bready;
  wire m_axi_bvalid;
  wire [31:0]m_axi_rdata;
  wire m_axi_rlast;
  wire m_axi_rready;
  wire m_axi_rvalid;
  wire m_axi_wready;
  wire [0:0]m_axi_wstrb;
  wire m_axi_wvalid;
  wire out;
  wire p_0_in5_in;
  wire sig_addr2data_addr_posted;
  wire sig_addr2stat_cmd_fifo_empty;
  wire [4:0]\sig_btt_cntr_reg[14] ;
  wire sig_calc_error_reg0;
  wire sig_cmd2all_doing_read;
  wire sig_cmd2pcc_cmd_valid;
  wire \sig_cmd_mst_length_reg[13] ;
  wire sig_cmd_mstrd_req;
  wire [4:0]sig_cmd_mstrd_req_reg;
  wire sig_cmd_reset_reg_reg;
  wire sig_doing_read_reg;
  wire sig_llink2cmd_rd_busy;
  wire sig_llink2rd_allow_addr_req;
  wire sig_llink_busy_reg;
  wire sig_llink_busy_reg_0;
  wire sig_llink_reset_reg_reg;
  wire sig_m_valid_out_reg;
  wire sig_next_eof_reg;
  wire [31:2]sig_pcc2addr_addr;
  wire [0:0]sig_pcc2addr_burst;
  wire sig_pcc2all_calc_err;
  wire sig_pcc2data_calc_error;
  wire sig_pcc2data_cmd_cmplt;
  wire sig_pcc2data_cmd_valid;
  wire sig_pcc2data_dre_src_align;
  wire sig_pcc2data_eof;
  wire [7:0]sig_pcc2data_len;
  wire [1:1]sig_pcc2data_saddr_lsb;
  wire sig_pcc2data_sequential;
  wire sig_pcc_taking_command;
  wire sig_push_addr_reg1_out;
  wire sig_push_cmd_reg;
  wire sig_push_status1_out;
  wire sig_rd_addr_valid_reg0;
  wire sig_rd_discontinue;
  wire sig_rd_llink_enable;
  wire sig_rdc2pcc_cmd_ready;
  wire sig_rdc2rdskid_tlast;
  wire sig_rdc2rsc_calc_err;
  wire sig_rdc2rsc_valid;
  wire sig_rdskid2rdc_tready;
  wire sig_rsc2rdc_ready;
  wire [0:0]sig_rsc2stat_status;
  wire sig_rsc2stat_status_valid;
  wire sig_status_reg_empty;
  wire [3:3]\NLW_i_/i_/i__carry__2_CO_UNCONNECTED ;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axi_master_burst_addr_cntl I_ADDR_CNTL
       (.Q({sig_pcc2addr_addr,sig_pcc2data_saddr_lsb,sig_pcc2data_dre_src_align}),
        .m_axi_aclk(m_axi_aclk),
        .m_axi_araddr(m_axi_araddr),
        .m_axi_arburst(m_axi_arburst),
        .m_axi_arlen(m_axi_arlen),
        .m_axi_arready(m_axi_arready),
        .m_axi_arsize(m_axi_arsize),
        .m_axi_arvalid(m_axi_arvalid),
        .out(sig_addr2data_addr_posted),
        .sig_addr2stat_cmd_fifo_empty(sig_addr2stat_cmd_fifo_empty),
        .sig_cmd2all_doing_read(sig_cmd2all_doing_read),
        .sig_pcc2addr_burst(sig_pcc2addr_burst),
        .sig_pcc2data_calc_error(sig_pcc2data_calc_error),
        .sig_push_addr_reg1_out(sig_push_addr_reg1_out),
        .sig_rd_addr_valid_reg0(sig_rd_addr_valid_reg0),
        .sig_rdwr_reset_reg_reg(out),
        .\sig_xfer_len_reg_reg[7] (sig_pcc2data_len));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axi_master_burst_pcc I_MSTR_PCC
       (.D(D),
        .DI(I_MSTR_PCC_n_18),
        .\GEN_ADDR_32.sig_addr_cntr_lsh_reg[1]_0 (\GEN_ADDR_32.sig_addr_cntr_lsh_reg[1] ),
        .\GEN_ADDR_32.sig_addr_cntr_msh_reg[0]_0 (sig_calc_error_reg0),
        .\GEN_ADDR_32.sig_addr_cntr_msh_reg[11]_0 ({I_MSTR_PCC_n_27,I_MSTR_PCC_n_28,I_MSTR_PCC_n_29,I_MSTR_PCC_n_30}),
        .\GEN_ADDR_32.sig_addr_cntr_msh_reg[11]_1 ({\i_/i_/i__carry__1_n_4 ,\i_/i_/i__carry__1_n_5 ,\i_/i_/i__carry__1_n_6 ,\i_/i_/i__carry__1_n_7 }),
        .\GEN_ADDR_32.sig_addr_cntr_msh_reg[15]_0 ({I_MSTR_PCC_n_23,I_MSTR_PCC_n_24,I_MSTR_PCC_n_25,I_MSTR_PCC_n_26}),
        .\GEN_ADDR_32.sig_addr_cntr_msh_reg[15]_1 ({\i_/i_/i__carry__2_n_4 ,\i_/i_/i__carry__2_n_5 ,\i_/i_/i__carry__2_n_6 ,\i_/i_/i__carry__2_n_7 }),
        .\GEN_ADDR_32.sig_addr_cntr_msh_reg[7]_0 ({I_MSTR_PCC_n_19,I_MSTR_PCC_n_20,I_MSTR_PCC_n_21,I_MSTR_PCC_n_22}),
        .\GEN_ADDR_32.sig_addr_cntr_msh_reg[7]_1 ({\i_/i_/i__carry__0_n_4 ,\i_/i_/i__carry__0_n_5 ,\i_/i_/i__carry__0_n_6 ,\i_/i_/i__carry__0_n_7 }),
        .O({\i_/i_/i__carry_n_4 ,\i_/i_/i__carry_n_5 ,\i_/i_/i__carry_n_6 ,\i_/i_/i__carry_n_7 }),
        .Q(Q),
        .S({I_MSTR_PCC_n_12,I_MSTR_PCC_n_13,I_MSTR_PCC_n_14,I_MSTR_PCC_n_15}),
        .SR(SR),
        .m_axi_aclk(m_axi_aclk),
        .out(out),
        .sig_addr2stat_cmd_fifo_empty(sig_addr2stat_cmd_fifo_empty),
        .\sig_btt_cntr_reg[14]_0 (\sig_btt_cntr_reg[14] ),
        .sig_cmd2all_doing_read(sig_cmd2all_doing_read),
        .sig_cmd2pcc_cmd_valid(sig_cmd2pcc_cmd_valid),
        .\sig_cmd_mst_length_reg[13] (\sig_cmd_mst_length_reg[13] ),
        .sig_cmd_mstrd_req(sig_cmd_mstrd_req),
        .sig_cmd_mstrd_req_reg(sig_cmd_mstrd_req_reg),
        .sig_cmd_reset_reg_reg(sig_cmd_reset_reg_reg),
        .sig_last_dbeat_reg(I_MSTR_PCC_n_33),
        .sig_llink2rd_allow_addr_req(sig_llink2rd_allow_addr_req),
        .\sig_next_addr_reg_reg[31] ({sig_pcc2addr_addr,sig_pcc2data_saddr_lsb,sig_pcc2data_dre_src_align}),
        .\sig_next_len_reg_reg[7] (sig_pcc2data_len),
        .sig_pcc2addr_burst(sig_pcc2addr_burst),
        .sig_pcc2data_calc_error(sig_pcc2data_calc_error),
        .sig_pcc2data_cmd_cmplt(sig_pcc2data_cmd_cmplt),
        .sig_pcc2data_cmd_valid(sig_pcc2data_cmd_valid),
        .sig_pcc2data_eof(sig_pcc2data_eof),
        .sig_pcc2data_sequential(sig_pcc2data_sequential),
        .sig_pcc_taking_command(sig_pcc_taking_command),
        .sig_push_addr_reg1_out(sig_push_addr_reg1_out),
        .sig_push_cmd_reg(sig_push_cmd_reg),
        .sig_rdc2pcc_cmd_ready(sig_rdc2pcc_cmd_ready),
        .sig_xfer_calc_err_reg_reg_0(sig_pcc2all_calc_err));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axi_master_burst_rddata_cntl I_RD_DATA_CNTL
       (.m_axi_aclk(m_axi_aclk),
        .m_axi_rlast(m_axi_rlast),
        .m_axi_rvalid(m_axi_rvalid),
        .out(out),
        .sig_cmd2all_doing_read(sig_cmd2all_doing_read),
        .sig_m_valid_out_reg(I_RD_DATA_CNTL_n_5),
        .sig_next_eof_reg(sig_next_eof_reg),
        .sig_next_eof_reg_reg_0(I_RD_DATA_CNTL_n_4),
        .sig_pcc2data_calc_error(sig_pcc2data_calc_error),
        .sig_pcc2data_cmd_cmplt(sig_pcc2data_cmd_cmplt),
        .sig_pcc2data_cmd_valid(sig_pcc2data_cmd_valid),
        .sig_pcc2data_eof(sig_pcc2data_eof),
        .sig_pcc2data_sequential(sig_pcc2data_sequential),
        .sig_posted_to_axi_reg(sig_addr2data_addr_posted),
        .sig_rdc2pcc_cmd_ready(sig_rdc2pcc_cmd_ready),
        .sig_rdc2rdskid_tlast(sig_rdc2rdskid_tlast),
        .sig_rdc2rsc_calc_err(sig_rdc2rsc_calc_err),
        .sig_rdc2rsc_valid(sig_rdc2rsc_valid),
        .sig_rsc2rdc_ready(sig_rsc2rdc_ready),
        .sig_rsc2stat_status_valid(sig_rsc2stat_status_valid),
        .sig_s_ready_out_reg(I_READ_STREAM_SKID_BUF_n_4),
        .sig_s_ready_out_reg_0(sig_rdskid2rdc_tready),
        .sig_status_reg_empty(sig_status_reg_empty),
        .\sig_xfer_len_reg_reg[0] (I_MSTR_PCC_n_33),
        .\sig_xfer_len_reg_reg[7] (sig_pcc2data_len));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axi_master_burst_rd_status_cntl I_RD_STATUS_CNTLR
       (.m_axi_aclk(m_axi_aclk),
        .out(out),
        .sig_push_status1_out(sig_push_status1_out),
        .sig_rdc2rsc_calc_err(sig_rdc2rsc_calc_err),
        .sig_rdc2rsc_valid(sig_rdc2rsc_valid),
        .sig_rsc2rdc_ready(sig_rsc2rdc_ready),
        .sig_rsc2stat_status(sig_rsc2stat_status),
        .sig_rsc2stat_status_valid(sig_rsc2stat_status_valid));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axi_master_burst_skid_buf I_READ_STREAM_SKID_BUF
       (.E(E),
        .dataPacket(dataPacket),
        .m_axi_aclk(m_axi_aclk),
        .m_axi_rdata(m_axi_rdata),
        .m_axi_rlast(m_axi_rlast),
        .m_axi_rready(m_axi_rready),
        .m_axi_rvalid(m_axi_rvalid),
        .out(sig_rdskid2rdc_tready),
        .\sig_addr_posted_cntr_reg[0] (I_RD_DATA_CNTL_n_4),
        .\sig_addr_posted_cntr_reg[0]_0 (I_RD_DATA_CNTL_n_5),
        .\sig_dbeat_cntr_reg[0] (I_READ_STREAM_SKID_BUF_n_4),
        .sig_init_reg(\GEN_OMIT_STORE_FORWARD.I_DATA_CNTL_STATUS_FIFO/sig_init_reg ),
        .sig_llink2cmd_rd_busy(sig_llink2cmd_rd_busy),
        .sig_llink_busy_reg(sig_llink_busy_reg),
        .sig_llink_busy_reg_0(sig_llink_busy_reg_0),
        .sig_llink_reset_reg_reg(sig_llink_reset_reg_reg),
        .sig_m_valid_out_reg_0(sig_m_valid_out_reg),
        .sig_next_eof_reg(sig_next_eof_reg),
        .sig_rd_discontinue(sig_rd_discontinue),
        .sig_rd_llink_enable(sig_rd_llink_enable),
        .sig_rdc2rdskid_tlast(sig_rdc2rdskid_tlast),
        .sig_rdwr_reset_reg_reg(out));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axi_master_burst_skid2mm_buf I_WRITE_MMAP_SKID_BUF
       (.m_axi_aclk(m_axi_aclk),
        .m_axi_wready(m_axi_wready),
        .m_axi_wstrb(m_axi_wstrb),
        .m_axi_wvalid(m_axi_wvalid),
        .out(out),
        .sig_init_reg(\GEN_OMIT_STORE_FORWARD.I_DATA_CNTL_STATUS_FIFO/sig_init_reg ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axi_master_burst_skid_buf_0 I_WRITE_STRM_SKID_BUF
       (.m_axi_aclk(m_axi_aclk),
        .out(p_0_in5_in),
        .sig_init_reg(\GEN_OMIT_STORE_FORWARD.I_DATA_CNTL_STATUS_FIFO/sig_init_reg ),
        .sig_init_reg_reg(I_WR_STATUS_CNTLR_n_1),
        .sig_rdwr_reset_reg_reg(out));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axi_master_burst_wr_status_cntl I_WR_STATUS_CNTLR
       (.m_axi_aclk(m_axi_aclk),
        .m_axi_bready(m_axi_bready),
        .m_axi_bvalid(m_axi_bvalid),
        .out(out),
        .sig_init_reg(\GEN_OMIT_STORE_FORWARD.I_DATA_CNTL_STATUS_FIFO/sig_init_reg ),
        .sig_s_ready_dup_reg(p_0_in5_in),
        .sig_s_ready_out_reg(I_WR_STATUS_CNTLR_n_1));
  CARRY4 \i_/i_/i__carry 
       (.CI(1'b0),
        .CO({\i_/i_/i__carry_n_0 ,\i_/i_/i__carry_n_1 ,\i_/i_/i__carry_n_2 ,\i_/i_/i__carry_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,I_MSTR_PCC_n_18}),
        .O({\i_/i_/i__carry_n_4 ,\i_/i_/i__carry_n_5 ,\i_/i_/i__carry_n_6 ,\i_/i_/i__carry_n_7 }),
        .S({I_MSTR_PCC_n_12,I_MSTR_PCC_n_13,I_MSTR_PCC_n_14,I_MSTR_PCC_n_15}));
  CARRY4 \i_/i_/i__carry__0 
       (.CI(\i_/i_/i__carry_n_0 ),
        .CO({\i_/i_/i__carry__0_n_0 ,\i_/i_/i__carry__0_n_1 ,\i_/i_/i__carry__0_n_2 ,\i_/i_/i__carry__0_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\i_/i_/i__carry__0_n_4 ,\i_/i_/i__carry__0_n_5 ,\i_/i_/i__carry__0_n_6 ,\i_/i_/i__carry__0_n_7 }),
        .S({I_MSTR_PCC_n_19,I_MSTR_PCC_n_20,I_MSTR_PCC_n_21,I_MSTR_PCC_n_22}));
  CARRY4 \i_/i_/i__carry__1 
       (.CI(\i_/i_/i__carry__0_n_0 ),
        .CO({\i_/i_/i__carry__1_n_0 ,\i_/i_/i__carry__1_n_1 ,\i_/i_/i__carry__1_n_2 ,\i_/i_/i__carry__1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\i_/i_/i__carry__1_n_4 ,\i_/i_/i__carry__1_n_5 ,\i_/i_/i__carry__1_n_6 ,\i_/i_/i__carry__1_n_7 }),
        .S({I_MSTR_PCC_n_27,I_MSTR_PCC_n_28,I_MSTR_PCC_n_29,I_MSTR_PCC_n_30}));
  CARRY4 \i_/i_/i__carry__2 
       (.CI(\i_/i_/i__carry__1_n_0 ),
        .CO({\NLW_i_/i_/i__carry__2_CO_UNCONNECTED [3],\i_/i_/i__carry__2_n_1 ,\i_/i_/i__carry__2_n_2 ,\i_/i_/i__carry__2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\i_/i_/i__carry__2_n_4 ,\i_/i_/i__carry__2_n_5 ,\i_/i_/i__carry__2_n_6 ,\i_/i_/i__carry__2_n_7 }),
        .S({I_MSTR_PCC_n_23,I_MSTR_PCC_n_24,I_MSTR_PCC_n_25,I_MSTR_PCC_n_26}));
  FDRE #(
    .INIT(1'b0)) 
    sig_doing_read_reg_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_cmd2all_doing_read),
        .Q(sig_doing_read_reg),
        .R(out));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axi_master_burst_rddata_cntl
   (sig_rdc2pcc_cmd_ready,
    sig_next_eof_reg,
    sig_rdc2rsc_valid,
    sig_rdc2rsc_calc_err,
    sig_next_eof_reg_reg_0,
    sig_m_valid_out_reg,
    sig_rdc2rdskid_tlast,
    sig_pcc2data_cmd_cmplt,
    m_axi_aclk,
    sig_pcc2data_calc_error,
    out,
    sig_pcc2data_sequential,
    sig_pcc2data_eof,
    sig_s_ready_out_reg,
    \sig_xfer_len_reg_reg[7] ,
    \sig_xfer_len_reg_reg[0] ,
    sig_s_ready_out_reg_0,
    m_axi_rvalid,
    m_axi_rlast,
    sig_rsc2stat_status_valid,
    sig_status_reg_empty,
    sig_pcc2data_cmd_valid,
    sig_cmd2all_doing_read,
    sig_rsc2rdc_ready,
    sig_posted_to_axi_reg);
  output sig_rdc2pcc_cmd_ready;
  output sig_next_eof_reg;
  output sig_rdc2rsc_valid;
  output sig_rdc2rsc_calc_err;
  output sig_next_eof_reg_reg_0;
  output sig_m_valid_out_reg;
  output sig_rdc2rdskid_tlast;
  input sig_pcc2data_cmd_cmplt;
  input m_axi_aclk;
  input sig_pcc2data_calc_error;
  input out;
  input sig_pcc2data_sequential;
  input sig_pcc2data_eof;
  input sig_s_ready_out_reg;
  input [7:0]\sig_xfer_len_reg_reg[7] ;
  input \sig_xfer_len_reg_reg[0] ;
  input sig_s_ready_out_reg_0;
  input m_axi_rvalid;
  input m_axi_rlast;
  input sig_rsc2stat_status_valid;
  input sig_status_reg_empty;
  input sig_pcc2data_cmd_valid;
  input sig_cmd2all_doing_read;
  input sig_rsc2rdc_ready;
  input sig_posted_to_axi_reg;

  wire m_axi_aclk;
  wire m_axi_rlast;
  wire m_axi_rvalid;
  wire out;
  wire [7:0]p_0_in;
  wire [2:0]sig_addr_posted_cntr;
  wire \sig_addr_posted_cntr[0]_i_1_n_0 ;
  wire \sig_addr_posted_cntr[1]_i_1_n_0 ;
  wire \sig_addr_posted_cntr[2]_i_1_n_0 ;
  wire sig_clr_dqual_reg;
  wire sig_cmd2all_doing_read;
  wire sig_cmd_cmplt_last_dbeat;
  wire sig_coelsc_interr_reg_i_1_n_0;
  wire sig_coelsc_reg_full_i_1_n_0;
  wire \sig_dbeat_cntr[4]_i_2_n_0 ;
  wire \sig_dbeat_cntr[7]_i_1_n_0 ;
  wire \sig_dbeat_cntr[7]_i_3_n_0 ;
  wire \sig_dbeat_cntr[7]_i_4_n_0 ;
  wire [7:0]sig_dbeat_cntr_reg__0;
  wire sig_dqual_reg_empty;
  wire sig_dqual_reg_full;
  wire sig_last_dbeat_i_1_n_0;
  wire sig_last_dbeat_i_2_n_0;
  wire sig_last_dbeat_i_3_n_0;
  wire sig_last_dbeat_i_5_n_0;
  wire sig_last_dbeat_i_6_n_0;
  wire sig_last_dbeat_reg_n_0;
  wire sig_last_mmap_dbeat;
  wire sig_ld_new_cmd_reg;
  wire sig_ld_new_cmd_reg_i_1_n_0;
  wire sig_m_valid_out_reg;
  wire sig_next_calc_error_reg;
  wire sig_next_cmd_cmplt_reg;
  wire sig_next_cmd_cmplt_reg_i_4_n_0;
  wire sig_next_cmd_cmplt_reg_i_5_n_0;
  wire sig_next_eof_reg;
  wire sig_next_eof_reg_reg_0;
  wire sig_next_sequential_reg;
  wire sig_pcc2data_calc_error;
  wire sig_pcc2data_cmd_cmplt;
  wire sig_pcc2data_cmd_valid;
  wire sig_pcc2data_eof;
  wire sig_pcc2data_sequential;
  wire sig_posted_to_axi_reg;
  wire sig_push_coelsc_reg;
  wire sig_rd2llink_xfer_cmplt;
  wire sig_rdc2pcc_cmd_ready;
  wire sig_rdc2rdskid_tlast;
  wire sig_rdc2rsc_calc_err;
  wire sig_rdc2rsc_valid;
  wire sig_rsc2rdc_ready;
  wire sig_rsc2stat_status_valid;
  wire sig_s_ready_out_reg;
  wire sig_s_ready_out_reg_0;
  wire sig_status_reg_empty;
  wire \sig_xfer_len_reg_reg[0] ;
  wire [7:0]\sig_xfer_len_reg_reg[7] ;

  LUT6 #(
    .INIT(64'hFFFFFFFFFF01FFFF)) 
    m_axi_rready_INST_0_i_1
       (.I0(sig_addr_posted_cntr[0]),
        .I1(sig_addr_posted_cntr[2]),
        .I2(sig_addr_posted_cntr[1]),
        .I3(sig_next_calc_error_reg),
        .I4(sig_dqual_reg_full),
        .I5(sig_rdc2rsc_valid),
        .O(sig_next_eof_reg_reg_0));
  LUT6 #(
    .INIT(64'h8F87878778787808)) 
    \sig_addr_posted_cntr[0]_i_1 
       (.I0(sig_cmd2all_doing_read),
        .I1(sig_posted_to_axi_reg),
        .I2(sig_rd2llink_xfer_cmplt),
        .I3(sig_addr_posted_cntr[2]),
        .I4(sig_addr_posted_cntr[1]),
        .I5(sig_addr_posted_cntr[0]),
        .O(\sig_addr_posted_cntr[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAADAA4AAA4AAA4AA)) 
    \sig_addr_posted_cntr[1]_i_1 
       (.I0(sig_addr_posted_cntr[1]),
        .I1(sig_addr_posted_cntr[2]),
        .I2(sig_addr_posted_cntr[0]),
        .I3(sig_rd2llink_xfer_cmplt),
        .I4(sig_posted_to_axi_reg),
        .I5(sig_cmd2all_doing_read),
        .O(\sig_addr_posted_cntr[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hCCECC8CCC8CCC8CC)) 
    \sig_addr_posted_cntr[2]_i_1 
       (.I0(sig_addr_posted_cntr[1]),
        .I1(sig_addr_posted_cntr[2]),
        .I2(sig_addr_posted_cntr[0]),
        .I3(sig_rd2llink_xfer_cmplt),
        .I4(sig_posted_to_axi_reg),
        .I5(sig_cmd2all_doing_read),
        .O(\sig_addr_posted_cntr[2]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_addr_posted_cntr_reg[0] 
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(\sig_addr_posted_cntr[0]_i_1_n_0 ),
        .Q(sig_addr_posted_cntr[0]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \sig_addr_posted_cntr_reg[1] 
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(\sig_addr_posted_cntr[1]_i_1_n_0 ),
        .Q(sig_addr_posted_cntr[1]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \sig_addr_posted_cntr_reg[2] 
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(\sig_addr_posted_cntr[2]_i_1_n_0 ),
        .Q(sig_addr_posted_cntr[2]),
        .R(out));
  LUT6 #(
    .INIT(64'h00000000EEEE0AAA)) 
    sig_coelsc_interr_reg_i_1
       (.I0(sig_rdc2rsc_calc_err),
        .I1(sig_next_calc_error_reg),
        .I2(sig_rsc2rdc_ready),
        .I3(sig_rdc2rsc_valid),
        .I4(sig_push_coelsc_reg),
        .I5(out),
        .O(sig_coelsc_interr_reg_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sig_coelsc_interr_reg_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_coelsc_interr_reg_i_1_n_0),
        .Q(sig_rdc2rsc_calc_err),
        .R(1'b0));
  LUT5 #(
    .INIT(32'h0000AA30)) 
    sig_coelsc_reg_full_i_1
       (.I0(sig_cmd_cmplt_last_dbeat),
        .I1(sig_rsc2rdc_ready),
        .I2(sig_rdc2rsc_valid),
        .I3(sig_push_coelsc_reg),
        .I4(out),
        .O(sig_coelsc_reg_full_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT4 #(
    .INIT(16'hEAAA)) 
    sig_coelsc_reg_full_i_2
       (.I0(sig_next_calc_error_reg),
        .I1(m_axi_rlast),
        .I2(m_axi_rvalid),
        .I3(sig_next_cmd_cmplt_reg),
        .O(sig_cmd_cmplt_last_dbeat));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hEA)) 
    sig_coelsc_reg_full_i_3
       (.I0(sig_s_ready_out_reg),
        .I1(sig_next_calc_error_reg),
        .I2(sig_ld_new_cmd_reg),
        .O(sig_push_coelsc_reg));
  FDRE #(
    .INIT(1'b0)) 
    sig_coelsc_reg_full_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_coelsc_reg_full_i_1_n_0),
        .Q(sig_rdc2rsc_valid),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'h8B)) 
    \sig_dbeat_cntr[0]_i_1 
       (.I0(\sig_xfer_len_reg_reg[7] [0]),
        .I1(sig_rdc2pcc_cmd_ready),
        .I2(sig_dbeat_cntr_reg__0[0]),
        .O(p_0_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'hB88B)) 
    \sig_dbeat_cntr[1]_i_1 
       (.I0(\sig_xfer_len_reg_reg[7] [1]),
        .I1(sig_rdc2pcc_cmd_ready),
        .I2(sig_dbeat_cntr_reg__0[1]),
        .I3(sig_dbeat_cntr_reg__0[0]),
        .O(p_0_in[1]));
  LUT5 #(
    .INIT(32'hBBB8888B)) 
    \sig_dbeat_cntr[2]_i_1 
       (.I0(\sig_xfer_len_reg_reg[7] [2]),
        .I1(sig_rdc2pcc_cmd_ready),
        .I2(sig_dbeat_cntr_reg__0[1]),
        .I3(sig_dbeat_cntr_reg__0[0]),
        .I4(sig_dbeat_cntr_reg__0[2]),
        .O(p_0_in[2]));
  LUT6 #(
    .INIT(64'hBBBBBBB88888888B)) 
    \sig_dbeat_cntr[3]_i_1 
       (.I0(\sig_xfer_len_reg_reg[7] [3]),
        .I1(sig_rdc2pcc_cmd_ready),
        .I2(sig_dbeat_cntr_reg__0[2]),
        .I3(sig_dbeat_cntr_reg__0[0]),
        .I4(sig_dbeat_cntr_reg__0[1]),
        .I5(sig_dbeat_cntr_reg__0[3]),
        .O(p_0_in[3]));
  LUT4 #(
    .INIT(16'hB88B)) 
    \sig_dbeat_cntr[4]_i_1 
       (.I0(\sig_xfer_len_reg_reg[7] [4]),
        .I1(sig_rdc2pcc_cmd_ready),
        .I2(\sig_dbeat_cntr[4]_i_2_n_0 ),
        .I3(sig_dbeat_cntr_reg__0[4]),
        .O(p_0_in[4]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \sig_dbeat_cntr[4]_i_2 
       (.I0(sig_dbeat_cntr_reg__0[0]),
        .I1(sig_dbeat_cntr_reg__0[1]),
        .I2(sig_dbeat_cntr_reg__0[3]),
        .I3(sig_dbeat_cntr_reg__0[2]),
        .O(\sig_dbeat_cntr[4]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hB88B)) 
    \sig_dbeat_cntr[5]_i_1 
       (.I0(\sig_xfer_len_reg_reg[7] [5]),
        .I1(sig_rdc2pcc_cmd_ready),
        .I2(\sig_dbeat_cntr[7]_i_4_n_0 ),
        .I3(sig_dbeat_cntr_reg__0[5]),
        .O(p_0_in[5]));
  LUT5 #(
    .INIT(32'hBBB8888B)) 
    \sig_dbeat_cntr[6]_i_1 
       (.I0(\sig_xfer_len_reg_reg[7] [6]),
        .I1(sig_rdc2pcc_cmd_ready),
        .I2(sig_dbeat_cntr_reg__0[5]),
        .I3(\sig_dbeat_cntr[7]_i_4_n_0 ),
        .I4(sig_dbeat_cntr_reg__0[6]),
        .O(p_0_in[6]));
  LUT6 #(
    .INIT(64'h5444FFFF44444444)) 
    \sig_dbeat_cntr[7]_i_1 
       (.I0(sig_next_cmd_cmplt_reg_i_4_n_0),
        .I1(sig_dqual_reg_empty),
        .I2(sig_next_sequential_reg),
        .I3(sig_last_dbeat_reg_n_0),
        .I4(\sig_dbeat_cntr[7]_i_3_n_0 ),
        .I5(sig_s_ready_out_reg),
        .O(\sig_dbeat_cntr[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hB8B8B8B8B8B8B88B)) 
    \sig_dbeat_cntr[7]_i_2 
       (.I0(\sig_xfer_len_reg_reg[7] [7]),
        .I1(sig_rdc2pcc_cmd_ready),
        .I2(sig_dbeat_cntr_reg__0[7]),
        .I3(sig_dbeat_cntr_reg__0[5]),
        .I4(sig_dbeat_cntr_reg__0[6]),
        .I5(\sig_dbeat_cntr[7]_i_4_n_0 ),
        .O(p_0_in[7]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    \sig_dbeat_cntr[7]_i_3 
       (.I0(\sig_dbeat_cntr[4]_i_2_n_0 ),
        .I1(sig_dbeat_cntr_reg__0[4]),
        .I2(sig_dbeat_cntr_reg__0[7]),
        .I3(sig_dbeat_cntr_reg__0[5]),
        .I4(sig_dbeat_cntr_reg__0[6]),
        .O(\sig_dbeat_cntr[7]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \sig_dbeat_cntr[7]_i_4 
       (.I0(sig_dbeat_cntr_reg__0[4]),
        .I1(sig_dbeat_cntr_reg__0[2]),
        .I2(sig_dbeat_cntr_reg__0[3]),
        .I3(sig_dbeat_cntr_reg__0[1]),
        .I4(sig_dbeat_cntr_reg__0[0]),
        .O(\sig_dbeat_cntr[7]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_dbeat_cntr_reg[0] 
       (.C(m_axi_aclk),
        .CE(\sig_dbeat_cntr[7]_i_1_n_0 ),
        .D(p_0_in[0]),
        .Q(sig_dbeat_cntr_reg__0[0]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \sig_dbeat_cntr_reg[1] 
       (.C(m_axi_aclk),
        .CE(\sig_dbeat_cntr[7]_i_1_n_0 ),
        .D(p_0_in[1]),
        .Q(sig_dbeat_cntr_reg__0[1]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \sig_dbeat_cntr_reg[2] 
       (.C(m_axi_aclk),
        .CE(\sig_dbeat_cntr[7]_i_1_n_0 ),
        .D(p_0_in[2]),
        .Q(sig_dbeat_cntr_reg__0[2]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \sig_dbeat_cntr_reg[3] 
       (.C(m_axi_aclk),
        .CE(\sig_dbeat_cntr[7]_i_1_n_0 ),
        .D(p_0_in[3]),
        .Q(sig_dbeat_cntr_reg__0[3]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \sig_dbeat_cntr_reg[4] 
       (.C(m_axi_aclk),
        .CE(\sig_dbeat_cntr[7]_i_1_n_0 ),
        .D(p_0_in[4]),
        .Q(sig_dbeat_cntr_reg__0[4]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \sig_dbeat_cntr_reg[5] 
       (.C(m_axi_aclk),
        .CE(\sig_dbeat_cntr[7]_i_1_n_0 ),
        .D(p_0_in[5]),
        .Q(sig_dbeat_cntr_reg__0[5]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \sig_dbeat_cntr_reg[6] 
       (.C(m_axi_aclk),
        .CE(\sig_dbeat_cntr[7]_i_1_n_0 ),
        .D(p_0_in[6]),
        .Q(sig_dbeat_cntr_reg__0[6]),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \sig_dbeat_cntr_reg[7] 
       (.C(m_axi_aclk),
        .CE(\sig_dbeat_cntr[7]_i_1_n_0 ),
        .D(p_0_in[7]),
        .Q(sig_dbeat_cntr_reg__0[7]),
        .R(out));
  FDSE #(
    .INIT(1'b0)) 
    sig_dqual_reg_empty_reg
       (.C(m_axi_aclk),
        .CE(sig_rdc2pcc_cmd_ready),
        .D(1'b0),
        .Q(sig_dqual_reg_empty),
        .S(sig_clr_dqual_reg));
  FDRE #(
    .INIT(1'b0)) 
    sig_dqual_reg_full_reg
       (.C(m_axi_aclk),
        .CE(sig_rdc2pcc_cmd_ready),
        .D(sig_rdc2pcc_cmd_ready),
        .Q(sig_dqual_reg_full),
        .R(sig_clr_dqual_reg));
  LUT4 #(
    .INIT(16'h1310)) 
    sig_last_dbeat_i_1
       (.I0(sig_last_dbeat_i_2_n_0),
        .I1(out),
        .I2(\sig_dbeat_cntr[7]_i_1_n_0 ),
        .I3(sig_last_dbeat_reg_n_0),
        .O(sig_last_dbeat_i_1_n_0));
  LUT6 #(
    .INIT(64'h77FF777777F07777)) 
    sig_last_dbeat_i_2
       (.I0(sig_s_ready_out_reg),
        .I1(sig_last_dbeat_i_3_n_0),
        .I2(\sig_xfer_len_reg_reg[0] ),
        .I3(sig_next_cmd_cmplt_reg_i_4_n_0),
        .I4(sig_last_dbeat_i_5_n_0),
        .I5(\sig_xfer_len_reg_reg[7] [6]),
        .O(sig_last_dbeat_i_2_n_0));
  LUT5 #(
    .INIT(32'h00000010)) 
    sig_last_dbeat_i_3
       (.I0(sig_last_dbeat_i_6_n_0),
        .I1(sig_dbeat_cntr_reg__0[1]),
        .I2(sig_dbeat_cntr_reg__0[0]),
        .I3(sig_dbeat_cntr_reg__0[2]),
        .I4(sig_dbeat_cntr_reg__0[3]),
        .O(sig_last_dbeat_i_3_n_0));
  LUT6 #(
    .INIT(64'hAAAAAAAAEAAAAAAA)) 
    sig_last_dbeat_i_5
       (.I0(sig_dqual_reg_empty),
        .I1(sig_next_sequential_reg),
        .I2(sig_last_dbeat_reg_n_0),
        .I3(sig_s_ready_out_reg_0),
        .I4(m_axi_rvalid),
        .I5(sig_next_eof_reg_reg_0),
        .O(sig_last_dbeat_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    sig_last_dbeat_i_6
       (.I0(sig_dbeat_cntr_reg__0[6]),
        .I1(sig_dbeat_cntr_reg__0[5]),
        .I2(sig_dbeat_cntr_reg__0[7]),
        .I3(sig_dbeat_cntr_reg__0[4]),
        .O(sig_last_dbeat_i_6_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sig_last_dbeat_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_last_dbeat_i_1_n_0),
        .Q(sig_last_dbeat_reg_n_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    sig_last_mmap_dbeat_reg_i_1
       (.I0(sig_next_eof_reg_reg_0),
        .I1(sig_s_ready_out_reg_0),
        .I2(m_axi_rvalid),
        .I3(m_axi_rlast),
        .O(sig_last_mmap_dbeat));
  FDRE #(
    .INIT(1'b0)) 
    sig_last_mmap_dbeat_reg_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_last_mmap_dbeat),
        .Q(sig_rd2llink_xfer_cmplt),
        .R(out));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'h80)) 
    sig_last_skid_reg_i_1
       (.I0(sig_next_eof_reg),
        .I1(m_axi_rvalid),
        .I2(m_axi_rlast),
        .O(sig_rdc2rdskid_tlast));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'h02)) 
    sig_ld_new_cmd_reg_i_1
       (.I0(sig_rdc2pcc_cmd_ready),
        .I1(sig_ld_new_cmd_reg),
        .I2(out),
        .O(sig_ld_new_cmd_reg_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sig_ld_new_cmd_reg_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_ld_new_cmd_reg_i_1_n_0),
        .Q(sig_ld_new_cmd_reg),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT2 #(
    .INIT(4'hB)) 
    sig_m_valid_dup_i_2
       (.I0(sig_next_eof_reg_reg_0),
        .I1(m_axi_rvalid),
        .O(sig_m_valid_out_reg));
  FDRE #(
    .INIT(1'b0)) 
    sig_next_calc_error_reg_reg
       (.C(m_axi_aclk),
        .CE(sig_rdc2pcc_cmd_ready),
        .D(sig_pcc2data_calc_error),
        .Q(sig_next_calc_error_reg),
        .R(sig_clr_dqual_reg));
  LUT6 #(
    .INIT(64'hABAAAAAAAAAAAAAA)) 
    sig_next_cmd_cmplt_reg_i_1
       (.I0(out),
        .I1(sig_rdc2pcc_cmd_ready),
        .I2(sig_next_eof_reg_reg_0),
        .I3(sig_s_ready_out_reg_0),
        .I4(m_axi_rvalid),
        .I5(m_axi_rlast),
        .O(sig_clr_dqual_reg));
  LUT5 #(
    .INIT(32'h0000FF80)) 
    sig_next_cmd_cmplt_reg_i_2
       (.I0(sig_s_ready_out_reg),
        .I1(sig_last_dbeat_reg_n_0),
        .I2(sig_next_sequential_reg),
        .I3(sig_dqual_reg_empty),
        .I4(sig_next_cmd_cmplt_reg_i_4_n_0),
        .O(sig_rdc2pcc_cmd_ready));
  LUT6 #(
    .INIT(64'hFFAEFFFFFFFFFFFF)) 
    sig_next_cmd_cmplt_reg_i_4
       (.I0(sig_next_cmd_cmplt_reg_i_5_n_0),
        .I1(sig_rsc2stat_status_valid),
        .I2(sig_status_reg_empty),
        .I3(sig_next_calc_error_reg),
        .I4(sig_pcc2data_cmd_valid),
        .I5(sig_cmd2all_doing_read),
        .O(sig_next_cmd_cmplt_reg_i_4_n_0));
  LUT3 #(
    .INIT(8'h80)) 
    sig_next_cmd_cmplt_reg_i_5
       (.I0(sig_addr_posted_cntr[2]),
        .I1(sig_addr_posted_cntr[1]),
        .I2(sig_addr_posted_cntr[0]),
        .O(sig_next_cmd_cmplt_reg_i_5_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sig_next_cmd_cmplt_reg_reg
       (.C(m_axi_aclk),
        .CE(sig_rdc2pcc_cmd_ready),
        .D(sig_pcc2data_cmd_cmplt),
        .Q(sig_next_cmd_cmplt_reg),
        .R(sig_clr_dqual_reg));
  FDRE #(
    .INIT(1'b0)) 
    sig_next_eof_reg_reg
       (.C(m_axi_aclk),
        .CE(sig_rdc2pcc_cmd_ready),
        .D(sig_pcc2data_eof),
        .Q(sig_next_eof_reg),
        .R(sig_clr_dqual_reg));
  FDRE #(
    .INIT(1'b0)) 
    sig_next_sequential_reg_reg
       (.C(m_axi_aclk),
        .CE(sig_rdc2pcc_cmd_ready),
        .D(sig_pcc2data_sequential),
        .Q(sig_next_sequential_reg),
        .R(sig_clr_dqual_reg));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axi_master_burst_reset
   (out,
    \INFERRED_GEN.cnt_i_reg[0] ,
    sig_rd_error_reg_reg,
    m_axi_aclk,
    m_axi_aresetn);
  output out;
  output \INFERRED_GEN.cnt_i_reg[0] ;
  output sig_rd_error_reg_reg;
  input m_axi_aclk;
  input m_axi_aresetn;

  wire m_axi_aclk;
  wire m_axi_aresetn;
  wire sig_axi_por2rst__0;
  wire sig_axi_por2rst_out;
  wire sig_axi_por2rst_out_i_2_n_0;
  wire sig_axi_por_reg1;
  wire sig_axi_por_reg2;
  wire sig_axi_por_reg3;
  wire sig_axi_por_reg4;
  wire sig_axi_por_reg5;
  wire sig_axi_por_reg6;
  wire sig_axi_por_reg7;
  wire sig_axi_por_reg8;
  (* RTL_KEEP = "true" *) (* equivalent_register_removal = "no" *) wire sig_cmd_reset_reg;
  wire sig_cmd_reset_reg_i_1_n_0;
  (* RTL_KEEP = "true" *) (* equivalent_register_removal = "no" *) wire sig_llink_reset_reg;
  (* RTL_KEEP = "true" *) (* equivalent_register_removal = "no" *) wire sig_rdwr_reset_reg;

  assign \INFERRED_GEN.cnt_i_reg[0]  = sig_rdwr_reset_reg;
  assign out = sig_cmd_reset_reg;
  assign sig_rd_error_reg_reg = sig_llink_reset_reg;
  LUT5 #(
    .INIT(32'hFFFF7FFF)) 
    sig_axi_por2rst_out_i_1
       (.I0(sig_axi_por_reg4),
        .I1(sig_axi_por_reg5),
        .I2(sig_axi_por_reg3),
        .I3(sig_axi_por_reg2),
        .I4(sig_axi_por2rst_out_i_2_n_0),
        .O(sig_axi_por2rst__0));
  LUT4 #(
    .INIT(16'h7FFF)) 
    sig_axi_por2rst_out_i_2
       (.I0(sig_axi_por_reg7),
        .I1(sig_axi_por_reg6),
        .I2(sig_axi_por_reg1),
        .I3(sig_axi_por_reg8),
        .O(sig_axi_por2rst_out_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sig_axi_por2rst_out_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_axi_por2rst__0),
        .Q(sig_axi_por2rst_out),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    sig_axi_por_reg1_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(1'b1),
        .Q(sig_axi_por_reg1),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    sig_axi_por_reg2_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_axi_por_reg1),
        .Q(sig_axi_por_reg2),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    sig_axi_por_reg3_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_axi_por_reg2),
        .Q(sig_axi_por_reg3),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    sig_axi_por_reg4_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_axi_por_reg3),
        .Q(sig_axi_por_reg4),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    sig_axi_por_reg5_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_axi_por_reg4),
        .Q(sig_axi_por_reg5),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    sig_axi_por_reg6_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_axi_por_reg5),
        .Q(sig_axi_por_reg6),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    sig_axi_por_reg7_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_axi_por_reg6),
        .Q(sig_axi_por_reg7),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    sig_axi_por_reg8_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_axi_por_reg7),
        .Q(sig_axi_por_reg8),
        .R(1'b0));
  LUT2 #(
    .INIT(4'hB)) 
    sig_cmd_reset_reg_i_1
       (.I0(sig_axi_por2rst_out),
        .I1(m_axi_aresetn),
        .O(sig_cmd_reset_reg_i_1_n_0));
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    sig_cmd_reset_reg_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_cmd_reset_reg_i_1_n_0),
        .Q(sig_cmd_reset_reg),
        .R(1'b0));
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    sig_llink_reset_reg_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_cmd_reset_reg_i_1_n_0),
        .Q(sig_llink_reset_reg),
        .R(1'b0));
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    sig_rdwr_reset_reg_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_cmd_reset_reg_i_1_n_0),
        .Q(sig_rdwr_reset_reg),
        .R(1'b0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axi_master_burst_skid2mm_buf
   (m_axi_wvalid,
    m_axi_wstrb,
    m_axi_aclk,
    out,
    m_axi_wready,
    sig_init_reg);
  output m_axi_wvalid;
  output [0:0]m_axi_wstrb;
  input m_axi_aclk;
  input out;
  input m_axi_wready;
  input sig_init_reg;

  wire m_axi_aclk;
  wire m_axi_wready;
  wire [0:0]m_axi_wstrb;
  wire out;
  wire sig_init_reg;
  (* RTL_KEEP = "true" *) (* equivalent_register_removal = "no" *) wire sig_m_valid_dup;
  wire sig_m_valid_dup_i_1__1_n_0;
  (* RTL_KEEP = "true" *) (* equivalent_register_removal = "no" *) wire sig_m_valid_out;
  (* RTL_KEEP = "true" *) (* equivalent_register_removal = "no" *) wire sig_s_ready_dup;
  wire sig_s_ready_dup_i_1__0_n_0;
  (* RTL_KEEP = "true" *) (* equivalent_register_removal = "no" *) wire sig_s_ready_out;
  wire \sig_strb_reg_out[3]_i_1_n_0 ;
  wire [3:3]sig_strb_skid_reg;

  assign m_axi_wvalid = sig_m_valid_out;
  LUT5 #(
    .INIT(32'h01110000)) 
    sig_m_valid_dup_i_1__1
       (.I0(sig_init_reg),
        .I1(out),
        .I2(sig_s_ready_dup),
        .I3(m_axi_wready),
        .I4(sig_m_valid_dup),
        .O(sig_m_valid_dup_i_1__1_n_0));
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    sig_m_valid_dup_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_m_valid_dup_i_1__1_n_0),
        .Q(sig_m_valid_dup),
        .R(1'b0));
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    sig_m_valid_out_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_m_valid_dup_i_1__1_n_0),
        .Q(sig_m_valid_out),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hFE)) 
    sig_s_ready_dup_i_1__0
       (.I0(m_axi_wready),
        .I1(sig_s_ready_dup),
        .I2(sig_init_reg),
        .O(sig_s_ready_dup_i_1__0_n_0));
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    sig_s_ready_dup_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_s_ready_dup_i_1__0_n_0),
        .Q(sig_s_ready_dup),
        .R(out));
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    sig_s_ready_out_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_s_ready_dup_i_1__0_n_0),
        .Q(sig_s_ready_out),
        .R(out));
  LUT5 #(
    .INIT(32'hEFEEE0EE)) 
    \sig_strb_reg_out[3]_i_1 
       (.I0(sig_s_ready_dup),
        .I1(sig_strb_skid_reg),
        .I2(m_axi_wready),
        .I3(sig_m_valid_dup),
        .I4(m_axi_wstrb),
        .O(\sig_strb_reg_out[3]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \sig_strb_reg_out_reg[3] 
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(\sig_strb_reg_out[3]_i_1_n_0 ),
        .Q(m_axi_wstrb),
        .R(out));
  FDRE #(
    .INIT(1'b0)) 
    \sig_strb_skid_reg_reg[3] 
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(1'b1),
        .Q(sig_strb_skid_reg),
        .R(out));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axi_master_burst_skid_buf
   (sig_m_valid_out_reg_0,
    out,
    sig_llink_busy_reg,
    m_axi_rready,
    \sig_dbeat_cntr_reg[0] ,
    sig_llink_busy_reg_0,
    dataPacket,
    m_axi_aclk,
    sig_rdwr_reset_reg_reg,
    sig_rdc2rdskid_tlast,
    sig_init_reg,
    sig_llink2cmd_rd_busy,
    m_axi_rvalid,
    \sig_addr_posted_cntr_reg[0] ,
    \sig_addr_posted_cntr_reg[0]_0 ,
    m_axi_rlast,
    m_axi_rdata,
    sig_rd_llink_enable,
    sig_rd_discontinue,
    sig_llink_reset_reg_reg,
    sig_next_eof_reg,
    E);
  output sig_m_valid_out_reg_0;
  output out;
  output sig_llink_busy_reg;
  output m_axi_rready;
  output \sig_dbeat_cntr_reg[0] ;
  output sig_llink_busy_reg_0;
  output [31:0]dataPacket;
  input m_axi_aclk;
  input sig_rdwr_reset_reg_reg;
  input sig_rdc2rdskid_tlast;
  input sig_init_reg;
  input sig_llink2cmd_rd_busy;
  input m_axi_rvalid;
  input \sig_addr_posted_cntr_reg[0] ;
  input \sig_addr_posted_cntr_reg[0]_0 ;
  input m_axi_rlast;
  input [31:0]m_axi_rdata;
  input sig_rd_llink_enable;
  input sig_rd_discontinue;
  input sig_llink_reset_reg_reg;
  input sig_next_eof_reg;
  input [0:0]E;

  wire [0:0]E;
  wire [31:0]dataPacket;
  wire m_axi_aclk;
  wire [31:0]m_axi_rdata;
  wire m_axi_rlast;
  wire m_axi_rready;
  wire m_axi_rvalid;
  wire \sig_addr_posted_cntr_reg[0] ;
  wire \sig_addr_posted_cntr_reg[0]_0 ;
  wire [31:0]sig_data_skid_mux_out;
  wire [31:0]sig_data_skid_reg;
  wire \sig_dbeat_cntr_reg[0] ;
  wire sig_init_reg;
  wire sig_last_reg_out_i_1_n_0;
  wire sig_last_reg_out_i_2_n_0;
  wire sig_last_skid_reg;
  wire sig_llink2cmd_rd_busy;
  wire sig_llink_busy_reg_0;
  wire sig_llink_reset_reg_reg;
  (* RTL_KEEP = "true" *) (* equivalent_register_removal = "no" *) wire sig_m_valid_dup;
  wire sig_m_valid_dup_i_1_n_0;
  (* RTL_KEEP = "true" *) (* equivalent_register_removal = "no" *) wire sig_m_valid_out;
  wire sig_next_eof_reg;
  wire sig_rd2llink_strm_tlast;
  wire sig_rd_discontinue;
  wire sig_rd_llink_enable;
  wire sig_rdc2rdskid_tlast;
  wire sig_rdwr_reset_reg_reg;
  (* RTL_KEEP = "true" *) (* equivalent_register_removal = "no" *) wire sig_s_ready_dup;
  wire sig_s_ready_dup_i_1_n_0;
  (* RTL_KEEP = "true" *) (* equivalent_register_removal = "no" *) wire sig_s_ready_out;

  assign out = sig_s_ready_out;
  assign sig_llink_busy_reg = sig_m_valid_out;
  assign sig_m_valid_out_reg_0 = sig_m_valid_dup;
  LUT2 #(
    .INIT(4'h2)) 
    m_axi_rready_INST_0
       (.I0(sig_s_ready_out),
        .I1(\sig_addr_posted_cntr_reg[0] ),
        .O(m_axi_rready));
  LUT3 #(
    .INIT(8'hB8)) 
    \sig_data_reg_out[0]_i_1 
       (.I0(m_axi_rdata[0]),
        .I1(sig_s_ready_dup),
        .I2(sig_data_skid_reg[0]),
        .O(sig_data_skid_mux_out[0]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sig_data_reg_out[10]_i_1 
       (.I0(m_axi_rdata[10]),
        .I1(sig_s_ready_dup),
        .I2(sig_data_skid_reg[10]),
        .O(sig_data_skid_mux_out[10]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sig_data_reg_out[11]_i_1 
       (.I0(m_axi_rdata[11]),
        .I1(sig_s_ready_dup),
        .I2(sig_data_skid_reg[11]),
        .O(sig_data_skid_mux_out[11]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sig_data_reg_out[12]_i_1 
       (.I0(m_axi_rdata[12]),
        .I1(sig_s_ready_dup),
        .I2(sig_data_skid_reg[12]),
        .O(sig_data_skid_mux_out[12]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sig_data_reg_out[13]_i_1 
       (.I0(m_axi_rdata[13]),
        .I1(sig_s_ready_dup),
        .I2(sig_data_skid_reg[13]),
        .O(sig_data_skid_mux_out[13]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sig_data_reg_out[14]_i_1 
       (.I0(m_axi_rdata[14]),
        .I1(sig_s_ready_dup),
        .I2(sig_data_skid_reg[14]),
        .O(sig_data_skid_mux_out[14]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sig_data_reg_out[15]_i_1 
       (.I0(m_axi_rdata[15]),
        .I1(sig_s_ready_dup),
        .I2(sig_data_skid_reg[15]),
        .O(sig_data_skid_mux_out[15]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sig_data_reg_out[16]_i_1 
       (.I0(m_axi_rdata[16]),
        .I1(sig_s_ready_dup),
        .I2(sig_data_skid_reg[16]),
        .O(sig_data_skid_mux_out[16]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sig_data_reg_out[17]_i_1 
       (.I0(m_axi_rdata[17]),
        .I1(sig_s_ready_dup),
        .I2(sig_data_skid_reg[17]),
        .O(sig_data_skid_mux_out[17]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sig_data_reg_out[18]_i_1 
       (.I0(m_axi_rdata[18]),
        .I1(sig_s_ready_dup),
        .I2(sig_data_skid_reg[18]),
        .O(sig_data_skid_mux_out[18]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sig_data_reg_out[19]_i_1 
       (.I0(m_axi_rdata[19]),
        .I1(sig_s_ready_dup),
        .I2(sig_data_skid_reg[19]),
        .O(sig_data_skid_mux_out[19]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sig_data_reg_out[1]_i_1 
       (.I0(m_axi_rdata[1]),
        .I1(sig_s_ready_dup),
        .I2(sig_data_skid_reg[1]),
        .O(sig_data_skid_mux_out[1]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sig_data_reg_out[20]_i_1 
       (.I0(m_axi_rdata[20]),
        .I1(sig_s_ready_dup),
        .I2(sig_data_skid_reg[20]),
        .O(sig_data_skid_mux_out[20]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sig_data_reg_out[21]_i_1 
       (.I0(m_axi_rdata[21]),
        .I1(sig_s_ready_dup),
        .I2(sig_data_skid_reg[21]),
        .O(sig_data_skid_mux_out[21]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sig_data_reg_out[22]_i_1 
       (.I0(m_axi_rdata[22]),
        .I1(sig_s_ready_dup),
        .I2(sig_data_skid_reg[22]),
        .O(sig_data_skid_mux_out[22]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sig_data_reg_out[23]_i_1 
       (.I0(m_axi_rdata[23]),
        .I1(sig_s_ready_dup),
        .I2(sig_data_skid_reg[23]),
        .O(sig_data_skid_mux_out[23]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sig_data_reg_out[24]_i_1 
       (.I0(m_axi_rdata[24]),
        .I1(sig_s_ready_dup),
        .I2(sig_data_skid_reg[24]),
        .O(sig_data_skid_mux_out[24]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sig_data_reg_out[25]_i_1 
       (.I0(m_axi_rdata[25]),
        .I1(sig_s_ready_dup),
        .I2(sig_data_skid_reg[25]),
        .O(sig_data_skid_mux_out[25]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sig_data_reg_out[26]_i_1 
       (.I0(m_axi_rdata[26]),
        .I1(sig_s_ready_dup),
        .I2(sig_data_skid_reg[26]),
        .O(sig_data_skid_mux_out[26]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sig_data_reg_out[27]_i_1 
       (.I0(m_axi_rdata[27]),
        .I1(sig_s_ready_dup),
        .I2(sig_data_skid_reg[27]),
        .O(sig_data_skid_mux_out[27]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sig_data_reg_out[28]_i_1 
       (.I0(m_axi_rdata[28]),
        .I1(sig_s_ready_dup),
        .I2(sig_data_skid_reg[28]),
        .O(sig_data_skid_mux_out[28]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sig_data_reg_out[29]_i_1 
       (.I0(m_axi_rdata[29]),
        .I1(sig_s_ready_dup),
        .I2(sig_data_skid_reg[29]),
        .O(sig_data_skid_mux_out[29]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sig_data_reg_out[2]_i_1 
       (.I0(m_axi_rdata[2]),
        .I1(sig_s_ready_dup),
        .I2(sig_data_skid_reg[2]),
        .O(sig_data_skid_mux_out[2]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sig_data_reg_out[30]_i_1 
       (.I0(m_axi_rdata[30]),
        .I1(sig_s_ready_dup),
        .I2(sig_data_skid_reg[30]),
        .O(sig_data_skid_mux_out[30]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sig_data_reg_out[31]_i_2 
       (.I0(m_axi_rdata[31]),
        .I1(sig_s_ready_dup),
        .I2(sig_data_skid_reg[31]),
        .O(sig_data_skid_mux_out[31]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sig_data_reg_out[3]_i_1 
       (.I0(m_axi_rdata[3]),
        .I1(sig_s_ready_dup),
        .I2(sig_data_skid_reg[3]),
        .O(sig_data_skid_mux_out[3]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sig_data_reg_out[4]_i_1 
       (.I0(m_axi_rdata[4]),
        .I1(sig_s_ready_dup),
        .I2(sig_data_skid_reg[4]),
        .O(sig_data_skid_mux_out[4]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sig_data_reg_out[5]_i_1 
       (.I0(m_axi_rdata[5]),
        .I1(sig_s_ready_dup),
        .I2(sig_data_skid_reg[5]),
        .O(sig_data_skid_mux_out[5]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sig_data_reg_out[6]_i_1 
       (.I0(m_axi_rdata[6]),
        .I1(sig_s_ready_dup),
        .I2(sig_data_skid_reg[6]),
        .O(sig_data_skid_mux_out[6]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sig_data_reg_out[7]_i_1 
       (.I0(m_axi_rdata[7]),
        .I1(sig_s_ready_dup),
        .I2(sig_data_skid_reg[7]),
        .O(sig_data_skid_mux_out[7]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sig_data_reg_out[8]_i_1 
       (.I0(m_axi_rdata[8]),
        .I1(sig_s_ready_dup),
        .I2(sig_data_skid_reg[8]),
        .O(sig_data_skid_mux_out[8]));
  LUT3 #(
    .INIT(8'hB8)) 
    \sig_data_reg_out[9]_i_1 
       (.I0(m_axi_rdata[9]),
        .I1(sig_s_ready_dup),
        .I2(sig_data_skid_reg[9]),
        .O(sig_data_skid_mux_out[9]));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_reg_out_reg[0] 
       (.C(m_axi_aclk),
        .CE(E),
        .D(sig_data_skid_mux_out[0]),
        .Q(dataPacket[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_reg_out_reg[10] 
       (.C(m_axi_aclk),
        .CE(E),
        .D(sig_data_skid_mux_out[10]),
        .Q(dataPacket[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_reg_out_reg[11] 
       (.C(m_axi_aclk),
        .CE(E),
        .D(sig_data_skid_mux_out[11]),
        .Q(dataPacket[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_reg_out_reg[12] 
       (.C(m_axi_aclk),
        .CE(E),
        .D(sig_data_skid_mux_out[12]),
        .Q(dataPacket[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_reg_out_reg[13] 
       (.C(m_axi_aclk),
        .CE(E),
        .D(sig_data_skid_mux_out[13]),
        .Q(dataPacket[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_reg_out_reg[14] 
       (.C(m_axi_aclk),
        .CE(E),
        .D(sig_data_skid_mux_out[14]),
        .Q(dataPacket[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_reg_out_reg[15] 
       (.C(m_axi_aclk),
        .CE(E),
        .D(sig_data_skid_mux_out[15]),
        .Q(dataPacket[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_reg_out_reg[16] 
       (.C(m_axi_aclk),
        .CE(E),
        .D(sig_data_skid_mux_out[16]),
        .Q(dataPacket[16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_reg_out_reg[17] 
       (.C(m_axi_aclk),
        .CE(E),
        .D(sig_data_skid_mux_out[17]),
        .Q(dataPacket[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_reg_out_reg[18] 
       (.C(m_axi_aclk),
        .CE(E),
        .D(sig_data_skid_mux_out[18]),
        .Q(dataPacket[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_reg_out_reg[19] 
       (.C(m_axi_aclk),
        .CE(E),
        .D(sig_data_skid_mux_out[19]),
        .Q(dataPacket[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_reg_out_reg[1] 
       (.C(m_axi_aclk),
        .CE(E),
        .D(sig_data_skid_mux_out[1]),
        .Q(dataPacket[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_reg_out_reg[20] 
       (.C(m_axi_aclk),
        .CE(E),
        .D(sig_data_skid_mux_out[20]),
        .Q(dataPacket[20]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_reg_out_reg[21] 
       (.C(m_axi_aclk),
        .CE(E),
        .D(sig_data_skid_mux_out[21]),
        .Q(dataPacket[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_reg_out_reg[22] 
       (.C(m_axi_aclk),
        .CE(E),
        .D(sig_data_skid_mux_out[22]),
        .Q(dataPacket[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_reg_out_reg[23] 
       (.C(m_axi_aclk),
        .CE(E),
        .D(sig_data_skid_mux_out[23]),
        .Q(dataPacket[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_reg_out_reg[24] 
       (.C(m_axi_aclk),
        .CE(E),
        .D(sig_data_skid_mux_out[24]),
        .Q(dataPacket[24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_reg_out_reg[25] 
       (.C(m_axi_aclk),
        .CE(E),
        .D(sig_data_skid_mux_out[25]),
        .Q(dataPacket[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_reg_out_reg[26] 
       (.C(m_axi_aclk),
        .CE(E),
        .D(sig_data_skid_mux_out[26]),
        .Q(dataPacket[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_reg_out_reg[27] 
       (.C(m_axi_aclk),
        .CE(E),
        .D(sig_data_skid_mux_out[27]),
        .Q(dataPacket[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_reg_out_reg[28] 
       (.C(m_axi_aclk),
        .CE(E),
        .D(sig_data_skid_mux_out[28]),
        .Q(dataPacket[28]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_reg_out_reg[29] 
       (.C(m_axi_aclk),
        .CE(E),
        .D(sig_data_skid_mux_out[29]),
        .Q(dataPacket[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_reg_out_reg[2] 
       (.C(m_axi_aclk),
        .CE(E),
        .D(sig_data_skid_mux_out[2]),
        .Q(dataPacket[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_reg_out_reg[30] 
       (.C(m_axi_aclk),
        .CE(E),
        .D(sig_data_skid_mux_out[30]),
        .Q(dataPacket[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_reg_out_reg[31] 
       (.C(m_axi_aclk),
        .CE(E),
        .D(sig_data_skid_mux_out[31]),
        .Q(dataPacket[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_reg_out_reg[3] 
       (.C(m_axi_aclk),
        .CE(E),
        .D(sig_data_skid_mux_out[3]),
        .Q(dataPacket[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_reg_out_reg[4] 
       (.C(m_axi_aclk),
        .CE(E),
        .D(sig_data_skid_mux_out[4]),
        .Q(dataPacket[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_reg_out_reg[5] 
       (.C(m_axi_aclk),
        .CE(E),
        .D(sig_data_skid_mux_out[5]),
        .Q(dataPacket[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_reg_out_reg[6] 
       (.C(m_axi_aclk),
        .CE(E),
        .D(sig_data_skid_mux_out[6]),
        .Q(dataPacket[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_reg_out_reg[7] 
       (.C(m_axi_aclk),
        .CE(E),
        .D(sig_data_skid_mux_out[7]),
        .Q(dataPacket[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_reg_out_reg[8] 
       (.C(m_axi_aclk),
        .CE(E),
        .D(sig_data_skid_mux_out[8]),
        .Q(dataPacket[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_reg_out_reg[9] 
       (.C(m_axi_aclk),
        .CE(E),
        .D(sig_data_skid_mux_out[9]),
        .Q(dataPacket[9]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_skid_reg_reg[0] 
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(m_axi_rdata[0]),
        .Q(sig_data_skid_reg[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_skid_reg_reg[10] 
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(m_axi_rdata[10]),
        .Q(sig_data_skid_reg[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_skid_reg_reg[11] 
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(m_axi_rdata[11]),
        .Q(sig_data_skid_reg[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_skid_reg_reg[12] 
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(m_axi_rdata[12]),
        .Q(sig_data_skid_reg[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_skid_reg_reg[13] 
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(m_axi_rdata[13]),
        .Q(sig_data_skid_reg[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_skid_reg_reg[14] 
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(m_axi_rdata[14]),
        .Q(sig_data_skid_reg[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_skid_reg_reg[15] 
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(m_axi_rdata[15]),
        .Q(sig_data_skid_reg[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_skid_reg_reg[16] 
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(m_axi_rdata[16]),
        .Q(sig_data_skid_reg[16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_skid_reg_reg[17] 
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(m_axi_rdata[17]),
        .Q(sig_data_skid_reg[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_skid_reg_reg[18] 
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(m_axi_rdata[18]),
        .Q(sig_data_skid_reg[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_skid_reg_reg[19] 
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(m_axi_rdata[19]),
        .Q(sig_data_skid_reg[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_skid_reg_reg[1] 
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(m_axi_rdata[1]),
        .Q(sig_data_skid_reg[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_skid_reg_reg[20] 
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(m_axi_rdata[20]),
        .Q(sig_data_skid_reg[20]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_skid_reg_reg[21] 
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(m_axi_rdata[21]),
        .Q(sig_data_skid_reg[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_skid_reg_reg[22] 
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(m_axi_rdata[22]),
        .Q(sig_data_skid_reg[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_skid_reg_reg[23] 
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(m_axi_rdata[23]),
        .Q(sig_data_skid_reg[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_skid_reg_reg[24] 
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(m_axi_rdata[24]),
        .Q(sig_data_skid_reg[24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_skid_reg_reg[25] 
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(m_axi_rdata[25]),
        .Q(sig_data_skid_reg[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_skid_reg_reg[26] 
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(m_axi_rdata[26]),
        .Q(sig_data_skid_reg[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_skid_reg_reg[27] 
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(m_axi_rdata[27]),
        .Q(sig_data_skid_reg[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_skid_reg_reg[28] 
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(m_axi_rdata[28]),
        .Q(sig_data_skid_reg[28]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_skid_reg_reg[29] 
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(m_axi_rdata[29]),
        .Q(sig_data_skid_reg[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_skid_reg_reg[2] 
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(m_axi_rdata[2]),
        .Q(sig_data_skid_reg[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_skid_reg_reg[30] 
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(m_axi_rdata[30]),
        .Q(sig_data_skid_reg[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_skid_reg_reg[31] 
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(m_axi_rdata[31]),
        .Q(sig_data_skid_reg[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_skid_reg_reg[3] 
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(m_axi_rdata[3]),
        .Q(sig_data_skid_reg[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_skid_reg_reg[4] 
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(m_axi_rdata[4]),
        .Q(sig_data_skid_reg[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_skid_reg_reg[5] 
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(m_axi_rdata[5]),
        .Q(sig_data_skid_reg[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_skid_reg_reg[6] 
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(m_axi_rdata[6]),
        .Q(sig_data_skid_reg[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_skid_reg_reg[7] 
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(m_axi_rdata[7]),
        .Q(sig_data_skid_reg[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_skid_reg_reg[8] 
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(m_axi_rdata[8]),
        .Q(sig_data_skid_reg[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \sig_data_skid_reg_reg[9] 
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(m_axi_rdata[9]),
        .Q(sig_data_skid_reg[9]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h2F20FFFF2F200000)) 
    sig_last_reg_out_i_1
       (.I0(sig_next_eof_reg),
        .I1(sig_last_reg_out_i_2_n_0),
        .I2(sig_s_ready_dup),
        .I3(sig_last_skid_reg),
        .I4(E),
        .I5(sig_rd2llink_strm_tlast),
        .O(sig_last_reg_out_i_1_n_0));
  LUT2 #(
    .INIT(4'h7)) 
    sig_last_reg_out_i_2
       (.I0(m_axi_rlast),
        .I1(m_axi_rvalid),
        .O(sig_last_reg_out_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    sig_last_reg_out_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_last_reg_out_i_1_n_0),
        .Q(sig_rd2llink_strm_tlast),
        .R(sig_rdwr_reset_reg_reg));
  FDRE #(
    .INIT(1'b0)) 
    sig_last_skid_reg_reg
       (.C(m_axi_aclk),
        .CE(sig_s_ready_dup),
        .D(sig_rdc2rdskid_tlast),
        .Q(sig_last_skid_reg),
        .R(sig_rdwr_reset_reg_reg));
  LUT6 #(
    .INIT(64'h0000000000F0F7F0)) 
    sig_llink_busy_i_1
       (.I0(sig_rd2llink_strm_tlast),
        .I1(sig_m_valid_out),
        .I2(sig_rd_llink_enable),
        .I3(sig_llink2cmd_rd_busy),
        .I4(sig_rd_discontinue),
        .I5(sig_llink_reset_reg_reg),
        .O(sig_llink_busy_reg_0));
  LUT6 #(
    .INIT(64'h0010101011111111)) 
    sig_m_valid_dup_i_1
       (.I0(sig_init_reg),
        .I1(sig_rdwr_reset_reg_reg),
        .I2(sig_m_valid_dup),
        .I3(sig_s_ready_dup),
        .I4(sig_llink2cmd_rd_busy),
        .I5(\sig_addr_posted_cntr_reg[0]_0 ),
        .O(sig_m_valid_dup_i_1_n_0));
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    sig_m_valid_dup_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_m_valid_dup_i_1_n_0),
        .Q(sig_m_valid_dup),
        .R(1'b0));
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    sig_m_valid_out_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_m_valid_dup_i_1_n_0),
        .Q(sig_m_valid_out),
        .R(1'b0));
  LUT3 #(
    .INIT(8'h08)) 
    sig_next_cmd_cmplt_reg_i_3
       (.I0(sig_s_ready_out),
        .I1(m_axi_rvalid),
        .I2(\sig_addr_posted_cntr_reg[0] ),
        .O(\sig_dbeat_cntr_reg[0] ));
  LUT6 #(
    .INIT(64'hFEFEFEFEEEFEFEFE)) 
    sig_s_ready_dup_i_1
       (.I0(sig_init_reg),
        .I1(sig_llink2cmd_rd_busy),
        .I2(sig_s_ready_dup),
        .I3(sig_m_valid_dup),
        .I4(m_axi_rvalid),
        .I5(\sig_addr_posted_cntr_reg[0] ),
        .O(sig_s_ready_dup_i_1_n_0));
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    sig_s_ready_dup_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_s_ready_dup_i_1_n_0),
        .Q(sig_s_ready_dup),
        .R(sig_rdwr_reset_reg_reg));
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    sig_s_ready_out_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_s_ready_dup_i_1_n_0),
        .Q(sig_s_ready_out),
        .R(sig_rdwr_reset_reg_reg));
endmodule

(* ORIG_REF_NAME = "axi_master_burst_skid_buf" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axi_master_burst_skid_buf_0
   (out,
    m_axi_aclk,
    sig_rdwr_reset_reg_reg,
    sig_init_reg_reg,
    sig_init_reg);
  output out;
  input m_axi_aclk;
  input sig_rdwr_reset_reg_reg;
  input sig_init_reg_reg;
  input sig_init_reg;

  wire m_axi_aclk;
  wire sig_init_reg;
  wire sig_init_reg_reg;
  (* RTL_KEEP = "true" *) (* equivalent_register_removal = "no" *) wire sig_m_valid_dup;
  wire sig_m_valid_dup_i_1__0_n_0;
  (* RTL_KEEP = "true" *) (* equivalent_register_removal = "no" *) wire sig_m_valid_out;
  wire sig_rdwr_reset_reg_reg;
  (* RTL_KEEP = "true" *) (* equivalent_register_removal = "no" *) wire sig_s_ready_dup;
  (* RTL_KEEP = "true" *) (* equivalent_register_removal = "no" *) wire sig_s_ready_out;

  assign out = sig_s_ready_dup;
  LUT3 #(
    .INIT(8'h02)) 
    sig_m_valid_dup_i_1__0
       (.I0(sig_m_valid_dup),
        .I1(sig_init_reg),
        .I2(sig_rdwr_reset_reg_reg),
        .O(sig_m_valid_dup_i_1__0_n_0));
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    sig_m_valid_dup_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_m_valid_dup_i_1__0_n_0),
        .Q(sig_m_valid_dup),
        .R(1'b0));
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    sig_m_valid_out_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_m_valid_dup_i_1__0_n_0),
        .Q(sig_m_valid_out),
        .R(1'b0));
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    sig_s_ready_dup_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_init_reg_reg),
        .Q(sig_s_ready_dup),
        .R(sig_rdwr_reset_reg_reg));
  (* KEEP = "yes" *) 
  (* equivalent_register_removal = "no" *) 
  FDRE #(
    .INIT(1'b0)) 
    sig_s_ready_out_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(sig_init_reg_reg),
        .Q(sig_s_ready_out),
        .R(sig_rdwr_reset_reg_reg));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axi_master_burst_wr_status_cntl
   (sig_init_reg,
    sig_s_ready_out_reg,
    m_axi_bready,
    out,
    m_axi_aclk,
    m_axi_bvalid,
    sig_s_ready_dup_reg);
  output sig_init_reg;
  output sig_s_ready_out_reg;
  output m_axi_bready;
  input out;
  input m_axi_aclk;
  input m_axi_bvalid;
  input sig_s_ready_dup_reg;

  wire m_axi_aclk;
  wire m_axi_bready;
  wire m_axi_bvalid;
  wire out;
  wire sig_init_reg;
  wire sig_s_ready_dup_reg;
  wire sig_s_ready_out_reg;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axi_master_burst_fifo I_WRESP_STATUS_FIFO
       (.m_axi_aclk(m_axi_aclk),
        .m_axi_bready(m_axi_bready),
        .m_axi_bvalid(m_axi_bvalid),
        .out(out),
        .sig_init_reg2_reg_0(sig_init_reg),
        .sig_s_ready_dup_reg(sig_s_ready_dup_reg),
        .sig_s_ready_out_reg(sig_s_ready_out_reg));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_cntr_incr_decr_addn_f
   (Q,
    sig_inhibit_rdy_n,
    FIFO_Full_reg,
    m_axi_bvalid,
    out,
    m_axi_aclk);
  output [2:0]Q;
  input sig_inhibit_rdy_n;
  input FIFO_Full_reg;
  input m_axi_bvalid;
  input out;
  input m_axi_aclk;

  wire FIFO_Full_reg;
  wire \INFERRED_GEN.cnt_i[1]_i_1_n_0 ;
  wire \INFERRED_GEN.cnt_i[2]_i_1_n_0 ;
  wire [2:0]Q;
  wire [0:0]addr_i_p1;
  wire m_axi_aclk;
  wire m_axi_bvalid;
  wire out;
  wire sig_inhibit_rdy_n;

  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT4 #(
    .INIT(16'hA6AA)) 
    Cnt_p10
       (.I0(Q[0]),
        .I1(sig_inhibit_rdy_n),
        .I2(FIFO_Full_reg),
        .I3(m_axi_bvalid),
        .O(addr_i_p1));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT5 #(
    .INIT(32'hF7FF0800)) 
    \INFERRED_GEN.cnt_i[1]_i_1 
       (.I0(Q[0]),
        .I1(m_axi_bvalid),
        .I2(FIFO_Full_reg),
        .I3(sig_inhibit_rdy_n),
        .I4(Q[1]),
        .O(\INFERRED_GEN.cnt_i[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hF7FFFFFF08000000)) 
    \INFERRED_GEN.cnt_i[2]_i_1 
       (.I0(Q[1]),
        .I1(sig_inhibit_rdy_n),
        .I2(FIFO_Full_reg),
        .I3(m_axi_bvalid),
        .I4(Q[0]),
        .I5(Q[2]),
        .O(\INFERRED_GEN.cnt_i[2]_i_1_n_0 ));
  FDSE \INFERRED_GEN.cnt_i_reg[0] 
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(addr_i_p1),
        .Q(Q[0]),
        .S(out));
  FDSE \INFERRED_GEN.cnt_i_reg[1] 
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(\INFERRED_GEN.cnt_i[1]_i_1_n_0 ),
        .Q(Q[1]),
        .S(out));
  FDSE \INFERRED_GEN.cnt_i_reg[2] 
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(\INFERRED_GEN.cnt_i[2]_i_1_n_0 ),
        .Q(Q[2]),
        .S(out));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_axiDataFetcher_0_1,axiDataFetcher,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "axiDataFetcher,Vivado 2017.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (ena,
    dataType,
    dataId,
    dataPacket,
    fetching,
    m_axi_arready,
    m_axi_arvalid,
    m_axi_araddr,
    m_axi_arlen,
    m_axi_arsize,
    m_axi_arburst,
    m_axi_arprot,
    m_axi_arcache,
    m_axi_rready,
    m_axi_rvalid,
    m_axi_rdata,
    m_axi_rresp,
    m_axi_rlast,
    m_axi_awready,
    m_axi_awvalid,
    m_axi_awaddr,
    m_axi_awlen,
    m_axi_awsize,
    m_axi_awburst,
    m_axi_awprot,
    m_axi_awcache,
    m_axi_wready,
    m_axi_wvalid,
    m_axi_wdata,
    m_axi_wstrb,
    m_axi_wlast,
    m_axi_bready,
    m_axi_bvalid,
    m_axi_bresp,
    m_axi_aclk,
    m_axi_aresetn);
  input ena;
  input dataType;
  input [7:0]dataId;
  output [31:0]dataPacket;
  output fetching;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi ARREADY" *) input m_axi_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi ARVALID" *) output m_axi_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi ARADDR" *) output [31:0]m_axi_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi ARLEN" *) output [7:0]m_axi_arlen;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi ARSIZE" *) output [2:0]m_axi_arsize;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi ARBURST" *) output [1:0]m_axi_arburst;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi ARPROT" *) output [2:0]m_axi_arprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi ARCACHE" *) output [3:0]m_axi_arcache;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi RREADY" *) output m_axi_rready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi RVALID" *) input m_axi_rvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi RDATA" *) input [31:0]m_axi_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi RRESP" *) input [1:0]m_axi_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi RLAST" *) input m_axi_rlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi AWREADY" *) input m_axi_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi AWVALID" *) output m_axi_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi AWADDR" *) output [31:0]m_axi_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi AWLEN" *) output [7:0]m_axi_awlen;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi AWSIZE" *) output [2:0]m_axi_awsize;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi AWBURST" *) output [1:0]m_axi_awburst;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi AWPROT" *) output [2:0]m_axi_awprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi AWCACHE" *) output [3:0]m_axi_awcache;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi WREADY" *) input m_axi_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi WVALID" *) output m_axi_wvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi WDATA" *) output [31:0]m_axi_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi WSTRB" *) output [7:0]m_axi_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi WLAST" *) output m_axi_wlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi BREADY" *) output m_axi_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi BVALID" *) input m_axi_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m_axi BRESP" *) input [1:0]m_axi_bresp;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 m_axi_aclk CLK" *) input m_axi_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 m_axi_aresetn RST" *) input m_axi_aresetn;

  wire \<const0> ;
  wire \<const1> ;
  wire [7:0]dataId;
  wire [31:0]dataPacket;
  wire dataType;
  wire ena;
  wire fetching;
  wire m_axi_aclk;
  wire [31:0]m_axi_araddr;
  wire [0:0]\^m_axi_arburst ;
  wire m_axi_aresetn;
  wire [7:0]m_axi_arlen;
  wire m_axi_arready;
  wire [1:1]\^m_axi_arsize ;
  wire m_axi_arvalid;
  wire m_axi_bready;
  wire m_axi_bvalid;
  wire [31:0]m_axi_rdata;
  wire m_axi_rlast;
  wire m_axi_rready;
  wire m_axi_rvalid;
  wire m_axi_wready;
  wire [2:2]\^m_axi_wstrb ;
  wire m_axi_wvalid;

  assign m_axi_arburst[1] = \<const0> ;
  assign m_axi_arburst[0] = \^m_axi_arburst [0];
  assign m_axi_arcache[3] = \<const0> ;
  assign m_axi_arcache[2] = \<const0> ;
  assign m_axi_arcache[1] = \<const1> ;
  assign m_axi_arcache[0] = \<const1> ;
  assign m_axi_arprot[2] = \<const0> ;
  assign m_axi_arprot[1] = \<const0> ;
  assign m_axi_arprot[0] = \<const0> ;
  assign m_axi_arsize[2] = \<const0> ;
  assign m_axi_arsize[1] = \^m_axi_arsize [1];
  assign m_axi_arsize[0] = \<const0> ;
  assign m_axi_awaddr[31:0] = m_axi_araddr;
  assign m_axi_awburst[1] = \<const0> ;
  assign m_axi_awburst[0] = \^m_axi_arburst [0];
  assign m_axi_awcache[3] = \<const0> ;
  assign m_axi_awcache[2] = \<const0> ;
  assign m_axi_awcache[1] = \<const1> ;
  assign m_axi_awcache[0] = \<const1> ;
  assign m_axi_awlen[7:0] = m_axi_arlen;
  assign m_axi_awprot[2] = \<const0> ;
  assign m_axi_awprot[1] = \<const0> ;
  assign m_axi_awprot[0] = \<const0> ;
  assign m_axi_awsize[2] = \<const0> ;
  assign m_axi_awsize[1] = \^m_axi_arsize [1];
  assign m_axi_awsize[0] = \<const0> ;
  assign m_axi_awvalid = \<const0> ;
  assign m_axi_wdata[31] = \<const0> ;
  assign m_axi_wdata[30] = \<const0> ;
  assign m_axi_wdata[29] = \<const0> ;
  assign m_axi_wdata[28] = \<const0> ;
  assign m_axi_wdata[27] = \<const0> ;
  assign m_axi_wdata[26] = \<const0> ;
  assign m_axi_wdata[25] = \<const0> ;
  assign m_axi_wdata[24] = \<const0> ;
  assign m_axi_wdata[23] = \<const0> ;
  assign m_axi_wdata[22] = \<const0> ;
  assign m_axi_wdata[21] = \<const0> ;
  assign m_axi_wdata[20] = \<const0> ;
  assign m_axi_wdata[19] = \<const0> ;
  assign m_axi_wdata[18] = \<const0> ;
  assign m_axi_wdata[17] = \<const0> ;
  assign m_axi_wdata[16] = \<const0> ;
  assign m_axi_wdata[15] = \<const0> ;
  assign m_axi_wdata[14] = \<const0> ;
  assign m_axi_wdata[13] = \<const0> ;
  assign m_axi_wdata[12] = \<const0> ;
  assign m_axi_wdata[11] = \<const0> ;
  assign m_axi_wdata[10] = \<const0> ;
  assign m_axi_wdata[9] = \<const0> ;
  assign m_axi_wdata[8] = \<const0> ;
  assign m_axi_wdata[7] = \<const0> ;
  assign m_axi_wdata[6] = \<const0> ;
  assign m_axi_wdata[5] = \<const0> ;
  assign m_axi_wdata[4] = \<const0> ;
  assign m_axi_wdata[3] = \<const0> ;
  assign m_axi_wdata[2] = \<const0> ;
  assign m_axi_wdata[1] = \<const0> ;
  assign m_axi_wdata[0] = \<const0> ;
  assign m_axi_wlast = \<const0> ;
  assign m_axi_wstrb[7] = \<const0> ;
  assign m_axi_wstrb[6] = \<const0> ;
  assign m_axi_wstrb[5] = \<const0> ;
  assign m_axi_wstrb[4] = \<const0> ;
  assign m_axi_wstrb[3] = \^m_axi_wstrb [2];
  assign m_axi_wstrb[2] = \^m_axi_wstrb [2];
  assign m_axi_wstrb[1] = \^m_axi_wstrb [2];
  assign m_axi_wstrb[0] = \^m_axi_wstrb [2];
  GND GND
       (.G(\<const0> ));
  VCC VCC
       (.P(\<const1> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_axiDataFetcher inst
       (.dataId(dataId),
        .dataPacket(dataPacket),
        .dataType(dataType),
        .ena(ena),
        .fetching(fetching),
        .m_axi_aclk(m_axi_aclk),
        .m_axi_araddr(m_axi_araddr),
        .m_axi_arburst(\^m_axi_arburst ),
        .m_axi_aresetn(m_axi_aresetn),
        .m_axi_arlen(m_axi_arlen),
        .m_axi_arready(m_axi_arready),
        .m_axi_arsize(\^m_axi_arsize ),
        .m_axi_arvalid(m_axi_arvalid),
        .m_axi_bready(m_axi_bready),
        .m_axi_bvalid(m_axi_bvalid),
        .m_axi_rdata(m_axi_rdata),
        .m_axi_rlast(m_axi_rlast),
        .m_axi_rready(m_axi_rready),
        .m_axi_rvalid(m_axi_rvalid),
        .m_axi_wready(m_axi_wready),
        .m_axi_wstrb(\^m_axi_wstrb ),
        .m_axi_wvalid(m_axi_wvalid));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_srl_fifo_f
   (m_axi_bready,
    out,
    m_axi_aclk,
    m_axi_bvalid,
    sig_inhibit_rdy_n);
  output m_axi_bready;
  input out;
  input m_axi_aclk;
  input m_axi_bvalid;
  input sig_inhibit_rdy_n;

  wire m_axi_aclk;
  wire m_axi_bready;
  wire m_axi_bvalid;
  wire out;
  wire sig_inhibit_rdy_n;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_srl_fifo_rbu_f I_SRL_FIFO_RBU_F
       (.m_axi_aclk(m_axi_aclk),
        .m_axi_bready(m_axi_bready),
        .m_axi_bvalid(m_axi_bvalid),
        .out(out),
        .sig_inhibit_rdy_n(sig_inhibit_rdy_n));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_srl_fifo_rbu_f
   (m_axi_bready,
    out,
    m_axi_aclk,
    m_axi_bvalid,
    sig_inhibit_rdy_n);
  output m_axi_bready;
  input out;
  input m_axi_aclk;
  input m_axi_bvalid;
  input sig_inhibit_rdy_n;

  wire CNTR_INCR_DECR_ADDN_F_I_n_1;
  wire CNTR_INCR_DECR_ADDN_F_I_n_2;
  wire FIFO_Full_reg_n_0;
  wire fifo_full_p1__0;
  wire m_axi_aclk;
  wire m_axi_bready;
  wire m_axi_bvalid;
  wire out;
  wire sig_inhibit_rdy_n;
  wire sig_rd_empty;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_cntr_incr_decr_addn_f CNTR_INCR_DECR_ADDN_F_I
       (.FIFO_Full_reg(FIFO_Full_reg_n_0),
        .Q({sig_rd_empty,CNTR_INCR_DECR_ADDN_F_I_n_1,CNTR_INCR_DECR_ADDN_F_I_n_2}),
        .m_axi_aclk(m_axi_aclk),
        .m_axi_bvalid(m_axi_bvalid),
        .out(out),
        .sig_inhibit_rdy_n(sig_inhibit_rdy_n));
  FDRE FIFO_Full_reg
       (.C(m_axi_aclk),
        .CE(1'b1),
        .D(fifo_full_p1__0),
        .Q(FIFO_Full_reg_n_0),
        .R(out));
  LUT6 #(
    .INIT(64'h000000000400A2AA)) 
    fifo_full_p1
       (.I0(CNTR_INCR_DECR_ADDN_F_I_n_1),
        .I1(sig_inhibit_rdy_n),
        .I2(FIFO_Full_reg_n_0),
        .I3(m_axi_bvalid),
        .I4(CNTR_INCR_DECR_ADDN_F_I_n_2),
        .I5(sig_rd_empty),
        .O(fifo_full_p1__0));
  LUT2 #(
    .INIT(4'h2)) 
    m_axi_bready_INST_0
       (.I0(sig_inhibit_rdy_n),
        .I1(FIFO_Full_reg_n_0),
        .O(m_axi_bready));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
