// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.1 (lin64) Build 1846317 Fri Apr 14 18:54:47 MDT 2017
// Date        : Fri Jun 16 13:47:10 2017
// Host        : surprise running 64-bit Linux Mint 18.1 Serena
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_rendVgaTmBoot_0_2_sim_netlist.v
// Design      : design_1_rendVgaTmBoot_0_2
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_booting
   (fetching_sprites,
    WEA,
    fetch,
    data_type,
    led0,
    led1,
    led2,
    led3,
    O,
    \Xmap_reg[4]_0 ,
    \Xmap_reg[4]_1 ,
    \Xmap_reg[4]_2 ,
    \Xmap_reg[4]_3 ,
    \Xmap_reg[4]_4 ,
    \Xmap_reg[4]_5 ,
    \Xmap_reg[4]_6 ,
    \Xmap_reg[4]_7 ,
    \Xmap_reg[4]_8 ,
    \Xmap_reg[4]_9 ,
    \Xmap_reg[4]_10 ,
    \Xmap_reg[4]_11 ,
    \Xmap_reg[4]_12 ,
    \Xmap_reg[4]_13 ,
    \Xmap_reg[4]_14 ,
    \Xmap_reg[4]_15 ,
    \Xmap_reg[4]_16 ,
    \Xmap_reg[4]_17 ,
    \Xmap_reg[4]_18 ,
    \Xmap_reg[4]_19 ,
    \Xmap_reg[4]_20 ,
    \Xmap_reg[4]_21 ,
    \Xmap_reg[4]_22 ,
    \Xmap_reg[4]_23 ,
    \Xmap_reg[4]_24 ,
    \Xmap_reg[4]_25 ,
    \ind_reg[0] ,
    Q,
    tm_reg_0,
    ADDRARDADDR,
    tm_reg_0_0,
    D,
    tm_reg_0_1,
    pixel,
    map_id,
    clk,
    packet_in,
    fetching,
    S,
    \cnt_reg[2]_0 ,
    \cnt_reg[17]_0 ,
    \cnt_reg[21]_0 ,
    \cnt_reg[20]_0 ,
    \cnt_reg[1]_0 ,
    \cnt_reg[1]_1 ,
    DI,
    \cnt_reg[2]_1 ,
    \cnt_reg[17]_1 ,
    \cnt_reg[2]_2 ,
    \cnt_reg[20]_1 ,
    \cnt_reg[20]_2 ,
    O34,
    \cnt_reg[20]_3 ,
    \Ymap_reg[2]_0 ,
    \Ymap_reg[3]_0 ,
    \tmp_rand_reg[6]_0 ,
    sw);
  output fetching_sprites;
  output [0:0]WEA;
  output fetch;
  output data_type;
  output led0;
  output led1;
  output led2;
  output led3;
  output [3:0]O;
  output [3:0]\Xmap_reg[4]_0 ;
  output [3:0]\Xmap_reg[4]_1 ;
  output [2:0]\Xmap_reg[4]_2 ;
  output [2:0]\Xmap_reg[4]_3 ;
  output [3:0]\Xmap_reg[4]_4 ;
  output [0:0]\Xmap_reg[4]_5 ;
  output [2:0]\Xmap_reg[4]_6 ;
  output [3:0]\Xmap_reg[4]_7 ;
  output [3:0]\Xmap_reg[4]_8 ;
  output [3:0]\Xmap_reg[4]_9 ;
  output [0:0]\Xmap_reg[4]_10 ;
  output [3:0]\Xmap_reg[4]_11 ;
  output [3:0]\Xmap_reg[4]_12 ;
  output [3:0]\Xmap_reg[4]_13 ;
  output [0:0]\Xmap_reg[4]_14 ;
  output [3:0]\Xmap_reg[4]_15 ;
  output [3:0]\Xmap_reg[4]_16 ;
  output [3:0]\Xmap_reg[4]_17 ;
  output [3:0]\Xmap_reg[4]_18 ;
  output [3:0]\Xmap_reg[4]_19 ;
  output [3:0]\Xmap_reg[4]_20 ;
  output [3:0]\Xmap_reg[4]_21 ;
  output [3:0]\Xmap_reg[4]_22 ;
  output [3:0]\Xmap_reg[4]_23 ;
  output [0:0]\Xmap_reg[4]_24 ;
  output [2:0]\Xmap_reg[4]_25 ;
  output \ind_reg[0] ;
  output [0:0]Q;
  output [6:0]tm_reg_0;
  output [11:0]ADDRARDADDR;
  output [2:0]tm_reg_0_0;
  output [6:0]D;
  output [3:0]tm_reg_0_1;
  output [5:0]pixel;
  output [6:0]map_id;
  input clk;
  input [5:0]packet_in;
  input fetching;
  input [2:0]S;
  input [3:0]\cnt_reg[2]_0 ;
  input [3:0]\cnt_reg[17]_0 ;
  input [3:0]\cnt_reg[21]_0 ;
  input [0:0]\cnt_reg[20]_0 ;
  input [3:0]\cnt_reg[1]_0 ;
  input [3:0]\cnt_reg[1]_1 ;
  input [3:0]DI;
  input [3:0]\cnt_reg[2]_1 ;
  input [3:0]\cnt_reg[17]_1 ;
  input [3:0]\cnt_reg[2]_2 ;
  input [1:0]\cnt_reg[20]_1 ;
  input [2:0]\cnt_reg[20]_2 ;
  input [0:0]O34;
  input [2:0]\cnt_reg[20]_3 ;
  input [3:0]\Ymap_reg[2]_0 ;
  input [3:0]\Ymap_reg[3]_0 ;
  input [6:0]\tmp_rand_reg[6]_0 ;
  input [2:0]sw;

  wire [11:0]ADDRARDADDR;
  wire [6:0]D;
  wire [3:0]DI;
  wire \FSM_sequential_state[0]_i_1_n_0 ;
  wire \FSM_sequential_state[1]_i_1_n_0 ;
  wire \FSM_sequential_state[2]_i_10_n_0 ;
  wire \FSM_sequential_state[2]_i_12_n_0 ;
  wire \FSM_sequential_state[2]_i_13_n_0 ;
  wire \FSM_sequential_state[2]_i_14_n_0 ;
  wire \FSM_sequential_state[2]_i_15_n_0 ;
  wire \FSM_sequential_state[2]_i_16_n_0 ;
  wire \FSM_sequential_state[2]_i_17_n_0 ;
  wire \FSM_sequential_state[2]_i_18_n_0 ;
  wire \FSM_sequential_state[2]_i_1_n_0 ;
  wire \FSM_sequential_state[2]_i_3_n_0 ;
  wire \FSM_sequential_state[2]_i_4_n_0 ;
  wire \FSM_sequential_state[2]_i_5_n_0 ;
  wire \FSM_sequential_state[2]_i_6_n_0 ;
  wire \FSM_sequential_state[2]_i_7_n_0 ;
  wire \FSM_sequential_state[2]_i_8_n_0 ;
  wire \FSM_sequential_state[2]_i_9_n_0 ;
  wire \FSM_sequential_state_reg[2]_i_2_n_0 ;
  wire [3:0]O;
  wire [0:0]O34;
  wire [0:0]Q;
  wire [2:0]S;
  wire [0:0]WEA;
  wire \Xmap[3]_i_2_n_0 ;
  wire \Xmap[3]_i_3_n_0 ;
  wire \Xmap[3]_i_4_n_0 ;
  wire \Xmap[3]_i_5_n_0 ;
  wire \Xmap[4]_i_1_n_0 ;
  wire \Xmap[5]_i_1_n_0 ;
  wire \Xmap[6]_i_107_n_0 ;
  wire \Xmap[6]_i_108_n_0 ;
  wire \Xmap[6]_i_109_n_0 ;
  wire \Xmap[6]_i_110_n_0 ;
  wire \Xmap[6]_i_111_n_0 ;
  wire \Xmap[6]_i_112_n_0 ;
  wire \Xmap[6]_i_114_n_0 ;
  wire \Xmap[6]_i_115_n_0 ;
  wire \Xmap[6]_i_116_n_0 ;
  wire \Xmap[6]_i_118_n_0 ;
  wire \Xmap[6]_i_119_n_0 ;
  wire \Xmap[6]_i_120_n_0 ;
  wire \Xmap[6]_i_121_n_0 ;
  wire \Xmap[6]_i_122_n_0 ;
  wire \Xmap[6]_i_123_n_0 ;
  wire \Xmap[6]_i_124_n_0 ;
  wire \Xmap[6]_i_130_n_0 ;
  wire \Xmap[6]_i_131_n_0 ;
  wire \Xmap[6]_i_132_n_0 ;
  wire \Xmap[6]_i_133_n_0 ;
  wire \Xmap[6]_i_134_n_0 ;
  wire \Xmap[6]_i_135_n_0 ;
  wire \Xmap[6]_i_136_n_0 ;
  wire \Xmap[6]_i_140_n_0 ;
  wire \Xmap[6]_i_141_n_0 ;
  wire \Xmap[6]_i_142_n_0 ;
  wire \Xmap[6]_i_143_n_0 ;
  wire \Xmap[6]_i_144_n_0 ;
  wire \Xmap[6]_i_145_n_0 ;
  wire \Xmap[6]_i_151_n_0 ;
  wire \Xmap[6]_i_152_n_0 ;
  wire \Xmap[6]_i_153_n_0 ;
  wire \Xmap[6]_i_154_n_0 ;
  wire \Xmap[6]_i_155_n_0 ;
  wire \Xmap[6]_i_156_n_0 ;
  wire \Xmap[6]_i_157_n_0 ;
  wire \Xmap[6]_i_158_n_0 ;
  wire \Xmap[6]_i_159_n_0 ;
  wire \Xmap[6]_i_160_n_0 ;
  wire \Xmap[6]_i_161_n_0 ;
  wire \Xmap[6]_i_162_n_0 ;
  wire \Xmap[6]_i_163_n_0 ;
  wire \Xmap[6]_i_164_n_0 ;
  wire \Xmap[6]_i_165_n_0 ;
  wire \Xmap[6]_i_166_n_0 ;
  wire \Xmap[6]_i_167_n_0 ;
  wire \Xmap[6]_i_168_n_0 ;
  wire \Xmap[6]_i_169_n_0 ;
  wire \Xmap[6]_i_170_n_0 ;
  wire \Xmap[6]_i_171_n_0 ;
  wire \Xmap[6]_i_172_n_0 ;
  wire \Xmap[6]_i_173_n_0 ;
  wire \Xmap[6]_i_174_n_0 ;
  wire \Xmap[6]_i_175_n_0 ;
  wire \Xmap[6]_i_176_n_0 ;
  wire \Xmap[6]_i_178_n_0 ;
  wire \Xmap[6]_i_179_n_0 ;
  wire \Xmap[6]_i_180_n_0 ;
  wire \Xmap[6]_i_181_n_0 ;
  wire \Xmap[6]_i_182_n_0 ;
  wire \Xmap[6]_i_183_n_0 ;
  wire \Xmap[6]_i_184_n_0 ;
  wire \Xmap[6]_i_185_n_0 ;
  wire \Xmap[6]_i_186_n_0 ;
  wire \Xmap[6]_i_187_n_0 ;
  wire \Xmap[6]_i_188_n_0 ;
  wire \Xmap[6]_i_189_n_0 ;
  wire \Xmap[6]_i_190_n_0 ;
  wire \Xmap[6]_i_191_n_0 ;
  wire \Xmap[6]_i_192_n_0 ;
  wire \Xmap[6]_i_193_n_0 ;
  wire \Xmap[6]_i_194_n_0 ;
  wire \Xmap[6]_i_195_n_0 ;
  wire \Xmap[6]_i_196_n_0 ;
  wire \Xmap[6]_i_197_n_0 ;
  wire \Xmap[6]_i_198_n_0 ;
  wire \Xmap[6]_i_1_n_0 ;
  wire \Xmap[6]_i_200_n_0 ;
  wire \Xmap[6]_i_201_n_0 ;
  wire \Xmap[6]_i_202_n_0 ;
  wire \Xmap[6]_i_203_n_0 ;
  wire \Xmap[6]_i_204_n_0 ;
  wire \Xmap[6]_i_205_n_0 ;
  wire \Xmap[6]_i_206_n_0 ;
  wire \Xmap[6]_i_212_n_0 ;
  wire \Xmap[6]_i_213_n_0 ;
  wire \Xmap[6]_i_214_n_0 ;
  wire \Xmap[6]_i_215_n_0 ;
  wire \Xmap[6]_i_216_n_0 ;
  wire \Xmap[6]_i_217_n_0 ;
  wire \Xmap[6]_i_218_n_0 ;
  wire \Xmap[6]_i_219_n_0 ;
  wire \Xmap[6]_i_220_n_0 ;
  wire \Xmap[6]_i_221_n_0 ;
  wire \Xmap[6]_i_223_n_0 ;
  wire \Xmap[6]_i_224_n_0 ;
  wire \Xmap[6]_i_225_n_0 ;
  wire \Xmap[6]_i_226_n_0 ;
  wire \Xmap[6]_i_227_n_0 ;
  wire \Xmap[6]_i_234_n_0 ;
  wire \Xmap[6]_i_235_n_0 ;
  wire \Xmap[6]_i_236_n_0 ;
  wire \Xmap[6]_i_237_n_0 ;
  wire \Xmap[6]_i_239_n_0 ;
  wire \Xmap[6]_i_240_n_0 ;
  wire \Xmap[6]_i_241_n_0 ;
  wire \Xmap[6]_i_242_n_0 ;
  wire \Xmap[6]_i_247_n_0 ;
  wire \Xmap[6]_i_248_n_0 ;
  wire \Xmap[6]_i_249_n_0 ;
  wire \Xmap[6]_i_250_n_0 ;
  wire \Xmap[6]_i_251_n_0 ;
  wire \Xmap[6]_i_252_n_0 ;
  wire \Xmap[6]_i_253_n_0 ;
  wire \Xmap[6]_i_254_n_0 ;
  wire \Xmap[6]_i_255_n_0 ;
  wire \Xmap[6]_i_256_n_0 ;
  wire \Xmap[6]_i_257_n_0 ;
  wire \Xmap[6]_i_258_n_0 ;
  wire \Xmap[6]_i_259_n_0 ;
  wire \Xmap[6]_i_260_n_0 ;
  wire \Xmap[6]_i_2_n_0 ;
  wire \Xmap[6]_i_44_n_0 ;
  wire \Xmap[6]_i_45_n_0 ;
  wire \Xmap[6]_i_46_n_0 ;
  wire \Xmap[6]_i_47_n_0 ;
  wire \Xmap[6]_i_4_n_0 ;
  wire \Xmap[6]_i_55_n_0 ;
  wire \Xmap[6]_i_56_n_0 ;
  wire \Xmap[6]_i_57_n_0 ;
  wire \Xmap[6]_i_58_n_0 ;
  wire \Xmap[6]_i_59_n_0 ;
  wire \Xmap[6]_i_5_n_0 ;
  wire \Xmap[6]_i_60_n_0 ;
  wire \Xmap[6]_i_61_n_0 ;
  wire \Xmap[6]_i_62_n_0 ;
  wire \Xmap[6]_i_63_n_0 ;
  wire \Xmap[6]_i_64_n_0 ;
  wire \Xmap[6]_i_65_n_0 ;
  wire \Xmap[6]_i_66_n_0 ;
  wire \Xmap[6]_i_67_n_0 ;
  wire \Xmap[6]_i_68_n_0 ;
  wire \Xmap[6]_i_69_n_0 ;
  wire \Xmap[6]_i_6_n_0 ;
  wire \Xmap[6]_i_70_n_0 ;
  wire \Xmap[6]_i_71_n_0 ;
  wire \Xmap[6]_i_72_n_0 ;
  wire \Xmap[6]_i_73_n_0 ;
  wire \Xmap[6]_i_74_n_0 ;
  wire \Xmap[6]_i_75_n_0 ;
  wire \Xmap[6]_i_76_n_0 ;
  wire \Xmap[6]_i_77_n_0 ;
  wire \Xmap[6]_i_78_n_0 ;
  wire \Xmap[6]_i_79_n_0 ;
  wire \Xmap[6]_i_7_n_0 ;
  wire \Xmap[6]_i_80_n_0 ;
  wire \Xmap[6]_i_81_n_0 ;
  wire \Xmap[6]_i_82_n_0 ;
  wire \Xmap[6]_i_83_n_0 ;
  wire \Xmap[6]_i_84_n_0 ;
  wire \Xmap[6]_i_85_n_0 ;
  wire \Xmap[6]_i_86_n_0 ;
  wire \Xmap[6]_i_87_n_0 ;
  wire \Xmap[6]_i_88_n_0 ;
  wire \Xmap[6]_i_89_n_0 ;
  wire \Xmap[6]_i_90_n_0 ;
  wire \Xmap[6]_i_91_n_0 ;
  wire \Xmap[6]_i_92_n_0 ;
  wire \Xmap[6]_i_93_n_0 ;
  wire \Xmap[6]_i_95_n_0 ;
  wire \Xmap[6]_i_96_n_0 ;
  wire \Xmap[6]_i_97_n_0 ;
  wire \Xmap[6]_i_98_n_0 ;
  wire \Xmap_reg[3]_i_1_n_0 ;
  wire \Xmap_reg[3]_i_1_n_1 ;
  wire \Xmap_reg[3]_i_1_n_2 ;
  wire \Xmap_reg[3]_i_1_n_3 ;
  wire \Xmap_reg[3]_i_1_n_4 ;
  wire \Xmap_reg[3]_i_1_n_5 ;
  wire \Xmap_reg[3]_i_1_n_6 ;
  wire \Xmap_reg[3]_i_1_n_7 ;
  wire [3:0]\Xmap_reg[4]_0 ;
  wire [3:0]\Xmap_reg[4]_1 ;
  wire [0:0]\Xmap_reg[4]_10 ;
  wire [3:0]\Xmap_reg[4]_11 ;
  wire [3:0]\Xmap_reg[4]_12 ;
  wire [3:0]\Xmap_reg[4]_13 ;
  wire [0:0]\Xmap_reg[4]_14 ;
  wire [3:0]\Xmap_reg[4]_15 ;
  wire [3:0]\Xmap_reg[4]_16 ;
  wire [3:0]\Xmap_reg[4]_17 ;
  wire [3:0]\Xmap_reg[4]_18 ;
  wire [3:0]\Xmap_reg[4]_19 ;
  wire [2:0]\Xmap_reg[4]_2 ;
  wire [3:0]\Xmap_reg[4]_20 ;
  wire [3:0]\Xmap_reg[4]_21 ;
  wire [3:0]\Xmap_reg[4]_22 ;
  wire [3:0]\Xmap_reg[4]_23 ;
  wire [0:0]\Xmap_reg[4]_24 ;
  wire [2:0]\Xmap_reg[4]_25 ;
  wire [2:0]\Xmap_reg[4]_3 ;
  wire [3:0]\Xmap_reg[4]_4 ;
  wire [0:0]\Xmap_reg[4]_5 ;
  wire [2:0]\Xmap_reg[4]_6 ;
  wire [3:0]\Xmap_reg[4]_7 ;
  wire [3:0]\Xmap_reg[4]_8 ;
  wire [3:0]\Xmap_reg[4]_9 ;
  wire \Xmap_reg[6]_i_103_n_0 ;
  wire \Xmap_reg[6]_i_103_n_1 ;
  wire \Xmap_reg[6]_i_103_n_2 ;
  wire \Xmap_reg[6]_i_103_n_3 ;
  wire \Xmap_reg[6]_i_104_n_0 ;
  wire \Xmap_reg[6]_i_104_n_1 ;
  wire \Xmap_reg[6]_i_104_n_2 ;
  wire \Xmap_reg[6]_i_104_n_3 ;
  wire \Xmap_reg[6]_i_105_n_0 ;
  wire \Xmap_reg[6]_i_105_n_1 ;
  wire \Xmap_reg[6]_i_105_n_2 ;
  wire \Xmap_reg[6]_i_105_n_3 ;
  wire \Xmap_reg[6]_i_106_n_0 ;
  wire \Xmap_reg[6]_i_106_n_1 ;
  wire \Xmap_reg[6]_i_106_n_2 ;
  wire \Xmap_reg[6]_i_106_n_3 ;
  wire \Xmap_reg[6]_i_10_n_2 ;
  wire \Xmap_reg[6]_i_10_n_3 ;
  wire \Xmap_reg[6]_i_113_n_0 ;
  wire \Xmap_reg[6]_i_113_n_1 ;
  wire \Xmap_reg[6]_i_113_n_2 ;
  wire \Xmap_reg[6]_i_113_n_3 ;
  wire \Xmap_reg[6]_i_113_n_4 ;
  wire \Xmap_reg[6]_i_113_n_5 ;
  wire \Xmap_reg[6]_i_113_n_6 ;
  wire \Xmap_reg[6]_i_125_n_0 ;
  wire \Xmap_reg[6]_i_125_n_1 ;
  wire \Xmap_reg[6]_i_125_n_2 ;
  wire \Xmap_reg[6]_i_125_n_3 ;
  wire \Xmap_reg[6]_i_125_n_4 ;
  wire \Xmap_reg[6]_i_125_n_5 ;
  wire \Xmap_reg[6]_i_125_n_6 ;
  wire \Xmap_reg[6]_i_125_n_7 ;
  wire \Xmap_reg[6]_i_126_n_0 ;
  wire \Xmap_reg[6]_i_126_n_1 ;
  wire \Xmap_reg[6]_i_126_n_2 ;
  wire \Xmap_reg[6]_i_126_n_3 ;
  wire \Xmap_reg[6]_i_126_n_4 ;
  wire \Xmap_reg[6]_i_126_n_5 ;
  wire \Xmap_reg[6]_i_126_n_6 ;
  wire \Xmap_reg[6]_i_126_n_7 ;
  wire \Xmap_reg[6]_i_127_n_0 ;
  wire \Xmap_reg[6]_i_127_n_1 ;
  wire \Xmap_reg[6]_i_127_n_2 ;
  wire \Xmap_reg[6]_i_127_n_3 ;
  wire \Xmap_reg[6]_i_127_n_4 ;
  wire \Xmap_reg[6]_i_127_n_5 ;
  wire \Xmap_reg[6]_i_127_n_6 ;
  wire \Xmap_reg[6]_i_128_n_1 ;
  wire \Xmap_reg[6]_i_128_n_2 ;
  wire \Xmap_reg[6]_i_128_n_3 ;
  wire \Xmap_reg[6]_i_128_n_4 ;
  wire \Xmap_reg[6]_i_128_n_5 ;
  wire \Xmap_reg[6]_i_128_n_6 ;
  wire \Xmap_reg[6]_i_128_n_7 ;
  wire \Xmap_reg[6]_i_129_n_1 ;
  wire \Xmap_reg[6]_i_129_n_2 ;
  wire \Xmap_reg[6]_i_129_n_3 ;
  wire \Xmap_reg[6]_i_129_n_4 ;
  wire \Xmap_reg[6]_i_129_n_5 ;
  wire \Xmap_reg[6]_i_129_n_6 ;
  wire \Xmap_reg[6]_i_129_n_7 ;
  wire \Xmap_reg[6]_i_137_n_0 ;
  wire \Xmap_reg[6]_i_137_n_1 ;
  wire \Xmap_reg[6]_i_137_n_2 ;
  wire \Xmap_reg[6]_i_137_n_3 ;
  wire \Xmap_reg[6]_i_138_n_0 ;
  wire \Xmap_reg[6]_i_138_n_1 ;
  wire \Xmap_reg[6]_i_138_n_2 ;
  wire \Xmap_reg[6]_i_138_n_3 ;
  wire \Xmap_reg[6]_i_139_n_0 ;
  wire \Xmap_reg[6]_i_139_n_1 ;
  wire \Xmap_reg[6]_i_139_n_2 ;
  wire \Xmap_reg[6]_i_139_n_3 ;
  wire \Xmap_reg[6]_i_146_n_0 ;
  wire \Xmap_reg[6]_i_146_n_1 ;
  wire \Xmap_reg[6]_i_146_n_2 ;
  wire \Xmap_reg[6]_i_146_n_3 ;
  wire \Xmap_reg[6]_i_14_n_0 ;
  wire \Xmap_reg[6]_i_14_n_1 ;
  wire \Xmap_reg[6]_i_14_n_2 ;
  wire \Xmap_reg[6]_i_14_n_3 ;
  wire \Xmap_reg[6]_i_177_n_0 ;
  wire \Xmap_reg[6]_i_177_n_1 ;
  wire \Xmap_reg[6]_i_177_n_2 ;
  wire \Xmap_reg[6]_i_177_n_3 ;
  wire \Xmap_reg[6]_i_199_n_0 ;
  wire \Xmap_reg[6]_i_199_n_1 ;
  wire \Xmap_reg[6]_i_199_n_2 ;
  wire \Xmap_reg[6]_i_199_n_3 ;
  wire \Xmap_reg[6]_i_199_n_4 ;
  wire \Xmap_reg[6]_i_199_n_5 ;
  wire \Xmap_reg[6]_i_199_n_6 ;
  wire \Xmap_reg[6]_i_207_n_0 ;
  wire \Xmap_reg[6]_i_207_n_1 ;
  wire \Xmap_reg[6]_i_207_n_2 ;
  wire \Xmap_reg[6]_i_207_n_3 ;
  wire \Xmap_reg[6]_i_222_n_0 ;
  wire \Xmap_reg[6]_i_222_n_1 ;
  wire \Xmap_reg[6]_i_222_n_2 ;
  wire \Xmap_reg[6]_i_222_n_3 ;
  wire \Xmap_reg[6]_i_228_n_0 ;
  wire \Xmap_reg[6]_i_228_n_1 ;
  wire \Xmap_reg[6]_i_228_n_2 ;
  wire \Xmap_reg[6]_i_228_n_3 ;
  wire \Xmap_reg[6]_i_229_n_0 ;
  wire \Xmap_reg[6]_i_229_n_1 ;
  wire \Xmap_reg[6]_i_229_n_2 ;
  wire \Xmap_reg[6]_i_229_n_3 ;
  wire \Xmap_reg[6]_i_238_n_0 ;
  wire \Xmap_reg[6]_i_238_n_1 ;
  wire \Xmap_reg[6]_i_238_n_2 ;
  wire \Xmap_reg[6]_i_238_n_3 ;
  wire \Xmap_reg[6]_i_243_n_0 ;
  wire \Xmap_reg[6]_i_243_n_1 ;
  wire \Xmap_reg[6]_i_243_n_2 ;
  wire \Xmap_reg[6]_i_243_n_3 ;
  wire \Xmap_reg[6]_i_243_n_7 ;
  wire \Xmap_reg[6]_i_28_n_0 ;
  wire \Xmap_reg[6]_i_28_n_1 ;
  wire \Xmap_reg[6]_i_28_n_2 ;
  wire \Xmap_reg[6]_i_28_n_3 ;
  wire \Xmap_reg[6]_i_37_n_0 ;
  wire \Xmap_reg[6]_i_37_n_1 ;
  wire \Xmap_reg[6]_i_37_n_2 ;
  wire \Xmap_reg[6]_i_37_n_3 ;
  wire \Xmap_reg[6]_i_38_n_0 ;
  wire \Xmap_reg[6]_i_38_n_1 ;
  wire \Xmap_reg[6]_i_38_n_2 ;
  wire \Xmap_reg[6]_i_38_n_3 ;
  wire \Xmap_reg[6]_i_39_n_0 ;
  wire \Xmap_reg[6]_i_39_n_1 ;
  wire \Xmap_reg[6]_i_39_n_2 ;
  wire \Xmap_reg[6]_i_39_n_3 ;
  wire \Xmap_reg[6]_i_3_n_1 ;
  wire \Xmap_reg[6]_i_3_n_2 ;
  wire \Xmap_reg[6]_i_3_n_3 ;
  wire \Xmap_reg[6]_i_3_n_4 ;
  wire \Xmap_reg[6]_i_3_n_5 ;
  wire \Xmap_reg[6]_i_3_n_6 ;
  wire \Xmap_reg[6]_i_3_n_7 ;
  wire \Xmap_reg[6]_i_40_n_1 ;
  wire \Xmap_reg[6]_i_40_n_2 ;
  wire \Xmap_reg[6]_i_40_n_3 ;
  wire \Xmap_reg[6]_i_41_n_1 ;
  wire \Xmap_reg[6]_i_41_n_2 ;
  wire \Xmap_reg[6]_i_41_n_3 ;
  wire \Xmap_reg[6]_i_42_n_1 ;
  wire \Xmap_reg[6]_i_42_n_2 ;
  wire \Xmap_reg[6]_i_42_n_3 ;
  wire \Xmap_reg[6]_i_43_n_0 ;
  wire \Xmap_reg[6]_i_43_n_1 ;
  wire \Xmap_reg[6]_i_43_n_2 ;
  wire \Xmap_reg[6]_i_43_n_3 ;
  wire \Xmap_reg[6]_i_52_n_0 ;
  wire \Xmap_reg[6]_i_52_n_1 ;
  wire \Xmap_reg[6]_i_52_n_2 ;
  wire \Xmap_reg[6]_i_52_n_3 ;
  wire \Xmap_reg[6]_i_53_n_0 ;
  wire \Xmap_reg[6]_i_53_n_1 ;
  wire \Xmap_reg[6]_i_53_n_2 ;
  wire \Xmap_reg[6]_i_53_n_3 ;
  wire \Xmap_reg[6]_i_54_n_0 ;
  wire \Xmap_reg[6]_i_54_n_1 ;
  wire \Xmap_reg[6]_i_54_n_2 ;
  wire \Xmap_reg[6]_i_54_n_3 ;
  wire \Xmap_reg[6]_i_94_n_0 ;
  wire \Xmap_reg[6]_i_94_n_1 ;
  wire \Xmap_reg[6]_i_94_n_2 ;
  wire \Xmap_reg[6]_i_94_n_3 ;
  wire \Xmap_reg[6]_i_9_n_0 ;
  wire \Xmap_reg[6]_i_9_n_1 ;
  wire \Xmap_reg[6]_i_9_n_2 ;
  wire \Xmap_reg[6]_i_9_n_3 ;
  wire [5:1]Ymap;
  wire \Ymap[0]_i_100_n_0 ;
  wire \Ymap[0]_i_101_n_0 ;
  wire \Ymap[0]_i_102_n_0 ;
  wire \Ymap[0]_i_106_n_0 ;
  wire \Ymap[0]_i_107_n_0 ;
  wire \Ymap[0]_i_108_n_0 ;
  wire \Ymap[0]_i_109_n_0 ;
  wire \Ymap[0]_i_10_n_0 ;
  wire \Ymap[0]_i_110_n_0 ;
  wire \Ymap[0]_i_111_n_0 ;
  wire \Ymap[0]_i_112_n_0 ;
  wire \Ymap[0]_i_113_n_0 ;
  wire \Ymap[0]_i_114_n_0 ;
  wire \Ymap[0]_i_115_n_0 ;
  wire \Ymap[0]_i_116_n_0 ;
  wire \Ymap[0]_i_117_n_0 ;
  wire \Ymap[0]_i_119_n_0 ;
  wire \Ymap[0]_i_11_n_0 ;
  wire \Ymap[0]_i_120_n_0 ;
  wire \Ymap[0]_i_121_n_0 ;
  wire \Ymap[0]_i_122_n_0 ;
  wire \Ymap[0]_i_123_n_0 ;
  wire \Ymap[0]_i_124_n_0 ;
  wire \Ymap[0]_i_125_n_0 ;
  wire \Ymap[0]_i_126_n_0 ;
  wire \Ymap[0]_i_127_n_0 ;
  wire \Ymap[0]_i_128_n_0 ;
  wire \Ymap[0]_i_129_n_0 ;
  wire \Ymap[0]_i_130_n_0 ;
  wire \Ymap[0]_i_132_n_0 ;
  wire \Ymap[0]_i_133_n_0 ;
  wire \Ymap[0]_i_134_n_0 ;
  wire \Ymap[0]_i_135_n_0 ;
  wire \Ymap[0]_i_138_n_0 ;
  wire \Ymap[0]_i_139_n_0 ;
  wire \Ymap[0]_i_13_n_0 ;
  wire \Ymap[0]_i_140_n_0 ;
  wire \Ymap[0]_i_141_n_0 ;
  wire \Ymap[0]_i_142_n_0 ;
  wire \Ymap[0]_i_143_n_0 ;
  wire \Ymap[0]_i_144_n_0 ;
  wire \Ymap[0]_i_145_n_0 ;
  wire \Ymap[0]_i_146_n_0 ;
  wire \Ymap[0]_i_147_n_0 ;
  wire \Ymap[0]_i_148_n_0 ;
  wire \Ymap[0]_i_149_n_0 ;
  wire \Ymap[0]_i_14_n_0 ;
  wire \Ymap[0]_i_150_n_0 ;
  wire \Ymap[0]_i_151_n_0 ;
  wire \Ymap[0]_i_152_n_0 ;
  wire \Ymap[0]_i_153_n_0 ;
  wire \Ymap[0]_i_154_n_0 ;
  wire \Ymap[0]_i_155_n_0 ;
  wire \Ymap[0]_i_156_n_0 ;
  wire \Ymap[0]_i_157_n_0 ;
  wire \Ymap[0]_i_158_n_0 ;
  wire \Ymap[0]_i_15_n_0 ;
  wire \Ymap[0]_i_160_n_0 ;
  wire \Ymap[0]_i_161_n_0 ;
  wire \Ymap[0]_i_162_n_0 ;
  wire \Ymap[0]_i_163_n_0 ;
  wire \Ymap[0]_i_166_n_0 ;
  wire \Ymap[0]_i_167_n_0 ;
  wire \Ymap[0]_i_168_n_0 ;
  wire \Ymap[0]_i_169_n_0 ;
  wire \Ymap[0]_i_16_n_0 ;
  wire \Ymap[0]_i_170_n_0 ;
  wire \Ymap[0]_i_171_n_0 ;
  wire \Ymap[0]_i_172_n_0 ;
  wire \Ymap[0]_i_173_n_0 ;
  wire \Ymap[0]_i_175_n_0 ;
  wire \Ymap[0]_i_176_n_0 ;
  wire \Ymap[0]_i_177_n_0 ;
  wire \Ymap[0]_i_178_n_0 ;
  wire \Ymap[0]_i_17_n_0 ;
  wire \Ymap[0]_i_180_n_0 ;
  wire \Ymap[0]_i_181_n_0 ;
  wire \Ymap[0]_i_182_n_0 ;
  wire \Ymap[0]_i_183_n_0 ;
  wire \Ymap[0]_i_184_n_0 ;
  wire \Ymap[0]_i_185_n_0 ;
  wire \Ymap[0]_i_186_n_0 ;
  wire \Ymap[0]_i_187_n_0 ;
  wire \Ymap[0]_i_188_n_0 ;
  wire \Ymap[0]_i_189_n_0 ;
  wire \Ymap[0]_i_18_n_0 ;
  wire \Ymap[0]_i_190_n_0 ;
  wire \Ymap[0]_i_191_n_0 ;
  wire \Ymap[0]_i_192_n_0 ;
  wire \Ymap[0]_i_193_n_0 ;
  wire \Ymap[0]_i_194_n_0 ;
  wire \Ymap[0]_i_195_n_0 ;
  wire \Ymap[0]_i_196_n_0 ;
  wire \Ymap[0]_i_19_n_0 ;
  wire \Ymap[0]_i_1_n_0 ;
  wire \Ymap[0]_i_20_n_0 ;
  wire \Ymap[0]_i_25_n_0 ;
  wire \Ymap[0]_i_26_n_0 ;
  wire \Ymap[0]_i_27_n_0 ;
  wire \Ymap[0]_i_28_n_0 ;
  wire \Ymap[0]_i_29_n_0 ;
  wire \Ymap[0]_i_30_n_0 ;
  wire \Ymap[0]_i_31_n_0 ;
  wire \Ymap[0]_i_32_n_0 ;
  wire \Ymap[0]_i_36_n_0 ;
  wire \Ymap[0]_i_37_n_0 ;
  wire \Ymap[0]_i_38_n_0 ;
  wire \Ymap[0]_i_39_n_0 ;
  wire \Ymap[0]_i_40_n_0 ;
  wire \Ymap[0]_i_41_n_0 ;
  wire \Ymap[0]_i_42_n_0 ;
  wire \Ymap[0]_i_43_n_0 ;
  wire \Ymap[0]_i_44_n_0 ;
  wire \Ymap[0]_i_45_n_0 ;
  wire \Ymap[0]_i_46_n_0 ;
  wire \Ymap[0]_i_47_n_0 ;
  wire \Ymap[0]_i_48_n_0 ;
  wire \Ymap[0]_i_49_n_0 ;
  wire \Ymap[0]_i_4_n_0 ;
  wire \Ymap[0]_i_50_n_0 ;
  wire \Ymap[0]_i_51_n_0 ;
  wire \Ymap[0]_i_52_n_0 ;
  wire \Ymap[0]_i_53_n_0 ;
  wire \Ymap[0]_i_54_n_0 ;
  wire \Ymap[0]_i_55_n_0 ;
  wire \Ymap[0]_i_56_n_0 ;
  wire \Ymap[0]_i_57_n_0 ;
  wire \Ymap[0]_i_58_n_0 ;
  wire \Ymap[0]_i_59_n_0 ;
  wire \Ymap[0]_i_5_n_0 ;
  wire \Ymap[0]_i_61_n_0 ;
  wire \Ymap[0]_i_62_n_0 ;
  wire \Ymap[0]_i_63_n_0 ;
  wire \Ymap[0]_i_64_n_0 ;
  wire \Ymap[0]_i_65_n_0 ;
  wire \Ymap[0]_i_66_n_0 ;
  wire \Ymap[0]_i_67_n_0 ;
  wire \Ymap[0]_i_68_n_0 ;
  wire \Ymap[0]_i_6_n_0 ;
  wire \Ymap[0]_i_73_n_0 ;
  wire \Ymap[0]_i_74_n_0 ;
  wire \Ymap[0]_i_75_n_0 ;
  wire \Ymap[0]_i_76_n_0 ;
  wire \Ymap[0]_i_77_n_0 ;
  wire \Ymap[0]_i_78_n_0 ;
  wire \Ymap[0]_i_79_n_0 ;
  wire \Ymap[0]_i_7_n_0 ;
  wire \Ymap[0]_i_80_n_0 ;
  wire \Ymap[0]_i_81_n_0 ;
  wire \Ymap[0]_i_82_n_0 ;
  wire \Ymap[0]_i_84_n_0 ;
  wire \Ymap[0]_i_85_n_0 ;
  wire \Ymap[0]_i_86_n_0 ;
  wire \Ymap[0]_i_87_n_0 ;
  wire \Ymap[0]_i_88_n_0 ;
  wire \Ymap[0]_i_89_n_0 ;
  wire \Ymap[0]_i_8_n_0 ;
  wire \Ymap[0]_i_90_n_0 ;
  wire \Ymap[0]_i_91_n_0 ;
  wire \Ymap[0]_i_92_n_0 ;
  wire \Ymap[0]_i_93_n_0 ;
  wire \Ymap[0]_i_94_n_0 ;
  wire \Ymap[0]_i_96_n_0 ;
  wire \Ymap[0]_i_97_n_0 ;
  wire \Ymap[0]_i_98_n_0 ;
  wire \Ymap[0]_i_99_n_0 ;
  wire \Ymap[0]_i_9_n_0 ;
  wire \Ymap[1]_i_1_n_0 ;
  wire \Ymap[2]_i_1_n_0 ;
  wire \Ymap[3]_i_1_n_0 ;
  wire \Ymap[3]_i_3_n_0 ;
  wire \Ymap[3]_i_4_n_0 ;
  wire \Ymap[3]_i_5_n_0 ;
  wire \Ymap[3]_i_6_n_0 ;
  wire \Ymap[4]_i_10_n_0 ;
  wire \Ymap[4]_i_14_n_0 ;
  wire \Ymap[4]_i_15_n_0 ;
  wire \Ymap[4]_i_16_n_0 ;
  wire \Ymap[4]_i_17_n_0 ;
  wire \Ymap[4]_i_18_n_0 ;
  wire \Ymap[4]_i_19_n_0 ;
  wire \Ymap[4]_i_1_n_0 ;
  wire \Ymap[4]_i_20_n_0 ;
  wire \Ymap[4]_i_21_n_0 ;
  wire \Ymap[4]_i_22_n_0 ;
  wire \Ymap[4]_i_23_n_0 ;
  wire \Ymap[4]_i_24_n_0 ;
  wire \Ymap[4]_i_25_n_0 ;
  wire \Ymap[4]_i_26_n_0 ;
  wire \Ymap[4]_i_27_n_0 ;
  wire \Ymap[4]_i_28_n_0 ;
  wire \Ymap[4]_i_29_n_0 ;
  wire \Ymap[4]_i_30_n_0 ;
  wire \Ymap[4]_i_31_n_0 ;
  wire \Ymap[4]_i_32_n_0 ;
  wire \Ymap[4]_i_33_n_0 ;
  wire \Ymap[4]_i_34_n_0 ;
  wire \Ymap[4]_i_35_n_0 ;
  wire \Ymap[4]_i_36_n_0 ;
  wire \Ymap[4]_i_37_n_0 ;
  wire \Ymap[4]_i_3_n_0 ;
  wire \Ymap[4]_i_40_n_0 ;
  wire \Ymap[4]_i_41_n_0 ;
  wire \Ymap[4]_i_42_n_0 ;
  wire \Ymap[4]_i_43_n_0 ;
  wire \Ymap[4]_i_44_n_0 ;
  wire \Ymap[4]_i_45_n_0 ;
  wire \Ymap[4]_i_46_n_0 ;
  wire \Ymap[4]_i_47_n_0 ;
  wire \Ymap[4]_i_4_n_0 ;
  wire \Ymap[4]_i_5_n_0 ;
  wire \Ymap[4]_i_6_n_0 ;
  wire \Ymap[4]_i_7_n_0 ;
  wire \Ymap[4]_i_8_n_0 ;
  wire \Ymap[4]_i_9_n_0 ;
  wire \Ymap[5]_i_100_n_0 ;
  wire \Ymap[5]_i_101_n_0 ;
  wire \Ymap[5]_i_102_n_0 ;
  wire \Ymap[5]_i_103_n_0 ;
  wire \Ymap[5]_i_106_n_0 ;
  wire \Ymap[5]_i_107_n_0 ;
  wire \Ymap[5]_i_108_n_0 ;
  wire \Ymap[5]_i_109_n_0 ;
  wire \Ymap[5]_i_10_n_0 ;
  wire \Ymap[5]_i_110_n_0 ;
  wire \Ymap[5]_i_111_n_0 ;
  wire \Ymap[5]_i_112_n_0 ;
  wire \Ymap[5]_i_113_n_0 ;
  wire \Ymap[5]_i_114_n_0 ;
  wire \Ymap[5]_i_115_n_0 ;
  wire \Ymap[5]_i_116_n_0 ;
  wire \Ymap[5]_i_118_n_0 ;
  wire \Ymap[5]_i_119_n_0 ;
  wire \Ymap[5]_i_11_n_0 ;
  wire \Ymap[5]_i_120_n_0 ;
  wire \Ymap[5]_i_121_n_0 ;
  wire \Ymap[5]_i_122_n_0 ;
  wire \Ymap[5]_i_123_n_0 ;
  wire \Ymap[5]_i_124_n_0 ;
  wire \Ymap[5]_i_125_n_0 ;
  wire \Ymap[5]_i_12_n_0 ;
  wire \Ymap[5]_i_133_n_0 ;
  wire \Ymap[5]_i_134_n_0 ;
  wire \Ymap[5]_i_135_n_0 ;
  wire \Ymap[5]_i_136_n_0 ;
  wire \Ymap[5]_i_137_n_0 ;
  wire \Ymap[5]_i_138_n_0 ;
  wire \Ymap[5]_i_139_n_0 ;
  wire \Ymap[5]_i_13_n_0 ;
  wire \Ymap[5]_i_140_n_0 ;
  wire \Ymap[5]_i_141_n_0 ;
  wire \Ymap[5]_i_142_n_0 ;
  wire \Ymap[5]_i_143_n_0 ;
  wire \Ymap[5]_i_144_n_0 ;
  wire \Ymap[5]_i_14_n_0 ;
  wire \Ymap[5]_i_151_n_0 ;
  wire \Ymap[5]_i_152_n_0 ;
  wire \Ymap[5]_i_153_n_0 ;
  wire \Ymap[5]_i_154_n_0 ;
  wire \Ymap[5]_i_155_n_0 ;
  wire \Ymap[5]_i_156_n_0 ;
  wire \Ymap[5]_i_157_n_0 ;
  wire \Ymap[5]_i_158_n_0 ;
  wire \Ymap[5]_i_159_n_0 ;
  wire \Ymap[5]_i_15_n_0 ;
  wire \Ymap[5]_i_160_n_0 ;
  wire \Ymap[5]_i_161_n_0 ;
  wire \Ymap[5]_i_162_n_0 ;
  wire \Ymap[5]_i_163_n_0 ;
  wire \Ymap[5]_i_164_n_0 ;
  wire \Ymap[5]_i_165_n_0 ;
  wire \Ymap[5]_i_166_n_0 ;
  wire \Ymap[5]_i_167_n_0 ;
  wire \Ymap[5]_i_168_n_0 ;
  wire \Ymap[5]_i_169_n_0 ;
  wire \Ymap[5]_i_16_n_0 ;
  wire \Ymap[5]_i_170_n_0 ;
  wire \Ymap[5]_i_171_n_0 ;
  wire \Ymap[5]_i_172_n_0 ;
  wire \Ymap[5]_i_173_n_0 ;
  wire \Ymap[5]_i_174_n_0 ;
  wire \Ymap[5]_i_175_n_0 ;
  wire \Ymap[5]_i_176_n_0 ;
  wire \Ymap[5]_i_177_n_0 ;
  wire \Ymap[5]_i_178_n_0 ;
  wire \Ymap[5]_i_179_n_0 ;
  wire \Ymap[5]_i_17_n_0 ;
  wire \Ymap[5]_i_180_n_0 ;
  wire \Ymap[5]_i_181_n_0 ;
  wire \Ymap[5]_i_182_n_0 ;
  wire \Ymap[5]_i_183_n_0 ;
  wire \Ymap[5]_i_184_n_0 ;
  wire \Ymap[5]_i_185_n_0 ;
  wire \Ymap[5]_i_186_n_0 ;
  wire \Ymap[5]_i_187_n_0 ;
  wire \Ymap[5]_i_188_n_0 ;
  wire \Ymap[5]_i_189_n_0 ;
  wire \Ymap[5]_i_18_n_0 ;
  wire \Ymap[5]_i_190_n_0 ;
  wire \Ymap[5]_i_193_n_0 ;
  wire \Ymap[5]_i_194_n_0 ;
  wire \Ymap[5]_i_195_n_0 ;
  wire \Ymap[5]_i_196_n_0 ;
  wire \Ymap[5]_i_197_n_0 ;
  wire \Ymap[5]_i_198_n_0 ;
  wire \Ymap[5]_i_199_n_0 ;
  wire \Ymap[5]_i_19_n_0 ;
  wire \Ymap[5]_i_1_n_0 ;
  wire \Ymap[5]_i_200_n_0 ;
  wire \Ymap[5]_i_201_n_0 ;
  wire \Ymap[5]_i_202_n_0 ;
  wire \Ymap[5]_i_203_n_0 ;
  wire \Ymap[5]_i_204_n_0 ;
  wire \Ymap[5]_i_208_n_0 ;
  wire \Ymap[5]_i_209_n_0 ;
  wire \Ymap[5]_i_20_n_0 ;
  wire \Ymap[5]_i_210_n_0 ;
  wire \Ymap[5]_i_211_n_0 ;
  wire \Ymap[5]_i_212_n_0 ;
  wire \Ymap[5]_i_213_n_0 ;
  wire \Ymap[5]_i_214_n_0 ;
  wire \Ymap[5]_i_215_n_0 ;
  wire \Ymap[5]_i_216_n_0 ;
  wire \Ymap[5]_i_217_n_0 ;
  wire \Ymap[5]_i_218_n_0 ;
  wire \Ymap[5]_i_219_n_0 ;
  wire \Ymap[5]_i_220_n_0 ;
  wire \Ymap[5]_i_221_n_0 ;
  wire \Ymap[5]_i_222_n_0 ;
  wire \Ymap[5]_i_223_n_0 ;
  wire \Ymap[5]_i_224_n_0 ;
  wire \Ymap[5]_i_226_n_0 ;
  wire \Ymap[5]_i_227_n_0 ;
  wire \Ymap[5]_i_228_n_0 ;
  wire \Ymap[5]_i_229_n_0 ;
  wire \Ymap[5]_i_22_n_0 ;
  wire \Ymap[5]_i_230_n_0 ;
  wire \Ymap[5]_i_231_n_0 ;
  wire \Ymap[5]_i_232_n_0 ;
  wire \Ymap[5]_i_233_n_0 ;
  wire \Ymap[5]_i_235_n_0 ;
  wire \Ymap[5]_i_236_n_0 ;
  wire \Ymap[5]_i_237_n_0 ;
  wire \Ymap[5]_i_238_n_0 ;
  wire \Ymap[5]_i_239_n_0 ;
  wire \Ymap[5]_i_23_n_0 ;
  wire \Ymap[5]_i_240_n_0 ;
  wire \Ymap[5]_i_241_n_0 ;
  wire \Ymap[5]_i_242_n_0 ;
  wire \Ymap[5]_i_243_n_0 ;
  wire \Ymap[5]_i_244_n_0 ;
  wire \Ymap[5]_i_245_n_0 ;
  wire \Ymap[5]_i_246_n_0 ;
  wire \Ymap[5]_i_249_n_0 ;
  wire \Ymap[5]_i_24_n_0 ;
  wire \Ymap[5]_i_250_n_0 ;
  wire \Ymap[5]_i_251_n_0 ;
  wire \Ymap[5]_i_252_n_0 ;
  wire \Ymap[5]_i_253_n_0 ;
  wire \Ymap[5]_i_254_n_0 ;
  wire \Ymap[5]_i_255_n_0 ;
  wire \Ymap[5]_i_256_n_0 ;
  wire \Ymap[5]_i_257_n_0 ;
  wire \Ymap[5]_i_258_n_0 ;
  wire \Ymap[5]_i_259_n_0 ;
  wire \Ymap[5]_i_25_n_0 ;
  wire \Ymap[5]_i_260_n_0 ;
  wire \Ymap[5]_i_261_n_0 ;
  wire \Ymap[5]_i_262_n_0 ;
  wire \Ymap[5]_i_263_n_0 ;
  wire \Ymap[5]_i_264_n_0 ;
  wire \Ymap[5]_i_265_n_0 ;
  wire \Ymap[5]_i_266_n_0 ;
  wire \Ymap[5]_i_267_n_0 ;
  wire \Ymap[5]_i_268_n_0 ;
  wire \Ymap[5]_i_269_n_0 ;
  wire \Ymap[5]_i_271_n_0 ;
  wire \Ymap[5]_i_272_n_0 ;
  wire \Ymap[5]_i_273_n_0 ;
  wire \Ymap[5]_i_274_n_0 ;
  wire \Ymap[5]_i_275_n_0 ;
  wire \Ymap[5]_i_276_n_0 ;
  wire \Ymap[5]_i_277_n_0 ;
  wire \Ymap[5]_i_278_n_0 ;
  wire \Ymap[5]_i_279_n_0 ;
  wire \Ymap[5]_i_27_n_0 ;
  wire \Ymap[5]_i_280_n_0 ;
  wire \Ymap[5]_i_281_n_0 ;
  wire \Ymap[5]_i_282_n_0 ;
  wire \Ymap[5]_i_283_n_0 ;
  wire \Ymap[5]_i_284_n_0 ;
  wire \Ymap[5]_i_285_n_0 ;
  wire \Ymap[5]_i_286_n_0 ;
  wire \Ymap[5]_i_287_n_0 ;
  wire \Ymap[5]_i_288_n_0 ;
  wire \Ymap[5]_i_289_n_0 ;
  wire \Ymap[5]_i_28_n_0 ;
  wire \Ymap[5]_i_290_n_0 ;
  wire \Ymap[5]_i_291_n_0 ;
  wire \Ymap[5]_i_292_n_0 ;
  wire \Ymap[5]_i_293_n_0 ;
  wire \Ymap[5]_i_29_n_0 ;
  wire \Ymap[5]_i_30_n_0 ;
  wire \Ymap[5]_i_31_n_0 ;
  wire \Ymap[5]_i_32_n_0 ;
  wire \Ymap[5]_i_33_n_0 ;
  wire \Ymap[5]_i_34_n_0 ;
  wire \Ymap[5]_i_43_n_0 ;
  wire \Ymap[5]_i_44_n_0 ;
  wire \Ymap[5]_i_45_n_0 ;
  wire \Ymap[5]_i_46_n_0 ;
  wire \Ymap[5]_i_48_n_0 ;
  wire \Ymap[5]_i_49_n_0 ;
  wire \Ymap[5]_i_50_n_0 ;
  wire \Ymap[5]_i_51_n_0 ;
  wire \Ymap[5]_i_52_n_0 ;
  wire \Ymap[5]_i_53_n_0 ;
  wire \Ymap[5]_i_54_n_0 ;
  wire \Ymap[5]_i_55_n_0 ;
  wire \Ymap[5]_i_56_n_0 ;
  wire \Ymap[5]_i_57_n_0 ;
  wire \Ymap[5]_i_58_n_0 ;
  wire \Ymap[5]_i_59_n_0 ;
  wire \Ymap[5]_i_60_n_0 ;
  wire \Ymap[5]_i_61_n_0 ;
  wire \Ymap[5]_i_62_n_0 ;
  wire \Ymap[5]_i_63_n_0 ;
  wire \Ymap[5]_i_64_n_0 ;
  wire \Ymap[5]_i_65_n_0 ;
  wire \Ymap[5]_i_66_n_0 ;
  wire \Ymap[5]_i_67_n_0 ;
  wire \Ymap[5]_i_68_n_0 ;
  wire \Ymap[5]_i_69_n_0 ;
  wire \Ymap[5]_i_70_n_0 ;
  wire \Ymap[5]_i_71_n_0 ;
  wire \Ymap[5]_i_72_n_0 ;
  wire \Ymap[5]_i_73_n_0 ;
  wire \Ymap[5]_i_74_n_0 ;
  wire \Ymap[5]_i_75_n_0 ;
  wire \Ymap[5]_i_76_n_0 ;
  wire \Ymap[5]_i_77_n_0 ;
  wire \Ymap[5]_i_78_n_0 ;
  wire \Ymap[5]_i_79_n_0 ;
  wire \Ymap[5]_i_7_n_0 ;
  wire \Ymap[5]_i_80_n_0 ;
  wire \Ymap[5]_i_81_n_0 ;
  wire \Ymap[5]_i_82_n_0 ;
  wire \Ymap[5]_i_83_n_0 ;
  wire \Ymap[5]_i_84_n_0 ;
  wire \Ymap[5]_i_85_n_0 ;
  wire \Ymap[5]_i_86_n_0 ;
  wire \Ymap[5]_i_87_n_0 ;
  wire \Ymap[5]_i_88_n_0 ;
  wire \Ymap[5]_i_89_n_0 ;
  wire \Ymap[5]_i_8_n_0 ;
  wire \Ymap[5]_i_90_n_0 ;
  wire \Ymap[5]_i_91_n_0 ;
  wire \Ymap[5]_i_92_n_0 ;
  wire \Ymap[5]_i_93_n_0 ;
  wire \Ymap[5]_i_94_n_0 ;
  wire \Ymap[5]_i_95_n_0 ;
  wire \Ymap[5]_i_96_n_0 ;
  wire \Ymap[5]_i_97_n_0 ;
  wire \Ymap[5]_i_98_n_0 ;
  wire \Ymap[5]_i_99_n_0 ;
  wire \Ymap[5]_i_9_n_0 ;
  wire \Ymap_reg[0]_i_103_n_0 ;
  wire \Ymap_reg[0]_i_103_n_1 ;
  wire \Ymap_reg[0]_i_103_n_2 ;
  wire \Ymap_reg[0]_i_103_n_3 ;
  wire \Ymap_reg[0]_i_103_n_4 ;
  wire \Ymap_reg[0]_i_103_n_5 ;
  wire \Ymap_reg[0]_i_103_n_6 ;
  wire \Ymap_reg[0]_i_103_n_7 ;
  wire \Ymap_reg[0]_i_104_n_0 ;
  wire \Ymap_reg[0]_i_104_n_1 ;
  wire \Ymap_reg[0]_i_104_n_2 ;
  wire \Ymap_reg[0]_i_104_n_3 ;
  wire \Ymap_reg[0]_i_104_n_4 ;
  wire \Ymap_reg[0]_i_104_n_5 ;
  wire \Ymap_reg[0]_i_104_n_6 ;
  wire \Ymap_reg[0]_i_105_n_0 ;
  wire \Ymap_reg[0]_i_105_n_1 ;
  wire \Ymap_reg[0]_i_105_n_2 ;
  wire \Ymap_reg[0]_i_105_n_3 ;
  wire \Ymap_reg[0]_i_105_n_7 ;
  wire \Ymap_reg[0]_i_118_n_0 ;
  wire \Ymap_reg[0]_i_118_n_1 ;
  wire \Ymap_reg[0]_i_118_n_2 ;
  wire \Ymap_reg[0]_i_118_n_3 ;
  wire \Ymap_reg[0]_i_118_n_4 ;
  wire \Ymap_reg[0]_i_118_n_5 ;
  wire \Ymap_reg[0]_i_118_n_6 ;
  wire \Ymap_reg[0]_i_118_n_7 ;
  wire \Ymap_reg[0]_i_12_n_0 ;
  wire \Ymap_reg[0]_i_12_n_1 ;
  wire \Ymap_reg[0]_i_12_n_2 ;
  wire \Ymap_reg[0]_i_12_n_3 ;
  wire \Ymap_reg[0]_i_131_n_0 ;
  wire \Ymap_reg[0]_i_131_n_1 ;
  wire \Ymap_reg[0]_i_131_n_2 ;
  wire \Ymap_reg[0]_i_131_n_3 ;
  wire \Ymap_reg[0]_i_131_n_4 ;
  wire \Ymap_reg[0]_i_131_n_5 ;
  wire \Ymap_reg[0]_i_131_n_6 ;
  wire \Ymap_reg[0]_i_131_n_7 ;
  wire \Ymap_reg[0]_i_136_n_0 ;
  wire \Ymap_reg[0]_i_136_n_1 ;
  wire \Ymap_reg[0]_i_136_n_2 ;
  wire \Ymap_reg[0]_i_136_n_3 ;
  wire \Ymap_reg[0]_i_136_n_4 ;
  wire \Ymap_reg[0]_i_136_n_5 ;
  wire \Ymap_reg[0]_i_136_n_6 ;
  wire \Ymap_reg[0]_i_137_n_0 ;
  wire \Ymap_reg[0]_i_137_n_1 ;
  wire \Ymap_reg[0]_i_137_n_2 ;
  wire \Ymap_reg[0]_i_137_n_3 ;
  wire \Ymap_reg[0]_i_137_n_4 ;
  wire \Ymap_reg[0]_i_137_n_5 ;
  wire \Ymap_reg[0]_i_137_n_6 ;
  wire \Ymap_reg[0]_i_137_n_7 ;
  wire \Ymap_reg[0]_i_159_n_0 ;
  wire \Ymap_reg[0]_i_159_n_1 ;
  wire \Ymap_reg[0]_i_159_n_2 ;
  wire \Ymap_reg[0]_i_159_n_3 ;
  wire \Ymap_reg[0]_i_159_n_4 ;
  wire \Ymap_reg[0]_i_159_n_5 ;
  wire \Ymap_reg[0]_i_159_n_6 ;
  wire \Ymap_reg[0]_i_159_n_7 ;
  wire \Ymap_reg[0]_i_164_n_0 ;
  wire \Ymap_reg[0]_i_164_n_1 ;
  wire \Ymap_reg[0]_i_164_n_2 ;
  wire \Ymap_reg[0]_i_164_n_3 ;
  wire \Ymap_reg[0]_i_165_n_0 ;
  wire \Ymap_reg[0]_i_165_n_1 ;
  wire \Ymap_reg[0]_i_165_n_2 ;
  wire \Ymap_reg[0]_i_165_n_3 ;
  wire \Ymap_reg[0]_i_165_n_4 ;
  wire \Ymap_reg[0]_i_165_n_5 ;
  wire \Ymap_reg[0]_i_165_n_6 ;
  wire \Ymap_reg[0]_i_165_n_7 ;
  wire \Ymap_reg[0]_i_174_n_0 ;
  wire \Ymap_reg[0]_i_174_n_1 ;
  wire \Ymap_reg[0]_i_174_n_2 ;
  wire \Ymap_reg[0]_i_174_n_3 ;
  wire \Ymap_reg[0]_i_174_n_4 ;
  wire \Ymap_reg[0]_i_174_n_5 ;
  wire \Ymap_reg[0]_i_174_n_6 ;
  wire \Ymap_reg[0]_i_174_n_7 ;
  wire \Ymap_reg[0]_i_179_n_0 ;
  wire \Ymap_reg[0]_i_179_n_1 ;
  wire \Ymap_reg[0]_i_179_n_2 ;
  wire \Ymap_reg[0]_i_179_n_3 ;
  wire \Ymap_reg[0]_i_179_n_4 ;
  wire \Ymap_reg[0]_i_179_n_5 ;
  wire \Ymap_reg[0]_i_179_n_6 ;
  wire \Ymap_reg[0]_i_179_n_7 ;
  wire \Ymap_reg[0]_i_21_n_0 ;
  wire \Ymap_reg[0]_i_21_n_1 ;
  wire \Ymap_reg[0]_i_21_n_2 ;
  wire \Ymap_reg[0]_i_21_n_3 ;
  wire \Ymap_reg[0]_i_21_n_4 ;
  wire \Ymap_reg[0]_i_21_n_5 ;
  wire \Ymap_reg[0]_i_21_n_6 ;
  wire \Ymap_reg[0]_i_21_n_7 ;
  wire \Ymap_reg[0]_i_22_n_0 ;
  wire \Ymap_reg[0]_i_22_n_1 ;
  wire \Ymap_reg[0]_i_22_n_2 ;
  wire \Ymap_reg[0]_i_22_n_3 ;
  wire \Ymap_reg[0]_i_22_n_4 ;
  wire \Ymap_reg[0]_i_22_n_5 ;
  wire \Ymap_reg[0]_i_22_n_6 ;
  wire \Ymap_reg[0]_i_22_n_7 ;
  wire \Ymap_reg[0]_i_23_n_0 ;
  wire \Ymap_reg[0]_i_23_n_1 ;
  wire \Ymap_reg[0]_i_23_n_2 ;
  wire \Ymap_reg[0]_i_23_n_3 ;
  wire \Ymap_reg[0]_i_23_n_4 ;
  wire \Ymap_reg[0]_i_23_n_5 ;
  wire \Ymap_reg[0]_i_23_n_6 ;
  wire \Ymap_reg[0]_i_23_n_7 ;
  wire \Ymap_reg[0]_i_24_n_0 ;
  wire \Ymap_reg[0]_i_24_n_1 ;
  wire \Ymap_reg[0]_i_24_n_2 ;
  wire \Ymap_reg[0]_i_24_n_3 ;
  wire \Ymap_reg[0]_i_2_n_0 ;
  wire \Ymap_reg[0]_i_2_n_1 ;
  wire \Ymap_reg[0]_i_2_n_2 ;
  wire \Ymap_reg[0]_i_2_n_3 ;
  wire \Ymap_reg[0]_i_2_n_4 ;
  wire \Ymap_reg[0]_i_33_n_0 ;
  wire \Ymap_reg[0]_i_33_n_1 ;
  wire \Ymap_reg[0]_i_33_n_2 ;
  wire \Ymap_reg[0]_i_33_n_3 ;
  wire \Ymap_reg[0]_i_33_n_4 ;
  wire \Ymap_reg[0]_i_33_n_5 ;
  wire \Ymap_reg[0]_i_33_n_6 ;
  wire \Ymap_reg[0]_i_33_n_7 ;
  wire \Ymap_reg[0]_i_34_n_0 ;
  wire \Ymap_reg[0]_i_34_n_1 ;
  wire \Ymap_reg[0]_i_34_n_2 ;
  wire \Ymap_reg[0]_i_34_n_3 ;
  wire \Ymap_reg[0]_i_34_n_4 ;
  wire \Ymap_reg[0]_i_34_n_5 ;
  wire \Ymap_reg[0]_i_34_n_6 ;
  wire \Ymap_reg[0]_i_34_n_7 ;
  wire \Ymap_reg[0]_i_35_n_0 ;
  wire \Ymap_reg[0]_i_35_n_1 ;
  wire \Ymap_reg[0]_i_35_n_2 ;
  wire \Ymap_reg[0]_i_35_n_3 ;
  wire \Ymap_reg[0]_i_35_n_4 ;
  wire \Ymap_reg[0]_i_35_n_5 ;
  wire \Ymap_reg[0]_i_35_n_6 ;
  wire \Ymap_reg[0]_i_35_n_7 ;
  wire \Ymap_reg[0]_i_3_n_0 ;
  wire \Ymap_reg[0]_i_3_n_1 ;
  wire \Ymap_reg[0]_i_3_n_2 ;
  wire \Ymap_reg[0]_i_3_n_3 ;
  wire \Ymap_reg[0]_i_60_n_0 ;
  wire \Ymap_reg[0]_i_60_n_1 ;
  wire \Ymap_reg[0]_i_60_n_2 ;
  wire \Ymap_reg[0]_i_60_n_3 ;
  wire \Ymap_reg[0]_i_69_n_0 ;
  wire \Ymap_reg[0]_i_69_n_1 ;
  wire \Ymap_reg[0]_i_69_n_2 ;
  wire \Ymap_reg[0]_i_69_n_3 ;
  wire \Ymap_reg[0]_i_69_n_4 ;
  wire \Ymap_reg[0]_i_69_n_5 ;
  wire \Ymap_reg[0]_i_69_n_6 ;
  wire \Ymap_reg[0]_i_69_n_7 ;
  wire \Ymap_reg[0]_i_70_n_0 ;
  wire \Ymap_reg[0]_i_70_n_1 ;
  wire \Ymap_reg[0]_i_70_n_2 ;
  wire \Ymap_reg[0]_i_70_n_3 ;
  wire \Ymap_reg[0]_i_70_n_4 ;
  wire \Ymap_reg[0]_i_70_n_5 ;
  wire \Ymap_reg[0]_i_70_n_6 ;
  wire \Ymap_reg[0]_i_71_n_0 ;
  wire \Ymap_reg[0]_i_71_n_1 ;
  wire \Ymap_reg[0]_i_71_n_2 ;
  wire \Ymap_reg[0]_i_71_n_3 ;
  wire \Ymap_reg[0]_i_71_n_4 ;
  wire \Ymap_reg[0]_i_71_n_5 ;
  wire \Ymap_reg[0]_i_71_n_6 ;
  wire \Ymap_reg[0]_i_71_n_7 ;
  wire \Ymap_reg[0]_i_72_n_0 ;
  wire \Ymap_reg[0]_i_72_n_1 ;
  wire \Ymap_reg[0]_i_72_n_2 ;
  wire \Ymap_reg[0]_i_72_n_3 ;
  wire \Ymap_reg[0]_i_72_n_4 ;
  wire \Ymap_reg[0]_i_72_n_5 ;
  wire \Ymap_reg[0]_i_72_n_6 ;
  wire \Ymap_reg[0]_i_72_n_7 ;
  wire \Ymap_reg[0]_i_83_n_0 ;
  wire \Ymap_reg[0]_i_83_n_1 ;
  wire \Ymap_reg[0]_i_83_n_2 ;
  wire \Ymap_reg[0]_i_83_n_3 ;
  wire \Ymap_reg[0]_i_83_n_4 ;
  wire \Ymap_reg[0]_i_83_n_5 ;
  wire \Ymap_reg[0]_i_83_n_6 ;
  wire \Ymap_reg[0]_i_83_n_7 ;
  wire \Ymap_reg[0]_i_95_n_0 ;
  wire \Ymap_reg[0]_i_95_n_1 ;
  wire \Ymap_reg[0]_i_95_n_2 ;
  wire \Ymap_reg[0]_i_95_n_3 ;
  wire \Ymap_reg[0]_i_95_n_4 ;
  wire \Ymap_reg[0]_i_95_n_5 ;
  wire \Ymap_reg[0]_i_95_n_6 ;
  wire \Ymap_reg[0]_i_95_n_7 ;
  wire [3:0]\Ymap_reg[2]_0 ;
  wire [3:0]\Ymap_reg[3]_0 ;
  wire \Ymap_reg[3]_i_2_n_0 ;
  wire \Ymap_reg[3]_i_2_n_1 ;
  wire \Ymap_reg[3]_i_2_n_2 ;
  wire \Ymap_reg[3]_i_2_n_3 ;
  wire \Ymap_reg[3]_i_2_n_4 ;
  wire \Ymap_reg[3]_i_2_n_5 ;
  wire \Ymap_reg[3]_i_2_n_6 ;
  wire \Ymap_reg[3]_i_2_n_7 ;
  wire \Ymap_reg[4]_i_11_n_0 ;
  wire \Ymap_reg[4]_i_11_n_1 ;
  wire \Ymap_reg[4]_i_11_n_2 ;
  wire \Ymap_reg[4]_i_11_n_3 ;
  wire \Ymap_reg[4]_i_11_n_4 ;
  wire \Ymap_reg[4]_i_11_n_5 ;
  wire \Ymap_reg[4]_i_11_n_6 ;
  wire \Ymap_reg[4]_i_11_n_7 ;
  wire \Ymap_reg[4]_i_12_n_0 ;
  wire \Ymap_reg[4]_i_12_n_1 ;
  wire \Ymap_reg[4]_i_12_n_2 ;
  wire \Ymap_reg[4]_i_12_n_3 ;
  wire \Ymap_reg[4]_i_12_n_4 ;
  wire \Ymap_reg[4]_i_12_n_5 ;
  wire \Ymap_reg[4]_i_12_n_6 ;
  wire \Ymap_reg[4]_i_12_n_7 ;
  wire \Ymap_reg[4]_i_13_n_0 ;
  wire \Ymap_reg[4]_i_13_n_1 ;
  wire \Ymap_reg[4]_i_13_n_2 ;
  wire \Ymap_reg[4]_i_13_n_3 ;
  wire \Ymap_reg[4]_i_13_n_4 ;
  wire \Ymap_reg[4]_i_13_n_5 ;
  wire \Ymap_reg[4]_i_13_n_6 ;
  wire \Ymap_reg[4]_i_13_n_7 ;
  wire \Ymap_reg[4]_i_2_n_0 ;
  wire \Ymap_reg[4]_i_2_n_1 ;
  wire \Ymap_reg[4]_i_2_n_2 ;
  wire \Ymap_reg[4]_i_2_n_3 ;
  wire \Ymap_reg[4]_i_2_n_4 ;
  wire \Ymap_reg[4]_i_2_n_5 ;
  wire \Ymap_reg[4]_i_2_n_6 ;
  wire \Ymap_reg[4]_i_2_n_7 ;
  wire \Ymap_reg[4]_i_38_n_0 ;
  wire \Ymap_reg[4]_i_38_n_1 ;
  wire \Ymap_reg[4]_i_38_n_2 ;
  wire \Ymap_reg[4]_i_38_n_3 ;
  wire \Ymap_reg[4]_i_38_n_4 ;
  wire \Ymap_reg[4]_i_38_n_5 ;
  wire \Ymap_reg[4]_i_38_n_6 ;
  wire \Ymap_reg[4]_i_38_n_7 ;
  wire \Ymap_reg[4]_i_39_n_0 ;
  wire \Ymap_reg[4]_i_39_n_1 ;
  wire \Ymap_reg[4]_i_39_n_2 ;
  wire \Ymap_reg[4]_i_39_n_3 ;
  wire \Ymap_reg[4]_i_39_n_4 ;
  wire \Ymap_reg[4]_i_39_n_5 ;
  wire \Ymap_reg[4]_i_39_n_6 ;
  wire \Ymap_reg[4]_i_39_n_7 ;
  wire \Ymap_reg[5]_i_104_n_0 ;
  wire \Ymap_reg[5]_i_104_n_1 ;
  wire \Ymap_reg[5]_i_104_n_2 ;
  wire \Ymap_reg[5]_i_104_n_3 ;
  wire \Ymap_reg[5]_i_104_n_4 ;
  wire \Ymap_reg[5]_i_104_n_5 ;
  wire \Ymap_reg[5]_i_104_n_6 ;
  wire \Ymap_reg[5]_i_104_n_7 ;
  wire \Ymap_reg[5]_i_105_n_0 ;
  wire \Ymap_reg[5]_i_105_n_1 ;
  wire \Ymap_reg[5]_i_105_n_2 ;
  wire \Ymap_reg[5]_i_105_n_3 ;
  wire \Ymap_reg[5]_i_105_n_4 ;
  wire \Ymap_reg[5]_i_105_n_5 ;
  wire \Ymap_reg[5]_i_105_n_6 ;
  wire \Ymap_reg[5]_i_105_n_7 ;
  wire \Ymap_reg[5]_i_117_n_0 ;
  wire \Ymap_reg[5]_i_117_n_1 ;
  wire \Ymap_reg[5]_i_117_n_2 ;
  wire \Ymap_reg[5]_i_117_n_3 ;
  wire \Ymap_reg[5]_i_126_n_0 ;
  wire \Ymap_reg[5]_i_126_n_1 ;
  wire \Ymap_reg[5]_i_126_n_2 ;
  wire \Ymap_reg[5]_i_126_n_3 ;
  wire \Ymap_reg[5]_i_126_n_4 ;
  wire \Ymap_reg[5]_i_126_n_5 ;
  wire \Ymap_reg[5]_i_126_n_6 ;
  wire \Ymap_reg[5]_i_126_n_7 ;
  wire \Ymap_reg[5]_i_127_n_1 ;
  wire \Ymap_reg[5]_i_127_n_3 ;
  wire \Ymap_reg[5]_i_127_n_6 ;
  wire \Ymap_reg[5]_i_127_n_7 ;
  wire \Ymap_reg[5]_i_128_n_0 ;
  wire \Ymap_reg[5]_i_128_n_1 ;
  wire \Ymap_reg[5]_i_128_n_2 ;
  wire \Ymap_reg[5]_i_128_n_3 ;
  wire \Ymap_reg[5]_i_128_n_4 ;
  wire \Ymap_reg[5]_i_128_n_5 ;
  wire \Ymap_reg[5]_i_128_n_6 ;
  wire \Ymap_reg[5]_i_128_n_7 ;
  wire \Ymap_reg[5]_i_129_n_0 ;
  wire \Ymap_reg[5]_i_129_n_1 ;
  wire \Ymap_reg[5]_i_129_n_2 ;
  wire \Ymap_reg[5]_i_129_n_3 ;
  wire \Ymap_reg[5]_i_129_n_4 ;
  wire \Ymap_reg[5]_i_129_n_5 ;
  wire \Ymap_reg[5]_i_129_n_6 ;
  wire \Ymap_reg[5]_i_129_n_7 ;
  wire \Ymap_reg[5]_i_130_n_0 ;
  wire \Ymap_reg[5]_i_130_n_1 ;
  wire \Ymap_reg[5]_i_130_n_2 ;
  wire \Ymap_reg[5]_i_130_n_3 ;
  wire \Ymap_reg[5]_i_130_n_4 ;
  wire \Ymap_reg[5]_i_130_n_5 ;
  wire \Ymap_reg[5]_i_130_n_6 ;
  wire \Ymap_reg[5]_i_130_n_7 ;
  wire \Ymap_reg[5]_i_131_n_0 ;
  wire \Ymap_reg[5]_i_131_n_1 ;
  wire \Ymap_reg[5]_i_131_n_2 ;
  wire \Ymap_reg[5]_i_131_n_3 ;
  wire \Ymap_reg[5]_i_131_n_4 ;
  wire \Ymap_reg[5]_i_131_n_5 ;
  wire \Ymap_reg[5]_i_131_n_6 ;
  wire \Ymap_reg[5]_i_131_n_7 ;
  wire \Ymap_reg[5]_i_132_n_0 ;
  wire \Ymap_reg[5]_i_132_n_1 ;
  wire \Ymap_reg[5]_i_132_n_2 ;
  wire \Ymap_reg[5]_i_132_n_3 ;
  wire \Ymap_reg[5]_i_132_n_4 ;
  wire \Ymap_reg[5]_i_132_n_5 ;
  wire \Ymap_reg[5]_i_132_n_6 ;
  wire \Ymap_reg[5]_i_132_n_7 ;
  wire \Ymap_reg[5]_i_145_n_1 ;
  wire \Ymap_reg[5]_i_145_n_3 ;
  wire \Ymap_reg[5]_i_145_n_6 ;
  wire \Ymap_reg[5]_i_145_n_7 ;
  wire \Ymap_reg[5]_i_146_n_0 ;
  wire \Ymap_reg[5]_i_146_n_1 ;
  wire \Ymap_reg[5]_i_146_n_2 ;
  wire \Ymap_reg[5]_i_146_n_3 ;
  wire \Ymap_reg[5]_i_146_n_4 ;
  wire \Ymap_reg[5]_i_146_n_5 ;
  wire \Ymap_reg[5]_i_146_n_6 ;
  wire \Ymap_reg[5]_i_146_n_7 ;
  wire \Ymap_reg[5]_i_147_n_0 ;
  wire \Ymap_reg[5]_i_147_n_1 ;
  wire \Ymap_reg[5]_i_147_n_2 ;
  wire \Ymap_reg[5]_i_147_n_3 ;
  wire \Ymap_reg[5]_i_147_n_4 ;
  wire \Ymap_reg[5]_i_147_n_5 ;
  wire \Ymap_reg[5]_i_147_n_6 ;
  wire \Ymap_reg[5]_i_147_n_7 ;
  wire \Ymap_reg[5]_i_148_n_3 ;
  wire \Ymap_reg[5]_i_149_n_7 ;
  wire \Ymap_reg[5]_i_150_n_0 ;
  wire \Ymap_reg[5]_i_150_n_1 ;
  wire \Ymap_reg[5]_i_150_n_2 ;
  wire \Ymap_reg[5]_i_150_n_3 ;
  wire \Ymap_reg[5]_i_191_n_0 ;
  wire \Ymap_reg[5]_i_191_n_1 ;
  wire \Ymap_reg[5]_i_191_n_2 ;
  wire \Ymap_reg[5]_i_191_n_3 ;
  wire \Ymap_reg[5]_i_191_n_4 ;
  wire \Ymap_reg[5]_i_191_n_5 ;
  wire \Ymap_reg[5]_i_191_n_6 ;
  wire \Ymap_reg[5]_i_191_n_7 ;
  wire \Ymap_reg[5]_i_192_n_0 ;
  wire \Ymap_reg[5]_i_192_n_1 ;
  wire \Ymap_reg[5]_i_192_n_2 ;
  wire \Ymap_reg[5]_i_192_n_3 ;
  wire \Ymap_reg[5]_i_192_n_4 ;
  wire \Ymap_reg[5]_i_192_n_5 ;
  wire \Ymap_reg[5]_i_192_n_6 ;
  wire \Ymap_reg[5]_i_192_n_7 ;
  wire \Ymap_reg[5]_i_205_n_0 ;
  wire \Ymap_reg[5]_i_205_n_1 ;
  wire \Ymap_reg[5]_i_205_n_2 ;
  wire \Ymap_reg[5]_i_205_n_3 ;
  wire \Ymap_reg[5]_i_205_n_4 ;
  wire \Ymap_reg[5]_i_205_n_5 ;
  wire \Ymap_reg[5]_i_205_n_6 ;
  wire \Ymap_reg[5]_i_205_n_7 ;
  wire \Ymap_reg[5]_i_206_n_0 ;
  wire \Ymap_reg[5]_i_206_n_1 ;
  wire \Ymap_reg[5]_i_206_n_2 ;
  wire \Ymap_reg[5]_i_206_n_3 ;
  wire \Ymap_reg[5]_i_206_n_4 ;
  wire \Ymap_reg[5]_i_206_n_5 ;
  wire \Ymap_reg[5]_i_206_n_6 ;
  wire \Ymap_reg[5]_i_206_n_7 ;
  wire \Ymap_reg[5]_i_207_n_0 ;
  wire \Ymap_reg[5]_i_207_n_1 ;
  wire \Ymap_reg[5]_i_207_n_2 ;
  wire \Ymap_reg[5]_i_207_n_3 ;
  wire \Ymap_reg[5]_i_207_n_4 ;
  wire \Ymap_reg[5]_i_207_n_5 ;
  wire \Ymap_reg[5]_i_207_n_6 ;
  wire \Ymap_reg[5]_i_207_n_7 ;
  wire \Ymap_reg[5]_i_21_n_0 ;
  wire \Ymap_reg[5]_i_21_n_1 ;
  wire \Ymap_reg[5]_i_21_n_2 ;
  wire \Ymap_reg[5]_i_21_n_3 ;
  wire \Ymap_reg[5]_i_21_n_4 ;
  wire \Ymap_reg[5]_i_21_n_5 ;
  wire \Ymap_reg[5]_i_21_n_6 ;
  wire \Ymap_reg[5]_i_21_n_7 ;
  wire \Ymap_reg[5]_i_225_n_0 ;
  wire \Ymap_reg[5]_i_225_n_1 ;
  wire \Ymap_reg[5]_i_225_n_2 ;
  wire \Ymap_reg[5]_i_225_n_3 ;
  wire \Ymap_reg[5]_i_234_n_0 ;
  wire \Ymap_reg[5]_i_234_n_1 ;
  wire \Ymap_reg[5]_i_234_n_2 ;
  wire \Ymap_reg[5]_i_234_n_3 ;
  wire \Ymap_reg[5]_i_234_n_4 ;
  wire \Ymap_reg[5]_i_234_n_5 ;
  wire \Ymap_reg[5]_i_234_n_6 ;
  wire \Ymap_reg[5]_i_234_n_7 ;
  wire \Ymap_reg[5]_i_247_n_0 ;
  wire \Ymap_reg[5]_i_247_n_1 ;
  wire \Ymap_reg[5]_i_247_n_2 ;
  wire \Ymap_reg[5]_i_247_n_3 ;
  wire \Ymap_reg[5]_i_247_n_4 ;
  wire \Ymap_reg[5]_i_247_n_5 ;
  wire \Ymap_reg[5]_i_247_n_6 ;
  wire \Ymap_reg[5]_i_247_n_7 ;
  wire \Ymap_reg[5]_i_248_n_0 ;
  wire \Ymap_reg[5]_i_248_n_1 ;
  wire \Ymap_reg[5]_i_248_n_2 ;
  wire \Ymap_reg[5]_i_248_n_3 ;
  wire \Ymap_reg[5]_i_248_n_4 ;
  wire \Ymap_reg[5]_i_248_n_5 ;
  wire \Ymap_reg[5]_i_248_n_6 ;
  wire \Ymap_reg[5]_i_248_n_7 ;
  wire \Ymap_reg[5]_i_26_n_0 ;
  wire \Ymap_reg[5]_i_26_n_1 ;
  wire \Ymap_reg[5]_i_26_n_2 ;
  wire \Ymap_reg[5]_i_26_n_3 ;
  wire \Ymap_reg[5]_i_270_n_3 ;
  wire \Ymap_reg[5]_i_2_n_1 ;
  wire \Ymap_reg[5]_i_2_n_2 ;
  wire \Ymap_reg[5]_i_2_n_3 ;
  wire \Ymap_reg[5]_i_35_n_0 ;
  wire \Ymap_reg[5]_i_35_n_1 ;
  wire \Ymap_reg[5]_i_35_n_2 ;
  wire \Ymap_reg[5]_i_35_n_3 ;
  wire \Ymap_reg[5]_i_35_n_4 ;
  wire \Ymap_reg[5]_i_35_n_5 ;
  wire \Ymap_reg[5]_i_35_n_6 ;
  wire \Ymap_reg[5]_i_35_n_7 ;
  wire \Ymap_reg[5]_i_36_n_0 ;
  wire \Ymap_reg[5]_i_36_n_1 ;
  wire \Ymap_reg[5]_i_36_n_2 ;
  wire \Ymap_reg[5]_i_36_n_3 ;
  wire \Ymap_reg[5]_i_36_n_4 ;
  wire \Ymap_reg[5]_i_36_n_5 ;
  wire \Ymap_reg[5]_i_36_n_6 ;
  wire \Ymap_reg[5]_i_36_n_7 ;
  wire \Ymap_reg[5]_i_37_n_0 ;
  wire \Ymap_reg[5]_i_37_n_1 ;
  wire \Ymap_reg[5]_i_37_n_2 ;
  wire \Ymap_reg[5]_i_37_n_3 ;
  wire \Ymap_reg[5]_i_37_n_4 ;
  wire \Ymap_reg[5]_i_37_n_5 ;
  wire \Ymap_reg[5]_i_37_n_6 ;
  wire \Ymap_reg[5]_i_37_n_7 ;
  wire \Ymap_reg[5]_i_38_n_0 ;
  wire \Ymap_reg[5]_i_38_n_1 ;
  wire \Ymap_reg[5]_i_38_n_2 ;
  wire \Ymap_reg[5]_i_38_n_3 ;
  wire \Ymap_reg[5]_i_38_n_4 ;
  wire \Ymap_reg[5]_i_38_n_5 ;
  wire \Ymap_reg[5]_i_38_n_6 ;
  wire \Ymap_reg[5]_i_38_n_7 ;
  wire \Ymap_reg[5]_i_39_n_0 ;
  wire \Ymap_reg[5]_i_39_n_1 ;
  wire \Ymap_reg[5]_i_39_n_2 ;
  wire \Ymap_reg[5]_i_39_n_3 ;
  wire \Ymap_reg[5]_i_39_n_4 ;
  wire \Ymap_reg[5]_i_39_n_5 ;
  wire \Ymap_reg[5]_i_39_n_6 ;
  wire \Ymap_reg[5]_i_39_n_7 ;
  wire \Ymap_reg[5]_i_3_n_0 ;
  wire \Ymap_reg[5]_i_3_n_1 ;
  wire \Ymap_reg[5]_i_3_n_2 ;
  wire \Ymap_reg[5]_i_3_n_3 ;
  wire \Ymap_reg[5]_i_3_n_4 ;
  wire \Ymap_reg[5]_i_3_n_5 ;
  wire \Ymap_reg[5]_i_3_n_6 ;
  wire \Ymap_reg[5]_i_3_n_7 ;
  wire \Ymap_reg[5]_i_40_n_0 ;
  wire \Ymap_reg[5]_i_40_n_1 ;
  wire \Ymap_reg[5]_i_40_n_2 ;
  wire \Ymap_reg[5]_i_40_n_3 ;
  wire \Ymap_reg[5]_i_40_n_4 ;
  wire \Ymap_reg[5]_i_40_n_5 ;
  wire \Ymap_reg[5]_i_40_n_6 ;
  wire \Ymap_reg[5]_i_40_n_7 ;
  wire \Ymap_reg[5]_i_41_n_0 ;
  wire \Ymap_reg[5]_i_41_n_1 ;
  wire \Ymap_reg[5]_i_41_n_2 ;
  wire \Ymap_reg[5]_i_41_n_3 ;
  wire \Ymap_reg[5]_i_41_n_4 ;
  wire \Ymap_reg[5]_i_41_n_5 ;
  wire \Ymap_reg[5]_i_41_n_6 ;
  wire \Ymap_reg[5]_i_41_n_7 ;
  wire \Ymap_reg[5]_i_42_n_1 ;
  wire \Ymap_reg[5]_i_42_n_2 ;
  wire \Ymap_reg[5]_i_42_n_3 ;
  wire \Ymap_reg[5]_i_42_n_4 ;
  wire \Ymap_reg[5]_i_42_n_5 ;
  wire \Ymap_reg[5]_i_42_n_6 ;
  wire \Ymap_reg[5]_i_42_n_7 ;
  wire \Ymap_reg[5]_i_47_n_0 ;
  wire \Ymap_reg[5]_i_47_n_1 ;
  wire \Ymap_reg[5]_i_47_n_2 ;
  wire \Ymap_reg[5]_i_47_n_3 ;
  wire \Ymap_reg[5]_i_4_n_3 ;
  wire \Ymap_reg[5]_i_4_n_6 ;
  wire \Ymap_reg[5]_i_4_n_7 ;
  wire \Ymap_reg[5]_i_5_n_3 ;
  wire \Ymap_reg[5]_i_5_n_6 ;
  wire \Ymap_reg[5]_i_5_n_7 ;
  wire \Ymap_reg[5]_i_6_n_0 ;
  wire \Ymap_reg[5]_i_6_n_1 ;
  wire \Ymap_reg[5]_i_6_n_2 ;
  wire \Ymap_reg[5]_i_6_n_3 ;
  wire clk;
  wire [30:0]cnt;
  wire \cnt[12]_i_3_n_0 ;
  wire \cnt[12]_i_4_n_0 ;
  wire \cnt[12]_i_5_n_0 ;
  wire \cnt[12]_i_6_n_0 ;
  wire \cnt[16]_i_3_n_0 ;
  wire \cnt[16]_i_4_n_0 ;
  wire \cnt[16]_i_5_n_0 ;
  wire \cnt[16]_i_6_n_0 ;
  wire \cnt[20]_i_3_n_0 ;
  wire \cnt[20]_i_4_n_0 ;
  wire \cnt[20]_i_5_n_0 ;
  wire \cnt[20]_i_6_n_0 ;
  wire \cnt[24]_i_3_n_0 ;
  wire \cnt[24]_i_4_n_0 ;
  wire \cnt[24]_i_5_n_0 ;
  wire \cnt[24]_i_6_n_0 ;
  wire \cnt[28]_i_3_n_0 ;
  wire \cnt[28]_i_4_n_0 ;
  wire \cnt[28]_i_5_n_0 ;
  wire \cnt[28]_i_6_n_0 ;
  wire \cnt[30]_i_1_n_0 ;
  wire \cnt[30]_i_4_n_0 ;
  wire \cnt[30]_i_5_n_0 ;
  wire \cnt[4]_i_3_n_0 ;
  wire \cnt[4]_i_4_n_0 ;
  wire \cnt[4]_i_5_n_0 ;
  wire \cnt[4]_i_6_n_0 ;
  wire \cnt[8]_i_3_n_0 ;
  wire \cnt[8]_i_4_n_0 ;
  wire \cnt[8]_i_5_n_0 ;
  wire \cnt[8]_i_6_n_0 ;
  wire \cnt_reg[12]_i_2_n_0 ;
  wire \cnt_reg[12]_i_2_n_1 ;
  wire \cnt_reg[12]_i_2_n_2 ;
  wire \cnt_reg[12]_i_2_n_3 ;
  wire \cnt_reg[12]_i_2_n_4 ;
  wire \cnt_reg[12]_i_2_n_5 ;
  wire \cnt_reg[12]_i_2_n_6 ;
  wire \cnt_reg[12]_i_2_n_7 ;
  wire \cnt_reg[16]_i_2_n_0 ;
  wire \cnt_reg[16]_i_2_n_1 ;
  wire \cnt_reg[16]_i_2_n_2 ;
  wire \cnt_reg[16]_i_2_n_3 ;
  wire \cnt_reg[16]_i_2_n_4 ;
  wire \cnt_reg[16]_i_2_n_5 ;
  wire \cnt_reg[16]_i_2_n_6 ;
  wire \cnt_reg[16]_i_2_n_7 ;
  wire [3:0]\cnt_reg[17]_0 ;
  wire [3:0]\cnt_reg[17]_1 ;
  wire [3:0]\cnt_reg[1]_0 ;
  wire [3:0]\cnt_reg[1]_1 ;
  wire [0:0]\cnt_reg[20]_0 ;
  wire [1:0]\cnt_reg[20]_1 ;
  wire [2:0]\cnt_reg[20]_2 ;
  wire [2:0]\cnt_reg[20]_3 ;
  wire \cnt_reg[20]_i_2_n_0 ;
  wire \cnt_reg[20]_i_2_n_1 ;
  wire \cnt_reg[20]_i_2_n_2 ;
  wire \cnt_reg[20]_i_2_n_3 ;
  wire \cnt_reg[20]_i_2_n_4 ;
  wire \cnt_reg[20]_i_2_n_5 ;
  wire \cnt_reg[20]_i_2_n_6 ;
  wire \cnt_reg[20]_i_2_n_7 ;
  wire [3:0]\cnt_reg[21]_0 ;
  wire \cnt_reg[24]_i_2_n_0 ;
  wire \cnt_reg[24]_i_2_n_1 ;
  wire \cnt_reg[24]_i_2_n_2 ;
  wire \cnt_reg[24]_i_2_n_3 ;
  wire \cnt_reg[24]_i_2_n_4 ;
  wire \cnt_reg[24]_i_2_n_5 ;
  wire \cnt_reg[24]_i_2_n_6 ;
  wire \cnt_reg[24]_i_2_n_7 ;
  wire \cnt_reg[28]_i_2_n_0 ;
  wire \cnt_reg[28]_i_2_n_1 ;
  wire \cnt_reg[28]_i_2_n_2 ;
  wire \cnt_reg[28]_i_2_n_3 ;
  wire \cnt_reg[28]_i_2_n_4 ;
  wire \cnt_reg[28]_i_2_n_5 ;
  wire \cnt_reg[28]_i_2_n_6 ;
  wire \cnt_reg[28]_i_2_n_7 ;
  wire [3:0]\cnt_reg[2]_0 ;
  wire [3:0]\cnt_reg[2]_1 ;
  wire [3:0]\cnt_reg[2]_2 ;
  wire \cnt_reg[30]_i_3_n_3 ;
  wire \cnt_reg[30]_i_3_n_6 ;
  wire \cnt_reg[30]_i_3_n_7 ;
  wire \cnt_reg[4]_i_2_n_0 ;
  wire \cnt_reg[4]_i_2_n_1 ;
  wire \cnt_reg[4]_i_2_n_2 ;
  wire \cnt_reg[4]_i_2_n_3 ;
  wire \cnt_reg[4]_i_2_n_4 ;
  wire \cnt_reg[4]_i_2_n_5 ;
  wire \cnt_reg[4]_i_2_n_6 ;
  wire \cnt_reg[4]_i_2_n_7 ;
  wire \cnt_reg[8]_i_2_n_0 ;
  wire \cnt_reg[8]_i_2_n_1 ;
  wire \cnt_reg[8]_i_2_n_2 ;
  wire \cnt_reg[8]_i_2_n_3 ;
  wire \cnt_reg[8]_i_2_n_4 ;
  wire \cnt_reg[8]_i_2_n_5 ;
  wire \cnt_reg[8]_i_2_n_6 ;
  wire \cnt_reg[8]_i_2_n_7 ;
  wire \cnt_reg_n_0_[0] ;
  wire \cnt_reg_n_0_[10] ;
  wire \cnt_reg_n_0_[11] ;
  wire \cnt_reg_n_0_[12] ;
  wire \cnt_reg_n_0_[13] ;
  wire \cnt_reg_n_0_[14] ;
  wire \cnt_reg_n_0_[15] ;
  wire \cnt_reg_n_0_[16] ;
  wire \cnt_reg_n_0_[17] ;
  wire \cnt_reg_n_0_[18] ;
  wire \cnt_reg_n_0_[19] ;
  wire \cnt_reg_n_0_[1] ;
  wire \cnt_reg_n_0_[20] ;
  wire \cnt_reg_n_0_[21] ;
  wire \cnt_reg_n_0_[22] ;
  wire \cnt_reg_n_0_[23] ;
  wire \cnt_reg_n_0_[24] ;
  wire \cnt_reg_n_0_[25] ;
  wire \cnt_reg_n_0_[26] ;
  wire \cnt_reg_n_0_[27] ;
  wire \cnt_reg_n_0_[28] ;
  wire \cnt_reg_n_0_[29] ;
  wire \cnt_reg_n_0_[2] ;
  wire \cnt_reg_n_0_[30] ;
  wire \cnt_reg_n_0_[3] ;
  wire \cnt_reg_n_0_[4] ;
  wire \cnt_reg_n_0_[5] ;
  wire \cnt_reg_n_0_[6] ;
  wire \cnt_reg_n_0_[7] ;
  wire \cnt_reg_n_0_[8] ;
  wire \cnt_reg_n_0_[9] ;
  wire data_type;
  wire data_type_i_1_n_0;
  wire fetch;
  wire fetch_i_1_n_0;
  wire fetch_i_3_n_0;
  wire fetching;
  wire fetching_sprites;
  wire fetching_sprites_i_2_n_0;
  wire fetching_sprites_i_3_n_0;
  wire fetching_sprites_i_4_n_0;
  wire fetching_sprites_i_5_n_0;
  wire \ind_reg[0] ;
  wire led0;
  wire led0_i_1_n_0;
  wire led1;
  wire led1_i_1_n_0;
  wire led1_i_2_n_0;
  wire led2;
  wire led2_i_1_n_0;
  wire led3;
  wire led3_i_1_n_0;
  wire led3_i_2_n_0;
  wire led3_i_3_n_0;
  wire [6:0]map_id;
  wire \map_id[6]_i_1_n_0 ;
  wire \map_id[6]_i_2_n_0 ;
  wire [12:12]p_0_out;
  wire [5:0]packet_in;
  wire [5:0]pixel;
  wire \pixel_out[0]_i_1_n_0 ;
  wire \pixel_out[1]_i_1_n_0 ;
  wire \pixel_out[2]_i_1_n_0 ;
  wire \pixel_out[3]_i_1_n_0 ;
  wire \pixel_out[4]_i_1_n_0 ;
  wire \pixel_out[5]_i_1_n_0 ;
  wire \pixel_out[5]_i_2_n_0 ;
  wire \pixel_out[5]_i_3_n_0 ;
  wire rand;
  wire [6:0]rand0;
  wire \rand[6]_i_3_n_0 ;
  wire \rand_reg_n_0_[0] ;
  wire \rand_reg_n_0_[1] ;
  wire \rand_reg_n_0_[2] ;
  wire \rand_reg_n_0_[3] ;
  wire \rand_reg_n_0_[4] ;
  wire \rand_reg_n_0_[5] ;
  wire \rand_reg_n_0_[6] ;
  wire state;
  wire state05_out;
  wire state16_out;
  wire state18_out;
  (* RTL_KEEP = "yes" *) wire [2:0]state__0;
  wire [2:0]sw;
  wire [6:0]tm_reg_0;
  wire [2:0]tm_reg_0_0;
  wire [3:0]tm_reg_0_1;
  wire tm_reg_0_i_27_n_2;
  wire tm_reg_0_i_27_n_3;
  wire tm_reg_0_i_28_n_0;
  wire tm_reg_0_i_28_n_1;
  wire tm_reg_0_i_28_n_2;
  wire tm_reg_0_i_28_n_3;
  wire tm_reg_0_i_2_n_0;
  wire tm_reg_0_i_2_n_1;
  wire tm_reg_0_i_2_n_2;
  wire tm_reg_0_i_2_n_3;
  wire tm_reg_0_i_31_n_0;
  wire tm_reg_0_i_32_n_0;
  wire tm_reg_0_i_33_n_0;
  wire tm_reg_0_i_34_n_0;
  wire tm_reg_0_i_35_n_0;
  wire tm_reg_0_i_36_n_0;
  wire tm_reg_0_i_37_n_0;
  wire tm_reg_0_i_3_n_0;
  wire tm_reg_0_i_3_n_1;
  wire tm_reg_0_i_3_n_2;
  wire tm_reg_0_i_3_n_3;
  wire tm_reg_0_i_9_n_0;
  wire \tmp_rand[0]_i_1_n_0 ;
  wire \tmp_rand[1]_i_1_n_0 ;
  wire \tmp_rand[2]_i_1_n_0 ;
  wire \tmp_rand[3]_i_1_n_0 ;
  wire \tmp_rand[4]_i_1_n_0 ;
  wire \tmp_rand[5]_i_1_n_0 ;
  wire \tmp_rand[6]_i_1_n_0 ;
  wire \tmp_rand[6]_i_2_n_0 ;
  wire \tmp_rand[6]_i_3_n_0 ;
  wire \tmp_rand[6]_i_5_n_0 ;
  wire \tmp_rand[6]_i_6_n_0 ;
  wire [6:0]\tmp_rand_reg[6]_0 ;
  wire write_enable_i_1_n_0;
  wire write_enable_i_2_n_0;
  wire [3:2]\NLW_Xmap_reg[6]_i_10_CO_UNCONNECTED ;
  wire [3:3]\NLW_Xmap_reg[6]_i_10_O_UNCONNECTED ;
  wire [0:0]\NLW_Xmap_reg[6]_i_105_O_UNCONNECTED ;
  wire [3:3]\NLW_Xmap_reg[6]_i_128_CO_UNCONNECTED ;
  wire [3:3]\NLW_Xmap_reg[6]_i_129_CO_UNCONNECTED ;
  wire [0:0]\NLW_Xmap_reg[6]_i_137_O_UNCONNECTED ;
  wire [3:1]\NLW_Xmap_reg[6]_i_139_O_UNCONNECTED ;
  wire [3:0]\NLW_Xmap_reg[6]_i_14_O_UNCONNECTED ;
  wire [0:0]\NLW_Xmap_reg[6]_i_199_O_UNCONNECTED ;
  wire [3:0]\NLW_Xmap_reg[6]_i_228_O_UNCONNECTED ;
  wire [3:0]\NLW_Xmap_reg[6]_i_28_O_UNCONNECTED ;
  wire [3:3]\NLW_Xmap_reg[6]_i_3_CO_UNCONNECTED ;
  wire [3:3]\NLW_Xmap_reg[6]_i_40_CO_UNCONNECTED ;
  wire [3:3]\NLW_Xmap_reg[6]_i_41_CO_UNCONNECTED ;
  wire [3:3]\NLW_Xmap_reg[6]_i_42_CO_UNCONNECTED ;
  wire [3:0]\NLW_Xmap_reg[6]_i_43_O_UNCONNECTED ;
  wire [2:0]\NLW_Xmap_reg[6]_i_9_O_UNCONNECTED ;
  wire [3:0]\NLW_Xmap_reg[6]_i_94_O_UNCONNECTED ;
  wire [0:0]\NLW_Ymap_reg[0]_i_104_O_UNCONNECTED ;
  wire [3:1]\NLW_Ymap_reg[0]_i_105_O_UNCONNECTED ;
  wire [3:0]\NLW_Ymap_reg[0]_i_12_O_UNCONNECTED ;
  wire [0:0]\NLW_Ymap_reg[0]_i_136_O_UNCONNECTED ;
  wire [3:0]\NLW_Ymap_reg[0]_i_164_O_UNCONNECTED ;
  wire [2:0]\NLW_Ymap_reg[0]_i_2_O_UNCONNECTED ;
  wire [3:0]\NLW_Ymap_reg[0]_i_24_O_UNCONNECTED ;
  wire [3:0]\NLW_Ymap_reg[0]_i_3_O_UNCONNECTED ;
  wire [3:0]\NLW_Ymap_reg[0]_i_60_O_UNCONNECTED ;
  wire [0:0]\NLW_Ymap_reg[0]_i_70_O_UNCONNECTED ;
  wire [3:0]\NLW_Ymap_reg[5]_i_117_O_UNCONNECTED ;
  wire [3:1]\NLW_Ymap_reg[5]_i_127_CO_UNCONNECTED ;
  wire [3:2]\NLW_Ymap_reg[5]_i_127_O_UNCONNECTED ;
  wire [3:1]\NLW_Ymap_reg[5]_i_145_CO_UNCONNECTED ;
  wire [3:2]\NLW_Ymap_reg[5]_i_145_O_UNCONNECTED ;
  wire [3:1]\NLW_Ymap_reg[5]_i_148_CO_UNCONNECTED ;
  wire [3:0]\NLW_Ymap_reg[5]_i_148_O_UNCONNECTED ;
  wire [3:0]\NLW_Ymap_reg[5]_i_149_CO_UNCONNECTED ;
  wire [3:1]\NLW_Ymap_reg[5]_i_149_O_UNCONNECTED ;
  wire [3:0]\NLW_Ymap_reg[5]_i_150_O_UNCONNECTED ;
  wire [3:3]\NLW_Ymap_reg[5]_i_2_CO_UNCONNECTED ;
  wire [3:0]\NLW_Ymap_reg[5]_i_2_O_UNCONNECTED ;
  wire [3:0]\NLW_Ymap_reg[5]_i_225_O_UNCONNECTED ;
  wire [3:0]\NLW_Ymap_reg[5]_i_26_O_UNCONNECTED ;
  wire [3:1]\NLW_Ymap_reg[5]_i_270_CO_UNCONNECTED ;
  wire [3:0]\NLW_Ymap_reg[5]_i_270_O_UNCONNECTED ;
  wire [3:1]\NLW_Ymap_reg[5]_i_4_CO_UNCONNECTED ;
  wire [3:2]\NLW_Ymap_reg[5]_i_4_O_UNCONNECTED ;
  wire [3:3]\NLW_Ymap_reg[5]_i_42_CO_UNCONNECTED ;
  wire [3:0]\NLW_Ymap_reg[5]_i_47_O_UNCONNECTED ;
  wire [3:1]\NLW_Ymap_reg[5]_i_5_CO_UNCONNECTED ;
  wire [3:2]\NLW_Ymap_reg[5]_i_5_O_UNCONNECTED ;
  wire [3:0]\NLW_Ymap_reg[5]_i_6_O_UNCONNECTED ;
  wire [3:1]\NLW_cnt_reg[30]_i_3_CO_UNCONNECTED ;
  wire [3:2]\NLW_cnt_reg[30]_i_3_O_UNCONNECTED ;
  wire [3:0]NLW_tm_reg_0_i_1_CO_UNCONNECTED;
  wire [3:1]NLW_tm_reg_0_i_1_O_UNCONNECTED;
  wire [2:2]NLW_tm_reg_0_i_27_CO_UNCONNECTED;
  wire [3:3]NLW_tm_reg_0_i_27_O_UNCONNECTED;
  wire [0:0]NLW_tm_reg_0_i_3_O_UNCONNECTED;

  LUT6 #(
    .INIT(64'hCFAA0F0FCFAAAAAA)) 
    \FSM_sequential_state[0]_i_1 
       (.I0(state__0[0]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .I3(\FSM_sequential_state_reg[2]_i_2_n_0 ),
        .I4(state__0[2]),
        .I5(\FSM_sequential_state[2]_i_3_n_0 ),
        .O(\FSM_sequential_state[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h6F666FFF60666000)) 
    \FSM_sequential_state[1]_i_1 
       (.I0(state__0[0]),
        .I1(state__0[1]),
        .I2(\FSM_sequential_state_reg[2]_i_2_n_0 ),
        .I3(state__0[2]),
        .I4(\FSM_sequential_state[2]_i_3_n_0 ),
        .I5(state__0[1]),
        .O(\FSM_sequential_state[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFF88FFFFF088F000)) 
    \FSM_sequential_state[2]_i_1 
       (.I0(state__0[1]),
        .I1(state__0[0]),
        .I2(\FSM_sequential_state_reg[2]_i_2_n_0 ),
        .I3(state__0[2]),
        .I4(\FSM_sequential_state[2]_i_3_n_0 ),
        .I5(state__0[2]),
        .O(\FSM_sequential_state[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFE000000)) 
    \FSM_sequential_state[2]_i_10 
       (.I0(\cnt_reg_n_0_[4] ),
        .I1(\cnt_reg_n_0_[2] ),
        .I2(\cnt_reg_n_0_[3] ),
        .I3(\cnt_reg_n_0_[5] ),
        .I4(\cnt_reg_n_0_[6] ),
        .I5(\cnt_reg_n_0_[12] ),
        .O(\FSM_sequential_state[2]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'hEFFEFEFF00000000)) 
    \FSM_sequential_state[2]_i_11 
       (.I0(\FSM_sequential_state[2]_i_12_n_0 ),
        .I1(\FSM_sequential_state[2]_i_13_n_0 ),
        .I2(\rand_reg_n_0_[6] ),
        .I3(\tmp_rand_reg[6]_0 [6]),
        .I4(\rand[6]_i_3_n_0 ),
        .I5(\FSM_sequential_state[2]_i_14_n_0 ),
        .O(state05_out));
  LUT6 #(
    .INIT(64'hBFFDEFF7F7BFFDEF)) 
    \FSM_sequential_state[2]_i_12 
       (.I0(\rand_reg_n_0_[0] ),
        .I1(\tmp_rand_reg[6]_0 [2]),
        .I2(\tmp_rand_reg[6]_0 [0]),
        .I3(\tmp_rand_reg[6]_0 [1]),
        .I4(\rand_reg_n_0_[2] ),
        .I5(\rand_reg_n_0_[1] ),
        .O(\FSM_sequential_state[2]_i_12_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFF99FF99FFFFF)) 
    \FSM_sequential_state[2]_i_13 
       (.I0(\rand_reg_n_0_[3] ),
        .I1(\FSM_sequential_state[2]_i_15_n_0 ),
        .I2(\FSM_sequential_state[2]_i_16_n_0 ),
        .I3(\rand_reg_n_0_[5] ),
        .I4(\FSM_sequential_state[2]_i_17_n_0 ),
        .I5(\rand_reg_n_0_[4] ),
        .O(\FSM_sequential_state[2]_i_13_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \FSM_sequential_state[2]_i_14 
       (.I0(\rand_reg_n_0_[6] ),
        .I1(\rand_reg_n_0_[4] ),
        .I2(\rand_reg_n_0_[5] ),
        .I3(\FSM_sequential_state[2]_i_18_n_0 ),
        .O(\FSM_sequential_state[2]_i_14_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'h01FE)) 
    \FSM_sequential_state[2]_i_15 
       (.I0(\tmp_rand_reg[6]_0 [2]),
        .I1(\tmp_rand_reg[6]_0 [0]),
        .I2(\tmp_rand_reg[6]_0 [1]),
        .I3(\tmp_rand_reg[6]_0 [3]),
        .O(\FSM_sequential_state[2]_i_15_n_0 ));
  LUT6 #(
    .INIT(64'h00000001FFFFFFFE)) 
    \FSM_sequential_state[2]_i_16 
       (.I0(\tmp_rand_reg[6]_0 [4]),
        .I1(\tmp_rand_reg[6]_0 [2]),
        .I2(\tmp_rand_reg[6]_0 [0]),
        .I3(\tmp_rand_reg[6]_0 [1]),
        .I4(\tmp_rand_reg[6]_0 [3]),
        .I5(\tmp_rand_reg[6]_0 [5]),
        .O(\FSM_sequential_state[2]_i_16_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h0001FFFE)) 
    \FSM_sequential_state[2]_i_17 
       (.I0(\tmp_rand_reg[6]_0 [3]),
        .I1(\tmp_rand_reg[6]_0 [1]),
        .I2(\tmp_rand_reg[6]_0 [0]),
        .I3(\tmp_rand_reg[6]_0 [2]),
        .I4(\tmp_rand_reg[6]_0 [4]),
        .O(\FSM_sequential_state[2]_i_17_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \FSM_sequential_state[2]_i_18 
       (.I0(\rand_reg_n_0_[2] ),
        .I1(\rand_reg_n_0_[3] ),
        .I2(\rand_reg_n_0_[0] ),
        .I3(\rand_reg_n_0_[1] ),
        .O(\FSM_sequential_state[2]_i_18_n_0 ));
  LUT4 #(
    .INIT(16'hBFB0)) 
    \FSM_sequential_state[2]_i_3 
       (.I0(led1_i_2_n_0),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(\FSM_sequential_state[2]_i_6_n_0 ),
        .O(\FSM_sequential_state[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \FSM_sequential_state[2]_i_4 
       (.I0(state__0[0]),
        .I1(\FSM_sequential_state[2]_i_7_n_0 ),
        .I2(\FSM_sequential_state[2]_i_8_n_0 ),
        .I3(\FSM_sequential_state[2]_i_9_n_0 ),
        .I4(led1_i_2_n_0),
        .I5(\FSM_sequential_state[2]_i_10_n_0 ),
        .O(\FSM_sequential_state[2]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hBBBBBBBBBBB8B8B8)) 
    \FSM_sequential_state[2]_i_5 
       (.I0(state05_out),
        .I1(state__0[0]),
        .I2(\FSM_sequential_state[2]_i_9_n_0 ),
        .I3(led3_i_3_n_0),
        .I4(\cnt_reg_n_0_[12] ),
        .I5(led1_i_2_n_0),
        .O(\FSM_sequential_state[2]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \FSM_sequential_state[2]_i_6 
       (.I0(state),
        .I1(state__0[0]),
        .I2(sw[1]),
        .I3(sw[2]),
        .I4(sw[0]),
        .O(\FSM_sequential_state[2]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \FSM_sequential_state[2]_i_7 
       (.I0(\cnt_reg_n_0_[7] ),
        .I1(\cnt_reg_n_0_[8] ),
        .O(\FSM_sequential_state[2]_i_7_n_0 ));
  LUT3 #(
    .INIT(8'hFE)) 
    \FSM_sequential_state[2]_i_8 
       (.I0(\cnt_reg_n_0_[9] ),
        .I1(\cnt_reg_n_0_[11] ),
        .I2(\cnt_reg_n_0_[10] ),
        .O(\FSM_sequential_state[2]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \FSM_sequential_state[2]_i_9 
       (.I0(\cnt_reg_n_0_[13] ),
        .I1(\cnt_reg_n_0_[14] ),
        .O(\FSM_sequential_state[2]_i_9_n_0 ));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_state_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_sequential_state[0]_i_1_n_0 ),
        .Q(state__0[0]),
        .R(1'b0));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_state_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_sequential_state[1]_i_1_n_0 ),
        .Q(state__0[1]),
        .R(1'b0));
  (* KEEP = "yes" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_sequential_state_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_sequential_state[2]_i_1_n_0 ),
        .Q(state__0[2]),
        .R(1'b0));
  MUXF7 \FSM_sequential_state_reg[2]_i_2 
       (.I0(\FSM_sequential_state[2]_i_4_n_0 ),
        .I1(\FSM_sequential_state[2]_i_5_n_0 ),
        .O(\FSM_sequential_state_reg[2]_i_2_n_0 ),
        .S(state__0[1]));
  LUT1 #(
    .INIT(2'h1)) 
    \Xmap[3]_i_2 
       (.I0(\cnt_reg_n_0_[3] ),
        .O(\Xmap[3]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \Xmap[3]_i_3 
       (.I0(\cnt_reg_n_0_[2] ),
        .O(\Xmap[3]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \Xmap[3]_i_4 
       (.I0(\cnt_reg_n_0_[1] ),
        .O(\Xmap[3]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \Xmap[3]_i_5 
       (.I0(\cnt_reg_n_0_[0] ),
        .O(\Xmap[3]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h323C)) 
    \Xmap[4]_i_1 
       (.I0(\Xmap_reg[6]_i_3_n_6 ),
        .I1(\Xmap_reg[6]_i_3_n_7 ),
        .I2(\Xmap_reg[6]_i_3_n_4 ),
        .I3(\Xmap_reg[6]_i_3_n_5 ),
        .O(\Xmap[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'hA4B4)) 
    \Xmap[5]_i_1 
       (.I0(\Xmap_reg[6]_i_3_n_7 ),
        .I1(\Xmap_reg[6]_i_3_n_4 ),
        .I2(\Xmap_reg[6]_i_3_n_6 ),
        .I3(\Xmap_reg[6]_i_3_n_5 ),
        .O(\Xmap[5]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h2000)) 
    \Xmap[6]_i_1 
       (.I0(state__0[2]),
        .I1(state__0[0]),
        .I2(state16_out),
        .I3(state__0[1]),
        .O(\Xmap[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Xmap[6]_i_107 
       (.I0(\cnt_reg_n_0_[10] ),
        .I1(\cnt_reg_n_0_[8] ),
        .I2(\cnt_reg_n_0_[13] ),
        .I3(\cnt_reg_n_0_[14] ),
        .I4(\cnt_reg_n_0_[9] ),
        .I5(\cnt_reg_n_0_[11] ),
        .O(\Xmap[6]_i_107_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Xmap[6]_i_108 
       (.I0(\cnt_reg_n_0_[9] ),
        .I1(\cnt_reg_n_0_[7] ),
        .I2(\cnt_reg_n_0_[12] ),
        .I3(\cnt_reg_n_0_[13] ),
        .I4(\cnt_reg_n_0_[10] ),
        .I5(\cnt_reg_n_0_[8] ),
        .O(\Xmap[6]_i_108_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Xmap[6]_i_109 
       (.I0(\cnt_reg_n_0_[8] ),
        .I1(\cnt_reg_n_0_[6] ),
        .I2(\cnt_reg_n_0_[11] ),
        .I3(\cnt_reg_n_0_[9] ),
        .I4(\cnt_reg_n_0_[7] ),
        .I5(\cnt_reg_n_0_[12] ),
        .O(\Xmap[6]_i_109_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Xmap[6]_i_110 
       (.I0(\cnt_reg_n_0_[7] ),
        .I1(\cnt_reg_n_0_[5] ),
        .I2(\cnt_reg_n_0_[10] ),
        .I3(\cnt_reg_n_0_[6] ),
        .I4(\cnt_reg_n_0_[8] ),
        .I5(\cnt_reg_n_0_[11] ),
        .O(\Xmap[6]_i_110_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Xmap[6]_i_111 
       (.I0(\cnt_reg_n_0_[1] ),
        .I1(\Xmap_reg[6]_i_127_n_5 ),
        .I2(\Xmap_reg[6]_i_113_n_5 ),
        .O(\Xmap[6]_i_111_n_0 ));
  LUT3 #(
    .INIT(8'h96)) 
    \Xmap[6]_i_112 
       (.I0(\cnt_reg_n_0_[1] ),
        .I1(\Xmap_reg[6]_i_127_n_5 ),
        .I2(\Xmap_reg[6]_i_113_n_5 ),
        .O(\Xmap[6]_i_112_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Xmap[6]_i_114 
       (.I0(\Xmap_reg[6]_i_113_n_5 ),
        .I1(\Xmap_reg[6]_i_127_n_5 ),
        .I2(\cnt_reg_n_0_[1] ),
        .I3(\Xmap_reg[6]_i_127_n_4 ),
        .I4(\cnt_reg_n_0_[2] ),
        .I5(\Xmap_reg[6]_i_113_n_4 ),
        .O(\Xmap[6]_i_114_n_0 ));
  LUT5 #(
    .INIT(32'h69969696)) 
    \Xmap[6]_i_115 
       (.I0(\Xmap_reg[6]_i_113_n_5 ),
        .I1(\Xmap_reg[6]_i_127_n_5 ),
        .I2(\cnt_reg_n_0_[1] ),
        .I3(\Xmap_reg[6]_i_113_n_6 ),
        .I4(\Xmap_reg[6]_i_127_n_6 ),
        .O(\Xmap[6]_i_115_n_0 ));
  LUT3 #(
    .INIT(8'h96)) 
    \Xmap[6]_i_116 
       (.I0(\Xmap_reg[6]_i_127_n_6 ),
        .I1(\Xmap_reg[6]_i_113_n_6 ),
        .I2(\cnt_reg_n_0_[0] ),
        .O(\Xmap[6]_i_116_n_0 ));
  LUT3 #(
    .INIT(8'hB2)) 
    \Xmap[6]_i_118 
       (.I0(\cnt_reg_n_0_[2] ),
        .I1(\cnt_reg_n_0_[6] ),
        .I2(\cnt_reg_n_0_[4] ),
        .O(\Xmap[6]_i_118_n_0 ));
  LUT3 #(
    .INIT(8'hB2)) 
    \Xmap[6]_i_119 
       (.I0(\cnt_reg_n_0_[1] ),
        .I1(\cnt_reg_n_0_[5] ),
        .I2(\cnt_reg_n_0_[3] ),
        .O(\Xmap[6]_i_119_n_0 ));
  LUT3 #(
    .INIT(8'h69)) 
    \Xmap[6]_i_120 
       (.I0(\cnt_reg_n_0_[1] ),
        .I1(\cnt_reg_n_0_[3] ),
        .I2(\cnt_reg_n_0_[5] ),
        .O(\Xmap[6]_i_120_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_121 
       (.I0(\cnt_reg_n_0_[4] ),
        .I1(\cnt_reg_n_0_[6] ),
        .I2(\cnt_reg_n_0_[2] ),
        .I3(\cnt_reg_n_0_[5] ),
        .I4(\cnt_reg_n_0_[3] ),
        .I5(\cnt_reg_n_0_[7] ),
        .O(\Xmap[6]_i_121_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_122 
       (.I0(\cnt_reg_n_0_[3] ),
        .I1(\cnt_reg_n_0_[5] ),
        .I2(\cnt_reg_n_0_[1] ),
        .I3(\cnt_reg_n_0_[4] ),
        .I4(\cnt_reg_n_0_[6] ),
        .I5(\cnt_reg_n_0_[2] ),
        .O(\Xmap[6]_i_122_n_0 ));
  LUT5 #(
    .INIT(32'h69966969)) 
    \Xmap[6]_i_123 
       (.I0(\cnt_reg_n_0_[5] ),
        .I1(\cnt_reg_n_0_[3] ),
        .I2(\cnt_reg_n_0_[1] ),
        .I3(\cnt_reg_n_0_[4] ),
        .I4(\cnt_reg_n_0_[0] ),
        .O(\Xmap[6]_i_123_n_0 ));
  LUT3 #(
    .INIT(8'h69)) 
    \Xmap[6]_i_124 
       (.I0(\cnt_reg_n_0_[4] ),
        .I1(\cnt_reg_n_0_[0] ),
        .I2(\cnt_reg_n_0_[2] ),
        .O(\Xmap[6]_i_124_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \Xmap[6]_i_130 
       (.I0(\Xmap_reg[6]_i_199_n_4 ),
        .I1(\cnt_reg_n_0_[2] ),
        .O(\Xmap[6]_i_130_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \Xmap[6]_i_131 
       (.I0(\Xmap_reg[6]_i_199_n_5 ),
        .I1(\cnt_reg_n_0_[1] ),
        .O(\Xmap[6]_i_131_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \Xmap[6]_i_132 
       (.I0(\Xmap_reg[6]_i_199_n_6 ),
        .I1(\cnt_reg_n_0_[0] ),
        .O(\Xmap[6]_i_132_n_0 ));
  LUT4 #(
    .INIT(16'h8778)) 
    \Xmap[6]_i_133 
       (.I0(\cnt_reg_n_0_[2] ),
        .I1(\Xmap_reg[6]_i_199_n_4 ),
        .I2(\Xmap_reg[4]_4 [0]),
        .I3(\Xmap_reg[4]_5 ),
        .O(\Xmap[6]_i_133_n_0 ));
  LUT4 #(
    .INIT(16'h8778)) 
    \Xmap[6]_i_134 
       (.I0(\cnt_reg_n_0_[1] ),
        .I1(\Xmap_reg[6]_i_199_n_5 ),
        .I2(\Xmap_reg[6]_i_199_n_4 ),
        .I3(\cnt_reg_n_0_[2] ),
        .O(\Xmap[6]_i_134_n_0 ));
  LUT4 #(
    .INIT(16'h8778)) 
    \Xmap[6]_i_135 
       (.I0(\cnt_reg_n_0_[0] ),
        .I1(\Xmap_reg[6]_i_199_n_6 ),
        .I2(\Xmap_reg[6]_i_199_n_5 ),
        .I3(\cnt_reg_n_0_[1] ),
        .O(\Xmap[6]_i_135_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Xmap[6]_i_136 
       (.I0(\cnt_reg_n_0_[0] ),
        .I1(\Xmap_reg[6]_i_199_n_6 ),
        .O(\Xmap[6]_i_136_n_0 ));
  LUT3 #(
    .INIT(8'hB2)) 
    \Xmap[6]_i_140 
       (.I0(\cnt_reg_n_0_[2] ),
        .I1(\cnt_reg_n_0_[4] ),
        .I2(\cnt_reg_n_0_[7] ),
        .O(\Xmap[6]_i_140_n_0 ));
  LUT3 #(
    .INIT(8'hB2)) 
    \Xmap[6]_i_141 
       (.I0(\cnt_reg_n_0_[1] ),
        .I1(\cnt_reg_n_0_[3] ),
        .I2(\cnt_reg_n_0_[6] ),
        .O(\Xmap[6]_i_141_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Xmap[6]_i_142 
       (.I0(\cnt_reg_n_0_[6] ),
        .I1(\cnt_reg_n_0_[4] ),
        .I2(\cnt_reg_n_0_[9] ),
        .I3(\cnt_reg_n_0_[10] ),
        .I4(\cnt_reg_n_0_[5] ),
        .I5(\cnt_reg_n_0_[7] ),
        .O(\Xmap[6]_i_142_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Xmap[6]_i_143 
       (.I0(\cnt_reg_n_0_[5] ),
        .I1(\cnt_reg_n_0_[3] ),
        .I2(\cnt_reg_n_0_[8] ),
        .I3(\cnt_reg_n_0_[4] ),
        .I4(\cnt_reg_n_0_[6] ),
        .I5(\cnt_reg_n_0_[9] ),
        .O(\Xmap[6]_i_143_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_144 
       (.I0(\cnt_reg_n_0_[7] ),
        .I1(\cnt_reg_n_0_[4] ),
        .I2(\cnt_reg_n_0_[2] ),
        .I3(\cnt_reg_n_0_[5] ),
        .I4(\cnt_reg_n_0_[3] ),
        .I5(\cnt_reg_n_0_[8] ),
        .O(\Xmap[6]_i_144_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_145 
       (.I0(\cnt_reg_n_0_[6] ),
        .I1(\cnt_reg_n_0_[3] ),
        .I2(\cnt_reg_n_0_[1] ),
        .I3(\cnt_reg_n_0_[7] ),
        .I4(\cnt_reg_n_0_[2] ),
        .I5(\cnt_reg_n_0_[4] ),
        .O(\Xmap[6]_i_145_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \Xmap[6]_i_151 
       (.I0(\cnt_reg_n_0_[1] ),
        .I1(\cnt_reg_n_0_[3] ),
        .O(\Xmap[6]_i_151_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \Xmap[6]_i_152 
       (.I0(\cnt_reg_n_0_[0] ),
        .I1(\cnt_reg_n_0_[2] ),
        .O(\Xmap[6]_i_152_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \Xmap[6]_i_153 
       (.I0(\cnt_reg_n_0_[1] ),
        .O(\Xmap[6]_i_153_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \Xmap[6]_i_154 
       (.I0(\cnt_reg_n_0_[0] ),
        .O(\Xmap[6]_i_154_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \Xmap[6]_i_155 
       (.I0(\cnt_reg_n_0_[1] ),
        .I1(\cnt_reg_n_0_[3] ),
        .O(\Xmap[6]_i_155_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \Xmap[6]_i_156 
       (.I0(\cnt_reg_n_0_[0] ),
        .I1(\cnt_reg_n_0_[2] ),
        .O(\Xmap[6]_i_156_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \Xmap[6]_i_157 
       (.I0(\cnt_reg_n_0_[1] ),
        .O(\Xmap[6]_i_157_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \Xmap[6]_i_158 
       (.I0(\cnt_reg_n_0_[0] ),
        .O(\Xmap[6]_i_158_n_0 ));
  LUT3 #(
    .INIT(8'hB2)) 
    \Xmap[6]_i_159 
       (.I0(\cnt_reg_n_0_[20] ),
        .I1(\cnt_reg_n_0_[22] ),
        .I2(\cnt_reg_n_0_[25] ),
        .O(\Xmap[6]_i_159_n_0 ));
  LUT3 #(
    .INIT(8'hB2)) 
    \Xmap[6]_i_160 
       (.I0(\cnt_reg_n_0_[17] ),
        .I1(\cnt_reg_n_0_[19] ),
        .I2(\cnt_reg_n_0_[22] ),
        .O(\Xmap[6]_i_160_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_161 
       (.I0(\cnt_reg_n_0_[25] ),
        .I1(\cnt_reg_n_0_[22] ),
        .I2(\cnt_reg_n_0_[20] ),
        .I3(\cnt_reg_n_0_[21] ),
        .I4(\cnt_reg_n_0_[23] ),
        .I5(\cnt_reg_n_0_[26] ),
        .O(\Xmap[6]_i_161_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Xmap[6]_i_162 
       (.I0(\cnt_reg_n_0_[21] ),
        .I1(\cnt_reg_n_0_[19] ),
        .I2(\cnt_reg_n_0_[24] ),
        .I3(\cnt_reg_n_0_[20] ),
        .I4(\cnt_reg_n_0_[22] ),
        .I5(\cnt_reg_n_0_[25] ),
        .O(\Xmap[6]_i_162_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Xmap[6]_i_163 
       (.I0(\cnt_reg_n_0_[20] ),
        .I1(\cnt_reg_n_0_[18] ),
        .I2(\cnt_reg_n_0_[23] ),
        .I3(\cnt_reg_n_0_[19] ),
        .I4(\cnt_reg_n_0_[21] ),
        .I5(\cnt_reg_n_0_[24] ),
        .O(\Xmap[6]_i_163_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_164 
       (.I0(\cnt_reg_n_0_[22] ),
        .I1(\cnt_reg_n_0_[19] ),
        .I2(\cnt_reg_n_0_[17] ),
        .I3(\cnt_reg_n_0_[18] ),
        .I4(\cnt_reg_n_0_[20] ),
        .I5(\cnt_reg_n_0_[23] ),
        .O(\Xmap[6]_i_164_n_0 ));
  LUT3 #(
    .INIT(8'hB2)) 
    \Xmap[6]_i_165 
       (.I0(\cnt_reg_n_0_[24] ),
        .I1(\cnt_reg_n_0_[26] ),
        .I2(\cnt_reg_n_0_[29] ),
        .O(\Xmap[6]_i_165_n_0 ));
  LUT3 #(
    .INIT(8'hB2)) 
    \Xmap[6]_i_166 
       (.I0(\cnt_reg_n_0_[21] ),
        .I1(\cnt_reg_n_0_[23] ),
        .I2(\cnt_reg_n_0_[26] ),
        .O(\Xmap[6]_i_166_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_167 
       (.I0(\cnt_reg_n_0_[29] ),
        .I1(\cnt_reg_n_0_[26] ),
        .I2(\cnt_reg_n_0_[24] ),
        .I3(\cnt_reg_n_0_[25] ),
        .I4(\cnt_reg_n_0_[27] ),
        .I5(\cnt_reg_n_0_[30] ),
        .O(\Xmap[6]_i_167_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Xmap[6]_i_168 
       (.I0(\cnt_reg_n_0_[25] ),
        .I1(\cnt_reg_n_0_[23] ),
        .I2(\cnt_reg_n_0_[28] ),
        .I3(\cnt_reg_n_0_[24] ),
        .I4(\cnt_reg_n_0_[26] ),
        .I5(\cnt_reg_n_0_[29] ),
        .O(\Xmap[6]_i_168_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Xmap[6]_i_169 
       (.I0(\cnt_reg_n_0_[24] ),
        .I1(\cnt_reg_n_0_[22] ),
        .I2(\cnt_reg_n_0_[27] ),
        .I3(\cnt_reg_n_0_[23] ),
        .I4(\cnt_reg_n_0_[25] ),
        .I5(\cnt_reg_n_0_[28] ),
        .O(\Xmap[6]_i_169_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_170 
       (.I0(\cnt_reg_n_0_[26] ),
        .I1(\cnt_reg_n_0_[23] ),
        .I2(\cnt_reg_n_0_[21] ),
        .I3(\cnt_reg_n_0_[24] ),
        .I4(\cnt_reg_n_0_[22] ),
        .I5(\cnt_reg_n_0_[27] ),
        .O(\Xmap[6]_i_170_n_0 ));
  LUT3 #(
    .INIT(8'hB2)) 
    \Xmap[6]_i_171 
       (.I0(\cnt_reg_n_0_[17] ),
        .I1(\cnt_reg_n_0_[21] ),
        .I2(\cnt_reg_n_0_[19] ),
        .O(\Xmap[6]_i_171_n_0 ));
  LUT3 #(
    .INIT(8'hB2)) 
    \Xmap[6]_i_172 
       (.I0(\cnt_reg_n_0_[16] ),
        .I1(\cnt_reg_n_0_[20] ),
        .I2(\cnt_reg_n_0_[18] ),
        .O(\Xmap[6]_i_172_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Xmap[6]_i_173 
       (.I0(\cnt_reg_n_0_[20] ),
        .I1(\cnt_reg_n_0_[18] ),
        .I2(\cnt_reg_n_0_[22] ),
        .I3(\cnt_reg_n_0_[19] ),
        .I4(\cnt_reg_n_0_[21] ),
        .I5(\cnt_reg_n_0_[23] ),
        .O(\Xmap[6]_i_173_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_174 
       (.I0(\cnt_reg_n_0_[19] ),
        .I1(\cnt_reg_n_0_[21] ),
        .I2(\cnt_reg_n_0_[17] ),
        .I3(\cnt_reg_n_0_[18] ),
        .I4(\cnt_reg_n_0_[20] ),
        .I5(\cnt_reg_n_0_[22] ),
        .O(\Xmap[6]_i_174_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_175 
       (.I0(\cnt_reg_n_0_[18] ),
        .I1(\cnt_reg_n_0_[20] ),
        .I2(\cnt_reg_n_0_[16] ),
        .I3(\cnt_reg_n_0_[19] ),
        .I4(\cnt_reg_n_0_[21] ),
        .I5(\cnt_reg_n_0_[17] ),
        .O(\Xmap[6]_i_175_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Xmap[6]_i_176 
       (.I0(\cnt_reg_n_0_[17] ),
        .I1(\cnt_reg_n_0_[15] ),
        .I2(\cnt_reg_n_0_[19] ),
        .I3(\cnt_reg_n_0_[20] ),
        .I4(\cnt_reg_n_0_[18] ),
        .I5(\cnt_reg_n_0_[16] ),
        .O(\Xmap[6]_i_176_n_0 ));
  LUT3 #(
    .INIT(8'hB2)) 
    \Xmap[6]_i_178 
       (.I0(\cnt_reg_n_0_[14] ),
        .I1(\cnt_reg_n_0_[18] ),
        .I2(\cnt_reg_n_0_[16] ),
        .O(\Xmap[6]_i_178_n_0 ));
  LUT3 #(
    .INIT(8'hB2)) 
    \Xmap[6]_i_179 
       (.I0(\cnt_reg_n_0_[13] ),
        .I1(\cnt_reg_n_0_[17] ),
        .I2(\cnt_reg_n_0_[15] ),
        .O(\Xmap[6]_i_179_n_0 ));
  LUT3 #(
    .INIT(8'hB2)) 
    \Xmap[6]_i_180 
       (.I0(\cnt_reg_n_0_[12] ),
        .I1(\cnt_reg_n_0_[16] ),
        .I2(\cnt_reg_n_0_[14] ),
        .O(\Xmap[6]_i_180_n_0 ));
  LUT3 #(
    .INIT(8'hB2)) 
    \Xmap[6]_i_181 
       (.I0(\cnt_reg_n_0_[11] ),
        .I1(\cnt_reg_n_0_[15] ),
        .I2(\cnt_reg_n_0_[13] ),
        .O(\Xmap[6]_i_181_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_182 
       (.I0(\cnt_reg_n_0_[16] ),
        .I1(\cnt_reg_n_0_[18] ),
        .I2(\cnt_reg_n_0_[14] ),
        .I3(\cnt_reg_n_0_[15] ),
        .I4(\cnt_reg_n_0_[17] ),
        .I5(\cnt_reg_n_0_[19] ),
        .O(\Xmap[6]_i_182_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_183 
       (.I0(\cnt_reg_n_0_[15] ),
        .I1(\cnt_reg_n_0_[17] ),
        .I2(\cnt_reg_n_0_[13] ),
        .I3(\cnt_reg_n_0_[14] ),
        .I4(\cnt_reg_n_0_[16] ),
        .I5(\cnt_reg_n_0_[18] ),
        .O(\Xmap[6]_i_183_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_184 
       (.I0(\cnt_reg_n_0_[14] ),
        .I1(\cnt_reg_n_0_[16] ),
        .I2(\cnt_reg_n_0_[12] ),
        .I3(\cnt_reg_n_0_[15] ),
        .I4(\cnt_reg_n_0_[17] ),
        .I5(\cnt_reg_n_0_[13] ),
        .O(\Xmap[6]_i_184_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_185 
       (.I0(\cnt_reg_n_0_[13] ),
        .I1(\cnt_reg_n_0_[15] ),
        .I2(\cnt_reg_n_0_[11] ),
        .I3(\cnt_reg_n_0_[14] ),
        .I4(\cnt_reg_n_0_[16] ),
        .I5(\cnt_reg_n_0_[12] ),
        .O(\Xmap[6]_i_185_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Xmap[6]_i_186 
       (.I0(\cnt_reg_n_0_[27] ),
        .I1(\cnt_reg_n_0_[29] ),
        .O(\Xmap[6]_i_186_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Xmap[6]_i_187 
       (.I0(\cnt_reg_n_0_[26] ),
        .I1(\cnt_reg_n_0_[28] ),
        .O(\Xmap[6]_i_187_n_0 ));
  LUT3 #(
    .INIT(8'hB2)) 
    \Xmap[6]_i_188 
       (.I0(\cnt_reg_n_0_[25] ),
        .I1(\cnt_reg_n_0_[27] ),
        .I2(\cnt_reg_n_0_[30] ),
        .O(\Xmap[6]_i_188_n_0 ));
  LUT3 #(
    .INIT(8'h2D)) 
    \Xmap[6]_i_189 
       (.I0(\cnt_reg_n_0_[28] ),
        .I1(\cnt_reg_n_0_[30] ),
        .I2(\cnt_reg_n_0_[29] ),
        .O(\Xmap[6]_i_189_n_0 ));
  LUT4 #(
    .INIT(16'hB44B)) 
    \Xmap[6]_i_190 
       (.I0(\cnt_reg_n_0_[29] ),
        .I1(\cnt_reg_n_0_[27] ),
        .I2(\cnt_reg_n_0_[28] ),
        .I3(\cnt_reg_n_0_[30] ),
        .O(\Xmap[6]_i_190_n_0 ));
  LUT4 #(
    .INIT(16'hB44B)) 
    \Xmap[6]_i_191 
       (.I0(\cnt_reg_n_0_[28] ),
        .I1(\cnt_reg_n_0_[26] ),
        .I2(\cnt_reg_n_0_[29] ),
        .I3(\cnt_reg_n_0_[27] ),
        .O(\Xmap[6]_i_191_n_0 ));
  LUT5 #(
    .INIT(32'h4DB2B24D)) 
    \Xmap[6]_i_192 
       (.I0(\cnt_reg_n_0_[30] ),
        .I1(\cnt_reg_n_0_[27] ),
        .I2(\cnt_reg_n_0_[25] ),
        .I3(\cnt_reg_n_0_[28] ),
        .I4(\cnt_reg_n_0_[26] ),
        .O(\Xmap[6]_i_192_n_0 ));
  LUT3 #(
    .INIT(8'hB2)) 
    \Xmap[6]_i_193 
       (.I0(\cnt_reg_n_0_[21] ),
        .I1(\cnt_reg_n_0_[25] ),
        .I2(\cnt_reg_n_0_[23] ),
        .O(\Xmap[6]_i_193_n_0 ));
  LUT3 #(
    .INIT(8'hB2)) 
    \Xmap[6]_i_194 
       (.I0(\cnt_reg_n_0_[20] ),
        .I1(\cnt_reg_n_0_[24] ),
        .I2(\cnt_reg_n_0_[22] ),
        .O(\Xmap[6]_i_194_n_0 ));
  LUT6 #(
    .INIT(64'h9669696996969669)) 
    \Xmap[6]_i_195 
       (.I0(\cnt_reg_n_0_[27] ),
        .I1(\cnt_reg_n_0_[25] ),
        .I2(\cnt_reg_n_0_[23] ),
        .I3(\cnt_reg_n_0_[24] ),
        .I4(\cnt_reg_n_0_[22] ),
        .I5(\cnt_reg_n_0_[26] ),
        .O(\Xmap[6]_i_195_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_196 
       (.I0(\cnt_reg_n_0_[23] ),
        .I1(\cnt_reg_n_0_[25] ),
        .I2(\cnt_reg_n_0_[21] ),
        .I3(\cnt_reg_n_0_[22] ),
        .I4(\cnt_reg_n_0_[24] ),
        .I5(\cnt_reg_n_0_[26] ),
        .O(\Xmap[6]_i_196_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_197 
       (.I0(\cnt_reg_n_0_[22] ),
        .I1(\cnt_reg_n_0_[24] ),
        .I2(\cnt_reg_n_0_[20] ),
        .I3(\cnt_reg_n_0_[23] ),
        .I4(\cnt_reg_n_0_[25] ),
        .I5(\cnt_reg_n_0_[21] ),
        .O(\Xmap[6]_i_197_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Xmap[6]_i_198 
       (.I0(\cnt_reg_n_0_[21] ),
        .I1(\cnt_reg_n_0_[19] ),
        .I2(\cnt_reg_n_0_[23] ),
        .I3(\cnt_reg_n_0_[22] ),
        .I4(\cnt_reg_n_0_[24] ),
        .I5(\cnt_reg_n_0_[20] ),
        .O(\Xmap[6]_i_198_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'h03A8)) 
    \Xmap[6]_i_2 
       (.I0(\Xmap_reg[6]_i_3_n_4 ),
        .I1(\Xmap_reg[6]_i_3_n_7 ),
        .I2(\Xmap_reg[6]_i_3_n_6 ),
        .I3(\Xmap_reg[6]_i_3_n_5 ),
        .O(\Xmap[6]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'hB2)) 
    \Xmap[6]_i_200 
       (.I0(\cnt_reg_n_0_[0] ),
        .I1(\cnt_reg_n_0_[2] ),
        .I2(\cnt_reg_n_0_[5] ),
        .O(\Xmap[6]_i_200_n_0 ));
  LUT3 #(
    .INIT(8'h69)) 
    \Xmap[6]_i_201 
       (.I0(\cnt_reg_n_0_[5] ),
        .I1(\cnt_reg_n_0_[2] ),
        .I2(\cnt_reg_n_0_[0] ),
        .O(\Xmap[6]_i_201_n_0 ));
  LUT2 #(
    .INIT(4'hB)) 
    \Xmap[6]_i_202 
       (.I0(\cnt_reg_n_0_[3] ),
        .I1(\cnt_reg_n_0_[0] ),
        .O(\Xmap[6]_i_202_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_203 
       (.I0(\cnt_reg_n_0_[5] ),
        .I1(\cnt_reg_n_0_[2] ),
        .I2(\cnt_reg_n_0_[0] ),
        .I3(\cnt_reg_n_0_[1] ),
        .I4(\cnt_reg_n_0_[3] ),
        .I5(\cnt_reg_n_0_[6] ),
        .O(\Xmap[6]_i_203_n_0 ));
  LUT5 #(
    .INIT(32'h69966969)) 
    \Xmap[6]_i_204 
       (.I0(\cnt_reg_n_0_[0] ),
        .I1(\cnt_reg_n_0_[2] ),
        .I2(\cnt_reg_n_0_[5] ),
        .I3(\cnt_reg_n_0_[1] ),
        .I4(\cnt_reg_n_0_[4] ),
        .O(\Xmap[6]_i_204_n_0 ));
  LUT4 #(
    .INIT(16'h2DD2)) 
    \Xmap[6]_i_205 
       (.I0(\cnt_reg_n_0_[0] ),
        .I1(\cnt_reg_n_0_[3] ),
        .I2(\cnt_reg_n_0_[4] ),
        .I3(\cnt_reg_n_0_[1] ),
        .O(\Xmap[6]_i_205_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Xmap[6]_i_206 
       (.I0(\cnt_reg_n_0_[3] ),
        .I1(\cnt_reg_n_0_[0] ),
        .O(\Xmap[6]_i_206_n_0 ));
  LUT3 #(
    .INIT(8'h69)) 
    \Xmap[6]_i_212 
       (.I0(\cnt_reg_n_0_[5] ),
        .I1(\cnt_reg_n_0_[2] ),
        .I2(\cnt_reg_n_0_[0] ),
        .O(\Xmap[6]_i_212_n_0 ));
  LUT2 #(
    .INIT(4'hB)) 
    \Xmap[6]_i_213 
       (.I0(\cnt_reg_n_0_[3] ),
        .I1(\cnt_reg_n_0_[0] ),
        .O(\Xmap[6]_i_213_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_214 
       (.I0(\cnt_reg_n_0_[5] ),
        .I1(\cnt_reg_n_0_[2] ),
        .I2(\cnt_reg_n_0_[0] ),
        .I3(\cnt_reg_n_0_[1] ),
        .I4(\cnt_reg_n_0_[3] ),
        .I5(\cnt_reg_n_0_[6] ),
        .O(\Xmap[6]_i_214_n_0 ));
  LUT5 #(
    .INIT(32'h69966969)) 
    \Xmap[6]_i_215 
       (.I0(\cnt_reg_n_0_[0] ),
        .I1(\cnt_reg_n_0_[2] ),
        .I2(\cnt_reg_n_0_[5] ),
        .I3(\cnt_reg_n_0_[1] ),
        .I4(\cnt_reg_n_0_[4] ),
        .O(\Xmap[6]_i_215_n_0 ));
  LUT4 #(
    .INIT(16'h2DD2)) 
    \Xmap[6]_i_216 
       (.I0(\cnt_reg_n_0_[0] ),
        .I1(\cnt_reg_n_0_[3] ),
        .I2(\cnt_reg_n_0_[4] ),
        .I3(\cnt_reg_n_0_[1] ),
        .O(\Xmap[6]_i_216_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Xmap[6]_i_217 
       (.I0(\cnt_reg_n_0_[3] ),
        .I1(\cnt_reg_n_0_[0] ),
        .O(\Xmap[6]_i_217_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Xmap[6]_i_218 
       (.I0(\cnt_reg_n_0_[18] ),
        .I1(\cnt_reg_n_0_[16] ),
        .I2(\cnt_reg_n_0_[21] ),
        .I3(\cnt_reg_n_0_[17] ),
        .I4(\cnt_reg_n_0_[19] ),
        .I5(\cnt_reg_n_0_[22] ),
        .O(\Xmap[6]_i_218_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Xmap[6]_i_219 
       (.I0(\cnt_reg_n_0_[17] ),
        .I1(\cnt_reg_n_0_[15] ),
        .I2(\cnt_reg_n_0_[20] ),
        .I3(\cnt_reg_n_0_[21] ),
        .I4(\cnt_reg_n_0_[18] ),
        .I5(\cnt_reg_n_0_[16] ),
        .O(\Xmap[6]_i_219_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Xmap[6]_i_220 
       (.I0(\cnt_reg_n_0_[16] ),
        .I1(\cnt_reg_n_0_[14] ),
        .I2(\cnt_reg_n_0_[19] ),
        .I3(\cnt_reg_n_0_[15] ),
        .I4(\cnt_reg_n_0_[17] ),
        .I5(\cnt_reg_n_0_[20] ),
        .O(\Xmap[6]_i_220_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Xmap[6]_i_221 
       (.I0(\cnt_reg_n_0_[15] ),
        .I1(\cnt_reg_n_0_[13] ),
        .I2(\cnt_reg_n_0_[18] ),
        .I3(\cnt_reg_n_0_[14] ),
        .I4(\cnt_reg_n_0_[16] ),
        .I5(\cnt_reg_n_0_[19] ),
        .O(\Xmap[6]_i_221_n_0 ));
  LUT3 #(
    .INIT(8'hB2)) 
    \Xmap[6]_i_223 
       (.I0(\cnt_reg_n_0_[10] ),
        .I1(\cnt_reg_n_0_[14] ),
        .I2(\cnt_reg_n_0_[12] ),
        .O(\Xmap[6]_i_223_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_224 
       (.I0(\cnt_reg_n_0_[12] ),
        .I1(\cnt_reg_n_0_[14] ),
        .I2(\cnt_reg_n_0_[10] ),
        .I3(\cnt_reg_n_0_[13] ),
        .I4(\cnt_reg_n_0_[11] ),
        .I5(\cnt_reg_n_0_[15] ),
        .O(\Xmap[6]_i_224_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_225 
       (.I0(\cnt_reg_n_0_[11] ),
        .I1(\cnt_reg_n_0_[13] ),
        .I2(\cnt_reg_n_0_[9] ),
        .I3(\cnt_reg_n_0_[10] ),
        .I4(\cnt_reg_n_0_[12] ),
        .I5(\cnt_reg_n_0_[14] ),
        .O(\Xmap[6]_i_225_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_226 
       (.I0(\cnt_reg_n_0_[10] ),
        .I1(\cnt_reg_n_0_[12] ),
        .I2(\cnt_reg_n_0_[8] ),
        .I3(\cnt_reg_n_0_[13] ),
        .I4(\cnt_reg_n_0_[11] ),
        .I5(\cnt_reg_n_0_[9] ),
        .O(\Xmap[6]_i_226_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_227 
       (.I0(\cnt_reg_n_0_[9] ),
        .I1(\cnt_reg_n_0_[11] ),
        .I2(\cnt_reg_n_0_[7] ),
        .I3(\cnt_reg_n_0_[10] ),
        .I4(\cnt_reg_n_0_[12] ),
        .I5(\cnt_reg_n_0_[8] ),
        .O(\Xmap[6]_i_227_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Xmap[6]_i_234 
       (.I0(\cnt_reg_n_0_[14] ),
        .I1(\cnt_reg_n_0_[12] ),
        .I2(\cnt_reg_n_0_[17] ),
        .I3(\cnt_reg_n_0_[18] ),
        .I4(\cnt_reg_n_0_[13] ),
        .I5(\cnt_reg_n_0_[15] ),
        .O(\Xmap[6]_i_234_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Xmap[6]_i_235 
       (.I0(\cnt_reg_n_0_[13] ),
        .I1(\cnt_reg_n_0_[11] ),
        .I2(\cnt_reg_n_0_[16] ),
        .I3(\cnt_reg_n_0_[17] ),
        .I4(\cnt_reg_n_0_[12] ),
        .I5(\cnt_reg_n_0_[14] ),
        .O(\Xmap[6]_i_235_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Xmap[6]_i_236 
       (.I0(\cnt_reg_n_0_[12] ),
        .I1(\cnt_reg_n_0_[10] ),
        .I2(\cnt_reg_n_0_[15] ),
        .I3(\cnt_reg_n_0_[13] ),
        .I4(\cnt_reg_n_0_[11] ),
        .I5(\cnt_reg_n_0_[16] ),
        .O(\Xmap[6]_i_236_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Xmap[6]_i_237 
       (.I0(\cnt_reg_n_0_[11] ),
        .I1(\cnt_reg_n_0_[9] ),
        .I2(\cnt_reg_n_0_[14] ),
        .I3(\cnt_reg_n_0_[10] ),
        .I4(\cnt_reg_n_0_[12] ),
        .I5(\cnt_reg_n_0_[15] ),
        .O(\Xmap[6]_i_237_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_239 
       (.I0(\cnt_reg_n_0_[8] ),
        .I1(\cnt_reg_n_0_[10] ),
        .I2(\cnt_reg_n_0_[6] ),
        .I3(\cnt_reg_n_0_[9] ),
        .I4(\cnt_reg_n_0_[7] ),
        .I5(\cnt_reg_n_0_[11] ),
        .O(\Xmap[6]_i_239_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_240 
       (.I0(\cnt_reg_n_0_[7] ),
        .I1(\cnt_reg_n_0_[9] ),
        .I2(\cnt_reg_n_0_[5] ),
        .I3(\cnt_reg_n_0_[6] ),
        .I4(\cnt_reg_n_0_[8] ),
        .I5(\cnt_reg_n_0_[10] ),
        .O(\Xmap[6]_i_240_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_241 
       (.I0(\cnt_reg_n_0_[6] ),
        .I1(\cnt_reg_n_0_[8] ),
        .I2(\cnt_reg_n_0_[4] ),
        .I3(\cnt_reg_n_0_[9] ),
        .I4(\cnt_reg_n_0_[7] ),
        .I5(\cnt_reg_n_0_[5] ),
        .O(\Xmap[6]_i_241_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_242 
       (.I0(\cnt_reg_n_0_[5] ),
        .I1(\cnt_reg_n_0_[7] ),
        .I2(\cnt_reg_n_0_[3] ),
        .I3(\cnt_reg_n_0_[6] ),
        .I4(\cnt_reg_n_0_[8] ),
        .I5(\cnt_reg_n_0_[4] ),
        .O(\Xmap[6]_i_242_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Xmap[6]_i_247 
       (.I0(\Xmap_reg[6]_i_243_n_7 ),
        .I1(\cnt_reg_n_0_[0] ),
        .O(\Xmap[6]_i_247_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Xmap[6]_i_248 
       (.I0(\cnt_reg_n_0_[10] ),
        .I1(\cnt_reg_n_0_[8] ),
        .I2(\cnt_reg_n_0_[13] ),
        .I3(\cnt_reg_n_0_[14] ),
        .I4(\cnt_reg_n_0_[9] ),
        .I5(\cnt_reg_n_0_[11] ),
        .O(\Xmap[6]_i_248_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Xmap[6]_i_249 
       (.I0(\cnt_reg_n_0_[9] ),
        .I1(\cnt_reg_n_0_[7] ),
        .I2(\cnt_reg_n_0_[12] ),
        .I3(\cnt_reg_n_0_[13] ),
        .I4(\cnt_reg_n_0_[10] ),
        .I5(\cnt_reg_n_0_[8] ),
        .O(\Xmap[6]_i_249_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Xmap[6]_i_250 
       (.I0(\cnt_reg_n_0_[8] ),
        .I1(\cnt_reg_n_0_[6] ),
        .I2(\cnt_reg_n_0_[11] ),
        .I3(\cnt_reg_n_0_[9] ),
        .I4(\cnt_reg_n_0_[7] ),
        .I5(\cnt_reg_n_0_[12] ),
        .O(\Xmap[6]_i_250_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Xmap[6]_i_251 
       (.I0(\cnt_reg_n_0_[7] ),
        .I1(\cnt_reg_n_0_[5] ),
        .I2(\cnt_reg_n_0_[10] ),
        .I3(\cnt_reg_n_0_[6] ),
        .I4(\cnt_reg_n_0_[8] ),
        .I5(\cnt_reg_n_0_[11] ),
        .O(\Xmap[6]_i_251_n_0 ));
  LUT3 #(
    .INIT(8'h69)) 
    \Xmap[6]_i_252 
       (.I0(\cnt_reg_n_0_[1] ),
        .I1(\cnt_reg_n_0_[3] ),
        .I2(\cnt_reg_n_0_[5] ),
        .O(\Xmap[6]_i_252_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_253 
       (.I0(\cnt_reg_n_0_[4] ),
        .I1(\cnt_reg_n_0_[6] ),
        .I2(\cnt_reg_n_0_[2] ),
        .I3(\cnt_reg_n_0_[5] ),
        .I4(\cnt_reg_n_0_[3] ),
        .I5(\cnt_reg_n_0_[7] ),
        .O(\Xmap[6]_i_253_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_254 
       (.I0(\cnt_reg_n_0_[3] ),
        .I1(\cnt_reg_n_0_[5] ),
        .I2(\cnt_reg_n_0_[1] ),
        .I3(\cnt_reg_n_0_[4] ),
        .I4(\cnt_reg_n_0_[6] ),
        .I5(\cnt_reg_n_0_[2] ),
        .O(\Xmap[6]_i_254_n_0 ));
  LUT5 #(
    .INIT(32'h69966969)) 
    \Xmap[6]_i_255 
       (.I0(\cnt_reg_n_0_[5] ),
        .I1(\cnt_reg_n_0_[3] ),
        .I2(\cnt_reg_n_0_[1] ),
        .I3(\cnt_reg_n_0_[4] ),
        .I4(\cnt_reg_n_0_[0] ),
        .O(\Xmap[6]_i_255_n_0 ));
  LUT3 #(
    .INIT(8'h69)) 
    \Xmap[6]_i_256 
       (.I0(\cnt_reg_n_0_[4] ),
        .I1(\cnt_reg_n_0_[0] ),
        .I2(\cnt_reg_n_0_[2] ),
        .O(\Xmap[6]_i_256_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Xmap[6]_i_257 
       (.I0(\cnt_reg_n_0_[6] ),
        .I1(\cnt_reg_n_0_[4] ),
        .I2(\cnt_reg_n_0_[9] ),
        .I3(\cnt_reg_n_0_[10] ),
        .I4(\cnt_reg_n_0_[5] ),
        .I5(\cnt_reg_n_0_[7] ),
        .O(\Xmap[6]_i_257_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Xmap[6]_i_258 
       (.I0(\cnt_reg_n_0_[5] ),
        .I1(\cnt_reg_n_0_[3] ),
        .I2(\cnt_reg_n_0_[8] ),
        .I3(\cnt_reg_n_0_[4] ),
        .I4(\cnt_reg_n_0_[6] ),
        .I5(\cnt_reg_n_0_[9] ),
        .O(\Xmap[6]_i_258_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_259 
       (.I0(\cnt_reg_n_0_[7] ),
        .I1(\cnt_reg_n_0_[4] ),
        .I2(\cnt_reg_n_0_[2] ),
        .I3(\cnt_reg_n_0_[5] ),
        .I4(\cnt_reg_n_0_[3] ),
        .I5(\cnt_reg_n_0_[8] ),
        .O(\Xmap[6]_i_259_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_260 
       (.I0(\cnt_reg_n_0_[6] ),
        .I1(\cnt_reg_n_0_[3] ),
        .I2(\cnt_reg_n_0_[1] ),
        .I3(\cnt_reg_n_0_[7] ),
        .I4(\cnt_reg_n_0_[2] ),
        .I5(\cnt_reg_n_0_[4] ),
        .O(\Xmap[6]_i_260_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \Xmap[6]_i_4 
       (.I0(\cnt_reg_n_0_[7] ),
        .I1(\cnt_reg[20]_3 [2]),
        .O(\Xmap[6]_i_4_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Xmap[6]_i_44 
       (.I0(O[3]),
        .I1(\Xmap_reg[4]_0 [3]),
        .I2(\Xmap_reg[4]_2 [2]),
        .O(\Xmap[6]_i_44_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Xmap[6]_i_45 
       (.I0(O[2]),
        .I1(\Xmap_reg[4]_0 [2]),
        .I2(\Xmap_reg[4]_2 [1]),
        .O(\Xmap[6]_i_45_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Xmap[6]_i_46 
       (.I0(O[1]),
        .I1(\Xmap_reg[4]_0 [1]),
        .I2(\Xmap_reg[4]_2 [0]),
        .O(\Xmap[6]_i_46_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Xmap[6]_i_47 
       (.I0(O[0]),
        .I1(\Xmap_reg[4]_0 [0]),
        .I2(\Xmap_reg[4]_1 [0]),
        .O(\Xmap[6]_i_47_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \Xmap[6]_i_5 
       (.I0(\cnt_reg_n_0_[6] ),
        .I1(\cnt_reg[20]_3 [1]),
        .O(\Xmap[6]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Xmap[6]_i_55 
       (.I0(\cnt_reg_n_0_[15] ),
        .I1(\cnt_reg_n_0_[10] ),
        .I2(\cnt_reg_n_0_[12] ),
        .O(\Xmap[6]_i_55_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Xmap[6]_i_56 
       (.I0(\cnt_reg_n_0_[14] ),
        .I1(\cnt_reg_n_0_[12] ),
        .I2(\cnt_reg_n_0_[17] ),
        .I3(\cnt_reg_n_0_[18] ),
        .I4(\cnt_reg_n_0_[13] ),
        .I5(\cnt_reg_n_0_[15] ),
        .O(\Xmap[6]_i_56_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Xmap[6]_i_57 
       (.I0(\cnt_reg_n_0_[13] ),
        .I1(\cnt_reg_n_0_[11] ),
        .I2(\cnt_reg_n_0_[16] ),
        .I3(\cnt_reg_n_0_[17] ),
        .I4(\cnt_reg_n_0_[12] ),
        .I5(\cnt_reg_n_0_[14] ),
        .O(\Xmap[6]_i_57_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Xmap[6]_i_58 
       (.I0(\cnt_reg_n_0_[12] ),
        .I1(\cnt_reg_n_0_[10] ),
        .I2(\cnt_reg_n_0_[15] ),
        .I3(\cnt_reg_n_0_[13] ),
        .I4(\cnt_reg_n_0_[11] ),
        .I5(\cnt_reg_n_0_[16] ),
        .O(\Xmap[6]_i_58_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Xmap[6]_i_59 
       (.I0(\cnt_reg_n_0_[11] ),
        .I1(\cnt_reg_n_0_[9] ),
        .I2(\cnt_reg_n_0_[14] ),
        .I3(\cnt_reg_n_0_[10] ),
        .I4(\cnt_reg_n_0_[12] ),
        .I5(\cnt_reg_n_0_[15] ),
        .O(\Xmap[6]_i_59_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \Xmap[6]_i_6 
       (.I0(\cnt_reg_n_0_[5] ),
        .I1(\cnt_reg[20]_3 [0]),
        .O(\Xmap[6]_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Xmap[6]_i_60 
       (.I0(\Xmap_reg[6]_i_125_n_5 ),
        .I1(\cnt_reg_n_0_[5] ),
        .I2(\Xmap_reg[6]_i_126_n_5 ),
        .O(\Xmap[6]_i_60_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Xmap[6]_i_61 
       (.I0(\Xmap_reg[6]_i_125_n_6 ),
        .I1(\cnt_reg_n_0_[4] ),
        .I2(\Xmap_reg[6]_i_126_n_6 ),
        .O(\Xmap[6]_i_61_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Xmap[6]_i_62 
       (.I0(\Xmap_reg[6]_i_125_n_7 ),
        .I1(\cnt_reg_n_0_[3] ),
        .I2(\Xmap_reg[6]_i_126_n_7 ),
        .O(\Xmap[6]_i_62_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Xmap[6]_i_63 
       (.I0(\Xmap_reg[6]_i_113_n_4 ),
        .I1(\cnt_reg_n_0_[2] ),
        .I2(\Xmap_reg[6]_i_127_n_4 ),
        .O(\Xmap[6]_i_63_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Xmap[6]_i_64 
       (.I0(\Xmap_reg[6]_i_126_n_5 ),
        .I1(\cnt_reg_n_0_[5] ),
        .I2(\Xmap_reg[6]_i_125_n_5 ),
        .I3(\Xmap_reg[6]_i_126_n_4 ),
        .I4(\cnt_reg_n_0_[6] ),
        .I5(\Xmap_reg[6]_i_125_n_4 ),
        .O(\Xmap[6]_i_64_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Xmap[6]_i_65 
       (.I0(\Xmap_reg[6]_i_126_n_6 ),
        .I1(\cnt_reg_n_0_[4] ),
        .I2(\Xmap_reg[6]_i_125_n_6 ),
        .I3(\Xmap_reg[6]_i_126_n_5 ),
        .I4(\cnt_reg_n_0_[5] ),
        .I5(\Xmap_reg[6]_i_125_n_5 ),
        .O(\Xmap[6]_i_65_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Xmap[6]_i_66 
       (.I0(\Xmap_reg[6]_i_126_n_7 ),
        .I1(\cnt_reg_n_0_[3] ),
        .I2(\Xmap_reg[6]_i_125_n_7 ),
        .I3(\Xmap_reg[6]_i_126_n_6 ),
        .I4(\cnt_reg_n_0_[4] ),
        .I5(\Xmap_reg[6]_i_125_n_6 ),
        .O(\Xmap[6]_i_66_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Xmap[6]_i_67 
       (.I0(\Xmap_reg[6]_i_127_n_4 ),
        .I1(\cnt_reg_n_0_[2] ),
        .I2(\Xmap_reg[6]_i_113_n_4 ),
        .I3(\Xmap_reg[6]_i_126_n_7 ),
        .I4(\cnt_reg_n_0_[3] ),
        .I5(\Xmap_reg[6]_i_125_n_7 ),
        .O(\Xmap[6]_i_67_n_0 ));
  LUT3 #(
    .INIT(8'hB2)) 
    \Xmap[6]_i_68 
       (.I0(\cnt_reg_n_0_[6] ),
        .I1(\cnt_reg_n_0_[10] ),
        .I2(\cnt_reg_n_0_[8] ),
        .O(\Xmap[6]_i_68_n_0 ));
  LUT3 #(
    .INIT(8'hB2)) 
    \Xmap[6]_i_69 
       (.I0(\cnt_reg_n_0_[5] ),
        .I1(\cnt_reg_n_0_[9] ),
        .I2(\cnt_reg_n_0_[7] ),
        .O(\Xmap[6]_i_69_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \Xmap[6]_i_7 
       (.I0(\cnt_reg_n_0_[4] ),
        .I1(\Xmap_reg[4]_24 ),
        .O(\Xmap[6]_i_7_n_0 ));
  LUT3 #(
    .INIT(8'hB2)) 
    \Xmap[6]_i_70 
       (.I0(\cnt_reg_n_0_[4] ),
        .I1(\cnt_reg_n_0_[8] ),
        .I2(\cnt_reg_n_0_[6] ),
        .O(\Xmap[6]_i_70_n_0 ));
  LUT3 #(
    .INIT(8'hB2)) 
    \Xmap[6]_i_71 
       (.I0(\cnt_reg_n_0_[3] ),
        .I1(\cnt_reg_n_0_[7] ),
        .I2(\cnt_reg_n_0_[5] ),
        .O(\Xmap[6]_i_71_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_72 
       (.I0(\cnt_reg_n_0_[8] ),
        .I1(\cnt_reg_n_0_[10] ),
        .I2(\cnt_reg_n_0_[6] ),
        .I3(\cnt_reg_n_0_[9] ),
        .I4(\cnt_reg_n_0_[7] ),
        .I5(\cnt_reg_n_0_[11] ),
        .O(\Xmap[6]_i_72_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_73 
       (.I0(\cnt_reg_n_0_[7] ),
        .I1(\cnt_reg_n_0_[9] ),
        .I2(\cnt_reg_n_0_[5] ),
        .I3(\cnt_reg_n_0_[6] ),
        .I4(\cnt_reg_n_0_[8] ),
        .I5(\cnt_reg_n_0_[10] ),
        .O(\Xmap[6]_i_73_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_74 
       (.I0(\cnt_reg_n_0_[6] ),
        .I1(\cnt_reg_n_0_[8] ),
        .I2(\cnt_reg_n_0_[4] ),
        .I3(\cnt_reg_n_0_[9] ),
        .I4(\cnt_reg_n_0_[7] ),
        .I5(\cnt_reg_n_0_[5] ),
        .O(\Xmap[6]_i_74_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_75 
       (.I0(\cnt_reg_n_0_[5] ),
        .I1(\cnt_reg_n_0_[7] ),
        .I2(\cnt_reg_n_0_[3] ),
        .I3(\cnt_reg_n_0_[6] ),
        .I4(\cnt_reg_n_0_[8] ),
        .I5(\cnt_reg_n_0_[4] ),
        .O(\Xmap[6]_i_75_n_0 ));
  LUT3 #(
    .INIT(8'hB2)) 
    \Xmap[6]_i_76 
       (.I0(\cnt_reg_n_0_[9] ),
        .I1(\cnt_reg_n_0_[13] ),
        .I2(\cnt_reg_n_0_[11] ),
        .O(\Xmap[6]_i_76_n_0 ));
  LUT3 #(
    .INIT(8'hB2)) 
    \Xmap[6]_i_77 
       (.I0(\cnt_reg_n_0_[8] ),
        .I1(\cnt_reg_n_0_[12] ),
        .I2(\cnt_reg_n_0_[10] ),
        .O(\Xmap[6]_i_77_n_0 ));
  LUT3 #(
    .INIT(8'hB2)) 
    \Xmap[6]_i_78 
       (.I0(\cnt_reg_n_0_[7] ),
        .I1(\cnt_reg_n_0_[11] ),
        .I2(\cnt_reg_n_0_[9] ),
        .O(\Xmap[6]_i_78_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_79 
       (.I0(\cnt_reg_n_0_[12] ),
        .I1(\cnt_reg_n_0_[14] ),
        .I2(\cnt_reg_n_0_[10] ),
        .I3(\cnt_reg_n_0_[13] ),
        .I4(\cnt_reg_n_0_[11] ),
        .I5(\cnt_reg_n_0_[15] ),
        .O(\Xmap[6]_i_79_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_80 
       (.I0(\cnt_reg_n_0_[11] ),
        .I1(\cnt_reg_n_0_[13] ),
        .I2(\cnt_reg_n_0_[9] ),
        .I3(\cnt_reg_n_0_[10] ),
        .I4(\cnt_reg_n_0_[12] ),
        .I5(\cnt_reg_n_0_[14] ),
        .O(\Xmap[6]_i_80_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_81 
       (.I0(\cnt_reg_n_0_[10] ),
        .I1(\cnt_reg_n_0_[12] ),
        .I2(\cnt_reg_n_0_[8] ),
        .I3(\cnt_reg_n_0_[13] ),
        .I4(\cnt_reg_n_0_[11] ),
        .I5(\cnt_reg_n_0_[9] ),
        .O(\Xmap[6]_i_81_n_0 ));
  LUT6 #(
    .INIT(64'hB24D4DB24DB2B24D)) 
    \Xmap[6]_i_82 
       (.I0(\cnt_reg_n_0_[9] ),
        .I1(\cnt_reg_n_0_[11] ),
        .I2(\cnt_reg_n_0_[7] ),
        .I3(\cnt_reg_n_0_[10] ),
        .I4(\cnt_reg_n_0_[12] ),
        .I5(\cnt_reg_n_0_[8] ),
        .O(\Xmap[6]_i_82_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Xmap[6]_i_83 
       (.I0(\Xmap_reg[6]_i_128_n_6 ),
        .I1(\cnt_reg_n_0_[8] ),
        .I2(\Xmap_reg[6]_i_129_n_6 ),
        .O(\Xmap[6]_i_83_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Xmap[6]_i_84 
       (.I0(\Xmap_reg[6]_i_128_n_7 ),
        .I1(\cnt_reg_n_0_[7] ),
        .I2(\Xmap_reg[6]_i_129_n_7 ),
        .O(\Xmap[6]_i_84_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Xmap[6]_i_85 
       (.I0(\Xmap_reg[6]_i_125_n_4 ),
        .I1(\cnt_reg_n_0_[6] ),
        .I2(\Xmap_reg[6]_i_126_n_4 ),
        .O(\Xmap[6]_i_85_n_0 ));
  LUT6 #(
    .INIT(64'h3CC369966996C33C)) 
    \Xmap[6]_i_86 
       (.I0(\Xmap_reg[6]_i_128_n_5 ),
        .I1(\Xmap_reg[6]_i_129_n_4 ),
        .I2(\cnt_reg_n_0_[10] ),
        .I3(\Xmap_reg[6]_i_128_n_4 ),
        .I4(\cnt_reg_n_0_[9] ),
        .I5(\Xmap_reg[6]_i_129_n_5 ),
        .O(\Xmap[6]_i_86_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Xmap[6]_i_87 
       (.I0(\Xmap_reg[6]_i_129_n_6 ),
        .I1(\cnt_reg_n_0_[8] ),
        .I2(\Xmap_reg[6]_i_128_n_6 ),
        .I3(\Xmap_reg[6]_i_129_n_5 ),
        .I4(\cnt_reg_n_0_[9] ),
        .I5(\Xmap_reg[6]_i_128_n_5 ),
        .O(\Xmap[6]_i_87_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Xmap[6]_i_88 
       (.I0(\Xmap_reg[6]_i_129_n_7 ),
        .I1(\cnt_reg_n_0_[7] ),
        .I2(\Xmap_reg[6]_i_128_n_7 ),
        .I3(\Xmap_reg[6]_i_129_n_6 ),
        .I4(\cnt_reg_n_0_[8] ),
        .I5(\Xmap_reg[6]_i_128_n_6 ),
        .O(\Xmap[6]_i_88_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Xmap[6]_i_89 
       (.I0(\Xmap_reg[6]_i_126_n_4 ),
        .I1(\cnt_reg_n_0_[6] ),
        .I2(\Xmap_reg[6]_i_125_n_4 ),
        .I3(\Xmap_reg[6]_i_129_n_7 ),
        .I4(\cnt_reg_n_0_[7] ),
        .I5(\Xmap_reg[6]_i_128_n_7 ),
        .O(\Xmap[6]_i_89_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Xmap[6]_i_90 
       (.I0(\cnt_reg_n_0_[18] ),
        .I1(\cnt_reg_n_0_[16] ),
        .I2(\cnt_reg_n_0_[21] ),
        .I3(\cnt_reg_n_0_[17] ),
        .I4(\cnt_reg_n_0_[19] ),
        .I5(\cnt_reg_n_0_[22] ),
        .O(\Xmap[6]_i_90_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Xmap[6]_i_91 
       (.I0(\cnt_reg_n_0_[17] ),
        .I1(\cnt_reg_n_0_[15] ),
        .I2(\cnt_reg_n_0_[20] ),
        .I3(\cnt_reg_n_0_[21] ),
        .I4(\cnt_reg_n_0_[18] ),
        .I5(\cnt_reg_n_0_[16] ),
        .O(\Xmap[6]_i_91_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Xmap[6]_i_92 
       (.I0(\cnt_reg_n_0_[16] ),
        .I1(\cnt_reg_n_0_[14] ),
        .I2(\cnt_reg_n_0_[19] ),
        .I3(\cnt_reg_n_0_[15] ),
        .I4(\cnt_reg_n_0_[17] ),
        .I5(\cnt_reg_n_0_[20] ),
        .O(\Xmap[6]_i_92_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Xmap[6]_i_93 
       (.I0(\cnt_reg_n_0_[15] ),
        .I1(\cnt_reg_n_0_[13] ),
        .I2(\cnt_reg_n_0_[18] ),
        .I3(\cnt_reg_n_0_[14] ),
        .I4(\cnt_reg_n_0_[16] ),
        .I5(\cnt_reg_n_0_[19] ),
        .O(\Xmap[6]_i_93_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \Xmap[6]_i_95 
       (.I0(\Xmap_reg[4]_3 [2]),
        .I1(\Xmap_reg[4]_4 [3]),
        .O(\Xmap[6]_i_95_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \Xmap[6]_i_96 
       (.I0(\Xmap_reg[4]_3 [1]),
        .I1(\Xmap_reg[4]_4 [2]),
        .O(\Xmap[6]_i_96_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \Xmap[6]_i_97 
       (.I0(\Xmap_reg[4]_3 [0]),
        .I1(\Xmap_reg[4]_4 [1]),
        .O(\Xmap[6]_i_97_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \Xmap[6]_i_98 
       (.I0(\Xmap_reg[4]_5 ),
        .I1(\Xmap_reg[4]_4 [0]),
        .O(\Xmap[6]_i_98_n_0 ));
  FDRE \Xmap_reg[0] 
       (.C(clk),
        .CE(\Xmap[6]_i_1_n_0 ),
        .D(\Xmap_reg[3]_i_1_n_7 ),
        .Q(ADDRARDADDR[0]),
        .R(1'b0));
  FDRE \Xmap_reg[1] 
       (.C(clk),
        .CE(\Xmap[6]_i_1_n_0 ),
        .D(\Xmap_reg[3]_i_1_n_6 ),
        .Q(ADDRARDADDR[1]),
        .R(1'b0));
  FDRE \Xmap_reg[2] 
       (.C(clk),
        .CE(\Xmap[6]_i_1_n_0 ),
        .D(\Xmap_reg[3]_i_1_n_5 ),
        .Q(ADDRARDADDR[2]),
        .R(1'b0));
  FDRE \Xmap_reg[3] 
       (.C(clk),
        .CE(\Xmap[6]_i_1_n_0 ),
        .D(\Xmap_reg[3]_i_1_n_4 ),
        .Q(ADDRARDADDR[3]),
        .R(1'b0));
  CARRY4 \Xmap_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\Xmap_reg[3]_i_1_n_0 ,\Xmap_reg[3]_i_1_n_1 ,\Xmap_reg[3]_i_1_n_2 ,\Xmap_reg[3]_i_1_n_3 }),
        .CYINIT(1'b1),
        .DI({\cnt_reg_n_0_[3] ,\cnt_reg_n_0_[2] ,\cnt_reg_n_0_[1] ,\cnt_reg_n_0_[0] }),
        .O({\Xmap_reg[3]_i_1_n_4 ,\Xmap_reg[3]_i_1_n_5 ,\Xmap_reg[3]_i_1_n_6 ,\Xmap_reg[3]_i_1_n_7 }),
        .S({\Xmap[3]_i_2_n_0 ,\Xmap[3]_i_3_n_0 ,\Xmap[3]_i_4_n_0 ,\Xmap[3]_i_5_n_0 }));
  FDRE \Xmap_reg[4] 
       (.C(clk),
        .CE(\Xmap[6]_i_1_n_0 ),
        .D(\Xmap[4]_i_1_n_0 ),
        .Q(tm_reg_0_0[0]),
        .R(1'b0));
  FDRE \Xmap_reg[5] 
       (.C(clk),
        .CE(\Xmap[6]_i_1_n_0 ),
        .D(\Xmap[5]_i_1_n_0 ),
        .Q(tm_reg_0_0[1]),
        .R(1'b0));
  FDRE \Xmap_reg[6] 
       (.C(clk),
        .CE(\Xmap[6]_i_1_n_0 ),
        .D(\Xmap[6]_i_2_n_0 ),
        .Q(tm_reg_0_0[2]),
        .R(1'b0));
  CARRY4 \Xmap_reg[6]_i_10 
       (.CI(\Xmap_reg[6]_i_9_n_0 ),
        .CO({\NLW_Xmap_reg[6]_i_10_CO_UNCONNECTED [3:2],\Xmap_reg[6]_i_10_n_2 ,\Xmap_reg[6]_i_10_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,\cnt_reg[20]_1 }),
        .O({\NLW_Xmap_reg[6]_i_10_O_UNCONNECTED [3],\Xmap_reg[4]_25 }),
        .S({1'b0,\cnt_reg[20]_2 }));
  CARRY4 \Xmap_reg[6]_i_103 
       (.CI(\Xmap_reg[6]_i_137_n_0 ),
        .CO({\Xmap_reg[6]_i_103_n_0 ,\Xmap_reg[6]_i_103_n_1 ,\Xmap_reg[6]_i_103_n_2 ,\Xmap_reg[6]_i_103_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[0]_i_106_n_0 ,\Ymap[0]_i_107_n_0 ,\Xmap[6]_i_140_n_0 ,\Xmap[6]_i_141_n_0 }),
        .O(O),
        .S({\Xmap[6]_i_142_n_0 ,\Xmap[6]_i_143_n_0 ,\Xmap[6]_i_144_n_0 ,\Xmap[6]_i_145_n_0 }));
  CARRY4 \Xmap_reg[6]_i_104 
       (.CI(\Xmap_reg[6]_i_138_n_0 ),
        .CO({\Xmap_reg[6]_i_104_n_0 ,\Xmap_reg[6]_i_104_n_1 ,\Xmap_reg[6]_i_104_n_2 ,\Xmap_reg[6]_i_104_n_3 }),
        .CYINIT(1'b0),
        .DI(\Xmap_reg[4]_9 ),
        .O(\Xmap_reg[4]_0 ),
        .S(\cnt_reg[21]_0 ));
  CARRY4 \Xmap_reg[6]_i_105 
       (.CI(1'b0),
        .CO({\Xmap_reg[6]_i_105_n_0 ,\Xmap_reg[6]_i_105_n_1 ,\Xmap_reg[6]_i_105_n_2 ,\Xmap_reg[6]_i_105_n_3 }),
        .CYINIT(1'b0),
        .DI({\cnt_reg_n_0_[1] ,\cnt_reg_n_0_[0] ,1'b0,1'b1}),
        .O({\Xmap_reg[4]_2 ,\NLW_Xmap_reg[6]_i_105_O_UNCONNECTED [0]}),
        .S({\Xmap[6]_i_151_n_0 ,\Xmap[6]_i_152_n_0 ,\Xmap[6]_i_153_n_0 ,\Xmap[6]_i_154_n_0 }));
  CARRY4 \Xmap_reg[6]_i_106 
       (.CI(1'b0),
        .CO({\Xmap_reg[6]_i_106_n_0 ,\Xmap_reg[6]_i_106_n_1 ,\Xmap_reg[6]_i_106_n_2 ,\Xmap_reg[6]_i_106_n_3 }),
        .CYINIT(1'b0),
        .DI({\cnt_reg_n_0_[1] ,\cnt_reg_n_0_[0] ,1'b0,1'b1}),
        .O(\Xmap_reg[4]_1 ),
        .S({\Xmap[6]_i_155_n_0 ,\Xmap[6]_i_156_n_0 ,\Xmap[6]_i_157_n_0 ,\Xmap[6]_i_158_n_0 }));
  CARRY4 \Xmap_reg[6]_i_113 
       (.CI(\Xmap_reg[6]_i_146_n_0 ),
        .CO({\Xmap_reg[6]_i_113_n_0 ,\Xmap_reg[6]_i_113_n_1 ,\Xmap_reg[6]_i_113_n_2 ,\Xmap_reg[6]_i_113_n_3 }),
        .CYINIT(1'b0),
        .DI({\Xmap[6]_i_159_n_0 ,\Ymap[5]_i_57_n_0 ,\Ymap[5]_i_58_n_0 ,\Xmap[6]_i_160_n_0 }),
        .O({\Xmap_reg[6]_i_113_n_4 ,\Xmap_reg[6]_i_113_n_5 ,\Xmap_reg[6]_i_113_n_6 ,\Xmap_reg[4]_10 }),
        .S({\Xmap[6]_i_161_n_0 ,\Xmap[6]_i_162_n_0 ,\Xmap[6]_i_163_n_0 ,\Xmap[6]_i_164_n_0 }));
  CARRY4 \Xmap_reg[6]_i_125 
       (.CI(\Xmap_reg[6]_i_113_n_0 ),
        .CO({\Xmap_reg[6]_i_125_n_0 ,\Xmap_reg[6]_i_125_n_1 ,\Xmap_reg[6]_i_125_n_2 ,\Xmap_reg[6]_i_125_n_3 }),
        .CYINIT(1'b0),
        .DI({\Xmap[6]_i_165_n_0 ,\Ymap[5]_i_81_n_0 ,\Ymap[5]_i_82_n_0 ,\Xmap[6]_i_166_n_0 }),
        .O({\Xmap_reg[6]_i_125_n_4 ,\Xmap_reg[6]_i_125_n_5 ,\Xmap_reg[6]_i_125_n_6 ,\Xmap_reg[6]_i_125_n_7 }),
        .S({\Xmap[6]_i_167_n_0 ,\Xmap[6]_i_168_n_0 ,\Xmap[6]_i_169_n_0 ,\Xmap[6]_i_170_n_0 }));
  CARRY4 \Xmap_reg[6]_i_126 
       (.CI(\Xmap_reg[6]_i_127_n_0 ),
        .CO({\Xmap_reg[6]_i_126_n_0 ,\Xmap_reg[6]_i_126_n_1 ,\Xmap_reg[6]_i_126_n_2 ,\Xmap_reg[6]_i_126_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[5]_i_96_n_0 ,\Xmap[6]_i_171_n_0 ,\Xmap[6]_i_172_n_0 ,\Ymap[5]_i_99_n_0 }),
        .O({\Xmap_reg[6]_i_126_n_4 ,\Xmap_reg[6]_i_126_n_5 ,\Xmap_reg[6]_i_126_n_6 ,\Xmap_reg[6]_i_126_n_7 }),
        .S({\Xmap[6]_i_173_n_0 ,\Xmap[6]_i_174_n_0 ,\Xmap[6]_i_175_n_0 ,\Xmap[6]_i_176_n_0 }));
  CARRY4 \Xmap_reg[6]_i_127 
       (.CI(\Xmap_reg[6]_i_177_n_0 ),
        .CO({\Xmap_reg[6]_i_127_n_0 ,\Xmap_reg[6]_i_127_n_1 ,\Xmap_reg[6]_i_127_n_2 ,\Xmap_reg[6]_i_127_n_3 }),
        .CYINIT(1'b0),
        .DI({\Xmap[6]_i_178_n_0 ,\Xmap[6]_i_179_n_0 ,\Xmap[6]_i_180_n_0 ,\Xmap[6]_i_181_n_0 }),
        .O({\Xmap_reg[6]_i_127_n_4 ,\Xmap_reg[6]_i_127_n_5 ,\Xmap_reg[6]_i_127_n_6 ,\Xmap_reg[4]_14 }),
        .S({\Xmap[6]_i_182_n_0 ,\Xmap[6]_i_183_n_0 ,\Xmap[6]_i_184_n_0 ,\Xmap[6]_i_185_n_0 }));
  CARRY4 \Xmap_reg[6]_i_128 
       (.CI(\Xmap_reg[6]_i_125_n_0 ),
        .CO({\NLW_Xmap_reg[6]_i_128_CO_UNCONNECTED [3],\Xmap_reg[6]_i_128_n_1 ,\Xmap_reg[6]_i_128_n_2 ,\Xmap_reg[6]_i_128_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,\Xmap[6]_i_186_n_0 ,\Xmap[6]_i_187_n_0 ,\Xmap[6]_i_188_n_0 }),
        .O({\Xmap_reg[6]_i_128_n_4 ,\Xmap_reg[6]_i_128_n_5 ,\Xmap_reg[6]_i_128_n_6 ,\Xmap_reg[6]_i_128_n_7 }),
        .S({\Xmap[6]_i_189_n_0 ,\Xmap[6]_i_190_n_0 ,\Xmap[6]_i_191_n_0 ,\Xmap[6]_i_192_n_0 }));
  CARRY4 \Xmap_reg[6]_i_129 
       (.CI(\Xmap_reg[6]_i_126_n_0 ),
        .CO({\NLW_Xmap_reg[6]_i_129_CO_UNCONNECTED [3],\Xmap_reg[6]_i_129_n_1 ,\Xmap_reg[6]_i_129_n_2 ,\Xmap_reg[6]_i_129_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,\Xmap[6]_i_193_n_0 ,\Xmap[6]_i_194_n_0 ,\Ymap[5]_i_172_n_0 }),
        .O({\Xmap_reg[6]_i_129_n_4 ,\Xmap_reg[6]_i_129_n_5 ,\Xmap_reg[6]_i_129_n_6 ,\Xmap_reg[6]_i_129_n_7 }),
        .S({\Xmap[6]_i_195_n_0 ,\Xmap[6]_i_196_n_0 ,\Xmap[6]_i_197_n_0 ,\Xmap[6]_i_198_n_0 }));
  CARRY4 \Xmap_reg[6]_i_137 
       (.CI(1'b0),
        .CO({\Xmap_reg[6]_i_137_n_0 ,\Xmap_reg[6]_i_137_n_1 ,\Xmap_reg[6]_i_137_n_2 ,\Xmap_reg[6]_i_137_n_3 }),
        .CYINIT(1'b0),
        .DI({\Xmap[6]_i_200_n_0 ,\Xmap[6]_i_201_n_0 ,\Xmap[6]_i_202_n_0 ,1'b0}),
        .O({\Xmap_reg[4]_3 ,\NLW_Xmap_reg[6]_i_137_O_UNCONNECTED [0]}),
        .S({\Xmap[6]_i_203_n_0 ,\Xmap[6]_i_204_n_0 ,\Xmap[6]_i_205_n_0 ,\Xmap[6]_i_206_n_0 }));
  CARRY4 \Xmap_reg[6]_i_138 
       (.CI(\Xmap_reg[6]_i_199_n_0 ),
        .CO({\Xmap_reg[6]_i_138_n_0 ,\Xmap_reg[6]_i_138_n_1 ,\Xmap_reg[6]_i_138_n_2 ,\Xmap_reg[6]_i_138_n_3 }),
        .CYINIT(1'b0),
        .DI(\Xmap_reg[4]_8 ),
        .O(\Xmap_reg[4]_4 ),
        .S(\cnt_reg[17]_0 ));
  CARRY4 \Xmap_reg[6]_i_139 
       (.CI(1'b0),
        .CO({\Xmap_reg[6]_i_139_n_0 ,\Xmap_reg[6]_i_139_n_1 ,\Xmap_reg[6]_i_139_n_2 ,\Xmap_reg[6]_i_139_n_3 }),
        .CYINIT(1'b0),
        .DI({\Xmap[6]_i_200_n_0 ,\Xmap[6]_i_212_n_0 ,\Xmap[6]_i_213_n_0 ,1'b0}),
        .O({\NLW_Xmap_reg[6]_i_139_O_UNCONNECTED [3:1],\Xmap_reg[4]_5 }),
        .S({\Xmap[6]_i_214_n_0 ,\Xmap[6]_i_215_n_0 ,\Xmap[6]_i_216_n_0 ,\Xmap[6]_i_217_n_0 }));
  CARRY4 \Xmap_reg[6]_i_14 
       (.CI(\Xmap_reg[6]_i_28_n_0 ),
        .CO({\Xmap_reg[6]_i_14_n_0 ,\Xmap_reg[6]_i_14_n_1 ,\Xmap_reg[6]_i_14_n_2 ,\Xmap_reg[6]_i_14_n_3 }),
        .CYINIT(1'b0),
        .DI(DI),
        .O(\NLW_Xmap_reg[6]_i_14_O_UNCONNECTED [3:0]),
        .S(\cnt_reg[2]_1 ));
  CARRY4 \Xmap_reg[6]_i_146 
       (.CI(\Xmap_reg[6]_i_207_n_0 ),
        .CO({\Xmap_reg[6]_i_146_n_0 ,\Xmap_reg[6]_i_146_n_1 ,\Xmap_reg[6]_i_146_n_2 ,\Xmap_reg[6]_i_146_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[4]_i_14_n_0 ,\Ymap[4]_i_15_n_0 ,\Ymap[4]_i_16_n_0 ,\Ymap[4]_i_17_n_0 }),
        .O(\Xmap_reg[4]_9 ),
        .S({\Xmap[6]_i_218_n_0 ,\Xmap[6]_i_219_n_0 ,\Xmap[6]_i_220_n_0 ,\Xmap[6]_i_221_n_0 }));
  CARRY4 \Xmap_reg[6]_i_177 
       (.CI(\Xmap_reg[6]_i_222_n_0 ),
        .CO({\Xmap_reg[6]_i_177_n_0 ,\Xmap_reg[6]_i_177_n_1 ,\Xmap_reg[6]_i_177_n_2 ,\Xmap_reg[6]_i_177_n_3 }),
        .CYINIT(1'b0),
        .DI({\Xmap[6]_i_223_n_0 ,\Xmap[6]_i_76_n_0 ,\Xmap[6]_i_77_n_0 ,\Xmap[6]_i_78_n_0 }),
        .O(\Xmap_reg[4]_13 ),
        .S({\Xmap[6]_i_224_n_0 ,\Xmap[6]_i_225_n_0 ,\Xmap[6]_i_226_n_0 ,\Xmap[6]_i_227_n_0 }));
  CARRY4 \Xmap_reg[6]_i_199 
       (.CI(\Xmap_reg[6]_i_228_n_0 ),
        .CO({\Xmap_reg[6]_i_199_n_0 ,\Xmap_reg[6]_i_199_n_1 ,\Xmap_reg[6]_i_199_n_2 ,\Xmap_reg[6]_i_199_n_3 }),
        .CYINIT(1'b0),
        .DI(\Xmap_reg[4]_7 ),
        .O({\Xmap_reg[6]_i_199_n_4 ,\Xmap_reg[6]_i_199_n_5 ,\Xmap_reg[6]_i_199_n_6 ,\NLW_Xmap_reg[6]_i_199_O_UNCONNECTED [0]}),
        .S(\cnt_reg[2]_0 ));
  CARRY4 \Xmap_reg[6]_i_207 
       (.CI(\Xmap_reg[6]_i_229_n_0 ),
        .CO({\Xmap_reg[6]_i_207_n_0 ,\Xmap_reg[6]_i_207_n_1 ,\Xmap_reg[6]_i_207_n_2 ,\Xmap_reg[6]_i_207_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[0]_i_36_n_0 ,\Ymap[0]_i_37_n_0 ,\Xmap[6]_i_55_n_0 ,\Ymap[0]_i_39_n_0 }),
        .O(\Xmap_reg[4]_8 ),
        .S({\Xmap[6]_i_234_n_0 ,\Xmap[6]_i_235_n_0 ,\Xmap[6]_i_236_n_0 ,\Xmap[6]_i_237_n_0 }));
  CARRY4 \Xmap_reg[6]_i_222 
       (.CI(\Xmap_reg[6]_i_238_n_0 ),
        .CO({\Xmap_reg[6]_i_222_n_0 ,\Xmap_reg[6]_i_222_n_1 ,\Xmap_reg[6]_i_222_n_2 ,\Xmap_reg[6]_i_222_n_3 }),
        .CYINIT(1'b0),
        .DI({\Xmap[6]_i_68_n_0 ,\Xmap[6]_i_69_n_0 ,\Xmap[6]_i_70_n_0 ,\Xmap[6]_i_71_n_0 }),
        .O(\Xmap_reg[4]_12 ),
        .S({\Xmap[6]_i_239_n_0 ,\Xmap[6]_i_240_n_0 ,\Xmap[6]_i_241_n_0 ,\Xmap[6]_i_242_n_0 }));
  CARRY4 \Xmap_reg[6]_i_228 
       (.CI(1'b0),
        .CO({\Xmap_reg[6]_i_228_n_0 ,\Xmap_reg[6]_i_228_n_1 ,\Xmap_reg[6]_i_228_n_2 ,\Xmap_reg[6]_i_228_n_3 }),
        .CYINIT(1'b0),
        .DI({\Xmap_reg[4]_6 ,\Xmap_reg[6]_i_243_n_7 }),
        .O(\NLW_Xmap_reg[6]_i_228_O_UNCONNECTED [3:0]),
        .S({S,\Xmap[6]_i_247_n_0 }));
  CARRY4 \Xmap_reg[6]_i_229 
       (.CI(\Xmap_reg[6]_i_243_n_0 ),
        .CO({\Xmap_reg[6]_i_229_n_0 ,\Xmap_reg[6]_i_229_n_1 ,\Xmap_reg[6]_i_229_n_2 ,\Xmap_reg[6]_i_229_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[0]_i_73_n_0 ,\Ymap[0]_i_74_n_0 ,\Ymap[0]_i_75_n_0 ,\Ymap[0]_i_76_n_0 }),
        .O(\Xmap_reg[4]_7 ),
        .S({\Xmap[6]_i_248_n_0 ,\Xmap[6]_i_249_n_0 ,\Xmap[6]_i_250_n_0 ,\Xmap[6]_i_251_n_0 }));
  CARRY4 \Xmap_reg[6]_i_238 
       (.CI(\Xmap_reg[6]_i_106_n_0 ),
        .CO({\Xmap_reg[6]_i_238_n_0 ,\Xmap_reg[6]_i_238_n_1 ,\Xmap_reg[6]_i_238_n_2 ,\Xmap_reg[6]_i_238_n_3 }),
        .CYINIT(1'b0),
        .DI({\Xmap[6]_i_118_n_0 ,\Xmap[6]_i_119_n_0 ,\Xmap[6]_i_252_n_0 ,\cnt_reg_n_0_[2] }),
        .O(\Xmap_reg[4]_11 ),
        .S({\Xmap[6]_i_253_n_0 ,\Xmap[6]_i_254_n_0 ,\Xmap[6]_i_255_n_0 ,\Xmap[6]_i_256_n_0 }));
  CARRY4 \Xmap_reg[6]_i_243 
       (.CI(\Xmap_reg[6]_i_139_n_0 ),
        .CO({\Xmap_reg[6]_i_243_n_0 ,\Xmap_reg[6]_i_243_n_1 ,\Xmap_reg[6]_i_243_n_2 ,\Xmap_reg[6]_i_243_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[0]_i_106_n_0 ,\Ymap[0]_i_107_n_0 ,\Xmap[6]_i_140_n_0 ,\Xmap[6]_i_141_n_0 }),
        .O({\Xmap_reg[4]_6 ,\Xmap_reg[6]_i_243_n_7 }),
        .S({\Xmap[6]_i_257_n_0 ,\Xmap[6]_i_258_n_0 ,\Xmap[6]_i_259_n_0 ,\Xmap[6]_i_260_n_0 }));
  CARRY4 \Xmap_reg[6]_i_28 
       (.CI(\Xmap_reg[6]_i_43_n_0 ),
        .CO({\Xmap_reg[6]_i_28_n_0 ,\Xmap_reg[6]_i_28_n_1 ,\Xmap_reg[6]_i_28_n_2 ,\Xmap_reg[6]_i_28_n_3 }),
        .CYINIT(1'b0),
        .DI({\Xmap[6]_i_44_n_0 ,\Xmap[6]_i_45_n_0 ,\Xmap[6]_i_46_n_0 ,\Xmap[6]_i_47_n_0 }),
        .O(\NLW_Xmap_reg[6]_i_28_O_UNCONNECTED [3:0]),
        .S(\cnt_reg[1]_1 ));
  CARRY4 \Xmap_reg[6]_i_3 
       (.CI(\Xmap_reg[3]_i_1_n_0 ),
        .CO({\NLW_Xmap_reg[6]_i_3_CO_UNCONNECTED [3],\Xmap_reg[6]_i_3_n_1 ,\Xmap_reg[6]_i_3_n_2 ,\Xmap_reg[6]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,\cnt_reg_n_0_[6] ,\cnt_reg_n_0_[5] ,\cnt_reg_n_0_[4] }),
        .O({\Xmap_reg[6]_i_3_n_4 ,\Xmap_reg[6]_i_3_n_5 ,\Xmap_reg[6]_i_3_n_6 ,\Xmap_reg[6]_i_3_n_7 }),
        .S({\Xmap[6]_i_4_n_0 ,\Xmap[6]_i_5_n_0 ,\Xmap[6]_i_6_n_0 ,\Xmap[6]_i_7_n_0 }));
  CARRY4 \Xmap_reg[6]_i_37 
       (.CI(\Xmap_reg[6]_i_52_n_0 ),
        .CO({\Xmap_reg[6]_i_37_n_0 ,\Xmap_reg[6]_i_37_n_1 ,\Xmap_reg[6]_i_37_n_2 ,\Xmap_reg[6]_i_37_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[0]_i_36_n_0 ,\Ymap[0]_i_37_n_0 ,\Xmap[6]_i_55_n_0 ,\Ymap[0]_i_39_n_0 }),
        .O(\Xmap_reg[4]_16 ),
        .S({\Xmap[6]_i_56_n_0 ,\Xmap[6]_i_57_n_0 ,\Xmap[6]_i_58_n_0 ,\Xmap[6]_i_59_n_0 }));
  CARRY4 \Xmap_reg[6]_i_38 
       (.CI(\Xmap_reg[6]_i_53_n_0 ),
        .CO({\Xmap_reg[6]_i_38_n_0 ,\Xmap_reg[6]_i_38_n_1 ,\Xmap_reg[6]_i_38_n_2 ,\Xmap_reg[6]_i_38_n_3 }),
        .CYINIT(1'b0),
        .DI({\Xmap[6]_i_60_n_0 ,\Xmap[6]_i_61_n_0 ,\Xmap[6]_i_62_n_0 ,\Xmap[6]_i_63_n_0 }),
        .O(\Xmap_reg[4]_22 ),
        .S({\Xmap[6]_i_64_n_0 ,\Xmap[6]_i_65_n_0 ,\Xmap[6]_i_66_n_0 ,\Xmap[6]_i_67_n_0 }));
  CARRY4 \Xmap_reg[6]_i_39 
       (.CI(\Xmap_reg[6]_i_54_n_0 ),
        .CO({\Xmap_reg[6]_i_39_n_0 ,\Xmap_reg[6]_i_39_n_1 ,\Xmap_reg[6]_i_39_n_2 ,\Xmap_reg[6]_i_39_n_3 }),
        .CYINIT(1'b0),
        .DI({\Xmap[6]_i_68_n_0 ,\Xmap[6]_i_69_n_0 ,\Xmap[6]_i_70_n_0 ,\Xmap[6]_i_71_n_0 }),
        .O(\Xmap_reg[4]_19 ),
        .S({\Xmap[6]_i_72_n_0 ,\Xmap[6]_i_73_n_0 ,\Xmap[6]_i_74_n_0 ,\Xmap[6]_i_75_n_0 }));
  CARRY4 \Xmap_reg[6]_i_40 
       (.CI(\Xmap_reg[6]_i_39_n_0 ),
        .CO({\NLW_Xmap_reg[6]_i_40_CO_UNCONNECTED [3],\Xmap_reg[6]_i_40_n_1 ,\Xmap_reg[6]_i_40_n_2 ,\Xmap_reg[6]_i_40_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,\Xmap[6]_i_76_n_0 ,\Xmap[6]_i_77_n_0 ,\Xmap[6]_i_78_n_0 }),
        .O(\Xmap_reg[4]_20 ),
        .S({\Xmap[6]_i_79_n_0 ,\Xmap[6]_i_80_n_0 ,\Xmap[6]_i_81_n_0 ,\Xmap[6]_i_82_n_0 }));
  CARRY4 \Xmap_reg[6]_i_41 
       (.CI(\Xmap_reg[6]_i_38_n_0 ),
        .CO({\NLW_Xmap_reg[6]_i_41_CO_UNCONNECTED [3],\Xmap_reg[6]_i_41_n_1 ,\Xmap_reg[6]_i_41_n_2 ,\Xmap_reg[6]_i_41_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,\Xmap[6]_i_83_n_0 ,\Xmap[6]_i_84_n_0 ,\Xmap[6]_i_85_n_0 }),
        .O(\Xmap_reg[4]_23 ),
        .S({\Xmap[6]_i_86_n_0 ,\Xmap[6]_i_87_n_0 ,\Xmap[6]_i_88_n_0 ,\Xmap[6]_i_89_n_0 }));
  CARRY4 \Xmap_reg[6]_i_42 
       (.CI(\Xmap_reg[6]_i_37_n_0 ),
        .CO({\NLW_Xmap_reg[6]_i_42_CO_UNCONNECTED [3],\Xmap_reg[6]_i_42_n_1 ,\Xmap_reg[6]_i_42_n_2 ,\Xmap_reg[6]_i_42_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,\Ymap[4]_i_15_n_0 ,\Ymap[4]_i_16_n_0 ,\Ymap[4]_i_17_n_0 }),
        .O(\Xmap_reg[4]_17 ),
        .S({\Xmap[6]_i_90_n_0 ,\Xmap[6]_i_91_n_0 ,\Xmap[6]_i_92_n_0 ,\Xmap[6]_i_93_n_0 }));
  CARRY4 \Xmap_reg[6]_i_43 
       (.CI(\Xmap_reg[6]_i_94_n_0 ),
        .CO({\Xmap_reg[6]_i_43_n_0 ,\Xmap_reg[6]_i_43_n_1 ,\Xmap_reg[6]_i_43_n_2 ,\Xmap_reg[6]_i_43_n_3 }),
        .CYINIT(1'b0),
        .DI({\Xmap[6]_i_95_n_0 ,\Xmap[6]_i_96_n_0 ,\Xmap[6]_i_97_n_0 ,\Xmap[6]_i_98_n_0 }),
        .O(\NLW_Xmap_reg[6]_i_43_O_UNCONNECTED [3:0]),
        .S(\cnt_reg[1]_0 ));
  CARRY4 \Xmap_reg[6]_i_52 
       (.CI(\Xmap_reg[6]_i_103_n_0 ),
        .CO({\Xmap_reg[6]_i_52_n_0 ,\Xmap_reg[6]_i_52_n_1 ,\Xmap_reg[6]_i_52_n_2 ,\Xmap_reg[6]_i_52_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[0]_i_73_n_0 ,\Ymap[0]_i_74_n_0 ,\Ymap[0]_i_75_n_0 ,\Ymap[0]_i_76_n_0 }),
        .O(\Xmap_reg[4]_15 ),
        .S({\Xmap[6]_i_107_n_0 ,\Xmap[6]_i_108_n_0 ,\Xmap[6]_i_109_n_0 ,\Xmap[6]_i_110_n_0 }));
  CARRY4 \Xmap_reg[6]_i_53 
       (.CI(\Xmap_reg[6]_i_104_n_0 ),
        .CO({\Xmap_reg[6]_i_53_n_0 ,\Xmap_reg[6]_i_53_n_1 ,\Xmap_reg[6]_i_53_n_2 ,\Xmap_reg[6]_i_53_n_3 }),
        .CYINIT(1'b0),
        .DI({\Xmap[6]_i_111_n_0 ,\Xmap[6]_i_112_n_0 ,\cnt_reg_n_0_[0] ,\Xmap_reg[4]_10 }),
        .O(\Xmap_reg[4]_21 ),
        .S({\Xmap[6]_i_114_n_0 ,\Xmap[6]_i_115_n_0 ,\Xmap[6]_i_116_n_0 ,\cnt_reg[20]_0 }));
  CARRY4 \Xmap_reg[6]_i_54 
       (.CI(\Xmap_reg[6]_i_105_n_0 ),
        .CO({\Xmap_reg[6]_i_54_n_0 ,\Xmap_reg[6]_i_54_n_1 ,\Xmap_reg[6]_i_54_n_2 ,\Xmap_reg[6]_i_54_n_3 }),
        .CYINIT(1'b0),
        .DI({\Xmap[6]_i_118_n_0 ,\Xmap[6]_i_119_n_0 ,\Xmap[6]_i_120_n_0 ,\cnt_reg_n_0_[2] }),
        .O(\Xmap_reg[4]_18 ),
        .S({\Xmap[6]_i_121_n_0 ,\Xmap[6]_i_122_n_0 ,\Xmap[6]_i_123_n_0 ,\Xmap[6]_i_124_n_0 }));
  CARRY4 \Xmap_reg[6]_i_9 
       (.CI(\Xmap_reg[6]_i_14_n_0 ),
        .CO({\Xmap_reg[6]_i_9_n_0 ,\Xmap_reg[6]_i_9_n_1 ,\Xmap_reg[6]_i_9_n_2 ,\Xmap_reg[6]_i_9_n_3 }),
        .CYINIT(1'b0),
        .DI(\cnt_reg[17]_1 ),
        .O({\Xmap_reg[4]_24 ,\NLW_Xmap_reg[6]_i_9_O_UNCONNECTED [2:0]}),
        .S(\cnt_reg[2]_2 ));
  CARRY4 \Xmap_reg[6]_i_94 
       (.CI(1'b0),
        .CO({\Xmap_reg[6]_i_94_n_0 ,\Xmap_reg[6]_i_94_n_1 ,\Xmap_reg[6]_i_94_n_2 ,\Xmap_reg[6]_i_94_n_3 }),
        .CYINIT(1'b0),
        .DI({\Xmap[6]_i_130_n_0 ,\Xmap[6]_i_131_n_0 ,\Xmap[6]_i_132_n_0 ,1'b0}),
        .O(\NLW_Xmap_reg[6]_i_94_O_UNCONNECTED [3:0]),
        .S({\Xmap[6]_i_133_n_0 ,\Xmap[6]_i_134_n_0 ,\Xmap[6]_i_135_n_0 ,\Xmap[6]_i_136_n_0 }));
  LUT5 #(
    .INIT(32'hF4F5B0A0)) 
    \Ymap[0]_i_1 
       (.I0(\Ymap_reg[5]_i_2_n_1 ),
        .I1(\cnt_reg_n_0_[30] ),
        .I2(\Ymap_reg[0]_i_2_n_4 ),
        .I3(\Ymap_reg[5]_i_4_n_6 ),
        .I4(\Ymap_reg[3]_i_2_n_7 ),
        .O(\Ymap[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[0]_i_10 
       (.I0(\Ymap_reg[0]_i_23_n_6 ),
        .I1(\Ymap_reg[0]_i_22_n_6 ),
        .I2(\Ymap_reg[0]_i_21_n_6 ),
        .I3(\Ymap_reg[0]_i_21_n_5 ),
        .I4(\Ymap_reg[0]_i_22_n_5 ),
        .I5(\Ymap_reg[0]_i_23_n_5 ),
        .O(\Ymap[0]_i_10_n_0 ));
  LUT4 #(
    .INIT(16'h8778)) 
    \Ymap[0]_i_100 
       (.I0(\Ymap_reg[0]_i_136_n_5 ),
        .I1(\cnt_reg_n_0_[1] ),
        .I2(\cnt_reg_n_0_[2] ),
        .I3(\Ymap_reg[0]_i_136_n_4 ),
        .O(\Ymap[0]_i_100_n_0 ));
  LUT4 #(
    .INIT(16'h8778)) 
    \Ymap[0]_i_101 
       (.I0(\Ymap_reg[0]_i_136_n_6 ),
        .I1(\cnt_reg_n_0_[0] ),
        .I2(\cnt_reg_n_0_[1] ),
        .I3(\Ymap_reg[0]_i_136_n_5 ),
        .O(\Ymap[0]_i_101_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[0]_i_102 
       (.I0(\cnt_reg_n_0_[0] ),
        .I1(\Ymap_reg[0]_i_136_n_6 ),
        .O(\Ymap[0]_i_102_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[0]_i_106 
       (.I0(\cnt_reg_n_0_[9] ),
        .I1(\cnt_reg_n_0_[4] ),
        .I2(\cnt_reg_n_0_[6] ),
        .O(\Ymap[0]_i_106_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[0]_i_107 
       (.I0(\cnt_reg_n_0_[8] ),
        .I1(\cnt_reg_n_0_[3] ),
        .I2(\cnt_reg_n_0_[5] ),
        .O(\Ymap[0]_i_107_n_0 ));
  LUT3 #(
    .INIT(8'hD4)) 
    \Ymap[0]_i_108 
       (.I0(\cnt_reg_n_0_[4] ),
        .I1(\cnt_reg_n_0_[7] ),
        .I2(\cnt_reg_n_0_[2] ),
        .O(\Ymap[0]_i_108_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[0]_i_109 
       (.I0(\cnt_reg_n_0_[6] ),
        .I1(\cnt_reg_n_0_[1] ),
        .I2(\cnt_reg_n_0_[3] ),
        .O(\Ymap[0]_i_109_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[0]_i_11 
       (.I0(\Ymap_reg[0]_i_23_n_7 ),
        .I1(\Ymap_reg[0]_i_22_n_7 ),
        .I2(\Ymap_reg[0]_i_21_n_7 ),
        .I3(\Ymap_reg[0]_i_21_n_6 ),
        .I4(\Ymap_reg[0]_i_22_n_6 ),
        .I5(\Ymap_reg[0]_i_23_n_6 ),
        .O(\Ymap[0]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_110 
       (.I0(\cnt_reg_n_0_[6] ),
        .I1(\cnt_reg_n_0_[4] ),
        .I2(\cnt_reg_n_0_[9] ),
        .I3(\cnt_reg_n_0_[5] ),
        .I4(\cnt_reg_n_0_[7] ),
        .I5(\cnt_reg_n_0_[10] ),
        .O(\Ymap[0]_i_110_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_111 
       (.I0(\cnt_reg_n_0_[5] ),
        .I1(\cnt_reg_n_0_[3] ),
        .I2(\cnt_reg_n_0_[8] ),
        .I3(\cnt_reg_n_0_[9] ),
        .I4(\cnt_reg_n_0_[4] ),
        .I5(\cnt_reg_n_0_[6] ),
        .O(\Ymap[0]_i_111_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Ymap[0]_i_112 
       (.I0(\cnt_reg_n_0_[2] ),
        .I1(\cnt_reg_n_0_[7] ),
        .I2(\cnt_reg_n_0_[4] ),
        .I3(\cnt_reg_n_0_[8] ),
        .I4(\cnt_reg_n_0_[3] ),
        .I5(\cnt_reg_n_0_[5] ),
        .O(\Ymap[0]_i_112_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_113 
       (.I0(\cnt_reg_n_0_[3] ),
        .I1(\cnt_reg_n_0_[1] ),
        .I2(\cnt_reg_n_0_[6] ),
        .I3(\cnt_reg_n_0_[2] ),
        .I4(\cnt_reg_n_0_[4] ),
        .I5(\cnt_reg_n_0_[7] ),
        .O(\Ymap[0]_i_113_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \Ymap[0]_i_114 
       (.I0(\cnt_reg_n_0_[1] ),
        .I1(\cnt_reg_n_0_[3] ),
        .O(\Ymap[0]_i_114_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \Ymap[0]_i_115 
       (.I0(\cnt_reg_n_0_[0] ),
        .I1(\cnt_reg_n_0_[2] ),
        .O(\Ymap[0]_i_115_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \Ymap[0]_i_116 
       (.I0(\cnt_reg_n_0_[1] ),
        .O(\Ymap[0]_i_116_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \Ymap[0]_i_117 
       (.I0(\cnt_reg_n_0_[0] ),
        .O(\Ymap[0]_i_117_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[0]_i_119 
       (.I0(\Ymap_reg[0]_i_118_n_4 ),
        .I1(\Ymap_reg[0]_i_131_n_4 ),
        .O(\Ymap[0]_i_119_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[0]_i_120 
       (.I0(\Ymap_reg[0]_i_118_n_5 ),
        .I1(\Ymap_reg[0]_i_131_n_5 ),
        .O(\Ymap[0]_i_120_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[0]_i_121 
       (.I0(\Ymap_reg[0]_i_118_n_6 ),
        .I1(\Ymap_reg[0]_i_131_n_6 ),
        .O(\Ymap[0]_i_121_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[0]_i_122 
       (.I0(\Ymap_reg[0]_i_118_n_7 ),
        .I1(\Ymap_reg[0]_i_131_n_7 ),
        .O(\Ymap[0]_i_122_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \Ymap[0]_i_123 
       (.I0(\cnt_reg_n_0_[1] ),
        .I1(\cnt_reg_n_0_[3] ),
        .O(\Ymap[0]_i_123_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \Ymap[0]_i_124 
       (.I0(\cnt_reg_n_0_[0] ),
        .I1(\cnt_reg_n_0_[2] ),
        .O(\Ymap[0]_i_124_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \Ymap[0]_i_125 
       (.I0(\cnt_reg_n_0_[1] ),
        .O(\Ymap[0]_i_125_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \Ymap[0]_i_126 
       (.I0(\cnt_reg_n_0_[0] ),
        .O(\Ymap[0]_i_126_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_127 
       (.I0(\cnt_reg_n_0_[22] ),
        .I1(\cnt_reg_n_0_[20] ),
        .I2(\cnt_reg_n_0_[25] ),
        .I3(\cnt_reg_n_0_[21] ),
        .I4(\cnt_reg_n_0_[23] ),
        .I5(\cnt_reg_n_0_[26] ),
        .O(\Ymap[0]_i_127_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_128 
       (.I0(\cnt_reg_n_0_[21] ),
        .I1(\cnt_reg_n_0_[19] ),
        .I2(\cnt_reg_n_0_[24] ),
        .I3(\cnt_reg_n_0_[25] ),
        .I4(\cnt_reg_n_0_[20] ),
        .I5(\cnt_reg_n_0_[22] ),
        .O(\Ymap[0]_i_128_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_129 
       (.I0(\cnt_reg_n_0_[20] ),
        .I1(\cnt_reg_n_0_[18] ),
        .I2(\cnt_reg_n_0_[23] ),
        .I3(\cnt_reg_n_0_[24] ),
        .I4(\cnt_reg_n_0_[19] ),
        .I5(\cnt_reg_n_0_[21] ),
        .O(\Ymap[0]_i_129_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[0]_i_13 
       (.I0(\Ymap_reg[0]_i_33_n_4 ),
        .I1(\Ymap_reg[0]_i_34_n_4 ),
        .I2(\Ymap_reg[0]_i_35_n_4 ),
        .O(\Ymap[0]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_130 
       (.I0(\cnt_reg_n_0_[19] ),
        .I1(\cnt_reg_n_0_[17] ),
        .I2(\cnt_reg_n_0_[22] ),
        .I3(\cnt_reg_n_0_[18] ),
        .I4(\cnt_reg_n_0_[20] ),
        .I5(\cnt_reg_n_0_[23] ),
        .O(\Ymap[0]_i_130_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Ymap[0]_i_132 
       (.I0(\cnt_reg_n_0_[14] ),
        .I1(\cnt_reg_n_0_[16] ),
        .I2(\cnt_reg_n_0_[18] ),
        .I3(\cnt_reg_n_0_[19] ),
        .I4(\cnt_reg_n_0_[15] ),
        .I5(\cnt_reg_n_0_[17] ),
        .O(\Ymap[0]_i_132_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Ymap[0]_i_133 
       (.I0(\cnt_reg_n_0_[13] ),
        .I1(\cnt_reg_n_0_[15] ),
        .I2(\cnt_reg_n_0_[17] ),
        .I3(\cnt_reg_n_0_[14] ),
        .I4(\cnt_reg_n_0_[16] ),
        .I5(\cnt_reg_n_0_[18] ),
        .O(\Ymap[0]_i_133_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_134 
       (.I0(\cnt_reg_n_0_[16] ),
        .I1(\cnt_reg_n_0_[14] ),
        .I2(\cnt_reg_n_0_[12] ),
        .I3(\cnt_reg_n_0_[13] ),
        .I4(\cnt_reg_n_0_[15] ),
        .I5(\cnt_reg_n_0_[17] ),
        .O(\Ymap[0]_i_134_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_135 
       (.I0(\cnt_reg_n_0_[15] ),
        .I1(\cnt_reg_n_0_[13] ),
        .I2(\cnt_reg_n_0_[11] ),
        .I3(\cnt_reg_n_0_[16] ),
        .I4(\cnt_reg_n_0_[12] ),
        .I5(\cnt_reg_n_0_[14] ),
        .O(\Ymap[0]_i_135_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[0]_i_138 
       (.I0(\Ymap_reg[0]_i_137_n_4 ),
        .I1(\Ymap_reg[0]_i_159_n_4 ),
        .O(\Ymap[0]_i_138_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[0]_i_139 
       (.I0(\Ymap_reg[0]_i_137_n_5 ),
        .I1(\Ymap_reg[0]_i_159_n_5 ),
        .O(\Ymap[0]_i_139_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[0]_i_14 
       (.I0(\Ymap_reg[0]_i_33_n_5 ),
        .I1(\Ymap_reg[0]_i_34_n_5 ),
        .I2(\Ymap_reg[0]_i_35_n_5 ),
        .O(\Ymap[0]_i_14_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[0]_i_140 
       (.I0(\Ymap_reg[0]_i_137_n_6 ),
        .I1(\Ymap_reg[0]_i_159_n_6 ),
        .O(\Ymap[0]_i_140_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[0]_i_141 
       (.I0(\Ymap_reg[0]_i_137_n_7 ),
        .I1(\Ymap_reg[0]_i_159_n_7 ),
        .O(\Ymap[0]_i_141_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[0]_i_142 
       (.I0(\cnt_reg_n_0_[5] ),
        .I1(\cnt_reg_n_0_[0] ),
        .I2(\cnt_reg_n_0_[2] ),
        .O(\Ymap[0]_i_142_n_0 ));
  LUT3 #(
    .INIT(8'h69)) 
    \Ymap[0]_i_143 
       (.I0(\cnt_reg_n_0_[0] ),
        .I1(\cnt_reg_n_0_[2] ),
        .I2(\cnt_reg_n_0_[5] ),
        .O(\Ymap[0]_i_143_n_0 ));
  LUT2 #(
    .INIT(4'hB)) 
    \Ymap[0]_i_144 
       (.I0(\cnt_reg_n_0_[3] ),
        .I1(\cnt_reg_n_0_[0] ),
        .O(\Ymap[0]_i_144_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_145 
       (.I0(\cnt_reg_n_0_[2] ),
        .I1(\cnt_reg_n_0_[0] ),
        .I2(\cnt_reg_n_0_[5] ),
        .I3(\cnt_reg_n_0_[1] ),
        .I4(\cnt_reg_n_0_[3] ),
        .I5(\cnt_reg_n_0_[6] ),
        .O(\Ymap[0]_i_145_n_0 ));
  LUT5 #(
    .INIT(32'h69966969)) 
    \Ymap[0]_i_146 
       (.I0(\cnt_reg_n_0_[0] ),
        .I1(\cnt_reg_n_0_[2] ),
        .I2(\cnt_reg_n_0_[5] ),
        .I3(\cnt_reg_n_0_[1] ),
        .I4(\cnt_reg_n_0_[4] ),
        .O(\Ymap[0]_i_146_n_0 ));
  LUT4 #(
    .INIT(16'h2DD2)) 
    \Ymap[0]_i_147 
       (.I0(\cnt_reg_n_0_[0] ),
        .I1(\cnt_reg_n_0_[3] ),
        .I2(\cnt_reg_n_0_[1] ),
        .I3(\cnt_reg_n_0_[4] ),
        .O(\Ymap[0]_i_147_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[0]_i_148 
       (.I0(\cnt_reg_n_0_[3] ),
        .I1(\cnt_reg_n_0_[0] ),
        .O(\Ymap[0]_i_148_n_0 ));
  LUT3 #(
    .INIT(8'h69)) 
    \Ymap[0]_i_149 
       (.I0(\cnt_reg_n_0_[0] ),
        .I1(\cnt_reg_n_0_[2] ),
        .I2(\cnt_reg_n_0_[5] ),
        .O(\Ymap[0]_i_149_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[0]_i_15 
       (.I0(\Ymap_reg[0]_i_33_n_6 ),
        .I1(\Ymap_reg[0]_i_34_n_6 ),
        .I2(\Ymap_reg[0]_i_35_n_6 ),
        .O(\Ymap[0]_i_15_n_0 ));
  LUT2 #(
    .INIT(4'hB)) 
    \Ymap[0]_i_150 
       (.I0(\cnt_reg_n_0_[3] ),
        .I1(\cnt_reg_n_0_[0] ),
        .O(\Ymap[0]_i_150_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_151 
       (.I0(\cnt_reg_n_0_[2] ),
        .I1(\cnt_reg_n_0_[0] ),
        .I2(\cnt_reg_n_0_[5] ),
        .I3(\cnt_reg_n_0_[1] ),
        .I4(\cnt_reg_n_0_[3] ),
        .I5(\cnt_reg_n_0_[6] ),
        .O(\Ymap[0]_i_151_n_0 ));
  LUT5 #(
    .INIT(32'h69966969)) 
    \Ymap[0]_i_152 
       (.I0(\cnt_reg_n_0_[0] ),
        .I1(\cnt_reg_n_0_[2] ),
        .I2(\cnt_reg_n_0_[5] ),
        .I3(\cnt_reg_n_0_[1] ),
        .I4(\cnt_reg_n_0_[4] ),
        .O(\Ymap[0]_i_152_n_0 ));
  LUT4 #(
    .INIT(16'h2DD2)) 
    \Ymap[0]_i_153 
       (.I0(\cnt_reg_n_0_[0] ),
        .I1(\cnt_reg_n_0_[3] ),
        .I2(\cnt_reg_n_0_[1] ),
        .I3(\cnt_reg_n_0_[4] ),
        .O(\Ymap[0]_i_153_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[0]_i_154 
       (.I0(\cnt_reg_n_0_[3] ),
        .I1(\cnt_reg_n_0_[0] ),
        .O(\Ymap[0]_i_154_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_155 
       (.I0(\cnt_reg_n_0_[18] ),
        .I1(\cnt_reg_n_0_[16] ),
        .I2(\cnt_reg_n_0_[21] ),
        .I3(\cnt_reg_n_0_[17] ),
        .I4(\cnt_reg_n_0_[19] ),
        .I5(\cnt_reg_n_0_[22] ),
        .O(\Ymap[0]_i_155_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_156 
       (.I0(\cnt_reg_n_0_[17] ),
        .I1(\cnt_reg_n_0_[15] ),
        .I2(\cnt_reg_n_0_[20] ),
        .I3(\cnt_reg_n_0_[21] ),
        .I4(\cnt_reg_n_0_[16] ),
        .I5(\cnt_reg_n_0_[18] ),
        .O(\Ymap[0]_i_156_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_157 
       (.I0(\cnt_reg_n_0_[16] ),
        .I1(\cnt_reg_n_0_[14] ),
        .I2(\cnt_reg_n_0_[19] ),
        .I3(\cnt_reg_n_0_[20] ),
        .I4(\cnt_reg_n_0_[15] ),
        .I5(\cnt_reg_n_0_[17] ),
        .O(\Ymap[0]_i_157_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_158 
       (.I0(\cnt_reg_n_0_[15] ),
        .I1(\cnt_reg_n_0_[13] ),
        .I2(\cnt_reg_n_0_[18] ),
        .I3(\cnt_reg_n_0_[14] ),
        .I4(\cnt_reg_n_0_[16] ),
        .I5(\cnt_reg_n_0_[19] ),
        .O(\Ymap[0]_i_158_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[0]_i_16 
       (.I0(\Ymap_reg[0]_i_33_n_7 ),
        .I1(\Ymap_reg[0]_i_34_n_7 ),
        .I2(\Ymap_reg[0]_i_35_n_7 ),
        .O(\Ymap[0]_i_16_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_160 
       (.I0(\cnt_reg_n_0_[14] ),
        .I1(\cnt_reg_n_0_[10] ),
        .I2(\cnt_reg_n_0_[12] ),
        .I3(\cnt_reg_n_0_[15] ),
        .I4(\cnt_reg_n_0_[11] ),
        .I5(\cnt_reg_n_0_[13] ),
        .O(\Ymap[0]_i_160_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_161 
       (.I0(\cnt_reg_n_0_[13] ),
        .I1(\cnt_reg_n_0_[11] ),
        .I2(\cnt_reg_n_0_[9] ),
        .I3(\cnt_reg_n_0_[10] ),
        .I4(\cnt_reg_n_0_[12] ),
        .I5(\cnt_reg_n_0_[14] ),
        .O(\Ymap[0]_i_161_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Ymap[0]_i_162 
       (.I0(\cnt_reg_n_0_[10] ),
        .I1(\cnt_reg_n_0_[8] ),
        .I2(\cnt_reg_n_0_[12] ),
        .I3(\cnt_reg_n_0_[9] ),
        .I4(\cnt_reg_n_0_[11] ),
        .I5(\cnt_reg_n_0_[13] ),
        .O(\Ymap[0]_i_162_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_163 
       (.I0(\cnt_reg_n_0_[11] ),
        .I1(\cnt_reg_n_0_[7] ),
        .I2(\cnt_reg_n_0_[9] ),
        .I3(\cnt_reg_n_0_[12] ),
        .I4(\cnt_reg_n_0_[8] ),
        .I5(\cnt_reg_n_0_[10] ),
        .O(\Ymap[0]_i_163_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[0]_i_166 
       (.I0(\Ymap_reg[0]_i_165_n_4 ),
        .I1(\Ymap_reg[0]_i_174_n_4 ),
        .O(\Ymap[0]_i_166_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[0]_i_167 
       (.I0(\Ymap_reg[0]_i_165_n_5 ),
        .I1(\Ymap_reg[0]_i_174_n_5 ),
        .O(\Ymap[0]_i_167_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[0]_i_168 
       (.I0(\Ymap_reg[0]_i_165_n_6 ),
        .I1(\Ymap_reg[0]_i_174_n_6 ),
        .O(\Ymap[0]_i_168_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[0]_i_169 
       (.I0(\Ymap_reg[0]_i_165_n_7 ),
        .I1(\Ymap_reg[0]_i_174_n_7 ),
        .O(\Ymap[0]_i_169_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[0]_i_17 
       (.I0(\Ymap_reg[0]_i_35_n_4 ),
        .I1(\Ymap_reg[0]_i_34_n_4 ),
        .I2(\Ymap_reg[0]_i_33_n_4 ),
        .I3(\Ymap_reg[0]_i_21_n_7 ),
        .I4(\Ymap_reg[0]_i_22_n_7 ),
        .I5(\Ymap_reg[0]_i_23_n_7 ),
        .O(\Ymap[0]_i_17_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_170 
       (.I0(\cnt_reg_n_0_[14] ),
        .I1(\cnt_reg_n_0_[12] ),
        .I2(\cnt_reg_n_0_[17] ),
        .I3(\cnt_reg_n_0_[13] ),
        .I4(\cnt_reg_n_0_[15] ),
        .I5(\cnt_reg_n_0_[18] ),
        .O(\Ymap[0]_i_170_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_171 
       (.I0(\cnt_reg_n_0_[13] ),
        .I1(\cnt_reg_n_0_[11] ),
        .I2(\cnt_reg_n_0_[16] ),
        .I3(\cnt_reg_n_0_[17] ),
        .I4(\cnt_reg_n_0_[12] ),
        .I5(\cnt_reg_n_0_[14] ),
        .O(\Ymap[0]_i_171_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Ymap[0]_i_172 
       (.I0(\cnt_reg_n_0_[10] ),
        .I1(\cnt_reg_n_0_[15] ),
        .I2(\cnt_reg_n_0_[12] ),
        .I3(\cnt_reg_n_0_[16] ),
        .I4(\cnt_reg_n_0_[11] ),
        .I5(\cnt_reg_n_0_[13] ),
        .O(\Ymap[0]_i_172_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_173 
       (.I0(\cnt_reg_n_0_[11] ),
        .I1(\cnt_reg_n_0_[9] ),
        .I2(\cnt_reg_n_0_[14] ),
        .I3(\cnt_reg_n_0_[10] ),
        .I4(\cnt_reg_n_0_[12] ),
        .I5(\cnt_reg_n_0_[15] ),
        .O(\Ymap[0]_i_173_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Ymap[0]_i_175 
       (.I0(\cnt_reg_n_0_[6] ),
        .I1(\cnt_reg_n_0_[8] ),
        .I2(\cnt_reg_n_0_[10] ),
        .I3(\cnt_reg_n_0_[11] ),
        .I4(\cnt_reg_n_0_[7] ),
        .I5(\cnt_reg_n_0_[9] ),
        .O(\Ymap[0]_i_175_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Ymap[0]_i_176 
       (.I0(\cnt_reg_n_0_[5] ),
        .I1(\cnt_reg_n_0_[7] ),
        .I2(\cnt_reg_n_0_[9] ),
        .I3(\cnt_reg_n_0_[6] ),
        .I4(\cnt_reg_n_0_[8] ),
        .I5(\cnt_reg_n_0_[10] ),
        .O(\Ymap[0]_i_176_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_177 
       (.I0(\cnt_reg_n_0_[8] ),
        .I1(\cnt_reg_n_0_[6] ),
        .I2(\cnt_reg_n_0_[4] ),
        .I3(\cnt_reg_n_0_[5] ),
        .I4(\cnt_reg_n_0_[7] ),
        .I5(\cnt_reg_n_0_[9] ),
        .O(\Ymap[0]_i_177_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_178 
       (.I0(\cnt_reg_n_0_[7] ),
        .I1(\cnt_reg_n_0_[5] ),
        .I2(\cnt_reg_n_0_[3] ),
        .I3(\cnt_reg_n_0_[8] ),
        .I4(\cnt_reg_n_0_[4] ),
        .I5(\cnt_reg_n_0_[6] ),
        .O(\Ymap[0]_i_178_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[0]_i_18 
       (.I0(\Ymap_reg[0]_i_35_n_5 ),
        .I1(\Ymap_reg[0]_i_34_n_5 ),
        .I2(\Ymap_reg[0]_i_33_n_5 ),
        .I3(\Ymap_reg[0]_i_33_n_4 ),
        .I4(\Ymap_reg[0]_i_34_n_4 ),
        .I5(\Ymap_reg[0]_i_35_n_4 ),
        .O(\Ymap[0]_i_18_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[0]_i_180 
       (.I0(\Ymap_reg[0]_i_179_n_4 ),
        .I1(\Ymap_reg[0]_i_72_n_4 ),
        .O(\Ymap[0]_i_180_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[0]_i_181 
       (.I0(\Ymap_reg[0]_i_179_n_5 ),
        .I1(\Ymap_reg[0]_i_72_n_5 ),
        .O(\Ymap[0]_i_181_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[0]_i_182 
       (.I0(\Ymap_reg[0]_i_179_n_6 ),
        .I1(\Ymap_reg[0]_i_72_n_6 ),
        .O(\Ymap[0]_i_182_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[0]_i_183 
       (.I0(\Ymap_reg[0]_i_179_n_7 ),
        .I1(\cnt_reg_n_0_[0] ),
        .O(\Ymap[0]_i_183_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_184 
       (.I0(\cnt_reg_n_0_[10] ),
        .I1(\cnt_reg_n_0_[8] ),
        .I2(\cnt_reg_n_0_[13] ),
        .I3(\cnt_reg_n_0_[9] ),
        .I4(\cnt_reg_n_0_[11] ),
        .I5(\cnt_reg_n_0_[14] ),
        .O(\Ymap[0]_i_184_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_185 
       (.I0(\cnt_reg_n_0_[9] ),
        .I1(\cnt_reg_n_0_[7] ),
        .I2(\cnt_reg_n_0_[12] ),
        .I3(\cnt_reg_n_0_[13] ),
        .I4(\cnt_reg_n_0_[8] ),
        .I5(\cnt_reg_n_0_[10] ),
        .O(\Ymap[0]_i_185_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_186 
       (.I0(\cnt_reg_n_0_[8] ),
        .I1(\cnt_reg_n_0_[6] ),
        .I2(\cnt_reg_n_0_[11] ),
        .I3(\cnt_reg_n_0_[12] ),
        .I4(\cnt_reg_n_0_[7] ),
        .I5(\cnt_reg_n_0_[9] ),
        .O(\Ymap[0]_i_186_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_187 
       (.I0(\cnt_reg_n_0_[7] ),
        .I1(\cnt_reg_n_0_[5] ),
        .I2(\cnt_reg_n_0_[10] ),
        .I3(\cnt_reg_n_0_[6] ),
        .I4(\cnt_reg_n_0_[8] ),
        .I5(\cnt_reg_n_0_[11] ),
        .O(\Ymap[0]_i_187_n_0 ));
  LUT3 #(
    .INIT(8'h69)) 
    \Ymap[0]_i_188 
       (.I0(\cnt_reg_n_0_[1] ),
        .I1(\cnt_reg_n_0_[3] ),
        .I2(\cnt_reg_n_0_[5] ),
        .O(\Ymap[0]_i_188_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_189 
       (.I0(\cnt_reg_n_0_[6] ),
        .I1(\cnt_reg_n_0_[2] ),
        .I2(\cnt_reg_n_0_[4] ),
        .I3(\cnt_reg_n_0_[7] ),
        .I4(\cnt_reg_n_0_[3] ),
        .I5(\cnt_reg_n_0_[5] ),
        .O(\Ymap[0]_i_189_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[0]_i_19 
       (.I0(\Ymap_reg[0]_i_35_n_6 ),
        .I1(\Ymap_reg[0]_i_34_n_6 ),
        .I2(\Ymap_reg[0]_i_33_n_6 ),
        .I3(\Ymap_reg[0]_i_33_n_5 ),
        .I4(\Ymap_reg[0]_i_34_n_5 ),
        .I5(\Ymap_reg[0]_i_35_n_5 ),
        .O(\Ymap[0]_i_19_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_190 
       (.I0(\cnt_reg_n_0_[5] ),
        .I1(\cnt_reg_n_0_[3] ),
        .I2(\cnt_reg_n_0_[1] ),
        .I3(\cnt_reg_n_0_[2] ),
        .I4(\cnt_reg_n_0_[4] ),
        .I5(\cnt_reg_n_0_[6] ),
        .O(\Ymap[0]_i_190_n_0 ));
  LUT5 #(
    .INIT(32'h69966969)) 
    \Ymap[0]_i_191 
       (.I0(\cnt_reg_n_0_[1] ),
        .I1(\cnt_reg_n_0_[3] ),
        .I2(\cnt_reg_n_0_[5] ),
        .I3(\cnt_reg_n_0_[4] ),
        .I4(\cnt_reg_n_0_[0] ),
        .O(\Ymap[0]_i_191_n_0 ));
  LUT3 #(
    .INIT(8'h69)) 
    \Ymap[0]_i_192 
       (.I0(\cnt_reg_n_0_[0] ),
        .I1(\cnt_reg_n_0_[4] ),
        .I2(\cnt_reg_n_0_[2] ),
        .O(\Ymap[0]_i_192_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_193 
       (.I0(\cnt_reg_n_0_[6] ),
        .I1(\cnt_reg_n_0_[4] ),
        .I2(\cnt_reg_n_0_[9] ),
        .I3(\cnt_reg_n_0_[5] ),
        .I4(\cnt_reg_n_0_[7] ),
        .I5(\cnt_reg_n_0_[10] ),
        .O(\Ymap[0]_i_193_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_194 
       (.I0(\cnt_reg_n_0_[5] ),
        .I1(\cnt_reg_n_0_[3] ),
        .I2(\cnt_reg_n_0_[8] ),
        .I3(\cnt_reg_n_0_[9] ),
        .I4(\cnt_reg_n_0_[4] ),
        .I5(\cnt_reg_n_0_[6] ),
        .O(\Ymap[0]_i_194_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Ymap[0]_i_195 
       (.I0(\cnt_reg_n_0_[2] ),
        .I1(\cnt_reg_n_0_[7] ),
        .I2(\cnt_reg_n_0_[4] ),
        .I3(\cnt_reg_n_0_[8] ),
        .I4(\cnt_reg_n_0_[3] ),
        .I5(\cnt_reg_n_0_[5] ),
        .O(\Ymap[0]_i_195_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_196 
       (.I0(\cnt_reg_n_0_[3] ),
        .I1(\cnt_reg_n_0_[1] ),
        .I2(\cnt_reg_n_0_[6] ),
        .I3(\cnt_reg_n_0_[2] ),
        .I4(\cnt_reg_n_0_[4] ),
        .I5(\cnt_reg_n_0_[7] ),
        .O(\Ymap[0]_i_196_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[0]_i_20 
       (.I0(\Ymap_reg[0]_i_35_n_7 ),
        .I1(\Ymap_reg[0]_i_34_n_7 ),
        .I2(\Ymap_reg[0]_i_33_n_7 ),
        .I3(\Ymap_reg[0]_i_33_n_6 ),
        .I4(\Ymap_reg[0]_i_34_n_6 ),
        .I5(\Ymap_reg[0]_i_35_n_6 ),
        .O(\Ymap[0]_i_20_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[0]_i_25 
       (.I0(\Ymap_reg[0]_i_69_n_4 ),
        .I1(\Ymap_reg[0]_i_70_n_4 ),
        .I2(\Ymap_reg[0]_i_71_n_4 ),
        .O(\Ymap[0]_i_25_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[0]_i_26 
       (.I0(\Ymap_reg[0]_i_69_n_5 ),
        .I1(\Ymap_reg[0]_i_70_n_5 ),
        .I2(\Ymap_reg[0]_i_71_n_5 ),
        .O(\Ymap[0]_i_26_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[0]_i_27 
       (.I0(\Ymap_reg[0]_i_69_n_6 ),
        .I1(\Ymap_reg[0]_i_70_n_6 ),
        .I2(\Ymap_reg[0]_i_71_n_6 ),
        .O(\Ymap[0]_i_27_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[0]_i_28 
       (.I0(\Ymap_reg[0]_i_69_n_7 ),
        .I1(\Ymap_reg[0]_i_72_n_7 ),
        .I2(\Ymap_reg[0]_i_71_n_7 ),
        .O(\Ymap[0]_i_28_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[0]_i_29 
       (.I0(\Ymap_reg[0]_i_71_n_4 ),
        .I1(\Ymap_reg[0]_i_70_n_4 ),
        .I2(\Ymap_reg[0]_i_69_n_4 ),
        .I3(\Ymap_reg[0]_i_33_n_7 ),
        .I4(\Ymap_reg[0]_i_34_n_7 ),
        .I5(\Ymap_reg[0]_i_35_n_7 ),
        .O(\Ymap[0]_i_29_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[0]_i_30 
       (.I0(\Ymap_reg[0]_i_71_n_5 ),
        .I1(\Ymap_reg[0]_i_70_n_5 ),
        .I2(\Ymap_reg[0]_i_69_n_5 ),
        .I3(\Ymap_reg[0]_i_69_n_4 ),
        .I4(\Ymap_reg[0]_i_70_n_4 ),
        .I5(\Ymap_reg[0]_i_71_n_4 ),
        .O(\Ymap[0]_i_30_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[0]_i_31 
       (.I0(\Ymap_reg[0]_i_71_n_6 ),
        .I1(\Ymap_reg[0]_i_70_n_6 ),
        .I2(\Ymap_reg[0]_i_69_n_6 ),
        .I3(\Ymap_reg[0]_i_69_n_5 ),
        .I4(\Ymap_reg[0]_i_70_n_5 ),
        .I5(\Ymap_reg[0]_i_71_n_5 ),
        .O(\Ymap[0]_i_31_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[0]_i_32 
       (.I0(\Ymap_reg[0]_i_71_n_7 ),
        .I1(\Ymap_reg[0]_i_72_n_7 ),
        .I2(\Ymap_reg[0]_i_69_n_7 ),
        .I3(\Ymap_reg[0]_i_69_n_6 ),
        .I4(\Ymap_reg[0]_i_70_n_6 ),
        .I5(\Ymap_reg[0]_i_71_n_6 ),
        .O(\Ymap[0]_i_32_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[0]_i_36 
       (.I0(\cnt_reg_n_0_[17] ),
        .I1(\cnt_reg_n_0_[12] ),
        .I2(\cnt_reg_n_0_[14] ),
        .O(\Ymap[0]_i_36_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[0]_i_37 
       (.I0(\cnt_reg_n_0_[16] ),
        .I1(\cnt_reg_n_0_[11] ),
        .I2(\cnt_reg_n_0_[13] ),
        .O(\Ymap[0]_i_37_n_0 ));
  LUT3 #(
    .INIT(8'hD4)) 
    \Ymap[0]_i_38 
       (.I0(\cnt_reg_n_0_[12] ),
        .I1(\cnt_reg_n_0_[15] ),
        .I2(\cnt_reg_n_0_[10] ),
        .O(\Ymap[0]_i_38_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[0]_i_39 
       (.I0(\cnt_reg_n_0_[14] ),
        .I1(\cnt_reg_n_0_[9] ),
        .I2(\cnt_reg_n_0_[11] ),
        .O(\Ymap[0]_i_39_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[0]_i_4 
       (.I0(\Ymap_reg[0]_i_21_n_4 ),
        .I1(\Ymap_reg[0]_i_22_n_4 ),
        .I2(\Ymap_reg[0]_i_23_n_4 ),
        .O(\Ymap[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_40 
       (.I0(\cnt_reg_n_0_[14] ),
        .I1(\cnt_reg_n_0_[12] ),
        .I2(\cnt_reg_n_0_[17] ),
        .I3(\cnt_reg_n_0_[13] ),
        .I4(\cnt_reg_n_0_[15] ),
        .I5(\cnt_reg_n_0_[18] ),
        .O(\Ymap[0]_i_40_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_41 
       (.I0(\cnt_reg_n_0_[13] ),
        .I1(\cnt_reg_n_0_[11] ),
        .I2(\cnt_reg_n_0_[16] ),
        .I3(\cnt_reg_n_0_[17] ),
        .I4(\cnt_reg_n_0_[12] ),
        .I5(\cnt_reg_n_0_[14] ),
        .O(\Ymap[0]_i_41_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Ymap[0]_i_42 
       (.I0(\cnt_reg_n_0_[10] ),
        .I1(\cnt_reg_n_0_[15] ),
        .I2(\cnt_reg_n_0_[12] ),
        .I3(\cnt_reg_n_0_[16] ),
        .I4(\cnt_reg_n_0_[11] ),
        .I5(\cnt_reg_n_0_[13] ),
        .O(\Ymap[0]_i_42_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_43 
       (.I0(\cnt_reg_n_0_[11] ),
        .I1(\cnt_reg_n_0_[9] ),
        .I2(\cnt_reg_n_0_[14] ),
        .I3(\cnt_reg_n_0_[10] ),
        .I4(\cnt_reg_n_0_[12] ),
        .I5(\cnt_reg_n_0_[15] ),
        .O(\Ymap[0]_i_43_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[0]_i_44 
       (.I0(\cnt_reg_n_0_[5] ),
        .I1(\Ymap_reg[4]_i_38_n_5 ),
        .I2(\Ymap_reg[4]_i_39_n_5 ),
        .O(\Ymap[0]_i_44_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[0]_i_45 
       (.I0(\cnt_reg_n_0_[4] ),
        .I1(\Ymap_reg[4]_i_38_n_6 ),
        .I2(\Ymap_reg[4]_i_39_n_6 ),
        .O(\Ymap[0]_i_45_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[0]_i_46 
       (.I0(\cnt_reg_n_0_[3] ),
        .I1(\Ymap_reg[4]_i_38_n_7 ),
        .I2(\Ymap_reg[4]_i_39_n_7 ),
        .O(\Ymap[0]_i_46_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[0]_i_47 
       (.I0(\cnt_reg_n_0_[2] ),
        .I1(\Ymap_reg[0]_i_95_n_4 ),
        .I2(\Ymap_reg[0]_i_83_n_4 ),
        .O(\Ymap[0]_i_47_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[0]_i_48 
       (.I0(\Ymap_reg[4]_i_39_n_5 ),
        .I1(\Ymap_reg[4]_i_38_n_5 ),
        .I2(\cnt_reg_n_0_[5] ),
        .I3(\Ymap_reg[4]_i_39_n_4 ),
        .I4(\Ymap_reg[4]_i_38_n_4 ),
        .I5(\cnt_reg_n_0_[6] ),
        .O(\Ymap[0]_i_48_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[0]_i_49 
       (.I0(\Ymap_reg[4]_i_39_n_6 ),
        .I1(\Ymap_reg[4]_i_38_n_6 ),
        .I2(\cnt_reg_n_0_[4] ),
        .I3(\Ymap_reg[4]_i_39_n_5 ),
        .I4(\Ymap_reg[4]_i_38_n_5 ),
        .I5(\cnt_reg_n_0_[5] ),
        .O(\Ymap[0]_i_49_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[0]_i_5 
       (.I0(\Ymap_reg[0]_i_21_n_5 ),
        .I1(\Ymap_reg[0]_i_22_n_5 ),
        .I2(\Ymap_reg[0]_i_23_n_5 ),
        .O(\Ymap[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[0]_i_50 
       (.I0(\Ymap_reg[4]_i_39_n_7 ),
        .I1(\Ymap_reg[4]_i_38_n_7 ),
        .I2(\cnt_reg_n_0_[3] ),
        .I3(\Ymap_reg[4]_i_39_n_6 ),
        .I4(\Ymap_reg[4]_i_38_n_6 ),
        .I5(\cnt_reg_n_0_[4] ),
        .O(\Ymap[0]_i_50_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[0]_i_51 
       (.I0(\Ymap_reg[0]_i_83_n_4 ),
        .I1(\Ymap_reg[0]_i_95_n_4 ),
        .I2(\cnt_reg_n_0_[2] ),
        .I3(\Ymap_reg[4]_i_39_n_7 ),
        .I4(\Ymap_reg[4]_i_38_n_7 ),
        .I5(\cnt_reg_n_0_[3] ),
        .O(\Ymap[0]_i_51_n_0 ));
  LUT3 #(
    .INIT(8'hD4)) 
    \Ymap[0]_i_52 
       (.I0(\cnt_reg_n_0_[10] ),
        .I1(\cnt_reg_n_0_[8] ),
        .I2(\cnt_reg_n_0_[6] ),
        .O(\Ymap[0]_i_52_n_0 ));
  LUT3 #(
    .INIT(8'hD4)) 
    \Ymap[0]_i_53 
       (.I0(\cnt_reg_n_0_[9] ),
        .I1(\cnt_reg_n_0_[7] ),
        .I2(\cnt_reg_n_0_[5] ),
        .O(\Ymap[0]_i_53_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[0]_i_54 
       (.I0(\cnt_reg_n_0_[4] ),
        .I1(\cnt_reg_n_0_[6] ),
        .I2(\cnt_reg_n_0_[8] ),
        .O(\Ymap[0]_i_54_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[0]_i_55 
       (.I0(\cnt_reg_n_0_[3] ),
        .I1(\cnt_reg_n_0_[5] ),
        .I2(\cnt_reg_n_0_[7] ),
        .O(\Ymap[0]_i_55_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Ymap[0]_i_56 
       (.I0(\cnt_reg_n_0_[6] ),
        .I1(\cnt_reg_n_0_[8] ),
        .I2(\cnt_reg_n_0_[10] ),
        .I3(\cnt_reg_n_0_[11] ),
        .I4(\cnt_reg_n_0_[7] ),
        .I5(\cnt_reg_n_0_[9] ),
        .O(\Ymap[0]_i_56_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Ymap[0]_i_57 
       (.I0(\cnt_reg_n_0_[5] ),
        .I1(\cnt_reg_n_0_[7] ),
        .I2(\cnt_reg_n_0_[9] ),
        .I3(\cnt_reg_n_0_[6] ),
        .I4(\cnt_reg_n_0_[8] ),
        .I5(\cnt_reg_n_0_[10] ),
        .O(\Ymap[0]_i_57_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_58 
       (.I0(\cnt_reg_n_0_[8] ),
        .I1(\cnt_reg_n_0_[6] ),
        .I2(\cnt_reg_n_0_[4] ),
        .I3(\cnt_reg_n_0_[5] ),
        .I4(\cnt_reg_n_0_[7] ),
        .I5(\cnt_reg_n_0_[9] ),
        .O(\Ymap[0]_i_58_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_59 
       (.I0(\cnt_reg_n_0_[7] ),
        .I1(\cnt_reg_n_0_[5] ),
        .I2(\cnt_reg_n_0_[3] ),
        .I3(\cnt_reg_n_0_[8] ),
        .I4(\cnt_reg_n_0_[4] ),
        .I5(\cnt_reg_n_0_[6] ),
        .O(\Ymap[0]_i_59_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[0]_i_6 
       (.I0(\Ymap_reg[0]_i_21_n_6 ),
        .I1(\Ymap_reg[0]_i_22_n_6 ),
        .I2(\Ymap_reg[0]_i_23_n_6 ),
        .O(\Ymap[0]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \Ymap[0]_i_61 
       (.I0(\Ymap_reg[0]_i_103_n_4 ),
        .I1(\Ymap_reg[0]_i_104_n_4 ),
        .O(\Ymap[0]_i_61_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \Ymap[0]_i_62 
       (.I0(\Ymap_reg[0]_i_103_n_5 ),
        .I1(\Ymap_reg[0]_i_104_n_5 ),
        .O(\Ymap[0]_i_62_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \Ymap[0]_i_63 
       (.I0(\Ymap_reg[0]_i_103_n_6 ),
        .I1(\Ymap_reg[0]_i_104_n_6 ),
        .O(\Ymap[0]_i_63_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \Ymap[0]_i_64 
       (.I0(\Ymap_reg[0]_i_103_n_7 ),
        .I1(\Ymap_reg[0]_i_105_n_7 ),
        .O(\Ymap[0]_i_64_n_0 ));
  LUT5 #(
    .INIT(32'h78878778)) 
    \Ymap[0]_i_65 
       (.I0(\Ymap_reg[0]_i_104_n_4 ),
        .I1(\Ymap_reg[0]_i_103_n_4 ),
        .I2(\Ymap_reg[0]_i_69_n_7 ),
        .I3(\Ymap_reg[0]_i_72_n_7 ),
        .I4(\Ymap_reg[0]_i_71_n_7 ),
        .O(\Ymap[0]_i_65_n_0 ));
  LUT4 #(
    .INIT(16'h8778)) 
    \Ymap[0]_i_66 
       (.I0(\Ymap_reg[0]_i_104_n_5 ),
        .I1(\Ymap_reg[0]_i_103_n_5 ),
        .I2(\Ymap_reg[0]_i_103_n_4 ),
        .I3(\Ymap_reg[0]_i_104_n_4 ),
        .O(\Ymap[0]_i_66_n_0 ));
  LUT4 #(
    .INIT(16'h8778)) 
    \Ymap[0]_i_67 
       (.I0(\Ymap_reg[0]_i_104_n_6 ),
        .I1(\Ymap_reg[0]_i_103_n_6 ),
        .I2(\Ymap_reg[0]_i_103_n_5 ),
        .I3(\Ymap_reg[0]_i_104_n_5 ),
        .O(\Ymap[0]_i_67_n_0 ));
  LUT4 #(
    .INIT(16'h8778)) 
    \Ymap[0]_i_68 
       (.I0(\Ymap_reg[0]_i_105_n_7 ),
        .I1(\Ymap_reg[0]_i_103_n_7 ),
        .I2(\Ymap_reg[0]_i_103_n_6 ),
        .I3(\Ymap_reg[0]_i_104_n_6 ),
        .O(\Ymap[0]_i_68_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[0]_i_7 
       (.I0(\Ymap_reg[0]_i_21_n_7 ),
        .I1(\Ymap_reg[0]_i_22_n_7 ),
        .I2(\Ymap_reg[0]_i_23_n_7 ),
        .O(\Ymap[0]_i_7_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[0]_i_73 
       (.I0(\cnt_reg_n_0_[13] ),
        .I1(\cnt_reg_n_0_[8] ),
        .I2(\cnt_reg_n_0_[10] ),
        .O(\Ymap[0]_i_73_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[0]_i_74 
       (.I0(\cnt_reg_n_0_[12] ),
        .I1(\cnt_reg_n_0_[7] ),
        .I2(\cnt_reg_n_0_[9] ),
        .O(\Ymap[0]_i_74_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[0]_i_75 
       (.I0(\cnt_reg_n_0_[11] ),
        .I1(\cnt_reg_n_0_[6] ),
        .I2(\cnt_reg_n_0_[8] ),
        .O(\Ymap[0]_i_75_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[0]_i_76 
       (.I0(\cnt_reg_n_0_[10] ),
        .I1(\cnt_reg_n_0_[5] ),
        .I2(\cnt_reg_n_0_[7] ),
        .O(\Ymap[0]_i_76_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_77 
       (.I0(\cnt_reg_n_0_[10] ),
        .I1(\cnt_reg_n_0_[8] ),
        .I2(\cnt_reg_n_0_[13] ),
        .I3(\cnt_reg_n_0_[9] ),
        .I4(\cnt_reg_n_0_[11] ),
        .I5(\cnt_reg_n_0_[14] ),
        .O(\Ymap[0]_i_77_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_78 
       (.I0(\cnt_reg_n_0_[9] ),
        .I1(\cnt_reg_n_0_[7] ),
        .I2(\cnt_reg_n_0_[12] ),
        .I3(\cnt_reg_n_0_[13] ),
        .I4(\cnt_reg_n_0_[8] ),
        .I5(\cnt_reg_n_0_[10] ),
        .O(\Ymap[0]_i_78_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_79 
       (.I0(\cnt_reg_n_0_[8] ),
        .I1(\cnt_reg_n_0_[6] ),
        .I2(\cnt_reg_n_0_[11] ),
        .I3(\cnt_reg_n_0_[12] ),
        .I4(\cnt_reg_n_0_[7] ),
        .I5(\cnt_reg_n_0_[9] ),
        .O(\Ymap[0]_i_79_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[0]_i_8 
       (.I0(\Ymap_reg[0]_i_23_n_4 ),
        .I1(\Ymap_reg[0]_i_22_n_4 ),
        .I2(\Ymap_reg[0]_i_21_n_4 ),
        .I3(\Ymap_reg[4]_i_11_n_7 ),
        .I4(\Ymap_reg[4]_i_12_n_7 ),
        .I5(\Ymap_reg[4]_i_13_n_7 ),
        .O(\Ymap[0]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_80 
       (.I0(\cnt_reg_n_0_[7] ),
        .I1(\cnt_reg_n_0_[5] ),
        .I2(\cnt_reg_n_0_[10] ),
        .I3(\cnt_reg_n_0_[6] ),
        .I4(\cnt_reg_n_0_[8] ),
        .I5(\cnt_reg_n_0_[11] ),
        .O(\Ymap[0]_i_80_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[0]_i_81 
       (.I0(\Ymap_reg[0]_i_83_n_5 ),
        .I1(\Ymap_reg[0]_i_95_n_5 ),
        .I2(\cnt_reg_n_0_[1] ),
        .O(\Ymap[0]_i_81_n_0 ));
  LUT3 #(
    .INIT(8'h96)) 
    \Ymap[0]_i_82 
       (.I0(\cnt_reg_n_0_[1] ),
        .I1(\Ymap_reg[0]_i_95_n_5 ),
        .I2(\Ymap_reg[0]_i_83_n_5 ),
        .O(\Ymap[0]_i_82_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[0]_i_84 
       (.I0(\cnt_reg_n_0_[1] ),
        .I1(\Ymap_reg[0]_i_95_n_5 ),
        .I2(\Ymap_reg[0]_i_83_n_5 ),
        .I3(\Ymap_reg[0]_i_83_n_4 ),
        .I4(\Ymap_reg[0]_i_95_n_4 ),
        .I5(\cnt_reg_n_0_[2] ),
        .O(\Ymap[0]_i_84_n_0 ));
  LUT5 #(
    .INIT(32'h69969696)) 
    \Ymap[0]_i_85 
       (.I0(\Ymap_reg[0]_i_83_n_5 ),
        .I1(\Ymap_reg[0]_i_95_n_5 ),
        .I2(\cnt_reg_n_0_[1] ),
        .I3(\Ymap_reg[0]_i_95_n_6 ),
        .I4(\Ymap_reg[0]_i_83_n_6 ),
        .O(\Ymap[0]_i_85_n_0 ));
  LUT3 #(
    .INIT(8'h96)) 
    \Ymap[0]_i_86 
       (.I0(\Ymap_reg[0]_i_95_n_6 ),
        .I1(\Ymap_reg[0]_i_83_n_6 ),
        .I2(\cnt_reg_n_0_[0] ),
        .O(\Ymap[0]_i_86_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[0]_i_87 
       (.I0(\Ymap_reg[0]_i_83_n_7 ),
        .I1(\Ymap_reg[0]_i_95_n_7 ),
        .O(\Ymap[0]_i_87_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[0]_i_88 
       (.I0(\cnt_reg_n_0_[4] ),
        .I1(\cnt_reg_n_0_[2] ),
        .I2(\cnt_reg_n_0_[6] ),
        .O(\Ymap[0]_i_88_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[0]_i_89 
       (.I0(\cnt_reg_n_0_[1] ),
        .I1(\cnt_reg_n_0_[3] ),
        .I2(\cnt_reg_n_0_[5] ),
        .O(\Ymap[0]_i_89_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[0]_i_9 
       (.I0(\Ymap_reg[0]_i_23_n_5 ),
        .I1(\Ymap_reg[0]_i_22_n_5 ),
        .I2(\Ymap_reg[0]_i_21_n_5 ),
        .I3(\Ymap_reg[0]_i_21_n_4 ),
        .I4(\Ymap_reg[0]_i_22_n_4 ),
        .I5(\Ymap_reg[0]_i_23_n_4 ),
        .O(\Ymap[0]_i_9_n_0 ));
  LUT3 #(
    .INIT(8'h69)) 
    \Ymap[0]_i_90 
       (.I0(\cnt_reg_n_0_[1] ),
        .I1(\cnt_reg_n_0_[3] ),
        .I2(\cnt_reg_n_0_[5] ),
        .O(\Ymap[0]_i_90_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_91 
       (.I0(\cnt_reg_n_0_[6] ),
        .I1(\cnt_reg_n_0_[2] ),
        .I2(\cnt_reg_n_0_[4] ),
        .I3(\cnt_reg_n_0_[7] ),
        .I4(\cnt_reg_n_0_[3] ),
        .I5(\cnt_reg_n_0_[5] ),
        .O(\Ymap[0]_i_91_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[0]_i_92 
       (.I0(\cnt_reg_n_0_[5] ),
        .I1(\cnt_reg_n_0_[3] ),
        .I2(\cnt_reg_n_0_[1] ),
        .I3(\cnt_reg_n_0_[2] ),
        .I4(\cnt_reg_n_0_[4] ),
        .I5(\cnt_reg_n_0_[6] ),
        .O(\Ymap[0]_i_92_n_0 ));
  LUT5 #(
    .INIT(32'h69966969)) 
    \Ymap[0]_i_93 
       (.I0(\cnt_reg_n_0_[1] ),
        .I1(\cnt_reg_n_0_[3] ),
        .I2(\cnt_reg_n_0_[5] ),
        .I3(\cnt_reg_n_0_[4] ),
        .I4(\cnt_reg_n_0_[0] ),
        .O(\Ymap[0]_i_93_n_0 ));
  LUT3 #(
    .INIT(8'h69)) 
    \Ymap[0]_i_94 
       (.I0(\cnt_reg_n_0_[0] ),
        .I1(\cnt_reg_n_0_[4] ),
        .I2(\cnt_reg_n_0_[2] ),
        .O(\Ymap[0]_i_94_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \Ymap[0]_i_96 
       (.I0(\cnt_reg_n_0_[2] ),
        .I1(\Ymap_reg[0]_i_136_n_4 ),
        .O(\Ymap[0]_i_96_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \Ymap[0]_i_97 
       (.I0(\cnt_reg_n_0_[1] ),
        .I1(\Ymap_reg[0]_i_136_n_5 ),
        .O(\Ymap[0]_i_97_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \Ymap[0]_i_98 
       (.I0(\cnt_reg_n_0_[0] ),
        .I1(\Ymap_reg[0]_i_136_n_6 ),
        .O(\Ymap[0]_i_98_n_0 ));
  LUT4 #(
    .INIT(16'h8778)) 
    \Ymap[0]_i_99 
       (.I0(\Ymap_reg[0]_i_136_n_4 ),
        .I1(\cnt_reg_n_0_[2] ),
        .I2(\Ymap_reg[0]_i_103_n_7 ),
        .I3(\Ymap_reg[0]_i_105_n_7 ),
        .O(\Ymap[0]_i_99_n_0 ));
  LUT5 #(
    .INIT(32'hF4F5B0A0)) 
    \Ymap[1]_i_1 
       (.I0(\Ymap_reg[5]_i_2_n_1 ),
        .I1(\cnt_reg_n_0_[30] ),
        .I2(\Ymap_reg[4]_i_2_n_7 ),
        .I3(\Ymap_reg[5]_i_4_n_6 ),
        .I4(\Ymap_reg[3]_i_2_n_6 ),
        .O(\Ymap[1]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hF4F5B0A0)) 
    \Ymap[2]_i_1 
       (.I0(\Ymap_reg[5]_i_2_n_1 ),
        .I1(\cnt_reg_n_0_[30] ),
        .I2(\Ymap_reg[4]_i_2_n_6 ),
        .I3(\Ymap_reg[5]_i_4_n_6 ),
        .I4(\Ymap_reg[3]_i_2_n_5 ),
        .O(\Ymap[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hF4F5B0A0)) 
    \Ymap[3]_i_1 
       (.I0(\Ymap_reg[5]_i_2_n_1 ),
        .I1(\cnt_reg_n_0_[30] ),
        .I2(\Ymap_reg[4]_i_2_n_5 ),
        .I3(\Ymap_reg[5]_i_4_n_6 ),
        .I4(\Ymap_reg[3]_i_2_n_4 ),
        .O(\Ymap[3]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \Ymap[3]_i_3 
       (.I0(\Ymap_reg[4]_i_2_n_5 ),
        .O(\Ymap[3]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \Ymap[3]_i_4 
       (.I0(\Ymap_reg[4]_i_2_n_6 ),
        .O(\Ymap[3]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \Ymap[3]_i_5 
       (.I0(\Ymap_reg[4]_i_2_n_7 ),
        .O(\Ymap[3]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \Ymap[3]_i_6 
       (.I0(\Ymap_reg[0]_i_2_n_4 ),
        .O(\Ymap[3]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hF4F5B0A0)) 
    \Ymap[4]_i_1 
       (.I0(\Ymap_reg[5]_i_2_n_1 ),
        .I1(\cnt_reg_n_0_[30] ),
        .I2(\Ymap_reg[4]_i_2_n_4 ),
        .I3(\Ymap_reg[5]_i_4_n_6 ),
        .I4(\Ymap_reg[5]_i_5_n_7 ),
        .O(\Ymap[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[4]_i_10 
       (.I0(\Ymap_reg[4]_i_13_n_7 ),
        .I1(\Ymap_reg[4]_i_12_n_7 ),
        .I2(\Ymap_reg[4]_i_11_n_7 ),
        .I3(\Ymap_reg[4]_i_11_n_6 ),
        .I4(\Ymap_reg[4]_i_12_n_6 ),
        .I5(\Ymap_reg[4]_i_13_n_6 ),
        .O(\Ymap[4]_i_10_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[4]_i_14 
       (.I0(\cnt_reg_n_0_[21] ),
        .I1(\cnt_reg_n_0_[16] ),
        .I2(\cnt_reg_n_0_[18] ),
        .O(\Ymap[4]_i_14_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[4]_i_15 
       (.I0(\cnt_reg_n_0_[20] ),
        .I1(\cnt_reg_n_0_[15] ),
        .I2(\cnt_reg_n_0_[17] ),
        .O(\Ymap[4]_i_15_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[4]_i_16 
       (.I0(\cnt_reg_n_0_[19] ),
        .I1(\cnt_reg_n_0_[14] ),
        .I2(\cnt_reg_n_0_[16] ),
        .O(\Ymap[4]_i_16_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[4]_i_17 
       (.I0(\cnt_reg_n_0_[18] ),
        .I1(\cnt_reg_n_0_[13] ),
        .I2(\cnt_reg_n_0_[15] ),
        .O(\Ymap[4]_i_17_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[4]_i_18 
       (.I0(\cnt_reg_n_0_[18] ),
        .I1(\cnt_reg_n_0_[16] ),
        .I2(\cnt_reg_n_0_[21] ),
        .I3(\cnt_reg_n_0_[17] ),
        .I4(\cnt_reg_n_0_[19] ),
        .I5(\cnt_reg_n_0_[22] ),
        .O(\Ymap[4]_i_18_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[4]_i_19 
       (.I0(\cnt_reg_n_0_[17] ),
        .I1(\cnt_reg_n_0_[15] ),
        .I2(\cnt_reg_n_0_[20] ),
        .I3(\cnt_reg_n_0_[21] ),
        .I4(\cnt_reg_n_0_[16] ),
        .I5(\cnt_reg_n_0_[18] ),
        .O(\Ymap[4]_i_19_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[4]_i_20 
       (.I0(\cnt_reg_n_0_[16] ),
        .I1(\cnt_reg_n_0_[14] ),
        .I2(\cnt_reg_n_0_[19] ),
        .I3(\cnt_reg_n_0_[20] ),
        .I4(\cnt_reg_n_0_[15] ),
        .I5(\cnt_reg_n_0_[17] ),
        .O(\Ymap[4]_i_20_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[4]_i_21 
       (.I0(\cnt_reg_n_0_[15] ),
        .I1(\cnt_reg_n_0_[13] ),
        .I2(\cnt_reg_n_0_[18] ),
        .I3(\cnt_reg_n_0_[14] ),
        .I4(\cnt_reg_n_0_[16] ),
        .I5(\cnt_reg_n_0_[19] ),
        .O(\Ymap[4]_i_21_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[4]_i_22 
       (.I0(\cnt_reg_n_0_[9] ),
        .I1(\Ymap_reg[5]_i_128_n_5 ),
        .I2(\Ymap_reg[5]_i_129_n_5 ),
        .O(\Ymap[4]_i_22_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[4]_i_23 
       (.I0(\cnt_reg_n_0_[8] ),
        .I1(\Ymap_reg[5]_i_128_n_6 ),
        .I2(\Ymap_reg[5]_i_129_n_6 ),
        .O(\Ymap[4]_i_23_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[4]_i_24 
       (.I0(\cnt_reg_n_0_[7] ),
        .I1(\Ymap_reg[5]_i_128_n_7 ),
        .I2(\Ymap_reg[5]_i_129_n_7 ),
        .O(\Ymap[4]_i_24_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[4]_i_25 
       (.I0(\cnt_reg_n_0_[6] ),
        .I1(\Ymap_reg[4]_i_38_n_4 ),
        .I2(\Ymap_reg[4]_i_39_n_4 ),
        .O(\Ymap[4]_i_25_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[4]_i_26 
       (.I0(\Ymap_reg[5]_i_129_n_5 ),
        .I1(\Ymap_reg[5]_i_128_n_5 ),
        .I2(\cnt_reg_n_0_[9] ),
        .I3(\Ymap_reg[5]_i_129_n_4 ),
        .I4(\Ymap_reg[5]_i_128_n_4 ),
        .I5(\cnt_reg_n_0_[10] ),
        .O(\Ymap[4]_i_26_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[4]_i_27 
       (.I0(\Ymap_reg[5]_i_129_n_6 ),
        .I1(\Ymap_reg[5]_i_128_n_6 ),
        .I2(\cnt_reg_n_0_[8] ),
        .I3(\Ymap_reg[5]_i_129_n_5 ),
        .I4(\Ymap_reg[5]_i_128_n_5 ),
        .I5(\cnt_reg_n_0_[9] ),
        .O(\Ymap[4]_i_27_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[4]_i_28 
       (.I0(\Ymap_reg[5]_i_129_n_7 ),
        .I1(\Ymap_reg[5]_i_128_n_7 ),
        .I2(\cnt_reg_n_0_[7] ),
        .I3(\Ymap_reg[5]_i_129_n_6 ),
        .I4(\Ymap_reg[5]_i_128_n_6 ),
        .I5(\cnt_reg_n_0_[8] ),
        .O(\Ymap[4]_i_28_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[4]_i_29 
       (.I0(\Ymap_reg[4]_i_39_n_4 ),
        .I1(\Ymap_reg[4]_i_38_n_4 ),
        .I2(\cnt_reg_n_0_[6] ),
        .I3(\Ymap_reg[5]_i_129_n_7 ),
        .I4(\Ymap_reg[5]_i_128_n_7 ),
        .I5(\cnt_reg_n_0_[7] ),
        .O(\Ymap[4]_i_29_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[4]_i_3 
       (.I0(\Ymap_reg[4]_i_11_n_4 ),
        .I1(\Ymap_reg[4]_i_12_n_4 ),
        .I2(\Ymap_reg[4]_i_13_n_4 ),
        .O(\Ymap[4]_i_3_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[4]_i_30 
       (.I0(\cnt_reg_n_0_[12] ),
        .I1(\cnt_reg_n_0_[10] ),
        .I2(\cnt_reg_n_0_[14] ),
        .O(\Ymap[4]_i_30_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[4]_i_31 
       (.I0(\cnt_reg_n_0_[9] ),
        .I1(\cnt_reg_n_0_[11] ),
        .I2(\cnt_reg_n_0_[13] ),
        .O(\Ymap[4]_i_31_n_0 ));
  LUT3 #(
    .INIT(8'hD4)) 
    \Ymap[4]_i_32 
       (.I0(\cnt_reg_n_0_[12] ),
        .I1(\cnt_reg_n_0_[8] ),
        .I2(\cnt_reg_n_0_[10] ),
        .O(\Ymap[4]_i_32_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[4]_i_33 
       (.I0(\cnt_reg_n_0_[9] ),
        .I1(\cnt_reg_n_0_[7] ),
        .I2(\cnt_reg_n_0_[11] ),
        .O(\Ymap[4]_i_33_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[4]_i_34 
       (.I0(\cnt_reg_n_0_[14] ),
        .I1(\cnt_reg_n_0_[10] ),
        .I2(\cnt_reg_n_0_[12] ),
        .I3(\cnt_reg_n_0_[15] ),
        .I4(\cnt_reg_n_0_[11] ),
        .I5(\cnt_reg_n_0_[13] ),
        .O(\Ymap[4]_i_34_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[4]_i_35 
       (.I0(\cnt_reg_n_0_[13] ),
        .I1(\cnt_reg_n_0_[11] ),
        .I2(\cnt_reg_n_0_[9] ),
        .I3(\cnt_reg_n_0_[10] ),
        .I4(\cnt_reg_n_0_[12] ),
        .I5(\cnt_reg_n_0_[14] ),
        .O(\Ymap[4]_i_35_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Ymap[4]_i_36 
       (.I0(\cnt_reg_n_0_[10] ),
        .I1(\cnt_reg_n_0_[8] ),
        .I2(\cnt_reg_n_0_[12] ),
        .I3(\cnt_reg_n_0_[9] ),
        .I4(\cnt_reg_n_0_[11] ),
        .I5(\cnt_reg_n_0_[13] ),
        .O(\Ymap[4]_i_36_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[4]_i_37 
       (.I0(\cnt_reg_n_0_[11] ),
        .I1(\cnt_reg_n_0_[7] ),
        .I2(\cnt_reg_n_0_[9] ),
        .I3(\cnt_reg_n_0_[12] ),
        .I4(\cnt_reg_n_0_[8] ),
        .I5(\cnt_reg_n_0_[10] ),
        .O(\Ymap[4]_i_37_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[4]_i_4 
       (.I0(\Ymap_reg[4]_i_11_n_5 ),
        .I1(\Ymap_reg[4]_i_12_n_5 ),
        .I2(\Ymap_reg[4]_i_13_n_5 ),
        .O(\Ymap[4]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Ymap[4]_i_40 
       (.I0(\cnt_reg_n_0_[20] ),
        .I1(\cnt_reg_n_0_[18] ),
        .I2(\cnt_reg_n_0_[22] ),
        .I3(\cnt_reg_n_0_[23] ),
        .I4(\cnt_reg_n_0_[19] ),
        .I5(\cnt_reg_n_0_[21] ),
        .O(\Ymap[4]_i_40_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Ymap[4]_i_41 
       (.I0(\cnt_reg_n_0_[19] ),
        .I1(\cnt_reg_n_0_[17] ),
        .I2(\cnt_reg_n_0_[21] ),
        .I3(\cnt_reg_n_0_[18] ),
        .I4(\cnt_reg_n_0_[20] ),
        .I5(\cnt_reg_n_0_[22] ),
        .O(\Ymap[4]_i_41_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Ymap[4]_i_42 
       (.I0(\cnt_reg_n_0_[18] ),
        .I1(\cnt_reg_n_0_[16] ),
        .I2(\cnt_reg_n_0_[20] ),
        .I3(\cnt_reg_n_0_[17] ),
        .I4(\cnt_reg_n_0_[19] ),
        .I5(\cnt_reg_n_0_[21] ),
        .O(\Ymap[4]_i_42_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Ymap[4]_i_43 
       (.I0(\cnt_reg_n_0_[17] ),
        .I1(\cnt_reg_n_0_[15] ),
        .I2(\cnt_reg_n_0_[19] ),
        .I3(\cnt_reg_n_0_[20] ),
        .I4(\cnt_reg_n_0_[16] ),
        .I5(\cnt_reg_n_0_[18] ),
        .O(\Ymap[4]_i_43_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[4]_i_44 
       (.I0(\cnt_reg_n_0_[26] ),
        .I1(\cnt_reg_n_0_[24] ),
        .I2(\cnt_reg_n_0_[29] ),
        .I3(\cnt_reg_n_0_[25] ),
        .I4(\cnt_reg_n_0_[27] ),
        .I5(\cnt_reg_n_0_[30] ),
        .O(\Ymap[4]_i_44_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[4]_i_45 
       (.I0(\cnt_reg_n_0_[25] ),
        .I1(\cnt_reg_n_0_[23] ),
        .I2(\cnt_reg_n_0_[28] ),
        .I3(\cnt_reg_n_0_[29] ),
        .I4(\cnt_reg_n_0_[24] ),
        .I5(\cnt_reg_n_0_[26] ),
        .O(\Ymap[4]_i_45_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[4]_i_46 
       (.I0(\cnt_reg_n_0_[24] ),
        .I1(\cnt_reg_n_0_[22] ),
        .I2(\cnt_reg_n_0_[27] ),
        .I3(\cnt_reg_n_0_[28] ),
        .I4(\cnt_reg_n_0_[23] ),
        .I5(\cnt_reg_n_0_[25] ),
        .O(\Ymap[4]_i_46_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[4]_i_47 
       (.I0(\cnt_reg_n_0_[23] ),
        .I1(\cnt_reg_n_0_[21] ),
        .I2(\cnt_reg_n_0_[26] ),
        .I3(\cnt_reg_n_0_[22] ),
        .I4(\cnt_reg_n_0_[24] ),
        .I5(\cnt_reg_n_0_[27] ),
        .O(\Ymap[4]_i_47_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[4]_i_5 
       (.I0(\Ymap_reg[4]_i_11_n_6 ),
        .I1(\Ymap_reg[4]_i_12_n_6 ),
        .I2(\Ymap_reg[4]_i_13_n_6 ),
        .O(\Ymap[4]_i_5_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[4]_i_6 
       (.I0(\Ymap_reg[4]_i_11_n_7 ),
        .I1(\Ymap_reg[4]_i_12_n_7 ),
        .I2(\Ymap_reg[4]_i_13_n_7 ),
        .O(\Ymap[4]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[4]_i_7 
       (.I0(\Ymap_reg[4]_i_13_n_4 ),
        .I1(\Ymap_reg[4]_i_12_n_4 ),
        .I2(\Ymap_reg[4]_i_11_n_4 ),
        .I3(\Ymap_reg[5]_i_35_n_7 ),
        .I4(\Ymap_reg[5]_i_36_n_7 ),
        .I5(\Ymap_reg[5]_i_37_n_7 ),
        .O(\Ymap[4]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[4]_i_8 
       (.I0(\Ymap_reg[4]_i_13_n_5 ),
        .I1(\Ymap_reg[4]_i_12_n_5 ),
        .I2(\Ymap_reg[4]_i_11_n_5 ),
        .I3(\Ymap_reg[4]_i_11_n_4 ),
        .I4(\Ymap_reg[4]_i_12_n_4 ),
        .I5(\Ymap_reg[4]_i_13_n_4 ),
        .O(\Ymap[4]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[4]_i_9 
       (.I0(\Ymap_reg[4]_i_13_n_6 ),
        .I1(\Ymap_reg[4]_i_12_n_6 ),
        .I2(\Ymap_reg[4]_i_11_n_6 ),
        .I3(\Ymap_reg[4]_i_11_n_5 ),
        .I4(\Ymap_reg[4]_i_12_n_5 ),
        .I5(\Ymap_reg[4]_i_13_n_5 ),
        .O(\Ymap[4]_i_9_n_0 ));
  LUT5 #(
    .INIT(32'hF4F5B0A0)) 
    \Ymap[5]_i_1 
       (.I0(\Ymap_reg[5]_i_2_n_1 ),
        .I1(\cnt_reg_n_0_[30] ),
        .I2(\Ymap_reg[5]_i_3_n_7 ),
        .I3(\Ymap_reg[5]_i_4_n_6 ),
        .I4(\Ymap_reg[5]_i_5_n_6 ),
        .O(\Ymap[5]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hB44B)) 
    \Ymap[5]_i_10 
       (.I0(\cnt_reg_n_0_[29] ),
        .I1(\Ymap_reg[5]_i_4_n_7 ),
        .I2(\cnt_reg_n_0_[30] ),
        .I3(\Ymap_reg[5]_i_4_n_6 ),
        .O(\Ymap[5]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Ymap[5]_i_100 
       (.I0(\cnt_reg_n_0_[20] ),
        .I1(\cnt_reg_n_0_[18] ),
        .I2(\cnt_reg_n_0_[22] ),
        .I3(\cnt_reg_n_0_[23] ),
        .I4(\cnt_reg_n_0_[19] ),
        .I5(\cnt_reg_n_0_[21] ),
        .O(\Ymap[5]_i_100_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Ymap[5]_i_101 
       (.I0(\cnt_reg_n_0_[19] ),
        .I1(\cnt_reg_n_0_[17] ),
        .I2(\cnt_reg_n_0_[21] ),
        .I3(\cnt_reg_n_0_[18] ),
        .I4(\cnt_reg_n_0_[20] ),
        .I5(\cnt_reg_n_0_[22] ),
        .O(\Ymap[5]_i_101_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Ymap[5]_i_102 
       (.I0(\cnt_reg_n_0_[18] ),
        .I1(\cnt_reg_n_0_[16] ),
        .I2(\cnt_reg_n_0_[20] ),
        .I3(\cnt_reg_n_0_[17] ),
        .I4(\cnt_reg_n_0_[19] ),
        .I5(\cnt_reg_n_0_[21] ),
        .O(\Ymap[5]_i_102_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Ymap[5]_i_103 
       (.I0(\cnt_reg_n_0_[17] ),
        .I1(\cnt_reg_n_0_[15] ),
        .I2(\cnt_reg_n_0_[19] ),
        .I3(\cnt_reg_n_0_[20] ),
        .I4(\cnt_reg_n_0_[16] ),
        .I5(\cnt_reg_n_0_[18] ),
        .O(\Ymap[5]_i_103_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[5]_i_106 
       (.I0(\Ymap_reg[5]_i_105_n_4 ),
        .I1(\Ymap_reg[5]_i_105_n_6 ),
        .O(\Ymap[5]_i_106_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[5]_i_107 
       (.I0(\Ymap_reg[5]_i_105_n_5 ),
        .I1(\Ymap_reg[5]_i_105_n_7 ),
        .O(\Ymap[5]_i_107_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[5]_i_108 
       (.I0(\Ymap_reg[5]_i_105_n_6 ),
        .I1(\Ymap_reg[5]_i_132_n_4 ),
        .O(\Ymap[5]_i_108_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[5]_i_109 
       (.I0(\Ymap_reg[5]_i_105_n_7 ),
        .I1(\Ymap_reg[5]_i_132_n_5 ),
        .O(\Ymap[5]_i_109_n_0 ));
  LUT4 #(
    .INIT(16'hB44B)) 
    \Ymap[5]_i_11 
       (.I0(\cnt_reg_n_0_[28] ),
        .I1(\Ymap_reg[5]_i_21_n_4 ),
        .I2(\cnt_reg_n_0_[29] ),
        .I3(\Ymap_reg[5]_i_4_n_7 ),
        .O(\Ymap[5]_i_11_n_0 ));
  LUT3 #(
    .INIT(8'hD4)) 
    \Ymap[5]_i_110 
       (.I0(\Ymap_reg[5]_i_145_n_1 ),
        .I1(\Ymap_reg[5]_i_146_n_5 ),
        .I2(\Ymap_reg[5]_i_147_n_5 ),
        .O(\Ymap[5]_i_110_n_0 ));
  LUT3 #(
    .INIT(8'hD4)) 
    \Ymap[5]_i_111 
       (.I0(\Ymap_reg[5]_i_145_n_1 ),
        .I1(\Ymap_reg[5]_i_146_n_6 ),
        .I2(\Ymap_reg[5]_i_147_n_6 ),
        .O(\Ymap[5]_i_111_n_0 ));
  LUT3 #(
    .INIT(8'hD4)) 
    \Ymap[5]_i_112 
       (.I0(\Ymap_reg[5]_i_145_n_1 ),
        .I1(\Ymap_reg[5]_i_146_n_7 ),
        .I2(\Ymap_reg[5]_i_147_n_7 ),
        .O(\Ymap[5]_i_112_n_0 ));
  LUT5 #(
    .INIT(32'h7E81817E)) 
    \Ymap[5]_i_113 
       (.I0(\Ymap_reg[5]_i_145_n_1 ),
        .I1(\Ymap_reg[5]_i_147_n_4 ),
        .I2(\Ymap_reg[5]_i_146_n_4 ),
        .I3(\Ymap_reg[5]_i_148_n_3 ),
        .I4(\Ymap_reg[5]_i_149_n_7 ),
        .O(\Ymap[5]_i_113_n_0 ));
  LUT5 #(
    .INIT(32'h817E7E81)) 
    \Ymap[5]_i_114 
       (.I0(\Ymap_reg[5]_i_147_n_5 ),
        .I1(\Ymap_reg[5]_i_146_n_5 ),
        .I2(\Ymap_reg[5]_i_145_n_1 ),
        .I3(\Ymap_reg[5]_i_146_n_4 ),
        .I4(\Ymap_reg[5]_i_147_n_4 ),
        .O(\Ymap[5]_i_114_n_0 ));
  LUT5 #(
    .INIT(32'h817E7E81)) 
    \Ymap[5]_i_115 
       (.I0(\Ymap_reg[5]_i_147_n_6 ),
        .I1(\Ymap_reg[5]_i_146_n_6 ),
        .I2(\Ymap_reg[5]_i_145_n_1 ),
        .I3(\Ymap_reg[5]_i_146_n_5 ),
        .I4(\Ymap_reg[5]_i_147_n_5 ),
        .O(\Ymap[5]_i_115_n_0 ));
  LUT5 #(
    .INIT(32'h817E7E81)) 
    \Ymap[5]_i_116 
       (.I0(\Ymap_reg[5]_i_147_n_7 ),
        .I1(\Ymap_reg[5]_i_146_n_7 ),
        .I2(\Ymap_reg[5]_i_145_n_1 ),
        .I3(\Ymap_reg[5]_i_146_n_6 ),
        .I4(\Ymap_reg[5]_i_147_n_6 ),
        .O(\Ymap[5]_i_116_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Ymap[5]_i_118 
       (.I0(\Ymap_reg[5]_i_104_n_6 ),
        .I1(\cnt_reg_n_0_[18] ),
        .O(\Ymap[5]_i_118_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Ymap[5]_i_119 
       (.I0(\Ymap_reg[5]_i_104_n_7 ),
        .I1(\cnt_reg_n_0_[17] ),
        .O(\Ymap[5]_i_119_n_0 ));
  LUT4 #(
    .INIT(16'hB44B)) 
    \Ymap[5]_i_12 
       (.I0(\cnt_reg_n_0_[27] ),
        .I1(\Ymap_reg[5]_i_21_n_5 ),
        .I2(\cnt_reg_n_0_[28] ),
        .I3(\Ymap_reg[5]_i_21_n_4 ),
        .O(\Ymap[5]_i_12_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Ymap[5]_i_120 
       (.I0(\Ymap_reg[5]_i_131_n_4 ),
        .I1(\cnt_reg_n_0_[16] ),
        .O(\Ymap[5]_i_120_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Ymap[5]_i_121 
       (.I0(\Ymap_reg[5]_i_131_n_5 ),
        .I1(\cnt_reg_n_0_[15] ),
        .O(\Ymap[5]_i_121_n_0 ));
  LUT4 #(
    .INIT(16'hB44B)) 
    \Ymap[5]_i_122 
       (.I0(\cnt_reg_n_0_[18] ),
        .I1(\Ymap_reg[5]_i_104_n_6 ),
        .I2(\cnt_reg_n_0_[19] ),
        .I3(\Ymap_reg[5]_i_104_n_5 ),
        .O(\Ymap[5]_i_122_n_0 ));
  LUT4 #(
    .INIT(16'hB44B)) 
    \Ymap[5]_i_123 
       (.I0(\cnt_reg_n_0_[17] ),
        .I1(\Ymap_reg[5]_i_104_n_7 ),
        .I2(\cnt_reg_n_0_[18] ),
        .I3(\Ymap_reg[5]_i_104_n_6 ),
        .O(\Ymap[5]_i_123_n_0 ));
  LUT4 #(
    .INIT(16'hB44B)) 
    \Ymap[5]_i_124 
       (.I0(\cnt_reg_n_0_[16] ),
        .I1(\Ymap_reg[5]_i_131_n_4 ),
        .I2(\cnt_reg_n_0_[17] ),
        .I3(\Ymap_reg[5]_i_104_n_7 ),
        .O(\Ymap[5]_i_124_n_0 ));
  LUT4 #(
    .INIT(16'hB44B)) 
    \Ymap[5]_i_125 
       (.I0(\cnt_reg_n_0_[15] ),
        .I1(\Ymap_reg[5]_i_131_n_5 ),
        .I2(\cnt_reg_n_0_[16] ),
        .I3(\Ymap_reg[5]_i_131_n_4 ),
        .O(\Ymap[5]_i_125_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[5]_i_13 
       (.I0(\Ymap_reg[5]_i_35_n_4 ),
        .I1(\Ymap_reg[5]_i_36_n_4 ),
        .I2(\Ymap_reg[5]_i_37_n_4 ),
        .O(\Ymap[5]_i_13_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[5]_i_133 
       (.I0(\Ymap_reg[5]_i_132_n_4 ),
        .I1(\Ymap_reg[5]_i_132_n_6 ),
        .O(\Ymap[5]_i_133_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[5]_i_134 
       (.I0(\Ymap_reg[5]_i_132_n_5 ),
        .I1(\Ymap_reg[5]_i_132_n_7 ),
        .O(\Ymap[5]_i_134_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[5]_i_135 
       (.I0(\Ymap_reg[5]_i_132_n_6 ),
        .I1(\Ymap_reg[5]_i_192_n_4 ),
        .O(\Ymap[5]_i_135_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[5]_i_136 
       (.I0(\Ymap_reg[5]_i_132_n_7 ),
        .I1(\Ymap_reg[5]_i_192_n_5 ),
        .O(\Ymap[5]_i_136_n_0 ));
  LUT3 #(
    .INIT(8'hD4)) 
    \Ymap[5]_i_137 
       (.I0(\Ymap_reg[5]_i_145_n_1 ),
        .I1(\Ymap_reg[5]_i_205_n_4 ),
        .I2(\Ymap_reg[5]_i_206_n_4 ),
        .O(\Ymap[5]_i_137_n_0 ));
  LUT3 #(
    .INIT(8'hD4)) 
    \Ymap[5]_i_138 
       (.I0(\Ymap_reg[5]_i_145_n_1 ),
        .I1(\Ymap_reg[5]_i_205_n_5 ),
        .I2(\Ymap_reg[5]_i_206_n_5 ),
        .O(\Ymap[5]_i_138_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[5]_i_139 
       (.I0(\Ymap_reg[5]_i_145_n_6 ),
        .I1(\Ymap_reg[5]_i_205_n_6 ),
        .I2(\Ymap_reg[5]_i_206_n_6 ),
        .O(\Ymap[5]_i_139_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[5]_i_14 
       (.I0(\Ymap_reg[5]_i_35_n_5 ),
        .I1(\Ymap_reg[5]_i_36_n_5 ),
        .I2(\Ymap_reg[5]_i_37_n_5 ),
        .O(\Ymap[5]_i_14_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[5]_i_140 
       (.I0(\Ymap_reg[5]_i_145_n_7 ),
        .I1(\Ymap_reg[5]_i_205_n_7 ),
        .I2(\Ymap_reg[5]_i_206_n_7 ),
        .O(\Ymap[5]_i_140_n_0 ));
  LUT5 #(
    .INIT(32'h817E7E81)) 
    \Ymap[5]_i_141 
       (.I0(\Ymap_reg[5]_i_206_n_4 ),
        .I1(\Ymap_reg[5]_i_205_n_4 ),
        .I2(\Ymap_reg[5]_i_145_n_1 ),
        .I3(\Ymap_reg[5]_i_146_n_7 ),
        .I4(\Ymap_reg[5]_i_147_n_7 ),
        .O(\Ymap[5]_i_141_n_0 ));
  LUT5 #(
    .INIT(32'h817E7E81)) 
    \Ymap[5]_i_142 
       (.I0(\Ymap_reg[5]_i_206_n_5 ),
        .I1(\Ymap_reg[5]_i_205_n_5 ),
        .I2(\Ymap_reg[5]_i_145_n_1 ),
        .I3(\Ymap_reg[5]_i_205_n_4 ),
        .I4(\Ymap_reg[5]_i_206_n_4 ),
        .O(\Ymap[5]_i_142_n_0 ));
  LUT6 #(
    .INIT(64'hE81717E817E8E817)) 
    \Ymap[5]_i_143 
       (.I0(\Ymap_reg[5]_i_206_n_6 ),
        .I1(\Ymap_reg[5]_i_205_n_6 ),
        .I2(\Ymap_reg[5]_i_145_n_6 ),
        .I3(\Ymap_reg[5]_i_145_n_1 ),
        .I4(\Ymap_reg[5]_i_205_n_5 ),
        .I5(\Ymap_reg[5]_i_206_n_5 ),
        .O(\Ymap[5]_i_143_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[5]_i_144 
       (.I0(\Ymap_reg[5]_i_206_n_7 ),
        .I1(\Ymap_reg[5]_i_205_n_7 ),
        .I2(\Ymap_reg[5]_i_145_n_7 ),
        .I3(\Ymap_reg[5]_i_145_n_6 ),
        .I4(\Ymap_reg[5]_i_205_n_6 ),
        .I5(\Ymap_reg[5]_i_206_n_6 ),
        .O(\Ymap[5]_i_144_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[5]_i_15 
       (.I0(\Ymap_reg[5]_i_35_n_6 ),
        .I1(\Ymap_reg[5]_i_36_n_6 ),
        .I2(\Ymap_reg[5]_i_37_n_6 ),
        .O(\Ymap[5]_i_15_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Ymap[5]_i_151 
       (.I0(\Ymap_reg[5]_i_131_n_6 ),
        .I1(\cnt_reg_n_0_[14] ),
        .O(\Ymap[5]_i_151_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Ymap[5]_i_152 
       (.I0(\Ymap_reg[5]_i_131_n_7 ),
        .I1(\cnt_reg_n_0_[13] ),
        .O(\Ymap[5]_i_152_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Ymap[5]_i_153 
       (.I0(\Ymap_reg[5]_i_191_n_4 ),
        .I1(\cnt_reg_n_0_[12] ),
        .O(\Ymap[5]_i_153_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Ymap[5]_i_154 
       (.I0(\Ymap_reg[5]_i_191_n_5 ),
        .I1(\cnt_reg_n_0_[11] ),
        .O(\Ymap[5]_i_154_n_0 ));
  LUT4 #(
    .INIT(16'hB44B)) 
    \Ymap[5]_i_155 
       (.I0(\cnt_reg_n_0_[14] ),
        .I1(\Ymap_reg[5]_i_131_n_6 ),
        .I2(\cnt_reg_n_0_[15] ),
        .I3(\Ymap_reg[5]_i_131_n_5 ),
        .O(\Ymap[5]_i_155_n_0 ));
  LUT4 #(
    .INIT(16'hB44B)) 
    \Ymap[5]_i_156 
       (.I0(\cnt_reg_n_0_[13] ),
        .I1(\Ymap_reg[5]_i_131_n_7 ),
        .I2(\cnt_reg_n_0_[14] ),
        .I3(\Ymap_reg[5]_i_131_n_6 ),
        .O(\Ymap[5]_i_156_n_0 ));
  LUT4 #(
    .INIT(16'hB44B)) 
    \Ymap[5]_i_157 
       (.I0(\cnt_reg_n_0_[12] ),
        .I1(\Ymap_reg[5]_i_191_n_4 ),
        .I2(\cnt_reg_n_0_[13] ),
        .I3(\Ymap_reg[5]_i_131_n_7 ),
        .O(\Ymap[5]_i_157_n_0 ));
  LUT4 #(
    .INIT(16'hB44B)) 
    \Ymap[5]_i_158 
       (.I0(\cnt_reg_n_0_[11] ),
        .I1(\Ymap_reg[5]_i_191_n_5 ),
        .I2(\cnt_reg_n_0_[12] ),
        .I3(\Ymap_reg[5]_i_191_n_4 ),
        .O(\Ymap[5]_i_158_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \Ymap[5]_i_159 
       (.I0(\cnt_reg_n_0_[27] ),
        .I1(\cnt_reg_n_0_[29] ),
        .O(\Ymap[5]_i_159_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[5]_i_16 
       (.I0(\Ymap_reg[5]_i_35_n_7 ),
        .I1(\Ymap_reg[5]_i_36_n_7 ),
        .I2(\Ymap_reg[5]_i_37_n_7 ),
        .O(\Ymap[5]_i_16_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[5]_i_160 
       (.I0(\cnt_reg_n_0_[27] ),
        .I1(\cnt_reg_n_0_[25] ),
        .I2(\cnt_reg_n_0_[29] ),
        .O(\Ymap[5]_i_160_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[5]_i_161 
       (.I0(\cnt_reg_n_0_[26] ),
        .I1(\cnt_reg_n_0_[24] ),
        .I2(\cnt_reg_n_0_[28] ),
        .O(\Ymap[5]_i_161_n_0 ));
  LUT3 #(
    .INIT(8'hD4)) 
    \Ymap[5]_i_162 
       (.I0(\cnt_reg_n_0_[27] ),
        .I1(\cnt_reg_n_0_[23] ),
        .I2(\cnt_reg_n_0_[25] ),
        .O(\Ymap[5]_i_162_n_0 ));
  LUT5 #(
    .INIT(32'h718E8E71)) 
    \Ymap[5]_i_163 
       (.I0(\cnt_reg_n_0_[26] ),
        .I1(\cnt_reg_n_0_[28] ),
        .I2(\cnt_reg_n_0_[30] ),
        .I3(\cnt_reg_n_0_[29] ),
        .I4(\cnt_reg_n_0_[27] ),
        .O(\Ymap[5]_i_163_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[5]_i_164 
       (.I0(\cnt_reg_n_0_[29] ),
        .I1(\cnt_reg_n_0_[25] ),
        .I2(\cnt_reg_n_0_[27] ),
        .I3(\cnt_reg_n_0_[26] ),
        .I4(\cnt_reg_n_0_[30] ),
        .I5(\cnt_reg_n_0_[28] ),
        .O(\Ymap[5]_i_164_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[5]_i_165 
       (.I0(\cnt_reg_n_0_[28] ),
        .I1(\cnt_reg_n_0_[24] ),
        .I2(\cnt_reg_n_0_[26] ),
        .I3(\cnt_reg_n_0_[25] ),
        .I4(\cnt_reg_n_0_[27] ),
        .I5(\cnt_reg_n_0_[29] ),
        .O(\Ymap[5]_i_165_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Ymap[5]_i_166 
       (.I0(\cnt_reg_n_0_[25] ),
        .I1(\cnt_reg_n_0_[23] ),
        .I2(\cnt_reg_n_0_[27] ),
        .I3(\cnt_reg_n_0_[28] ),
        .I4(\cnt_reg_n_0_[24] ),
        .I5(\cnt_reg_n_0_[26] ),
        .O(\Ymap[5]_i_166_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \Ymap[5]_i_167 
       (.I0(\cnt_reg_n_0_[30] ),
        .O(\Ymap[5]_i_167_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \Ymap[5]_i_168 
       (.I0(\cnt_reg_n_0_[29] ),
        .I1(\cnt_reg_n_0_[30] ),
        .O(\Ymap[5]_i_168_n_0 ));
  LUT3 #(
    .INIT(8'hD4)) 
    \Ymap[5]_i_169 
       (.I0(\cnt_reg_n_0_[26] ),
        .I1(\cnt_reg_n_0_[22] ),
        .I2(\cnt_reg_n_0_[24] ),
        .O(\Ymap[5]_i_169_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[5]_i_17 
       (.I0(\Ymap_reg[5]_i_37_n_4 ),
        .I1(\Ymap_reg[5]_i_36_n_4 ),
        .I2(\Ymap_reg[5]_i_35_n_4 ),
        .I3(\Ymap_reg[5]_i_38_n_7 ),
        .I4(\Ymap_reg[5]_i_39_n_7 ),
        .I5(\Ymap_reg[5]_i_40_n_7 ),
        .O(\Ymap[5]_i_17_n_0 ));
  LUT3 #(
    .INIT(8'hD4)) 
    \Ymap[5]_i_170 
       (.I0(\cnt_reg_n_0_[25] ),
        .I1(\cnt_reg_n_0_[21] ),
        .I2(\cnt_reg_n_0_[23] ),
        .O(\Ymap[5]_i_170_n_0 ));
  LUT3 #(
    .INIT(8'hD4)) 
    \Ymap[5]_i_171 
       (.I0(\cnt_reg_n_0_[24] ),
        .I1(\cnt_reg_n_0_[20] ),
        .I2(\cnt_reg_n_0_[22] ),
        .O(\Ymap[5]_i_171_n_0 ));
  LUT3 #(
    .INIT(8'hD4)) 
    \Ymap[5]_i_172 
       (.I0(\cnt_reg_n_0_[23] ),
        .I1(\cnt_reg_n_0_[19] ),
        .I2(\cnt_reg_n_0_[21] ),
        .O(\Ymap[5]_i_172_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Ymap[5]_i_173 
       (.I0(\cnt_reg_n_0_[24] ),
        .I1(\cnt_reg_n_0_[22] ),
        .I2(\cnt_reg_n_0_[26] ),
        .I3(\cnt_reg_n_0_[27] ),
        .I4(\cnt_reg_n_0_[23] ),
        .I5(\cnt_reg_n_0_[25] ),
        .O(\Ymap[5]_i_173_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Ymap[5]_i_174 
       (.I0(\cnt_reg_n_0_[23] ),
        .I1(\cnt_reg_n_0_[21] ),
        .I2(\cnt_reg_n_0_[25] ),
        .I3(\cnt_reg_n_0_[22] ),
        .I4(\cnt_reg_n_0_[24] ),
        .I5(\cnt_reg_n_0_[26] ),
        .O(\Ymap[5]_i_174_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Ymap[5]_i_175 
       (.I0(\cnt_reg_n_0_[22] ),
        .I1(\cnt_reg_n_0_[20] ),
        .I2(\cnt_reg_n_0_[24] ),
        .I3(\cnt_reg_n_0_[21] ),
        .I4(\cnt_reg_n_0_[23] ),
        .I5(\cnt_reg_n_0_[25] ),
        .O(\Ymap[5]_i_175_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Ymap[5]_i_176 
       (.I0(\cnt_reg_n_0_[21] ),
        .I1(\cnt_reg_n_0_[19] ),
        .I2(\cnt_reg_n_0_[23] ),
        .I3(\cnt_reg_n_0_[24] ),
        .I4(\cnt_reg_n_0_[20] ),
        .I5(\cnt_reg_n_0_[22] ),
        .O(\Ymap[5]_i_176_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \Ymap[5]_i_177 
       (.I0(\cnt_reg_n_0_[29] ),
        .O(\Ymap[5]_i_177_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Ymap[5]_i_178 
       (.I0(\cnt_reg_n_0_[27] ),
        .I1(\cnt_reg_n_0_[29] ),
        .O(\Ymap[5]_i_178_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Ymap[5]_i_179 
       (.I0(\cnt_reg_n_0_[26] ),
        .I1(\cnt_reg_n_0_[28] ),
        .O(\Ymap[5]_i_179_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[5]_i_18 
       (.I0(\Ymap_reg[5]_i_37_n_5 ),
        .I1(\Ymap_reg[5]_i_36_n_5 ),
        .I2(\Ymap_reg[5]_i_35_n_5 ),
        .I3(\Ymap_reg[5]_i_35_n_4 ),
        .I4(\Ymap_reg[5]_i_36_n_4 ),
        .I5(\Ymap_reg[5]_i_37_n_4 ),
        .O(\Ymap[5]_i_18_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \Ymap[5]_i_180 
       (.I0(\cnt_reg_n_0_[26] ),
        .I1(\cnt_reg_n_0_[28] ),
        .O(\Ymap[5]_i_180_n_0 ));
  LUT3 #(
    .INIT(8'h4B)) 
    \Ymap[5]_i_181 
       (.I0(\cnt_reg_n_0_[30] ),
        .I1(\cnt_reg_n_0_[28] ),
        .I2(\cnt_reg_n_0_[29] ),
        .O(\Ymap[5]_i_181_n_0 ));
  LUT4 #(
    .INIT(16'hB44B)) 
    \Ymap[5]_i_182 
       (.I0(\cnt_reg_n_0_[29] ),
        .I1(\cnt_reg_n_0_[27] ),
        .I2(\cnt_reg_n_0_[28] ),
        .I3(\cnt_reg_n_0_[30] ),
        .O(\Ymap[5]_i_182_n_0 ));
  LUT4 #(
    .INIT(16'hB44B)) 
    \Ymap[5]_i_183 
       (.I0(\cnt_reg_n_0_[28] ),
        .I1(\cnt_reg_n_0_[26] ),
        .I2(\cnt_reg_n_0_[29] ),
        .I3(\cnt_reg_n_0_[27] ),
        .O(\Ymap[5]_i_183_n_0 ));
  LUT5 #(
    .INIT(32'h2BD4D42B)) 
    \Ymap[5]_i_184 
       (.I0(\cnt_reg_n_0_[27] ),
        .I1(\cnt_reg_n_0_[25] ),
        .I2(\cnt_reg_n_0_[30] ),
        .I3(\cnt_reg_n_0_[28] ),
        .I4(\cnt_reg_n_0_[26] ),
        .O(\Ymap[5]_i_184_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \Ymap[5]_i_185 
       (.I0(\cnt_reg_n_0_[29] ),
        .O(\Ymap[5]_i_185_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \Ymap[5]_i_186 
       (.I0(\cnt_reg_n_0_[30] ),
        .I1(\cnt_reg_n_0_[28] ),
        .O(\Ymap[5]_i_186_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \Ymap[5]_i_187 
       (.I0(\cnt_reg_n_0_[30] ),
        .O(\Ymap[5]_i_187_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \Ymap[5]_i_188 
       (.I0(\cnt_reg_n_0_[29] ),
        .I1(\cnt_reg_n_0_[30] ),
        .O(\Ymap[5]_i_188_n_0 ));
  LUT3 #(
    .INIT(8'hE1)) 
    \Ymap[5]_i_189 
       (.I0(\cnt_reg_n_0_[30] ),
        .I1(\cnt_reg_n_0_[28] ),
        .I2(\cnt_reg_n_0_[29] ),
        .O(\Ymap[5]_i_189_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[5]_i_19 
       (.I0(\Ymap_reg[5]_i_37_n_6 ),
        .I1(\Ymap_reg[5]_i_36_n_6 ),
        .I2(\Ymap_reg[5]_i_35_n_6 ),
        .I3(\Ymap_reg[5]_i_35_n_5 ),
        .I4(\Ymap_reg[5]_i_36_n_5 ),
        .I5(\Ymap_reg[5]_i_37_n_5 ),
        .O(\Ymap[5]_i_19_n_0 ));
  LUT4 #(
    .INIT(16'h1EE1)) 
    \Ymap[5]_i_190 
       (.I0(\cnt_reg_n_0_[27] ),
        .I1(\cnt_reg_n_0_[29] ),
        .I2(\cnt_reg_n_0_[28] ),
        .I3(\cnt_reg_n_0_[30] ),
        .O(\Ymap[5]_i_190_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[5]_i_193 
       (.I0(\Ymap_reg[5]_i_192_n_4 ),
        .I1(\Ymap_reg[5]_i_192_n_6 ),
        .O(\Ymap[5]_i_193_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[5]_i_194 
       (.I0(\Ymap_reg[5]_i_192_n_5 ),
        .I1(\Ymap_reg[5]_i_192_n_7 ),
        .O(\Ymap[5]_i_194_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[5]_i_195 
       (.I0(\Ymap_reg[5]_i_192_n_6 ),
        .I1(\Ymap_reg[5]_i_3_n_4 ),
        .O(\Ymap[5]_i_195_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[5]_i_196 
       (.I0(\Ymap_reg[5]_i_192_n_7 ),
        .I1(\Ymap_reg[5]_i_3_n_5 ),
        .O(\Ymap[5]_i_196_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[5]_i_197 
       (.I0(\Ymap_reg[5]_i_207_n_4 ),
        .I1(\Ymap_reg[5]_i_247_n_4 ),
        .I2(\Ymap_reg[5]_i_248_n_4 ),
        .O(\Ymap[5]_i_197_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[5]_i_198 
       (.I0(\Ymap_reg[5]_i_207_n_5 ),
        .I1(\Ymap_reg[5]_i_247_n_5 ),
        .I2(\Ymap_reg[5]_i_248_n_5 ),
        .O(\Ymap[5]_i_198_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[5]_i_199 
       (.I0(\Ymap_reg[5]_i_207_n_6 ),
        .I1(\Ymap_reg[5]_i_247_n_6 ),
        .I2(\Ymap_reg[5]_i_248_n_6 ),
        .O(\Ymap[5]_i_199_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[5]_i_20 
       (.I0(\Ymap_reg[5]_i_37_n_7 ),
        .I1(\Ymap_reg[5]_i_36_n_7 ),
        .I2(\Ymap_reg[5]_i_35_n_7 ),
        .I3(\Ymap_reg[5]_i_35_n_6 ),
        .I4(\Ymap_reg[5]_i_36_n_6 ),
        .I5(\Ymap_reg[5]_i_37_n_6 ),
        .O(\Ymap[5]_i_20_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[5]_i_200 
       (.I0(\Ymap_reg[5]_i_207_n_7 ),
        .I1(\Ymap_reg[5]_i_247_n_7 ),
        .I2(\Ymap_reg[5]_i_248_n_7 ),
        .O(\Ymap[5]_i_200_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[5]_i_201 
       (.I0(\Ymap_reg[5]_i_248_n_4 ),
        .I1(\Ymap_reg[5]_i_247_n_4 ),
        .I2(\Ymap_reg[5]_i_207_n_4 ),
        .I3(\Ymap_reg[5]_i_145_n_7 ),
        .I4(\Ymap_reg[5]_i_205_n_7 ),
        .I5(\Ymap_reg[5]_i_206_n_7 ),
        .O(\Ymap[5]_i_201_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[5]_i_202 
       (.I0(\Ymap_reg[5]_i_248_n_5 ),
        .I1(\Ymap_reg[5]_i_247_n_5 ),
        .I2(\Ymap_reg[5]_i_207_n_5 ),
        .I3(\Ymap_reg[5]_i_207_n_4 ),
        .I4(\Ymap_reg[5]_i_247_n_4 ),
        .I5(\Ymap_reg[5]_i_248_n_4 ),
        .O(\Ymap[5]_i_202_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[5]_i_203 
       (.I0(\Ymap_reg[5]_i_248_n_6 ),
        .I1(\Ymap_reg[5]_i_247_n_6 ),
        .I2(\Ymap_reg[5]_i_207_n_6 ),
        .I3(\Ymap_reg[5]_i_207_n_5 ),
        .I4(\Ymap_reg[5]_i_247_n_5 ),
        .I5(\Ymap_reg[5]_i_248_n_5 ),
        .O(\Ymap[5]_i_203_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[5]_i_204 
       (.I0(\Ymap_reg[5]_i_248_n_7 ),
        .I1(\Ymap_reg[5]_i_247_n_7 ),
        .I2(\Ymap_reg[5]_i_207_n_7 ),
        .I3(\Ymap_reg[5]_i_207_n_6 ),
        .I4(\Ymap_reg[5]_i_247_n_6 ),
        .I5(\Ymap_reg[5]_i_248_n_6 ),
        .O(\Ymap[5]_i_204_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \Ymap[5]_i_208 
       (.I0(\cnt_reg_n_0_[30] ),
        .O(\Ymap[5]_i_208_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \Ymap[5]_i_209 
       (.I0(\cnt_reg_n_0_[29] ),
        .I1(\cnt_reg_n_0_[30] ),
        .O(\Ymap[5]_i_209_n_0 ));
  (* HLUTNM = "lutpair0" *) 
  LUT3 #(
    .INIT(8'h2B)) 
    \Ymap[5]_i_210 
       (.I0(\cnt_reg_n_0_[29] ),
        .I1(\Ymap_reg[5]_i_127_n_1 ),
        .I2(\Ymap_reg[5]_i_270_n_3 ),
        .O(\Ymap[5]_i_210_n_0 ));
  LUT3 #(
    .INIT(8'h2B)) 
    \Ymap[5]_i_211 
       (.I0(\cnt_reg_n_0_[28] ),
        .I1(\Ymap_reg[5]_i_127_n_1 ),
        .I2(\Ymap_reg[5]_i_270_n_3 ),
        .O(\Ymap[5]_i_211_n_0 ));
  LUT3 #(
    .INIT(8'h2B)) 
    \Ymap[5]_i_212 
       (.I0(\cnt_reg_n_0_[27] ),
        .I1(\Ymap_reg[5]_i_127_n_1 ),
        .I2(\Ymap_reg[5]_i_270_n_3 ),
        .O(\Ymap[5]_i_212_n_0 ));
  LUT3 #(
    .INIT(8'h2B)) 
    \Ymap[5]_i_213 
       (.I0(\cnt_reg_n_0_[26] ),
        .I1(\Ymap_reg[5]_i_127_n_1 ),
        .I2(\Ymap_reg[5]_i_270_n_3 ),
        .O(\Ymap[5]_i_213_n_0 ));
  LUT4 #(
    .INIT(16'h6996)) 
    \Ymap[5]_i_214 
       (.I0(\Ymap[5]_i_210_n_0 ),
        .I1(\cnt_reg_n_0_[30] ),
        .I2(\Ymap_reg[5]_i_127_n_1 ),
        .I3(\Ymap_reg[5]_i_270_n_3 ),
        .O(\Ymap[5]_i_214_n_0 ));
  (* HLUTNM = "lutpair0" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    \Ymap[5]_i_215 
       (.I0(\cnt_reg_n_0_[29] ),
        .I1(\Ymap_reg[5]_i_127_n_1 ),
        .I2(\Ymap_reg[5]_i_270_n_3 ),
        .I3(\Ymap[5]_i_211_n_0 ),
        .O(\Ymap[5]_i_215_n_0 ));
  LUT4 #(
    .INIT(16'h6996)) 
    \Ymap[5]_i_216 
       (.I0(\cnt_reg_n_0_[28] ),
        .I1(\Ymap_reg[5]_i_127_n_1 ),
        .I2(\Ymap_reg[5]_i_270_n_3 ),
        .I3(\Ymap[5]_i_212_n_0 ),
        .O(\Ymap[5]_i_216_n_0 ));
  LUT4 #(
    .INIT(16'h6996)) 
    \Ymap[5]_i_217 
       (.I0(\cnt_reg_n_0_[27] ),
        .I1(\Ymap_reg[5]_i_127_n_1 ),
        .I2(\Ymap_reg[5]_i_270_n_3 ),
        .I3(\Ymap[5]_i_213_n_0 ),
        .O(\Ymap[5]_i_217_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \Ymap[5]_i_218 
       (.I0(\cnt_reg_n_0_[29] ),
        .O(\Ymap[5]_i_218_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \Ymap[5]_i_219 
       (.I0(\cnt_reg_n_0_[30] ),
        .I1(\cnt_reg_n_0_[28] ),
        .O(\Ymap[5]_i_219_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \Ymap[5]_i_22 
       (.I0(\Ymap_reg[5]_i_42_n_4 ),
        .O(\Ymap[5]_i_22_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \Ymap[5]_i_220 
       (.I0(\cnt_reg_n_0_[30] ),
        .O(\Ymap[5]_i_220_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \Ymap[5]_i_221 
       (.I0(\cnt_reg_n_0_[29] ),
        .I1(\cnt_reg_n_0_[30] ),
        .O(\Ymap[5]_i_221_n_0 ));
  LUT3 #(
    .INIT(8'hE1)) 
    \Ymap[5]_i_222 
       (.I0(\cnt_reg_n_0_[30] ),
        .I1(\cnt_reg_n_0_[28] ),
        .I2(\cnt_reg_n_0_[29] ),
        .O(\Ymap[5]_i_222_n_0 ));
  LUT4 #(
    .INIT(16'h1EE1)) 
    \Ymap[5]_i_223 
       (.I0(\cnt_reg_n_0_[27] ),
        .I1(\cnt_reg_n_0_[29] ),
        .I2(\cnt_reg_n_0_[28] ),
        .I3(\cnt_reg_n_0_[30] ),
        .O(\Ymap[5]_i_223_n_0 ));
  LUT3 #(
    .INIT(8'h17)) 
    \Ymap[5]_i_224 
       (.I0(\cnt_reg_n_0_[30] ),
        .I1(\Ymap_reg[5]_i_127_n_1 ),
        .I2(\Ymap_reg[5]_i_270_n_3 ),
        .O(\Ymap[5]_i_224_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Ymap[5]_i_226 
       (.I0(\Ymap_reg[5]_i_191_n_6 ),
        .I1(\cnt_reg_n_0_[10] ),
        .O(\Ymap[5]_i_226_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Ymap[5]_i_227 
       (.I0(\Ymap_reg[5]_i_191_n_7 ),
        .I1(\cnt_reg_n_0_[9] ),
        .O(\Ymap[5]_i_227_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Ymap[5]_i_228 
       (.I0(\Ymap_reg[5]_i_234_n_4 ),
        .I1(\cnt_reg_n_0_[8] ),
        .O(\Ymap[5]_i_228_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Ymap[5]_i_229 
       (.I0(\Ymap_reg[5]_i_234_n_5 ),
        .I1(\cnt_reg_n_0_[7] ),
        .O(\Ymap[5]_i_229_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \Ymap[5]_i_23 
       (.I0(\Ymap_reg[5]_i_42_n_5 ),
        .O(\Ymap[5]_i_23_n_0 ));
  LUT4 #(
    .INIT(16'hB44B)) 
    \Ymap[5]_i_230 
       (.I0(\cnt_reg_n_0_[10] ),
        .I1(\Ymap_reg[5]_i_191_n_6 ),
        .I2(\cnt_reg_n_0_[11] ),
        .I3(\Ymap_reg[5]_i_191_n_5 ),
        .O(\Ymap[5]_i_230_n_0 ));
  LUT4 #(
    .INIT(16'hB44B)) 
    \Ymap[5]_i_231 
       (.I0(\cnt_reg_n_0_[9] ),
        .I1(\Ymap_reg[5]_i_191_n_7 ),
        .I2(\cnt_reg_n_0_[10] ),
        .I3(\Ymap_reg[5]_i_191_n_6 ),
        .O(\Ymap[5]_i_231_n_0 ));
  LUT4 #(
    .INIT(16'hB44B)) 
    \Ymap[5]_i_232 
       (.I0(\cnt_reg_n_0_[8] ),
        .I1(\Ymap_reg[5]_i_234_n_4 ),
        .I2(\cnt_reg_n_0_[9] ),
        .I3(\Ymap_reg[5]_i_191_n_7 ),
        .O(\Ymap[5]_i_232_n_0 ));
  LUT4 #(
    .INIT(16'hB44B)) 
    \Ymap[5]_i_233 
       (.I0(\cnt_reg_n_0_[7] ),
        .I1(\Ymap_reg[5]_i_234_n_5 ),
        .I2(\cnt_reg_n_0_[8] ),
        .I3(\Ymap_reg[5]_i_234_n_4 ),
        .O(\Ymap[5]_i_233_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[5]_i_235 
       (.I0(\Ymap_reg[5]_i_3_n_4 ),
        .I1(\Ymap_reg[5]_i_3_n_6 ),
        .O(\Ymap[5]_i_235_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[5]_i_236 
       (.I0(\Ymap_reg[5]_i_3_n_5 ),
        .I1(\Ymap_reg[5]_i_3_n_7 ),
        .O(\Ymap[5]_i_236_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[5]_i_237 
       (.I0(\Ymap_reg[5]_i_3_n_6 ),
        .I1(\Ymap_reg[4]_i_2_n_4 ),
        .O(\Ymap[5]_i_237_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[5]_i_238 
       (.I0(\Ymap_reg[5]_i_3_n_7 ),
        .I1(\Ymap_reg[4]_i_2_n_5 ),
        .O(\Ymap[5]_i_238_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[5]_i_239 
       (.I0(\Ymap_reg[5]_i_38_n_4 ),
        .I1(\Ymap_reg[5]_i_39_n_4 ),
        .I2(\Ymap_reg[5]_i_40_n_4 ),
        .O(\Ymap[5]_i_239_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \Ymap[5]_i_24 
       (.I0(\Ymap_reg[5]_i_3_n_7 ),
        .O(\Ymap[5]_i_24_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[5]_i_240 
       (.I0(\Ymap_reg[5]_i_38_n_5 ),
        .I1(\Ymap_reg[5]_i_39_n_5 ),
        .I2(\Ymap_reg[5]_i_40_n_5 ),
        .O(\Ymap[5]_i_240_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[5]_i_241 
       (.I0(\Ymap_reg[5]_i_38_n_6 ),
        .I1(\Ymap_reg[5]_i_39_n_6 ),
        .I2(\Ymap_reg[5]_i_40_n_6 ),
        .O(\Ymap[5]_i_241_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[5]_i_242 
       (.I0(\Ymap_reg[5]_i_38_n_7 ),
        .I1(\Ymap_reg[5]_i_39_n_7 ),
        .I2(\Ymap_reg[5]_i_40_n_7 ),
        .O(\Ymap[5]_i_242_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[5]_i_243 
       (.I0(\Ymap_reg[5]_i_40_n_4 ),
        .I1(\Ymap_reg[5]_i_39_n_4 ),
        .I2(\Ymap_reg[5]_i_38_n_4 ),
        .I3(\Ymap_reg[5]_i_207_n_7 ),
        .I4(\Ymap_reg[5]_i_247_n_7 ),
        .I5(\Ymap_reg[5]_i_248_n_7 ),
        .O(\Ymap[5]_i_243_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[5]_i_244 
       (.I0(\Ymap_reg[5]_i_40_n_5 ),
        .I1(\Ymap_reg[5]_i_39_n_5 ),
        .I2(\Ymap_reg[5]_i_38_n_5 ),
        .I3(\Ymap_reg[5]_i_38_n_4 ),
        .I4(\Ymap_reg[5]_i_39_n_4 ),
        .I5(\Ymap_reg[5]_i_40_n_4 ),
        .O(\Ymap[5]_i_244_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[5]_i_245 
       (.I0(\Ymap_reg[5]_i_40_n_6 ),
        .I1(\Ymap_reg[5]_i_39_n_6 ),
        .I2(\Ymap_reg[5]_i_38_n_6 ),
        .I3(\Ymap_reg[5]_i_38_n_5 ),
        .I4(\Ymap_reg[5]_i_39_n_5 ),
        .I5(\Ymap_reg[5]_i_40_n_5 ),
        .O(\Ymap[5]_i_245_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[5]_i_246 
       (.I0(\Ymap_reg[5]_i_40_n_7 ),
        .I1(\Ymap_reg[5]_i_39_n_7 ),
        .I2(\Ymap_reg[5]_i_38_n_7 ),
        .I3(\Ymap_reg[5]_i_38_n_6 ),
        .I4(\Ymap_reg[5]_i_39_n_6 ),
        .I5(\Ymap_reg[5]_i_40_n_6 ),
        .O(\Ymap[5]_i_246_n_0 ));
  LUT3 #(
    .INIT(8'h2B)) 
    \Ymap[5]_i_249 
       (.I0(\cnt_reg_n_0_[25] ),
        .I1(\Ymap_reg[5]_i_127_n_1 ),
        .I2(\Ymap_reg[5]_i_270_n_3 ),
        .O(\Ymap[5]_i_249_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \Ymap[5]_i_25 
       (.I0(\Ymap_reg[4]_i_2_n_4 ),
        .O(\Ymap[5]_i_25_n_0 ));
  LUT3 #(
    .INIT(8'h2B)) 
    \Ymap[5]_i_250 
       (.I0(\cnt_reg_n_0_[24] ),
        .I1(\Ymap_reg[5]_i_127_n_1 ),
        .I2(\Ymap_reg[5]_i_270_n_3 ),
        .O(\Ymap[5]_i_250_n_0 ));
  LUT3 #(
    .INIT(8'h2B)) 
    \Ymap[5]_i_251 
       (.I0(\cnt_reg_n_0_[23] ),
        .I1(\Ymap_reg[5]_i_127_n_1 ),
        .I2(\Ymap_reg[5]_i_270_n_3 ),
        .O(\Ymap[5]_i_251_n_0 ));
  LUT3 #(
    .INIT(8'h2B)) 
    \Ymap[5]_i_252 
       (.I0(\cnt_reg_n_0_[22] ),
        .I1(\Ymap_reg[5]_i_127_n_1 ),
        .I2(\Ymap_reg[5]_i_270_n_3 ),
        .O(\Ymap[5]_i_252_n_0 ));
  LUT4 #(
    .INIT(16'h6996)) 
    \Ymap[5]_i_253 
       (.I0(\cnt_reg_n_0_[26] ),
        .I1(\Ymap_reg[5]_i_127_n_1 ),
        .I2(\Ymap_reg[5]_i_270_n_3 ),
        .I3(\Ymap[5]_i_249_n_0 ),
        .O(\Ymap[5]_i_253_n_0 ));
  LUT4 #(
    .INIT(16'h6996)) 
    \Ymap[5]_i_254 
       (.I0(\cnt_reg_n_0_[25] ),
        .I1(\Ymap_reg[5]_i_127_n_1 ),
        .I2(\Ymap_reg[5]_i_270_n_3 ),
        .I3(\Ymap[5]_i_250_n_0 ),
        .O(\Ymap[5]_i_254_n_0 ));
  LUT4 #(
    .INIT(16'h6996)) 
    \Ymap[5]_i_255 
       (.I0(\cnt_reg_n_0_[24] ),
        .I1(\Ymap_reg[5]_i_127_n_1 ),
        .I2(\Ymap_reg[5]_i_270_n_3 ),
        .I3(\Ymap[5]_i_251_n_0 ),
        .O(\Ymap[5]_i_255_n_0 ));
  LUT4 #(
    .INIT(16'h6996)) 
    \Ymap[5]_i_256 
       (.I0(\cnt_reg_n_0_[23] ),
        .I1(\Ymap_reg[5]_i_127_n_1 ),
        .I2(\Ymap_reg[5]_i_270_n_3 ),
        .I3(\Ymap[5]_i_252_n_0 ),
        .O(\Ymap[5]_i_256_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \Ymap[5]_i_257 
       (.I0(\cnt_reg_n_0_[27] ),
        .I1(\cnt_reg_n_0_[29] ),
        .O(\Ymap[5]_i_257_n_0 ));
  LUT5 #(
    .INIT(32'h718E8E71)) 
    \Ymap[5]_i_258 
       (.I0(\cnt_reg_n_0_[26] ),
        .I1(\cnt_reg_n_0_[28] ),
        .I2(\cnt_reg_n_0_[30] ),
        .I3(\cnt_reg_n_0_[29] ),
        .I4(\cnt_reg_n_0_[27] ),
        .O(\Ymap[5]_i_258_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[5]_i_259 
       (.I0(\cnt_reg_n_0_[29] ),
        .I1(\cnt_reg_n_0_[25] ),
        .I2(\cnt_reg_n_0_[27] ),
        .I3(\cnt_reg_n_0_[26] ),
        .I4(\cnt_reg_n_0_[30] ),
        .I5(\cnt_reg_n_0_[28] ),
        .O(\Ymap[5]_i_259_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[5]_i_260 
       (.I0(\cnt_reg_n_0_[28] ),
        .I1(\cnt_reg_n_0_[24] ),
        .I2(\cnt_reg_n_0_[26] ),
        .I3(\cnt_reg_n_0_[25] ),
        .I4(\cnt_reg_n_0_[27] ),
        .I5(\cnt_reg_n_0_[29] ),
        .O(\Ymap[5]_i_260_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Ymap[5]_i_261 
       (.I0(\cnt_reg_n_0_[25] ),
        .I1(\cnt_reg_n_0_[23] ),
        .I2(\cnt_reg_n_0_[27] ),
        .I3(\cnt_reg_n_0_[28] ),
        .I4(\cnt_reg_n_0_[24] ),
        .I5(\cnt_reg_n_0_[26] ),
        .O(\Ymap[5]_i_261_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \Ymap[5]_i_262 
       (.I0(\cnt_reg_n_0_[29] ),
        .O(\Ymap[5]_i_262_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Ymap[5]_i_263 
       (.I0(\cnt_reg_n_0_[27] ),
        .I1(\cnt_reg_n_0_[29] ),
        .O(\Ymap[5]_i_263_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Ymap[5]_i_264 
       (.I0(\cnt_reg_n_0_[26] ),
        .I1(\cnt_reg_n_0_[28] ),
        .O(\Ymap[5]_i_264_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \Ymap[5]_i_265 
       (.I0(\cnt_reg_n_0_[26] ),
        .I1(\cnt_reg_n_0_[28] ),
        .O(\Ymap[5]_i_265_n_0 ));
  LUT3 #(
    .INIT(8'h4B)) 
    \Ymap[5]_i_266 
       (.I0(\cnt_reg_n_0_[30] ),
        .I1(\cnt_reg_n_0_[28] ),
        .I2(\cnt_reg_n_0_[29] ),
        .O(\Ymap[5]_i_266_n_0 ));
  LUT4 #(
    .INIT(16'hB44B)) 
    \Ymap[5]_i_267 
       (.I0(\cnt_reg_n_0_[29] ),
        .I1(\cnt_reg_n_0_[27] ),
        .I2(\cnt_reg_n_0_[28] ),
        .I3(\cnt_reg_n_0_[30] ),
        .O(\Ymap[5]_i_267_n_0 ));
  LUT4 #(
    .INIT(16'hB44B)) 
    \Ymap[5]_i_268 
       (.I0(\cnt_reg_n_0_[28] ),
        .I1(\cnt_reg_n_0_[26] ),
        .I2(\cnt_reg_n_0_[29] ),
        .I3(\cnt_reg_n_0_[27] ),
        .O(\Ymap[5]_i_268_n_0 ));
  LUT5 #(
    .INIT(32'h2BD4D42B)) 
    \Ymap[5]_i_269 
       (.I0(\cnt_reg_n_0_[27] ),
        .I1(\cnt_reg_n_0_[25] ),
        .I2(\cnt_reg_n_0_[30] ),
        .I3(\cnt_reg_n_0_[28] ),
        .I4(\cnt_reg_n_0_[26] ),
        .O(\Ymap[5]_i_269_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Ymap[5]_i_27 
       (.I0(\Ymap_reg[5]_i_21_n_6 ),
        .I1(\cnt_reg_n_0_[26] ),
        .O(\Ymap[5]_i_27_n_0 ));
  LUT2 #(
    .INIT(4'hB)) 
    \Ymap[5]_i_271 
       (.I0(\Ymap_reg[5]_i_234_n_6 ),
        .I1(\cnt_reg_n_0_[6] ),
        .O(\Ymap[5]_i_271_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Ymap[5]_i_272 
       (.I0(\Ymap_reg[5]_i_234_n_7 ),
        .I1(\cnt_reg_n_0_[5] ),
        .O(\Ymap[5]_i_272_n_0 ));
  LUT2 #(
    .INIT(4'hB)) 
    \Ymap[5]_i_273 
       (.I0(\Ymap_reg[0]_i_2_n_4 ),
        .I1(\cnt_reg_n_0_[4] ),
        .O(\Ymap[5]_i_273_n_0 ));
  LUT4 #(
    .INIT(16'h2DD2)) 
    \Ymap[5]_i_274 
       (.I0(\cnt_reg_n_0_[6] ),
        .I1(\Ymap_reg[5]_i_234_n_6 ),
        .I2(\cnt_reg_n_0_[7] ),
        .I3(\Ymap_reg[5]_i_234_n_5 ),
        .O(\Ymap[5]_i_274_n_0 ));
  LUT4 #(
    .INIT(16'h4BB4)) 
    \Ymap[5]_i_275 
       (.I0(\cnt_reg_n_0_[5] ),
        .I1(\Ymap_reg[5]_i_234_n_7 ),
        .I2(\cnt_reg_n_0_[6] ),
        .I3(\Ymap_reg[5]_i_234_n_6 ),
        .O(\Ymap[5]_i_275_n_0 ));
  LUT4 #(
    .INIT(16'h2DD2)) 
    \Ymap[5]_i_276 
       (.I0(\cnt_reg_n_0_[4] ),
        .I1(\Ymap_reg[0]_i_2_n_4 ),
        .I2(\cnt_reg_n_0_[5] ),
        .I3(\Ymap_reg[5]_i_234_n_7 ),
        .O(\Ymap[5]_i_276_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[5]_i_277 
       (.I0(\cnt_reg_n_0_[4] ),
        .I1(\Ymap_reg[0]_i_2_n_4 ),
        .O(\Ymap[5]_i_277_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[5]_i_278 
       (.I0(\Ymap_reg[4]_i_2_n_4 ),
        .I1(\Ymap_reg[4]_i_2_n_6 ),
        .O(\Ymap[5]_i_278_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[5]_i_279 
       (.I0(\Ymap_reg[4]_i_2_n_5 ),
        .I1(\Ymap_reg[4]_i_2_n_7 ),
        .O(\Ymap[5]_i_279_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Ymap[5]_i_28 
       (.I0(\Ymap_reg[5]_i_21_n_7 ),
        .I1(\cnt_reg_n_0_[25] ),
        .O(\Ymap[5]_i_28_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[5]_i_280 
       (.I0(\Ymap_reg[4]_i_2_n_6 ),
        .I1(\Ymap_reg[0]_i_2_n_4 ),
        .O(\Ymap[5]_i_280_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \Ymap[5]_i_281 
       (.I0(\Ymap_reg[4]_i_2_n_7 ),
        .O(\Ymap[5]_i_281_n_0 ));
  LUT3 #(
    .INIT(8'h2B)) 
    \Ymap[5]_i_282 
       (.I0(\cnt_reg_n_0_[21] ),
        .I1(\Ymap_reg[5]_i_127_n_1 ),
        .I2(\Ymap_reg[5]_i_270_n_3 ),
        .O(\Ymap[5]_i_282_n_0 ));
  LUT3 #(
    .INIT(8'h2B)) 
    \Ymap[5]_i_283 
       (.I0(\cnt_reg_n_0_[20] ),
        .I1(\Ymap_reg[5]_i_127_n_1 ),
        .I2(\Ymap_reg[5]_i_270_n_3 ),
        .O(\Ymap[5]_i_283_n_0 ));
  LUT3 #(
    .INIT(8'h2B)) 
    \Ymap[5]_i_284 
       (.I0(\cnt_reg_n_0_[19] ),
        .I1(\Ymap_reg[5]_i_127_n_1 ),
        .I2(\Ymap_reg[5]_i_270_n_3 ),
        .O(\Ymap[5]_i_284_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[5]_i_285 
       (.I0(\cnt_reg_n_0_[18] ),
        .I1(\Ymap_reg[5]_i_130_n_4 ),
        .I2(\Ymap_reg[5]_i_127_n_1 ),
        .O(\Ymap[5]_i_285_n_0 ));
  LUT4 #(
    .INIT(16'h6996)) 
    \Ymap[5]_i_286 
       (.I0(\cnt_reg_n_0_[22] ),
        .I1(\Ymap_reg[5]_i_127_n_1 ),
        .I2(\Ymap_reg[5]_i_270_n_3 ),
        .I3(\Ymap[5]_i_282_n_0 ),
        .O(\Ymap[5]_i_286_n_0 ));
  LUT4 #(
    .INIT(16'h6996)) 
    \Ymap[5]_i_287 
       (.I0(\cnt_reg_n_0_[21] ),
        .I1(\Ymap_reg[5]_i_127_n_1 ),
        .I2(\Ymap_reg[5]_i_270_n_3 ),
        .I3(\Ymap[5]_i_283_n_0 ),
        .O(\Ymap[5]_i_287_n_0 ));
  LUT4 #(
    .INIT(16'h6996)) 
    \Ymap[5]_i_288 
       (.I0(\cnt_reg_n_0_[20] ),
        .I1(\Ymap_reg[5]_i_127_n_1 ),
        .I2(\Ymap_reg[5]_i_270_n_3 ),
        .I3(\Ymap[5]_i_284_n_0 ),
        .O(\Ymap[5]_i_288_n_0 ));
  LUT4 #(
    .INIT(16'h6996)) 
    \Ymap[5]_i_289 
       (.I0(\cnt_reg_n_0_[19] ),
        .I1(\Ymap_reg[5]_i_127_n_1 ),
        .I2(\Ymap_reg[5]_i_270_n_3 ),
        .I3(\Ymap[5]_i_285_n_0 ),
        .O(\Ymap[5]_i_289_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Ymap[5]_i_29 
       (.I0(\Ymap_reg[5]_i_41_n_4 ),
        .I1(\cnt_reg_n_0_[24] ),
        .O(\Ymap[5]_i_29_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Ymap[5]_i_290 
       (.I0(\cnt_reg_n_0_[24] ),
        .I1(\cnt_reg_n_0_[22] ),
        .I2(\cnt_reg_n_0_[26] ),
        .I3(\cnt_reg_n_0_[27] ),
        .I4(\cnt_reg_n_0_[23] ),
        .I5(\cnt_reg_n_0_[25] ),
        .O(\Ymap[5]_i_290_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Ymap[5]_i_291 
       (.I0(\cnt_reg_n_0_[23] ),
        .I1(\cnt_reg_n_0_[21] ),
        .I2(\cnt_reg_n_0_[25] ),
        .I3(\cnt_reg_n_0_[22] ),
        .I4(\cnt_reg_n_0_[24] ),
        .I5(\cnt_reg_n_0_[26] ),
        .O(\Ymap[5]_i_291_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Ymap[5]_i_292 
       (.I0(\cnt_reg_n_0_[22] ),
        .I1(\cnt_reg_n_0_[20] ),
        .I2(\cnt_reg_n_0_[24] ),
        .I3(\cnt_reg_n_0_[21] ),
        .I4(\cnt_reg_n_0_[23] ),
        .I5(\cnt_reg_n_0_[25] ),
        .O(\Ymap[5]_i_292_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Ymap[5]_i_293 
       (.I0(\cnt_reg_n_0_[21] ),
        .I1(\cnt_reg_n_0_[19] ),
        .I2(\cnt_reg_n_0_[23] ),
        .I3(\cnt_reg_n_0_[24] ),
        .I4(\cnt_reg_n_0_[20] ),
        .I5(\cnt_reg_n_0_[22] ),
        .O(\Ymap[5]_i_293_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Ymap[5]_i_30 
       (.I0(\Ymap_reg[5]_i_41_n_5 ),
        .I1(\cnt_reg_n_0_[23] ),
        .O(\Ymap[5]_i_30_n_0 ));
  LUT4 #(
    .INIT(16'hB44B)) 
    \Ymap[5]_i_31 
       (.I0(\cnt_reg_n_0_[26] ),
        .I1(\Ymap_reg[5]_i_21_n_6 ),
        .I2(\cnt_reg_n_0_[27] ),
        .I3(\Ymap_reg[5]_i_21_n_5 ),
        .O(\Ymap[5]_i_31_n_0 ));
  LUT4 #(
    .INIT(16'hB44B)) 
    \Ymap[5]_i_32 
       (.I0(\cnt_reg_n_0_[25] ),
        .I1(\Ymap_reg[5]_i_21_n_7 ),
        .I2(\cnt_reg_n_0_[26] ),
        .I3(\Ymap_reg[5]_i_21_n_6 ),
        .O(\Ymap[5]_i_32_n_0 ));
  LUT4 #(
    .INIT(16'hB44B)) 
    \Ymap[5]_i_33 
       (.I0(\cnt_reg_n_0_[24] ),
        .I1(\Ymap_reg[5]_i_41_n_4 ),
        .I2(\cnt_reg_n_0_[25] ),
        .I3(\Ymap_reg[5]_i_21_n_7 ),
        .O(\Ymap[5]_i_33_n_0 ));
  LUT4 #(
    .INIT(16'hB44B)) 
    \Ymap[5]_i_34 
       (.I0(\cnt_reg_n_0_[23] ),
        .I1(\Ymap_reg[5]_i_41_n_5 ),
        .I2(\cnt_reg_n_0_[24] ),
        .I3(\Ymap_reg[5]_i_41_n_4 ),
        .O(\Ymap[5]_i_34_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[5]_i_43 
       (.I0(\Ymap_reg[5]_i_42_n_4 ),
        .I1(\Ymap_reg[5]_i_42_n_6 ),
        .O(\Ymap[5]_i_43_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[5]_i_44 
       (.I0(\Ymap_reg[5]_i_42_n_5 ),
        .I1(\Ymap_reg[5]_i_42_n_7 ),
        .O(\Ymap[5]_i_44_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[5]_i_45 
       (.I0(\Ymap_reg[5]_i_42_n_6 ),
        .I1(\Ymap_reg[5]_i_105_n_4 ),
        .O(\Ymap[5]_i_45_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Ymap[5]_i_46 
       (.I0(\Ymap_reg[5]_i_42_n_7 ),
        .I1(\Ymap_reg[5]_i_105_n_5 ),
        .O(\Ymap[5]_i_46_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Ymap[5]_i_48 
       (.I0(\Ymap_reg[5]_i_41_n_6 ),
        .I1(\cnt_reg_n_0_[22] ),
        .O(\Ymap[5]_i_48_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Ymap[5]_i_49 
       (.I0(\Ymap_reg[5]_i_41_n_7 ),
        .I1(\cnt_reg_n_0_[21] ),
        .O(\Ymap[5]_i_49_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Ymap[5]_i_50 
       (.I0(\Ymap_reg[5]_i_104_n_4 ),
        .I1(\cnt_reg_n_0_[20] ),
        .O(\Ymap[5]_i_50_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Ymap[5]_i_51 
       (.I0(\Ymap_reg[5]_i_104_n_5 ),
        .I1(\cnt_reg_n_0_[19] ),
        .O(\Ymap[5]_i_51_n_0 ));
  LUT4 #(
    .INIT(16'hB44B)) 
    \Ymap[5]_i_52 
       (.I0(\cnt_reg_n_0_[22] ),
        .I1(\Ymap_reg[5]_i_41_n_6 ),
        .I2(\cnt_reg_n_0_[23] ),
        .I3(\Ymap_reg[5]_i_41_n_5 ),
        .O(\Ymap[5]_i_52_n_0 ));
  LUT4 #(
    .INIT(16'hB44B)) 
    \Ymap[5]_i_53 
       (.I0(\cnt_reg_n_0_[21] ),
        .I1(\Ymap_reg[5]_i_41_n_7 ),
        .I2(\cnt_reg_n_0_[22] ),
        .I3(\Ymap_reg[5]_i_41_n_6 ),
        .O(\Ymap[5]_i_53_n_0 ));
  LUT4 #(
    .INIT(16'hB44B)) 
    \Ymap[5]_i_54 
       (.I0(\cnt_reg_n_0_[20] ),
        .I1(\Ymap_reg[5]_i_104_n_4 ),
        .I2(\cnt_reg_n_0_[21] ),
        .I3(\Ymap_reg[5]_i_41_n_7 ),
        .O(\Ymap[5]_i_54_n_0 ));
  LUT4 #(
    .INIT(16'hB44B)) 
    \Ymap[5]_i_55 
       (.I0(\cnt_reg_n_0_[19] ),
        .I1(\Ymap_reg[5]_i_104_n_5 ),
        .I2(\cnt_reg_n_0_[20] ),
        .I3(\Ymap_reg[5]_i_104_n_4 ),
        .O(\Ymap[5]_i_55_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[5]_i_56 
       (.I0(\cnt_reg_n_0_[25] ),
        .I1(\cnt_reg_n_0_[20] ),
        .I2(\cnt_reg_n_0_[22] ),
        .O(\Ymap[5]_i_56_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[5]_i_57 
       (.I0(\cnt_reg_n_0_[24] ),
        .I1(\cnt_reg_n_0_[19] ),
        .I2(\cnt_reg_n_0_[21] ),
        .O(\Ymap[5]_i_57_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[5]_i_58 
       (.I0(\cnt_reg_n_0_[23] ),
        .I1(\cnt_reg_n_0_[18] ),
        .I2(\cnt_reg_n_0_[20] ),
        .O(\Ymap[5]_i_58_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[5]_i_59 
       (.I0(\cnt_reg_n_0_[22] ),
        .I1(\cnt_reg_n_0_[17] ),
        .I2(\cnt_reg_n_0_[19] ),
        .O(\Ymap[5]_i_59_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[5]_i_60 
       (.I0(\cnt_reg_n_0_[22] ),
        .I1(\cnt_reg_n_0_[20] ),
        .I2(\cnt_reg_n_0_[25] ),
        .I3(\cnt_reg_n_0_[21] ),
        .I4(\cnt_reg_n_0_[23] ),
        .I5(\cnt_reg_n_0_[26] ),
        .O(\Ymap[5]_i_60_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[5]_i_61 
       (.I0(\cnt_reg_n_0_[21] ),
        .I1(\cnt_reg_n_0_[19] ),
        .I2(\cnt_reg_n_0_[24] ),
        .I3(\cnt_reg_n_0_[25] ),
        .I4(\cnt_reg_n_0_[20] ),
        .I5(\cnt_reg_n_0_[22] ),
        .O(\Ymap[5]_i_61_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[5]_i_62 
       (.I0(\cnt_reg_n_0_[20] ),
        .I1(\cnt_reg_n_0_[18] ),
        .I2(\cnt_reg_n_0_[23] ),
        .I3(\cnt_reg_n_0_[24] ),
        .I4(\cnt_reg_n_0_[19] ),
        .I5(\cnt_reg_n_0_[21] ),
        .O(\Ymap[5]_i_62_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[5]_i_63 
       (.I0(\cnt_reg_n_0_[19] ),
        .I1(\cnt_reg_n_0_[17] ),
        .I2(\cnt_reg_n_0_[22] ),
        .I3(\cnt_reg_n_0_[18] ),
        .I4(\cnt_reg_n_0_[20] ),
        .I5(\cnt_reg_n_0_[23] ),
        .O(\Ymap[5]_i_63_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[5]_i_64 
       (.I0(\cnt_reg_n_0_[13] ),
        .I1(\Ymap_reg[5]_i_126_n_5 ),
        .I2(\Ymap_reg[5]_i_127_n_1 ),
        .O(\Ymap[5]_i_64_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[5]_i_65 
       (.I0(\cnt_reg_n_0_[12] ),
        .I1(\Ymap_reg[5]_i_126_n_6 ),
        .I2(\Ymap_reg[5]_i_127_n_6 ),
        .O(\Ymap[5]_i_65_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[5]_i_66 
       (.I0(\cnt_reg_n_0_[11] ),
        .I1(\Ymap_reg[5]_i_126_n_7 ),
        .I2(\Ymap_reg[5]_i_127_n_7 ),
        .O(\Ymap[5]_i_66_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Ymap[5]_i_67 
       (.I0(\cnt_reg_n_0_[10] ),
        .I1(\Ymap_reg[5]_i_128_n_4 ),
        .I2(\Ymap_reg[5]_i_129_n_4 ),
        .O(\Ymap[5]_i_67_n_0 ));
  LUT5 #(
    .INIT(32'h817E7E81)) 
    \Ymap[5]_i_68 
       (.I0(\Ymap_reg[5]_i_126_n_5 ),
        .I1(\cnt_reg_n_0_[13] ),
        .I2(\Ymap_reg[5]_i_127_n_1 ),
        .I3(\Ymap_reg[5]_i_126_n_4 ),
        .I4(\cnt_reg_n_0_[14] ),
        .O(\Ymap[5]_i_68_n_0 ));
  LUT6 #(
    .INIT(64'hE81717E817E8E817)) 
    \Ymap[5]_i_69 
       (.I0(\Ymap_reg[5]_i_127_n_6 ),
        .I1(\Ymap_reg[5]_i_126_n_6 ),
        .I2(\cnt_reg_n_0_[12] ),
        .I3(\Ymap_reg[5]_i_127_n_1 ),
        .I4(\Ymap_reg[5]_i_126_n_5 ),
        .I5(\cnt_reg_n_0_[13] ),
        .O(\Ymap[5]_i_69_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Ymap[5]_i_7 
       (.I0(\Ymap_reg[5]_i_4_n_7 ),
        .I1(\cnt_reg_n_0_[29] ),
        .O(\Ymap[5]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[5]_i_70 
       (.I0(\Ymap_reg[5]_i_127_n_7 ),
        .I1(\Ymap_reg[5]_i_126_n_7 ),
        .I2(\cnt_reg_n_0_[11] ),
        .I3(\Ymap_reg[5]_i_127_n_6 ),
        .I4(\Ymap_reg[5]_i_126_n_6 ),
        .I5(\cnt_reg_n_0_[12] ),
        .O(\Ymap[5]_i_70_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Ymap[5]_i_71 
       (.I0(\Ymap_reg[5]_i_129_n_4 ),
        .I1(\Ymap_reg[5]_i_128_n_4 ),
        .I2(\cnt_reg_n_0_[10] ),
        .I3(\Ymap_reg[5]_i_127_n_7 ),
        .I4(\Ymap_reg[5]_i_126_n_7 ),
        .I5(\cnt_reg_n_0_[11] ),
        .O(\Ymap[5]_i_71_n_0 ));
  LUT3 #(
    .INIT(8'hD4)) 
    \Ymap[5]_i_72 
       (.I0(\cnt_reg_n_0_[18] ),
        .I1(\cnt_reg_n_0_[16] ),
        .I2(\cnt_reg_n_0_[14] ),
        .O(\Ymap[5]_i_72_n_0 ));
  LUT3 #(
    .INIT(8'hD4)) 
    \Ymap[5]_i_73 
       (.I0(\cnt_reg_n_0_[17] ),
        .I1(\cnt_reg_n_0_[15] ),
        .I2(\cnt_reg_n_0_[13] ),
        .O(\Ymap[5]_i_73_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[5]_i_74 
       (.I0(\cnt_reg_n_0_[12] ),
        .I1(\cnt_reg_n_0_[14] ),
        .I2(\cnt_reg_n_0_[16] ),
        .O(\Ymap[5]_i_74_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[5]_i_75 
       (.I0(\cnt_reg_n_0_[11] ),
        .I1(\cnt_reg_n_0_[13] ),
        .I2(\cnt_reg_n_0_[15] ),
        .O(\Ymap[5]_i_75_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Ymap[5]_i_76 
       (.I0(\cnt_reg_n_0_[14] ),
        .I1(\cnt_reg_n_0_[16] ),
        .I2(\cnt_reg_n_0_[18] ),
        .I3(\cnt_reg_n_0_[19] ),
        .I4(\cnt_reg_n_0_[15] ),
        .I5(\cnt_reg_n_0_[17] ),
        .O(\Ymap[5]_i_76_n_0 ));
  LUT6 #(
    .INIT(64'h8E71718E718E8E71)) 
    \Ymap[5]_i_77 
       (.I0(\cnt_reg_n_0_[13] ),
        .I1(\cnt_reg_n_0_[15] ),
        .I2(\cnt_reg_n_0_[17] ),
        .I3(\cnt_reg_n_0_[14] ),
        .I4(\cnt_reg_n_0_[16] ),
        .I5(\cnt_reg_n_0_[18] ),
        .O(\Ymap[5]_i_77_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[5]_i_78 
       (.I0(\cnt_reg_n_0_[16] ),
        .I1(\cnt_reg_n_0_[14] ),
        .I2(\cnt_reg_n_0_[12] ),
        .I3(\cnt_reg_n_0_[13] ),
        .I4(\cnt_reg_n_0_[15] ),
        .I5(\cnt_reg_n_0_[17] ),
        .O(\Ymap[5]_i_78_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[5]_i_79 
       (.I0(\cnt_reg_n_0_[15] ),
        .I1(\cnt_reg_n_0_[13] ),
        .I2(\cnt_reg_n_0_[11] ),
        .I3(\cnt_reg_n_0_[16] ),
        .I4(\cnt_reg_n_0_[12] ),
        .I5(\cnt_reg_n_0_[14] ),
        .O(\Ymap[5]_i_79_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Ymap[5]_i_8 
       (.I0(\Ymap_reg[5]_i_21_n_4 ),
        .I1(\cnt_reg_n_0_[28] ),
        .O(\Ymap[5]_i_8_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[5]_i_80 
       (.I0(\cnt_reg_n_0_[29] ),
        .I1(\cnt_reg_n_0_[24] ),
        .I2(\cnt_reg_n_0_[26] ),
        .O(\Ymap[5]_i_80_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[5]_i_81 
       (.I0(\cnt_reg_n_0_[28] ),
        .I1(\cnt_reg_n_0_[23] ),
        .I2(\cnt_reg_n_0_[25] ),
        .O(\Ymap[5]_i_81_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[5]_i_82 
       (.I0(\cnt_reg_n_0_[27] ),
        .I1(\cnt_reg_n_0_[22] ),
        .I2(\cnt_reg_n_0_[24] ),
        .O(\Ymap[5]_i_82_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[5]_i_83 
       (.I0(\cnt_reg_n_0_[26] ),
        .I1(\cnt_reg_n_0_[21] ),
        .I2(\cnt_reg_n_0_[23] ),
        .O(\Ymap[5]_i_83_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[5]_i_84 
       (.I0(\cnt_reg_n_0_[26] ),
        .I1(\cnt_reg_n_0_[24] ),
        .I2(\cnt_reg_n_0_[29] ),
        .I3(\cnt_reg_n_0_[25] ),
        .I4(\cnt_reg_n_0_[27] ),
        .I5(\cnt_reg_n_0_[30] ),
        .O(\Ymap[5]_i_84_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[5]_i_85 
       (.I0(\cnt_reg_n_0_[25] ),
        .I1(\cnt_reg_n_0_[23] ),
        .I2(\cnt_reg_n_0_[28] ),
        .I3(\cnt_reg_n_0_[29] ),
        .I4(\cnt_reg_n_0_[24] ),
        .I5(\cnt_reg_n_0_[26] ),
        .O(\Ymap[5]_i_85_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[5]_i_86 
       (.I0(\cnt_reg_n_0_[24] ),
        .I1(\cnt_reg_n_0_[22] ),
        .I2(\cnt_reg_n_0_[27] ),
        .I3(\cnt_reg_n_0_[28] ),
        .I4(\cnt_reg_n_0_[23] ),
        .I5(\cnt_reg_n_0_[25] ),
        .O(\Ymap[5]_i_86_n_0 ));
  LUT6 #(
    .INIT(64'hD42B2BD42BD4D42B)) 
    \Ymap[5]_i_87 
       (.I0(\cnt_reg_n_0_[23] ),
        .I1(\cnt_reg_n_0_[21] ),
        .I2(\cnt_reg_n_0_[26] ),
        .I3(\cnt_reg_n_0_[22] ),
        .I4(\cnt_reg_n_0_[24] ),
        .I5(\cnt_reg_n_0_[27] ),
        .O(\Ymap[5]_i_87_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[5]_i_88 
       (.I0(\cnt_reg_n_0_[17] ),
        .I1(\Ymap_reg[5]_i_130_n_5 ),
        .I2(\Ymap_reg[5]_i_127_n_1 ),
        .O(\Ymap[5]_i_88_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[5]_i_89 
       (.I0(\cnt_reg_n_0_[16] ),
        .I1(\Ymap_reg[5]_i_130_n_6 ),
        .I2(\Ymap_reg[5]_i_127_n_1 ),
        .O(\Ymap[5]_i_89_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \Ymap[5]_i_9 
       (.I0(\Ymap_reg[5]_i_21_n_5 ),
        .I1(\cnt_reg_n_0_[27] ),
        .O(\Ymap[5]_i_9_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[5]_i_90 
       (.I0(\cnt_reg_n_0_[15] ),
        .I1(\Ymap_reg[5]_i_130_n_7 ),
        .I2(\Ymap_reg[5]_i_127_n_1 ),
        .O(\Ymap[5]_i_90_n_0 ));
  LUT3 #(
    .INIT(8'h8E)) 
    \Ymap[5]_i_91 
       (.I0(\cnt_reg_n_0_[14] ),
        .I1(\Ymap_reg[5]_i_126_n_4 ),
        .I2(\Ymap_reg[5]_i_127_n_1 ),
        .O(\Ymap[5]_i_91_n_0 ));
  LUT5 #(
    .INIT(32'h96666669)) 
    \Ymap[5]_i_92 
       (.I0(\cnt_reg_n_0_[18] ),
        .I1(\Ymap_reg[5]_i_130_n_4 ),
        .I2(\Ymap_reg[5]_i_127_n_1 ),
        .I3(\Ymap_reg[5]_i_130_n_5 ),
        .I4(\cnt_reg_n_0_[17] ),
        .O(\Ymap[5]_i_92_n_0 ));
  LUT5 #(
    .INIT(32'h817E7E81)) 
    \Ymap[5]_i_93 
       (.I0(\Ymap_reg[5]_i_130_n_6 ),
        .I1(\cnt_reg_n_0_[16] ),
        .I2(\Ymap_reg[5]_i_127_n_1 ),
        .I3(\Ymap_reg[5]_i_130_n_5 ),
        .I4(\cnt_reg_n_0_[17] ),
        .O(\Ymap[5]_i_93_n_0 ));
  LUT5 #(
    .INIT(32'h817E7E81)) 
    \Ymap[5]_i_94 
       (.I0(\Ymap_reg[5]_i_130_n_7 ),
        .I1(\cnt_reg_n_0_[15] ),
        .I2(\Ymap_reg[5]_i_127_n_1 ),
        .I3(\Ymap_reg[5]_i_130_n_6 ),
        .I4(\cnt_reg_n_0_[16] ),
        .O(\Ymap[5]_i_94_n_0 ));
  LUT5 #(
    .INIT(32'h817E7E81)) 
    \Ymap[5]_i_95 
       (.I0(\Ymap_reg[5]_i_126_n_4 ),
        .I1(\cnt_reg_n_0_[14] ),
        .I2(\Ymap_reg[5]_i_127_n_1 ),
        .I3(\Ymap_reg[5]_i_130_n_7 ),
        .I4(\cnt_reg_n_0_[15] ),
        .O(\Ymap[5]_i_95_n_0 ));
  LUT3 #(
    .INIT(8'hD4)) 
    \Ymap[5]_i_96 
       (.I0(\cnt_reg_n_0_[22] ),
        .I1(\cnt_reg_n_0_[18] ),
        .I2(\cnt_reg_n_0_[20] ),
        .O(\Ymap[5]_i_96_n_0 ));
  LUT3 #(
    .INIT(8'hD4)) 
    \Ymap[5]_i_97 
       (.I0(\cnt_reg_n_0_[21] ),
        .I1(\cnt_reg_n_0_[17] ),
        .I2(\cnt_reg_n_0_[19] ),
        .O(\Ymap[5]_i_97_n_0 ));
  LUT3 #(
    .INIT(8'hD4)) 
    \Ymap[5]_i_98 
       (.I0(\cnt_reg_n_0_[20] ),
        .I1(\cnt_reg_n_0_[16] ),
        .I2(\cnt_reg_n_0_[18] ),
        .O(\Ymap[5]_i_98_n_0 ));
  LUT3 #(
    .INIT(8'hD4)) 
    \Ymap[5]_i_99 
       (.I0(\cnt_reg_n_0_[19] ),
        .I1(\cnt_reg_n_0_[15] ),
        .I2(\cnt_reg_n_0_[17] ),
        .O(\Ymap[5]_i_99_n_0 ));
  FDRE \Ymap_reg[0] 
       (.C(clk),
        .CE(\Xmap[6]_i_1_n_0 ),
        .D(\Ymap[0]_i_1_n_0 ),
        .Q(Q),
        .R(1'b0));
  CARRY4 \Ymap_reg[0]_i_103 
       (.CI(\Ymap_reg[0]_i_136_n_0 ),
        .CO({\Ymap_reg[0]_i_103_n_0 ,\Ymap_reg[0]_i_103_n_1 ,\Ymap_reg[0]_i_103_n_2 ,\Ymap_reg[0]_i_103_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap_reg[0]_i_137_n_4 ,\Ymap_reg[0]_i_137_n_5 ,\Ymap_reg[0]_i_137_n_6 ,\Ymap_reg[0]_i_137_n_7 }),
        .O({\Ymap_reg[0]_i_103_n_4 ,\Ymap_reg[0]_i_103_n_5 ,\Ymap_reg[0]_i_103_n_6 ,\Ymap_reg[0]_i_103_n_7 }),
        .S({\Ymap[0]_i_138_n_0 ,\Ymap[0]_i_139_n_0 ,\Ymap[0]_i_140_n_0 ,\Ymap[0]_i_141_n_0 }));
  CARRY4 \Ymap_reg[0]_i_104 
       (.CI(1'b0),
        .CO({\Ymap_reg[0]_i_104_n_0 ,\Ymap_reg[0]_i_104_n_1 ,\Ymap_reg[0]_i_104_n_2 ,\Ymap_reg[0]_i_104_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[0]_i_142_n_0 ,\Ymap[0]_i_143_n_0 ,\Ymap[0]_i_144_n_0 ,1'b0}),
        .O({\Ymap_reg[0]_i_104_n_4 ,\Ymap_reg[0]_i_104_n_5 ,\Ymap_reg[0]_i_104_n_6 ,\NLW_Ymap_reg[0]_i_104_O_UNCONNECTED [0]}),
        .S({\Ymap[0]_i_145_n_0 ,\Ymap[0]_i_146_n_0 ,\Ymap[0]_i_147_n_0 ,\Ymap[0]_i_148_n_0 }));
  CARRY4 \Ymap_reg[0]_i_105 
       (.CI(1'b0),
        .CO({\Ymap_reg[0]_i_105_n_0 ,\Ymap_reg[0]_i_105_n_1 ,\Ymap_reg[0]_i_105_n_2 ,\Ymap_reg[0]_i_105_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[0]_i_142_n_0 ,\Ymap[0]_i_149_n_0 ,\Ymap[0]_i_150_n_0 ,1'b0}),
        .O({\NLW_Ymap_reg[0]_i_105_O_UNCONNECTED [3:1],\Ymap_reg[0]_i_105_n_7 }),
        .S({\Ymap[0]_i_151_n_0 ,\Ymap[0]_i_152_n_0 ,\Ymap[0]_i_153_n_0 ,\Ymap[0]_i_154_n_0 }));
  CARRY4 \Ymap_reg[0]_i_118 
       (.CI(\Ymap_reg[0]_i_137_n_0 ),
        .CO({\Ymap_reg[0]_i_118_n_0 ,\Ymap_reg[0]_i_118_n_1 ,\Ymap_reg[0]_i_118_n_2 ,\Ymap_reg[0]_i_118_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[4]_i_14_n_0 ,\Ymap[4]_i_15_n_0 ,\Ymap[4]_i_16_n_0 ,\Ymap[4]_i_17_n_0 }),
        .O({\Ymap_reg[0]_i_118_n_4 ,\Ymap_reg[0]_i_118_n_5 ,\Ymap_reg[0]_i_118_n_6 ,\Ymap_reg[0]_i_118_n_7 }),
        .S({\Ymap[0]_i_155_n_0 ,\Ymap[0]_i_156_n_0 ,\Ymap[0]_i_157_n_0 ,\Ymap[0]_i_158_n_0 }));
  CARRY4 \Ymap_reg[0]_i_12 
       (.CI(\Ymap_reg[0]_i_24_n_0 ),
        .CO({\Ymap_reg[0]_i_12_n_0 ,\Ymap_reg[0]_i_12_n_1 ,\Ymap_reg[0]_i_12_n_2 ,\Ymap_reg[0]_i_12_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[0]_i_25_n_0 ,\Ymap[0]_i_26_n_0 ,\Ymap[0]_i_27_n_0 ,\Ymap[0]_i_28_n_0 }),
        .O(\NLW_Ymap_reg[0]_i_12_O_UNCONNECTED [3:0]),
        .S({\Ymap[0]_i_29_n_0 ,\Ymap[0]_i_30_n_0 ,\Ymap[0]_i_31_n_0 ,\Ymap[0]_i_32_n_0 }));
  CARRY4 \Ymap_reg[0]_i_131 
       (.CI(\Ymap_reg[0]_i_159_n_0 ),
        .CO({\Ymap_reg[0]_i_131_n_0 ,\Ymap_reg[0]_i_131_n_1 ,\Ymap_reg[0]_i_131_n_2 ,\Ymap_reg[0]_i_131_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[4]_i_30_n_0 ,\Ymap[4]_i_31_n_0 ,\Ymap[4]_i_32_n_0 ,\Ymap[4]_i_33_n_0 }),
        .O({\Ymap_reg[0]_i_131_n_4 ,\Ymap_reg[0]_i_131_n_5 ,\Ymap_reg[0]_i_131_n_6 ,\Ymap_reg[0]_i_131_n_7 }),
        .S({\Ymap[0]_i_160_n_0 ,\Ymap[0]_i_161_n_0 ,\Ymap[0]_i_162_n_0 ,\Ymap[0]_i_163_n_0 }));
  CARRY4 \Ymap_reg[0]_i_136 
       (.CI(\Ymap_reg[0]_i_164_n_0 ),
        .CO({\Ymap_reg[0]_i_136_n_0 ,\Ymap_reg[0]_i_136_n_1 ,\Ymap_reg[0]_i_136_n_2 ,\Ymap_reg[0]_i_136_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap_reg[0]_i_165_n_4 ,\Ymap_reg[0]_i_165_n_5 ,\Ymap_reg[0]_i_165_n_6 ,\Ymap_reg[0]_i_165_n_7 }),
        .O({\Ymap_reg[0]_i_136_n_4 ,\Ymap_reg[0]_i_136_n_5 ,\Ymap_reg[0]_i_136_n_6 ,\NLW_Ymap_reg[0]_i_136_O_UNCONNECTED [0]}),
        .S({\Ymap[0]_i_166_n_0 ,\Ymap[0]_i_167_n_0 ,\Ymap[0]_i_168_n_0 ,\Ymap[0]_i_169_n_0 }));
  CARRY4 \Ymap_reg[0]_i_137 
       (.CI(\Ymap_reg[0]_i_165_n_0 ),
        .CO({\Ymap_reg[0]_i_137_n_0 ,\Ymap_reg[0]_i_137_n_1 ,\Ymap_reg[0]_i_137_n_2 ,\Ymap_reg[0]_i_137_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[0]_i_36_n_0 ,\Ymap[0]_i_37_n_0 ,\Ymap[0]_i_38_n_0 ,\Ymap[0]_i_39_n_0 }),
        .O({\Ymap_reg[0]_i_137_n_4 ,\Ymap_reg[0]_i_137_n_5 ,\Ymap_reg[0]_i_137_n_6 ,\Ymap_reg[0]_i_137_n_7 }),
        .S({\Ymap[0]_i_170_n_0 ,\Ymap[0]_i_171_n_0 ,\Ymap[0]_i_172_n_0 ,\Ymap[0]_i_173_n_0 }));
  CARRY4 \Ymap_reg[0]_i_159 
       (.CI(\Ymap_reg[0]_i_174_n_0 ),
        .CO({\Ymap_reg[0]_i_159_n_0 ,\Ymap_reg[0]_i_159_n_1 ,\Ymap_reg[0]_i_159_n_2 ,\Ymap_reg[0]_i_159_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[0]_i_52_n_0 ,\Ymap[0]_i_53_n_0 ,\Ymap[0]_i_54_n_0 ,\Ymap[0]_i_55_n_0 }),
        .O({\Ymap_reg[0]_i_159_n_4 ,\Ymap_reg[0]_i_159_n_5 ,\Ymap_reg[0]_i_159_n_6 ,\Ymap_reg[0]_i_159_n_7 }),
        .S({\Ymap[0]_i_175_n_0 ,\Ymap[0]_i_176_n_0 ,\Ymap[0]_i_177_n_0 ,\Ymap[0]_i_178_n_0 }));
  CARRY4 \Ymap_reg[0]_i_164 
       (.CI(1'b0),
        .CO({\Ymap_reg[0]_i_164_n_0 ,\Ymap_reg[0]_i_164_n_1 ,\Ymap_reg[0]_i_164_n_2 ,\Ymap_reg[0]_i_164_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap_reg[0]_i_179_n_4 ,\Ymap_reg[0]_i_179_n_5 ,\Ymap_reg[0]_i_179_n_6 ,\Ymap_reg[0]_i_179_n_7 }),
        .O(\NLW_Ymap_reg[0]_i_164_O_UNCONNECTED [3:0]),
        .S({\Ymap[0]_i_180_n_0 ,\Ymap[0]_i_181_n_0 ,\Ymap[0]_i_182_n_0 ,\Ymap[0]_i_183_n_0 }));
  CARRY4 \Ymap_reg[0]_i_165 
       (.CI(\Ymap_reg[0]_i_179_n_0 ),
        .CO({\Ymap_reg[0]_i_165_n_0 ,\Ymap_reg[0]_i_165_n_1 ,\Ymap_reg[0]_i_165_n_2 ,\Ymap_reg[0]_i_165_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[0]_i_73_n_0 ,\Ymap[0]_i_74_n_0 ,\Ymap[0]_i_75_n_0 ,\Ymap[0]_i_76_n_0 }),
        .O({\Ymap_reg[0]_i_165_n_4 ,\Ymap_reg[0]_i_165_n_5 ,\Ymap_reg[0]_i_165_n_6 ,\Ymap_reg[0]_i_165_n_7 }),
        .S({\Ymap[0]_i_184_n_0 ,\Ymap[0]_i_185_n_0 ,\Ymap[0]_i_186_n_0 ,\Ymap[0]_i_187_n_0 }));
  CARRY4 \Ymap_reg[0]_i_174 
       (.CI(\Ymap_reg[0]_i_72_n_0 ),
        .CO({\Ymap_reg[0]_i_174_n_0 ,\Ymap_reg[0]_i_174_n_1 ,\Ymap_reg[0]_i_174_n_2 ,\Ymap_reg[0]_i_174_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[0]_i_88_n_0 ,\Ymap[0]_i_89_n_0 ,\Ymap[0]_i_188_n_0 ,\cnt_reg_n_0_[2] }),
        .O({\Ymap_reg[0]_i_174_n_4 ,\Ymap_reg[0]_i_174_n_5 ,\Ymap_reg[0]_i_174_n_6 ,\Ymap_reg[0]_i_174_n_7 }),
        .S({\Ymap[0]_i_189_n_0 ,\Ymap[0]_i_190_n_0 ,\Ymap[0]_i_191_n_0 ,\Ymap[0]_i_192_n_0 }));
  CARRY4 \Ymap_reg[0]_i_179 
       (.CI(\Ymap_reg[0]_i_105_n_0 ),
        .CO({\Ymap_reg[0]_i_179_n_0 ,\Ymap_reg[0]_i_179_n_1 ,\Ymap_reg[0]_i_179_n_2 ,\Ymap_reg[0]_i_179_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[0]_i_106_n_0 ,\Ymap[0]_i_107_n_0 ,\Ymap[0]_i_108_n_0 ,\Ymap[0]_i_109_n_0 }),
        .O({\Ymap_reg[0]_i_179_n_4 ,\Ymap_reg[0]_i_179_n_5 ,\Ymap_reg[0]_i_179_n_6 ,\Ymap_reg[0]_i_179_n_7 }),
        .S({\Ymap[0]_i_193_n_0 ,\Ymap[0]_i_194_n_0 ,\Ymap[0]_i_195_n_0 ,\Ymap[0]_i_196_n_0 }));
  CARRY4 \Ymap_reg[0]_i_2 
       (.CI(\Ymap_reg[0]_i_3_n_0 ),
        .CO({\Ymap_reg[0]_i_2_n_0 ,\Ymap_reg[0]_i_2_n_1 ,\Ymap_reg[0]_i_2_n_2 ,\Ymap_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[0]_i_4_n_0 ,\Ymap[0]_i_5_n_0 ,\Ymap[0]_i_6_n_0 ,\Ymap[0]_i_7_n_0 }),
        .O({\Ymap_reg[0]_i_2_n_4 ,\NLW_Ymap_reg[0]_i_2_O_UNCONNECTED [2:0]}),
        .S({\Ymap[0]_i_8_n_0 ,\Ymap[0]_i_9_n_0 ,\Ymap[0]_i_10_n_0 ,\Ymap[0]_i_11_n_0 }));
  CARRY4 \Ymap_reg[0]_i_21 
       (.CI(\Ymap_reg[0]_i_33_n_0 ),
        .CO({\Ymap_reg[0]_i_21_n_0 ,\Ymap_reg[0]_i_21_n_1 ,\Ymap_reg[0]_i_21_n_2 ,\Ymap_reg[0]_i_21_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[0]_i_36_n_0 ,\Ymap[0]_i_37_n_0 ,\Ymap[0]_i_38_n_0 ,\Ymap[0]_i_39_n_0 }),
        .O({\Ymap_reg[0]_i_21_n_4 ,\Ymap_reg[0]_i_21_n_5 ,\Ymap_reg[0]_i_21_n_6 ,\Ymap_reg[0]_i_21_n_7 }),
        .S({\Ymap[0]_i_40_n_0 ,\Ymap[0]_i_41_n_0 ,\Ymap[0]_i_42_n_0 ,\Ymap[0]_i_43_n_0 }));
  CARRY4 \Ymap_reg[0]_i_22 
       (.CI(\Ymap_reg[0]_i_34_n_0 ),
        .CO({\Ymap_reg[0]_i_22_n_0 ,\Ymap_reg[0]_i_22_n_1 ,\Ymap_reg[0]_i_22_n_2 ,\Ymap_reg[0]_i_22_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[0]_i_44_n_0 ,\Ymap[0]_i_45_n_0 ,\Ymap[0]_i_46_n_0 ,\Ymap[0]_i_47_n_0 }),
        .O({\Ymap_reg[0]_i_22_n_4 ,\Ymap_reg[0]_i_22_n_5 ,\Ymap_reg[0]_i_22_n_6 ,\Ymap_reg[0]_i_22_n_7 }),
        .S({\Ymap[0]_i_48_n_0 ,\Ymap[0]_i_49_n_0 ,\Ymap[0]_i_50_n_0 ,\Ymap[0]_i_51_n_0 }));
  CARRY4 \Ymap_reg[0]_i_23 
       (.CI(\Ymap_reg[0]_i_35_n_0 ),
        .CO({\Ymap_reg[0]_i_23_n_0 ,\Ymap_reg[0]_i_23_n_1 ,\Ymap_reg[0]_i_23_n_2 ,\Ymap_reg[0]_i_23_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[0]_i_52_n_0 ,\Ymap[0]_i_53_n_0 ,\Ymap[0]_i_54_n_0 ,\Ymap[0]_i_55_n_0 }),
        .O({\Ymap_reg[0]_i_23_n_4 ,\Ymap_reg[0]_i_23_n_5 ,\Ymap_reg[0]_i_23_n_6 ,\Ymap_reg[0]_i_23_n_7 }),
        .S({\Ymap[0]_i_56_n_0 ,\Ymap[0]_i_57_n_0 ,\Ymap[0]_i_58_n_0 ,\Ymap[0]_i_59_n_0 }));
  CARRY4 \Ymap_reg[0]_i_24 
       (.CI(\Ymap_reg[0]_i_60_n_0 ),
        .CO({\Ymap_reg[0]_i_24_n_0 ,\Ymap_reg[0]_i_24_n_1 ,\Ymap_reg[0]_i_24_n_2 ,\Ymap_reg[0]_i_24_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[0]_i_61_n_0 ,\Ymap[0]_i_62_n_0 ,\Ymap[0]_i_63_n_0 ,\Ymap[0]_i_64_n_0 }),
        .O(\NLW_Ymap_reg[0]_i_24_O_UNCONNECTED [3:0]),
        .S({\Ymap[0]_i_65_n_0 ,\Ymap[0]_i_66_n_0 ,\Ymap[0]_i_67_n_0 ,\Ymap[0]_i_68_n_0 }));
  CARRY4 \Ymap_reg[0]_i_3 
       (.CI(\Ymap_reg[0]_i_12_n_0 ),
        .CO({\Ymap_reg[0]_i_3_n_0 ,\Ymap_reg[0]_i_3_n_1 ,\Ymap_reg[0]_i_3_n_2 ,\Ymap_reg[0]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[0]_i_13_n_0 ,\Ymap[0]_i_14_n_0 ,\Ymap[0]_i_15_n_0 ,\Ymap[0]_i_16_n_0 }),
        .O(\NLW_Ymap_reg[0]_i_3_O_UNCONNECTED [3:0]),
        .S({\Ymap[0]_i_17_n_0 ,\Ymap[0]_i_18_n_0 ,\Ymap[0]_i_19_n_0 ,\Ymap[0]_i_20_n_0 }));
  CARRY4 \Ymap_reg[0]_i_33 
       (.CI(\Ymap_reg[0]_i_69_n_0 ),
        .CO({\Ymap_reg[0]_i_33_n_0 ,\Ymap_reg[0]_i_33_n_1 ,\Ymap_reg[0]_i_33_n_2 ,\Ymap_reg[0]_i_33_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[0]_i_73_n_0 ,\Ymap[0]_i_74_n_0 ,\Ymap[0]_i_75_n_0 ,\Ymap[0]_i_76_n_0 }),
        .O({\Ymap_reg[0]_i_33_n_4 ,\Ymap_reg[0]_i_33_n_5 ,\Ymap_reg[0]_i_33_n_6 ,\Ymap_reg[0]_i_33_n_7 }),
        .S({\Ymap[0]_i_77_n_0 ,\Ymap[0]_i_78_n_0 ,\Ymap[0]_i_79_n_0 ,\Ymap[0]_i_80_n_0 }));
  CARRY4 \Ymap_reg[0]_i_34 
       (.CI(\Ymap_reg[0]_i_71_n_0 ),
        .CO({\Ymap_reg[0]_i_34_n_0 ,\Ymap_reg[0]_i_34_n_1 ,\Ymap_reg[0]_i_34_n_2 ,\Ymap_reg[0]_i_34_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[0]_i_81_n_0 ,\Ymap[0]_i_82_n_0 ,\cnt_reg_n_0_[0] ,\Ymap_reg[0]_i_83_n_7 }),
        .O({\Ymap_reg[0]_i_34_n_4 ,\Ymap_reg[0]_i_34_n_5 ,\Ymap_reg[0]_i_34_n_6 ,\Ymap_reg[0]_i_34_n_7 }),
        .S({\Ymap[0]_i_84_n_0 ,\Ymap[0]_i_85_n_0 ,\Ymap[0]_i_86_n_0 ,\Ymap[0]_i_87_n_0 }));
  CARRY4 \Ymap_reg[0]_i_35 
       (.CI(\Ymap_reg[0]_i_70_n_0 ),
        .CO({\Ymap_reg[0]_i_35_n_0 ,\Ymap_reg[0]_i_35_n_1 ,\Ymap_reg[0]_i_35_n_2 ,\Ymap_reg[0]_i_35_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[0]_i_88_n_0 ,\Ymap[0]_i_89_n_0 ,\Ymap[0]_i_90_n_0 ,\cnt_reg_n_0_[2] }),
        .O({\Ymap_reg[0]_i_35_n_4 ,\Ymap_reg[0]_i_35_n_5 ,\Ymap_reg[0]_i_35_n_6 ,\Ymap_reg[0]_i_35_n_7 }),
        .S({\Ymap[0]_i_91_n_0 ,\Ymap[0]_i_92_n_0 ,\Ymap[0]_i_93_n_0 ,\Ymap[0]_i_94_n_0 }));
  CARRY4 \Ymap_reg[0]_i_60 
       (.CI(1'b0),
        .CO({\Ymap_reg[0]_i_60_n_0 ,\Ymap_reg[0]_i_60_n_1 ,\Ymap_reg[0]_i_60_n_2 ,\Ymap_reg[0]_i_60_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[0]_i_96_n_0 ,\Ymap[0]_i_97_n_0 ,\Ymap[0]_i_98_n_0 ,1'b0}),
        .O(\NLW_Ymap_reg[0]_i_60_O_UNCONNECTED [3:0]),
        .S({\Ymap[0]_i_99_n_0 ,\Ymap[0]_i_100_n_0 ,\Ymap[0]_i_101_n_0 ,\Ymap[0]_i_102_n_0 }));
  CARRY4 \Ymap_reg[0]_i_69 
       (.CI(\Ymap_reg[0]_i_104_n_0 ),
        .CO({\Ymap_reg[0]_i_69_n_0 ,\Ymap_reg[0]_i_69_n_1 ,\Ymap_reg[0]_i_69_n_2 ,\Ymap_reg[0]_i_69_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[0]_i_106_n_0 ,\Ymap[0]_i_107_n_0 ,\Ymap[0]_i_108_n_0 ,\Ymap[0]_i_109_n_0 }),
        .O({\Ymap_reg[0]_i_69_n_4 ,\Ymap_reg[0]_i_69_n_5 ,\Ymap_reg[0]_i_69_n_6 ,\Ymap_reg[0]_i_69_n_7 }),
        .S({\Ymap[0]_i_110_n_0 ,\Ymap[0]_i_111_n_0 ,\Ymap[0]_i_112_n_0 ,\Ymap[0]_i_113_n_0 }));
  CARRY4 \Ymap_reg[0]_i_70 
       (.CI(1'b0),
        .CO({\Ymap_reg[0]_i_70_n_0 ,\Ymap_reg[0]_i_70_n_1 ,\Ymap_reg[0]_i_70_n_2 ,\Ymap_reg[0]_i_70_n_3 }),
        .CYINIT(1'b0),
        .DI({\cnt_reg_n_0_[1] ,\cnt_reg_n_0_[0] ,1'b0,1'b1}),
        .O({\Ymap_reg[0]_i_70_n_4 ,\Ymap_reg[0]_i_70_n_5 ,\Ymap_reg[0]_i_70_n_6 ,\NLW_Ymap_reg[0]_i_70_O_UNCONNECTED [0]}),
        .S({\Ymap[0]_i_114_n_0 ,\Ymap[0]_i_115_n_0 ,\Ymap[0]_i_116_n_0 ,\Ymap[0]_i_117_n_0 }));
  CARRY4 \Ymap_reg[0]_i_71 
       (.CI(\Ymap_reg[0]_i_103_n_0 ),
        .CO({\Ymap_reg[0]_i_71_n_0 ,\Ymap_reg[0]_i_71_n_1 ,\Ymap_reg[0]_i_71_n_2 ,\Ymap_reg[0]_i_71_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap_reg[0]_i_118_n_4 ,\Ymap_reg[0]_i_118_n_5 ,\Ymap_reg[0]_i_118_n_6 ,\Ymap_reg[0]_i_118_n_7 }),
        .O({\Ymap_reg[0]_i_71_n_4 ,\Ymap_reg[0]_i_71_n_5 ,\Ymap_reg[0]_i_71_n_6 ,\Ymap_reg[0]_i_71_n_7 }),
        .S({\Ymap[0]_i_119_n_0 ,\Ymap[0]_i_120_n_0 ,\Ymap[0]_i_121_n_0 ,\Ymap[0]_i_122_n_0 }));
  CARRY4 \Ymap_reg[0]_i_72 
       (.CI(1'b0),
        .CO({\Ymap_reg[0]_i_72_n_0 ,\Ymap_reg[0]_i_72_n_1 ,\Ymap_reg[0]_i_72_n_2 ,\Ymap_reg[0]_i_72_n_3 }),
        .CYINIT(1'b0),
        .DI({\cnt_reg_n_0_[1] ,\cnt_reg_n_0_[0] ,1'b0,1'b1}),
        .O({\Ymap_reg[0]_i_72_n_4 ,\Ymap_reg[0]_i_72_n_5 ,\Ymap_reg[0]_i_72_n_6 ,\Ymap_reg[0]_i_72_n_7 }),
        .S({\Ymap[0]_i_123_n_0 ,\Ymap[0]_i_124_n_0 ,\Ymap[0]_i_125_n_0 ,\Ymap[0]_i_126_n_0 }));
  CARRY4 \Ymap_reg[0]_i_83 
       (.CI(\Ymap_reg[0]_i_118_n_0 ),
        .CO({\Ymap_reg[0]_i_83_n_0 ,\Ymap_reg[0]_i_83_n_1 ,\Ymap_reg[0]_i_83_n_2 ,\Ymap_reg[0]_i_83_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[5]_i_56_n_0 ,\Ymap[5]_i_57_n_0 ,\Ymap[5]_i_58_n_0 ,\Ymap[5]_i_59_n_0 }),
        .O({\Ymap_reg[0]_i_83_n_4 ,\Ymap_reg[0]_i_83_n_5 ,\Ymap_reg[0]_i_83_n_6 ,\Ymap_reg[0]_i_83_n_7 }),
        .S({\Ymap[0]_i_127_n_0 ,\Ymap[0]_i_128_n_0 ,\Ymap[0]_i_129_n_0 ,\Ymap[0]_i_130_n_0 }));
  CARRY4 \Ymap_reg[0]_i_95 
       (.CI(\Ymap_reg[0]_i_131_n_0 ),
        .CO({\Ymap_reg[0]_i_95_n_0 ,\Ymap_reg[0]_i_95_n_1 ,\Ymap_reg[0]_i_95_n_2 ,\Ymap_reg[0]_i_95_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[5]_i_72_n_0 ,\Ymap[5]_i_73_n_0 ,\Ymap[5]_i_74_n_0 ,\Ymap[5]_i_75_n_0 }),
        .O({\Ymap_reg[0]_i_95_n_4 ,\Ymap_reg[0]_i_95_n_5 ,\Ymap_reg[0]_i_95_n_6 ,\Ymap_reg[0]_i_95_n_7 }),
        .S({\Ymap[0]_i_132_n_0 ,\Ymap[0]_i_133_n_0 ,\Ymap[0]_i_134_n_0 ,\Ymap[0]_i_135_n_0 }));
  FDRE \Ymap_reg[1] 
       (.C(clk),
        .CE(\Xmap[6]_i_1_n_0 ),
        .D(\Ymap[1]_i_1_n_0 ),
        .Q(Ymap[1]),
        .R(1'b0));
  FDRE \Ymap_reg[2] 
       (.C(clk),
        .CE(\Xmap[6]_i_1_n_0 ),
        .D(\Ymap[2]_i_1_n_0 ),
        .Q(Ymap[2]),
        .R(1'b0));
  FDRE \Ymap_reg[3] 
       (.C(clk),
        .CE(\Xmap[6]_i_1_n_0 ),
        .D(\Ymap[3]_i_1_n_0 ),
        .Q(Ymap[3]),
        .R(1'b0));
  CARRY4 \Ymap_reg[3]_i_2 
       (.CI(1'b0),
        .CO({\Ymap_reg[3]_i_2_n_0 ,\Ymap_reg[3]_i_2_n_1 ,\Ymap_reg[3]_i_2_n_2 ,\Ymap_reg[3]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\Ymap_reg[3]_i_2_n_4 ,\Ymap_reg[3]_i_2_n_5 ,\Ymap_reg[3]_i_2_n_6 ,\Ymap_reg[3]_i_2_n_7 }),
        .S({\Ymap[3]_i_3_n_0 ,\Ymap[3]_i_4_n_0 ,\Ymap[3]_i_5_n_0 ,\Ymap[3]_i_6_n_0 }));
  FDRE \Ymap_reg[4] 
       (.C(clk),
        .CE(\Xmap[6]_i_1_n_0 ),
        .D(\Ymap[4]_i_1_n_0 ),
        .Q(Ymap[4]),
        .R(1'b0));
  CARRY4 \Ymap_reg[4]_i_11 
       (.CI(\Ymap_reg[0]_i_21_n_0 ),
        .CO({\Ymap_reg[4]_i_11_n_0 ,\Ymap_reg[4]_i_11_n_1 ,\Ymap_reg[4]_i_11_n_2 ,\Ymap_reg[4]_i_11_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[4]_i_14_n_0 ,\Ymap[4]_i_15_n_0 ,\Ymap[4]_i_16_n_0 ,\Ymap[4]_i_17_n_0 }),
        .O({\Ymap_reg[4]_i_11_n_4 ,\Ymap_reg[4]_i_11_n_5 ,\Ymap_reg[4]_i_11_n_6 ,\Ymap_reg[4]_i_11_n_7 }),
        .S({\Ymap[4]_i_18_n_0 ,\Ymap[4]_i_19_n_0 ,\Ymap[4]_i_20_n_0 ,\Ymap[4]_i_21_n_0 }));
  CARRY4 \Ymap_reg[4]_i_12 
       (.CI(\Ymap_reg[0]_i_22_n_0 ),
        .CO({\Ymap_reg[4]_i_12_n_0 ,\Ymap_reg[4]_i_12_n_1 ,\Ymap_reg[4]_i_12_n_2 ,\Ymap_reg[4]_i_12_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[4]_i_22_n_0 ,\Ymap[4]_i_23_n_0 ,\Ymap[4]_i_24_n_0 ,\Ymap[4]_i_25_n_0 }),
        .O({\Ymap_reg[4]_i_12_n_4 ,\Ymap_reg[4]_i_12_n_5 ,\Ymap_reg[4]_i_12_n_6 ,\Ymap_reg[4]_i_12_n_7 }),
        .S({\Ymap[4]_i_26_n_0 ,\Ymap[4]_i_27_n_0 ,\Ymap[4]_i_28_n_0 ,\Ymap[4]_i_29_n_0 }));
  CARRY4 \Ymap_reg[4]_i_13 
       (.CI(\Ymap_reg[0]_i_23_n_0 ),
        .CO({\Ymap_reg[4]_i_13_n_0 ,\Ymap_reg[4]_i_13_n_1 ,\Ymap_reg[4]_i_13_n_2 ,\Ymap_reg[4]_i_13_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[4]_i_30_n_0 ,\Ymap[4]_i_31_n_0 ,\Ymap[4]_i_32_n_0 ,\Ymap[4]_i_33_n_0 }),
        .O({\Ymap_reg[4]_i_13_n_4 ,\Ymap_reg[4]_i_13_n_5 ,\Ymap_reg[4]_i_13_n_6 ,\Ymap_reg[4]_i_13_n_7 }),
        .S({\Ymap[4]_i_34_n_0 ,\Ymap[4]_i_35_n_0 ,\Ymap[4]_i_36_n_0 ,\Ymap[4]_i_37_n_0 }));
  CARRY4 \Ymap_reg[4]_i_2 
       (.CI(\Ymap_reg[0]_i_2_n_0 ),
        .CO({\Ymap_reg[4]_i_2_n_0 ,\Ymap_reg[4]_i_2_n_1 ,\Ymap_reg[4]_i_2_n_2 ,\Ymap_reg[4]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[4]_i_3_n_0 ,\Ymap[4]_i_4_n_0 ,\Ymap[4]_i_5_n_0 ,\Ymap[4]_i_6_n_0 }),
        .O({\Ymap_reg[4]_i_2_n_4 ,\Ymap_reg[4]_i_2_n_5 ,\Ymap_reg[4]_i_2_n_6 ,\Ymap_reg[4]_i_2_n_7 }),
        .S({\Ymap[4]_i_7_n_0 ,\Ymap[4]_i_8_n_0 ,\Ymap[4]_i_9_n_0 ,\Ymap[4]_i_10_n_0 }));
  CARRY4 \Ymap_reg[4]_i_38 
       (.CI(\Ymap_reg[0]_i_95_n_0 ),
        .CO({\Ymap_reg[4]_i_38_n_0 ,\Ymap_reg[4]_i_38_n_1 ,\Ymap_reg[4]_i_38_n_2 ,\Ymap_reg[4]_i_38_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[5]_i_96_n_0 ,\Ymap[5]_i_97_n_0 ,\Ymap[5]_i_98_n_0 ,\Ymap[5]_i_99_n_0 }),
        .O({\Ymap_reg[4]_i_38_n_4 ,\Ymap_reg[4]_i_38_n_5 ,\Ymap_reg[4]_i_38_n_6 ,\Ymap_reg[4]_i_38_n_7 }),
        .S({\Ymap[4]_i_40_n_0 ,\Ymap[4]_i_41_n_0 ,\Ymap[4]_i_42_n_0 ,\Ymap[4]_i_43_n_0 }));
  CARRY4 \Ymap_reg[4]_i_39 
       (.CI(\Ymap_reg[0]_i_83_n_0 ),
        .CO({\Ymap_reg[4]_i_39_n_0 ,\Ymap_reg[4]_i_39_n_1 ,\Ymap_reg[4]_i_39_n_2 ,\Ymap_reg[4]_i_39_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[5]_i_80_n_0 ,\Ymap[5]_i_81_n_0 ,\Ymap[5]_i_82_n_0 ,\Ymap[5]_i_83_n_0 }),
        .O({\Ymap_reg[4]_i_39_n_4 ,\Ymap_reg[4]_i_39_n_5 ,\Ymap_reg[4]_i_39_n_6 ,\Ymap_reg[4]_i_39_n_7 }),
        .S({\Ymap[4]_i_44_n_0 ,\Ymap[4]_i_45_n_0 ,\Ymap[4]_i_46_n_0 ,\Ymap[4]_i_47_n_0 }));
  FDRE \Ymap_reg[5] 
       (.C(clk),
        .CE(\Xmap[6]_i_1_n_0 ),
        .D(\Ymap[5]_i_1_n_0 ),
        .Q(Ymap[5]),
        .R(1'b0));
  CARRY4 \Ymap_reg[5]_i_104 
       (.CI(\Ymap_reg[5]_i_131_n_0 ),
        .CO({\Ymap_reg[5]_i_104_n_0 ,\Ymap_reg[5]_i_104_n_1 ,\Ymap_reg[5]_i_104_n_2 ,\Ymap_reg[5]_i_104_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap_reg[5]_i_132_n_4 ,\Ymap_reg[5]_i_132_n_5 ,\Ymap_reg[5]_i_132_n_6 ,\Ymap_reg[5]_i_132_n_7 }),
        .O({\Ymap_reg[5]_i_104_n_4 ,\Ymap_reg[5]_i_104_n_5 ,\Ymap_reg[5]_i_104_n_6 ,\Ymap_reg[5]_i_104_n_7 }),
        .S({\Ymap[5]_i_133_n_0 ,\Ymap[5]_i_134_n_0 ,\Ymap[5]_i_135_n_0 ,\Ymap[5]_i_136_n_0 }));
  CARRY4 \Ymap_reg[5]_i_105 
       (.CI(\Ymap_reg[5]_i_132_n_0 ),
        .CO({\Ymap_reg[5]_i_105_n_0 ,\Ymap_reg[5]_i_105_n_1 ,\Ymap_reg[5]_i_105_n_2 ,\Ymap_reg[5]_i_105_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[5]_i_137_n_0 ,\Ymap[5]_i_138_n_0 ,\Ymap[5]_i_139_n_0 ,\Ymap[5]_i_140_n_0 }),
        .O({\Ymap_reg[5]_i_105_n_4 ,\Ymap_reg[5]_i_105_n_5 ,\Ymap_reg[5]_i_105_n_6 ,\Ymap_reg[5]_i_105_n_7 }),
        .S({\Ymap[5]_i_141_n_0 ,\Ymap[5]_i_142_n_0 ,\Ymap[5]_i_143_n_0 ,\Ymap[5]_i_144_n_0 }));
  CARRY4 \Ymap_reg[5]_i_117 
       (.CI(\Ymap_reg[5]_i_150_n_0 ),
        .CO({\Ymap_reg[5]_i_117_n_0 ,\Ymap_reg[5]_i_117_n_1 ,\Ymap_reg[5]_i_117_n_2 ,\Ymap_reg[5]_i_117_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[5]_i_151_n_0 ,\Ymap[5]_i_152_n_0 ,\Ymap[5]_i_153_n_0 ,\Ymap[5]_i_154_n_0 }),
        .O(\NLW_Ymap_reg[5]_i_117_O_UNCONNECTED [3:0]),
        .S({\Ymap[5]_i_155_n_0 ,\Ymap[5]_i_156_n_0 ,\Ymap[5]_i_157_n_0 ,\Ymap[5]_i_158_n_0 }));
  CARRY4 \Ymap_reg[5]_i_126 
       (.CI(\Ymap_reg[5]_i_128_n_0 ),
        .CO({\Ymap_reg[5]_i_126_n_0 ,\Ymap_reg[5]_i_126_n_1 ,\Ymap_reg[5]_i_126_n_2 ,\Ymap_reg[5]_i_126_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[5]_i_159_n_0 ,\Ymap[5]_i_160_n_0 ,\Ymap[5]_i_161_n_0 ,\Ymap[5]_i_162_n_0 }),
        .O({\Ymap_reg[5]_i_126_n_4 ,\Ymap_reg[5]_i_126_n_5 ,\Ymap_reg[5]_i_126_n_6 ,\Ymap_reg[5]_i_126_n_7 }),
        .S({\Ymap[5]_i_163_n_0 ,\Ymap[5]_i_164_n_0 ,\Ymap[5]_i_165_n_0 ,\Ymap[5]_i_166_n_0 }));
  CARRY4 \Ymap_reg[5]_i_127 
       (.CI(\Ymap_reg[5]_i_129_n_0 ),
        .CO({\NLW_Ymap_reg[5]_i_127_CO_UNCONNECTED [3],\Ymap_reg[5]_i_127_n_1 ,\NLW_Ymap_reg[5]_i_127_CO_UNCONNECTED [1],\Ymap_reg[5]_i_127_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,\cnt_reg_n_0_[30] ,\cnt_reg_n_0_[29] }),
        .O({\NLW_Ymap_reg[5]_i_127_O_UNCONNECTED [3:2],\Ymap_reg[5]_i_127_n_6 ,\Ymap_reg[5]_i_127_n_7 }),
        .S({1'b0,1'b1,\Ymap[5]_i_167_n_0 ,\Ymap[5]_i_168_n_0 }));
  CARRY4 \Ymap_reg[5]_i_128 
       (.CI(\Ymap_reg[4]_i_38_n_0 ),
        .CO({\Ymap_reg[5]_i_128_n_0 ,\Ymap_reg[5]_i_128_n_1 ,\Ymap_reg[5]_i_128_n_2 ,\Ymap_reg[5]_i_128_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[5]_i_169_n_0 ,\Ymap[5]_i_170_n_0 ,\Ymap[5]_i_171_n_0 ,\Ymap[5]_i_172_n_0 }),
        .O({\Ymap_reg[5]_i_128_n_4 ,\Ymap_reg[5]_i_128_n_5 ,\Ymap_reg[5]_i_128_n_6 ,\Ymap_reg[5]_i_128_n_7 }),
        .S({\Ymap[5]_i_173_n_0 ,\Ymap[5]_i_174_n_0 ,\Ymap[5]_i_175_n_0 ,\Ymap[5]_i_176_n_0 }));
  CARRY4 \Ymap_reg[5]_i_129 
       (.CI(\Ymap_reg[4]_i_39_n_0 ),
        .CO({\Ymap_reg[5]_i_129_n_0 ,\Ymap_reg[5]_i_129_n_1 ,\Ymap_reg[5]_i_129_n_2 ,\Ymap_reg[5]_i_129_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[5]_i_177_n_0 ,\Ymap[5]_i_178_n_0 ,\Ymap[5]_i_179_n_0 ,\Ymap[5]_i_180_n_0 }),
        .O({\Ymap_reg[5]_i_129_n_4 ,\Ymap_reg[5]_i_129_n_5 ,\Ymap_reg[5]_i_129_n_6 ,\Ymap_reg[5]_i_129_n_7 }),
        .S({\Ymap[5]_i_181_n_0 ,\Ymap[5]_i_182_n_0 ,\Ymap[5]_i_183_n_0 ,\Ymap[5]_i_184_n_0 }));
  CARRY4 \Ymap_reg[5]_i_130 
       (.CI(\Ymap_reg[5]_i_126_n_0 ),
        .CO({\Ymap_reg[5]_i_130_n_0 ,\Ymap_reg[5]_i_130_n_1 ,\Ymap_reg[5]_i_130_n_2 ,\Ymap_reg[5]_i_130_n_3 }),
        .CYINIT(1'b0),
        .DI({\cnt_reg_n_0_[30] ,\cnt_reg_n_0_[29] ,\Ymap[5]_i_185_n_0 ,\Ymap[5]_i_186_n_0 }),
        .O({\Ymap_reg[5]_i_130_n_4 ,\Ymap_reg[5]_i_130_n_5 ,\Ymap_reg[5]_i_130_n_6 ,\Ymap_reg[5]_i_130_n_7 }),
        .S({\Ymap[5]_i_187_n_0 ,\Ymap[5]_i_188_n_0 ,\Ymap[5]_i_189_n_0 ,\Ymap[5]_i_190_n_0 }));
  CARRY4 \Ymap_reg[5]_i_131 
       (.CI(\Ymap_reg[5]_i_191_n_0 ),
        .CO({\Ymap_reg[5]_i_131_n_0 ,\Ymap_reg[5]_i_131_n_1 ,\Ymap_reg[5]_i_131_n_2 ,\Ymap_reg[5]_i_131_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap_reg[5]_i_192_n_4 ,\Ymap_reg[5]_i_192_n_5 ,\Ymap_reg[5]_i_192_n_6 ,\Ymap_reg[5]_i_192_n_7 }),
        .O({\Ymap_reg[5]_i_131_n_4 ,\Ymap_reg[5]_i_131_n_5 ,\Ymap_reg[5]_i_131_n_6 ,\Ymap_reg[5]_i_131_n_7 }),
        .S({\Ymap[5]_i_193_n_0 ,\Ymap[5]_i_194_n_0 ,\Ymap[5]_i_195_n_0 ,\Ymap[5]_i_196_n_0 }));
  CARRY4 \Ymap_reg[5]_i_132 
       (.CI(\Ymap_reg[5]_i_192_n_0 ),
        .CO({\Ymap_reg[5]_i_132_n_0 ,\Ymap_reg[5]_i_132_n_1 ,\Ymap_reg[5]_i_132_n_2 ,\Ymap_reg[5]_i_132_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[5]_i_197_n_0 ,\Ymap[5]_i_198_n_0 ,\Ymap[5]_i_199_n_0 ,\Ymap[5]_i_200_n_0 }),
        .O({\Ymap_reg[5]_i_132_n_4 ,\Ymap_reg[5]_i_132_n_5 ,\Ymap_reg[5]_i_132_n_6 ,\Ymap_reg[5]_i_132_n_7 }),
        .S({\Ymap[5]_i_201_n_0 ,\Ymap[5]_i_202_n_0 ,\Ymap[5]_i_203_n_0 ,\Ymap[5]_i_204_n_0 }));
  CARRY4 \Ymap_reg[5]_i_145 
       (.CI(\Ymap_reg[5]_i_207_n_0 ),
        .CO({\NLW_Ymap_reg[5]_i_145_CO_UNCONNECTED [3],\Ymap_reg[5]_i_145_n_1 ,\NLW_Ymap_reg[5]_i_145_CO_UNCONNECTED [1],\Ymap_reg[5]_i_145_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,\cnt_reg_n_0_[30] ,\cnt_reg_n_0_[29] }),
        .O({\NLW_Ymap_reg[5]_i_145_O_UNCONNECTED [3:2],\Ymap_reg[5]_i_145_n_6 ,\Ymap_reg[5]_i_145_n_7 }),
        .S({1'b0,1'b1,\Ymap[5]_i_208_n_0 ,\Ymap[5]_i_209_n_0 }));
  CARRY4 \Ymap_reg[5]_i_146 
       (.CI(\Ymap_reg[5]_i_205_n_0 ),
        .CO({\Ymap_reg[5]_i_146_n_0 ,\Ymap_reg[5]_i_146_n_1 ,\Ymap_reg[5]_i_146_n_2 ,\Ymap_reg[5]_i_146_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[5]_i_210_n_0 ,\Ymap[5]_i_211_n_0 ,\Ymap[5]_i_212_n_0 ,\Ymap[5]_i_213_n_0 }),
        .O({\Ymap_reg[5]_i_146_n_4 ,\Ymap_reg[5]_i_146_n_5 ,\Ymap_reg[5]_i_146_n_6 ,\Ymap_reg[5]_i_146_n_7 }),
        .S({\Ymap[5]_i_214_n_0 ,\Ymap[5]_i_215_n_0 ,\Ymap[5]_i_216_n_0 ,\Ymap[5]_i_217_n_0 }));
  CARRY4 \Ymap_reg[5]_i_147 
       (.CI(\Ymap_reg[5]_i_206_n_0 ),
        .CO({\Ymap_reg[5]_i_147_n_0 ,\Ymap_reg[5]_i_147_n_1 ,\Ymap_reg[5]_i_147_n_2 ,\Ymap_reg[5]_i_147_n_3 }),
        .CYINIT(1'b0),
        .DI({\cnt_reg_n_0_[30] ,\cnt_reg_n_0_[29] ,\Ymap[5]_i_218_n_0 ,\Ymap[5]_i_219_n_0 }),
        .O({\Ymap_reg[5]_i_147_n_4 ,\Ymap_reg[5]_i_147_n_5 ,\Ymap_reg[5]_i_147_n_6 ,\Ymap_reg[5]_i_147_n_7 }),
        .S({\Ymap[5]_i_220_n_0 ,\Ymap[5]_i_221_n_0 ,\Ymap[5]_i_222_n_0 ,\Ymap[5]_i_223_n_0 }));
  CARRY4 \Ymap_reg[5]_i_148 
       (.CI(\Ymap_reg[5]_i_147_n_0 ),
        .CO({\NLW_Ymap_reg[5]_i_148_CO_UNCONNECTED [3:1],\Ymap_reg[5]_i_148_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_Ymap_reg[5]_i_148_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,1'b1}));
  CARRY4 \Ymap_reg[5]_i_149 
       (.CI(\Ymap_reg[5]_i_146_n_0 ),
        .CO(\NLW_Ymap_reg[5]_i_149_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_Ymap_reg[5]_i_149_O_UNCONNECTED [3:1],\Ymap_reg[5]_i_149_n_7 }),
        .S({1'b0,1'b0,1'b0,\Ymap[5]_i_224_n_0 }));
  CARRY4 \Ymap_reg[5]_i_150 
       (.CI(\Ymap_reg[5]_i_225_n_0 ),
        .CO({\Ymap_reg[5]_i_150_n_0 ,\Ymap_reg[5]_i_150_n_1 ,\Ymap_reg[5]_i_150_n_2 ,\Ymap_reg[5]_i_150_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[5]_i_226_n_0 ,\Ymap[5]_i_227_n_0 ,\Ymap[5]_i_228_n_0 ,\Ymap[5]_i_229_n_0 }),
        .O(\NLW_Ymap_reg[5]_i_150_O_UNCONNECTED [3:0]),
        .S({\Ymap[5]_i_230_n_0 ,\Ymap[5]_i_231_n_0 ,\Ymap[5]_i_232_n_0 ,\Ymap[5]_i_233_n_0 }));
  CARRY4 \Ymap_reg[5]_i_191 
       (.CI(\Ymap_reg[5]_i_234_n_0 ),
        .CO({\Ymap_reg[5]_i_191_n_0 ,\Ymap_reg[5]_i_191_n_1 ,\Ymap_reg[5]_i_191_n_2 ,\Ymap_reg[5]_i_191_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap_reg[5]_i_3_n_4 ,\Ymap_reg[5]_i_3_n_5 ,\Ymap_reg[5]_i_3_n_6 ,\Ymap_reg[5]_i_3_n_7 }),
        .O({\Ymap_reg[5]_i_191_n_4 ,\Ymap_reg[5]_i_191_n_5 ,\Ymap_reg[5]_i_191_n_6 ,\Ymap_reg[5]_i_191_n_7 }),
        .S({\Ymap[5]_i_235_n_0 ,\Ymap[5]_i_236_n_0 ,\Ymap[5]_i_237_n_0 ,\Ymap[5]_i_238_n_0 }));
  CARRY4 \Ymap_reg[5]_i_192 
       (.CI(\Ymap_reg[5]_i_3_n_0 ),
        .CO({\Ymap_reg[5]_i_192_n_0 ,\Ymap_reg[5]_i_192_n_1 ,\Ymap_reg[5]_i_192_n_2 ,\Ymap_reg[5]_i_192_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[5]_i_239_n_0 ,\Ymap[5]_i_240_n_0 ,\Ymap[5]_i_241_n_0 ,\Ymap[5]_i_242_n_0 }),
        .O({\Ymap_reg[5]_i_192_n_4 ,\Ymap_reg[5]_i_192_n_5 ,\Ymap_reg[5]_i_192_n_6 ,\Ymap_reg[5]_i_192_n_7 }),
        .S({\Ymap[5]_i_243_n_0 ,\Ymap[5]_i_244_n_0 ,\Ymap[5]_i_245_n_0 ,\Ymap[5]_i_246_n_0 }));
  CARRY4 \Ymap_reg[5]_i_2 
       (.CI(\Ymap_reg[5]_i_6_n_0 ),
        .CO({\NLW_Ymap_reg[5]_i_2_CO_UNCONNECTED [3],\Ymap_reg[5]_i_2_n_1 ,\Ymap_reg[5]_i_2_n_2 ,\Ymap_reg[5]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,\Ymap[5]_i_7_n_0 ,\Ymap[5]_i_8_n_0 ,\Ymap[5]_i_9_n_0 }),
        .O(\NLW_Ymap_reg[5]_i_2_O_UNCONNECTED [3:0]),
        .S({1'b0,\Ymap[5]_i_10_n_0 ,\Ymap[5]_i_11_n_0 ,\Ymap[5]_i_12_n_0 }));
  CARRY4 \Ymap_reg[5]_i_205 
       (.CI(\Ymap_reg[5]_i_247_n_0 ),
        .CO({\Ymap_reg[5]_i_205_n_0 ,\Ymap_reg[5]_i_205_n_1 ,\Ymap_reg[5]_i_205_n_2 ,\Ymap_reg[5]_i_205_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[5]_i_249_n_0 ,\Ymap[5]_i_250_n_0 ,\Ymap[5]_i_251_n_0 ,\Ymap[5]_i_252_n_0 }),
        .O({\Ymap_reg[5]_i_205_n_4 ,\Ymap_reg[5]_i_205_n_5 ,\Ymap_reg[5]_i_205_n_6 ,\Ymap_reg[5]_i_205_n_7 }),
        .S({\Ymap[5]_i_253_n_0 ,\Ymap[5]_i_254_n_0 ,\Ymap[5]_i_255_n_0 ,\Ymap[5]_i_256_n_0 }));
  CARRY4 \Ymap_reg[5]_i_206 
       (.CI(\Ymap_reg[5]_i_248_n_0 ),
        .CO({\Ymap_reg[5]_i_206_n_0 ,\Ymap_reg[5]_i_206_n_1 ,\Ymap_reg[5]_i_206_n_2 ,\Ymap_reg[5]_i_206_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[5]_i_257_n_0 ,\Ymap[5]_i_160_n_0 ,\Ymap[5]_i_161_n_0 ,\Ymap[5]_i_162_n_0 }),
        .O({\Ymap_reg[5]_i_206_n_4 ,\Ymap_reg[5]_i_206_n_5 ,\Ymap_reg[5]_i_206_n_6 ,\Ymap_reg[5]_i_206_n_7 }),
        .S({\Ymap[5]_i_258_n_0 ,\Ymap[5]_i_259_n_0 ,\Ymap[5]_i_260_n_0 ,\Ymap[5]_i_261_n_0 }));
  CARRY4 \Ymap_reg[5]_i_207 
       (.CI(\Ymap_reg[5]_i_38_n_0 ),
        .CO({\Ymap_reg[5]_i_207_n_0 ,\Ymap_reg[5]_i_207_n_1 ,\Ymap_reg[5]_i_207_n_2 ,\Ymap_reg[5]_i_207_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[5]_i_262_n_0 ,\Ymap[5]_i_263_n_0 ,\Ymap[5]_i_264_n_0 ,\Ymap[5]_i_265_n_0 }),
        .O({\Ymap_reg[5]_i_207_n_4 ,\Ymap_reg[5]_i_207_n_5 ,\Ymap_reg[5]_i_207_n_6 ,\Ymap_reg[5]_i_207_n_7 }),
        .S({\Ymap[5]_i_266_n_0 ,\Ymap[5]_i_267_n_0 ,\Ymap[5]_i_268_n_0 ,\Ymap[5]_i_269_n_0 }));
  CARRY4 \Ymap_reg[5]_i_21 
       (.CI(\Ymap_reg[5]_i_41_n_0 ),
        .CO({\Ymap_reg[5]_i_21_n_0 ,\Ymap_reg[5]_i_21_n_1 ,\Ymap_reg[5]_i_21_n_2 ,\Ymap_reg[5]_i_21_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap_reg[5]_i_42_n_4 ,\Ymap_reg[5]_i_42_n_5 ,\Ymap_reg[5]_i_42_n_6 ,\Ymap_reg[5]_i_42_n_7 }),
        .O({\Ymap_reg[5]_i_21_n_4 ,\Ymap_reg[5]_i_21_n_5 ,\Ymap_reg[5]_i_21_n_6 ,\Ymap_reg[5]_i_21_n_7 }),
        .S({\Ymap[5]_i_43_n_0 ,\Ymap[5]_i_44_n_0 ,\Ymap[5]_i_45_n_0 ,\Ymap[5]_i_46_n_0 }));
  CARRY4 \Ymap_reg[5]_i_225 
       (.CI(1'b0),
        .CO({\Ymap_reg[5]_i_225_n_0 ,\Ymap_reg[5]_i_225_n_1 ,\Ymap_reg[5]_i_225_n_2 ,\Ymap_reg[5]_i_225_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[5]_i_271_n_0 ,\Ymap[5]_i_272_n_0 ,\Ymap[5]_i_273_n_0 ,1'b0}),
        .O(\NLW_Ymap_reg[5]_i_225_O_UNCONNECTED [3:0]),
        .S({\Ymap[5]_i_274_n_0 ,\Ymap[5]_i_275_n_0 ,\Ymap[5]_i_276_n_0 ,\Ymap[5]_i_277_n_0 }));
  CARRY4 \Ymap_reg[5]_i_234 
       (.CI(1'b0),
        .CO({\Ymap_reg[5]_i_234_n_0 ,\Ymap_reg[5]_i_234_n_1 ,\Ymap_reg[5]_i_234_n_2 ,\Ymap_reg[5]_i_234_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap_reg[4]_i_2_n_4 ,\Ymap_reg[4]_i_2_n_5 ,\Ymap_reg[4]_i_2_n_6 ,1'b0}),
        .O({\Ymap_reg[5]_i_234_n_4 ,\Ymap_reg[5]_i_234_n_5 ,\Ymap_reg[5]_i_234_n_6 ,\Ymap_reg[5]_i_234_n_7 }),
        .S({\Ymap[5]_i_278_n_0 ,\Ymap[5]_i_279_n_0 ,\Ymap[5]_i_280_n_0 ,\Ymap[5]_i_281_n_0 }));
  CARRY4 \Ymap_reg[5]_i_247 
       (.CI(\Ymap_reg[5]_i_39_n_0 ),
        .CO({\Ymap_reg[5]_i_247_n_0 ,\Ymap_reg[5]_i_247_n_1 ,\Ymap_reg[5]_i_247_n_2 ,\Ymap_reg[5]_i_247_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[5]_i_282_n_0 ,\Ymap[5]_i_283_n_0 ,\Ymap[5]_i_284_n_0 ,\Ymap[5]_i_285_n_0 }),
        .O({\Ymap_reg[5]_i_247_n_4 ,\Ymap_reg[5]_i_247_n_5 ,\Ymap_reg[5]_i_247_n_6 ,\Ymap_reg[5]_i_247_n_7 }),
        .S({\Ymap[5]_i_286_n_0 ,\Ymap[5]_i_287_n_0 ,\Ymap[5]_i_288_n_0 ,\Ymap[5]_i_289_n_0 }));
  CARRY4 \Ymap_reg[5]_i_248 
       (.CI(\Ymap_reg[5]_i_40_n_0 ),
        .CO({\Ymap_reg[5]_i_248_n_0 ,\Ymap_reg[5]_i_248_n_1 ,\Ymap_reg[5]_i_248_n_2 ,\Ymap_reg[5]_i_248_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[5]_i_169_n_0 ,\Ymap[5]_i_170_n_0 ,\Ymap[5]_i_171_n_0 ,\Ymap[5]_i_172_n_0 }),
        .O({\Ymap_reg[5]_i_248_n_4 ,\Ymap_reg[5]_i_248_n_5 ,\Ymap_reg[5]_i_248_n_6 ,\Ymap_reg[5]_i_248_n_7 }),
        .S({\Ymap[5]_i_290_n_0 ,\Ymap[5]_i_291_n_0 ,\Ymap[5]_i_292_n_0 ,\Ymap[5]_i_293_n_0 }));
  CARRY4 \Ymap_reg[5]_i_26 
       (.CI(\Ymap_reg[5]_i_47_n_0 ),
        .CO({\Ymap_reg[5]_i_26_n_0 ,\Ymap_reg[5]_i_26_n_1 ,\Ymap_reg[5]_i_26_n_2 ,\Ymap_reg[5]_i_26_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[5]_i_48_n_0 ,\Ymap[5]_i_49_n_0 ,\Ymap[5]_i_50_n_0 ,\Ymap[5]_i_51_n_0 }),
        .O(\NLW_Ymap_reg[5]_i_26_O_UNCONNECTED [3:0]),
        .S({\Ymap[5]_i_52_n_0 ,\Ymap[5]_i_53_n_0 ,\Ymap[5]_i_54_n_0 ,\Ymap[5]_i_55_n_0 }));
  CARRY4 \Ymap_reg[5]_i_270 
       (.CI(\Ymap_reg[5]_i_130_n_0 ),
        .CO({\NLW_Ymap_reg[5]_i_270_CO_UNCONNECTED [3:1],\Ymap_reg[5]_i_270_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_Ymap_reg[5]_i_270_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,1'b1}));
  CARRY4 \Ymap_reg[5]_i_3 
       (.CI(\Ymap_reg[4]_i_2_n_0 ),
        .CO({\Ymap_reg[5]_i_3_n_0 ,\Ymap_reg[5]_i_3_n_1 ,\Ymap_reg[5]_i_3_n_2 ,\Ymap_reg[5]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[5]_i_13_n_0 ,\Ymap[5]_i_14_n_0 ,\Ymap[5]_i_15_n_0 ,\Ymap[5]_i_16_n_0 }),
        .O({\Ymap_reg[5]_i_3_n_4 ,\Ymap_reg[5]_i_3_n_5 ,\Ymap_reg[5]_i_3_n_6 ,\Ymap_reg[5]_i_3_n_7 }),
        .S({\Ymap[5]_i_17_n_0 ,\Ymap[5]_i_18_n_0 ,\Ymap[5]_i_19_n_0 ,\Ymap[5]_i_20_n_0 }));
  CARRY4 \Ymap_reg[5]_i_35 
       (.CI(\Ymap_reg[4]_i_11_n_0 ),
        .CO({\Ymap_reg[5]_i_35_n_0 ,\Ymap_reg[5]_i_35_n_1 ,\Ymap_reg[5]_i_35_n_2 ,\Ymap_reg[5]_i_35_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[5]_i_56_n_0 ,\Ymap[5]_i_57_n_0 ,\Ymap[5]_i_58_n_0 ,\Ymap[5]_i_59_n_0 }),
        .O({\Ymap_reg[5]_i_35_n_4 ,\Ymap_reg[5]_i_35_n_5 ,\Ymap_reg[5]_i_35_n_6 ,\Ymap_reg[5]_i_35_n_7 }),
        .S({\Ymap[5]_i_60_n_0 ,\Ymap[5]_i_61_n_0 ,\Ymap[5]_i_62_n_0 ,\Ymap[5]_i_63_n_0 }));
  CARRY4 \Ymap_reg[5]_i_36 
       (.CI(\Ymap_reg[4]_i_12_n_0 ),
        .CO({\Ymap_reg[5]_i_36_n_0 ,\Ymap_reg[5]_i_36_n_1 ,\Ymap_reg[5]_i_36_n_2 ,\Ymap_reg[5]_i_36_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[5]_i_64_n_0 ,\Ymap[5]_i_65_n_0 ,\Ymap[5]_i_66_n_0 ,\Ymap[5]_i_67_n_0 }),
        .O({\Ymap_reg[5]_i_36_n_4 ,\Ymap_reg[5]_i_36_n_5 ,\Ymap_reg[5]_i_36_n_6 ,\Ymap_reg[5]_i_36_n_7 }),
        .S({\Ymap[5]_i_68_n_0 ,\Ymap[5]_i_69_n_0 ,\Ymap[5]_i_70_n_0 ,\Ymap[5]_i_71_n_0 }));
  CARRY4 \Ymap_reg[5]_i_37 
       (.CI(\Ymap_reg[4]_i_13_n_0 ),
        .CO({\Ymap_reg[5]_i_37_n_0 ,\Ymap_reg[5]_i_37_n_1 ,\Ymap_reg[5]_i_37_n_2 ,\Ymap_reg[5]_i_37_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[5]_i_72_n_0 ,\Ymap[5]_i_73_n_0 ,\Ymap[5]_i_74_n_0 ,\Ymap[5]_i_75_n_0 }),
        .O({\Ymap_reg[5]_i_37_n_4 ,\Ymap_reg[5]_i_37_n_5 ,\Ymap_reg[5]_i_37_n_6 ,\Ymap_reg[5]_i_37_n_7 }),
        .S({\Ymap[5]_i_76_n_0 ,\Ymap[5]_i_77_n_0 ,\Ymap[5]_i_78_n_0 ,\Ymap[5]_i_79_n_0 }));
  CARRY4 \Ymap_reg[5]_i_38 
       (.CI(\Ymap_reg[5]_i_35_n_0 ),
        .CO({\Ymap_reg[5]_i_38_n_0 ,\Ymap_reg[5]_i_38_n_1 ,\Ymap_reg[5]_i_38_n_2 ,\Ymap_reg[5]_i_38_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[5]_i_80_n_0 ,\Ymap[5]_i_81_n_0 ,\Ymap[5]_i_82_n_0 ,\Ymap[5]_i_83_n_0 }),
        .O({\Ymap_reg[5]_i_38_n_4 ,\Ymap_reg[5]_i_38_n_5 ,\Ymap_reg[5]_i_38_n_6 ,\Ymap_reg[5]_i_38_n_7 }),
        .S({\Ymap[5]_i_84_n_0 ,\Ymap[5]_i_85_n_0 ,\Ymap[5]_i_86_n_0 ,\Ymap[5]_i_87_n_0 }));
  CARRY4 \Ymap_reg[5]_i_39 
       (.CI(\Ymap_reg[5]_i_36_n_0 ),
        .CO({\Ymap_reg[5]_i_39_n_0 ,\Ymap_reg[5]_i_39_n_1 ,\Ymap_reg[5]_i_39_n_2 ,\Ymap_reg[5]_i_39_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[5]_i_88_n_0 ,\Ymap[5]_i_89_n_0 ,\Ymap[5]_i_90_n_0 ,\Ymap[5]_i_91_n_0 }),
        .O({\Ymap_reg[5]_i_39_n_4 ,\Ymap_reg[5]_i_39_n_5 ,\Ymap_reg[5]_i_39_n_6 ,\Ymap_reg[5]_i_39_n_7 }),
        .S({\Ymap[5]_i_92_n_0 ,\Ymap[5]_i_93_n_0 ,\Ymap[5]_i_94_n_0 ,\Ymap[5]_i_95_n_0 }));
  CARRY4 \Ymap_reg[5]_i_4 
       (.CI(\Ymap_reg[5]_i_21_n_0 ),
        .CO({\NLW_Ymap_reg[5]_i_4_CO_UNCONNECTED [3:1],\Ymap_reg[5]_i_4_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_Ymap_reg[5]_i_4_O_UNCONNECTED [3:2],\Ymap_reg[5]_i_4_n_6 ,\Ymap_reg[5]_i_4_n_7 }),
        .S({1'b0,1'b0,\Ymap[5]_i_22_n_0 ,\Ymap[5]_i_23_n_0 }));
  CARRY4 \Ymap_reg[5]_i_40 
       (.CI(\Ymap_reg[5]_i_37_n_0 ),
        .CO({\Ymap_reg[5]_i_40_n_0 ,\Ymap_reg[5]_i_40_n_1 ,\Ymap_reg[5]_i_40_n_2 ,\Ymap_reg[5]_i_40_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[5]_i_96_n_0 ,\Ymap[5]_i_97_n_0 ,\Ymap[5]_i_98_n_0 ,\Ymap[5]_i_99_n_0 }),
        .O({\Ymap_reg[5]_i_40_n_4 ,\Ymap_reg[5]_i_40_n_5 ,\Ymap_reg[5]_i_40_n_6 ,\Ymap_reg[5]_i_40_n_7 }),
        .S({\Ymap[5]_i_100_n_0 ,\Ymap[5]_i_101_n_0 ,\Ymap[5]_i_102_n_0 ,\Ymap[5]_i_103_n_0 }));
  CARRY4 \Ymap_reg[5]_i_41 
       (.CI(\Ymap_reg[5]_i_104_n_0 ),
        .CO({\Ymap_reg[5]_i_41_n_0 ,\Ymap_reg[5]_i_41_n_1 ,\Ymap_reg[5]_i_41_n_2 ,\Ymap_reg[5]_i_41_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap_reg[5]_i_105_n_4 ,\Ymap_reg[5]_i_105_n_5 ,\Ymap_reg[5]_i_105_n_6 ,\Ymap_reg[5]_i_105_n_7 }),
        .O({\Ymap_reg[5]_i_41_n_4 ,\Ymap_reg[5]_i_41_n_5 ,\Ymap_reg[5]_i_41_n_6 ,\Ymap_reg[5]_i_41_n_7 }),
        .S({\Ymap[5]_i_106_n_0 ,\Ymap[5]_i_107_n_0 ,\Ymap[5]_i_108_n_0 ,\Ymap[5]_i_109_n_0 }));
  CARRY4 \Ymap_reg[5]_i_42 
       (.CI(\Ymap_reg[5]_i_105_n_0 ),
        .CO({\NLW_Ymap_reg[5]_i_42_CO_UNCONNECTED [3],\Ymap_reg[5]_i_42_n_1 ,\Ymap_reg[5]_i_42_n_2 ,\Ymap_reg[5]_i_42_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,\Ymap[5]_i_110_n_0 ,\Ymap[5]_i_111_n_0 ,\Ymap[5]_i_112_n_0 }),
        .O({\Ymap_reg[5]_i_42_n_4 ,\Ymap_reg[5]_i_42_n_5 ,\Ymap_reg[5]_i_42_n_6 ,\Ymap_reg[5]_i_42_n_7 }),
        .S({\Ymap[5]_i_113_n_0 ,\Ymap[5]_i_114_n_0 ,\Ymap[5]_i_115_n_0 ,\Ymap[5]_i_116_n_0 }));
  CARRY4 \Ymap_reg[5]_i_47 
       (.CI(\Ymap_reg[5]_i_117_n_0 ),
        .CO({\Ymap_reg[5]_i_47_n_0 ,\Ymap_reg[5]_i_47_n_1 ,\Ymap_reg[5]_i_47_n_2 ,\Ymap_reg[5]_i_47_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[5]_i_118_n_0 ,\Ymap[5]_i_119_n_0 ,\Ymap[5]_i_120_n_0 ,\Ymap[5]_i_121_n_0 }),
        .O(\NLW_Ymap_reg[5]_i_47_O_UNCONNECTED [3:0]),
        .S({\Ymap[5]_i_122_n_0 ,\Ymap[5]_i_123_n_0 ,\Ymap[5]_i_124_n_0 ,\Ymap[5]_i_125_n_0 }));
  CARRY4 \Ymap_reg[5]_i_5 
       (.CI(\Ymap_reg[3]_i_2_n_0 ),
        .CO({\NLW_Ymap_reg[5]_i_5_CO_UNCONNECTED [3:1],\Ymap_reg[5]_i_5_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_Ymap_reg[5]_i_5_O_UNCONNECTED [3:2],\Ymap_reg[5]_i_5_n_6 ,\Ymap_reg[5]_i_5_n_7 }),
        .S({1'b0,1'b0,\Ymap[5]_i_24_n_0 ,\Ymap[5]_i_25_n_0 }));
  CARRY4 \Ymap_reg[5]_i_6 
       (.CI(\Ymap_reg[5]_i_26_n_0 ),
        .CO({\Ymap_reg[5]_i_6_n_0 ,\Ymap_reg[5]_i_6_n_1 ,\Ymap_reg[5]_i_6_n_2 ,\Ymap_reg[5]_i_6_n_3 }),
        .CYINIT(1'b0),
        .DI({\Ymap[5]_i_27_n_0 ,\Ymap[5]_i_28_n_0 ,\Ymap[5]_i_29_n_0 ,\Ymap[5]_i_30_n_0 }),
        .O(\NLW_Ymap_reg[5]_i_6_O_UNCONNECTED [3:0]),
        .S({\Ymap[5]_i_31_n_0 ,\Ymap[5]_i_32_n_0 ,\Ymap[5]_i_33_n_0 ,\Ymap[5]_i_34_n_0 }));
  LUT5 #(
    .INIT(32'h00A000CF)) 
    \cnt[0]_i_1__0 
       (.I0(state18_out),
        .I1(state__0[2]),
        .I2(state__0[1]),
        .I3(\cnt_reg_n_0_[0] ),
        .I4(state__0[0]),
        .O(cnt[0]));
  LUT5 #(
    .INIT(32'hA000CF00)) 
    \cnt[10]_i_1 
       (.I0(state18_out),
        .I1(state__0[2]),
        .I2(state__0[1]),
        .I3(\cnt_reg[12]_i_2_n_6 ),
        .I4(state__0[0]),
        .O(cnt[10]));
  LUT5 #(
    .INIT(32'hA000CF00)) 
    \cnt[11]_i_1 
       (.I0(state18_out),
        .I1(state__0[2]),
        .I2(state__0[1]),
        .I3(\cnt_reg[12]_i_2_n_5 ),
        .I4(state__0[0]),
        .O(cnt[11]));
  LUT5 #(
    .INIT(32'hA000CF00)) 
    \cnt[12]_i_1 
       (.I0(state18_out),
        .I1(state__0[2]),
        .I2(state__0[1]),
        .I3(\cnt_reg[12]_i_2_n_4 ),
        .I4(state__0[0]),
        .O(cnt[12]));
  LUT1 #(
    .INIT(2'h2)) 
    \cnt[12]_i_3 
       (.I0(\cnt_reg_n_0_[12] ),
        .O(\cnt[12]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \cnt[12]_i_4 
       (.I0(\cnt_reg_n_0_[11] ),
        .O(\cnt[12]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \cnt[12]_i_5 
       (.I0(\cnt_reg_n_0_[10] ),
        .O(\cnt[12]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \cnt[12]_i_6 
       (.I0(\cnt_reg_n_0_[9] ),
        .O(\cnt[12]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hA000CF00)) 
    \cnt[13]_i_1 
       (.I0(state18_out),
        .I1(state__0[2]),
        .I2(state__0[1]),
        .I3(\cnt_reg[16]_i_2_n_7 ),
        .I4(state__0[0]),
        .O(cnt[13]));
  LUT5 #(
    .INIT(32'hA000CF00)) 
    \cnt[14]_i_1 
       (.I0(state18_out),
        .I1(state__0[2]),
        .I2(state__0[1]),
        .I3(\cnt_reg[16]_i_2_n_6 ),
        .I4(state__0[0]),
        .O(cnt[14]));
  LUT5 #(
    .INIT(32'hA000CF00)) 
    \cnt[15]_i_1 
       (.I0(state18_out),
        .I1(state__0[2]),
        .I2(state__0[1]),
        .I3(\cnt_reg[16]_i_2_n_5 ),
        .I4(state__0[0]),
        .O(cnt[15]));
  LUT5 #(
    .INIT(32'hA000CF00)) 
    \cnt[16]_i_1 
       (.I0(state18_out),
        .I1(state__0[2]),
        .I2(state__0[1]),
        .I3(\cnt_reg[16]_i_2_n_4 ),
        .I4(state__0[0]),
        .O(cnt[16]));
  LUT1 #(
    .INIT(2'h2)) 
    \cnt[16]_i_3 
       (.I0(\cnt_reg_n_0_[16] ),
        .O(\cnt[16]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \cnt[16]_i_4 
       (.I0(\cnt_reg_n_0_[15] ),
        .O(\cnt[16]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \cnt[16]_i_5 
       (.I0(\cnt_reg_n_0_[14] ),
        .O(\cnt[16]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \cnt[16]_i_6 
       (.I0(\cnt_reg_n_0_[13] ),
        .O(\cnt[16]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hA000CF00)) 
    \cnt[17]_i_1 
       (.I0(state18_out),
        .I1(state__0[2]),
        .I2(state__0[1]),
        .I3(\cnt_reg[20]_i_2_n_7 ),
        .I4(state__0[0]),
        .O(cnt[17]));
  LUT5 #(
    .INIT(32'hA000CF00)) 
    \cnt[18]_i_1 
       (.I0(state18_out),
        .I1(state__0[2]),
        .I2(state__0[1]),
        .I3(\cnt_reg[20]_i_2_n_6 ),
        .I4(state__0[0]),
        .O(cnt[18]));
  LUT5 #(
    .INIT(32'hA000CF00)) 
    \cnt[19]_i_1 
       (.I0(state18_out),
        .I1(state__0[2]),
        .I2(state__0[1]),
        .I3(\cnt_reg[20]_i_2_n_5 ),
        .I4(state__0[0]),
        .O(cnt[19]));
  LUT5 #(
    .INIT(32'hA000CF00)) 
    \cnt[1]_i_1__0 
       (.I0(state18_out),
        .I1(state__0[2]),
        .I2(state__0[1]),
        .I3(\cnt_reg[4]_i_2_n_7 ),
        .I4(state__0[0]),
        .O(cnt[1]));
  LUT5 #(
    .INIT(32'hA000CF00)) 
    \cnt[20]_i_1 
       (.I0(state18_out),
        .I1(state__0[2]),
        .I2(state__0[1]),
        .I3(\cnt_reg[20]_i_2_n_4 ),
        .I4(state__0[0]),
        .O(cnt[20]));
  LUT1 #(
    .INIT(2'h2)) 
    \cnt[20]_i_3 
       (.I0(\cnt_reg_n_0_[20] ),
        .O(\cnt[20]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \cnt[20]_i_4 
       (.I0(\cnt_reg_n_0_[19] ),
        .O(\cnt[20]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \cnt[20]_i_5 
       (.I0(\cnt_reg_n_0_[18] ),
        .O(\cnt[20]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \cnt[20]_i_6 
       (.I0(\cnt_reg_n_0_[17] ),
        .O(\cnt[20]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hA000CF00)) 
    \cnt[21]_i_1 
       (.I0(state18_out),
        .I1(state__0[2]),
        .I2(state__0[1]),
        .I3(\cnt_reg[24]_i_2_n_7 ),
        .I4(state__0[0]),
        .O(cnt[21]));
  LUT5 #(
    .INIT(32'hA000CF00)) 
    \cnt[22]_i_1 
       (.I0(state18_out),
        .I1(state__0[2]),
        .I2(state__0[1]),
        .I3(\cnt_reg[24]_i_2_n_6 ),
        .I4(state__0[0]),
        .O(cnt[22]));
  LUT5 #(
    .INIT(32'hA000CF00)) 
    \cnt[23]_i_1 
       (.I0(state18_out),
        .I1(state__0[2]),
        .I2(state__0[1]),
        .I3(\cnt_reg[24]_i_2_n_5 ),
        .I4(state__0[0]),
        .O(cnt[23]));
  LUT5 #(
    .INIT(32'hA000CF00)) 
    \cnt[24]_i_1 
       (.I0(state18_out),
        .I1(state__0[2]),
        .I2(state__0[1]),
        .I3(\cnt_reg[24]_i_2_n_4 ),
        .I4(state__0[0]),
        .O(cnt[24]));
  LUT1 #(
    .INIT(2'h2)) 
    \cnt[24]_i_3 
       (.I0(\cnt_reg_n_0_[24] ),
        .O(\cnt[24]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \cnt[24]_i_4 
       (.I0(\cnt_reg_n_0_[23] ),
        .O(\cnt[24]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \cnt[24]_i_5 
       (.I0(\cnt_reg_n_0_[22] ),
        .O(\cnt[24]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \cnt[24]_i_6 
       (.I0(\cnt_reg_n_0_[21] ),
        .O(\cnt[24]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hA000CF00)) 
    \cnt[25]_i_1 
       (.I0(state18_out),
        .I1(state__0[2]),
        .I2(state__0[1]),
        .I3(\cnt_reg[28]_i_2_n_7 ),
        .I4(state__0[0]),
        .O(cnt[25]));
  LUT5 #(
    .INIT(32'hA000CF00)) 
    \cnt[26]_i_1 
       (.I0(state18_out),
        .I1(state__0[2]),
        .I2(state__0[1]),
        .I3(\cnt_reg[28]_i_2_n_6 ),
        .I4(state__0[0]),
        .O(cnt[26]));
  LUT5 #(
    .INIT(32'hA000CF00)) 
    \cnt[27]_i_1 
       (.I0(state18_out),
        .I1(state__0[2]),
        .I2(state__0[1]),
        .I3(\cnt_reg[28]_i_2_n_5 ),
        .I4(state__0[0]),
        .O(cnt[27]));
  LUT5 #(
    .INIT(32'hA000CF00)) 
    \cnt[28]_i_1 
       (.I0(state18_out),
        .I1(state__0[2]),
        .I2(state__0[1]),
        .I3(\cnt_reg[28]_i_2_n_4 ),
        .I4(state__0[0]),
        .O(cnt[28]));
  LUT1 #(
    .INIT(2'h2)) 
    \cnt[28]_i_3 
       (.I0(\cnt_reg_n_0_[28] ),
        .O(\cnt[28]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \cnt[28]_i_4 
       (.I0(\cnt_reg_n_0_[27] ),
        .O(\cnt[28]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \cnt[28]_i_5 
       (.I0(\cnt_reg_n_0_[26] ),
        .O(\cnt[28]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \cnt[28]_i_6 
       (.I0(\cnt_reg_n_0_[25] ),
        .O(\cnt[28]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hA000CF00)) 
    \cnt[29]_i_1 
       (.I0(state18_out),
        .I1(state__0[2]),
        .I2(state__0[1]),
        .I3(\cnt_reg[30]_i_3_n_7 ),
        .I4(state__0[0]),
        .O(cnt[29]));
  LUT5 #(
    .INIT(32'hA000CF00)) 
    \cnt[2]_i_1 
       (.I0(state18_out),
        .I1(state__0[2]),
        .I2(state__0[1]),
        .I3(\cnt_reg[4]_i_2_n_6 ),
        .I4(state__0[0]),
        .O(cnt[2]));
  LUT6 #(
    .INIT(64'h2222FFFFFFF33333)) 
    \cnt[30]_i_1 
       (.I0(state16_out),
        .I1(state__0[0]),
        .I2(fetching),
        .I3(led1_i_2_n_0),
        .I4(state__0[1]),
        .I5(state__0[2]),
        .O(\cnt[30]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hA000CF00)) 
    \cnt[30]_i_2 
       (.I0(state18_out),
        .I1(state__0[2]),
        .I2(state__0[1]),
        .I3(\cnt_reg[30]_i_3_n_6 ),
        .I4(state__0[0]),
        .O(cnt[30]));
  LUT1 #(
    .INIT(2'h2)) 
    \cnt[30]_i_4 
       (.I0(\cnt_reg_n_0_[30] ),
        .O(\cnt[30]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \cnt[30]_i_5 
       (.I0(\cnt_reg_n_0_[29] ),
        .O(\cnt[30]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hA000CF00)) 
    \cnt[3]_i_1 
       (.I0(state18_out),
        .I1(state__0[2]),
        .I2(state__0[1]),
        .I3(\cnt_reg[4]_i_2_n_5 ),
        .I4(state__0[0]),
        .O(cnt[3]));
  LUT5 #(
    .INIT(32'hA000CF00)) 
    \cnt[4]_i_1 
       (.I0(state18_out),
        .I1(state__0[2]),
        .I2(state__0[1]),
        .I3(\cnt_reg[4]_i_2_n_4 ),
        .I4(state__0[0]),
        .O(cnt[4]));
  LUT1 #(
    .INIT(2'h2)) 
    \cnt[4]_i_3 
       (.I0(\cnt_reg_n_0_[4] ),
        .O(\cnt[4]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \cnt[4]_i_4 
       (.I0(\cnt_reg_n_0_[3] ),
        .O(\cnt[4]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \cnt[4]_i_5 
       (.I0(\cnt_reg_n_0_[2] ),
        .O(\cnt[4]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \cnt[4]_i_6 
       (.I0(\cnt_reg_n_0_[1] ),
        .O(\cnt[4]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hA000CF00)) 
    \cnt[5]_i_1 
       (.I0(state18_out),
        .I1(state__0[2]),
        .I2(state__0[1]),
        .I3(\cnt_reg[8]_i_2_n_7 ),
        .I4(state__0[0]),
        .O(cnt[5]));
  LUT5 #(
    .INIT(32'hA000CF00)) 
    \cnt[6]_i_1 
       (.I0(state18_out),
        .I1(state__0[2]),
        .I2(state__0[1]),
        .I3(\cnt_reg[8]_i_2_n_6 ),
        .I4(state__0[0]),
        .O(cnt[6]));
  LUT5 #(
    .INIT(32'hA000CF00)) 
    \cnt[7]_i_1 
       (.I0(state18_out),
        .I1(state__0[2]),
        .I2(state__0[1]),
        .I3(\cnt_reg[8]_i_2_n_5 ),
        .I4(state__0[0]),
        .O(cnt[7]));
  LUT5 #(
    .INIT(32'hA000CF00)) 
    \cnt[8]_i_1 
       (.I0(state18_out),
        .I1(state__0[2]),
        .I2(state__0[1]),
        .I3(\cnt_reg[8]_i_2_n_4 ),
        .I4(state__0[0]),
        .O(cnt[8]));
  LUT1 #(
    .INIT(2'h2)) 
    \cnt[8]_i_3 
       (.I0(\cnt_reg_n_0_[8] ),
        .O(\cnt[8]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \cnt[8]_i_4 
       (.I0(\cnt_reg_n_0_[7] ),
        .O(\cnt[8]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \cnt[8]_i_5 
       (.I0(\cnt_reg_n_0_[6] ),
        .O(\cnt[8]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \cnt[8]_i_6 
       (.I0(\cnt_reg_n_0_[5] ),
        .O(\cnt[8]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hA000CF00)) 
    \cnt[9]_i_1 
       (.I0(state18_out),
        .I1(state__0[2]),
        .I2(state__0[1]),
        .I3(\cnt_reg[12]_i_2_n_7 ),
        .I4(state__0[0]),
        .O(cnt[9]));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[0] 
       (.C(clk),
        .CE(\cnt[30]_i_1_n_0 ),
        .D(cnt[0]),
        .Q(\cnt_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[10] 
       (.C(clk),
        .CE(\cnt[30]_i_1_n_0 ),
        .D(cnt[10]),
        .Q(\cnt_reg_n_0_[10] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[11] 
       (.C(clk),
        .CE(\cnt[30]_i_1_n_0 ),
        .D(cnt[11]),
        .Q(\cnt_reg_n_0_[11] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[12] 
       (.C(clk),
        .CE(\cnt[30]_i_1_n_0 ),
        .D(cnt[12]),
        .Q(\cnt_reg_n_0_[12] ),
        .R(1'b0));
  CARRY4 \cnt_reg[12]_i_2 
       (.CI(\cnt_reg[8]_i_2_n_0 ),
        .CO({\cnt_reg[12]_i_2_n_0 ,\cnt_reg[12]_i_2_n_1 ,\cnt_reg[12]_i_2_n_2 ,\cnt_reg[12]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[12]_i_2_n_4 ,\cnt_reg[12]_i_2_n_5 ,\cnt_reg[12]_i_2_n_6 ,\cnt_reg[12]_i_2_n_7 }),
        .S({\cnt[12]_i_3_n_0 ,\cnt[12]_i_4_n_0 ,\cnt[12]_i_5_n_0 ,\cnt[12]_i_6_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[13] 
       (.C(clk),
        .CE(\cnt[30]_i_1_n_0 ),
        .D(cnt[13]),
        .Q(\cnt_reg_n_0_[13] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[14] 
       (.C(clk),
        .CE(\cnt[30]_i_1_n_0 ),
        .D(cnt[14]),
        .Q(\cnt_reg_n_0_[14] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[15] 
       (.C(clk),
        .CE(\cnt[30]_i_1_n_0 ),
        .D(cnt[15]),
        .Q(\cnt_reg_n_0_[15] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[16] 
       (.C(clk),
        .CE(\cnt[30]_i_1_n_0 ),
        .D(cnt[16]),
        .Q(\cnt_reg_n_0_[16] ),
        .R(1'b0));
  CARRY4 \cnt_reg[16]_i_2 
       (.CI(\cnt_reg[12]_i_2_n_0 ),
        .CO({\cnt_reg[16]_i_2_n_0 ,\cnt_reg[16]_i_2_n_1 ,\cnt_reg[16]_i_2_n_2 ,\cnt_reg[16]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[16]_i_2_n_4 ,\cnt_reg[16]_i_2_n_5 ,\cnt_reg[16]_i_2_n_6 ,\cnt_reg[16]_i_2_n_7 }),
        .S({\cnt[16]_i_3_n_0 ,\cnt[16]_i_4_n_0 ,\cnt[16]_i_5_n_0 ,\cnt[16]_i_6_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[17] 
       (.C(clk),
        .CE(\cnt[30]_i_1_n_0 ),
        .D(cnt[17]),
        .Q(\cnt_reg_n_0_[17] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[18] 
       (.C(clk),
        .CE(\cnt[30]_i_1_n_0 ),
        .D(cnt[18]),
        .Q(\cnt_reg_n_0_[18] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[19] 
       (.C(clk),
        .CE(\cnt[30]_i_1_n_0 ),
        .D(cnt[19]),
        .Q(\cnt_reg_n_0_[19] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[1] 
       (.C(clk),
        .CE(\cnt[30]_i_1_n_0 ),
        .D(cnt[1]),
        .Q(\cnt_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[20] 
       (.C(clk),
        .CE(\cnt[30]_i_1_n_0 ),
        .D(cnt[20]),
        .Q(\cnt_reg_n_0_[20] ),
        .R(1'b0));
  CARRY4 \cnt_reg[20]_i_2 
       (.CI(\cnt_reg[16]_i_2_n_0 ),
        .CO({\cnt_reg[20]_i_2_n_0 ,\cnt_reg[20]_i_2_n_1 ,\cnt_reg[20]_i_2_n_2 ,\cnt_reg[20]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[20]_i_2_n_4 ,\cnt_reg[20]_i_2_n_5 ,\cnt_reg[20]_i_2_n_6 ,\cnt_reg[20]_i_2_n_7 }),
        .S({\cnt[20]_i_3_n_0 ,\cnt[20]_i_4_n_0 ,\cnt[20]_i_5_n_0 ,\cnt[20]_i_6_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[21] 
       (.C(clk),
        .CE(\cnt[30]_i_1_n_0 ),
        .D(cnt[21]),
        .Q(\cnt_reg_n_0_[21] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[22] 
       (.C(clk),
        .CE(\cnt[30]_i_1_n_0 ),
        .D(cnt[22]),
        .Q(\cnt_reg_n_0_[22] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[23] 
       (.C(clk),
        .CE(\cnt[30]_i_1_n_0 ),
        .D(cnt[23]),
        .Q(\cnt_reg_n_0_[23] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[24] 
       (.C(clk),
        .CE(\cnt[30]_i_1_n_0 ),
        .D(cnt[24]),
        .Q(\cnt_reg_n_0_[24] ),
        .R(1'b0));
  CARRY4 \cnt_reg[24]_i_2 
       (.CI(\cnt_reg[20]_i_2_n_0 ),
        .CO({\cnt_reg[24]_i_2_n_0 ,\cnt_reg[24]_i_2_n_1 ,\cnt_reg[24]_i_2_n_2 ,\cnt_reg[24]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[24]_i_2_n_4 ,\cnt_reg[24]_i_2_n_5 ,\cnt_reg[24]_i_2_n_6 ,\cnt_reg[24]_i_2_n_7 }),
        .S({\cnt[24]_i_3_n_0 ,\cnt[24]_i_4_n_0 ,\cnt[24]_i_5_n_0 ,\cnt[24]_i_6_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[25] 
       (.C(clk),
        .CE(\cnt[30]_i_1_n_0 ),
        .D(cnt[25]),
        .Q(\cnt_reg_n_0_[25] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[26] 
       (.C(clk),
        .CE(\cnt[30]_i_1_n_0 ),
        .D(cnt[26]),
        .Q(\cnt_reg_n_0_[26] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[27] 
       (.C(clk),
        .CE(\cnt[30]_i_1_n_0 ),
        .D(cnt[27]),
        .Q(\cnt_reg_n_0_[27] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[28] 
       (.C(clk),
        .CE(\cnt[30]_i_1_n_0 ),
        .D(cnt[28]),
        .Q(\cnt_reg_n_0_[28] ),
        .R(1'b0));
  CARRY4 \cnt_reg[28]_i_2 
       (.CI(\cnt_reg[24]_i_2_n_0 ),
        .CO({\cnt_reg[28]_i_2_n_0 ,\cnt_reg[28]_i_2_n_1 ,\cnt_reg[28]_i_2_n_2 ,\cnt_reg[28]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[28]_i_2_n_4 ,\cnt_reg[28]_i_2_n_5 ,\cnt_reg[28]_i_2_n_6 ,\cnt_reg[28]_i_2_n_7 }),
        .S({\cnt[28]_i_3_n_0 ,\cnt[28]_i_4_n_0 ,\cnt[28]_i_5_n_0 ,\cnt[28]_i_6_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[29] 
       (.C(clk),
        .CE(\cnt[30]_i_1_n_0 ),
        .D(cnt[29]),
        .Q(\cnt_reg_n_0_[29] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[2] 
       (.C(clk),
        .CE(\cnt[30]_i_1_n_0 ),
        .D(cnt[2]),
        .Q(\cnt_reg_n_0_[2] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[30] 
       (.C(clk),
        .CE(\cnt[30]_i_1_n_0 ),
        .D(cnt[30]),
        .Q(\cnt_reg_n_0_[30] ),
        .R(1'b0));
  CARRY4 \cnt_reg[30]_i_3 
       (.CI(\cnt_reg[28]_i_2_n_0 ),
        .CO({\NLW_cnt_reg[30]_i_3_CO_UNCONNECTED [3:1],\cnt_reg[30]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_cnt_reg[30]_i_3_O_UNCONNECTED [3:2],\cnt_reg[30]_i_3_n_6 ,\cnt_reg[30]_i_3_n_7 }),
        .S({1'b0,1'b0,\cnt[30]_i_4_n_0 ,\cnt[30]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[3] 
       (.C(clk),
        .CE(\cnt[30]_i_1_n_0 ),
        .D(cnt[3]),
        .Q(\cnt_reg_n_0_[3] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[4] 
       (.C(clk),
        .CE(\cnt[30]_i_1_n_0 ),
        .D(cnt[4]),
        .Q(\cnt_reg_n_0_[4] ),
        .R(1'b0));
  CARRY4 \cnt_reg[4]_i_2 
       (.CI(1'b0),
        .CO({\cnt_reg[4]_i_2_n_0 ,\cnt_reg[4]_i_2_n_1 ,\cnt_reg[4]_i_2_n_2 ,\cnt_reg[4]_i_2_n_3 }),
        .CYINIT(\cnt_reg_n_0_[0] ),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[4]_i_2_n_4 ,\cnt_reg[4]_i_2_n_5 ,\cnt_reg[4]_i_2_n_6 ,\cnt_reg[4]_i_2_n_7 }),
        .S({\cnt[4]_i_3_n_0 ,\cnt[4]_i_4_n_0 ,\cnt[4]_i_5_n_0 ,\cnt[4]_i_6_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[5] 
       (.C(clk),
        .CE(\cnt[30]_i_1_n_0 ),
        .D(cnt[5]),
        .Q(\cnt_reg_n_0_[5] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[6] 
       (.C(clk),
        .CE(\cnt[30]_i_1_n_0 ),
        .D(cnt[6]),
        .Q(\cnt_reg_n_0_[6] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[7] 
       (.C(clk),
        .CE(\cnt[30]_i_1_n_0 ),
        .D(cnt[7]),
        .Q(\cnt_reg_n_0_[7] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[8] 
       (.C(clk),
        .CE(\cnt[30]_i_1_n_0 ),
        .D(cnt[8]),
        .Q(\cnt_reg_n_0_[8] ),
        .R(1'b0));
  CARRY4 \cnt_reg[8]_i_2 
       (.CI(\cnt_reg[4]_i_2_n_0 ),
        .CO({\cnt_reg[8]_i_2_n_0 ,\cnt_reg[8]_i_2_n_1 ,\cnt_reg[8]_i_2_n_2 ,\cnt_reg[8]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[8]_i_2_n_4 ,\cnt_reg[8]_i_2_n_5 ,\cnt_reg[8]_i_2_n_6 ,\cnt_reg[8]_i_2_n_7 }),
        .S({\cnt[8]_i_3_n_0 ,\cnt[8]_i_4_n_0 ,\cnt[8]_i_5_n_0 ,\cnt[8]_i_6_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[9] 
       (.C(clk),
        .CE(\cnt[30]_i_1_n_0 ),
        .D(cnt[9]),
        .Q(\cnt_reg_n_0_[9] ),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hDF04)) 
    data_type_i_1
       (.I0(state__0[0]),
        .I1(state__0[1]),
        .I2(state__0[2]),
        .I3(data_type),
        .O(data_type_i_1_n_0));
  FDRE data_type_reg
       (.C(clk),
        .CE(1'b1),
        .D(data_type_i_1_n_0),
        .Q(data_type),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h9EDEBEFE18181818)) 
    fetch_i_1
       (.I0(state__0[2]),
        .I1(state__0[0]),
        .I2(state__0[1]),
        .I3(state18_out),
        .I4(state16_out),
        .I5(fetch),
        .O(fetch_i_1_n_0));
  LUT5 #(
    .INIT(32'h10110000)) 
    fetch_i_2
       (.I0(\cnt_reg_n_0_[13] ),
        .I1(\cnt_reg_n_0_[14] ),
        .I2(fetch_i_3_n_0),
        .I3(\cnt_reg_n_0_[12] ),
        .I4(state18_out),
        .O(state16_out));
  LUT6 #(
    .INIT(64'h000000000000557F)) 
    fetch_i_3
       (.I0(\cnt_reg_n_0_[9] ),
        .I1(\cnt_reg_n_0_[7] ),
        .I2(\cnt_reg_n_0_[6] ),
        .I3(\cnt_reg_n_0_[8] ),
        .I4(\cnt_reg_n_0_[11] ),
        .I5(\cnt_reg_n_0_[10] ),
        .O(fetch_i_3_n_0));
  FDRE fetch_reg
       (.C(clk),
        .CE(1'b1),
        .D(fetch_i_1_n_0),
        .Q(fetch),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    fetching_sprites_i_1
       (.I0(fetching),
        .I1(fetching_sprites_i_2_n_0),
        .I2(fetching_sprites_i_3_n_0),
        .I3(\cnt_reg_n_0_[26] ),
        .I4(\cnt_reg_n_0_[29] ),
        .I5(fetching_sprites_i_4_n_0),
        .O(state18_out));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    fetching_sprites_i_2
       (.I0(\cnt_reg_n_0_[19] ),
        .I1(\cnt_reg_n_0_[21] ),
        .I2(\cnt_reg_n_0_[15] ),
        .I3(\cnt_reg_n_0_[17] ),
        .I4(\cnt_reg_n_0_[16] ),
        .I5(fetching_sprites_i_5_n_0),
        .O(fetching_sprites_i_2_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    fetching_sprites_i_3
       (.I0(\cnt_reg_n_0_[28] ),
        .I1(\cnt_reg_n_0_[30] ),
        .O(fetching_sprites_i_3_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    fetching_sprites_i_4
       (.I0(\cnt_reg_n_0_[27] ),
        .I1(\cnt_reg_n_0_[22] ),
        .I2(\cnt_reg_n_0_[24] ),
        .I3(\cnt_reg_n_0_[25] ),
        .I4(\cnt_reg_n_0_[23] ),
        .O(fetching_sprites_i_4_n_0));
  LUT2 #(
    .INIT(4'hE)) 
    fetching_sprites_i_5
       (.I0(\cnt_reg_n_0_[18] ),
        .I1(\cnt_reg_n_0_[20] ),
        .O(fetching_sprites_i_5_n_0));
  FDRE fetching_sprites_reg
       (.C(clk),
        .CE(\pixel_out[5]_i_2_n_0 ),
        .D(state18_out),
        .Q(fetching_sprites),
        .R(\pixel_out[5]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \ind[0]_i_1 
       (.I0(fetching_sprites),
        .I1(O34),
        .O(\ind_reg[0] ));
  LUT4 #(
    .INIT(16'hFE02)) 
    led0_i_1
       (.I0(state__0[1]),
        .I1(state__0[0]),
        .I2(state__0[2]),
        .I3(led0),
        .O(led0_i_1_n_0));
  FDRE led0_reg
       (.C(clk),
        .CE(1'b1),
        .D(led0_i_1_n_0),
        .Q(led0),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFEFE4000)) 
    led1_i_1
       (.I0(state__0[2]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .I3(led1_i_2_n_0),
        .I4(led1),
        .O(led1_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    led1_i_2
       (.I0(fetching_sprites_i_4_n_0),
        .I1(\cnt_reg_n_0_[29] ),
        .I2(\cnt_reg_n_0_[26] ),
        .I3(\cnt_reg_n_0_[30] ),
        .I4(\cnt_reg_n_0_[28] ),
        .I5(fetching_sprites_i_2_n_0),
        .O(led1_i_2_n_0));
  FDRE led1_reg
       (.C(clk),
        .CE(1'b1),
        .D(led1_i_1_n_0),
        .Q(led1),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hFE40)) 
    led2_i_1
       (.I0(state__0[1]),
        .I1(state__0[2]),
        .I2(state__0[0]),
        .I3(led2),
        .O(led2_i_1_n_0));
  FDRE led2_reg
       (.C(clk),
        .CE(1'b1),
        .D(led2_i_1_n_0),
        .Q(led2),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFEFE4000)) 
    led3_i_1
       (.I0(state__0[0]),
        .I1(state__0[2]),
        .I2(state__0[1]),
        .I3(led3_i_2_n_0),
        .I4(led3),
        .O(led3_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFFFFFEEE)) 
    led3_i_2
       (.I0(\cnt_reg_n_0_[13] ),
        .I1(\cnt_reg_n_0_[14] ),
        .I2(led3_i_3_n_0),
        .I3(\cnt_reg_n_0_[12] ),
        .I4(led1_i_2_n_0),
        .O(led3_i_2_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFA888)) 
    led3_i_3
       (.I0(\cnt_reg_n_0_[9] ),
        .I1(\cnt_reg_n_0_[8] ),
        .I2(\cnt_reg_n_0_[6] ),
        .I3(\cnt_reg_n_0_[7] ),
        .I4(\cnt_reg_n_0_[11] ),
        .I5(\cnt_reg_n_0_[10] ),
        .O(led3_i_3_n_0));
  FDRE led3_reg
       (.C(clk),
        .CE(1'b1),
        .D(led3_i_1_n_0),
        .Q(led3),
        .R(1'b0));
  LUT3 #(
    .INIT(8'h02)) 
    \map_id[6]_i_1 
       (.I0(state__0[1]),
        .I1(state__0[0]),
        .I2(state__0[2]),
        .O(\map_id[6]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h24)) 
    \map_id[6]_i_2 
       (.I0(state__0[0]),
        .I1(state__0[1]),
        .I2(state__0[2]),
        .O(\map_id[6]_i_2_n_0 ));
  FDRE \map_id_reg[0] 
       (.C(clk),
        .CE(\map_id[6]_i_2_n_0 ),
        .D(\rand_reg_n_0_[0] ),
        .Q(map_id[0]),
        .R(\map_id[6]_i_1_n_0 ));
  FDRE \map_id_reg[1] 
       (.C(clk),
        .CE(\map_id[6]_i_2_n_0 ),
        .D(\rand_reg_n_0_[1] ),
        .Q(map_id[1]),
        .R(\map_id[6]_i_1_n_0 ));
  FDRE \map_id_reg[2] 
       (.C(clk),
        .CE(\map_id[6]_i_2_n_0 ),
        .D(\rand_reg_n_0_[2] ),
        .Q(map_id[2]),
        .R(\map_id[6]_i_1_n_0 ));
  FDRE \map_id_reg[3] 
       (.C(clk),
        .CE(\map_id[6]_i_2_n_0 ),
        .D(\rand_reg_n_0_[3] ),
        .Q(map_id[3]),
        .R(\map_id[6]_i_1_n_0 ));
  FDRE \map_id_reg[4] 
       (.C(clk),
        .CE(\map_id[6]_i_2_n_0 ),
        .D(\rand_reg_n_0_[4] ),
        .Q(map_id[4]),
        .R(\map_id[6]_i_1_n_0 ));
  FDRE \map_id_reg[5] 
       (.C(clk),
        .CE(\map_id[6]_i_2_n_0 ),
        .D(\rand_reg_n_0_[5] ),
        .Q(map_id[5]),
        .R(\map_id[6]_i_1_n_0 ));
  FDRE \map_id_reg[6] 
       (.C(clk),
        .CE(\map_id[6]_i_2_n_0 ),
        .D(\rand_reg_n_0_[6] ),
        .Q(map_id[6]),
        .R(\map_id[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \pixel_out[0]_i_1 
       (.I0(state18_out),
        .I1(packet_in[0]),
        .O(\pixel_out[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \pixel_out[1]_i_1 
       (.I0(state18_out),
        .I1(packet_in[1]),
        .O(\pixel_out[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \pixel_out[2]_i_1 
       (.I0(state18_out),
        .I1(packet_in[2]),
        .O(\pixel_out[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \pixel_out[3]_i_1 
       (.I0(state18_out),
        .I1(packet_in[3]),
        .O(\pixel_out[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \pixel_out[4]_i_1 
       (.I0(state18_out),
        .I1(packet_in[4]),
        .O(\pixel_out[4]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \pixel_out[5]_i_1 
       (.I0(state__0[2]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .O(\pixel_out[5]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h41414101)) 
    \pixel_out[5]_i_2 
       (.I0(state__0[2]),
        .I1(state__0[1]),
        .I2(state__0[0]),
        .I3(led1_i_2_n_0),
        .I4(fetching),
        .O(\pixel_out[5]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \pixel_out[5]_i_3 
       (.I0(state18_out),
        .I1(packet_in[5]),
        .O(\pixel_out[5]_i_3_n_0 ));
  FDRE \pixel_out_reg[0] 
       (.C(clk),
        .CE(\pixel_out[5]_i_2_n_0 ),
        .D(\pixel_out[0]_i_1_n_0 ),
        .Q(pixel[0]),
        .R(\pixel_out[5]_i_1_n_0 ));
  FDRE \pixel_out_reg[1] 
       (.C(clk),
        .CE(\pixel_out[5]_i_2_n_0 ),
        .D(\pixel_out[1]_i_1_n_0 ),
        .Q(pixel[1]),
        .R(\pixel_out[5]_i_1_n_0 ));
  FDRE \pixel_out_reg[2] 
       (.C(clk),
        .CE(\pixel_out[5]_i_2_n_0 ),
        .D(\pixel_out[2]_i_1_n_0 ),
        .Q(pixel[2]),
        .R(\pixel_out[5]_i_1_n_0 ));
  FDRE \pixel_out_reg[3] 
       (.C(clk),
        .CE(\pixel_out[5]_i_2_n_0 ),
        .D(\pixel_out[3]_i_1_n_0 ),
        .Q(pixel[3]),
        .R(\pixel_out[5]_i_1_n_0 ));
  FDRE \pixel_out_reg[4] 
       (.C(clk),
        .CE(\pixel_out[5]_i_2_n_0 ),
        .D(\pixel_out[4]_i_1_n_0 ),
        .Q(pixel[4]),
        .R(\pixel_out[5]_i_1_n_0 ));
  FDRE \pixel_out_reg[5] 
       (.C(clk),
        .CE(\pixel_out[5]_i_2_n_0 ),
        .D(\pixel_out[5]_i_3_n_0 ),
        .Q(pixel[5]),
        .R(\pixel_out[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \rand[0]_i_1 
       (.I0(\tmp_rand_reg[6]_0 [0]),
        .O(rand0[0]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \rand[1]_i_1 
       (.I0(\tmp_rand_reg[6]_0 [1]),
        .I1(\tmp_rand_reg[6]_0 [0]),
        .O(rand0[1]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hA9)) 
    \rand[2]_i_1 
       (.I0(\tmp_rand_reg[6]_0 [2]),
        .I1(\tmp_rand_reg[6]_0 [0]),
        .I2(\tmp_rand_reg[6]_0 [1]),
        .O(rand0[2]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'hAAA9)) 
    \rand[3]_i_1 
       (.I0(\tmp_rand_reg[6]_0 [3]),
        .I1(\tmp_rand_reg[6]_0 [1]),
        .I2(\tmp_rand_reg[6]_0 [0]),
        .I3(\tmp_rand_reg[6]_0 [2]),
        .O(rand0[3]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hAAAAAAA9)) 
    \rand[4]_i_1 
       (.I0(\tmp_rand_reg[6]_0 [4]),
        .I1(\tmp_rand_reg[6]_0 [2]),
        .I2(\tmp_rand_reg[6]_0 [0]),
        .I3(\tmp_rand_reg[6]_0 [1]),
        .I4(\tmp_rand_reg[6]_0 [3]),
        .O(rand0[4]));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAAAA9)) 
    \rand[5]_i_1 
       (.I0(\tmp_rand_reg[6]_0 [5]),
        .I1(\tmp_rand_reg[6]_0 [3]),
        .I2(\tmp_rand_reg[6]_0 [1]),
        .I3(\tmp_rand_reg[6]_0 [0]),
        .I4(\tmp_rand_reg[6]_0 [2]),
        .I5(\tmp_rand_reg[6]_0 [4]),
        .O(rand0[5]));
  LUT3 #(
    .INIT(8'h02)) 
    \rand[6]_i_1 
       (.I0(state__0[1]),
        .I1(state__0[2]),
        .I2(state__0[0]),
        .O(rand));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \rand[6]_i_2 
       (.I0(\tmp_rand_reg[6]_0 [6]),
        .I1(\rand[6]_i_3_n_0 ),
        .O(rand0[6]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \rand[6]_i_3 
       (.I0(\tmp_rand_reg[6]_0 [4]),
        .I1(\tmp_rand_reg[6]_0 [2]),
        .I2(\tmp_rand_reg[6]_0 [0]),
        .I3(\tmp_rand_reg[6]_0 [1]),
        .I4(\tmp_rand_reg[6]_0 [3]),
        .I5(\tmp_rand_reg[6]_0 [5]),
        .O(\rand[6]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \rand_reg[0] 
       (.C(clk),
        .CE(rand),
        .D(rand0[0]),
        .Q(\rand_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rand_reg[1] 
       (.C(clk),
        .CE(rand),
        .D(rand0[1]),
        .Q(\rand_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rand_reg[2] 
       (.C(clk),
        .CE(rand),
        .D(rand0[2]),
        .Q(\rand_reg_n_0_[2] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rand_reg[3] 
       (.C(clk),
        .CE(rand),
        .D(rand0[3]),
        .Q(\rand_reg_n_0_[3] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rand_reg[4] 
       (.C(clk),
        .CE(rand),
        .D(rand0[4]),
        .Q(\rand_reg_n_0_[4] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rand_reg[5] 
       (.C(clk),
        .CE(rand),
        .D(rand0[5]),
        .Q(\rand_reg_n_0_[5] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \rand_reg[6] 
       (.C(clk),
        .CE(rand),
        .D(rand0[6]),
        .Q(\rand_reg_n_0_[6] ),
        .R(1'b0));
  FDRE \tile_out_reg[0] 
       (.C(clk),
        .CE(\Xmap[6]_i_1_n_0 ),
        .D(packet_in[0]),
        .Q(tm_reg_0_1[0]),
        .R(1'b0));
  FDRE \tile_out_reg[1] 
       (.C(clk),
        .CE(\Xmap[6]_i_1_n_0 ),
        .D(packet_in[1]),
        .Q(tm_reg_0_1[1]),
        .R(1'b0));
  FDRE \tile_out_reg[2] 
       (.C(clk),
        .CE(\Xmap[6]_i_1_n_0 ),
        .D(packet_in[2]),
        .Q(tm_reg_0_1[2]),
        .R(1'b0));
  FDRE \tile_out_reg[3] 
       (.C(clk),
        .CE(\Xmap[6]_i_1_n_0 ),
        .D(packet_in[3]),
        .Q(tm_reg_0_1[3]),
        .R(1'b0));
  CARRY4 tm_reg_0_i_1
       (.CI(tm_reg_0_i_2_n_0),
        .CO(NLW_tm_reg_0_i_1_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_tm_reg_0_i_1_O_UNCONNECTED[3:1],ADDRARDADDR[11]}),
        .S({1'b0,1'b0,1'b0,tm_reg_0_i_9_n_0}));
  CARRY4 tm_reg_0_i_2
       (.CI(tm_reg_0_i_3_n_0),
        .CO({tm_reg_0_i_2_n_0,tm_reg_0_i_2_n_1,tm_reg_0_i_2_n_2,tm_reg_0_i_2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(ADDRARDADDR[10:7]),
        .S(\Ymap_reg[3]_0 ));
  CARRY4 tm_reg_0_i_27
       (.CI(tm_reg_0_i_28_n_0),
        .CO({p_0_out,NLW_tm_reg_0_i_27_CO_UNCONNECTED[2],tm_reg_0_i_27_n_2,tm_reg_0_i_27_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,Ymap[3]}),
        .O({NLW_tm_reg_0_i_27_O_UNCONNECTED[3],tm_reg_0[6:4]}),
        .S({1'b1,tm_reg_0_i_31_n_0,tm_reg_0_i_32_n_0,tm_reg_0_i_33_n_0}));
  CARRY4 tm_reg_0_i_28
       (.CI(1'b0),
        .CO({tm_reg_0_i_28_n_0,tm_reg_0_i_28_n_1,tm_reg_0_i_28_n_2,tm_reg_0_i_28_n_3}),
        .CYINIT(1'b0),
        .DI({Ymap[2:1],Q,1'b0}),
        .O(tm_reg_0[3:0]),
        .S({tm_reg_0_i_34_n_0,tm_reg_0_i_35_n_0,tm_reg_0_i_36_n_0,tm_reg_0_i_37_n_0}));
  CARRY4 tm_reg_0_i_3
       (.CI(1'b0),
        .CO({tm_reg_0_i_3_n_0,tm_reg_0_i_3_n_1,tm_reg_0_i_3_n_2,tm_reg_0_i_3_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,tm_reg_0_0}),
        .O({ADDRARDADDR[6:4],NLW_tm_reg_0_i_3_O_UNCONNECTED[0]}),
        .S(\Ymap_reg[2]_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    tm_reg_0_i_31
       (.I0(Ymap[5]),
        .O(tm_reg_0_i_31_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    tm_reg_0_i_32
       (.I0(Ymap[4]),
        .O(tm_reg_0_i_32_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    tm_reg_0_i_33
       (.I0(Ymap[3]),
        .I1(Ymap[5]),
        .O(tm_reg_0_i_33_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    tm_reg_0_i_34
       (.I0(Ymap[2]),
        .I1(Ymap[4]),
        .O(tm_reg_0_i_34_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    tm_reg_0_i_35
       (.I0(Ymap[1]),
        .I1(Ymap[3]),
        .O(tm_reg_0_i_35_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    tm_reg_0_i_36
       (.I0(Q),
        .I1(Ymap[2]),
        .O(tm_reg_0_i_36_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    tm_reg_0_i_37
       (.I0(Ymap[1]),
        .O(tm_reg_0_i_37_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    tm_reg_0_i_9
       (.I0(p_0_out),
        .O(tm_reg_0_i_9_n_0));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \tmp_rand[0]_i_1 
       (.I0(\cnt_reg_n_0_[0] ),
        .O(\tmp_rand[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \tmp_rand[1]_i_1 
       (.I0(\cnt_reg_n_0_[0] ),
        .I1(\cnt_reg_n_0_[1] ),
        .O(\tmp_rand[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \tmp_rand[2]_i_1 
       (.I0(\cnt_reg_n_0_[1] ),
        .I1(\cnt_reg_n_0_[0] ),
        .I2(\cnt_reg_n_0_[2] ),
        .O(\tmp_rand[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \tmp_rand[3]_i_1 
       (.I0(\cnt_reg_n_0_[2] ),
        .I1(\cnt_reg_n_0_[0] ),
        .I2(\cnt_reg_n_0_[1] ),
        .I3(\cnt_reg_n_0_[3] ),
        .O(\tmp_rand[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \tmp_rand[4]_i_1 
       (.I0(\cnt_reg_n_0_[3] ),
        .I1(\cnt_reg_n_0_[1] ),
        .I2(\cnt_reg_n_0_[0] ),
        .I3(\cnt_reg_n_0_[2] ),
        .I4(\cnt_reg_n_0_[4] ),
        .O(\tmp_rand[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \tmp_rand[5]_i_1 
       (.I0(\cnt_reg_n_0_[4] ),
        .I1(\cnt_reg_n_0_[2] ),
        .I2(\cnt_reg_n_0_[0] ),
        .I3(\cnt_reg_n_0_[1] ),
        .I4(\cnt_reg_n_0_[3] ),
        .I5(\cnt_reg_n_0_[5] ),
        .O(\tmp_rand[5]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \tmp_rand[6]_i_1 
       (.I0(state__0[0]),
        .I1(state__0[2]),
        .I2(state__0[1]),
        .O(\tmp_rand[6]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0007)) 
    \tmp_rand[6]_i_2 
       (.I0(state),
        .I1(state__0[0]),
        .I2(state__0[2]),
        .I3(state__0[1]),
        .O(\tmp_rand[6]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hB4F0)) 
    \tmp_rand[6]_i_3 
       (.I0(\tmp_rand[6]_i_5_n_0 ),
        .I1(\cnt_reg_n_0_[4] ),
        .I2(\cnt_reg_n_0_[6] ),
        .I3(\cnt_reg_n_0_[5] ),
        .O(\tmp_rand[6]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \tmp_rand[6]_i_4 
       (.I0(\tmp_rand_reg[6]_0 [6]),
        .I1(\tmp_rand_reg[6]_0 [4]),
        .I2(\tmp_rand_reg[6]_0 [5]),
        .I3(\tmp_rand[6]_i_6_n_0 ),
        .O(state));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \tmp_rand[6]_i_5 
       (.I0(\cnt_reg_n_0_[2] ),
        .I1(\cnt_reg_n_0_[0] ),
        .I2(\cnt_reg_n_0_[1] ),
        .I3(\cnt_reg_n_0_[3] ),
        .O(\tmp_rand[6]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \tmp_rand[6]_i_6 
       (.I0(\tmp_rand_reg[6]_0 [2]),
        .I1(\tmp_rand_reg[6]_0 [3]),
        .I2(\tmp_rand_reg[6]_0 [0]),
        .I3(\tmp_rand_reg[6]_0 [1]),
        .O(\tmp_rand[6]_i_6_n_0 ));
  FDRE \tmp_rand_reg[0] 
       (.C(clk),
        .CE(\tmp_rand[6]_i_2_n_0 ),
        .D(\tmp_rand[0]_i_1_n_0 ),
        .Q(D[0]),
        .R(\tmp_rand[6]_i_1_n_0 ));
  FDRE \tmp_rand_reg[1] 
       (.C(clk),
        .CE(\tmp_rand[6]_i_2_n_0 ),
        .D(\tmp_rand[1]_i_1_n_0 ),
        .Q(D[1]),
        .R(\tmp_rand[6]_i_1_n_0 ));
  FDRE \tmp_rand_reg[2] 
       (.C(clk),
        .CE(\tmp_rand[6]_i_2_n_0 ),
        .D(\tmp_rand[2]_i_1_n_0 ),
        .Q(D[2]),
        .R(\tmp_rand[6]_i_1_n_0 ));
  FDRE \tmp_rand_reg[3] 
       (.C(clk),
        .CE(\tmp_rand[6]_i_2_n_0 ),
        .D(\tmp_rand[3]_i_1_n_0 ),
        .Q(D[3]),
        .R(\tmp_rand[6]_i_1_n_0 ));
  FDRE \tmp_rand_reg[4] 
       (.C(clk),
        .CE(\tmp_rand[6]_i_2_n_0 ),
        .D(\tmp_rand[4]_i_1_n_0 ),
        .Q(D[4]),
        .R(\tmp_rand[6]_i_1_n_0 ));
  FDRE \tmp_rand_reg[5] 
       (.C(clk),
        .CE(\tmp_rand[6]_i_2_n_0 ),
        .D(\tmp_rand[5]_i_1_n_0 ),
        .Q(D[5]),
        .R(\tmp_rand[6]_i_1_n_0 ));
  FDRE \tmp_rand_reg[6] 
       (.C(clk),
        .CE(\tmp_rand[6]_i_2_n_0 ),
        .D(\tmp_rand[6]_i_3_n_0 ),
        .Q(D[6]),
        .R(\tmp_rand[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hEEEFFFFF22200000)) 
    write_enable_i_1
       (.I0(state16_out),
        .I1(write_enable_i_2_n_0),
        .I2(led3_i_2_n_0),
        .I3(fetching),
        .I4(state__0[1]),
        .I5(WEA),
        .O(write_enable_i_1_n_0));
  LUT2 #(
    .INIT(4'hB)) 
    write_enable_i_2
       (.I0(state__0[0]),
        .I1(state__0[2]),
        .O(write_enable_i_2_n_0));
  FDRE write_enable_reg
       (.C(clk),
        .CE(1'b1),
        .D(write_enable_i_1_n_0),
        .Q(WEA),
        .R(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_rendVgaTmBoot_0_2,top,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "top,Vivado 2017.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clk,
    pixel_clk,
    sw,
    vga_r,
    vga_g,
    vga_b,
    vga_hs,
    vga_vs,
    fetch,
    data_type,
    map_id,
    packet_in,
    fetching,
    led0,
    led1,
    led2,
    led3);
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk CLK" *) input clk;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 pixel_clk CLK" *) input pixel_clk;
  input [3:0]sw;
  output [4:0]vga_r;
  output [5:0]vga_g;
  output [4:0]vga_b;
  output vga_hs;
  output vga_vs;
  output fetch;
  output data_type;
  output [7:0]map_id;
  input [31:0]packet_in;
  input fetching;
  output led0;
  output led1;
  output led2;
  output led3;

  wire \<const0> ;
  wire U0_n_10;
  wire U0_n_11;
  wire U0_n_12;
  wire U0_n_13;
  wire U0_n_14;
  wire U0_n_15;
  wire U0_n_16;
  wire U0_n_17;
  wire U0_n_18;
  wire U0_n_19;
  wire U0_n_20;
  wire U0_n_21;
  wire U0_n_22;
  wire U0_n_23;
  wire U0_n_24;
  wire U0_n_25;
  wire U0_n_26;
  wire U0_n_27;
  wire U0_n_28;
  wire U0_n_29;
  wire U0_n_30;
  wire U0_n_31;
  wire U0_n_32;
  wire U0_n_33;
  wire U0_n_34;
  wire U0_n_35;
  wire U0_n_36;
  wire U0_n_37;
  wire U0_n_38;
  wire U0_n_39;
  wire U0_n_40;
  wire U0_n_41;
  wire U0_n_42;
  wire U0_n_43;
  wire U0_n_44;
  wire U0_n_45;
  wire U0_n_46;
  wire U0_n_47;
  wire U0_n_48;
  wire U0_n_49;
  wire U0_n_50;
  wire U0_n_51;
  wire U0_n_52;
  wire U0_n_53;
  wire U0_n_54;
  wire U0_n_55;
  wire U0_n_56;
  wire U0_n_57;
  wire U0_n_58;
  wire U0_n_59;
  wire U0_n_6;
  wire U0_n_60;
  wire U0_n_61;
  wire U0_n_62;
  wire U0_n_63;
  wire U0_n_64;
  wire U0_n_65;
  wire U0_n_66;
  wire U0_n_67;
  wire U0_n_68;
  wire U0_n_69;
  wire U0_n_7;
  wire U0_n_70;
  wire U0_n_71;
  wire U0_n_72;
  wire U0_n_73;
  wire U0_n_74;
  wire U0_n_75;
  wire U0_n_76;
  wire U0_n_77;
  wire U0_n_78;
  wire U0_n_79;
  wire U0_n_8;
  wire U0_n_80;
  wire U0_n_81;
  wire U0_n_82;
  wire U0_n_83;
  wire U0_n_84;
  wire U0_n_85;
  wire U0_n_86;
  wire U0_n_87;
  wire U0_n_88;
  wire U0_n_89;
  wire U0_n_9;
  wire U0_n_90;
  wire U0_n_91;
  wire U0_n_92;
  wire U0_n_93;
  wire U0_n_94;
  wire U0_n_95;
  wire U0_n_96;
  wire U0_n_97;
  wire \Xmap[6]_i_100_n_0 ;
  wire \Xmap[6]_i_101_n_0 ;
  wire \Xmap[6]_i_102_n_0 ;
  wire \Xmap[6]_i_117_n_0 ;
  wire \Xmap[6]_i_11_n_0 ;
  wire \Xmap[6]_i_12_n_0 ;
  wire \Xmap[6]_i_13_n_0 ;
  wire \Xmap[6]_i_147_n_0 ;
  wire \Xmap[6]_i_148_n_0 ;
  wire \Xmap[6]_i_149_n_0 ;
  wire \Xmap[6]_i_150_n_0 ;
  wire \Xmap[6]_i_15_n_0 ;
  wire \Xmap[6]_i_16_n_0 ;
  wire \Xmap[6]_i_17_n_0 ;
  wire \Xmap[6]_i_18_n_0 ;
  wire \Xmap[6]_i_19_n_0 ;
  wire \Xmap[6]_i_208_n_0 ;
  wire \Xmap[6]_i_209_n_0 ;
  wire \Xmap[6]_i_20_n_0 ;
  wire \Xmap[6]_i_210_n_0 ;
  wire \Xmap[6]_i_211_n_0 ;
  wire \Xmap[6]_i_21_n_0 ;
  wire \Xmap[6]_i_22_n_0 ;
  wire \Xmap[6]_i_230_n_0 ;
  wire \Xmap[6]_i_231_n_0 ;
  wire \Xmap[6]_i_232_n_0 ;
  wire \Xmap[6]_i_233_n_0 ;
  wire \Xmap[6]_i_23_n_0 ;
  wire \Xmap[6]_i_244_n_0 ;
  wire \Xmap[6]_i_245_n_0 ;
  wire \Xmap[6]_i_246_n_0 ;
  wire \Xmap[6]_i_24_n_0 ;
  wire \Xmap[6]_i_25_n_0 ;
  wire \Xmap[6]_i_26_n_0 ;
  wire \Xmap[6]_i_27_n_0 ;
  wire \Xmap[6]_i_29_n_0 ;
  wire \Xmap[6]_i_30_n_0 ;
  wire \Xmap[6]_i_31_n_0 ;
  wire \Xmap[6]_i_32_n_0 ;
  wire \Xmap[6]_i_33_n_0 ;
  wire \Xmap[6]_i_34_n_0 ;
  wire \Xmap[6]_i_35_n_0 ;
  wire \Xmap[6]_i_36_n_0 ;
  wire \Xmap[6]_i_48_n_0 ;
  wire \Xmap[6]_i_49_n_0 ;
  wire \Xmap[6]_i_50_n_0 ;
  wire \Xmap[6]_i_51_n_0 ;
  wire \Xmap[6]_i_99_n_0 ;
  wire \Xmap_reg[6]_i_8_n_2 ;
  wire \Xmap_reg[6]_i_8_n_3 ;
  wire \Xmap_reg[6]_i_8_n_5 ;
  wire \Xmap_reg[6]_i_8_n_6 ;
  wire \Xmap_reg[6]_i_8_n_7 ;
  wire clk;
  wire data_type;
  wire fetch;
  wire fetching;
  wire led0;
  wire led1;
  wire led2;
  wire led3;
  wire [6:0]\^map_id ;
  wire [31:0]packet_in;
  wire pixel_clk;
  wire sprites_data_reg_r1_0_63_0_2_i_1_n_0;
  wire [3:0]sw;
  wire [4:2]\^vga_b ;
  wire [5:2]\^vga_g ;
  wire vga_hs;
  wire [4:2]\^vga_r ;
  wire vga_vs;
  wire [3:2]\NLW_Xmap_reg[6]_i_8_CO_UNCONNECTED ;
  wire [3:3]\NLW_Xmap_reg[6]_i_8_O_UNCONNECTED ;

  assign map_id[7] = \<const0> ;
  assign map_id[6:0] = \^map_id [6:0];
  assign vga_b[4:2] = \^vga_b [4:2];
  assign vga_b[1] = \^vga_b [3];
  assign vga_b[0] = \^vga_b [4];
  assign vga_g[5:2] = \^vga_g [5:2];
  assign vga_g[1] = \^vga_g [3];
  assign vga_g[0] = \^vga_g [4];
  assign vga_r[4:2] = \^vga_r [4:2];
  assign vga_r[1] = \^vga_r [3];
  assign vga_r[0] = \^vga_r [4];
  GND GND
       (.G(\<const0> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_top U0
       (.DI({\Xmap[6]_i_29_n_0 ,\Xmap[6]_i_30_n_0 ,\Xmap[6]_i_31_n_0 ,\Xmap[6]_i_32_n_0 }),
        .O({U0_n_6,U0_n_7,U0_n_8,U0_n_9}),
        .S({\Xmap[6]_i_244_n_0 ,\Xmap[6]_i_245_n_0 ,\Xmap[6]_i_246_n_0 }),
        .\Xmap_reg[4] ({U0_n_10,U0_n_11,U0_n_12,U0_n_13}),
        .\Xmap_reg[4]_0 ({U0_n_14,U0_n_15,U0_n_16,U0_n_17}),
        .\Xmap_reg[4]_1 ({U0_n_18,U0_n_19,U0_n_20}),
        .\Xmap_reg[4]_10 ({U0_n_45,U0_n_46,U0_n_47,U0_n_48}),
        .\Xmap_reg[4]_11 ({U0_n_49,U0_n_50,U0_n_51,U0_n_52}),
        .\Xmap_reg[4]_12 ({U0_n_53,U0_n_54,U0_n_55,U0_n_56}),
        .\Xmap_reg[4]_13 (U0_n_57),
        .\Xmap_reg[4]_14 ({U0_n_58,U0_n_59,U0_n_60,U0_n_61}),
        .\Xmap_reg[4]_15 ({U0_n_62,U0_n_63,U0_n_64,U0_n_65}),
        .\Xmap_reg[4]_16 ({U0_n_66,U0_n_67,U0_n_68,U0_n_69}),
        .\Xmap_reg[4]_17 ({U0_n_70,U0_n_71,U0_n_72,U0_n_73}),
        .\Xmap_reg[4]_18 ({U0_n_74,U0_n_75,U0_n_76,U0_n_77}),
        .\Xmap_reg[4]_19 ({U0_n_78,U0_n_79,U0_n_80,U0_n_81}),
        .\Xmap_reg[4]_2 ({U0_n_21,U0_n_22,U0_n_23}),
        .\Xmap_reg[4]_20 ({U0_n_82,U0_n_83,U0_n_84,U0_n_85}),
        .\Xmap_reg[4]_21 ({U0_n_86,U0_n_87,U0_n_88,U0_n_89}),
        .\Xmap_reg[4]_22 ({U0_n_90,U0_n_91,U0_n_92,U0_n_93}),
        .\Xmap_reg[4]_23 (U0_n_94),
        .\Xmap_reg[4]_24 ({U0_n_95,U0_n_96,U0_n_97}),
        .\Xmap_reg[4]_3 ({U0_n_24,U0_n_25,U0_n_26,U0_n_27}),
        .\Xmap_reg[4]_4 (U0_n_28),
        .\Xmap_reg[4]_5 ({U0_n_29,U0_n_30,U0_n_31}),
        .\Xmap_reg[4]_6 ({U0_n_32,U0_n_33,U0_n_34,U0_n_35}),
        .\Xmap_reg[4]_7 ({U0_n_36,U0_n_37,U0_n_38,U0_n_39}),
        .\Xmap_reg[4]_8 ({U0_n_40,U0_n_41,U0_n_42,U0_n_43}),
        .\Xmap_reg[4]_9 (U0_n_44),
        .clk(clk),
        .clk_0(clk),
        .\cnt_reg[17] ({\Xmap[6]_i_208_n_0 ,\Xmap[6]_i_209_n_0 ,\Xmap[6]_i_210_n_0 ,\Xmap[6]_i_211_n_0 }),
        .\cnt_reg[17]_0 ({\Xmap[6]_i_15_n_0 ,\Xmap[6]_i_16_n_0 ,\Xmap[6]_i_17_n_0 ,\Xmap[6]_i_18_n_0 }),
        .\cnt_reg[1]_0 ({\Xmap[6]_i_99_n_0 ,\Xmap[6]_i_100_n_0 ,\Xmap[6]_i_101_n_0 ,\Xmap[6]_i_102_n_0 }),
        .\cnt_reg[1]_1 ({\Xmap[6]_i_48_n_0 ,\Xmap[6]_i_49_n_0 ,\Xmap[6]_i_50_n_0 ,\Xmap[6]_i_51_n_0 }),
        .\cnt_reg[20] (\Xmap[6]_i_117_n_0 ),
        .\cnt_reg[20]_0 ({\Xmap[6]_i_23_n_0 ,\Xmap[6]_i_24_n_0 }),
        .\cnt_reg[20]_1 ({\Xmap[6]_i_25_n_0 ,\Xmap[6]_i_26_n_0 ,\Xmap[6]_i_27_n_0 }),
        .\cnt_reg[20]_2 ({\Xmap_reg[6]_i_8_n_5 ,\Xmap_reg[6]_i_8_n_6 ,\Xmap_reg[6]_i_8_n_7 }),
        .\cnt_reg[21] ({\Xmap[6]_i_147_n_0 ,\Xmap[6]_i_148_n_0 ,\Xmap[6]_i_149_n_0 ,\Xmap[6]_i_150_n_0 }),
        .\cnt_reg[2]_0 ({\Xmap[6]_i_230_n_0 ,\Xmap[6]_i_231_n_0 ,\Xmap[6]_i_232_n_0 ,\Xmap[6]_i_233_n_0 }),
        .\cnt_reg[2]_1 ({\Xmap[6]_i_33_n_0 ,\Xmap[6]_i_34_n_0 ,\Xmap[6]_i_35_n_0 ,\Xmap[6]_i_36_n_0 }),
        .\cnt_reg[2]_2 ({\Xmap[6]_i_19_n_0 ,\Xmap[6]_i_20_n_0 ,\Xmap[6]_i_21_n_0 ,\Xmap[6]_i_22_n_0 }),
        .data_type(data_type),
        .fetch(fetch),
        .fetching(fetching),
        .led0(led0),
        .led1(led1),
        .led2(led2),
        .led3(led3),
        .lopt(sprites_data_reg_r1_0_63_0_2_i_1_n_0),
        .map_id(\^map_id ),
        .packet_in(packet_in[5:0]),
        .pixel_clk(pixel_clk),
        .sw(sw[3:1]),
        .vga_b(\^vga_b ),
        .vga_g(\^vga_g ),
        .vga_hs(vga_hs),
        .vga_r(\^vga_r ),
        .vga_vs(vga_vs));
  LUT4 #(
    .INIT(16'h8778)) 
    \Xmap[6]_i_100 
       (.I0(U0_n_25),
        .I1(U0_n_22),
        .I2(U0_n_24),
        .I3(U0_n_21),
        .O(\Xmap[6]_i_100_n_0 ));
  LUT4 #(
    .INIT(16'h8778)) 
    \Xmap[6]_i_101 
       (.I0(U0_n_26),
        .I1(U0_n_23),
        .I2(U0_n_25),
        .I3(U0_n_22),
        .O(\Xmap[6]_i_101_n_0 ));
  LUT4 #(
    .INIT(16'h8778)) 
    \Xmap[6]_i_102 
       (.I0(U0_n_27),
        .I1(U0_n_28),
        .I2(U0_n_26),
        .I3(U0_n_23),
        .O(\Xmap[6]_i_102_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Xmap[6]_i_11 
       (.I0(U0_n_95),
        .I1(U0_n_97),
        .O(\Xmap[6]_i_11_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Xmap[6]_i_117 
       (.I0(U0_n_44),
        .I1(U0_n_57),
        .O(\Xmap[6]_i_117_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Xmap[6]_i_12 
       (.I0(U0_n_96),
        .I1(U0_n_94),
        .O(\Xmap[6]_i_12_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \Xmap[6]_i_13 
       (.I0(U0_n_97),
        .O(\Xmap[6]_i_13_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Xmap[6]_i_147 
       (.I0(U0_n_40),
        .I1(U0_n_53),
        .O(\Xmap[6]_i_147_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Xmap[6]_i_148 
       (.I0(U0_n_41),
        .I1(U0_n_54),
        .O(\Xmap[6]_i_148_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Xmap[6]_i_149 
       (.I0(U0_n_42),
        .I1(U0_n_55),
        .O(\Xmap[6]_i_149_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Xmap[6]_i_15 
       (.I0(U0_n_62),
        .I1(U0_n_86),
        .I2(U0_n_74),
        .O(\Xmap[6]_i_15_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Xmap[6]_i_150 
       (.I0(U0_n_43),
        .I1(U0_n_56),
        .O(\Xmap[6]_i_150_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Xmap[6]_i_16 
       (.I0(U0_n_63),
        .I1(U0_n_87),
        .I2(U0_n_75),
        .O(\Xmap[6]_i_16_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Xmap[6]_i_17 
       (.I0(U0_n_64),
        .I1(U0_n_88),
        .I2(U0_n_76),
        .O(\Xmap[6]_i_17_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Xmap[6]_i_18 
       (.I0(U0_n_65),
        .I1(U0_n_89),
        .I2(U0_n_77),
        .O(\Xmap[6]_i_18_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Xmap[6]_i_19 
       (.I0(U0_n_74),
        .I1(U0_n_86),
        .I2(U0_n_62),
        .I3(U0_n_81),
        .I4(U0_n_93),
        .I5(U0_n_69),
        .O(\Xmap[6]_i_19_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Xmap[6]_i_20 
       (.I0(U0_n_75),
        .I1(U0_n_87),
        .I2(U0_n_63),
        .I3(U0_n_74),
        .I4(U0_n_86),
        .I5(U0_n_62),
        .O(\Xmap[6]_i_20_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Xmap[6]_i_208 
       (.I0(U0_n_36),
        .I1(U0_n_49),
        .O(\Xmap[6]_i_208_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Xmap[6]_i_209 
       (.I0(U0_n_37),
        .I1(U0_n_50),
        .O(\Xmap[6]_i_209_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Xmap[6]_i_21 
       (.I0(U0_n_76),
        .I1(U0_n_88),
        .I2(U0_n_64),
        .I3(U0_n_75),
        .I4(U0_n_87),
        .I5(U0_n_63),
        .O(\Xmap[6]_i_21_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Xmap[6]_i_210 
       (.I0(U0_n_38),
        .I1(U0_n_51),
        .O(\Xmap[6]_i_210_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Xmap[6]_i_211 
       (.I0(U0_n_39),
        .I1(U0_n_52),
        .O(\Xmap[6]_i_211_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Xmap[6]_i_22 
       (.I0(U0_n_77),
        .I1(U0_n_89),
        .I2(U0_n_65),
        .I3(U0_n_76),
        .I4(U0_n_88),
        .I5(U0_n_64),
        .O(\Xmap[6]_i_22_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Xmap[6]_i_23 
       (.I0(U0_n_68),
        .I1(U0_n_92),
        .I2(U0_n_80),
        .O(\Xmap[6]_i_23_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Xmap[6]_i_230 
       (.I0(U0_n_32),
        .I1(U0_n_45),
        .O(\Xmap[6]_i_230_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Xmap[6]_i_231 
       (.I0(U0_n_33),
        .I1(U0_n_46),
        .O(\Xmap[6]_i_231_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Xmap[6]_i_232 
       (.I0(U0_n_34),
        .I1(U0_n_47),
        .O(\Xmap[6]_i_232_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Xmap[6]_i_233 
       (.I0(U0_n_35),
        .I1(U0_n_48),
        .O(\Xmap[6]_i_233_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Xmap[6]_i_24 
       (.I0(U0_n_69),
        .I1(U0_n_93),
        .I2(U0_n_81),
        .O(\Xmap[6]_i_24_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Xmap[6]_i_244 
       (.I0(U0_n_29),
        .I1(U0_n_14),
        .O(\Xmap[6]_i_244_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Xmap[6]_i_245 
       (.I0(U0_n_30),
        .I1(U0_n_15),
        .O(\Xmap[6]_i_245_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \Xmap[6]_i_246 
       (.I0(U0_n_31),
        .I1(U0_n_16),
        .O(\Xmap[6]_i_246_n_0 ));
  LUT6 #(
    .INIT(64'h3CC369966996C33C)) 
    \Xmap[6]_i_25 
       (.I0(U0_n_67),
        .I1(U0_n_78),
        .I2(U0_n_90),
        .I3(U0_n_66),
        .I4(U0_n_91),
        .I5(U0_n_79),
        .O(\Xmap[6]_i_25_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Xmap[6]_i_26 
       (.I0(U0_n_80),
        .I1(U0_n_92),
        .I2(U0_n_68),
        .I3(U0_n_79),
        .I4(U0_n_91),
        .I5(U0_n_67),
        .O(\Xmap[6]_i_26_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Xmap[6]_i_27 
       (.I0(U0_n_81),
        .I1(U0_n_93),
        .I2(U0_n_69),
        .I3(U0_n_80),
        .I4(U0_n_92),
        .I5(U0_n_68),
        .O(\Xmap[6]_i_27_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Xmap[6]_i_29 
       (.I0(U0_n_58),
        .I1(U0_n_82),
        .I2(U0_n_70),
        .O(\Xmap[6]_i_29_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Xmap[6]_i_30 
       (.I0(U0_n_59),
        .I1(U0_n_83),
        .I2(U0_n_71),
        .O(\Xmap[6]_i_30_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Xmap[6]_i_31 
       (.I0(U0_n_60),
        .I1(U0_n_84),
        .I2(U0_n_72),
        .O(\Xmap[6]_i_31_n_0 ));
  LUT3 #(
    .INIT(8'hE8)) 
    \Xmap[6]_i_32 
       (.I0(U0_n_61),
        .I1(U0_n_85),
        .I2(U0_n_73),
        .O(\Xmap[6]_i_32_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Xmap[6]_i_33 
       (.I0(U0_n_70),
        .I1(U0_n_82),
        .I2(U0_n_58),
        .I3(U0_n_77),
        .I4(U0_n_89),
        .I5(U0_n_65),
        .O(\Xmap[6]_i_33_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Xmap[6]_i_34 
       (.I0(U0_n_71),
        .I1(U0_n_83),
        .I2(U0_n_59),
        .I3(U0_n_70),
        .I4(U0_n_82),
        .I5(U0_n_58),
        .O(\Xmap[6]_i_34_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Xmap[6]_i_35 
       (.I0(U0_n_72),
        .I1(U0_n_84),
        .I2(U0_n_60),
        .I3(U0_n_71),
        .I4(U0_n_83),
        .I5(U0_n_59),
        .O(\Xmap[6]_i_35_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Xmap[6]_i_36 
       (.I0(U0_n_73),
        .I1(U0_n_85),
        .I2(U0_n_61),
        .I3(U0_n_72),
        .I4(U0_n_84),
        .I5(U0_n_60),
        .O(\Xmap[6]_i_36_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Xmap[6]_i_48 
       (.I0(U0_n_18),
        .I1(U0_n_10),
        .I2(U0_n_6),
        .I3(U0_n_73),
        .I4(U0_n_85),
        .I5(U0_n_61),
        .O(\Xmap[6]_i_48_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Xmap[6]_i_49 
       (.I0(U0_n_19),
        .I1(U0_n_11),
        .I2(U0_n_7),
        .I3(U0_n_18),
        .I4(U0_n_10),
        .I5(U0_n_6),
        .O(\Xmap[6]_i_49_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Xmap[6]_i_50 
       (.I0(U0_n_20),
        .I1(U0_n_12),
        .I2(U0_n_8),
        .I3(U0_n_19),
        .I4(U0_n_11),
        .I5(U0_n_7),
        .O(\Xmap[6]_i_50_n_0 ));
  LUT6 #(
    .INIT(64'h17E8E817E81717E8)) 
    \Xmap[6]_i_51 
       (.I0(U0_n_17),
        .I1(U0_n_13),
        .I2(U0_n_9),
        .I3(U0_n_20),
        .I4(U0_n_12),
        .I5(U0_n_8),
        .O(\Xmap[6]_i_51_n_0 ));
  LUT5 #(
    .INIT(32'h78878778)) 
    \Xmap[6]_i_99 
       (.I0(U0_n_24),
        .I1(U0_n_21),
        .I2(U0_n_17),
        .I3(U0_n_13),
        .I4(U0_n_9),
        .O(\Xmap[6]_i_99_n_0 ));
  CARRY4 \Xmap_reg[6]_i_8 
       (.CI(1'b0),
        .CO({\NLW_Xmap_reg[6]_i_8_CO_UNCONNECTED [3:2],\Xmap_reg[6]_i_8_n_2 ,\Xmap_reg[6]_i_8_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,U0_n_96,1'b0}),
        .O({\NLW_Xmap_reg[6]_i_8_O_UNCONNECTED [3],\Xmap_reg[6]_i_8_n_5 ,\Xmap_reg[6]_i_8_n_6 ,\Xmap_reg[6]_i_8_n_7 }),
        .S({1'b0,\Xmap[6]_i_11_n_0 ,\Xmap[6]_i_12_n_0 ,\Xmap[6]_i_13_n_0 }));
  LUT1 #(
    .INIT(2'h1)) 
    sprites_data_reg_r1_0_63_0_2_i_1
       (.I0(clk),
        .O(sprites_data_reg_r1_0_63_0_2_i_1_n_0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_renderer
   (O34,
    \addr_X_reg[0]_0 ,
    Q,
    tm_reg_0,
    tm_reg_0_0,
    \pixel_bus_reg[2]_0 ,
    \addr_Y_reg[0]_0 ,
    \current_tile_reg[3]_0 ,
    \addr_Y_reg[0]_1 ,
    \current_tile_reg[1]_0 ,
    ADDRBWRADDR,
    tm_reg_0_1,
    pixel_bus,
    \isFinder_reg[0]_0 ,
    \isFinder_reg[1]_0 ,
    clk_0,
    pixel,
    ADDRC,
    \v_cnt_reg[1]_rep ,
    ADDRB,
    ADDRA,
    \v_cnt_reg[1]_rep__1 ,
    render_enable_reg,
    clk,
    SR,
    fetching_sprites_reg,
    v_cnt,
    fetching_sprites,
    render_enable,
    \h_cnt_reg[6] ,
    \current_tile_reg[1]_1 ,
    sw,
    \h_cnt_reg[6]_0 ,
    \h_cnt_reg[7] ,
    \h_cnt_reg[1] ,
    \h_cnt_reg[1]_0 ,
    S,
    \addr_Y_reg[3]_0 ,
    \addr_Y_reg[2]_0 ,
    \addr_Y_reg[3]_1 ,
    pixel_clk,
    render_enable_reg_0,
    D,
    \v_cnt_reg[8] ,
    \v_cnt_reg[9] ,
    tile_id,
    I7,
    lopt);
  output [0:0]O34;
  output \addr_X_reg[0]_0 ;
  output [0:0]Q;
  output [5:0]tm_reg_0;
  output [6:0]tm_reg_0_0;
  output \pixel_bus_reg[2]_0 ;
  output \addr_Y_reg[0]_0 ;
  output [2:0]\current_tile_reg[3]_0 ;
  output \addr_Y_reg[0]_1 ;
  output [1:0]\current_tile_reg[1]_0 ;
  output [11:0]ADDRBWRADDR;
  output [1:0]tm_reg_0_1;
  output [9:0]pixel_bus;
  output \isFinder_reg[0]_0 ;
  output \isFinder_reg[1]_0 ;
  input clk_0;
  input [5:0]pixel;
  input [5:0]ADDRC;
  input [5:0]\v_cnt_reg[1]_rep ;
  input [1:0]ADDRB;
  input [1:0]ADDRA;
  input [1:0]\v_cnt_reg[1]_rep__1 ;
  input render_enable_reg;
  input clk;
  input [0:0]SR;
  input fetching_sprites_reg;
  input [1:0]v_cnt;
  input fetching_sprites;
  input render_enable;
  input \h_cnt_reg[6] ;
  input \current_tile_reg[1]_1 ;
  input [0:0]sw;
  input \h_cnt_reg[6]_0 ;
  input \h_cnt_reg[7] ;
  input \h_cnt_reg[1] ;
  input \h_cnt_reg[1]_0 ;
  input [2:0]S;
  input [0:0]\addr_Y_reg[3]_0 ;
  input [3:0]\addr_Y_reg[2]_0 ;
  input [3:0]\addr_Y_reg[3]_1 ;
  input pixel_clk;
  input render_enable_reg_0;
  input [1:0]D;
  input [0:0]\v_cnt_reg[8] ;
  input [5:0]\v_cnt_reg[9] ;
  input [3:0]tile_id;
  input I7;
  input lopt;

  wire [1:0]ADDRA;
  wire [1:0]ADDRB;
  wire [11:0]ADDRBWRADDR;
  wire [5:0]ADDRC;
  wire [1:0]D;
  wire I7;
  wire [0:0]O34;
  wire [0:0]Q;
  wire [2:0]S;
  wire [0:0]SR;
  wire \addr_X[0]_i_1_n_0 ;
  wire \addr_X[4]_i_2_n_0 ;
  wire \addr_X[5]_i_7_n_0 ;
  wire \addr_X_reg[0]_0 ;
  wire \addr_Y_reg[0]_0 ;
  wire \addr_Y_reg[0]_1 ;
  wire [3:0]\addr_Y_reg[2]_0 ;
  wire [0:0]\addr_Y_reg[3]_0 ;
  wire [3:0]\addr_Y_reg[3]_1 ;
  wire clk;
  wire clk_0;
  wire [5:3]current_tile;
  wire \current_tile[2]_i_1_n_0 ;
  wire \current_tile[3]_i_1_n_0 ;
  wire \current_tile[4]_i_1_n_0 ;
  wire \current_tile[5]_i_1_n_0 ;
  wire \current_tile[5]_i_3_n_0 ;
  wire [5:2]current_tile__0;
  wire [1:0]\current_tile_reg[1]_0 ;
  wire \current_tile_reg[1]_1 ;
  wire [2:0]\current_tile_reg[3]_0 ;
  wire fetching_sprites;
  wire fetching_sprites_reg;
  wire \h_cnt_reg[1] ;
  wire \h_cnt_reg[1]_0 ;
  wire \h_cnt_reg[6] ;
  wire \h_cnt_reg[6]_0 ;
  wire \h_cnt_reg[7] ;
  wire \ind[0]_i_3_n_0 ;
  wire \ind[0]_i_4_n_0 ;
  wire \ind[0]_i_5_n_0 ;
  wire \ind[0]_i_6_n_0 ;
  wire \ind[4]_i_2_n_0 ;
  wire \ind[4]_i_3_n_0 ;
  wire \ind[4]_i_4_n_0 ;
  wire \ind[4]_i_5_n_0 ;
  wire \ind[8]_i_2_n_0 ;
  wire \ind[8]_i_3_n_0 ;
  wire \ind[8]_i_4_n_0 ;
  wire \ind[8]_i_5_n_0 ;
  wire [10:0]ind_reg;
  wire \ind_reg[0]_i_2_n_0 ;
  wire \ind_reg[0]_i_2_n_1 ;
  wire \ind_reg[0]_i_2_n_2 ;
  wire \ind_reg[0]_i_2_n_3 ;
  wire \ind_reg[0]_i_2_n_4 ;
  wire \ind_reg[0]_i_2_n_5 ;
  wire \ind_reg[0]_i_2_n_6 ;
  wire \ind_reg[0]_i_2_n_7 ;
  wire \ind_reg[4]_i_1_n_0 ;
  wire \ind_reg[4]_i_1_n_1 ;
  wire \ind_reg[4]_i_1_n_2 ;
  wire \ind_reg[4]_i_1_n_3 ;
  wire \ind_reg[4]_i_1_n_4 ;
  wire \ind_reg[4]_i_1_n_5 ;
  wire \ind_reg[4]_i_1_n_6 ;
  wire \ind_reg[4]_i_1_n_7 ;
  wire \ind_reg[8]_i_1_n_1 ;
  wire \ind_reg[8]_i_1_n_2 ;
  wire \ind_reg[8]_i_1_n_3 ;
  wire \ind_reg[8]_i_1_n_4 ;
  wire \ind_reg[8]_i_1_n_5 ;
  wire \ind_reg[8]_i_1_n_6 ;
  wire \ind_reg[8]_i_1_n_7 ;
  wire isFinder;
  wire \isFinder[0]_i_1_n_0 ;
  wire \isFinder[1]_i_1_n_0 ;
  wire \isFinder[1]_i_3_n_0 ;
  wire \isFinder_reg[0]_0 ;
  wire \isFinder_reg[1]_0 ;
  wire line_complete_i_1_n_0;
  wire line_complete_i_2_n_0;
  wire line_complete_reg_n_0;
  wire lopt;
  wire [12:12]out_tile2;
  wire p_0_in;
  wire p_0_in12_in;
  wire p_0_in15_in;
  wire p_0_in18_in;
  wire p_0_in1_in;
  wire p_0_in21_in;
  wire p_0_in3_in;
  wire p_0_in6_in;
  wire p_0_in9_in;
  wire p_10_in;
  wire p_1_in;
  wire p_1_in10_in;
  wire p_1_in13_in;
  wire p_1_in19_in;
  wire p_1_in22_in;
  wire p_1_in4_in;
  wire [5:1]p_2_out;
  wire [5:0]pixel;
  wire [9:0]pixel_bus;
  wire \pixel_bus[12]_i_1_n_0 ;
  wire \pixel_bus[12]_i_2_n_0 ;
  wire \pixel_bus[12]_i_3_n_0 ;
  wire \pixel_bus[13]_i_1_n_0 ;
  wire \pixel_bus[13]_i_2_n_0 ;
  wire \pixel_bus[13]_i_3_n_0 ;
  wire \pixel_bus[14]_i_18_n_0 ;
  wire \pixel_bus[14]_i_19_n_0 ;
  wire \pixel_bus[14]_i_1_n_0 ;
  wire \pixel_bus[14]_i_20_n_0 ;
  wire \pixel_bus[14]_i_21_n_0 ;
  wire \pixel_bus[14]_i_22_n_0 ;
  wire \pixel_bus[14]_i_23_n_0 ;
  wire \pixel_bus[14]_i_24_n_0 ;
  wire \pixel_bus[14]_i_25_n_0 ;
  wire \pixel_bus[14]_i_26_n_0 ;
  wire \pixel_bus[14]_i_27_n_0 ;
  wire \pixel_bus[14]_i_28_n_0 ;
  wire \pixel_bus[14]_i_29_n_0 ;
  wire \pixel_bus[14]_i_2_n_0 ;
  wire \pixel_bus[14]_i_30_n_0 ;
  wire \pixel_bus[14]_i_31_n_0 ;
  wire \pixel_bus[14]_i_32_n_0 ;
  wire \pixel_bus[14]_i_33_n_0 ;
  wire \pixel_bus[14]_i_3_n_0 ;
  wire \pixel_bus[14]_i_4_n_0 ;
  wire \pixel_bus[14]_i_5_n_0 ;
  wire \pixel_bus[14]_i_6_n_0 ;
  wire \pixel_bus[14]_i_7_n_0 ;
  wire \pixel_bus[14]_i_8_n_0 ;
  wire \pixel_bus[14]_i_9_n_0 ;
  wire \pixel_bus[15]_i_10_n_0 ;
  wire \pixel_bus[15]_i_13_n_0 ;
  wire \pixel_bus[15]_i_16_n_0 ;
  wire \pixel_bus[15]_i_19_n_0 ;
  wire \pixel_bus[15]_i_2_n_0 ;
  wire \pixel_bus[15]_i_38_n_0 ;
  wire \pixel_bus[15]_i_39_n_0 ;
  wire \pixel_bus[15]_i_40_n_0 ;
  wire \pixel_bus[15]_i_41_n_0 ;
  wire \pixel_bus[15]_i_42_n_0 ;
  wire \pixel_bus[15]_i_43_n_0 ;
  wire \pixel_bus[15]_i_44_n_0 ;
  wire \pixel_bus[15]_i_45_n_0 ;
  wire \pixel_bus[15]_i_46_n_0 ;
  wire \pixel_bus[15]_i_47_n_0 ;
  wire \pixel_bus[15]_i_48_n_0 ;
  wire \pixel_bus[15]_i_49_n_0 ;
  wire \pixel_bus[15]_i_50_n_0 ;
  wire \pixel_bus[15]_i_51_n_0 ;
  wire \pixel_bus[15]_i_52_n_0 ;
  wire \pixel_bus[15]_i_53_n_0 ;
  wire \pixel_bus[15]_i_54_n_0 ;
  wire \pixel_bus[15]_i_55_n_0 ;
  wire \pixel_bus[15]_i_56_n_0 ;
  wire \pixel_bus[15]_i_57_n_0 ;
  wire \pixel_bus[15]_i_58_n_0 ;
  wire \pixel_bus[15]_i_59_n_0 ;
  wire \pixel_bus[15]_i_60_n_0 ;
  wire \pixel_bus[15]_i_61_n_0 ;
  wire \pixel_bus[15]_i_62_n_0 ;
  wire \pixel_bus[15]_i_63_n_0 ;
  wire \pixel_bus[15]_i_64_n_0 ;
  wire \pixel_bus[15]_i_65_n_0 ;
  wire \pixel_bus[15]_i_66_n_0 ;
  wire \pixel_bus[15]_i_67_n_0 ;
  wire \pixel_bus[15]_i_68_n_0 ;
  wire \pixel_bus[15]_i_69_n_0 ;
  wire \pixel_bus[2]_i_1_n_0 ;
  wire \pixel_bus[2]_i_2_n_0 ;
  wire \pixel_bus[2]_i_3_n_0 ;
  wire \pixel_bus[2]_i_4_n_0 ;
  wire \pixel_bus[2]_i_5_n_0 ;
  wire \pixel_bus[2]_i_6_n_0 ;
  wire \pixel_bus[2]_i_7_n_0 ;
  wire \pixel_bus[3]_i_1_n_0 ;
  wire \pixel_bus[3]_i_2_n_0 ;
  wire \pixel_bus[3]_i_3_n_0 ;
  wire \pixel_bus[4]_i_100_n_0 ;
  wire \pixel_bus[4]_i_101_n_0 ;
  wire \pixel_bus[4]_i_102_n_0 ;
  wire \pixel_bus[4]_i_103_n_0 ;
  wire \pixel_bus[4]_i_14_n_0 ;
  wire \pixel_bus[4]_i_16_n_0 ;
  wire \pixel_bus[4]_i_17_n_0 ;
  wire \pixel_bus[4]_i_18_n_0 ;
  wire \pixel_bus[4]_i_19_n_0 ;
  wire \pixel_bus[4]_i_1_n_0 ;
  wire \pixel_bus[4]_i_20_n_0 ;
  wire \pixel_bus[4]_i_21_n_0 ;
  wire \pixel_bus[4]_i_22_n_0 ;
  wire \pixel_bus[4]_i_23_n_0 ;
  wire \pixel_bus[4]_i_24_n_0 ;
  wire \pixel_bus[4]_i_25_n_0 ;
  wire \pixel_bus[4]_i_26_n_0 ;
  wire \pixel_bus[4]_i_27_n_0 ;
  wire \pixel_bus[4]_i_2_n_0 ;
  wire \pixel_bus[4]_i_3_n_0 ;
  wire \pixel_bus[4]_i_56_n_0 ;
  wire \pixel_bus[4]_i_57_n_0 ;
  wire \pixel_bus[4]_i_58_n_0 ;
  wire \pixel_bus[4]_i_59_n_0 ;
  wire \pixel_bus[4]_i_5_n_0 ;
  wire \pixel_bus[4]_i_60_n_0 ;
  wire \pixel_bus[4]_i_61_n_0 ;
  wire \pixel_bus[4]_i_62_n_0 ;
  wire \pixel_bus[4]_i_63_n_0 ;
  wire \pixel_bus[4]_i_64_n_0 ;
  wire \pixel_bus[4]_i_65_n_0 ;
  wire \pixel_bus[4]_i_66_n_0 ;
  wire \pixel_bus[4]_i_67_n_0 ;
  wire \pixel_bus[4]_i_68_n_0 ;
  wire \pixel_bus[4]_i_69_n_0 ;
  wire \pixel_bus[4]_i_6_n_0 ;
  wire \pixel_bus[4]_i_70_n_0 ;
  wire \pixel_bus[4]_i_71_n_0 ;
  wire \pixel_bus[4]_i_72_n_0 ;
  wire \pixel_bus[4]_i_73_n_0 ;
  wire \pixel_bus[4]_i_74_n_0 ;
  wire \pixel_bus[4]_i_75_n_0 ;
  wire \pixel_bus[4]_i_76_n_0 ;
  wire \pixel_bus[4]_i_77_n_0 ;
  wire \pixel_bus[4]_i_78_n_0 ;
  wire \pixel_bus[4]_i_79_n_0 ;
  wire \pixel_bus[4]_i_80_n_0 ;
  wire \pixel_bus[4]_i_81_n_0 ;
  wire \pixel_bus[4]_i_82_n_0 ;
  wire \pixel_bus[4]_i_83_n_0 ;
  wire \pixel_bus[4]_i_84_n_0 ;
  wire \pixel_bus[4]_i_85_n_0 ;
  wire \pixel_bus[4]_i_86_n_0 ;
  wire \pixel_bus[4]_i_87_n_0 ;
  wire \pixel_bus[4]_i_88_n_0 ;
  wire \pixel_bus[4]_i_89_n_0 ;
  wire \pixel_bus[4]_i_90_n_0 ;
  wire \pixel_bus[4]_i_91_n_0 ;
  wire \pixel_bus[4]_i_92_n_0 ;
  wire \pixel_bus[4]_i_93_n_0 ;
  wire \pixel_bus[4]_i_94_n_0 ;
  wire \pixel_bus[4]_i_95_n_0 ;
  wire \pixel_bus[4]_i_96_n_0 ;
  wire \pixel_bus[4]_i_97_n_0 ;
  wire \pixel_bus[4]_i_98_n_0 ;
  wire \pixel_bus[4]_i_99_n_0 ;
  wire \pixel_bus[7]_i_1_n_0 ;
  wire \pixel_bus[7]_i_2_n_0 ;
  wire \pixel_bus[8]_i_1_n_0 ;
  wire \pixel_bus[9]_i_1_n_0 ;
  wire \pixel_bus[9]_i_3_n_0 ;
  wire \pixel_bus_reg[14]_i_10_n_0 ;
  wire \pixel_bus_reg[14]_i_11_n_0 ;
  wire \pixel_bus_reg[14]_i_12_n_0 ;
  wire \pixel_bus_reg[14]_i_13_n_0 ;
  wire \pixel_bus_reg[14]_i_14_n_0 ;
  wire \pixel_bus_reg[14]_i_15_n_0 ;
  wire \pixel_bus_reg[14]_i_16_n_0 ;
  wire \pixel_bus_reg[14]_i_17_n_0 ;
  wire \pixel_bus_reg[15]_i_11_n_0 ;
  wire \pixel_bus_reg[15]_i_12_n_0 ;
  wire \pixel_bus_reg[15]_i_14_n_0 ;
  wire \pixel_bus_reg[15]_i_15_n_0 ;
  wire \pixel_bus_reg[15]_i_17_n_0 ;
  wire \pixel_bus_reg[15]_i_18_n_0 ;
  wire \pixel_bus_reg[15]_i_20_n_0 ;
  wire \pixel_bus_reg[15]_i_21_n_0 ;
  wire \pixel_bus_reg[15]_i_22_n_0 ;
  wire \pixel_bus_reg[15]_i_23_n_0 ;
  wire \pixel_bus_reg[15]_i_24_n_0 ;
  wire \pixel_bus_reg[15]_i_25_n_0 ;
  wire \pixel_bus_reg[15]_i_26_n_0 ;
  wire \pixel_bus_reg[15]_i_27_n_0 ;
  wire \pixel_bus_reg[15]_i_28_n_0 ;
  wire \pixel_bus_reg[15]_i_29_n_0 ;
  wire \pixel_bus_reg[15]_i_30_n_0 ;
  wire \pixel_bus_reg[15]_i_31_n_0 ;
  wire \pixel_bus_reg[15]_i_32_n_0 ;
  wire \pixel_bus_reg[15]_i_33_n_0 ;
  wire \pixel_bus_reg[15]_i_34_n_0 ;
  wire \pixel_bus_reg[15]_i_35_n_0 ;
  wire \pixel_bus_reg[15]_i_36_n_0 ;
  wire \pixel_bus_reg[15]_i_37_n_0 ;
  wire \pixel_bus_reg[15]_i_3_n_0 ;
  wire \pixel_bus_reg[15]_i_4_n_0 ;
  wire \pixel_bus_reg[2]_0 ;
  wire \pixel_bus_reg[4]_i_10_n_0 ;
  wire \pixel_bus_reg[4]_i_12_n_0 ;
  wire \pixel_bus_reg[4]_i_30_n_0 ;
  wire \pixel_bus_reg[4]_i_31_n_0 ;
  wire \pixel_bus_reg[4]_i_32_n_0 ;
  wire \pixel_bus_reg[4]_i_33_n_0 ;
  wire \pixel_bus_reg[4]_i_34_n_0 ;
  wire \pixel_bus_reg[4]_i_35_n_0 ;
  wire \pixel_bus_reg[4]_i_36_n_0 ;
  wire \pixel_bus_reg[4]_i_37_n_0 ;
  wire \pixel_bus_reg[4]_i_38_n_0 ;
  wire \pixel_bus_reg[4]_i_39_n_0 ;
  wire \pixel_bus_reg[4]_i_40_n_0 ;
  wire \pixel_bus_reg[4]_i_41_n_0 ;
  wire \pixel_bus_reg[4]_i_42_n_0 ;
  wire \pixel_bus_reg[4]_i_43_n_0 ;
  wire \pixel_bus_reg[4]_i_44_n_0 ;
  wire \pixel_bus_reg[4]_i_45_n_0 ;
  wire \pixel_bus_reg[4]_i_46_n_0 ;
  wire \pixel_bus_reg[4]_i_47_n_0 ;
  wire \pixel_bus_reg[4]_i_48_n_0 ;
  wire \pixel_bus_reg[4]_i_49_n_0 ;
  wire \pixel_bus_reg[4]_i_50_n_0 ;
  wire \pixel_bus_reg[4]_i_51_n_0 ;
  wire \pixel_bus_reg[4]_i_52_n_0 ;
  wire \pixel_bus_reg[4]_i_53_n_0 ;
  wire pixel_clk;
  wire render_enable;
  wire render_enable_reg;
  wire render_enable_reg_0;
  wire [3:0]sprite_x_rev;
  wire \sprite_x_rev_reg_n_0_[0] ;
  wire \sprite_x_rev_reg_n_0_[1] ;
  wire \sprite_x_rev_reg_n_0_[2] ;
  wire \sprite_x_rev_reg_n_0_[3] ;
  wire sprites_data_reg_r1_0_63_0_2_i_2_n_0;
  wire sprites_data_reg_r1_0_63_0_2_i_3_n_0;
  wire sprites_data_reg_r1_0_63_0_2_n_0;
  wire sprites_data_reg_r1_0_63_0_2_n_1;
  wire sprites_data_reg_r1_0_63_0_2_n_2;
  wire sprites_data_reg_r1_0_63_3_5_n_0;
  wire sprites_data_reg_r1_0_63_3_5_n_1;
  wire sprites_data_reg_r1_0_63_3_5_n_2;
  wire sprites_data_reg_r1_128_191_0_2_i_1_n_0;
  wire sprites_data_reg_r1_128_191_0_2_i_2_n_0;
  wire sprites_data_reg_r1_128_191_0_2_n_0;
  wire sprites_data_reg_r1_128_191_0_2_n_1;
  wire sprites_data_reg_r1_128_191_0_2_n_2;
  wire sprites_data_reg_r1_128_191_3_5_n_0;
  wire sprites_data_reg_r1_128_191_3_5_n_1;
  wire sprites_data_reg_r1_128_191_3_5_n_2;
  wire sprites_data_reg_r1_192_255_0_2_i_1_n_0;
  wire sprites_data_reg_r1_192_255_0_2_i_2_n_0;
  wire sprites_data_reg_r1_192_255_0_2_n_0;
  wire sprites_data_reg_r1_192_255_0_2_n_1;
  wire sprites_data_reg_r1_192_255_0_2_n_2;
  wire sprites_data_reg_r1_192_255_3_5_n_0;
  wire sprites_data_reg_r1_192_255_3_5_n_1;
  wire sprites_data_reg_r1_192_255_3_5_n_2;
  wire sprites_data_reg_r1_64_127_0_2_i_1_n_0;
  wire sprites_data_reg_r1_64_127_0_2_i_2_n_0;
  wire sprites_data_reg_r1_64_127_0_2_n_0;
  wire sprites_data_reg_r1_64_127_0_2_n_1;
  wire sprites_data_reg_r1_64_127_0_2_n_2;
  wire sprites_data_reg_r1_64_127_3_5_n_0;
  wire sprites_data_reg_r1_64_127_3_5_n_1;
  wire sprites_data_reg_r1_64_127_3_5_n_2;
  wire sprites_data_reg_r2_0_63_0_2_n_0;
  wire sprites_data_reg_r2_0_63_0_2_n_1;
  wire sprites_data_reg_r2_0_63_0_2_n_2;
  wire sprites_data_reg_r2_0_63_3_5_n_0;
  wire sprites_data_reg_r2_0_63_3_5_n_1;
  wire sprites_data_reg_r2_0_63_3_5_n_2;
  wire sprites_data_reg_r2_1024_1087_0_2_i_1_n_0;
  wire sprites_data_reg_r2_1024_1087_0_2_n_0;
  wire sprites_data_reg_r2_1024_1087_0_2_n_1;
  wire sprites_data_reg_r2_1024_1087_0_2_n_2;
  wire sprites_data_reg_r2_1024_1087_3_5_n_0;
  wire sprites_data_reg_r2_1024_1087_3_5_n_1;
  wire sprites_data_reg_r2_1024_1087_3_5_n_2;
  wire sprites_data_reg_r2_1088_1151_0_2_i_1_n_0;
  wire sprites_data_reg_r2_1088_1151_0_2_n_0;
  wire sprites_data_reg_r2_1088_1151_0_2_n_1;
  wire sprites_data_reg_r2_1088_1151_0_2_n_2;
  wire sprites_data_reg_r2_1088_1151_3_5_n_0;
  wire sprites_data_reg_r2_1088_1151_3_5_n_1;
  wire sprites_data_reg_r2_1088_1151_3_5_n_2;
  wire sprites_data_reg_r2_1152_1215_0_2_i_1_n_0;
  wire sprites_data_reg_r2_1152_1215_0_2_n_0;
  wire sprites_data_reg_r2_1152_1215_0_2_n_1;
  wire sprites_data_reg_r2_1152_1215_0_2_n_2;
  wire sprites_data_reg_r2_1152_1215_3_5_n_0;
  wire sprites_data_reg_r2_1152_1215_3_5_n_1;
  wire sprites_data_reg_r2_1152_1215_3_5_n_2;
  wire sprites_data_reg_r2_1216_1279_0_2_i_1_n_0;
  wire sprites_data_reg_r2_1216_1279_0_2_i_2_n_0;
  wire sprites_data_reg_r2_1216_1279_0_2_n_0;
  wire sprites_data_reg_r2_1216_1279_0_2_n_1;
  wire sprites_data_reg_r2_1216_1279_0_2_n_2;
  wire sprites_data_reg_r2_1216_1279_3_5_n_0;
  wire sprites_data_reg_r2_1216_1279_3_5_n_1;
  wire sprites_data_reg_r2_1216_1279_3_5_n_2;
  wire sprites_data_reg_r2_1280_1343_0_2_i_1_n_0;
  wire sprites_data_reg_r2_1280_1343_0_2_n_0;
  wire sprites_data_reg_r2_1280_1343_0_2_n_1;
  wire sprites_data_reg_r2_1280_1343_0_2_n_2;
  wire sprites_data_reg_r2_1280_1343_3_5_n_0;
  wire sprites_data_reg_r2_1280_1343_3_5_n_1;
  wire sprites_data_reg_r2_1280_1343_3_5_n_2;
  wire sprites_data_reg_r2_128_191_0_2_n_0;
  wire sprites_data_reg_r2_128_191_0_2_n_1;
  wire sprites_data_reg_r2_128_191_0_2_n_2;
  wire sprites_data_reg_r2_128_191_3_5_n_0;
  wire sprites_data_reg_r2_128_191_3_5_n_1;
  wire sprites_data_reg_r2_128_191_3_5_n_2;
  wire sprites_data_reg_r2_1344_1407_0_2_i_1_n_0;
  wire sprites_data_reg_r2_1344_1407_0_2_n_0;
  wire sprites_data_reg_r2_1344_1407_0_2_n_1;
  wire sprites_data_reg_r2_1344_1407_0_2_n_2;
  wire sprites_data_reg_r2_1344_1407_3_5_n_0;
  wire sprites_data_reg_r2_1344_1407_3_5_n_1;
  wire sprites_data_reg_r2_1344_1407_3_5_n_2;
  wire sprites_data_reg_r2_1408_1471_0_2_i_1_n_0;
  wire sprites_data_reg_r2_1408_1471_0_2_n_0;
  wire sprites_data_reg_r2_1408_1471_0_2_n_1;
  wire sprites_data_reg_r2_1408_1471_0_2_n_2;
  wire sprites_data_reg_r2_1408_1471_3_5_n_0;
  wire sprites_data_reg_r2_1408_1471_3_5_n_1;
  wire sprites_data_reg_r2_1408_1471_3_5_n_2;
  wire sprites_data_reg_r2_1472_1535_0_2_i_1_n_0;
  wire sprites_data_reg_r2_1472_1535_0_2_n_0;
  wire sprites_data_reg_r2_1472_1535_0_2_n_1;
  wire sprites_data_reg_r2_1472_1535_0_2_n_2;
  wire sprites_data_reg_r2_1472_1535_3_5_n_0;
  wire sprites_data_reg_r2_1472_1535_3_5_n_1;
  wire sprites_data_reg_r2_1472_1535_3_5_n_2;
  wire sprites_data_reg_r2_1536_1599_0_2_i_1_n_0;
  wire sprites_data_reg_r2_1536_1599_0_2_n_0;
  wire sprites_data_reg_r2_1536_1599_0_2_n_1;
  wire sprites_data_reg_r2_1536_1599_0_2_n_2;
  wire sprites_data_reg_r2_1536_1599_3_5_n_0;
  wire sprites_data_reg_r2_1536_1599_3_5_n_1;
  wire sprites_data_reg_r2_1536_1599_3_5_n_2;
  wire sprites_data_reg_r2_1600_1663_0_2_i_1_n_0;
  wire sprites_data_reg_r2_1600_1663_0_2_n_0;
  wire sprites_data_reg_r2_1600_1663_0_2_n_1;
  wire sprites_data_reg_r2_1600_1663_0_2_n_2;
  wire sprites_data_reg_r2_1600_1663_3_5_n_0;
  wire sprites_data_reg_r2_1600_1663_3_5_n_1;
  wire sprites_data_reg_r2_1600_1663_3_5_n_2;
  wire sprites_data_reg_r2_1664_1727_0_2_i_1_n_0;
  wire sprites_data_reg_r2_1664_1727_0_2_n_0;
  wire sprites_data_reg_r2_1664_1727_0_2_n_1;
  wire sprites_data_reg_r2_1664_1727_0_2_n_2;
  wire sprites_data_reg_r2_1664_1727_3_5_n_0;
  wire sprites_data_reg_r2_1664_1727_3_5_n_1;
  wire sprites_data_reg_r2_1664_1727_3_5_n_2;
  wire sprites_data_reg_r2_1728_1791_0_2_i_1_n_0;
  wire sprites_data_reg_r2_1728_1791_0_2_n_0;
  wire sprites_data_reg_r2_1728_1791_0_2_n_1;
  wire sprites_data_reg_r2_1728_1791_0_2_n_2;
  wire sprites_data_reg_r2_1728_1791_3_5_n_0;
  wire sprites_data_reg_r2_1728_1791_3_5_n_1;
  wire sprites_data_reg_r2_1728_1791_3_5_n_2;
  wire sprites_data_reg_r2_1792_1855_0_2_i_1_n_0;
  wire sprites_data_reg_r2_1792_1855_0_2_n_0;
  wire sprites_data_reg_r2_1792_1855_0_2_n_1;
  wire sprites_data_reg_r2_1792_1855_0_2_n_2;
  wire sprites_data_reg_r2_1792_1855_3_5_n_0;
  wire sprites_data_reg_r2_1792_1855_3_5_n_1;
  wire sprites_data_reg_r2_1792_1855_3_5_n_2;
  wire sprites_data_reg_r2_1856_1919_0_2_i_1_n_0;
  wire sprites_data_reg_r2_1856_1919_0_2_i_2_n_0;
  wire sprites_data_reg_r2_1856_1919_0_2_n_0;
  wire sprites_data_reg_r2_1856_1919_0_2_n_1;
  wire sprites_data_reg_r2_1856_1919_0_2_n_2;
  wire sprites_data_reg_r2_1856_1919_3_5_n_0;
  wire sprites_data_reg_r2_1856_1919_3_5_n_1;
  wire sprites_data_reg_r2_1856_1919_3_5_n_2;
  wire sprites_data_reg_r2_1920_1983_0_2_i_1_n_0;
  wire sprites_data_reg_r2_1920_1983_0_2_i_2_n_0;
  wire sprites_data_reg_r2_1920_1983_0_2_n_0;
  wire sprites_data_reg_r2_1920_1983_0_2_n_1;
  wire sprites_data_reg_r2_1920_1983_0_2_n_2;
  wire sprites_data_reg_r2_1920_1983_3_5_n_0;
  wire sprites_data_reg_r2_1920_1983_3_5_n_1;
  wire sprites_data_reg_r2_1920_1983_3_5_n_2;
  wire sprites_data_reg_r2_192_255_0_2_n_0;
  wire sprites_data_reg_r2_192_255_0_2_n_1;
  wire sprites_data_reg_r2_192_255_0_2_n_2;
  wire sprites_data_reg_r2_192_255_3_5_n_0;
  wire sprites_data_reg_r2_192_255_3_5_n_1;
  wire sprites_data_reg_r2_192_255_3_5_n_2;
  wire sprites_data_reg_r2_1984_2047_0_2_i_1_n_0;
  wire sprites_data_reg_r2_1984_2047_0_2_n_0;
  wire sprites_data_reg_r2_1984_2047_0_2_n_1;
  wire sprites_data_reg_r2_1984_2047_0_2_n_2;
  wire sprites_data_reg_r2_1984_2047_3_5_n_0;
  wire sprites_data_reg_r2_1984_2047_3_5_n_1;
  wire sprites_data_reg_r2_1984_2047_3_5_n_2;
  wire sprites_data_reg_r2_2048_2111_0_2_n_0;
  wire sprites_data_reg_r2_2048_2111_0_2_n_1;
  wire sprites_data_reg_r2_2048_2111_0_2_n_2;
  wire sprites_data_reg_r2_2048_2111_3_5_n_0;
  wire sprites_data_reg_r2_2048_2111_3_5_n_1;
  wire sprites_data_reg_r2_2048_2111_3_5_n_2;
  wire sprites_data_reg_r2_256_319_0_2_i_1_n_0;
  wire sprites_data_reg_r2_256_319_0_2_n_0;
  wire sprites_data_reg_r2_256_319_0_2_n_1;
  wire sprites_data_reg_r2_256_319_0_2_n_2;
  wire sprites_data_reg_r2_256_319_3_5_n_0;
  wire sprites_data_reg_r2_256_319_3_5_n_1;
  wire sprites_data_reg_r2_256_319_3_5_n_2;
  wire sprites_data_reg_r2_320_383_0_2_i_1_n_0;
  wire sprites_data_reg_r2_320_383_0_2_n_0;
  wire sprites_data_reg_r2_320_383_0_2_n_1;
  wire sprites_data_reg_r2_320_383_0_2_n_2;
  wire sprites_data_reg_r2_320_383_3_5_n_0;
  wire sprites_data_reg_r2_320_383_3_5_n_1;
  wire sprites_data_reg_r2_320_383_3_5_n_2;
  wire sprites_data_reg_r2_384_447_0_2_i_1_n_0;
  wire sprites_data_reg_r2_384_447_0_2_n_0;
  wire sprites_data_reg_r2_384_447_0_2_n_1;
  wire sprites_data_reg_r2_384_447_0_2_n_2;
  wire sprites_data_reg_r2_384_447_3_5_n_0;
  wire sprites_data_reg_r2_384_447_3_5_n_1;
  wire sprites_data_reg_r2_384_447_3_5_n_2;
  wire sprites_data_reg_r2_448_511_0_2_i_1_n_0;
  wire sprites_data_reg_r2_448_511_0_2_i_2_n_0;
  wire sprites_data_reg_r2_448_511_0_2_n_0;
  wire sprites_data_reg_r2_448_511_0_2_n_1;
  wire sprites_data_reg_r2_448_511_0_2_n_2;
  wire sprites_data_reg_r2_448_511_3_5_n_0;
  wire sprites_data_reg_r2_448_511_3_5_n_1;
  wire sprites_data_reg_r2_448_511_3_5_n_2;
  wire sprites_data_reg_r2_512_575_0_2_i_1_n_0;
  wire sprites_data_reg_r2_512_575_0_2_n_0;
  wire sprites_data_reg_r2_512_575_0_2_n_1;
  wire sprites_data_reg_r2_512_575_0_2_n_2;
  wire sprites_data_reg_r2_512_575_3_5_n_0;
  wire sprites_data_reg_r2_512_575_3_5_n_1;
  wire sprites_data_reg_r2_512_575_3_5_n_2;
  wire sprites_data_reg_r2_576_639_0_2_i_1_n_0;
  wire sprites_data_reg_r2_576_639_0_2_n_0;
  wire sprites_data_reg_r2_576_639_0_2_n_1;
  wire sprites_data_reg_r2_576_639_0_2_n_2;
  wire sprites_data_reg_r2_576_639_3_5_n_0;
  wire sprites_data_reg_r2_576_639_3_5_n_1;
  wire sprites_data_reg_r2_576_639_3_5_n_2;
  wire sprites_data_reg_r2_640_703_0_2_i_1_n_0;
  wire sprites_data_reg_r2_640_703_0_2_n_0;
  wire sprites_data_reg_r2_640_703_0_2_n_1;
  wire sprites_data_reg_r2_640_703_0_2_n_2;
  wire sprites_data_reg_r2_640_703_3_5_n_0;
  wire sprites_data_reg_r2_640_703_3_5_n_1;
  wire sprites_data_reg_r2_640_703_3_5_n_2;
  wire sprites_data_reg_r2_64_127_0_2_n_0;
  wire sprites_data_reg_r2_64_127_0_2_n_1;
  wire sprites_data_reg_r2_64_127_0_2_n_2;
  wire sprites_data_reg_r2_64_127_3_5_n_0;
  wire sprites_data_reg_r2_64_127_3_5_n_1;
  wire sprites_data_reg_r2_64_127_3_5_n_2;
  wire sprites_data_reg_r2_704_767_0_2_i_1_n_0;
  wire sprites_data_reg_r2_704_767_0_2_n_0;
  wire sprites_data_reg_r2_704_767_0_2_n_1;
  wire sprites_data_reg_r2_704_767_0_2_n_2;
  wire sprites_data_reg_r2_704_767_3_5_n_0;
  wire sprites_data_reg_r2_704_767_3_5_n_1;
  wire sprites_data_reg_r2_704_767_3_5_n_2;
  wire sprites_data_reg_r2_768_831_0_2_i_1_n_0;
  wire sprites_data_reg_r2_768_831_0_2_n_0;
  wire sprites_data_reg_r2_768_831_0_2_n_1;
  wire sprites_data_reg_r2_768_831_0_2_n_2;
  wire sprites_data_reg_r2_768_831_3_5_n_0;
  wire sprites_data_reg_r2_768_831_3_5_n_1;
  wire sprites_data_reg_r2_768_831_3_5_n_2;
  wire sprites_data_reg_r2_832_895_0_2_i_1_n_0;
  wire sprites_data_reg_r2_832_895_0_2_i_2_n_0;
  wire sprites_data_reg_r2_832_895_0_2_n_0;
  wire sprites_data_reg_r2_832_895_0_2_n_1;
  wire sprites_data_reg_r2_832_895_0_2_n_2;
  wire sprites_data_reg_r2_832_895_3_5_n_0;
  wire sprites_data_reg_r2_832_895_3_5_n_1;
  wire sprites_data_reg_r2_832_895_3_5_n_2;
  wire sprites_data_reg_r2_896_959_0_2_i_1_n_0;
  wire sprites_data_reg_r2_896_959_0_2_n_0;
  wire sprites_data_reg_r2_896_959_0_2_n_1;
  wire sprites_data_reg_r2_896_959_0_2_n_2;
  wire sprites_data_reg_r2_896_959_3_5_n_0;
  wire sprites_data_reg_r2_896_959_3_5_n_1;
  wire sprites_data_reg_r2_896_959_3_5_n_2;
  wire sprites_data_reg_r2_960_1023_0_2_i_1_n_0;
  wire sprites_data_reg_r2_960_1023_0_2_n_0;
  wire sprites_data_reg_r2_960_1023_0_2_n_1;
  wire sprites_data_reg_r2_960_1023_0_2_n_2;
  wire sprites_data_reg_r2_960_1023_3_5_n_0;
  wire sprites_data_reg_r2_960_1023_3_5_n_1;
  wire sprites_data_reg_r2_960_1023_3_5_n_2;
  wire sprites_data_reg_r3_0_63_0_2_n_0;
  wire sprites_data_reg_r3_0_63_0_2_n_1;
  wire sprites_data_reg_r3_0_63_0_2_n_2;
  wire sprites_data_reg_r3_0_63_3_5_i_1_n_0;
  wire sprites_data_reg_r3_0_63_3_5_i_2_n_0;
  wire sprites_data_reg_r3_0_63_3_5_i_3_n_0;
  wire sprites_data_reg_r3_0_63_3_5_i_4_n_0;
  wire sprites_data_reg_r3_0_63_3_5_n_0;
  wire sprites_data_reg_r3_0_63_3_5_n_1;
  wire sprites_data_reg_r3_0_63_3_5_n_2;
  wire sprites_data_reg_r3_1024_1087_0_2_n_0;
  wire sprites_data_reg_r3_1024_1087_0_2_n_1;
  wire sprites_data_reg_r3_1024_1087_0_2_n_2;
  wire sprites_data_reg_r3_1024_1087_3_5_n_0;
  wire sprites_data_reg_r3_1024_1087_3_5_n_1;
  wire sprites_data_reg_r3_1024_1087_3_5_n_2;
  wire sprites_data_reg_r3_1088_1151_0_2_n_0;
  wire sprites_data_reg_r3_1088_1151_0_2_n_1;
  wire sprites_data_reg_r3_1088_1151_0_2_n_2;
  wire sprites_data_reg_r3_1088_1151_3_5_n_0;
  wire sprites_data_reg_r3_1088_1151_3_5_n_1;
  wire sprites_data_reg_r3_1088_1151_3_5_n_2;
  wire sprites_data_reg_r3_1152_1215_0_2_n_0;
  wire sprites_data_reg_r3_1152_1215_0_2_n_1;
  wire sprites_data_reg_r3_1152_1215_0_2_n_2;
  wire sprites_data_reg_r3_1152_1215_3_5_n_0;
  wire sprites_data_reg_r3_1152_1215_3_5_n_1;
  wire sprites_data_reg_r3_1152_1215_3_5_n_2;
  wire sprites_data_reg_r3_1216_1279_0_2_n_0;
  wire sprites_data_reg_r3_1216_1279_0_2_n_1;
  wire sprites_data_reg_r3_1216_1279_0_2_n_2;
  wire sprites_data_reg_r3_1216_1279_3_5_n_0;
  wire sprites_data_reg_r3_1216_1279_3_5_n_1;
  wire sprites_data_reg_r3_1216_1279_3_5_n_2;
  wire sprites_data_reg_r3_1280_1343_0_2_n_0;
  wire sprites_data_reg_r3_1280_1343_0_2_n_1;
  wire sprites_data_reg_r3_1280_1343_0_2_n_2;
  wire sprites_data_reg_r3_1280_1343_3_5_n_0;
  wire sprites_data_reg_r3_1280_1343_3_5_n_1;
  wire sprites_data_reg_r3_1280_1343_3_5_n_2;
  wire sprites_data_reg_r3_128_191_0_2_n_0;
  wire sprites_data_reg_r3_128_191_0_2_n_1;
  wire sprites_data_reg_r3_128_191_0_2_n_2;
  wire sprites_data_reg_r3_128_191_3_5_n_0;
  wire sprites_data_reg_r3_128_191_3_5_n_1;
  wire sprites_data_reg_r3_128_191_3_5_n_2;
  wire sprites_data_reg_r3_1344_1407_0_2_n_0;
  wire sprites_data_reg_r3_1344_1407_0_2_n_1;
  wire sprites_data_reg_r3_1344_1407_0_2_n_2;
  wire sprites_data_reg_r3_1344_1407_3_5_n_0;
  wire sprites_data_reg_r3_1344_1407_3_5_n_1;
  wire sprites_data_reg_r3_1344_1407_3_5_n_2;
  wire sprites_data_reg_r3_1408_1471_0_2_n_0;
  wire sprites_data_reg_r3_1408_1471_0_2_n_1;
  wire sprites_data_reg_r3_1408_1471_0_2_n_2;
  wire sprites_data_reg_r3_1408_1471_3_5_n_0;
  wire sprites_data_reg_r3_1408_1471_3_5_n_1;
  wire sprites_data_reg_r3_1408_1471_3_5_n_2;
  wire sprites_data_reg_r3_1472_1535_0_2_n_0;
  wire sprites_data_reg_r3_1472_1535_0_2_n_1;
  wire sprites_data_reg_r3_1472_1535_0_2_n_2;
  wire sprites_data_reg_r3_1472_1535_3_5_n_0;
  wire sprites_data_reg_r3_1472_1535_3_5_n_1;
  wire sprites_data_reg_r3_1472_1535_3_5_n_2;
  wire sprites_data_reg_r3_1536_1599_0_2_n_0;
  wire sprites_data_reg_r3_1536_1599_0_2_n_1;
  wire sprites_data_reg_r3_1536_1599_0_2_n_2;
  wire sprites_data_reg_r3_1536_1599_3_5_n_0;
  wire sprites_data_reg_r3_1536_1599_3_5_n_1;
  wire sprites_data_reg_r3_1536_1599_3_5_n_2;
  wire sprites_data_reg_r3_1600_1663_0_2_n_0;
  wire sprites_data_reg_r3_1600_1663_0_2_n_1;
  wire sprites_data_reg_r3_1600_1663_0_2_n_2;
  wire sprites_data_reg_r3_1600_1663_3_5_n_0;
  wire sprites_data_reg_r3_1600_1663_3_5_n_1;
  wire sprites_data_reg_r3_1600_1663_3_5_n_2;
  wire sprites_data_reg_r3_1664_1727_0_2_n_0;
  wire sprites_data_reg_r3_1664_1727_0_2_n_1;
  wire sprites_data_reg_r3_1664_1727_0_2_n_2;
  wire sprites_data_reg_r3_1664_1727_3_5_n_0;
  wire sprites_data_reg_r3_1664_1727_3_5_n_1;
  wire sprites_data_reg_r3_1664_1727_3_5_n_2;
  wire sprites_data_reg_r3_1728_1791_0_2_n_0;
  wire sprites_data_reg_r3_1728_1791_0_2_n_1;
  wire sprites_data_reg_r3_1728_1791_0_2_n_2;
  wire sprites_data_reg_r3_1728_1791_3_5_n_0;
  wire sprites_data_reg_r3_1728_1791_3_5_n_1;
  wire sprites_data_reg_r3_1728_1791_3_5_n_2;
  wire sprites_data_reg_r3_1792_1855_0_2_n_0;
  wire sprites_data_reg_r3_1792_1855_0_2_n_1;
  wire sprites_data_reg_r3_1792_1855_0_2_n_2;
  wire sprites_data_reg_r3_1792_1855_3_5_n_0;
  wire sprites_data_reg_r3_1792_1855_3_5_n_1;
  wire sprites_data_reg_r3_1792_1855_3_5_n_2;
  wire sprites_data_reg_r3_1856_1919_0_2_n_0;
  wire sprites_data_reg_r3_1856_1919_0_2_n_1;
  wire sprites_data_reg_r3_1856_1919_0_2_n_2;
  wire sprites_data_reg_r3_1856_1919_3_5_n_0;
  wire sprites_data_reg_r3_1856_1919_3_5_n_1;
  wire sprites_data_reg_r3_1856_1919_3_5_n_2;
  wire sprites_data_reg_r3_1920_1983_0_2_n_0;
  wire sprites_data_reg_r3_1920_1983_0_2_n_1;
  wire sprites_data_reg_r3_1920_1983_0_2_n_2;
  wire sprites_data_reg_r3_1920_1983_3_5_n_0;
  wire sprites_data_reg_r3_1920_1983_3_5_n_1;
  wire sprites_data_reg_r3_1920_1983_3_5_n_2;
  wire sprites_data_reg_r3_192_255_0_2_n_0;
  wire sprites_data_reg_r3_192_255_0_2_n_1;
  wire sprites_data_reg_r3_192_255_0_2_n_2;
  wire sprites_data_reg_r3_192_255_3_5_n_0;
  wire sprites_data_reg_r3_192_255_3_5_n_1;
  wire sprites_data_reg_r3_192_255_3_5_n_2;
  wire sprites_data_reg_r3_1984_2047_0_2_n_0;
  wire sprites_data_reg_r3_1984_2047_0_2_n_1;
  wire sprites_data_reg_r3_1984_2047_0_2_n_2;
  wire sprites_data_reg_r3_1984_2047_3_5_n_0;
  wire sprites_data_reg_r3_1984_2047_3_5_n_1;
  wire sprites_data_reg_r3_1984_2047_3_5_n_2;
  wire sprites_data_reg_r3_2048_2111_0_2_i_1_n_0;
  wire sprites_data_reg_r3_2048_2111_0_2_i_2_n_0;
  wire sprites_data_reg_r3_2048_2111_0_2_i_3_n_0;
  wire sprites_data_reg_r3_2048_2111_0_2_i_4_n_0;
  wire sprites_data_reg_r3_2048_2111_0_2_n_0;
  wire sprites_data_reg_r3_2048_2111_0_2_n_1;
  wire sprites_data_reg_r3_2048_2111_0_2_n_2;
  wire sprites_data_reg_r3_2048_2111_3_5_n_0;
  wire sprites_data_reg_r3_2048_2111_3_5_n_1;
  wire sprites_data_reg_r3_2048_2111_3_5_n_2;
  wire sprites_data_reg_r3_256_319_0_2_n_0;
  wire sprites_data_reg_r3_256_319_0_2_n_1;
  wire sprites_data_reg_r3_256_319_0_2_n_2;
  wire sprites_data_reg_r3_256_319_3_5_n_0;
  wire sprites_data_reg_r3_256_319_3_5_n_1;
  wire sprites_data_reg_r3_256_319_3_5_n_2;
  wire sprites_data_reg_r3_320_383_0_2_n_0;
  wire sprites_data_reg_r3_320_383_0_2_n_1;
  wire sprites_data_reg_r3_320_383_0_2_n_2;
  wire sprites_data_reg_r3_320_383_3_5_n_0;
  wire sprites_data_reg_r3_320_383_3_5_n_1;
  wire sprites_data_reg_r3_320_383_3_5_n_2;
  wire sprites_data_reg_r3_384_447_0_2_n_0;
  wire sprites_data_reg_r3_384_447_0_2_n_1;
  wire sprites_data_reg_r3_384_447_0_2_n_2;
  wire sprites_data_reg_r3_384_447_3_5_n_0;
  wire sprites_data_reg_r3_384_447_3_5_n_1;
  wire sprites_data_reg_r3_384_447_3_5_n_2;
  wire sprites_data_reg_r3_448_511_0_2_n_0;
  wire sprites_data_reg_r3_448_511_0_2_n_1;
  wire sprites_data_reg_r3_448_511_0_2_n_2;
  wire sprites_data_reg_r3_448_511_3_5_n_0;
  wire sprites_data_reg_r3_448_511_3_5_n_1;
  wire sprites_data_reg_r3_448_511_3_5_n_2;
  wire sprites_data_reg_r3_512_575_0_2_n_0;
  wire sprites_data_reg_r3_512_575_0_2_n_1;
  wire sprites_data_reg_r3_512_575_0_2_n_2;
  wire sprites_data_reg_r3_512_575_3_5_n_0;
  wire sprites_data_reg_r3_512_575_3_5_n_1;
  wire sprites_data_reg_r3_512_575_3_5_n_2;
  wire sprites_data_reg_r3_576_639_0_2_n_0;
  wire sprites_data_reg_r3_576_639_0_2_n_1;
  wire sprites_data_reg_r3_576_639_0_2_n_2;
  wire sprites_data_reg_r3_576_639_3_5_n_0;
  wire sprites_data_reg_r3_576_639_3_5_n_1;
  wire sprites_data_reg_r3_576_639_3_5_n_2;
  wire sprites_data_reg_r3_640_703_0_2_n_0;
  wire sprites_data_reg_r3_640_703_0_2_n_1;
  wire sprites_data_reg_r3_640_703_0_2_n_2;
  wire sprites_data_reg_r3_640_703_3_5_n_0;
  wire sprites_data_reg_r3_640_703_3_5_n_1;
  wire sprites_data_reg_r3_640_703_3_5_n_2;
  wire sprites_data_reg_r3_64_127_0_2_n_0;
  wire sprites_data_reg_r3_64_127_0_2_n_1;
  wire sprites_data_reg_r3_64_127_0_2_n_2;
  wire sprites_data_reg_r3_64_127_3_5_n_0;
  wire sprites_data_reg_r3_64_127_3_5_n_1;
  wire sprites_data_reg_r3_64_127_3_5_n_2;
  wire sprites_data_reg_r3_704_767_0_2_n_0;
  wire sprites_data_reg_r3_704_767_0_2_n_1;
  wire sprites_data_reg_r3_704_767_0_2_n_2;
  wire sprites_data_reg_r3_704_767_3_5_n_0;
  wire sprites_data_reg_r3_704_767_3_5_n_1;
  wire sprites_data_reg_r3_704_767_3_5_n_2;
  wire sprites_data_reg_r3_768_831_0_2_n_0;
  wire sprites_data_reg_r3_768_831_0_2_n_1;
  wire sprites_data_reg_r3_768_831_0_2_n_2;
  wire sprites_data_reg_r3_768_831_3_5_n_0;
  wire sprites_data_reg_r3_768_831_3_5_n_1;
  wire sprites_data_reg_r3_768_831_3_5_n_2;
  wire sprites_data_reg_r3_832_895_0_2_n_0;
  wire sprites_data_reg_r3_832_895_0_2_n_1;
  wire sprites_data_reg_r3_832_895_0_2_n_2;
  wire sprites_data_reg_r3_832_895_3_5_n_0;
  wire sprites_data_reg_r3_832_895_3_5_n_1;
  wire sprites_data_reg_r3_832_895_3_5_n_2;
  wire sprites_data_reg_r3_896_959_0_2_n_0;
  wire sprites_data_reg_r3_896_959_0_2_n_1;
  wire sprites_data_reg_r3_896_959_0_2_n_2;
  wire sprites_data_reg_r3_896_959_3_5_n_0;
  wire sprites_data_reg_r3_896_959_3_5_n_1;
  wire sprites_data_reg_r3_896_959_3_5_n_2;
  wire sprites_data_reg_r3_960_1023_0_2_n_0;
  wire sprites_data_reg_r3_960_1023_0_2_n_1;
  wire sprites_data_reg_r3_960_1023_0_2_n_2;
  wire sprites_data_reg_r3_960_1023_3_5_n_0;
  wire sprites_data_reg_r3_960_1023_3_5_n_1;
  wire sprites_data_reg_r3_960_1023_3_5_n_2;
  wire [0:0]sw;
  wire [5:0]tile_column_write_counter_reg__0;
  wire [3:0]tile_id;
  wire \tile_row_write_counter_reg_n_0_[0] ;
  wire \tile_row_write_counter_reg_n_0_[1] ;
  wire \tile_row_write_counter_reg_n_0_[2] ;
  wire \tile_row_write_counter_reg_n_0_[3] ;
  wire \tile_row_write_counter_reg_n_0_[4] ;
  wire tiles_reg_0_63_0_2_n_0;
  wire tiles_reg_0_63_0_2_n_1;
  wire tiles_reg_0_63_0_2_n_2;
  wire tiles_reg_0_63_3_3_n_0;
  wire [5:0]tm_reg_0;
  wire [6:0]tm_reg_0_0;
  wire [1:0]tm_reg_0_1;
  wire tm_reg_0_i_18_n_0;
  wire tm_reg_0_i_29_n_2;
  wire tm_reg_0_i_29_n_3;
  wire tm_reg_0_i_30_n_0;
  wire tm_reg_0_i_30_n_1;
  wire tm_reg_0_i_30_n_2;
  wire tm_reg_0_i_30_n_3;
  wire tm_reg_0_i_38_n_0;
  wire tm_reg_0_i_39_n_0;
  wire tm_reg_0_i_44_n_0;
  wire tm_reg_0_i_6_n_0;
  wire tm_reg_0_i_6_n_1;
  wire tm_reg_0_i_6_n_2;
  wire tm_reg_0_i_6_n_3;
  wire tm_reg_0_i_7_n_0;
  wire tm_reg_0_i_7_n_1;
  wire tm_reg_0_i_7_n_2;
  wire tm_reg_0_i_7_n_3;
  wire [1:0]v_cnt;
  wire [5:0]\v_cnt_reg[1]_rep ;
  wire [1:0]\v_cnt_reg[1]_rep__1 ;
  wire [0:0]\v_cnt_reg[8] ;
  wire [5:0]\v_cnt_reg[9] ;
  wire [3:3]\NLW_ind_reg[8]_i_1_CO_UNCONNECTED ;
  wire NLW_sprites_data_reg_r1_0_63_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r1_0_63_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r1_128_191_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r1_128_191_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r1_192_255_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r1_192_255_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r1_64_127_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r1_64_127_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_0_63_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_0_63_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_1024_1087_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_1024_1087_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_1088_1151_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_1088_1151_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_1152_1215_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_1152_1215_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_1216_1279_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_1216_1279_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_1280_1343_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_1280_1343_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_128_191_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_128_191_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_1344_1407_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_1344_1407_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_1408_1471_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_1408_1471_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_1472_1535_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_1472_1535_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_1536_1599_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_1536_1599_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_1600_1663_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_1600_1663_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_1664_1727_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_1664_1727_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_1728_1791_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_1728_1791_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_1792_1855_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_1792_1855_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_1856_1919_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_1856_1919_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_1920_1983_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_1920_1983_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_192_255_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_192_255_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_1984_2047_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_1984_2047_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_2048_2111_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_2048_2111_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_256_319_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_256_319_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_320_383_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_320_383_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_384_447_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_384_447_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_448_511_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_448_511_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_512_575_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_512_575_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_576_639_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_576_639_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_640_703_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_640_703_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_64_127_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_64_127_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_704_767_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_704_767_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_768_831_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_768_831_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_832_895_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_832_895_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_896_959_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_896_959_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_960_1023_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r2_960_1023_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_0_63_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_0_63_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_1024_1087_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_1024_1087_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_1088_1151_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_1088_1151_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_1152_1215_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_1152_1215_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_1216_1279_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_1216_1279_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_1280_1343_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_1280_1343_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_128_191_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_128_191_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_1344_1407_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_1344_1407_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_1408_1471_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_1408_1471_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_1472_1535_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_1472_1535_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_1536_1599_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_1536_1599_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_1600_1663_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_1600_1663_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_1664_1727_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_1664_1727_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_1728_1791_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_1728_1791_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_1792_1855_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_1792_1855_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_1856_1919_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_1856_1919_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_1920_1983_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_1920_1983_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_192_255_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_192_255_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_1984_2047_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_1984_2047_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_2048_2111_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_2048_2111_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_256_319_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_256_319_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_320_383_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_320_383_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_384_447_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_384_447_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_448_511_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_448_511_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_512_575_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_512_575_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_576_639_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_576_639_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_640_703_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_640_703_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_64_127_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_64_127_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_704_767_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_704_767_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_768_831_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_768_831_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_832_895_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_832_895_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_896_959_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_896_959_3_5_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_960_1023_0_2_DOD_UNCONNECTED;
  wire NLW_sprites_data_reg_r3_960_1023_3_5_DOD_UNCONNECTED;
  wire NLW_tiles_reg_0_63_0_2_DOD_UNCONNECTED;
  wire NLW_tiles_reg_0_63_3_3_DOB_UNCONNECTED;
  wire NLW_tiles_reg_0_63_3_3_DOC_UNCONNECTED;
  wire NLW_tiles_reg_0_63_3_3_DOD_UNCONNECTED;
  wire [2:2]NLW_tm_reg_0_i_29_CO_UNCONNECTED;
  wire [3:3]NLW_tm_reg_0_i_29_O_UNCONNECTED;
  wire [3:0]NLW_tm_reg_0_i_5_CO_UNCONNECTED;
  wire [3:1]NLW_tm_reg_0_i_5_O_UNCONNECTED;
  wire [0:0]NLW_tm_reg_0_i_7_O_UNCONNECTED;

  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'hA9)) 
    \addr_X[0]_i_1 
       (.I0(tile_column_write_counter_reg__0[0]),
        .I1(render_enable),
        .I2(\addr_X_reg[0]_0 ),
        .O(\addr_X[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT4 #(
    .INIT(16'hEF10)) 
    \addr_X[1]_i_1 
       (.I0(\addr_X_reg[0]_0 ),
        .I1(render_enable),
        .I2(tile_column_write_counter_reg__0[0]),
        .I3(tile_column_write_counter_reg__0[1]),
        .O(p_2_out[1]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT5 #(
    .INIT(32'hA9AAAAAA)) 
    \addr_X[2]_i_1 
       (.I0(tile_column_write_counter_reg__0[2]),
        .I1(render_enable),
        .I2(\addr_X_reg[0]_0 ),
        .I3(tile_column_write_counter_reg__0[1]),
        .I4(tile_column_write_counter_reg__0[0]),
        .O(p_2_out[2]));
  LUT6 #(
    .INIT(64'hEFFFFFFF10000000)) 
    \addr_X[3]_i_1 
       (.I0(render_enable),
        .I1(\addr_X_reg[0]_0 ),
        .I2(tile_column_write_counter_reg__0[2]),
        .I3(tile_column_write_counter_reg__0[0]),
        .I4(tile_column_write_counter_reg__0[1]),
        .I5(tile_column_write_counter_reg__0[3]),
        .O(p_2_out[3]));
  LUT6 #(
    .INIT(64'hFFFF00007FFF8000)) 
    \addr_X[4]_i_1 
       (.I0(tile_column_write_counter_reg__0[3]),
        .I1(tile_column_write_counter_reg__0[1]),
        .I2(tile_column_write_counter_reg__0[0]),
        .I3(tile_column_write_counter_reg__0[2]),
        .I4(tile_column_write_counter_reg__0[4]),
        .I5(\addr_X[4]_i_2_n_0 ),
        .O(p_2_out[4]));
  LUT2 #(
    .INIT(4'hE)) 
    \addr_X[4]_i_2 
       (.I0(\addr_X_reg[0]_0 ),
        .I1(render_enable),
        .O(\addr_X[4]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h9AAA)) 
    \addr_X[5]_i_2 
       (.I0(tile_column_write_counter_reg__0[5]),
        .I1(\addr_X[5]_i_7_n_0 ),
        .I2(tile_column_write_counter_reg__0[3]),
        .I3(tile_column_write_counter_reg__0[4]),
        .O(p_2_out[5]));
  LUT6 #(
    .INIT(64'h5555555555545555)) 
    \addr_X[5]_i_3 
       (.I0(line_complete_reg_n_0),
        .I1(\addr_X[4]_i_2_n_0 ),
        .I2(line_complete_i_2_n_0),
        .I3(tile_column_write_counter_reg__0[3]),
        .I4(tile_column_write_counter_reg__0[5]),
        .I5(tile_column_write_counter_reg__0[4]),
        .O(\addr_Y_reg[0]_0 ));
  LUT5 #(
    .INIT(32'h2AAAAAAA)) 
    \addr_X[5]_i_4 
       (.I0(\h_cnt_reg[7] ),
        .I1(\tile_row_write_counter_reg_n_0_[3] ),
        .I2(\tile_row_write_counter_reg_n_0_[1] ),
        .I3(\tile_row_write_counter_reg_n_0_[4] ),
        .I4(\tile_row_write_counter_reg_n_0_[2] ),
        .O(\addr_Y_reg[0]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT5 #(
    .INIT(32'hEFFFFFFF)) 
    \addr_X[5]_i_7 
       (.I0(render_enable),
        .I1(\addr_X_reg[0]_0 ),
        .I2(tile_column_write_counter_reg__0[2]),
        .I3(tile_column_write_counter_reg__0[0]),
        .I4(tile_column_write_counter_reg__0[1]),
        .O(\addr_X[5]_i_7_n_0 ));
  FDRE \addr_X_reg[0] 
       (.C(clk),
        .CE(\h_cnt_reg[6] ),
        .D(\addr_X[0]_i_1_n_0 ),
        .Q(ADDRBWRADDR[0]),
        .R(1'b0));
  FDRE \addr_X_reg[1] 
       (.C(clk),
        .CE(\h_cnt_reg[6] ),
        .D(p_2_out[1]),
        .Q(ADDRBWRADDR[1]),
        .R(1'b0));
  FDRE \addr_X_reg[2] 
       (.C(clk),
        .CE(\h_cnt_reg[6] ),
        .D(p_2_out[2]),
        .Q(ADDRBWRADDR[2]),
        .R(1'b0));
  FDRE \addr_X_reg[3] 
       (.C(clk),
        .CE(\h_cnt_reg[6] ),
        .D(p_2_out[3]),
        .Q(ADDRBWRADDR[3]),
        .R(1'b0));
  FDRE \addr_X_reg[4] 
       (.C(clk),
        .CE(\h_cnt_reg[6] ),
        .D(p_2_out[4]),
        .Q(tm_reg_0_1[0]),
        .R(1'b0));
  FDRE \addr_X_reg[5] 
       (.C(clk),
        .CE(\h_cnt_reg[6] ),
        .D(p_2_out[5]),
        .Q(tm_reg_0_1[1]),
        .R(1'b0));
  FDRE \addr_Y_reg[0] 
       (.C(clk),
        .CE(\h_cnt_reg[6] ),
        .D(\tile_row_write_counter_reg_n_0_[0] ),
        .Q(tm_reg_0[0]),
        .R(1'b0));
  FDRE \addr_Y_reg[1] 
       (.C(clk),
        .CE(\h_cnt_reg[6] ),
        .D(\tile_row_write_counter_reg_n_0_[1] ),
        .Q(tm_reg_0[1]),
        .R(1'b0));
  FDRE \addr_Y_reg[2] 
       (.C(clk),
        .CE(\h_cnt_reg[6] ),
        .D(\tile_row_write_counter_reg_n_0_[2] ),
        .Q(tm_reg_0[2]),
        .R(1'b0));
  FDRE \addr_Y_reg[3] 
       (.C(clk),
        .CE(\h_cnt_reg[6] ),
        .D(\tile_row_write_counter_reg_n_0_[3] ),
        .Q(tm_reg_0[3]),
        .R(1'b0));
  FDRE \addr_Y_reg[4] 
       (.C(clk),
        .CE(\h_cnt_reg[6] ),
        .D(\tile_row_write_counter_reg_n_0_[4] ),
        .Q(tm_reg_0[4]),
        .R(1'b0));
  FDRE \addr_Y_reg[5] 
       (.C(clk),
        .CE(\h_cnt_reg[6] ),
        .D(Q),
        .Q(tm_reg_0[5]),
        .R(1'b0));
  LUT3 #(
    .INIT(8'h6A)) 
    \current_tile[2]_i_1 
       (.I0(current_tile__0[2]),
        .I1(\h_cnt_reg[1] ),
        .I2(\current_tile_reg[3]_0 [1]),
        .O(\current_tile[2]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00006AAA)) 
    \current_tile[3]_i_1 
       (.I0(current_tile[3]),
        .I1(\current_tile_reg[3]_0 [1]),
        .I2(\h_cnt_reg[1] ),
        .I3(\current_tile_reg[3]_0 [2]),
        .I4(\h_cnt_reg[6]_0 ),
        .O(\current_tile[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h1222222222222222)) 
    \current_tile[4]_i_1 
       (.I0(current_tile[4]),
        .I1(\h_cnt_reg[6]_0 ),
        .I2(\current_tile_reg[3]_0 [2]),
        .I3(\h_cnt_reg[1] ),
        .I4(\current_tile_reg[3]_0 [1]),
        .I5(current_tile[3]),
        .O(\current_tile[4]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h12222222)) 
    \current_tile[5]_i_1 
       (.I0(current_tile[5]),
        .I1(\h_cnt_reg[6]_0 ),
        .I2(current_tile[3]),
        .I3(\current_tile[5]_i_3_n_0 ),
        .I4(current_tile[4]),
        .O(\current_tile[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \current_tile[5]_i_3 
       (.I0(current_tile__0[2]),
        .I1(\current_tile_reg[1]_0 [0]),
        .I2(\h_cnt_reg[1]_0 ),
        .I3(ADDRC[2]),
        .I4(ADDRC[3]),
        .I5(\current_tile_reg[3]_0 [1]),
        .O(\current_tile[5]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \current_tile_reg[0] 
       (.C(pixel_clk),
        .CE(render_enable),
        .D(D[0]),
        .Q(\current_tile_reg[3]_0 [0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \current_tile_reg[1] 
       (.C(pixel_clk),
        .CE(render_enable),
        .D(D[1]),
        .Q(\current_tile_reg[3]_0 [1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \current_tile_reg[2] 
       (.C(pixel_clk),
        .CE(render_enable),
        .D(\current_tile[2]_i_1_n_0 ),
        .Q(\current_tile_reg[3]_0 [2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \current_tile_reg[3] 
       (.C(pixel_clk),
        .CE(render_enable),
        .D(\current_tile[3]_i_1_n_0 ),
        .Q(current_tile[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \current_tile_reg[4] 
       (.C(pixel_clk),
        .CE(render_enable),
        .D(\current_tile[4]_i_1_n_0 ),
        .Q(current_tile[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \current_tile_reg[5] 
       (.C(pixel_clk),
        .CE(render_enable),
        .D(\current_tile[5]_i_1_n_0 ),
        .Q(current_tile[5]),
        .R(1'b0));
  LUT1 #(
    .INIT(2'h2)) 
    \ind[0]_i_3 
       (.I0(ind_reg[3]),
        .O(\ind[0]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \ind[0]_i_4 
       (.I0(ind_reg[2]),
        .O(\ind[0]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \ind[0]_i_5 
       (.I0(ind_reg[1]),
        .O(\ind[0]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \ind[0]_i_6 
       (.I0(ind_reg[0]),
        .O(\ind[0]_i_6_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \ind[4]_i_2 
       (.I0(ind_reg[7]),
        .O(\ind[4]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \ind[4]_i_3 
       (.I0(ind_reg[6]),
        .O(\ind[4]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \ind[4]_i_4 
       (.I0(ind_reg[5]),
        .O(\ind[4]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \ind[4]_i_5 
       (.I0(ind_reg[4]),
        .O(\ind[4]_i_5_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \ind[8]_i_2 
       (.I0(O34),
        .O(\ind[8]_i_2_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \ind[8]_i_3 
       (.I0(ind_reg[10]),
        .O(\ind[8]_i_3_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \ind[8]_i_4 
       (.I0(ind_reg[9]),
        .O(\ind[8]_i_4_n_0 ));
  LUT1 #(
    .INIT(2'h2)) 
    \ind[8]_i_5 
       (.I0(ind_reg[8]),
        .O(\ind[8]_i_5_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \ind_reg[0] 
       (.C(lopt),
        .CE(fetching_sprites_reg),
        .D(\ind_reg[0]_i_2_n_7 ),
        .Q(ind_reg[0]),
        .R(1'b0));
  CARRY4 \ind_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\ind_reg[0]_i_2_n_0 ,\ind_reg[0]_i_2_n_1 ,\ind_reg[0]_i_2_n_2 ,\ind_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\ind_reg[0]_i_2_n_4 ,\ind_reg[0]_i_2_n_5 ,\ind_reg[0]_i_2_n_6 ,\ind_reg[0]_i_2_n_7 }),
        .S({\ind[0]_i_3_n_0 ,\ind[0]_i_4_n_0 ,\ind[0]_i_5_n_0 ,\ind[0]_i_6_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \ind_reg[10] 
       (.C(lopt),
        .CE(fetching_sprites_reg),
        .D(\ind_reg[8]_i_1_n_5 ),
        .Q(ind_reg[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ind_reg[11] 
       (.C(lopt),
        .CE(fetching_sprites_reg),
        .D(\ind_reg[8]_i_1_n_4 ),
        .Q(O34),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ind_reg[1] 
       (.C(lopt),
        .CE(fetching_sprites_reg),
        .D(\ind_reg[0]_i_2_n_6 ),
        .Q(ind_reg[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ind_reg[2] 
       (.C(lopt),
        .CE(fetching_sprites_reg),
        .D(\ind_reg[0]_i_2_n_5 ),
        .Q(ind_reg[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ind_reg[3] 
       (.C(lopt),
        .CE(fetching_sprites_reg),
        .D(\ind_reg[0]_i_2_n_4 ),
        .Q(ind_reg[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ind_reg[4] 
       (.C(lopt),
        .CE(fetching_sprites_reg),
        .D(\ind_reg[4]_i_1_n_7 ),
        .Q(ind_reg[4]),
        .R(1'b0));
  CARRY4 \ind_reg[4]_i_1 
       (.CI(\ind_reg[0]_i_2_n_0 ),
        .CO({\ind_reg[4]_i_1_n_0 ,\ind_reg[4]_i_1_n_1 ,\ind_reg[4]_i_1_n_2 ,\ind_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\ind_reg[4]_i_1_n_4 ,\ind_reg[4]_i_1_n_5 ,\ind_reg[4]_i_1_n_6 ,\ind_reg[4]_i_1_n_7 }),
        .S({\ind[4]_i_2_n_0 ,\ind[4]_i_3_n_0 ,\ind[4]_i_4_n_0 ,\ind[4]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \ind_reg[5] 
       (.C(lopt),
        .CE(fetching_sprites_reg),
        .D(\ind_reg[4]_i_1_n_6 ),
        .Q(ind_reg[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ind_reg[6] 
       (.C(lopt),
        .CE(fetching_sprites_reg),
        .D(\ind_reg[4]_i_1_n_5 ),
        .Q(ind_reg[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ind_reg[7] 
       (.C(lopt),
        .CE(fetching_sprites_reg),
        .D(\ind_reg[4]_i_1_n_4 ),
        .Q(ind_reg[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \ind_reg[8] 
       (.C(lopt),
        .CE(fetching_sprites_reg),
        .D(\ind_reg[8]_i_1_n_7 ),
        .Q(ind_reg[8]),
        .R(1'b0));
  CARRY4 \ind_reg[8]_i_1 
       (.CI(\ind_reg[4]_i_1_n_0 ),
        .CO({\NLW_ind_reg[8]_i_1_CO_UNCONNECTED [3],\ind_reg[8]_i_1_n_1 ,\ind_reg[8]_i_1_n_2 ,\ind_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\ind_reg[8]_i_1_n_4 ,\ind_reg[8]_i_1_n_5 ,\ind_reg[8]_i_1_n_6 ,\ind_reg[8]_i_1_n_7 }),
        .S({\ind[8]_i_2_n_0 ,\ind[8]_i_3_n_0 ,\ind[8]_i_4_n_0 ,\ind[8]_i_5_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \ind_reg[9] 
       (.C(lopt),
        .CE(fetching_sprites_reg),
        .D(\ind_reg[8]_i_1_n_6 ),
        .Q(ind_reg[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT4 #(
    .INIT(16'h00E2)) 
    \isFinder[0]_i_1 
       (.I0(\isFinder_reg[0]_0 ),
        .I1(\h_cnt_reg[6] ),
        .I2(p_10_in),
        .I3(isFinder),
        .O(\isFinder[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000100000000)) 
    \isFinder[0]_i_2 
       (.I0(\tile_row_write_counter_reg_n_0_[4] ),
        .I1(Q),
        .I2(\tile_row_write_counter_reg_n_0_[3] ),
        .I3(\tile_row_write_counter_reg_n_0_[1] ),
        .I4(\tile_row_write_counter_reg_n_0_[2] ),
        .I5(\tile_row_write_counter_reg_n_0_[0] ),
        .O(p_10_in));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'hF2)) 
    \isFinder[1]_i_1 
       (.I0(\isFinder_reg[1]_0 ),
        .I1(\h_cnt_reg[6] ),
        .I2(isFinder),
        .O(\isFinder[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000000200000000)) 
    \isFinder[1]_i_2 
       (.I0(\h_cnt_reg[6] ),
        .I1(\tile_row_write_counter_reg_n_0_[3] ),
        .I2(\tile_row_write_counter_reg_n_0_[0] ),
        .I3(\tile_row_write_counter_reg_n_0_[1] ),
        .I4(\tile_row_write_counter_reg_n_0_[2] ),
        .I5(\isFinder[1]_i_3_n_0 ),
        .O(isFinder));
  LUT2 #(
    .INIT(4'h1)) 
    \isFinder[1]_i_3 
       (.I0(Q),
        .I1(\tile_row_write_counter_reg_n_0_[4] ),
        .O(\isFinder[1]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \isFinder_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\isFinder[0]_i_1_n_0 ),
        .Q(\isFinder_reg[0]_0 ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \isFinder_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\isFinder[1]_i_1_n_0 ),
        .Q(\isFinder_reg[1]_0 ),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFFFFF00000004)) 
    line_complete_i_1
       (.I0(tile_column_write_counter_reg__0[4]),
        .I1(tile_column_write_counter_reg__0[5]),
        .I2(tile_column_write_counter_reg__0[3]),
        .I3(line_complete_i_2_n_0),
        .I4(\addr_X[4]_i_2_n_0 ),
        .I5(line_complete_reg_n_0),
        .O(line_complete_i_1_n_0));
  LUT3 #(
    .INIT(8'h7F)) 
    line_complete_i_2
       (.I0(tile_column_write_counter_reg__0[1]),
        .I1(tile_column_write_counter_reg__0[0]),
        .I2(tile_column_write_counter_reg__0[2]),
        .O(line_complete_i_2_n_0));
  FDRE #(
    .INIT(1'b0)) 
    line_complete_reg
       (.C(clk),
        .CE(1'b1),
        .D(line_complete_i_1_n_0),
        .Q(line_complete_reg_n_0),
        .R(SR));
  LUT5 #(
    .INIT(32'hEEEAAAAA)) 
    \pixel_bus[12]_i_1 
       (.I0(\pixel_bus[12]_i_2_n_0 ),
        .I1(\pixel_bus[12]_i_3_n_0 ),
        .I2(p_1_in4_in),
        .I3(p_0_in3_in),
        .I4(\pixel_bus[4]_i_3_n_0 ),
        .O(\pixel_bus[12]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \pixel_bus[12]_i_2 
       (.I0(\pixel_bus_reg[15]_i_3_n_0 ),
        .I1(\pixel_bus_reg[15]_i_4_n_0 ),
        .O(\pixel_bus[12]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT5 #(
    .INIT(32'h00000047)) 
    \pixel_bus[12]_i_3 
       (.I0(p_0_in18_in),
        .I1(tiles_reg_0_63_0_2_n_0),
        .I2(p_0_in9_in),
        .I3(\pixel_bus_reg[15]_i_4_n_0 ),
        .I4(\pixel_bus[14]_i_3_n_0 ),
        .O(\pixel_bus[12]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[12]_i_4 
       (.I0(sprites_data_reg_r1_192_255_3_5_n_1),
        .I1(sprites_data_reg_r1_128_191_3_5_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r1_64_127_3_5_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r1_0_63_3_5_n_1),
        .O(p_1_in4_in));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAAABAA)) 
    \pixel_bus[13]_i_1 
       (.I0(\pixel_bus_reg[15]_i_4_n_0 ),
        .I1(\pixel_bus[3]_i_2_n_0 ),
        .I2(\pixel_bus[4]_i_6_n_0 ),
        .I3(\pixel_bus[3]_i_3_n_0 ),
        .I4(\pixel_bus[4]_i_2_n_0 ),
        .I5(\pixel_bus[13]_i_2_n_0 ),
        .O(\pixel_bus[13]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFDDD1FFFFFFFF)) 
    \pixel_bus[13]_i_2 
       (.I0(\pixel_bus[13]_i_3_n_0 ),
        .I1(tiles_reg_0_63_0_2_n_0),
        .I2(\pixel_bus_reg[4]_i_12_n_0 ),
        .I3(p_0_in15_in),
        .I4(\pixel_bus_reg[15]_i_3_n_0 ),
        .I5(p_1_in4_in),
        .O(\pixel_bus[13]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h00053305)) 
    \pixel_bus[13]_i_3 
       (.I0(\pixel_bus[4]_i_22_n_0 ),
        .I1(\pixel_bus[4]_i_23_n_0 ),
        .I2(\pixel_bus[4]_i_20_n_0 ),
        .I3(tiles_reg_0_63_3_3_n_0),
        .I4(\pixel_bus[4]_i_21_n_0 ),
        .O(\pixel_bus[13]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAAAAAABAAAAAAAAA)) 
    \pixel_bus[14]_i_1 
       (.I0(\pixel_bus[14]_i_2_n_0 ),
        .I1(\pixel_bus[4]_i_2_n_0 ),
        .I2(\pixel_bus[2]_i_4_n_0 ),
        .I3(\pixel_bus[14]_i_3_n_0 ),
        .I4(\pixel_bus[14]_i_4_n_0 ),
        .I5(\pixel_bus[14]_i_5_n_0 ),
        .O(\pixel_bus[14]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[14]_i_18 
       (.I0(sprites_data_reg_r3_1728_1791_0_2_n_2),
        .I1(sprites_data_reg_r3_1664_1727_0_2_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_1600_1663_0_2_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_1536_1599_0_2_n_2),
        .O(\pixel_bus[14]_i_18_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[14]_i_19 
       (.I0(sprites_data_reg_r3_1984_2047_0_2_n_2),
        .I1(sprites_data_reg_r3_1920_1983_0_2_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_1856_1919_0_2_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_1792_1855_0_2_n_2),
        .O(\pixel_bus[14]_i_19_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \pixel_bus[14]_i_2 
       (.I0(\pixel_bus_reg[15]_i_3_n_0 ),
        .I1(\pixel_bus_reg[15]_i_4_n_0 ),
        .O(\pixel_bus[14]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[14]_i_20 
       (.I0(sprites_data_reg_r3_1216_1279_0_2_n_2),
        .I1(sprites_data_reg_r3_1152_1215_0_2_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_1088_1151_0_2_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_1024_1087_0_2_n_2),
        .O(\pixel_bus[14]_i_20_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[14]_i_21 
       (.I0(sprites_data_reg_r3_1472_1535_0_2_n_2),
        .I1(sprites_data_reg_r3_1408_1471_0_2_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_1344_1407_0_2_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_1280_1343_0_2_n_2),
        .O(\pixel_bus[14]_i_21_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[14]_i_22 
       (.I0(sprites_data_reg_r3_704_767_0_2_n_2),
        .I1(sprites_data_reg_r3_640_703_0_2_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_576_639_0_2_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_512_575_0_2_n_2),
        .O(\pixel_bus[14]_i_22_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[14]_i_23 
       (.I0(sprites_data_reg_r3_960_1023_0_2_n_2),
        .I1(sprites_data_reg_r3_896_959_0_2_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_832_895_0_2_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_768_831_0_2_n_2),
        .O(\pixel_bus[14]_i_23_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[14]_i_24 
       (.I0(sprites_data_reg_r3_192_255_0_2_n_2),
        .I1(sprites_data_reg_r3_128_191_0_2_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_64_127_0_2_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_0_63_0_2_n_2),
        .O(\pixel_bus[14]_i_24_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[14]_i_25 
       (.I0(sprites_data_reg_r3_448_511_0_2_n_2),
        .I1(sprites_data_reg_r3_384_447_0_2_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_320_383_0_2_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_256_319_0_2_n_2),
        .O(\pixel_bus[14]_i_25_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[14]_i_26 
       (.I0(sprites_data_reg_r2_1728_1791_0_2_n_2),
        .I1(sprites_data_reg_r2_1664_1727_0_2_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_1600_1663_0_2_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_1536_1599_0_2_n_2),
        .O(\pixel_bus[14]_i_26_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[14]_i_27 
       (.I0(sprites_data_reg_r2_1984_2047_0_2_n_2),
        .I1(sprites_data_reg_r2_1920_1983_0_2_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_1856_1919_0_2_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_1792_1855_0_2_n_2),
        .O(\pixel_bus[14]_i_27_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[14]_i_28 
       (.I0(sprites_data_reg_r2_1216_1279_0_2_n_2),
        .I1(sprites_data_reg_r2_1152_1215_0_2_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_1088_1151_0_2_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_1024_1087_0_2_n_2),
        .O(\pixel_bus[14]_i_28_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[14]_i_29 
       (.I0(sprites_data_reg_r2_1472_1535_0_2_n_2),
        .I1(sprites_data_reg_r2_1408_1471_0_2_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_1344_1407_0_2_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_1280_1343_0_2_n_2),
        .O(\pixel_bus[14]_i_29_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[14]_i_3 
       (.I0(\pixel_bus[14]_i_6_n_0 ),
        .I1(\pixel_bus[14]_i_7_n_0 ),
        .I2(tiles_reg_0_63_0_2_n_0),
        .I3(\pixel_bus[14]_i_8_n_0 ),
        .I4(tiles_reg_0_63_3_3_n_0),
        .I5(\pixel_bus[14]_i_9_n_0 ),
        .O(\pixel_bus[14]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[14]_i_30 
       (.I0(sprites_data_reg_r2_704_767_0_2_n_2),
        .I1(sprites_data_reg_r2_640_703_0_2_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_576_639_0_2_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_512_575_0_2_n_2),
        .O(\pixel_bus[14]_i_30_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[14]_i_31 
       (.I0(sprites_data_reg_r2_960_1023_0_2_n_2),
        .I1(sprites_data_reg_r2_896_959_0_2_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_832_895_0_2_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_768_831_0_2_n_2),
        .O(\pixel_bus[14]_i_31_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[14]_i_32 
       (.I0(sprites_data_reg_r2_192_255_0_2_n_2),
        .I1(sprites_data_reg_r2_128_191_0_2_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_64_127_0_2_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_0_63_0_2_n_2),
        .O(\pixel_bus[14]_i_32_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[14]_i_33 
       (.I0(sprites_data_reg_r2_448_511_0_2_n_2),
        .I1(sprites_data_reg_r2_384_447_0_2_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_320_383_0_2_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_256_319_0_2_n_2),
        .O(\pixel_bus[14]_i_33_n_0 ));
  LUT2 #(
    .INIT(4'hB)) 
    \pixel_bus[14]_i_4 
       (.I0(\pixel_bus_reg[15]_i_4_n_0 ),
        .I1(p_0_in3_in),
        .O(\pixel_bus[14]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \pixel_bus[14]_i_5 
       (.I0(p_1_in4_in),
        .I1(\pixel_bus_reg[15]_i_3_n_0 ),
        .O(\pixel_bus[14]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \pixel_bus[14]_i_6 
       (.I0(tiles_reg_0_63_0_2_n_1),
        .I1(v_cnt[1]),
        .I2(sprites_data_reg_r3_2048_2111_0_2_n_2),
        .I3(v_cnt[0]),
        .I4(tiles_reg_0_63_0_2_n_0),
        .I5(tiles_reg_0_63_0_2_n_2),
        .O(\pixel_bus[14]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[14]_i_7 
       (.I0(\pixel_bus_reg[14]_i_10_n_0 ),
        .I1(\pixel_bus_reg[14]_i_11_n_0 ),
        .I2(tiles_reg_0_63_0_2_n_2),
        .I3(\pixel_bus_reg[14]_i_12_n_0 ),
        .I4(tiles_reg_0_63_0_2_n_1),
        .I5(\pixel_bus_reg[14]_i_13_n_0 ),
        .O(\pixel_bus[14]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \pixel_bus[14]_i_8 
       (.I0(tiles_reg_0_63_0_2_n_1),
        .I1(v_cnt[1]),
        .I2(sprites_data_reg_r2_2048_2111_0_2_n_2),
        .I3(v_cnt[0]),
        .I4(tiles_reg_0_63_0_2_n_0),
        .I5(tiles_reg_0_63_0_2_n_2),
        .O(\pixel_bus[14]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[14]_i_9 
       (.I0(\pixel_bus_reg[14]_i_14_n_0 ),
        .I1(\pixel_bus_reg[14]_i_15_n_0 ),
        .I2(tiles_reg_0_63_0_2_n_2),
        .I3(\pixel_bus_reg[14]_i_16_n_0 ),
        .I4(tiles_reg_0_63_0_2_n_1),
        .I5(\pixel_bus_reg[14]_i_17_n_0 ),
        .O(\pixel_bus[14]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \pixel_bus[15]_i_10 
       (.I0(tiles_reg_0_63_0_2_n_1),
        .I1(v_cnt[1]),
        .I2(sprites_data_reg_r2_2048_2111_3_5_n_2),
        .I3(v_cnt[0]),
        .I4(tiles_reg_0_63_0_2_n_0),
        .I5(tiles_reg_0_63_0_2_n_2),
        .O(\pixel_bus[15]_i_10_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \pixel_bus[15]_i_13 
       (.I0(tiles_reg_0_63_0_2_n_1),
        .I1(v_cnt[1]),
        .I2(sprites_data_reg_r3_2048_2111_3_5_n_2),
        .I3(v_cnt[0]),
        .I4(tiles_reg_0_63_0_2_n_0),
        .I5(tiles_reg_0_63_0_2_n_2),
        .O(\pixel_bus[15]_i_13_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \pixel_bus[15]_i_16 
       (.I0(tiles_reg_0_63_0_2_n_1),
        .I1(v_cnt[1]),
        .I2(sprites_data_reg_r2_2048_2111_3_5_n_1),
        .I3(v_cnt[0]),
        .I4(tiles_reg_0_63_0_2_n_0),
        .I5(tiles_reg_0_63_0_2_n_2),
        .O(\pixel_bus[15]_i_16_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \pixel_bus[15]_i_19 
       (.I0(tiles_reg_0_63_0_2_n_1),
        .I1(v_cnt[1]),
        .I2(sprites_data_reg_r3_2048_2111_3_5_n_1),
        .I3(v_cnt[0]),
        .I4(tiles_reg_0_63_0_2_n_0),
        .I5(tiles_reg_0_63_0_2_n_2),
        .O(\pixel_bus[15]_i_19_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT5 #(
    .INIT(32'hAAAABAAA)) 
    \pixel_bus[15]_i_2 
       (.I0(\pixel_bus_reg[15]_i_3_n_0 ),
        .I1(\pixel_bus_reg[15]_i_4_n_0 ),
        .I2(p_0_in3_in),
        .I3(\pixel_bus[4]_i_3_n_0 ),
        .I4(\pixel_bus[4]_i_2_n_0 ),
        .O(\pixel_bus[15]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[15]_i_38 
       (.I0(sprites_data_reg_r2_1216_1279_3_5_n_2),
        .I1(sprites_data_reg_r2_1152_1215_3_5_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_1088_1151_3_5_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_1024_1087_3_5_n_2),
        .O(\pixel_bus[15]_i_38_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[15]_i_39 
       (.I0(sprites_data_reg_r2_1472_1535_3_5_n_2),
        .I1(sprites_data_reg_r2_1408_1471_3_5_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_1344_1407_3_5_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_1280_1343_3_5_n_2),
        .O(\pixel_bus[15]_i_39_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[15]_i_40 
       (.I0(sprites_data_reg_r2_1728_1791_3_5_n_2),
        .I1(sprites_data_reg_r2_1664_1727_3_5_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_1600_1663_3_5_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_1536_1599_3_5_n_2),
        .O(\pixel_bus[15]_i_40_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[15]_i_41 
       (.I0(sprites_data_reg_r2_1984_2047_3_5_n_2),
        .I1(sprites_data_reg_r2_1920_1983_3_5_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_1856_1919_3_5_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_1792_1855_3_5_n_2),
        .O(\pixel_bus[15]_i_41_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[15]_i_42 
       (.I0(sprites_data_reg_r2_192_255_3_5_n_2),
        .I1(sprites_data_reg_r2_128_191_3_5_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_64_127_3_5_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_0_63_3_5_n_2),
        .O(\pixel_bus[15]_i_42_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[15]_i_43 
       (.I0(sprites_data_reg_r2_448_511_3_5_n_2),
        .I1(sprites_data_reg_r2_384_447_3_5_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_320_383_3_5_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_256_319_3_5_n_2),
        .O(\pixel_bus[15]_i_43_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[15]_i_44 
       (.I0(sprites_data_reg_r2_704_767_3_5_n_2),
        .I1(sprites_data_reg_r2_640_703_3_5_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_576_639_3_5_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_512_575_3_5_n_2),
        .O(\pixel_bus[15]_i_44_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[15]_i_45 
       (.I0(sprites_data_reg_r2_960_1023_3_5_n_2),
        .I1(sprites_data_reg_r2_896_959_3_5_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_832_895_3_5_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_768_831_3_5_n_2),
        .O(\pixel_bus[15]_i_45_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[15]_i_46 
       (.I0(sprites_data_reg_r3_1216_1279_3_5_n_2),
        .I1(sprites_data_reg_r3_1152_1215_3_5_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_1088_1151_3_5_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_1024_1087_3_5_n_2),
        .O(\pixel_bus[15]_i_46_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[15]_i_47 
       (.I0(sprites_data_reg_r3_1472_1535_3_5_n_2),
        .I1(sprites_data_reg_r3_1408_1471_3_5_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_1344_1407_3_5_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_1280_1343_3_5_n_2),
        .O(\pixel_bus[15]_i_47_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[15]_i_48 
       (.I0(sprites_data_reg_r3_1728_1791_3_5_n_2),
        .I1(sprites_data_reg_r3_1664_1727_3_5_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_1600_1663_3_5_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_1536_1599_3_5_n_2),
        .O(\pixel_bus[15]_i_48_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[15]_i_49 
       (.I0(sprites_data_reg_r3_1984_2047_3_5_n_2),
        .I1(sprites_data_reg_r3_1920_1983_3_5_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_1856_1919_3_5_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_1792_1855_3_5_n_2),
        .O(\pixel_bus[15]_i_49_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[15]_i_5 
       (.I0(sprites_data_reg_r1_192_255_3_5_n_2),
        .I1(sprites_data_reg_r1_128_191_3_5_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r1_64_127_3_5_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r1_0_63_3_5_n_2),
        .O(p_0_in3_in));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[15]_i_50 
       (.I0(sprites_data_reg_r3_192_255_3_5_n_2),
        .I1(sprites_data_reg_r3_128_191_3_5_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_64_127_3_5_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_0_63_3_5_n_2),
        .O(\pixel_bus[15]_i_50_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[15]_i_51 
       (.I0(sprites_data_reg_r3_448_511_3_5_n_2),
        .I1(sprites_data_reg_r3_384_447_3_5_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_320_383_3_5_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_256_319_3_5_n_2),
        .O(\pixel_bus[15]_i_51_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[15]_i_52 
       (.I0(sprites_data_reg_r3_704_767_3_5_n_2),
        .I1(sprites_data_reg_r3_640_703_3_5_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_576_639_3_5_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_512_575_3_5_n_2),
        .O(\pixel_bus[15]_i_52_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[15]_i_53 
       (.I0(sprites_data_reg_r3_960_1023_3_5_n_2),
        .I1(sprites_data_reg_r3_896_959_3_5_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_832_895_3_5_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_768_831_3_5_n_2),
        .O(\pixel_bus[15]_i_53_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[15]_i_54 
       (.I0(sprites_data_reg_r2_1216_1279_3_5_n_1),
        .I1(sprites_data_reg_r2_1152_1215_3_5_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_1088_1151_3_5_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_1024_1087_3_5_n_1),
        .O(\pixel_bus[15]_i_54_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[15]_i_55 
       (.I0(sprites_data_reg_r2_1472_1535_3_5_n_1),
        .I1(sprites_data_reg_r2_1408_1471_3_5_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_1344_1407_3_5_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_1280_1343_3_5_n_1),
        .O(\pixel_bus[15]_i_55_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[15]_i_56 
       (.I0(sprites_data_reg_r2_1728_1791_3_5_n_1),
        .I1(sprites_data_reg_r2_1664_1727_3_5_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_1600_1663_3_5_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_1536_1599_3_5_n_1),
        .O(\pixel_bus[15]_i_56_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[15]_i_57 
       (.I0(sprites_data_reg_r2_1984_2047_3_5_n_1),
        .I1(sprites_data_reg_r2_1920_1983_3_5_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_1856_1919_3_5_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_1792_1855_3_5_n_1),
        .O(\pixel_bus[15]_i_57_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[15]_i_58 
       (.I0(sprites_data_reg_r2_192_255_3_5_n_1),
        .I1(sprites_data_reg_r2_128_191_3_5_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_64_127_3_5_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_0_63_3_5_n_1),
        .O(\pixel_bus[15]_i_58_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[15]_i_59 
       (.I0(sprites_data_reg_r2_448_511_3_5_n_1),
        .I1(sprites_data_reg_r2_384_447_3_5_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_320_383_3_5_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_256_319_3_5_n_1),
        .O(\pixel_bus[15]_i_59_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \pixel_bus[15]_i_6 
       (.I0(\pixel_bus[15]_i_10_n_0 ),
        .I1(tiles_reg_0_63_3_3_n_0),
        .I2(\pixel_bus_reg[15]_i_11_n_0 ),
        .I3(tiles_reg_0_63_0_2_n_2),
        .I4(\pixel_bus_reg[15]_i_12_n_0 ),
        .O(p_0_in12_in));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[15]_i_60 
       (.I0(sprites_data_reg_r2_704_767_3_5_n_1),
        .I1(sprites_data_reg_r2_640_703_3_5_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_576_639_3_5_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_512_575_3_5_n_1),
        .O(\pixel_bus[15]_i_60_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[15]_i_61 
       (.I0(sprites_data_reg_r2_960_1023_3_5_n_1),
        .I1(sprites_data_reg_r2_896_959_3_5_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_832_895_3_5_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_768_831_3_5_n_1),
        .O(\pixel_bus[15]_i_61_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[15]_i_62 
       (.I0(sprites_data_reg_r3_1216_1279_3_5_n_1),
        .I1(sprites_data_reg_r3_1152_1215_3_5_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_1088_1151_3_5_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_1024_1087_3_5_n_1),
        .O(\pixel_bus[15]_i_62_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[15]_i_63 
       (.I0(sprites_data_reg_r3_1472_1535_3_5_n_1),
        .I1(sprites_data_reg_r3_1408_1471_3_5_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_1344_1407_3_5_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_1280_1343_3_5_n_1),
        .O(\pixel_bus[15]_i_63_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[15]_i_64 
       (.I0(sprites_data_reg_r3_1728_1791_3_5_n_1),
        .I1(sprites_data_reg_r3_1664_1727_3_5_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_1600_1663_3_5_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_1536_1599_3_5_n_1),
        .O(\pixel_bus[15]_i_64_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[15]_i_65 
       (.I0(sprites_data_reg_r3_1984_2047_3_5_n_1),
        .I1(sprites_data_reg_r3_1920_1983_3_5_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_1856_1919_3_5_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_1792_1855_3_5_n_1),
        .O(\pixel_bus[15]_i_65_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[15]_i_66 
       (.I0(sprites_data_reg_r3_192_255_3_5_n_1),
        .I1(sprites_data_reg_r3_128_191_3_5_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_64_127_3_5_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_0_63_3_5_n_1),
        .O(\pixel_bus[15]_i_66_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[15]_i_67 
       (.I0(sprites_data_reg_r3_448_511_3_5_n_1),
        .I1(sprites_data_reg_r3_384_447_3_5_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_320_383_3_5_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_256_319_3_5_n_1),
        .O(\pixel_bus[15]_i_67_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[15]_i_68 
       (.I0(sprites_data_reg_r3_704_767_3_5_n_1),
        .I1(sprites_data_reg_r3_640_703_3_5_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_576_639_3_5_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_512_575_3_5_n_1),
        .O(\pixel_bus[15]_i_68_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[15]_i_69 
       (.I0(sprites_data_reg_r3_960_1023_3_5_n_1),
        .I1(sprites_data_reg_r3_896_959_3_5_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_832_895_3_5_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_768_831_3_5_n_1),
        .O(\pixel_bus[15]_i_69_n_0 ));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \pixel_bus[15]_i_7 
       (.I0(\pixel_bus[15]_i_13_n_0 ),
        .I1(tiles_reg_0_63_3_3_n_0),
        .I2(\pixel_bus_reg[15]_i_14_n_0 ),
        .I3(tiles_reg_0_63_0_2_n_2),
        .I4(\pixel_bus_reg[15]_i_15_n_0 ),
        .O(p_0_in21_in));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \pixel_bus[15]_i_8 
       (.I0(\pixel_bus[15]_i_16_n_0 ),
        .I1(tiles_reg_0_63_3_3_n_0),
        .I2(\pixel_bus_reg[15]_i_17_n_0 ),
        .I3(tiles_reg_0_63_0_2_n_2),
        .I4(\pixel_bus_reg[15]_i_18_n_0 ),
        .O(p_1_in13_in));
  LUT5 #(
    .INIT(32'hB8BBB888)) 
    \pixel_bus[15]_i_9 
       (.I0(\pixel_bus[15]_i_19_n_0 ),
        .I1(tiles_reg_0_63_3_3_n_0),
        .I2(\pixel_bus_reg[15]_i_20_n_0 ),
        .I3(tiles_reg_0_63_0_2_n_2),
        .I4(\pixel_bus_reg[15]_i_21_n_0 ),
        .O(p_1_in22_in));
  LUT6 #(
    .INIT(64'h0010FFFFFFFFFFFF)) 
    \pixel_bus[2]_i_1 
       (.I0(\pixel_bus[2]_i_2_n_0 ),
        .I1(\pixel_bus[4]_i_2_n_0 ),
        .I2(\pixel_bus[3]_i_3_n_0 ),
        .I3(\pixel_bus[2]_i_3_n_0 ),
        .I4(\pixel_bus[2]_i_4_n_0 ),
        .I5(\pixel_bus_reg[2]_0 ),
        .O(\pixel_bus[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT4 #(
    .INIT(16'hEEEF)) 
    \pixel_bus[2]_i_2 
       (.I0(\pixel_bus_reg[15]_i_4_n_0 ),
        .I1(\pixel_bus_reg[15]_i_3_n_0 ),
        .I2(\pixel_bus[4]_i_14_n_0 ),
        .I3(p_0_in),
        .O(\pixel_bus[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFE222)) 
    \pixel_bus[2]_i_3 
       (.I0(\pixel_bus[2]_i_5_n_0 ),
        .I1(tiles_reg_0_63_0_2_n_0),
        .I2(p_1_in19_in),
        .I3(p_0_in18_in),
        .I4(\pixel_bus[2]_i_6_n_0 ),
        .I5(\pixel_bus[2]_i_7_n_0 ),
        .O(\pixel_bus[2]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'h1010101F)) 
    \pixel_bus[2]_i_4 
       (.I0(p_0_in15_in),
        .I1(\pixel_bus_reg[4]_i_12_n_0 ),
        .I2(tiles_reg_0_63_0_2_n_0),
        .I3(p_0_in6_in),
        .I4(\pixel_bus_reg[4]_i_10_n_0 ),
        .O(\pixel_bus[2]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hCCA000A0)) 
    \pixel_bus[2]_i_5 
       (.I0(\pixel_bus[4]_i_16_n_0 ),
        .I1(\pixel_bus[4]_i_17_n_0 ),
        .I2(\pixel_bus[14]_i_9_n_0 ),
        .I3(tiles_reg_0_63_3_3_n_0),
        .I4(\pixel_bus[14]_i_8_n_0 ),
        .O(\pixel_bus[2]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h00000000C0AAC000)) 
    \pixel_bus[2]_i_6 
       (.I0(\pixel_bus[4]_i_22_n_0 ),
        .I1(\pixel_bus[4]_i_23_n_0 ),
        .I2(\pixel_bus[4]_i_21_n_0 ),
        .I3(tiles_reg_0_63_3_3_n_0),
        .I4(\pixel_bus[4]_i_20_n_0 ),
        .I5(tiles_reg_0_63_0_2_n_0),
        .O(\pixel_bus[2]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hC000A0A0C0000000)) 
    \pixel_bus[2]_i_7 
       (.I0(\pixel_bus[4]_i_26_n_0 ),
        .I1(\pixel_bus[4]_i_27_n_0 ),
        .I2(tiles_reg_0_63_0_2_n_0),
        .I3(\pixel_bus[4]_i_25_n_0 ),
        .I4(tiles_reg_0_63_3_3_n_0),
        .I5(\pixel_bus[4]_i_24_n_0 ),
        .O(\pixel_bus[2]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h0001FFFFFFFFFFFF)) 
    \pixel_bus[3]_i_1 
       (.I0(\pixel_bus[4]_i_2_n_0 ),
        .I1(\pixel_bus[4]_i_5_n_0 ),
        .I2(\pixel_bus[3]_i_2_n_0 ),
        .I3(\pixel_bus[4]_i_6_n_0 ),
        .I4(\pixel_bus[3]_i_3_n_0 ),
        .I5(\pixel_bus_reg[2]_0 ),
        .O(\pixel_bus[3]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h8F808080)) 
    \pixel_bus[3]_i_2 
       (.I0(p_0_in18_in),
        .I1(p_1_in19_in),
        .I2(tiles_reg_0_63_0_2_n_0),
        .I3(p_0_in9_in),
        .I4(p_1_in10_in),
        .O(\pixel_bus[3]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h1D)) 
    \pixel_bus[3]_i_3 
       (.I0(\pixel_bus_reg[4]_i_10_n_0 ),
        .I1(tiles_reg_0_63_0_2_n_0),
        .I2(\pixel_bus_reg[4]_i_12_n_0 ),
        .O(\pixel_bus[3]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF0040FFFFFFFF)) 
    \pixel_bus[4]_i_1 
       (.I0(\pixel_bus[4]_i_2_n_0 ),
        .I1(\pixel_bus[4]_i_3_n_0 ),
        .I2(p_0_in),
        .I3(\pixel_bus[4]_i_5_n_0 ),
        .I4(\pixel_bus[4]_i_6_n_0 ),
        .I5(\pixel_bus_reg[2]_0 ),
        .O(\pixel_bus[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_100 
       (.I0(sprites_data_reg_r3_704_767_0_2_n_1),
        .I1(sprites_data_reg_r3_640_703_0_2_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_576_639_0_2_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_512_575_0_2_n_1),
        .O(\pixel_bus[4]_i_100_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_101 
       (.I0(sprites_data_reg_r3_960_1023_0_2_n_1),
        .I1(sprites_data_reg_r3_896_959_0_2_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_832_895_0_2_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_768_831_0_2_n_1),
        .O(\pixel_bus[4]_i_101_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_102 
       (.I0(sprites_data_reg_r3_192_255_0_2_n_1),
        .I1(sprites_data_reg_r3_128_191_0_2_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_64_127_0_2_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_0_63_0_2_n_1),
        .O(\pixel_bus[4]_i_102_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_103 
       (.I0(sprites_data_reg_r3_448_511_0_2_n_1),
        .I1(sprites_data_reg_r3_384_447_0_2_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_320_383_0_2_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_256_319_0_2_n_1),
        .O(\pixel_bus[4]_i_103_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_14 
       (.I0(sprites_data_reg_r1_192_255_0_2_n_0),
        .I1(sprites_data_reg_r1_128_191_0_2_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r1_64_127_0_2_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r1_0_63_0_2_n_0),
        .O(\pixel_bus[4]_i_14_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_16 
       (.I0(\pixel_bus_reg[4]_i_30_n_0 ),
        .I1(\pixel_bus_reg[4]_i_31_n_0 ),
        .I2(tiles_reg_0_63_0_2_n_2),
        .I3(\pixel_bus_reg[4]_i_32_n_0 ),
        .I4(tiles_reg_0_63_0_2_n_1),
        .I5(\pixel_bus_reg[4]_i_33_n_0 ),
        .O(\pixel_bus[4]_i_16_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \pixel_bus[4]_i_17 
       (.I0(tiles_reg_0_63_0_2_n_1),
        .I1(v_cnt[1]),
        .I2(sprites_data_reg_r2_2048_2111_3_5_n_0),
        .I3(v_cnt[0]),
        .I4(tiles_reg_0_63_0_2_n_0),
        .I5(tiles_reg_0_63_0_2_n_2),
        .O(\pixel_bus[4]_i_17_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_18 
       (.I0(\pixel_bus_reg[4]_i_34_n_0 ),
        .I1(\pixel_bus_reg[4]_i_35_n_0 ),
        .I2(tiles_reg_0_63_0_2_n_2),
        .I3(\pixel_bus_reg[4]_i_36_n_0 ),
        .I4(tiles_reg_0_63_0_2_n_1),
        .I5(\pixel_bus_reg[4]_i_37_n_0 ),
        .O(\pixel_bus[4]_i_18_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \pixel_bus[4]_i_19 
       (.I0(tiles_reg_0_63_0_2_n_1),
        .I1(v_cnt[1]),
        .I2(sprites_data_reg_r3_2048_2111_3_5_n_0),
        .I3(v_cnt[0]),
        .I4(tiles_reg_0_63_0_2_n_0),
        .I5(tiles_reg_0_63_0_2_n_2),
        .O(\pixel_bus[4]_i_19_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFEFEFEFFFE)) 
    \pixel_bus[4]_i_2 
       (.I0(\pixel_bus_reg[15]_i_3_n_0 ),
        .I1(\pixel_bus[14]_i_3_n_0 ),
        .I2(\pixel_bus_reg[15]_i_4_n_0 ),
        .I3(p_0_in9_in),
        .I4(tiles_reg_0_63_0_2_n_0),
        .I5(p_0_in18_in),
        .O(\pixel_bus[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_20 
       (.I0(\pixel_bus_reg[4]_i_38_n_0 ),
        .I1(\pixel_bus_reg[4]_i_39_n_0 ),
        .I2(tiles_reg_0_63_0_2_n_2),
        .I3(\pixel_bus_reg[4]_i_40_n_0 ),
        .I4(tiles_reg_0_63_0_2_n_1),
        .I5(\pixel_bus_reg[4]_i_41_n_0 ),
        .O(\pixel_bus[4]_i_20_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \pixel_bus[4]_i_21 
       (.I0(tiles_reg_0_63_0_2_n_1),
        .I1(v_cnt[1]),
        .I2(sprites_data_reg_r2_2048_2111_0_2_n_0),
        .I3(v_cnt[0]),
        .I4(tiles_reg_0_63_0_2_n_0),
        .I5(tiles_reg_0_63_0_2_n_2),
        .O(\pixel_bus[4]_i_21_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_22 
       (.I0(\pixel_bus_reg[4]_i_42_n_0 ),
        .I1(\pixel_bus_reg[4]_i_43_n_0 ),
        .I2(tiles_reg_0_63_0_2_n_2),
        .I3(\pixel_bus_reg[4]_i_44_n_0 ),
        .I4(tiles_reg_0_63_0_2_n_1),
        .I5(\pixel_bus_reg[4]_i_45_n_0 ),
        .O(\pixel_bus[4]_i_22_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \pixel_bus[4]_i_23 
       (.I0(tiles_reg_0_63_0_2_n_1),
        .I1(v_cnt[1]),
        .I2(sprites_data_reg_r2_2048_2111_0_2_n_1),
        .I3(v_cnt[0]),
        .I4(tiles_reg_0_63_0_2_n_0),
        .I5(tiles_reg_0_63_0_2_n_2),
        .O(\pixel_bus[4]_i_23_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_24 
       (.I0(\pixel_bus_reg[4]_i_46_n_0 ),
        .I1(\pixel_bus_reg[4]_i_47_n_0 ),
        .I2(tiles_reg_0_63_0_2_n_2),
        .I3(\pixel_bus_reg[4]_i_48_n_0 ),
        .I4(tiles_reg_0_63_0_2_n_1),
        .I5(\pixel_bus_reg[4]_i_49_n_0 ),
        .O(\pixel_bus[4]_i_24_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \pixel_bus[4]_i_25 
       (.I0(tiles_reg_0_63_0_2_n_1),
        .I1(v_cnt[1]),
        .I2(sprites_data_reg_r3_2048_2111_0_2_n_0),
        .I3(v_cnt[0]),
        .I4(tiles_reg_0_63_0_2_n_0),
        .I5(tiles_reg_0_63_0_2_n_2),
        .O(\pixel_bus[4]_i_25_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_26 
       (.I0(\pixel_bus_reg[4]_i_50_n_0 ),
        .I1(\pixel_bus_reg[4]_i_51_n_0 ),
        .I2(tiles_reg_0_63_0_2_n_2),
        .I3(\pixel_bus_reg[4]_i_52_n_0 ),
        .I4(tiles_reg_0_63_0_2_n_1),
        .I5(\pixel_bus_reg[4]_i_53_n_0 ),
        .O(\pixel_bus[4]_i_26_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000010)) 
    \pixel_bus[4]_i_27 
       (.I0(tiles_reg_0_63_0_2_n_1),
        .I1(v_cnt[1]),
        .I2(sprites_data_reg_r3_2048_2111_0_2_n_1),
        .I3(v_cnt[0]),
        .I4(tiles_reg_0_63_0_2_n_0),
        .I5(tiles_reg_0_63_0_2_n_2),
        .O(\pixel_bus[4]_i_27_n_0 ));
  LUT6 #(
    .INIT(64'h00000000010101F1)) 
    \pixel_bus[4]_i_3 
       (.I0(\pixel_bus_reg[4]_i_10_n_0 ),
        .I1(p_0_in6_in),
        .I2(tiles_reg_0_63_0_2_n_0),
        .I3(\pixel_bus_reg[4]_i_12_n_0 ),
        .I4(p_0_in15_in),
        .I5(\pixel_bus[14]_i_3_n_0 ),
        .O(\pixel_bus[4]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_4 
       (.I0(sprites_data_reg_r1_192_255_0_2_n_1),
        .I1(sprites_data_reg_r1_128_191_0_2_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r1_64_127_0_2_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r1_0_63_0_2_n_1),
        .O(p_0_in));
  LUT6 #(
    .INIT(64'hFFFDFFFDFFFD55FD)) 
    \pixel_bus[4]_i_5 
       (.I0(\pixel_bus[4]_i_14_n_0 ),
        .I1(\pixel_bus_reg[4]_i_10_n_0 ),
        .I2(p_0_in6_in),
        .I3(tiles_reg_0_63_0_2_n_0),
        .I4(\pixel_bus_reg[4]_i_12_n_0 ),
        .I5(p_0_in15_in),
        .O(\pixel_bus[4]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_56 
       (.I0(sprites_data_reg_r2_1728_1791_3_5_n_0),
        .I1(sprites_data_reg_r2_1664_1727_3_5_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_1600_1663_3_5_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_1536_1599_3_5_n_0),
        .O(\pixel_bus[4]_i_56_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_57 
       (.I0(sprites_data_reg_r2_1984_2047_3_5_n_0),
        .I1(sprites_data_reg_r2_1920_1983_3_5_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_1856_1919_3_5_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_1792_1855_3_5_n_0),
        .O(\pixel_bus[4]_i_57_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_58 
       (.I0(sprites_data_reg_r2_1216_1279_3_5_n_0),
        .I1(sprites_data_reg_r2_1152_1215_3_5_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_1088_1151_3_5_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_1024_1087_3_5_n_0),
        .O(\pixel_bus[4]_i_58_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_59 
       (.I0(sprites_data_reg_r2_1472_1535_3_5_n_0),
        .I1(sprites_data_reg_r2_1408_1471_3_5_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_1344_1407_3_5_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_1280_1343_3_5_n_0),
        .O(\pixel_bus[4]_i_59_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'h8F808080)) 
    \pixel_bus[4]_i_6 
       (.I0(\pixel_bus_reg[4]_i_12_n_0 ),
        .I1(p_0_in15_in),
        .I2(tiles_reg_0_63_0_2_n_0),
        .I3(\pixel_bus_reg[4]_i_10_n_0 ),
        .I4(p_0_in6_in),
        .O(\pixel_bus[4]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_60 
       (.I0(sprites_data_reg_r2_704_767_3_5_n_0),
        .I1(sprites_data_reg_r2_640_703_3_5_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_576_639_3_5_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_512_575_3_5_n_0),
        .O(\pixel_bus[4]_i_60_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_61 
       (.I0(sprites_data_reg_r2_960_1023_3_5_n_0),
        .I1(sprites_data_reg_r2_896_959_3_5_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_832_895_3_5_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_768_831_3_5_n_0),
        .O(\pixel_bus[4]_i_61_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_62 
       (.I0(sprites_data_reg_r2_192_255_3_5_n_0),
        .I1(sprites_data_reg_r2_128_191_3_5_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_64_127_3_5_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_0_63_3_5_n_0),
        .O(\pixel_bus[4]_i_62_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_63 
       (.I0(sprites_data_reg_r2_448_511_3_5_n_0),
        .I1(sprites_data_reg_r2_384_447_3_5_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_320_383_3_5_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_256_319_3_5_n_0),
        .O(\pixel_bus[4]_i_63_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_64 
       (.I0(sprites_data_reg_r3_1728_1791_3_5_n_0),
        .I1(sprites_data_reg_r3_1664_1727_3_5_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_1600_1663_3_5_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_1536_1599_3_5_n_0),
        .O(\pixel_bus[4]_i_64_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_65 
       (.I0(sprites_data_reg_r3_1984_2047_3_5_n_0),
        .I1(sprites_data_reg_r3_1920_1983_3_5_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_1856_1919_3_5_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_1792_1855_3_5_n_0),
        .O(\pixel_bus[4]_i_65_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_66 
       (.I0(sprites_data_reg_r3_1216_1279_3_5_n_0),
        .I1(sprites_data_reg_r3_1152_1215_3_5_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_1088_1151_3_5_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_1024_1087_3_5_n_0),
        .O(\pixel_bus[4]_i_66_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_67 
       (.I0(sprites_data_reg_r3_1472_1535_3_5_n_0),
        .I1(sprites_data_reg_r3_1408_1471_3_5_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_1344_1407_3_5_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_1280_1343_3_5_n_0),
        .O(\pixel_bus[4]_i_67_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_68 
       (.I0(sprites_data_reg_r3_704_767_3_5_n_0),
        .I1(sprites_data_reg_r3_640_703_3_5_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_576_639_3_5_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_512_575_3_5_n_0),
        .O(\pixel_bus[4]_i_68_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_69 
       (.I0(sprites_data_reg_r3_960_1023_3_5_n_0),
        .I1(sprites_data_reg_r3_896_959_3_5_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_832_895_3_5_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_768_831_3_5_n_0),
        .O(\pixel_bus[4]_i_69_n_0 ));
  LUT6 #(
    .INIT(64'hEFEFEFEFEFEFEFEE)) 
    \pixel_bus[4]_i_7 
       (.I0(\current_tile_reg[1]_1 ),
        .I1(sw),
        .I2(\h_cnt_reg[6]_0 ),
        .I3(current_tile[3]),
        .I4(current_tile[5]),
        .I5(current_tile[4]),
        .O(\pixel_bus_reg[2]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_70 
       (.I0(sprites_data_reg_r3_192_255_3_5_n_0),
        .I1(sprites_data_reg_r3_128_191_3_5_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_64_127_3_5_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_0_63_3_5_n_0),
        .O(\pixel_bus[4]_i_70_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_71 
       (.I0(sprites_data_reg_r3_448_511_3_5_n_0),
        .I1(sprites_data_reg_r3_384_447_3_5_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_320_383_3_5_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_256_319_3_5_n_0),
        .O(\pixel_bus[4]_i_71_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_72 
       (.I0(sprites_data_reg_r2_1728_1791_0_2_n_0),
        .I1(sprites_data_reg_r2_1664_1727_0_2_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_1600_1663_0_2_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_1536_1599_0_2_n_0),
        .O(\pixel_bus[4]_i_72_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_73 
       (.I0(sprites_data_reg_r2_1984_2047_0_2_n_0),
        .I1(sprites_data_reg_r2_1920_1983_0_2_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_1856_1919_0_2_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_1792_1855_0_2_n_0),
        .O(\pixel_bus[4]_i_73_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_74 
       (.I0(sprites_data_reg_r2_1216_1279_0_2_n_0),
        .I1(sprites_data_reg_r2_1152_1215_0_2_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_1088_1151_0_2_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_1024_1087_0_2_n_0),
        .O(\pixel_bus[4]_i_74_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_75 
       (.I0(sprites_data_reg_r2_1472_1535_0_2_n_0),
        .I1(sprites_data_reg_r2_1408_1471_0_2_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_1344_1407_0_2_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_1280_1343_0_2_n_0),
        .O(\pixel_bus[4]_i_75_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_76 
       (.I0(sprites_data_reg_r2_704_767_0_2_n_0),
        .I1(sprites_data_reg_r2_640_703_0_2_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_576_639_0_2_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_512_575_0_2_n_0),
        .O(\pixel_bus[4]_i_76_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_77 
       (.I0(sprites_data_reg_r2_960_1023_0_2_n_0),
        .I1(sprites_data_reg_r2_896_959_0_2_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_832_895_0_2_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_768_831_0_2_n_0),
        .O(\pixel_bus[4]_i_77_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_78 
       (.I0(sprites_data_reg_r2_192_255_0_2_n_0),
        .I1(sprites_data_reg_r2_128_191_0_2_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_64_127_0_2_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_0_63_0_2_n_0),
        .O(\pixel_bus[4]_i_78_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_79 
       (.I0(sprites_data_reg_r2_448_511_0_2_n_0),
        .I1(sprites_data_reg_r2_384_447_0_2_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_320_383_0_2_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_256_319_0_2_n_0),
        .O(\pixel_bus[4]_i_79_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_80 
       (.I0(sprites_data_reg_r2_1728_1791_0_2_n_1),
        .I1(sprites_data_reg_r2_1664_1727_0_2_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_1600_1663_0_2_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_1536_1599_0_2_n_1),
        .O(\pixel_bus[4]_i_80_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_81 
       (.I0(sprites_data_reg_r2_1984_2047_0_2_n_1),
        .I1(sprites_data_reg_r2_1920_1983_0_2_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_1856_1919_0_2_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_1792_1855_0_2_n_1),
        .O(\pixel_bus[4]_i_81_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_82 
       (.I0(sprites_data_reg_r2_1216_1279_0_2_n_1),
        .I1(sprites_data_reg_r2_1152_1215_0_2_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_1088_1151_0_2_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_1024_1087_0_2_n_1),
        .O(\pixel_bus[4]_i_82_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_83 
       (.I0(sprites_data_reg_r2_1472_1535_0_2_n_1),
        .I1(sprites_data_reg_r2_1408_1471_0_2_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_1344_1407_0_2_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_1280_1343_0_2_n_1),
        .O(\pixel_bus[4]_i_83_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_84 
       (.I0(sprites_data_reg_r2_704_767_0_2_n_1),
        .I1(sprites_data_reg_r2_640_703_0_2_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_576_639_0_2_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_512_575_0_2_n_1),
        .O(\pixel_bus[4]_i_84_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_85 
       (.I0(sprites_data_reg_r2_960_1023_0_2_n_1),
        .I1(sprites_data_reg_r2_896_959_0_2_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_832_895_0_2_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_768_831_0_2_n_1),
        .O(\pixel_bus[4]_i_85_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_86 
       (.I0(sprites_data_reg_r2_192_255_0_2_n_1),
        .I1(sprites_data_reg_r2_128_191_0_2_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_64_127_0_2_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_0_63_0_2_n_1),
        .O(\pixel_bus[4]_i_86_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_87 
       (.I0(sprites_data_reg_r2_448_511_0_2_n_1),
        .I1(sprites_data_reg_r2_384_447_0_2_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r2_320_383_0_2_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r2_256_319_0_2_n_1),
        .O(\pixel_bus[4]_i_87_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_88 
       (.I0(sprites_data_reg_r3_1728_1791_0_2_n_0),
        .I1(sprites_data_reg_r3_1664_1727_0_2_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_1600_1663_0_2_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_1536_1599_0_2_n_0),
        .O(\pixel_bus[4]_i_88_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_89 
       (.I0(sprites_data_reg_r3_1984_2047_0_2_n_0),
        .I1(sprites_data_reg_r3_1920_1983_0_2_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_1856_1919_0_2_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_1792_1855_0_2_n_0),
        .O(\pixel_bus[4]_i_89_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_90 
       (.I0(sprites_data_reg_r3_1216_1279_0_2_n_0),
        .I1(sprites_data_reg_r3_1152_1215_0_2_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_1088_1151_0_2_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_1024_1087_0_2_n_0),
        .O(\pixel_bus[4]_i_90_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_91 
       (.I0(sprites_data_reg_r3_1472_1535_0_2_n_0),
        .I1(sprites_data_reg_r3_1408_1471_0_2_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_1344_1407_0_2_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_1280_1343_0_2_n_0),
        .O(\pixel_bus[4]_i_91_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_92 
       (.I0(sprites_data_reg_r3_704_767_0_2_n_0),
        .I1(sprites_data_reg_r3_640_703_0_2_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_576_639_0_2_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_512_575_0_2_n_0),
        .O(\pixel_bus[4]_i_92_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_93 
       (.I0(sprites_data_reg_r3_960_1023_0_2_n_0),
        .I1(sprites_data_reg_r3_896_959_0_2_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_832_895_0_2_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_768_831_0_2_n_0),
        .O(\pixel_bus[4]_i_93_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_94 
       (.I0(sprites_data_reg_r3_192_255_0_2_n_0),
        .I1(sprites_data_reg_r3_128_191_0_2_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_64_127_0_2_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_0_63_0_2_n_0),
        .O(\pixel_bus[4]_i_94_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_95 
       (.I0(sprites_data_reg_r3_448_511_0_2_n_0),
        .I1(sprites_data_reg_r3_384_447_0_2_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_320_383_0_2_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_256_319_0_2_n_0),
        .O(\pixel_bus[4]_i_95_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_96 
       (.I0(sprites_data_reg_r3_1728_1791_0_2_n_1),
        .I1(sprites_data_reg_r3_1664_1727_0_2_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_1600_1663_0_2_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_1536_1599_0_2_n_1),
        .O(\pixel_bus[4]_i_96_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_97 
       (.I0(sprites_data_reg_r3_1984_2047_0_2_n_1),
        .I1(sprites_data_reg_r3_1920_1983_0_2_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_1856_1919_0_2_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_1792_1855_0_2_n_1),
        .O(\pixel_bus[4]_i_97_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_98 
       (.I0(sprites_data_reg_r3_1216_1279_0_2_n_1),
        .I1(sprites_data_reg_r3_1152_1215_0_2_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_1088_1151_0_2_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_1024_1087_0_2_n_1),
        .O(\pixel_bus[4]_i_98_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[4]_i_99 
       (.I0(sprites_data_reg_r3_1472_1535_0_2_n_1),
        .I1(sprites_data_reg_r3_1408_1471_0_2_n_1),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r3_1344_1407_0_2_n_1),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r3_1280_1343_0_2_n_1),
        .O(\pixel_bus[4]_i_99_n_0 ));
  LUT6 #(
    .INIT(64'h00005400FFFFFFFF)) 
    \pixel_bus[7]_i_1 
       (.I0(\pixel_bus[12]_i_2_n_0 ),
        .I1(p_1_in),
        .I2(p_0_in1_in),
        .I3(\pixel_bus[4]_i_3_n_0 ),
        .I4(\pixel_bus[4]_i_2_n_0 ),
        .I5(\pixel_bus[7]_i_2_n_0 ),
        .O(\pixel_bus[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT4 #(
    .INIT(16'h0151)) 
    \pixel_bus[7]_i_2 
       (.I0(\pixel_bus[14]_i_3_n_0 ),
        .I1(p_0_in9_in),
        .I2(tiles_reg_0_63_0_2_n_0),
        .I3(p_0_in18_in),
        .O(\pixel_bus[7]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hCCEC)) 
    \pixel_bus[8]_i_1 
       (.I0(p_1_in),
        .I1(\pixel_bus[14]_i_3_n_0 ),
        .I2(\pixel_bus[2]_i_4_n_0 ),
        .I3(\pixel_bus[4]_i_2_n_0 ),
        .O(\pixel_bus[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[8]_i_2 
       (.I0(sprites_data_reg_r1_192_255_0_2_n_2),
        .I1(sprites_data_reg_r1_128_191_0_2_n_2),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r1_64_127_0_2_n_2),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r1_0_63_0_2_n_2),
        .O(p_1_in));
  LUT6 #(
    .INIT(64'hAAAAAAAAAAAABAAA)) 
    \pixel_bus[9]_i_1 
       (.I0(\pixel_bus[3]_i_2_n_0 ),
        .I1(\pixel_bus[4]_i_2_n_0 ),
        .I2(p_0_in1_in),
        .I3(\pixel_bus[2]_i_4_n_0 ),
        .I4(\pixel_bus[12]_i_2_n_0 ),
        .I5(\pixel_bus[9]_i_3_n_0 ),
        .O(\pixel_bus[9]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \pixel_bus[9]_i_2 
       (.I0(sprites_data_reg_r1_192_255_3_5_n_0),
        .I1(sprites_data_reg_r1_128_191_3_5_n_0),
        .I2(v_cnt[1]),
        .I3(sprites_data_reg_r1_64_127_3_5_n_0),
        .I4(v_cnt[0]),
        .I5(sprites_data_reg_r1_0_63_3_5_n_0),
        .O(p_0_in1_in));
  LUT6 #(
    .INIT(64'hFFFDDDDDFFFDFFFF)) 
    \pixel_bus[9]_i_3 
       (.I0(p_1_in),
        .I1(\pixel_bus[14]_i_3_n_0 ),
        .I2(p_0_in15_in),
        .I3(\pixel_bus_reg[4]_i_12_n_0 ),
        .I4(tiles_reg_0_63_0_2_n_0),
        .I5(\pixel_bus[13]_i_3_n_0 ),
        .O(\pixel_bus[9]_i_3_n_0 ));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \pixel_bus_reg[12] 
       (.C(pixel_clk),
        .CE(render_enable),
        .D(\pixel_bus[12]_i_1_n_0 ),
        .Q(pixel_bus[6]),
        .R(render_enable_reg_0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \pixel_bus_reg[13] 
       (.C(pixel_clk),
        .CE(render_enable),
        .D(\pixel_bus[13]_i_1_n_0 ),
        .Q(pixel_bus[7]),
        .R(render_enable_reg_0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \pixel_bus_reg[14] 
       (.C(pixel_clk),
        .CE(render_enable),
        .D(\pixel_bus[14]_i_1_n_0 ),
        .Q(pixel_bus[8]),
        .R(render_enable_reg_0));
  MUXF7 \pixel_bus_reg[14]_i_10 
       (.I0(\pixel_bus[14]_i_18_n_0 ),
        .I1(\pixel_bus[14]_i_19_n_0 ),
        .O(\pixel_bus_reg[14]_i_10_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[14]_i_11 
       (.I0(\pixel_bus[14]_i_20_n_0 ),
        .I1(\pixel_bus[14]_i_21_n_0 ),
        .O(\pixel_bus_reg[14]_i_11_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[14]_i_12 
       (.I0(\pixel_bus[14]_i_22_n_0 ),
        .I1(\pixel_bus[14]_i_23_n_0 ),
        .O(\pixel_bus_reg[14]_i_12_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[14]_i_13 
       (.I0(\pixel_bus[14]_i_24_n_0 ),
        .I1(\pixel_bus[14]_i_25_n_0 ),
        .O(\pixel_bus_reg[14]_i_13_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[14]_i_14 
       (.I0(\pixel_bus[14]_i_26_n_0 ),
        .I1(\pixel_bus[14]_i_27_n_0 ),
        .O(\pixel_bus_reg[14]_i_14_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[14]_i_15 
       (.I0(\pixel_bus[14]_i_28_n_0 ),
        .I1(\pixel_bus[14]_i_29_n_0 ),
        .O(\pixel_bus_reg[14]_i_15_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[14]_i_16 
       (.I0(\pixel_bus[14]_i_30_n_0 ),
        .I1(\pixel_bus[14]_i_31_n_0 ),
        .O(\pixel_bus_reg[14]_i_16_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[14]_i_17 
       (.I0(\pixel_bus[14]_i_32_n_0 ),
        .I1(\pixel_bus[14]_i_33_n_0 ),
        .O(\pixel_bus_reg[14]_i_17_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \pixel_bus_reg[15] 
       (.C(pixel_clk),
        .CE(render_enable),
        .D(\pixel_bus[15]_i_2_n_0 ),
        .Q(pixel_bus[9]),
        .R(render_enable_reg_0));
  MUXF8 \pixel_bus_reg[15]_i_11 
       (.I0(\pixel_bus_reg[15]_i_22_n_0 ),
        .I1(\pixel_bus_reg[15]_i_23_n_0 ),
        .O(\pixel_bus_reg[15]_i_11_n_0 ),
        .S(tiles_reg_0_63_0_2_n_1));
  MUXF8 \pixel_bus_reg[15]_i_12 
       (.I0(\pixel_bus_reg[15]_i_24_n_0 ),
        .I1(\pixel_bus_reg[15]_i_25_n_0 ),
        .O(\pixel_bus_reg[15]_i_12_n_0 ),
        .S(tiles_reg_0_63_0_2_n_1));
  MUXF8 \pixel_bus_reg[15]_i_14 
       (.I0(\pixel_bus_reg[15]_i_26_n_0 ),
        .I1(\pixel_bus_reg[15]_i_27_n_0 ),
        .O(\pixel_bus_reg[15]_i_14_n_0 ),
        .S(tiles_reg_0_63_0_2_n_1));
  MUXF8 \pixel_bus_reg[15]_i_15 
       (.I0(\pixel_bus_reg[15]_i_28_n_0 ),
        .I1(\pixel_bus_reg[15]_i_29_n_0 ),
        .O(\pixel_bus_reg[15]_i_15_n_0 ),
        .S(tiles_reg_0_63_0_2_n_1));
  MUXF8 \pixel_bus_reg[15]_i_17 
       (.I0(\pixel_bus_reg[15]_i_30_n_0 ),
        .I1(\pixel_bus_reg[15]_i_31_n_0 ),
        .O(\pixel_bus_reg[15]_i_17_n_0 ),
        .S(tiles_reg_0_63_0_2_n_1));
  MUXF8 \pixel_bus_reg[15]_i_18 
       (.I0(\pixel_bus_reg[15]_i_32_n_0 ),
        .I1(\pixel_bus_reg[15]_i_33_n_0 ),
        .O(\pixel_bus_reg[15]_i_18_n_0 ),
        .S(tiles_reg_0_63_0_2_n_1));
  MUXF8 \pixel_bus_reg[15]_i_20 
       (.I0(\pixel_bus_reg[15]_i_34_n_0 ),
        .I1(\pixel_bus_reg[15]_i_35_n_0 ),
        .O(\pixel_bus_reg[15]_i_20_n_0 ),
        .S(tiles_reg_0_63_0_2_n_1));
  MUXF8 \pixel_bus_reg[15]_i_21 
       (.I0(\pixel_bus_reg[15]_i_36_n_0 ),
        .I1(\pixel_bus_reg[15]_i_37_n_0 ),
        .O(\pixel_bus_reg[15]_i_21_n_0 ),
        .S(tiles_reg_0_63_0_2_n_1));
  MUXF7 \pixel_bus_reg[15]_i_22 
       (.I0(\pixel_bus[15]_i_38_n_0 ),
        .I1(\pixel_bus[15]_i_39_n_0 ),
        .O(\pixel_bus_reg[15]_i_22_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[15]_i_23 
       (.I0(\pixel_bus[15]_i_40_n_0 ),
        .I1(\pixel_bus[15]_i_41_n_0 ),
        .O(\pixel_bus_reg[15]_i_23_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[15]_i_24 
       (.I0(\pixel_bus[15]_i_42_n_0 ),
        .I1(\pixel_bus[15]_i_43_n_0 ),
        .O(\pixel_bus_reg[15]_i_24_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[15]_i_25 
       (.I0(\pixel_bus[15]_i_44_n_0 ),
        .I1(\pixel_bus[15]_i_45_n_0 ),
        .O(\pixel_bus_reg[15]_i_25_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[15]_i_26 
       (.I0(\pixel_bus[15]_i_46_n_0 ),
        .I1(\pixel_bus[15]_i_47_n_0 ),
        .O(\pixel_bus_reg[15]_i_26_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[15]_i_27 
       (.I0(\pixel_bus[15]_i_48_n_0 ),
        .I1(\pixel_bus[15]_i_49_n_0 ),
        .O(\pixel_bus_reg[15]_i_27_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[15]_i_28 
       (.I0(\pixel_bus[15]_i_50_n_0 ),
        .I1(\pixel_bus[15]_i_51_n_0 ),
        .O(\pixel_bus_reg[15]_i_28_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[15]_i_29 
       (.I0(\pixel_bus[15]_i_52_n_0 ),
        .I1(\pixel_bus[15]_i_53_n_0 ),
        .O(\pixel_bus_reg[15]_i_29_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[15]_i_3 
       (.I0(p_0_in12_in),
        .I1(p_0_in21_in),
        .O(\pixel_bus_reg[15]_i_3_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[15]_i_30 
       (.I0(\pixel_bus[15]_i_54_n_0 ),
        .I1(\pixel_bus[15]_i_55_n_0 ),
        .O(\pixel_bus_reg[15]_i_30_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[15]_i_31 
       (.I0(\pixel_bus[15]_i_56_n_0 ),
        .I1(\pixel_bus[15]_i_57_n_0 ),
        .O(\pixel_bus_reg[15]_i_31_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[15]_i_32 
       (.I0(\pixel_bus[15]_i_58_n_0 ),
        .I1(\pixel_bus[15]_i_59_n_0 ),
        .O(\pixel_bus_reg[15]_i_32_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[15]_i_33 
       (.I0(\pixel_bus[15]_i_60_n_0 ),
        .I1(\pixel_bus[15]_i_61_n_0 ),
        .O(\pixel_bus_reg[15]_i_33_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[15]_i_34 
       (.I0(\pixel_bus[15]_i_62_n_0 ),
        .I1(\pixel_bus[15]_i_63_n_0 ),
        .O(\pixel_bus_reg[15]_i_34_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[15]_i_35 
       (.I0(\pixel_bus[15]_i_64_n_0 ),
        .I1(\pixel_bus[15]_i_65_n_0 ),
        .O(\pixel_bus_reg[15]_i_35_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[15]_i_36 
       (.I0(\pixel_bus[15]_i_66_n_0 ),
        .I1(\pixel_bus[15]_i_67_n_0 ),
        .O(\pixel_bus_reg[15]_i_36_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[15]_i_37 
       (.I0(\pixel_bus[15]_i_68_n_0 ),
        .I1(\pixel_bus[15]_i_69_n_0 ),
        .O(\pixel_bus_reg[15]_i_37_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[15]_i_4 
       (.I0(p_1_in13_in),
        .I1(p_1_in22_in),
        .O(\pixel_bus_reg[15]_i_4_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \pixel_bus_reg[2] 
       (.C(pixel_clk),
        .CE(render_enable),
        .D(\pixel_bus[2]_i_1_n_0 ),
        .Q(pixel_bus[0]),
        .R(1'b0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \pixel_bus_reg[3] 
       (.C(pixel_clk),
        .CE(render_enable),
        .D(\pixel_bus[3]_i_1_n_0 ),
        .Q(pixel_bus[1]),
        .R(1'b0));
  MUXF7 \pixel_bus_reg[3]_i_4 
       (.I0(\pixel_bus[14]_i_7_n_0 ),
        .I1(\pixel_bus[14]_i_6_n_0 ),
        .O(p_1_in19_in),
        .S(tiles_reg_0_63_3_3_n_0));
  MUXF7 \pixel_bus_reg[3]_i_5 
       (.I0(\pixel_bus[14]_i_9_n_0 ),
        .I1(\pixel_bus[14]_i_8_n_0 ),
        .O(p_1_in10_in),
        .S(tiles_reg_0_63_3_3_n_0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \pixel_bus_reg[4] 
       (.C(pixel_clk),
        .CE(render_enable),
        .D(\pixel_bus[4]_i_1_n_0 ),
        .Q(pixel_bus[2]),
        .R(1'b0));
  MUXF7 \pixel_bus_reg[4]_i_10 
       (.I0(\pixel_bus[4]_i_20_n_0 ),
        .I1(\pixel_bus[4]_i_21_n_0 ),
        .O(\pixel_bus_reg[4]_i_10_n_0 ),
        .S(tiles_reg_0_63_3_3_n_0));
  MUXF7 \pixel_bus_reg[4]_i_11 
       (.I0(\pixel_bus[4]_i_22_n_0 ),
        .I1(\pixel_bus[4]_i_23_n_0 ),
        .O(p_0_in6_in),
        .S(tiles_reg_0_63_3_3_n_0));
  MUXF7 \pixel_bus_reg[4]_i_12 
       (.I0(\pixel_bus[4]_i_24_n_0 ),
        .I1(\pixel_bus[4]_i_25_n_0 ),
        .O(\pixel_bus_reg[4]_i_12_n_0 ),
        .S(tiles_reg_0_63_3_3_n_0));
  MUXF7 \pixel_bus_reg[4]_i_13 
       (.I0(\pixel_bus[4]_i_26_n_0 ),
        .I1(\pixel_bus[4]_i_27_n_0 ),
        .O(p_0_in15_in),
        .S(tiles_reg_0_63_3_3_n_0));
  MUXF7 \pixel_bus_reg[4]_i_30 
       (.I0(\pixel_bus[4]_i_56_n_0 ),
        .I1(\pixel_bus[4]_i_57_n_0 ),
        .O(\pixel_bus_reg[4]_i_30_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[4]_i_31 
       (.I0(\pixel_bus[4]_i_58_n_0 ),
        .I1(\pixel_bus[4]_i_59_n_0 ),
        .O(\pixel_bus_reg[4]_i_31_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[4]_i_32 
       (.I0(\pixel_bus[4]_i_60_n_0 ),
        .I1(\pixel_bus[4]_i_61_n_0 ),
        .O(\pixel_bus_reg[4]_i_32_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[4]_i_33 
       (.I0(\pixel_bus[4]_i_62_n_0 ),
        .I1(\pixel_bus[4]_i_63_n_0 ),
        .O(\pixel_bus_reg[4]_i_33_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[4]_i_34 
       (.I0(\pixel_bus[4]_i_64_n_0 ),
        .I1(\pixel_bus[4]_i_65_n_0 ),
        .O(\pixel_bus_reg[4]_i_34_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[4]_i_35 
       (.I0(\pixel_bus[4]_i_66_n_0 ),
        .I1(\pixel_bus[4]_i_67_n_0 ),
        .O(\pixel_bus_reg[4]_i_35_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[4]_i_36 
       (.I0(\pixel_bus[4]_i_68_n_0 ),
        .I1(\pixel_bus[4]_i_69_n_0 ),
        .O(\pixel_bus_reg[4]_i_36_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[4]_i_37 
       (.I0(\pixel_bus[4]_i_70_n_0 ),
        .I1(\pixel_bus[4]_i_71_n_0 ),
        .O(\pixel_bus_reg[4]_i_37_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[4]_i_38 
       (.I0(\pixel_bus[4]_i_72_n_0 ),
        .I1(\pixel_bus[4]_i_73_n_0 ),
        .O(\pixel_bus_reg[4]_i_38_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[4]_i_39 
       (.I0(\pixel_bus[4]_i_74_n_0 ),
        .I1(\pixel_bus[4]_i_75_n_0 ),
        .O(\pixel_bus_reg[4]_i_39_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[4]_i_40 
       (.I0(\pixel_bus[4]_i_76_n_0 ),
        .I1(\pixel_bus[4]_i_77_n_0 ),
        .O(\pixel_bus_reg[4]_i_40_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[4]_i_41 
       (.I0(\pixel_bus[4]_i_78_n_0 ),
        .I1(\pixel_bus[4]_i_79_n_0 ),
        .O(\pixel_bus_reg[4]_i_41_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[4]_i_42 
       (.I0(\pixel_bus[4]_i_80_n_0 ),
        .I1(\pixel_bus[4]_i_81_n_0 ),
        .O(\pixel_bus_reg[4]_i_42_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[4]_i_43 
       (.I0(\pixel_bus[4]_i_82_n_0 ),
        .I1(\pixel_bus[4]_i_83_n_0 ),
        .O(\pixel_bus_reg[4]_i_43_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[4]_i_44 
       (.I0(\pixel_bus[4]_i_84_n_0 ),
        .I1(\pixel_bus[4]_i_85_n_0 ),
        .O(\pixel_bus_reg[4]_i_44_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[4]_i_45 
       (.I0(\pixel_bus[4]_i_86_n_0 ),
        .I1(\pixel_bus[4]_i_87_n_0 ),
        .O(\pixel_bus_reg[4]_i_45_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[4]_i_46 
       (.I0(\pixel_bus[4]_i_88_n_0 ),
        .I1(\pixel_bus[4]_i_89_n_0 ),
        .O(\pixel_bus_reg[4]_i_46_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[4]_i_47 
       (.I0(\pixel_bus[4]_i_90_n_0 ),
        .I1(\pixel_bus[4]_i_91_n_0 ),
        .O(\pixel_bus_reg[4]_i_47_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[4]_i_48 
       (.I0(\pixel_bus[4]_i_92_n_0 ),
        .I1(\pixel_bus[4]_i_93_n_0 ),
        .O(\pixel_bus_reg[4]_i_48_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[4]_i_49 
       (.I0(\pixel_bus[4]_i_94_n_0 ),
        .I1(\pixel_bus[4]_i_95_n_0 ),
        .O(\pixel_bus_reg[4]_i_49_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[4]_i_50 
       (.I0(\pixel_bus[4]_i_96_n_0 ),
        .I1(\pixel_bus[4]_i_97_n_0 ),
        .O(\pixel_bus_reg[4]_i_50_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[4]_i_51 
       (.I0(\pixel_bus[4]_i_98_n_0 ),
        .I1(\pixel_bus[4]_i_99_n_0 ),
        .O(\pixel_bus_reg[4]_i_51_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[4]_i_52 
       (.I0(\pixel_bus[4]_i_100_n_0 ),
        .I1(\pixel_bus[4]_i_101_n_0 ),
        .O(\pixel_bus_reg[4]_i_52_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[4]_i_53 
       (.I0(\pixel_bus[4]_i_102_n_0 ),
        .I1(\pixel_bus[4]_i_103_n_0 ),
        .O(\pixel_bus_reg[4]_i_53_n_0 ),
        .S(tiles_reg_0_63_0_2_n_0));
  MUXF7 \pixel_bus_reg[4]_i_8 
       (.I0(\pixel_bus[4]_i_16_n_0 ),
        .I1(\pixel_bus[4]_i_17_n_0 ),
        .O(p_0_in9_in),
        .S(tiles_reg_0_63_3_3_n_0));
  MUXF7 \pixel_bus_reg[4]_i_9 
       (.I0(\pixel_bus[4]_i_18_n_0 ),
        .I1(\pixel_bus[4]_i_19_n_0 ),
        .O(p_0_in18_in),
        .S(tiles_reg_0_63_3_3_n_0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \pixel_bus_reg[7] 
       (.C(pixel_clk),
        .CE(render_enable),
        .D(\pixel_bus[7]_i_1_n_0 ),
        .Q(pixel_bus[3]),
        .R(render_enable_reg_0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \pixel_bus_reg[8] 
       (.C(pixel_clk),
        .CE(render_enable),
        .D(\pixel_bus[8]_i_1_n_0 ),
        .Q(pixel_bus[4]),
        .R(render_enable_reg_0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    \pixel_bus_reg[9] 
       (.C(pixel_clk),
        .CE(render_enable),
        .D(\pixel_bus[9]_i_1_n_0 ),
        .Q(pixel_bus[5]),
        .R(render_enable_reg_0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \sprite_x_rev_reg[0] 
       (.C(pixel_clk),
        .CE(render_enable),
        .D(sprites_data_reg_r3_2048_2111_0_2_i_4_n_0),
        .Q(\sprite_x_rev_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \sprite_x_rev_reg[1] 
       (.C(pixel_clk),
        .CE(render_enable),
        .D(sprites_data_reg_r3_2048_2111_0_2_i_3_n_0),
        .Q(\sprite_x_rev_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \sprite_x_rev_reg[2] 
       (.C(pixel_clk),
        .CE(render_enable),
        .D(sprites_data_reg_r3_2048_2111_0_2_i_2_n_0),
        .Q(\sprite_x_rev_reg_n_0_[2] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \sprite_x_rev_reg[3] 
       (.C(pixel_clk),
        .CE(render_enable),
        .D(sprites_data_reg_r3_2048_2111_0_2_i_1_n_0),
        .Q(\sprite_x_rev_reg_n_0_[3] ),
        .R(1'b0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r1_0_63_0_2
       (.ADDRA(ADDRC),
        .ADDRB(ADDRC),
        .ADDRC(ADDRC),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r1_0_63_0_2_n_0),
        .DOB(sprites_data_reg_r1_0_63_0_2_n_1),
        .DOC(sprites_data_reg_r1_0_63_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r1_0_63_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r1_0_63_0_2_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000000000001000)) 
    sprites_data_reg_r1_0_63_0_2_i_2
       (.I0(O34),
        .I1(ind_reg[10]),
        .I2(fetching_sprites),
        .I3(sprites_data_reg_r1_0_63_0_2_i_3_n_0),
        .I4(ind_reg[9]),
        .I5(ind_reg[8]),
        .O(sprites_data_reg_r1_0_63_0_2_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT2 #(
    .INIT(4'h1)) 
    sprites_data_reg_r1_0_63_0_2_i_3
       (.I0(ind_reg[7]),
        .I1(ind_reg[6]),
        .O(sprites_data_reg_r1_0_63_0_2_i_3_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r1_0_63_3_5
       (.ADDRA(ADDRC),
        .ADDRB(ADDRC),
        .ADDRC(ADDRC),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r1_0_63_3_5_n_0),
        .DOB(sprites_data_reg_r1_0_63_3_5_n_1),
        .DOC(sprites_data_reg_r1_0_63_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r1_0_63_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r1_0_63_0_2_i_2_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r1_128_191_0_2
       (.ADDRA(ADDRC),
        .ADDRB(ADDRC),
        .ADDRC(ADDRC),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r1_128_191_0_2_n_0),
        .DOB(sprites_data_reg_r1_128_191_0_2_n_1),
        .DOC(sprites_data_reg_r1_128_191_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r1_128_191_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r1_128_191_0_2_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000002000)) 
    sprites_data_reg_r1_128_191_0_2_i_1
       (.I0(ind_reg[7]),
        .I1(O34),
        .I2(fetching_sprites),
        .I3(sprites_data_reg_r1_128_191_0_2_i_2_n_0),
        .I4(ind_reg[10]),
        .I5(ind_reg[9]),
        .O(sprites_data_reg_r1_128_191_0_2_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT2 #(
    .INIT(4'h1)) 
    sprites_data_reg_r1_128_191_0_2_i_2
       (.I0(ind_reg[8]),
        .I1(ind_reg[6]),
        .O(sprites_data_reg_r1_128_191_0_2_i_2_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r1_128_191_3_5
       (.ADDRA(ADDRC),
        .ADDRB(ADDRC),
        .ADDRC(ADDRC),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r1_128_191_3_5_n_0),
        .DOB(sprites_data_reg_r1_128_191_3_5_n_1),
        .DOC(sprites_data_reg_r1_128_191_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r1_128_191_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r1_128_191_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r1_192_255_0_2
       (.ADDRA(ADDRC),
        .ADDRB(ADDRC),
        .ADDRC(ADDRC),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r1_192_255_0_2_n_0),
        .DOB(sprites_data_reg_r1_192_255_0_2_n_1),
        .DOC(sprites_data_reg_r1_192_255_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r1_192_255_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r1_192_255_0_2_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    sprites_data_reg_r1_192_255_0_2_i_1
       (.I0(ind_reg[7]),
        .I1(ind_reg[6]),
        .I2(fetching_sprites_reg),
        .I3(ind_reg[9]),
        .I4(ind_reg[8]),
        .I5(sprites_data_reg_r1_192_255_0_2_i_2_n_0),
        .O(sprites_data_reg_r1_192_255_0_2_i_1_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    sprites_data_reg_r1_192_255_0_2_i_2
       (.I0(O34),
        .I1(ind_reg[10]),
        .O(sprites_data_reg_r1_192_255_0_2_i_2_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r1_192_255_3_5
       (.ADDRA(ADDRC),
        .ADDRB(ADDRC),
        .ADDRC(ADDRC),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r1_192_255_3_5_n_0),
        .DOB(sprites_data_reg_r1_192_255_3_5_n_1),
        .DOC(sprites_data_reg_r1_192_255_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r1_192_255_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r1_192_255_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r1_64_127_0_2
       (.ADDRA(ADDRC),
        .ADDRB(ADDRC),
        .ADDRC(ADDRC),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r1_64_127_0_2_n_0),
        .DOB(sprites_data_reg_r1_64_127_0_2_n_1),
        .DOC(sprites_data_reg_r1_64_127_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r1_64_127_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r1_64_127_0_2_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000002000)) 
    sprites_data_reg_r1_64_127_0_2_i_1
       (.I0(ind_reg[6]),
        .I1(O34),
        .I2(fetching_sprites),
        .I3(sprites_data_reg_r1_64_127_0_2_i_2_n_0),
        .I4(ind_reg[10]),
        .I5(ind_reg[9]),
        .O(sprites_data_reg_r1_64_127_0_2_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT2 #(
    .INIT(4'h1)) 
    sprites_data_reg_r1_64_127_0_2_i_2
       (.I0(ind_reg[8]),
        .I1(ind_reg[7]),
        .O(sprites_data_reg_r1_64_127_0_2_i_2_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r1_64_127_3_5
       (.ADDRA(ADDRC),
        .ADDRB(ADDRC),
        .ADDRC(ADDRC),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r1_64_127_3_5_n_0),
        .DOB(sprites_data_reg_r1_64_127_3_5_n_1),
        .DOC(sprites_data_reg_r1_64_127_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r1_64_127_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r1_64_127_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_0_63_0_2
       (.ADDRA(\v_cnt_reg[1]_rep ),
        .ADDRB(\v_cnt_reg[1]_rep ),
        .ADDRC(\v_cnt_reg[1]_rep ),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_0_63_0_2_n_0),
        .DOB(sprites_data_reg_r2_0_63_0_2_n_1),
        .DOC(sprites_data_reg_r2_0_63_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r2_0_63_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r1_0_63_0_2_i_2_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_0_63_3_5
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC({ADDRB,ADDRC[3:0]}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_0_63_3_5_n_0),
        .DOB(sprites_data_reg_r2_0_63_3_5_n_1),
        .DOC(sprites_data_reg_r2_0_63_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r2_0_63_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r1_0_63_0_2_i_2_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_1024_1087_0_2
       (.ADDRA(\v_cnt_reg[1]_rep ),
        .ADDRB(\v_cnt_reg[1]_rep ),
        .ADDRC(\v_cnt_reg[1]_rep ),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_1024_1087_0_2_n_0),
        .DOB(sprites_data_reg_r2_1024_1087_0_2_n_1),
        .DOC(sprites_data_reg_r2_1024_1087_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r2_1024_1087_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1024_1087_0_2_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000002000)) 
    sprites_data_reg_r2_1024_1087_0_2_i_1
       (.I0(ind_reg[10]),
        .I1(O34),
        .I2(fetching_sprites),
        .I3(sprites_data_reg_r1_0_63_0_2_i_3_n_0),
        .I4(ind_reg[9]),
        .I5(ind_reg[8]),
        .O(sprites_data_reg_r2_1024_1087_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_1024_1087_3_5
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC({ADDRB,ADDRC[3:0]}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_1024_1087_3_5_n_0),
        .DOB(sprites_data_reg_r2_1024_1087_3_5_n_1),
        .DOC(sprites_data_reg_r2_1024_1087_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r2_1024_1087_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1024_1087_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_1088_1151_0_2
       (.ADDRA(\v_cnt_reg[1]_rep ),
        .ADDRB(\v_cnt_reg[1]_rep ),
        .ADDRC(\v_cnt_reg[1]_rep ),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_1088_1151_0_2_n_0),
        .DOB(sprites_data_reg_r2_1088_1151_0_2_n_1),
        .DOC(sprites_data_reg_r2_1088_1151_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r2_1088_1151_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1088_1151_0_2_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    sprites_data_reg_r2_1088_1151_0_2_i_1
       (.I0(ind_reg[10]),
        .I1(ind_reg[6]),
        .I2(fetching_sprites),
        .I3(sprites_data_reg_r1_64_127_0_2_i_2_n_0),
        .I4(O34),
        .I5(ind_reg[9]),
        .O(sprites_data_reg_r2_1088_1151_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_1088_1151_3_5
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC({ADDRB,ADDRC[3:0]}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_1088_1151_3_5_n_0),
        .DOB(sprites_data_reg_r2_1088_1151_3_5_n_1),
        .DOC(sprites_data_reg_r2_1088_1151_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r2_1088_1151_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1088_1151_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_1152_1215_0_2
       (.ADDRA(\v_cnt_reg[1]_rep ),
        .ADDRB(\v_cnt_reg[1]_rep ),
        .ADDRC(\v_cnt_reg[1]_rep ),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_1152_1215_0_2_n_0),
        .DOB(sprites_data_reg_r2_1152_1215_0_2_n_1),
        .DOC(sprites_data_reg_r2_1152_1215_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r2_1152_1215_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1152_1215_0_2_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    sprites_data_reg_r2_1152_1215_0_2_i_1
       (.I0(ind_reg[10]),
        .I1(ind_reg[7]),
        .I2(fetching_sprites),
        .I3(sprites_data_reg_r1_128_191_0_2_i_2_n_0),
        .I4(O34),
        .I5(ind_reg[9]),
        .O(sprites_data_reg_r2_1152_1215_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_1152_1215_3_5
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC({ADDRB,ADDRC[3:0]}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_1152_1215_3_5_n_0),
        .DOB(sprites_data_reg_r2_1152_1215_3_5_n_1),
        .DOC(sprites_data_reg_r2_1152_1215_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r2_1152_1215_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1152_1215_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_1216_1279_0_2
       (.ADDRA(\v_cnt_reg[1]_rep ),
        .ADDRB(\v_cnt_reg[1]_rep ),
        .ADDRC(\v_cnt_reg[1]_rep ),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_1216_1279_0_2_n_0),
        .DOB(sprites_data_reg_r2_1216_1279_0_2_n_1),
        .DOC(sprites_data_reg_r2_1216_1279_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r2_1216_1279_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1216_1279_0_2_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000001000000)) 
    sprites_data_reg_r2_1216_1279_0_2_i_1
       (.I0(ind_reg[8]),
        .I1(ind_reg[9]),
        .I2(O34),
        .I3(ind_reg[6]),
        .I4(ind_reg[7]),
        .I5(sprites_data_reg_r2_1216_1279_0_2_i_2_n_0),
        .O(sprites_data_reg_r2_1216_1279_0_2_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hBF)) 
    sprites_data_reg_r2_1216_1279_0_2_i_2
       (.I0(O34),
        .I1(fetching_sprites),
        .I2(ind_reg[10]),
        .O(sprites_data_reg_r2_1216_1279_0_2_i_2_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_1216_1279_3_5
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC({ADDRB,ADDRC[3:0]}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_1216_1279_3_5_n_0),
        .DOB(sprites_data_reg_r2_1216_1279_3_5_n_1),
        .DOC(sprites_data_reg_r2_1216_1279_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r2_1216_1279_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1216_1279_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_1280_1343_0_2
       (.ADDRA(\v_cnt_reg[1]_rep ),
        .ADDRB(\v_cnt_reg[1]_rep ),
        .ADDRC(\v_cnt_reg[1]_rep ),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_1280_1343_0_2_n_0),
        .DOB(sprites_data_reg_r2_1280_1343_0_2_n_1),
        .DOC(sprites_data_reg_r2_1280_1343_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r2_1280_1343_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1280_1343_0_2_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    sprites_data_reg_r2_1280_1343_0_2_i_1
       (.I0(ind_reg[10]),
        .I1(ind_reg[8]),
        .I2(fetching_sprites),
        .I3(sprites_data_reg_r1_0_63_0_2_i_3_n_0),
        .I4(O34),
        .I5(ind_reg[9]),
        .O(sprites_data_reg_r2_1280_1343_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_1280_1343_3_5
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC({ADDRB,ADDRC[3:0]}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_1280_1343_3_5_n_0),
        .DOB(sprites_data_reg_r2_1280_1343_3_5_n_1),
        .DOC(sprites_data_reg_r2_1280_1343_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r2_1280_1343_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1280_1343_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_128_191_0_2
       (.ADDRA(\v_cnt_reg[1]_rep ),
        .ADDRB(\v_cnt_reg[1]_rep ),
        .ADDRC(\v_cnt_reg[1]_rep ),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_128_191_0_2_n_0),
        .DOB(sprites_data_reg_r2_128_191_0_2_n_1),
        .DOC(sprites_data_reg_r2_128_191_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r2_128_191_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r1_128_191_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_128_191_3_5
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC({ADDRB,ADDRC[3:0]}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_128_191_3_5_n_0),
        .DOB(sprites_data_reg_r2_128_191_3_5_n_1),
        .DOC(sprites_data_reg_r2_128_191_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r2_128_191_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r1_128_191_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_1344_1407_0_2
       (.ADDRA(\v_cnt_reg[1]_rep ),
        .ADDRB(\v_cnt_reg[1]_rep ),
        .ADDRC(\v_cnt_reg[1]_rep ),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_1344_1407_0_2_n_0),
        .DOB(sprites_data_reg_r2_1344_1407_0_2_n_1),
        .DOC(sprites_data_reg_r2_1344_1407_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r2_1344_1407_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1344_1407_0_2_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000001000000)) 
    sprites_data_reg_r2_1344_1407_0_2_i_1
       (.I0(ind_reg[7]),
        .I1(ind_reg[9]),
        .I2(O34),
        .I3(ind_reg[6]),
        .I4(ind_reg[8]),
        .I5(sprites_data_reg_r2_1216_1279_0_2_i_2_n_0),
        .O(sprites_data_reg_r2_1344_1407_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_1344_1407_3_5
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC({ADDRB,ADDRC[3:0]}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_1344_1407_3_5_n_0),
        .DOB(sprites_data_reg_r2_1344_1407_3_5_n_1),
        .DOC(sprites_data_reg_r2_1344_1407_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r2_1344_1407_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1344_1407_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_1408_1471_0_2
       (.ADDRA(\v_cnt_reg[1]_rep ),
        .ADDRB(\v_cnt_reg[1]_rep ),
        .ADDRC(\v_cnt_reg[1]_rep ),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_1408_1471_0_2_n_0),
        .DOB(sprites_data_reg_r2_1408_1471_0_2_n_1),
        .DOC(sprites_data_reg_r2_1408_1471_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r2_1408_1471_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1408_1471_0_2_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000001000000)) 
    sprites_data_reg_r2_1408_1471_0_2_i_1
       (.I0(ind_reg[6]),
        .I1(ind_reg[9]),
        .I2(O34),
        .I3(ind_reg[7]),
        .I4(ind_reg[8]),
        .I5(sprites_data_reg_r2_1216_1279_0_2_i_2_n_0),
        .O(sprites_data_reg_r2_1408_1471_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_1408_1471_3_5
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC({ADDRB,ADDRC[3:0]}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_1408_1471_3_5_n_0),
        .DOB(sprites_data_reg_r2_1408_1471_3_5_n_1),
        .DOC(sprites_data_reg_r2_1408_1471_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r2_1408_1471_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1408_1471_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_1472_1535_0_2
       (.ADDRA(\v_cnt_reg[1]_rep ),
        .ADDRB(\v_cnt_reg[1]_rep ),
        .ADDRC(\v_cnt_reg[1]_rep ),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_1472_1535_0_2_n_0),
        .DOB(sprites_data_reg_r2_1472_1535_0_2_n_1),
        .DOC(sprites_data_reg_r2_1472_1535_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r2_1472_1535_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1472_1535_0_2_i_1_n_0));
  LUT6 #(
    .INIT(64'h0010000000000000)) 
    sprites_data_reg_r2_1472_1535_0_2_i_1
       (.I0(ind_reg[9]),
        .I1(O34),
        .I2(fetching_sprites),
        .I3(sprites_data_reg_r2_448_511_0_2_i_2_n_0),
        .I4(ind_reg[8]),
        .I5(ind_reg[10]),
        .O(sprites_data_reg_r2_1472_1535_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_1472_1535_3_5
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC({ADDRB,ADDRC[3:0]}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_1472_1535_3_5_n_0),
        .DOB(sprites_data_reg_r2_1472_1535_3_5_n_1),
        .DOC(sprites_data_reg_r2_1472_1535_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r2_1472_1535_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1472_1535_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_1536_1599_0_2
       (.ADDRA(\v_cnt_reg[1]_rep ),
        .ADDRB(\v_cnt_reg[1]_rep ),
        .ADDRC(\v_cnt_reg[1]_rep ),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_1536_1599_0_2_n_0),
        .DOB(sprites_data_reg_r2_1536_1599_0_2_n_1),
        .DOC(sprites_data_reg_r2_1536_1599_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r2_1536_1599_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1536_1599_0_2_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    sprites_data_reg_r2_1536_1599_0_2_i_1
       (.I0(ind_reg[10]),
        .I1(ind_reg[9]),
        .I2(fetching_sprites),
        .I3(sprites_data_reg_r1_0_63_0_2_i_3_n_0),
        .I4(O34),
        .I5(ind_reg[8]),
        .O(sprites_data_reg_r2_1536_1599_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_1536_1599_3_5
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC({ADDRB,ADDRC[3:0]}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_1536_1599_3_5_n_0),
        .DOB(sprites_data_reg_r2_1536_1599_3_5_n_1),
        .DOC(sprites_data_reg_r2_1536_1599_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r2_1536_1599_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1536_1599_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_1600_1663_0_2
       (.ADDRA(\v_cnt_reg[1]_rep ),
        .ADDRB(\v_cnt_reg[1]_rep ),
        .ADDRC(\v_cnt_reg[1]_rep ),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_1600_1663_0_2_n_0),
        .DOB(sprites_data_reg_r2_1600_1663_0_2_n_1),
        .DOC(sprites_data_reg_r2_1600_1663_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r2_1600_1663_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1600_1663_0_2_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000001000000)) 
    sprites_data_reg_r2_1600_1663_0_2_i_1
       (.I0(ind_reg[7]),
        .I1(ind_reg[8]),
        .I2(O34),
        .I3(ind_reg[6]),
        .I4(ind_reg[9]),
        .I5(sprites_data_reg_r2_1216_1279_0_2_i_2_n_0),
        .O(sprites_data_reg_r2_1600_1663_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_1600_1663_3_5
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC({ADDRB,ADDRC[3:0]}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_1600_1663_3_5_n_0),
        .DOB(sprites_data_reg_r2_1600_1663_3_5_n_1),
        .DOC(sprites_data_reg_r2_1600_1663_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r2_1600_1663_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1600_1663_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_1664_1727_0_2
       (.ADDRA(\v_cnt_reg[1]_rep ),
        .ADDRB(\v_cnt_reg[1]_rep ),
        .ADDRC(\v_cnt_reg[1]_rep ),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_1664_1727_0_2_n_0),
        .DOB(sprites_data_reg_r2_1664_1727_0_2_n_1),
        .DOC(sprites_data_reg_r2_1664_1727_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r2_1664_1727_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1664_1727_0_2_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000001000000)) 
    sprites_data_reg_r2_1664_1727_0_2_i_1
       (.I0(ind_reg[6]),
        .I1(ind_reg[8]),
        .I2(O34),
        .I3(ind_reg[7]),
        .I4(ind_reg[9]),
        .I5(sprites_data_reg_r2_1216_1279_0_2_i_2_n_0),
        .O(sprites_data_reg_r2_1664_1727_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_1664_1727_3_5
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC({ADDRB,ADDRC[3:0]}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_1664_1727_3_5_n_0),
        .DOB(sprites_data_reg_r2_1664_1727_3_5_n_1),
        .DOC(sprites_data_reg_r2_1664_1727_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r2_1664_1727_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1664_1727_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_1728_1791_0_2
       (.ADDRA(\v_cnt_reg[1]_rep ),
        .ADDRB(\v_cnt_reg[1]_rep ),
        .ADDRC(\v_cnt_reg[1]_rep ),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_1728_1791_0_2_n_0),
        .DOB(sprites_data_reg_r2_1728_1791_0_2_n_1),
        .DOC(sprites_data_reg_r2_1728_1791_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r2_1728_1791_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1728_1791_0_2_i_1_n_0));
  LUT6 #(
    .INIT(64'h0010000000000000)) 
    sprites_data_reg_r2_1728_1791_0_2_i_1
       (.I0(ind_reg[8]),
        .I1(O34),
        .I2(fetching_sprites),
        .I3(sprites_data_reg_r2_448_511_0_2_i_2_n_0),
        .I4(ind_reg[9]),
        .I5(ind_reg[10]),
        .O(sprites_data_reg_r2_1728_1791_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_1728_1791_3_5
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC({ADDRB,ADDRC[3:0]}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_1728_1791_3_5_n_0),
        .DOB(sprites_data_reg_r2_1728_1791_3_5_n_1),
        .DOC(sprites_data_reg_r2_1728_1791_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r2_1728_1791_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1728_1791_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_1792_1855_0_2
       (.ADDRA(\v_cnt_reg[1]_rep ),
        .ADDRB(\v_cnt_reg[1]_rep ),
        .ADDRC(\v_cnt_reg[1]_rep ),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_1792_1855_0_2_n_0),
        .DOB(sprites_data_reg_r2_1792_1855_0_2_n_1),
        .DOC(sprites_data_reg_r2_1792_1855_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r2_1792_1855_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1792_1855_0_2_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000001000000)) 
    sprites_data_reg_r2_1792_1855_0_2_i_1
       (.I0(ind_reg[6]),
        .I1(ind_reg[7]),
        .I2(O34),
        .I3(ind_reg[8]),
        .I4(ind_reg[9]),
        .I5(sprites_data_reg_r2_1216_1279_0_2_i_2_n_0),
        .O(sprites_data_reg_r2_1792_1855_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_1792_1855_3_5
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC({ADDRB,ADDRC[3:0]}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_1792_1855_3_5_n_0),
        .DOB(sprites_data_reg_r2_1792_1855_3_5_n_1),
        .DOC(sprites_data_reg_r2_1792_1855_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r2_1792_1855_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1792_1855_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_1856_1919_0_2
       (.ADDRA(\v_cnt_reg[1]_rep ),
        .ADDRB(\v_cnt_reg[1]_rep ),
        .ADDRC(\v_cnt_reg[1]_rep ),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_1856_1919_0_2_n_0),
        .DOB(sprites_data_reg_r2_1856_1919_0_2_n_1),
        .DOC(sprites_data_reg_r2_1856_1919_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r2_1856_1919_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1856_1919_0_2_i_1_n_0));
  LUT6 #(
    .INIT(64'h0010000000000000)) 
    sprites_data_reg_r2_1856_1919_0_2_i_1
       (.I0(ind_reg[7]),
        .I1(O34),
        .I2(fetching_sprites),
        .I3(sprites_data_reg_r2_1856_1919_0_2_i_2_n_0),
        .I4(ind_reg[9]),
        .I5(ind_reg[10]),
        .O(sprites_data_reg_r2_1856_1919_0_2_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT2 #(
    .INIT(4'h7)) 
    sprites_data_reg_r2_1856_1919_0_2_i_2
       (.I0(ind_reg[6]),
        .I1(ind_reg[8]),
        .O(sprites_data_reg_r2_1856_1919_0_2_i_2_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_1856_1919_3_5
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC({ADDRB,ADDRC[3:0]}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_1856_1919_3_5_n_0),
        .DOB(sprites_data_reg_r2_1856_1919_3_5_n_1),
        .DOC(sprites_data_reg_r2_1856_1919_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r2_1856_1919_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1856_1919_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_1920_1983_0_2
       (.ADDRA(\v_cnt_reg[1]_rep ),
        .ADDRB(\v_cnt_reg[1]_rep ),
        .ADDRC(\v_cnt_reg[1]_rep ),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_1920_1983_0_2_n_0),
        .DOB(sprites_data_reg_r2_1920_1983_0_2_n_1),
        .DOC(sprites_data_reg_r2_1920_1983_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r2_1920_1983_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1920_1983_0_2_i_1_n_0));
  LUT6 #(
    .INIT(64'h0010000000000000)) 
    sprites_data_reg_r2_1920_1983_0_2_i_1
       (.I0(ind_reg[6]),
        .I1(O34),
        .I2(fetching_sprites),
        .I3(sprites_data_reg_r2_1920_1983_0_2_i_2_n_0),
        .I4(ind_reg[9]),
        .I5(ind_reg[10]),
        .O(sprites_data_reg_r2_1920_1983_0_2_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT2 #(
    .INIT(4'h7)) 
    sprites_data_reg_r2_1920_1983_0_2_i_2
       (.I0(ind_reg[7]),
        .I1(ind_reg[8]),
        .O(sprites_data_reg_r2_1920_1983_0_2_i_2_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_1920_1983_3_5
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC({ADDRB,ADDRC[3:0]}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_1920_1983_3_5_n_0),
        .DOB(sprites_data_reg_r2_1920_1983_3_5_n_1),
        .DOC(sprites_data_reg_r2_1920_1983_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r2_1920_1983_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1920_1983_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_192_255_0_2
       (.ADDRA(\v_cnt_reg[1]_rep ),
        .ADDRB(\v_cnt_reg[1]_rep ),
        .ADDRC(\v_cnt_reg[1]_rep ),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_192_255_0_2_n_0),
        .DOB(sprites_data_reg_r2_192_255_0_2_n_1),
        .DOC(sprites_data_reg_r2_192_255_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r2_192_255_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r1_192_255_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_192_255_3_5
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC({ADDRB,ADDRC[3:0]}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_192_255_3_5_n_0),
        .DOB(sprites_data_reg_r2_192_255_3_5_n_1),
        .DOC(sprites_data_reg_r2_192_255_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r2_192_255_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r1_192_255_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_1984_2047_0_2
       (.ADDRA(\v_cnt_reg[1]_rep ),
        .ADDRB(\v_cnt_reg[1]_rep ),
        .ADDRC(\v_cnt_reg[1]_rep ),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_1984_2047_0_2_n_0),
        .DOB(sprites_data_reg_r2_1984_2047_0_2_n_1),
        .DOC(sprites_data_reg_r2_1984_2047_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r2_1984_2047_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1984_2047_0_2_i_1_n_0));
  LUT6 #(
    .INIT(64'h1000000000000000)) 
    sprites_data_reg_r2_1984_2047_0_2_i_1
       (.I0(sprites_data_reg_r2_1216_1279_0_2_i_2_n_0),
        .I1(O34),
        .I2(ind_reg[6]),
        .I3(ind_reg[7]),
        .I4(ind_reg[8]),
        .I5(ind_reg[9]),
        .O(sprites_data_reg_r2_1984_2047_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_1984_2047_3_5
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC({ADDRB,ADDRC[3:0]}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_1984_2047_3_5_n_0),
        .DOB(sprites_data_reg_r2_1984_2047_3_5_n_1),
        .DOC(sprites_data_reg_r2_1984_2047_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r2_1984_2047_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1984_2047_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_2048_2111_0_2
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC(ADDRC),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_2048_2111_0_2_n_0),
        .DOB(sprites_data_reg_r2_2048_2111_0_2_n_1),
        .DOC(sprites_data_reg_r2_2048_2111_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r2_2048_2111_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(1'b0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_2048_2111_3_5
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC({ADDRB,ADDRC[3:0]}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_2048_2111_3_5_n_0),
        .DOB(sprites_data_reg_r2_2048_2111_3_5_n_1),
        .DOC(sprites_data_reg_r2_2048_2111_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r2_2048_2111_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(1'b0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_256_319_0_2
       (.ADDRA(\v_cnt_reg[1]_rep ),
        .ADDRB(\v_cnt_reg[1]_rep ),
        .ADDRC(\v_cnt_reg[1]_rep ),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_256_319_0_2_n_0),
        .DOB(sprites_data_reg_r2_256_319_0_2_n_1),
        .DOC(sprites_data_reg_r2_256_319_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r2_256_319_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_256_319_0_2_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000002000)) 
    sprites_data_reg_r2_256_319_0_2_i_1
       (.I0(ind_reg[8]),
        .I1(O34),
        .I2(fetching_sprites),
        .I3(sprites_data_reg_r1_0_63_0_2_i_3_n_0),
        .I4(ind_reg[10]),
        .I5(ind_reg[9]),
        .O(sprites_data_reg_r2_256_319_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_256_319_3_5
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC({ADDRB,ADDRC[3:0]}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_256_319_3_5_n_0),
        .DOB(sprites_data_reg_r2_256_319_3_5_n_1),
        .DOC(sprites_data_reg_r2_256_319_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r2_256_319_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_256_319_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_320_383_0_2
       (.ADDRA(\v_cnt_reg[1]_rep ),
        .ADDRB(\v_cnt_reg[1]_rep ),
        .ADDRC(\v_cnt_reg[1]_rep ),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_320_383_0_2_n_0),
        .DOB(sprites_data_reg_r2_320_383_0_2_n_1),
        .DOC(sprites_data_reg_r2_320_383_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r2_320_383_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_320_383_0_2_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    sprites_data_reg_r2_320_383_0_2_i_1
       (.I0(ind_reg[8]),
        .I1(ind_reg[6]),
        .I2(fetching_sprites_reg),
        .I3(ind_reg[9]),
        .I4(ind_reg[7]),
        .I5(sprites_data_reg_r1_192_255_0_2_i_2_n_0),
        .O(sprites_data_reg_r2_320_383_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_320_383_3_5
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC({ADDRB,ADDRC[3:0]}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_320_383_3_5_n_0),
        .DOB(sprites_data_reg_r2_320_383_3_5_n_1),
        .DOC(sprites_data_reg_r2_320_383_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r2_320_383_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_320_383_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_384_447_0_2
       (.ADDRA(\v_cnt_reg[1]_rep ),
        .ADDRB(\v_cnt_reg[1]_rep ),
        .ADDRC(\v_cnt_reg[1]_rep ),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_384_447_0_2_n_0),
        .DOB(sprites_data_reg_r2_384_447_0_2_n_1),
        .DOC(sprites_data_reg_r2_384_447_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r2_384_447_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_384_447_0_2_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    sprites_data_reg_r2_384_447_0_2_i_1
       (.I0(ind_reg[8]),
        .I1(ind_reg[7]),
        .I2(fetching_sprites_reg),
        .I3(ind_reg[9]),
        .I4(ind_reg[6]),
        .I5(sprites_data_reg_r1_192_255_0_2_i_2_n_0),
        .O(sprites_data_reg_r2_384_447_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_384_447_3_5
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC({ADDRB,ADDRC[3:0]}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_384_447_3_5_n_0),
        .DOB(sprites_data_reg_r2_384_447_3_5_n_1),
        .DOC(sprites_data_reg_r2_384_447_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r2_384_447_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_384_447_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_448_511_0_2
       (.ADDRA(\v_cnt_reg[1]_rep ),
        .ADDRB(\v_cnt_reg[1]_rep ),
        .ADDRC(\v_cnt_reg[1]_rep ),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_448_511_0_2_n_0),
        .DOB(sprites_data_reg_r2_448_511_0_2_n_1),
        .DOC(sprites_data_reg_r2_448_511_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r2_448_511_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_448_511_0_2_i_1_n_0));
  LUT6 #(
    .INIT(64'h0001000000000000)) 
    sprites_data_reg_r2_448_511_0_2_i_1
       (.I0(ind_reg[9]),
        .I1(ind_reg[10]),
        .I2(sprites_data_reg_r2_448_511_0_2_i_2_n_0),
        .I3(O34),
        .I4(fetching_sprites),
        .I5(ind_reg[8]),
        .O(sprites_data_reg_r2_448_511_0_2_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT2 #(
    .INIT(4'h7)) 
    sprites_data_reg_r2_448_511_0_2_i_2
       (.I0(ind_reg[6]),
        .I1(ind_reg[7]),
        .O(sprites_data_reg_r2_448_511_0_2_i_2_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_448_511_3_5
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC({ADDRB,ADDRC[3:0]}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_448_511_3_5_n_0),
        .DOB(sprites_data_reg_r2_448_511_3_5_n_1),
        .DOC(sprites_data_reg_r2_448_511_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r2_448_511_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_448_511_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_512_575_0_2
       (.ADDRA(\v_cnt_reg[1]_rep ),
        .ADDRB(\v_cnt_reg[1]_rep ),
        .ADDRC(\v_cnt_reg[1]_rep ),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_512_575_0_2_n_0),
        .DOB(sprites_data_reg_r2_512_575_0_2_n_1),
        .DOC(sprites_data_reg_r2_512_575_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r2_512_575_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_512_575_0_2_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000002000)) 
    sprites_data_reg_r2_512_575_0_2_i_1
       (.I0(ind_reg[9]),
        .I1(O34),
        .I2(fetching_sprites),
        .I3(sprites_data_reg_r1_0_63_0_2_i_3_n_0),
        .I4(ind_reg[10]),
        .I5(ind_reg[8]),
        .O(sprites_data_reg_r2_512_575_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_512_575_3_5
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC({ADDRB,ADDRC[3:0]}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_512_575_3_5_n_0),
        .DOB(sprites_data_reg_r2_512_575_3_5_n_1),
        .DOC(sprites_data_reg_r2_512_575_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r2_512_575_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_512_575_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_576_639_0_2
       (.ADDRA(\v_cnt_reg[1]_rep ),
        .ADDRB(\v_cnt_reg[1]_rep ),
        .ADDRC(\v_cnt_reg[1]_rep ),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_576_639_0_2_n_0),
        .DOB(sprites_data_reg_r2_576_639_0_2_n_1),
        .DOC(sprites_data_reg_r2_576_639_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r2_576_639_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_576_639_0_2_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    sprites_data_reg_r2_576_639_0_2_i_1
       (.I0(ind_reg[9]),
        .I1(ind_reg[6]),
        .I2(fetching_sprites_reg),
        .I3(ind_reg[8]),
        .I4(ind_reg[7]),
        .I5(sprites_data_reg_r1_192_255_0_2_i_2_n_0),
        .O(sprites_data_reg_r2_576_639_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_576_639_3_5
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC({ADDRB,ADDRC[3:0]}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_576_639_3_5_n_0),
        .DOB(sprites_data_reg_r2_576_639_3_5_n_1),
        .DOC(sprites_data_reg_r2_576_639_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r2_576_639_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_576_639_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_640_703_0_2
       (.ADDRA(\v_cnt_reg[1]_rep ),
        .ADDRB(\v_cnt_reg[1]_rep ),
        .ADDRC(\v_cnt_reg[1]_rep ),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_640_703_0_2_n_0),
        .DOB(sprites_data_reg_r2_640_703_0_2_n_1),
        .DOC(sprites_data_reg_r2_640_703_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r2_640_703_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_640_703_0_2_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000008000000000)) 
    sprites_data_reg_r2_640_703_0_2_i_1
       (.I0(ind_reg[9]),
        .I1(ind_reg[7]),
        .I2(fetching_sprites_reg),
        .I3(ind_reg[8]),
        .I4(ind_reg[6]),
        .I5(sprites_data_reg_r1_192_255_0_2_i_2_n_0),
        .O(sprites_data_reg_r2_640_703_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_640_703_3_5
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC({ADDRB,ADDRC[3:0]}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_640_703_3_5_n_0),
        .DOB(sprites_data_reg_r2_640_703_3_5_n_1),
        .DOC(sprites_data_reg_r2_640_703_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r2_640_703_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_640_703_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_64_127_0_2
       (.ADDRA(\v_cnt_reg[1]_rep ),
        .ADDRB(\v_cnt_reg[1]_rep ),
        .ADDRC(\v_cnt_reg[1]_rep ),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_64_127_0_2_n_0),
        .DOB(sprites_data_reg_r2_64_127_0_2_n_1),
        .DOC(sprites_data_reg_r2_64_127_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r2_64_127_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r1_64_127_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_64_127_3_5
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC({ADDRB,ADDRC[3:0]}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_64_127_3_5_n_0),
        .DOB(sprites_data_reg_r2_64_127_3_5_n_1),
        .DOC(sprites_data_reg_r2_64_127_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r2_64_127_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r1_64_127_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_704_767_0_2
       (.ADDRA(\v_cnt_reg[1]_rep ),
        .ADDRB(\v_cnt_reg[1]_rep ),
        .ADDRC(\v_cnt_reg[1]_rep ),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_704_767_0_2_n_0),
        .DOB(sprites_data_reg_r2_704_767_0_2_n_1),
        .DOC(sprites_data_reg_r2_704_767_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r2_704_767_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_704_767_0_2_i_1_n_0));
  LUT6 #(
    .INIT(64'h0001000000000000)) 
    sprites_data_reg_r2_704_767_0_2_i_1
       (.I0(ind_reg[8]),
        .I1(ind_reg[10]),
        .I2(sprites_data_reg_r2_448_511_0_2_i_2_n_0),
        .I3(O34),
        .I4(fetching_sprites),
        .I5(ind_reg[9]),
        .O(sprites_data_reg_r2_704_767_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_704_767_3_5
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC({ADDRB,ADDRC[3:0]}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_704_767_3_5_n_0),
        .DOB(sprites_data_reg_r2_704_767_3_5_n_1),
        .DOC(sprites_data_reg_r2_704_767_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r2_704_767_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_704_767_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_768_831_0_2
       (.ADDRA(\v_cnt_reg[1]_rep ),
        .ADDRB(\v_cnt_reg[1]_rep ),
        .ADDRC(\v_cnt_reg[1]_rep ),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_768_831_0_2_n_0),
        .DOB(sprites_data_reg_r2_768_831_0_2_n_1),
        .DOC(sprites_data_reg_r2_768_831_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r2_768_831_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_768_831_0_2_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000000008000)) 
    sprites_data_reg_r2_768_831_0_2_i_1
       (.I0(ind_reg[9]),
        .I1(ind_reg[8]),
        .I2(fetching_sprites),
        .I3(sprites_data_reg_r1_0_63_0_2_i_3_n_0),
        .I4(O34),
        .I5(ind_reg[10]),
        .O(sprites_data_reg_r2_768_831_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_768_831_3_5
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC({ADDRB,ADDRC[3:0]}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_768_831_3_5_n_0),
        .DOB(sprites_data_reg_r2_768_831_3_5_n_1),
        .DOC(sprites_data_reg_r2_768_831_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r2_768_831_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_768_831_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_832_895_0_2
       (.ADDRA(\v_cnt_reg[1]_rep ),
        .ADDRB(\v_cnt_reg[1]_rep ),
        .ADDRC(\v_cnt_reg[1]_rep ),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_832_895_0_2_n_0),
        .DOB(sprites_data_reg_r2_832_895_0_2_n_1),
        .DOC(sprites_data_reg_r2_832_895_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r2_832_895_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_832_895_0_2_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000001000000)) 
    sprites_data_reg_r2_832_895_0_2_i_1
       (.I0(ind_reg[10]),
        .I1(ind_reg[7]),
        .I2(O34),
        .I3(ind_reg[6]),
        .I4(ind_reg[8]),
        .I5(sprites_data_reg_r2_832_895_0_2_i_2_n_0),
        .O(sprites_data_reg_r2_832_895_0_2_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'hBF)) 
    sprites_data_reg_r2_832_895_0_2_i_2
       (.I0(O34),
        .I1(fetching_sprites),
        .I2(ind_reg[9]),
        .O(sprites_data_reg_r2_832_895_0_2_i_2_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_832_895_3_5
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC({ADDRB,ADDRC[3:0]}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_832_895_3_5_n_0),
        .DOB(sprites_data_reg_r2_832_895_3_5_n_1),
        .DOC(sprites_data_reg_r2_832_895_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r2_832_895_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_832_895_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_896_959_0_2
       (.ADDRA(\v_cnt_reg[1]_rep ),
        .ADDRB(\v_cnt_reg[1]_rep ),
        .ADDRC(\v_cnt_reg[1]_rep ),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_896_959_0_2_n_0),
        .DOB(sprites_data_reg_r2_896_959_0_2_n_1),
        .DOC(sprites_data_reg_r2_896_959_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r2_896_959_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_896_959_0_2_i_1_n_0));
  LUT6 #(
    .INIT(64'h0000000001000000)) 
    sprites_data_reg_r2_896_959_0_2_i_1
       (.I0(ind_reg[10]),
        .I1(ind_reg[6]),
        .I2(O34),
        .I3(ind_reg[7]),
        .I4(ind_reg[8]),
        .I5(sprites_data_reg_r2_832_895_0_2_i_2_n_0),
        .O(sprites_data_reg_r2_896_959_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_896_959_3_5
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC({ADDRB,ADDRC[3:0]}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_896_959_3_5_n_0),
        .DOB(sprites_data_reg_r2_896_959_3_5_n_1),
        .DOC(sprites_data_reg_r2_896_959_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r2_896_959_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_896_959_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_960_1023_0_2
       (.ADDRA(\v_cnt_reg[1]_rep ),
        .ADDRB(\v_cnt_reg[1]_rep ),
        .ADDRC(\v_cnt_reg[1]_rep ),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_960_1023_0_2_n_0),
        .DOB(sprites_data_reg_r2_960_1023_0_2_n_1),
        .DOC(sprites_data_reg_r2_960_1023_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r2_960_1023_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_960_1023_0_2_i_1_n_0));
  LUT6 #(
    .INIT(64'h0010000000000000)) 
    sprites_data_reg_r2_960_1023_0_2_i_1
       (.I0(ind_reg[10]),
        .I1(O34),
        .I2(fetching_sprites),
        .I3(sprites_data_reg_r2_448_511_0_2_i_2_n_0),
        .I4(ind_reg[8]),
        .I5(ind_reg[9]),
        .O(sprites_data_reg_r2_960_1023_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r2_960_1023_3_5
       (.ADDRA({ADDRB,ADDRC[3:0]}),
        .ADDRB({ADDRB,ADDRC[3:0]}),
        .ADDRC({ADDRB,ADDRC[3:0]}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r2_960_1023_3_5_n_0),
        .DOB(sprites_data_reg_r2_960_1023_3_5_n_1),
        .DOC(sprites_data_reg_r2_960_1023_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r2_960_1023_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_960_1023_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_0_63_0_2
       (.ADDRA({ADDRA,sprite_x_rev}),
        .ADDRB({ADDRA,sprite_x_rev}),
        .ADDRC({ADDRA,sprite_x_rev}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_0_63_0_2_n_0),
        .DOB(sprites_data_reg_r3_0_63_0_2_n_1),
        .DOC(sprites_data_reg_r3_0_63_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r3_0_63_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r1_0_63_0_2_i_2_n_0));
  LUT3 #(
    .INIT(8'h74)) 
    sprites_data_reg_r3_0_63_0_2_i_1
       (.I0(ADDRC[3]),
        .I1(tiles_reg_0_63_0_2_n_0),
        .I2(\sprite_x_rev_reg_n_0_[3] ),
        .O(sprite_x_rev[3]));
  LUT3 #(
    .INIT(8'h74)) 
    sprites_data_reg_r3_0_63_0_2_i_2
       (.I0(ADDRC[2]),
        .I1(tiles_reg_0_63_0_2_n_0),
        .I2(\sprite_x_rev_reg_n_0_[2] ),
        .O(sprite_x_rev[2]));
  LUT3 #(
    .INIT(8'h3A)) 
    sprites_data_reg_r3_0_63_0_2_i_3
       (.I0(\sprite_x_rev_reg_n_0_[1] ),
        .I1(ADDRC[1]),
        .I2(tiles_reg_0_63_0_2_n_0),
        .O(sprite_x_rev[1]));
  LUT3 #(
    .INIT(8'h3A)) 
    sprites_data_reg_r3_0_63_0_2_i_4
       (.I0(\sprite_x_rev_reg_n_0_[0] ),
        .I1(ADDRC[0]),
        .I2(tiles_reg_0_63_0_2_n_0),
        .O(sprite_x_rev[0]));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_0_63_3_5
       (.ADDRA({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRB({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRC({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_0_63_3_5_n_0),
        .DOB(sprites_data_reg_r3_0_63_3_5_n_1),
        .DOC(sprites_data_reg_r3_0_63_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r3_0_63_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r1_0_63_0_2_i_2_n_0));
  LUT3 #(
    .INIT(8'h74)) 
    sprites_data_reg_r3_0_63_3_5_i_1
       (.I0(ADDRC[3]),
        .I1(tiles_reg_0_63_0_2_n_0),
        .I2(\sprite_x_rev_reg_n_0_[3] ),
        .O(sprites_data_reg_r3_0_63_3_5_i_1_n_0));
  LUT3 #(
    .INIT(8'h74)) 
    sprites_data_reg_r3_0_63_3_5_i_2
       (.I0(ADDRC[2]),
        .I1(tiles_reg_0_63_0_2_n_0),
        .I2(\sprite_x_rev_reg_n_0_[2] ),
        .O(sprites_data_reg_r3_0_63_3_5_i_2_n_0));
  LUT3 #(
    .INIT(8'h3A)) 
    sprites_data_reg_r3_0_63_3_5_i_3
       (.I0(\sprite_x_rev_reg_n_0_[1] ),
        .I1(ADDRC[1]),
        .I2(tiles_reg_0_63_0_2_n_0),
        .O(sprites_data_reg_r3_0_63_3_5_i_3_n_0));
  LUT3 #(
    .INIT(8'h3A)) 
    sprites_data_reg_r3_0_63_3_5_i_4
       (.I0(\sprite_x_rev_reg_n_0_[0] ),
        .I1(ADDRC[0]),
        .I2(tiles_reg_0_63_0_2_n_0),
        .O(sprites_data_reg_r3_0_63_3_5_i_4_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_1024_1087_0_2
       (.ADDRA({ADDRA,sprite_x_rev}),
        .ADDRB({ADDRA,sprite_x_rev}),
        .ADDRC({ADDRA,sprite_x_rev}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_1024_1087_0_2_n_0),
        .DOB(sprites_data_reg_r3_1024_1087_0_2_n_1),
        .DOC(sprites_data_reg_r3_1024_1087_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r3_1024_1087_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1024_1087_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_1024_1087_3_5
       (.ADDRA({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRB({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRC({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_1024_1087_3_5_n_0),
        .DOB(sprites_data_reg_r3_1024_1087_3_5_n_1),
        .DOC(sprites_data_reg_r3_1024_1087_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r3_1024_1087_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1024_1087_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_1088_1151_0_2
       (.ADDRA({ADDRA,sprite_x_rev}),
        .ADDRB({ADDRA,sprite_x_rev}),
        .ADDRC({ADDRA,sprite_x_rev}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_1088_1151_0_2_n_0),
        .DOB(sprites_data_reg_r3_1088_1151_0_2_n_1),
        .DOC(sprites_data_reg_r3_1088_1151_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r3_1088_1151_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1088_1151_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_1088_1151_3_5
       (.ADDRA({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRB({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRC({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_1088_1151_3_5_n_0),
        .DOB(sprites_data_reg_r3_1088_1151_3_5_n_1),
        .DOC(sprites_data_reg_r3_1088_1151_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r3_1088_1151_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1088_1151_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_1152_1215_0_2
       (.ADDRA({ADDRA,sprite_x_rev}),
        .ADDRB({ADDRA,sprite_x_rev}),
        .ADDRC({ADDRA,sprite_x_rev}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_1152_1215_0_2_n_0),
        .DOB(sprites_data_reg_r3_1152_1215_0_2_n_1),
        .DOC(sprites_data_reg_r3_1152_1215_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r3_1152_1215_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1152_1215_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_1152_1215_3_5
       (.ADDRA({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRB({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRC({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_1152_1215_3_5_n_0),
        .DOB(sprites_data_reg_r3_1152_1215_3_5_n_1),
        .DOC(sprites_data_reg_r3_1152_1215_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r3_1152_1215_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1152_1215_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_1216_1279_0_2
       (.ADDRA({ADDRA,sprite_x_rev}),
        .ADDRB({ADDRA,sprite_x_rev}),
        .ADDRC({ADDRA,sprite_x_rev}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_1216_1279_0_2_n_0),
        .DOB(sprites_data_reg_r3_1216_1279_0_2_n_1),
        .DOC(sprites_data_reg_r3_1216_1279_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r3_1216_1279_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1216_1279_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_1216_1279_3_5
       (.ADDRA({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRB({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRC({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_1216_1279_3_5_n_0),
        .DOB(sprites_data_reg_r3_1216_1279_3_5_n_1),
        .DOC(sprites_data_reg_r3_1216_1279_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r3_1216_1279_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1216_1279_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_1280_1343_0_2
       (.ADDRA({ADDRA,sprite_x_rev}),
        .ADDRB({ADDRA,sprite_x_rev}),
        .ADDRC({ADDRA,sprite_x_rev}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_1280_1343_0_2_n_0),
        .DOB(sprites_data_reg_r3_1280_1343_0_2_n_1),
        .DOC(sprites_data_reg_r3_1280_1343_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r3_1280_1343_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1280_1343_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_1280_1343_3_5
       (.ADDRA({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRB({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRC({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_1280_1343_3_5_n_0),
        .DOB(sprites_data_reg_r3_1280_1343_3_5_n_1),
        .DOC(sprites_data_reg_r3_1280_1343_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r3_1280_1343_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1280_1343_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_128_191_0_2
       (.ADDRA({ADDRA,sprite_x_rev}),
        .ADDRB({ADDRA,sprite_x_rev}),
        .ADDRC({ADDRA,sprite_x_rev}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_128_191_0_2_n_0),
        .DOB(sprites_data_reg_r3_128_191_0_2_n_1),
        .DOC(sprites_data_reg_r3_128_191_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r3_128_191_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r1_128_191_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_128_191_3_5
       (.ADDRA({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRB({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRC({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_128_191_3_5_n_0),
        .DOB(sprites_data_reg_r3_128_191_3_5_n_1),
        .DOC(sprites_data_reg_r3_128_191_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r3_128_191_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r1_128_191_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_1344_1407_0_2
       (.ADDRA({ADDRA,sprite_x_rev}),
        .ADDRB({ADDRA,sprite_x_rev}),
        .ADDRC({ADDRA,sprite_x_rev}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_1344_1407_0_2_n_0),
        .DOB(sprites_data_reg_r3_1344_1407_0_2_n_1),
        .DOC(sprites_data_reg_r3_1344_1407_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r3_1344_1407_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1344_1407_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_1344_1407_3_5
       (.ADDRA({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRB({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRC({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_1344_1407_3_5_n_0),
        .DOB(sprites_data_reg_r3_1344_1407_3_5_n_1),
        .DOC(sprites_data_reg_r3_1344_1407_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r3_1344_1407_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1344_1407_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_1408_1471_0_2
       (.ADDRA({ADDRA,sprite_x_rev}),
        .ADDRB({ADDRA,sprite_x_rev}),
        .ADDRC({ADDRA,sprite_x_rev}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_1408_1471_0_2_n_0),
        .DOB(sprites_data_reg_r3_1408_1471_0_2_n_1),
        .DOC(sprites_data_reg_r3_1408_1471_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r3_1408_1471_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1408_1471_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_1408_1471_3_5
       (.ADDRA({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRB({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRC({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_1408_1471_3_5_n_0),
        .DOB(sprites_data_reg_r3_1408_1471_3_5_n_1),
        .DOC(sprites_data_reg_r3_1408_1471_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r3_1408_1471_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1408_1471_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_1472_1535_0_2
       (.ADDRA({ADDRA,sprite_x_rev}),
        .ADDRB({ADDRA,sprite_x_rev}),
        .ADDRC({ADDRA,sprite_x_rev}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_1472_1535_0_2_n_0),
        .DOB(sprites_data_reg_r3_1472_1535_0_2_n_1),
        .DOC(sprites_data_reg_r3_1472_1535_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r3_1472_1535_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1472_1535_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_1472_1535_3_5
       (.ADDRA({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRB({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRC({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_1472_1535_3_5_n_0),
        .DOB(sprites_data_reg_r3_1472_1535_3_5_n_1),
        .DOC(sprites_data_reg_r3_1472_1535_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r3_1472_1535_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1472_1535_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_1536_1599_0_2
       (.ADDRA({ADDRA,sprite_x_rev}),
        .ADDRB({ADDRA,sprite_x_rev}),
        .ADDRC({ADDRA,sprite_x_rev}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_1536_1599_0_2_n_0),
        .DOB(sprites_data_reg_r3_1536_1599_0_2_n_1),
        .DOC(sprites_data_reg_r3_1536_1599_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r3_1536_1599_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1536_1599_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_1536_1599_3_5
       (.ADDRA({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRB({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRC({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_1536_1599_3_5_n_0),
        .DOB(sprites_data_reg_r3_1536_1599_3_5_n_1),
        .DOC(sprites_data_reg_r3_1536_1599_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r3_1536_1599_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1536_1599_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_1600_1663_0_2
       (.ADDRA({ADDRA,sprite_x_rev}),
        .ADDRB({ADDRA,sprite_x_rev}),
        .ADDRC({ADDRA,sprite_x_rev}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_1600_1663_0_2_n_0),
        .DOB(sprites_data_reg_r3_1600_1663_0_2_n_1),
        .DOC(sprites_data_reg_r3_1600_1663_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r3_1600_1663_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1600_1663_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_1600_1663_3_5
       (.ADDRA({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRB({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRC({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_1600_1663_3_5_n_0),
        .DOB(sprites_data_reg_r3_1600_1663_3_5_n_1),
        .DOC(sprites_data_reg_r3_1600_1663_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r3_1600_1663_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1600_1663_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_1664_1727_0_2
       (.ADDRA({ADDRA,sprite_x_rev}),
        .ADDRB({ADDRA,sprite_x_rev}),
        .ADDRC({ADDRA,sprite_x_rev}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_1664_1727_0_2_n_0),
        .DOB(sprites_data_reg_r3_1664_1727_0_2_n_1),
        .DOC(sprites_data_reg_r3_1664_1727_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r3_1664_1727_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1664_1727_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_1664_1727_3_5
       (.ADDRA({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRB({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRC({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_1664_1727_3_5_n_0),
        .DOB(sprites_data_reg_r3_1664_1727_3_5_n_1),
        .DOC(sprites_data_reg_r3_1664_1727_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r3_1664_1727_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1664_1727_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_1728_1791_0_2
       (.ADDRA({ADDRA,sprite_x_rev}),
        .ADDRB({ADDRA,sprite_x_rev}),
        .ADDRC({ADDRA,sprite_x_rev}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_1728_1791_0_2_n_0),
        .DOB(sprites_data_reg_r3_1728_1791_0_2_n_1),
        .DOC(sprites_data_reg_r3_1728_1791_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r3_1728_1791_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1728_1791_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_1728_1791_3_5
       (.ADDRA({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRB({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRC({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_1728_1791_3_5_n_0),
        .DOB(sprites_data_reg_r3_1728_1791_3_5_n_1),
        .DOC(sprites_data_reg_r3_1728_1791_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r3_1728_1791_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1728_1791_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_1792_1855_0_2
       (.ADDRA({ADDRA,sprite_x_rev}),
        .ADDRB({ADDRA,sprite_x_rev}),
        .ADDRC({ADDRA,sprite_x_rev}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_1792_1855_0_2_n_0),
        .DOB(sprites_data_reg_r3_1792_1855_0_2_n_1),
        .DOC(sprites_data_reg_r3_1792_1855_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r3_1792_1855_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1792_1855_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_1792_1855_3_5
       (.ADDRA({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRB({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRC({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_1792_1855_3_5_n_0),
        .DOB(sprites_data_reg_r3_1792_1855_3_5_n_1),
        .DOC(sprites_data_reg_r3_1792_1855_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r3_1792_1855_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1792_1855_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_1856_1919_0_2
       (.ADDRA({ADDRA,sprite_x_rev}),
        .ADDRB({ADDRA,sprite_x_rev}),
        .ADDRC({ADDRA,sprite_x_rev}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_1856_1919_0_2_n_0),
        .DOB(sprites_data_reg_r3_1856_1919_0_2_n_1),
        .DOC(sprites_data_reg_r3_1856_1919_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r3_1856_1919_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1856_1919_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_1856_1919_3_5
       (.ADDRA({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRB({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRC({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_1856_1919_3_5_n_0),
        .DOB(sprites_data_reg_r3_1856_1919_3_5_n_1),
        .DOC(sprites_data_reg_r3_1856_1919_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r3_1856_1919_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1856_1919_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_1920_1983_0_2
       (.ADDRA({ADDRA,sprite_x_rev}),
        .ADDRB({ADDRA,sprite_x_rev}),
        .ADDRC({ADDRA,sprite_x_rev}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_1920_1983_0_2_n_0),
        .DOB(sprites_data_reg_r3_1920_1983_0_2_n_1),
        .DOC(sprites_data_reg_r3_1920_1983_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r3_1920_1983_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1920_1983_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_1920_1983_3_5
       (.ADDRA({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRB({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRC({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_1920_1983_3_5_n_0),
        .DOB(sprites_data_reg_r3_1920_1983_3_5_n_1),
        .DOC(sprites_data_reg_r3_1920_1983_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r3_1920_1983_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1920_1983_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_192_255_0_2
       (.ADDRA({ADDRA,sprite_x_rev}),
        .ADDRB({ADDRA,sprite_x_rev}),
        .ADDRC({ADDRA,sprite_x_rev}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_192_255_0_2_n_0),
        .DOB(sprites_data_reg_r3_192_255_0_2_n_1),
        .DOC(sprites_data_reg_r3_192_255_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r3_192_255_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r1_192_255_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_192_255_3_5
       (.ADDRA({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRB({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRC({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_192_255_3_5_n_0),
        .DOB(sprites_data_reg_r3_192_255_3_5_n_1),
        .DOC(sprites_data_reg_r3_192_255_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r3_192_255_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r1_192_255_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_1984_2047_0_2
       (.ADDRA({ADDRA,sprite_x_rev}),
        .ADDRB({ADDRA,sprite_x_rev}),
        .ADDRC({ADDRA,sprite_x_rev}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_1984_2047_0_2_n_0),
        .DOB(sprites_data_reg_r3_1984_2047_0_2_n_1),
        .DOC(sprites_data_reg_r3_1984_2047_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r3_1984_2047_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1984_2047_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_1984_2047_3_5
       (.ADDRA({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRB({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRC({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_1984_2047_3_5_n_0),
        .DOB(sprites_data_reg_r3_1984_2047_3_5_n_1),
        .DOC(sprites_data_reg_r3_1984_2047_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r3_1984_2047_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_1984_2047_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_2048_2111_0_2
       (.ADDRA({ADDRB,sprites_data_reg_r3_2048_2111_0_2_i_1_n_0,sprites_data_reg_r3_2048_2111_0_2_i_2_n_0,sprites_data_reg_r3_2048_2111_0_2_i_3_n_0,sprites_data_reg_r3_2048_2111_0_2_i_4_n_0}),
        .ADDRB({ADDRB,sprites_data_reg_r3_2048_2111_0_2_i_1_n_0,sprites_data_reg_r3_2048_2111_0_2_i_2_n_0,sprites_data_reg_r3_2048_2111_0_2_i_3_n_0,sprites_data_reg_r3_2048_2111_0_2_i_4_n_0}),
        .ADDRC({ADDRB,sprites_data_reg_r3_2048_2111_0_2_i_1_n_0,sprites_data_reg_r3_2048_2111_0_2_i_2_n_0,sprites_data_reg_r3_2048_2111_0_2_i_3_n_0,sprites_data_reg_r3_2048_2111_0_2_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_2048_2111_0_2_n_0),
        .DOB(sprites_data_reg_r3_2048_2111_0_2_n_1),
        .DOC(sprites_data_reg_r3_2048_2111_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r3_2048_2111_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(1'b0));
  LUT3 #(
    .INIT(8'h74)) 
    sprites_data_reg_r3_2048_2111_0_2_i_1
       (.I0(ADDRC[3]),
        .I1(tiles_reg_0_63_0_2_n_0),
        .I2(\sprite_x_rev_reg_n_0_[3] ),
        .O(sprites_data_reg_r3_2048_2111_0_2_i_1_n_0));
  LUT3 #(
    .INIT(8'h74)) 
    sprites_data_reg_r3_2048_2111_0_2_i_2
       (.I0(ADDRC[2]),
        .I1(tiles_reg_0_63_0_2_n_0),
        .I2(\sprite_x_rev_reg_n_0_[2] ),
        .O(sprites_data_reg_r3_2048_2111_0_2_i_2_n_0));
  LUT3 #(
    .INIT(8'h3A)) 
    sprites_data_reg_r3_2048_2111_0_2_i_3
       (.I0(\sprite_x_rev_reg_n_0_[1] ),
        .I1(ADDRC[1]),
        .I2(tiles_reg_0_63_0_2_n_0),
        .O(sprites_data_reg_r3_2048_2111_0_2_i_3_n_0));
  LUT3 #(
    .INIT(8'h3A)) 
    sprites_data_reg_r3_2048_2111_0_2_i_4
       (.I0(\sprite_x_rev_reg_n_0_[0] ),
        .I1(ADDRC[0]),
        .I2(tiles_reg_0_63_0_2_n_0),
        .O(sprites_data_reg_r3_2048_2111_0_2_i_4_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_2048_2111_3_5
       (.ADDRA({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRB({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRC({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_2048_2111_3_5_n_0),
        .DOB(sprites_data_reg_r3_2048_2111_3_5_n_1),
        .DOC(sprites_data_reg_r3_2048_2111_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r3_2048_2111_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(1'b0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_256_319_0_2
       (.ADDRA({ADDRA,sprite_x_rev}),
        .ADDRB({ADDRA,sprite_x_rev}),
        .ADDRC({ADDRA,sprite_x_rev}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_256_319_0_2_n_0),
        .DOB(sprites_data_reg_r3_256_319_0_2_n_1),
        .DOC(sprites_data_reg_r3_256_319_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r3_256_319_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_256_319_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_256_319_3_5
       (.ADDRA({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRB({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRC({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_256_319_3_5_n_0),
        .DOB(sprites_data_reg_r3_256_319_3_5_n_1),
        .DOC(sprites_data_reg_r3_256_319_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r3_256_319_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_256_319_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_320_383_0_2
       (.ADDRA({ADDRA,sprite_x_rev}),
        .ADDRB({ADDRA,sprite_x_rev}),
        .ADDRC({ADDRA,sprite_x_rev}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_320_383_0_2_n_0),
        .DOB(sprites_data_reg_r3_320_383_0_2_n_1),
        .DOC(sprites_data_reg_r3_320_383_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r3_320_383_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_320_383_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_320_383_3_5
       (.ADDRA({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRB({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRC({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_320_383_3_5_n_0),
        .DOB(sprites_data_reg_r3_320_383_3_5_n_1),
        .DOC(sprites_data_reg_r3_320_383_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r3_320_383_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_320_383_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_384_447_0_2
       (.ADDRA({ADDRA,sprite_x_rev}),
        .ADDRB({ADDRA,sprite_x_rev}),
        .ADDRC({ADDRA,sprite_x_rev}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_384_447_0_2_n_0),
        .DOB(sprites_data_reg_r3_384_447_0_2_n_1),
        .DOC(sprites_data_reg_r3_384_447_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r3_384_447_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_384_447_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_384_447_3_5
       (.ADDRA({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRB({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRC({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_384_447_3_5_n_0),
        .DOB(sprites_data_reg_r3_384_447_3_5_n_1),
        .DOC(sprites_data_reg_r3_384_447_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r3_384_447_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_384_447_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_448_511_0_2
       (.ADDRA({ADDRA,sprite_x_rev}),
        .ADDRB({ADDRA,sprite_x_rev}),
        .ADDRC({ADDRA,sprite_x_rev}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_448_511_0_2_n_0),
        .DOB(sprites_data_reg_r3_448_511_0_2_n_1),
        .DOC(sprites_data_reg_r3_448_511_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r3_448_511_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_448_511_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_448_511_3_5
       (.ADDRA({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRB({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRC({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_448_511_3_5_n_0),
        .DOB(sprites_data_reg_r3_448_511_3_5_n_1),
        .DOC(sprites_data_reg_r3_448_511_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r3_448_511_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_448_511_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_512_575_0_2
       (.ADDRA({ADDRA,sprite_x_rev}),
        .ADDRB({ADDRA,sprite_x_rev}),
        .ADDRC({ADDRA,sprite_x_rev}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_512_575_0_2_n_0),
        .DOB(sprites_data_reg_r3_512_575_0_2_n_1),
        .DOC(sprites_data_reg_r3_512_575_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r3_512_575_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_512_575_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_512_575_3_5
       (.ADDRA({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRB({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRC({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_512_575_3_5_n_0),
        .DOB(sprites_data_reg_r3_512_575_3_5_n_1),
        .DOC(sprites_data_reg_r3_512_575_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r3_512_575_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_512_575_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_576_639_0_2
       (.ADDRA({ADDRA,sprite_x_rev}),
        .ADDRB({ADDRA,sprite_x_rev}),
        .ADDRC({ADDRA,sprite_x_rev}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_576_639_0_2_n_0),
        .DOB(sprites_data_reg_r3_576_639_0_2_n_1),
        .DOC(sprites_data_reg_r3_576_639_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r3_576_639_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_576_639_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_576_639_3_5
       (.ADDRA({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRB({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRC({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_576_639_3_5_n_0),
        .DOB(sprites_data_reg_r3_576_639_3_5_n_1),
        .DOC(sprites_data_reg_r3_576_639_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r3_576_639_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_576_639_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_640_703_0_2
       (.ADDRA({ADDRA,sprite_x_rev}),
        .ADDRB({ADDRA,sprite_x_rev}),
        .ADDRC({ADDRA,sprite_x_rev}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_640_703_0_2_n_0),
        .DOB(sprites_data_reg_r3_640_703_0_2_n_1),
        .DOC(sprites_data_reg_r3_640_703_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r3_640_703_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_640_703_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_640_703_3_5
       (.ADDRA({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRB({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRC({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_640_703_3_5_n_0),
        .DOB(sprites_data_reg_r3_640_703_3_5_n_1),
        .DOC(sprites_data_reg_r3_640_703_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r3_640_703_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_640_703_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_64_127_0_2
       (.ADDRA({ADDRA,sprite_x_rev}),
        .ADDRB({ADDRA,sprite_x_rev}),
        .ADDRC({ADDRA,sprite_x_rev}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_64_127_0_2_n_0),
        .DOB(sprites_data_reg_r3_64_127_0_2_n_1),
        .DOC(sprites_data_reg_r3_64_127_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r3_64_127_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r1_64_127_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_64_127_3_5
       (.ADDRA({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRB({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRC({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_64_127_3_5_n_0),
        .DOB(sprites_data_reg_r3_64_127_3_5_n_1),
        .DOC(sprites_data_reg_r3_64_127_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r3_64_127_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r1_64_127_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_704_767_0_2
       (.ADDRA({ADDRA,sprite_x_rev}),
        .ADDRB({ADDRA,sprite_x_rev}),
        .ADDRC({ADDRA,sprite_x_rev}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_704_767_0_2_n_0),
        .DOB(sprites_data_reg_r3_704_767_0_2_n_1),
        .DOC(sprites_data_reg_r3_704_767_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r3_704_767_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_704_767_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_704_767_3_5
       (.ADDRA({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRB({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRC({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_704_767_3_5_n_0),
        .DOB(sprites_data_reg_r3_704_767_3_5_n_1),
        .DOC(sprites_data_reg_r3_704_767_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r3_704_767_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_704_767_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_768_831_0_2
       (.ADDRA({ADDRA,sprite_x_rev}),
        .ADDRB({ADDRA,sprite_x_rev}),
        .ADDRC({ADDRA,sprite_x_rev}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_768_831_0_2_n_0),
        .DOB(sprites_data_reg_r3_768_831_0_2_n_1),
        .DOC(sprites_data_reg_r3_768_831_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r3_768_831_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_768_831_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_768_831_3_5
       (.ADDRA({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRB({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRC({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_768_831_3_5_n_0),
        .DOB(sprites_data_reg_r3_768_831_3_5_n_1),
        .DOC(sprites_data_reg_r3_768_831_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r3_768_831_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_768_831_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_832_895_0_2
       (.ADDRA({ADDRA,sprite_x_rev}),
        .ADDRB({ADDRA,sprite_x_rev}),
        .ADDRC({ADDRA,sprite_x_rev}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_832_895_0_2_n_0),
        .DOB(sprites_data_reg_r3_832_895_0_2_n_1),
        .DOC(sprites_data_reg_r3_832_895_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r3_832_895_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_832_895_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_832_895_3_5
       (.ADDRA({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRB({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRC({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_832_895_3_5_n_0),
        .DOB(sprites_data_reg_r3_832_895_3_5_n_1),
        .DOC(sprites_data_reg_r3_832_895_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r3_832_895_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_832_895_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_896_959_0_2
       (.ADDRA({ADDRA,sprite_x_rev}),
        .ADDRB({ADDRA,sprite_x_rev}),
        .ADDRC({ADDRA,sprite_x_rev}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_896_959_0_2_n_0),
        .DOB(sprites_data_reg_r3_896_959_0_2_n_1),
        .DOC(sprites_data_reg_r3_896_959_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r3_896_959_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_896_959_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_896_959_3_5
       (.ADDRA({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRB({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRC({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_896_959_3_5_n_0),
        .DOB(sprites_data_reg_r3_896_959_3_5_n_1),
        .DOC(sprites_data_reg_r3_896_959_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r3_896_959_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_896_959_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_960_1023_0_2
       (.ADDRA({ADDRA,sprite_x_rev}),
        .ADDRB({ADDRA,sprite_x_rev}),
        .ADDRC({ADDRA,sprite_x_rev}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[0]),
        .DIB(pixel[1]),
        .DIC(pixel[2]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_960_1023_0_2_n_0),
        .DOB(sprites_data_reg_r3_960_1023_0_2_n_1),
        .DOC(sprites_data_reg_r3_960_1023_0_2_n_2),
        .DOD(NLW_sprites_data_reg_r3_960_1023_0_2_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_960_1023_0_2_i_1_n_0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000),
    .IS_WCLK_INVERTED(1'b1)) 
    sprites_data_reg_r3_960_1023_3_5
       (.ADDRA({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRB({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRC({\v_cnt_reg[1]_rep__1 ,sprites_data_reg_r3_0_63_3_5_i_1_n_0,sprites_data_reg_r3_0_63_3_5_i_2_n_0,sprites_data_reg_r3_0_63_3_5_i_3_n_0,sprites_data_reg_r3_0_63_3_5_i_4_n_0}),
        .ADDRD(ind_reg[5:0]),
        .DIA(pixel[3]),
        .DIB(pixel[4]),
        .DIC(pixel[5]),
        .DID(1'b0),
        .DOA(sprites_data_reg_r3_960_1023_3_5_n_0),
        .DOB(sprites_data_reg_r3_960_1023_3_5_n_1),
        .DOC(sprites_data_reg_r3_960_1023_3_5_n_2),
        .DOD(NLW_sprites_data_reg_r3_960_1023_3_5_DOD_UNCONNECTED),
        .WCLK(clk_0),
        .WE(sprites_data_reg_r2_960_1023_0_2_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    \tile_column_write_counter_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\addr_X[0]_i_1_n_0 ),
        .Q(tile_column_write_counter_reg__0[0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \tile_column_write_counter_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_out[1]),
        .Q(tile_column_write_counter_reg__0[1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \tile_column_write_counter_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_out[2]),
        .Q(tile_column_write_counter_reg__0[2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \tile_column_write_counter_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_out[3]),
        .Q(tile_column_write_counter_reg__0[3]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \tile_column_write_counter_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_out[4]),
        .Q(tile_column_write_counter_reg__0[4]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \tile_column_write_counter_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(p_2_out[5]),
        .Q(tile_column_write_counter_reg__0[5]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \tile_row_write_counter_reg[0] 
       (.C(clk),
        .CE(SR),
        .D(\v_cnt_reg[9] [0]),
        .Q(\tile_row_write_counter_reg_n_0_[0] ),
        .R(\v_cnt_reg[8] ));
  FDRE #(
    .INIT(1'b0)) 
    \tile_row_write_counter_reg[1] 
       (.C(clk),
        .CE(SR),
        .D(\v_cnt_reg[9] [1]),
        .Q(\tile_row_write_counter_reg_n_0_[1] ),
        .R(\v_cnt_reg[8] ));
  FDRE #(
    .INIT(1'b0)) 
    \tile_row_write_counter_reg[2] 
       (.C(clk),
        .CE(SR),
        .D(\v_cnt_reg[9] [2]),
        .Q(\tile_row_write_counter_reg_n_0_[2] ),
        .R(\v_cnt_reg[8] ));
  FDRE #(
    .INIT(1'b0)) 
    \tile_row_write_counter_reg[3] 
       (.C(clk),
        .CE(SR),
        .D(\v_cnt_reg[9] [3]),
        .Q(\tile_row_write_counter_reg_n_0_[3] ),
        .R(\v_cnt_reg[8] ));
  FDRE #(
    .INIT(1'b0)) 
    \tile_row_write_counter_reg[4] 
       (.C(clk),
        .CE(SR),
        .D(\v_cnt_reg[9] [4]),
        .Q(\tile_row_write_counter_reg_n_0_[4] ),
        .R(\v_cnt_reg[8] ));
  FDRE #(
    .INIT(1'b0)) 
    \tile_row_write_counter_reg[5] 
       (.C(clk),
        .CE(SR),
        .D(\v_cnt_reg[9] [5]),
        .Q(Q),
        .R(\v_cnt_reg[8] ));
  FDRE #(
    .INIT(1'b1)) 
    tile_wrote_reg
       (.C(clk),
        .CE(1'b1),
        .D(render_enable_reg),
        .Q(\addr_X_reg[0]_0 ),
        .R(1'b0));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000)) 
    tiles_reg_0_63_0_2
       (.ADDRA({current_tile__0,\current_tile_reg[1]_0 }),
        .ADDRB({current_tile__0,\current_tile_reg[1]_0 }),
        .ADDRC({current_tile__0,\current_tile_reg[1]_0 }),
        .ADDRD(tile_column_write_counter_reg__0),
        .DIA(tile_id[0]),
        .DIB(tile_id[1]),
        .DIC(tile_id[2]),
        .DID(1'b0),
        .DOA(tiles_reg_0_63_0_2_n_0),
        .DOB(tiles_reg_0_63_0_2_n_1),
        .DOC(tiles_reg_0_63_0_2_n_2),
        .DOD(NLW_tiles_reg_0_63_0_2_DOD_UNCONNECTED),
        .WCLK(clk),
        .WE(I7));
  LUT2 #(
    .INIT(4'h2)) 
    tiles_reg_0_63_0_2_i_2
       (.I0(current_tile[5]),
        .I1(\h_cnt_reg[6]_0 ),
        .O(current_tile__0[5]));
  LUT2 #(
    .INIT(4'h2)) 
    tiles_reg_0_63_0_2_i_3
       (.I0(current_tile[4]),
        .I1(\h_cnt_reg[6]_0 ),
        .O(current_tile__0[4]));
  LUT2 #(
    .INIT(4'h2)) 
    tiles_reg_0_63_0_2_i_4
       (.I0(current_tile[3]),
        .I1(\h_cnt_reg[6]_0 ),
        .O(current_tile__0[3]));
  LUT2 #(
    .INIT(4'h2)) 
    tiles_reg_0_63_0_2_i_5
       (.I0(\current_tile_reg[3]_0 [2]),
        .I1(\h_cnt_reg[6]_0 ),
        .O(current_tile__0[2]));
  LUT2 #(
    .INIT(4'h2)) 
    tiles_reg_0_63_0_2_i_6
       (.I0(\current_tile_reg[3]_0 [1]),
        .I1(\h_cnt_reg[6]_0 ),
        .O(\current_tile_reg[1]_0 [1]));
  LUT2 #(
    .INIT(4'h2)) 
    tiles_reg_0_63_0_2_i_7
       (.I0(\current_tile_reg[3]_0 [0]),
        .I1(\h_cnt_reg[6]_0 ),
        .O(\current_tile_reg[1]_0 [0]));
  (* METHODOLOGY_DRC_VIOS = "" *) 
  RAM64M #(
    .INIT_A(64'h0000000000000000),
    .INIT_B(64'h0000000000000000),
    .INIT_C(64'h0000000000000000),
    .INIT_D(64'h0000000000000000)) 
    tiles_reg_0_63_3_3
       (.ADDRA({current_tile__0,\current_tile_reg[1]_0 }),
        .ADDRB({current_tile__0,\current_tile_reg[1]_0 }),
        .ADDRC({current_tile__0,\current_tile_reg[1]_0 }),
        .ADDRD(tile_column_write_counter_reg__0),
        .DIA(tile_id[3]),
        .DIB(1'b0),
        .DIC(1'b0),
        .DID(1'b0),
        .DOA(tiles_reg_0_63_3_3_n_0),
        .DOB(NLW_tiles_reg_0_63_3_3_DOB_UNCONNECTED),
        .DOC(NLW_tiles_reg_0_63_3_3_DOC_UNCONNECTED),
        .DOD(NLW_tiles_reg_0_63_3_3_DOD_UNCONNECTED),
        .WCLK(clk),
        .WE(I7));
  LUT1 #(
    .INIT(2'h2)) 
    tm_reg_0_i_18
       (.I0(out_tile2),
        .O(tm_reg_0_i_18_n_0));
  CARRY4 tm_reg_0_i_29
       (.CI(tm_reg_0_i_30_n_0),
        .CO({out_tile2,NLW_tm_reg_0_i_29_CO_UNCONNECTED[2],tm_reg_0_i_29_n_2,tm_reg_0_i_29_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,tm_reg_0[3]}),
        .O({NLW_tm_reg_0_i_29_O_UNCONNECTED[3],tm_reg_0_0[6:4]}),
        .S({1'b1,tm_reg_0_i_38_n_0,tm_reg_0_i_39_n_0,\addr_Y_reg[3]_0 }));
  CARRY4 tm_reg_0_i_30
       (.CI(1'b0),
        .CO({tm_reg_0_i_30_n_0,tm_reg_0_i_30_n_1,tm_reg_0_i_30_n_2,tm_reg_0_i_30_n_3}),
        .CYINIT(1'b0),
        .DI({tm_reg_0[2:0],1'b0}),
        .O(tm_reg_0_0[3:0]),
        .S({S,tm_reg_0_i_44_n_0}));
  LUT1 #(
    .INIT(2'h2)) 
    tm_reg_0_i_38
       (.I0(tm_reg_0[5]),
        .O(tm_reg_0_i_38_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    tm_reg_0_i_39
       (.I0(tm_reg_0[4]),
        .O(tm_reg_0_i_39_n_0));
  LUT1 #(
    .INIT(2'h2)) 
    tm_reg_0_i_44
       (.I0(tm_reg_0[1]),
        .O(tm_reg_0_i_44_n_0));
  CARRY4 tm_reg_0_i_5
       (.CI(tm_reg_0_i_6_n_0),
        .CO(NLW_tm_reg_0_i_5_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_tm_reg_0_i_5_O_UNCONNECTED[3:1],ADDRBWRADDR[11]}),
        .S({1'b0,1'b0,1'b0,tm_reg_0_i_18_n_0}));
  CARRY4 tm_reg_0_i_6
       (.CI(tm_reg_0_i_7_n_0),
        .CO({tm_reg_0_i_6_n_0,tm_reg_0_i_6_n_1,tm_reg_0_i_6_n_2,tm_reg_0_i_6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(ADDRBWRADDR[10:7]),
        .S(\addr_Y_reg[3]_1 ));
  CARRY4 tm_reg_0_i_7
       (.CI(1'b0),
        .CO({tm_reg_0_i_7_n_0,tm_reg_0_i_7_n_1,tm_reg_0_i_7_n_2,tm_reg_0_i_7_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,tm_reg_0_1}),
        .O({ADDRBWRADDR[6:4],NLW_tm_reg_0_i_7_O_UNCONNECTED[0]}),
        .S(\addr_Y_reg[2]_0 ));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_tile_manager
   (tile_id,
    tm_reg_0_0,
    tm_reg_0_1,
    tm_reg_0_2,
    tm_reg_0_3,
    S,
    tm_reg_0_4,
    clk,
    clk_0,
    WEA,
    ADDRARDADDR,
    ADDRBWRADDR,
    \tile_out_reg[3] ,
    \Ymap_reg[3] ,
    \addr_Y_reg[3] ,
    \Xmap_reg[6] ,
    Q,
    \addr_Y_reg[5] ,
    \addr_X_reg[5] );
  output [3:0]tile_id;
  output [3:0]tm_reg_0_0;
  output [3:0]tm_reg_0_1;
  output [3:0]tm_reg_0_2;
  output [3:0]tm_reg_0_3;
  output [2:0]S;
  output [0:0]tm_reg_0_4;
  input clk;
  input clk_0;
  input [0:0]WEA;
  input [11:0]ADDRARDADDR;
  input [11:0]ADDRBWRADDR;
  input [3:0]\tile_out_reg[3] ;
  input [6:0]\Ymap_reg[3] ;
  input [6:0]\addr_Y_reg[3] ;
  input [2:0]\Xmap_reg[6] ;
  input [0:0]Q;
  input [5:0]\addr_Y_reg[5] ;
  input [1:0]\addr_X_reg[5] ;

  wire [11:0]ADDRARDADDR;
  wire [11:0]ADDRBWRADDR;
  wire [0:0]Q;
  wire [2:0]S;
  wire [0:0]WEA;
  wire [2:0]\Xmap_reg[6] ;
  wire [6:0]\Ymap_reg[3] ;
  wire [1:0]\addr_X_reg[5] ;
  wire [6:0]\addr_Y_reg[3] ;
  wire [5:0]\addr_Y_reg[5] ;
  wire clk;
  wire clk_0;
  wire [3:0]tile_id;
  wire [3:0]\tile_out_reg[3] ;
  wire [3:0]tm_reg_0_0;
  wire [3:0]tm_reg_0_1;
  wire [3:0]tm_reg_0_2;
  wire [3:0]tm_reg_0_3;
  wire [0:0]tm_reg_0_4;
  wire tm_reg_0_i_4_n_0;
  wire tm_reg_0_i_8_n_0;
  wire NLW_tm_reg_0_CASCADEOUTA_UNCONNECTED;
  wire NLW_tm_reg_0_CASCADEOUTB_UNCONNECTED;
  wire NLW_tm_reg_0_DBITERR_UNCONNECTED;
  wire NLW_tm_reg_0_INJECTDBITERR_UNCONNECTED;
  wire NLW_tm_reg_0_INJECTSBITERR_UNCONNECTED;
  wire NLW_tm_reg_0_SBITERR_UNCONNECTED;
  wire [31:0]NLW_tm_reg_0_DOADO_UNCONNECTED;
  wire [31:4]NLW_tm_reg_0_DOBDO_UNCONNECTED;
  wire [3:0]NLW_tm_reg_0_DOPADOP_UNCONNECTED;
  wire [3:0]NLW_tm_reg_0_DOPBDOP_UNCONNECTED;
  wire [7:0]NLW_tm_reg_0_ECCPARITY_UNCONNECTED;
  wire [8:0]NLW_tm_reg_0_RDADDRECC_UNCONNECTED;

  (* \MEM.PORTA.DATA_BIT_LAYOUT  = "p0_d4" *) 
  (* \MEM.PORTB.DATA_BIT_LAYOUT  = "p0_d4" *) 
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-6 {cell *THIS*}}" *) 
  (* RTL_RAM_BITS = "91200" *) 
  (* RTL_RAM_NAME = "tm" *) 
  (* bram_addr_begin = "0" *) 
  (* bram_addr_end = "8191" *) 
  (* bram_slice_begin = "0" *) 
  (* bram_slice_end = "3" *) 
  RAMB36E1 #(
    .DOA_REG(0),
    .DOB_REG(0),
    .EN_ECC_READ("FALSE"),
    .EN_ECC_WRITE("FALSE"),
    .INITP_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INITP_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_04(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_05(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_06(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_07(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_08(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_09(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_0F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_10(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_11(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_12(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_13(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_14(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_15(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_16(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_17(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_18(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_19(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_1F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_20(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_21(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_22(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_23(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_24(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_25(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_26(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_27(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_28(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_29(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_2F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_30(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_31(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_32(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_33(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_34(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_35(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_36(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_37(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_38(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_39(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_3F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_40(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_41(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_42(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_43(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_44(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_45(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_46(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_47(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_48(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_49(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_4F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_50(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_51(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_52(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_53(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_54(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_55(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_56(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_57(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_58(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_59(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_5F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_60(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_61(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_62(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_63(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_64(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_65(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_66(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_67(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_68(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_69(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_6F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_70(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_71(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_72(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_73(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_74(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_75(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_76(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_77(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_78(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_79(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7A(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7B(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7C(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7D(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7E(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_7F(256'h0000000000000000000000000000000000000000000000000000000000000000),
    .INIT_A(36'h000000000),
    .INIT_B(36'h000000000),
    .IS_CLKBWRCLK_INVERTED(1'b1),
    .RAM_EXTENSION_A("NONE"),
    .RAM_EXTENSION_B("NONE"),
    .RAM_MODE("TDP"),
    .RDADDR_COLLISION_HWCONFIG("DELAYED_WRITE"),
    .READ_WIDTH_A(4),
    .READ_WIDTH_B(4),
    .RSTREG_PRIORITY_A("RSTREG"),
    .RSTREG_PRIORITY_B("RSTREG"),
    .SIM_COLLISION_CHECK("ALL"),
    .SIM_DEVICE("7SERIES"),
    .SRVAL_A(36'h000000000),
    .SRVAL_B(36'h000000000),
    .WRITE_MODE_A("READ_FIRST"),
    .WRITE_MODE_B("WRITE_FIRST"),
    .WRITE_WIDTH_A(4),
    .WRITE_WIDTH_B(4)) 
    tm_reg_0
       (.ADDRARDADDR({1'b1,ADDRARDADDR[11:4],tm_reg_0_i_4_n_0,ADDRARDADDR[3:0],1'b0,1'b0}),
        .ADDRBWRADDR({1'b1,ADDRBWRADDR[11:4],tm_reg_0_i_8_n_0,ADDRBWRADDR[3:0],1'b0,1'b0}),
        .CASCADEINA(1'b1),
        .CASCADEINB(1'b1),
        .CASCADEOUTA(NLW_tm_reg_0_CASCADEOUTA_UNCONNECTED),
        .CASCADEOUTB(NLW_tm_reg_0_CASCADEOUTB_UNCONNECTED),
        .CLKARDCLK(clk),
        .CLKBWRCLK(clk_0),
        .DBITERR(NLW_tm_reg_0_DBITERR_UNCONNECTED),
        .DIADI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,\tile_out_reg[3] }),
        .DIBDI({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b1,1'b1,1'b1,1'b1}),
        .DIPADIP({1'b0,1'b0,1'b0,1'b0}),
        .DIPBDIP({1'b0,1'b0,1'b0,1'b0}),
        .DOADO(NLW_tm_reg_0_DOADO_UNCONNECTED[31:0]),
        .DOBDO({NLW_tm_reg_0_DOBDO_UNCONNECTED[31:4],tile_id}),
        .DOPADOP(NLW_tm_reg_0_DOPADOP_UNCONNECTED[3:0]),
        .DOPBDOP(NLW_tm_reg_0_DOPBDOP_UNCONNECTED[3:0]),
        .ECCPARITY(NLW_tm_reg_0_ECCPARITY_UNCONNECTED[7:0]),
        .ENARDEN(WEA),
        .ENBWREN(1'b1),
        .INJECTDBITERR(NLW_tm_reg_0_INJECTDBITERR_UNCONNECTED),
        .INJECTSBITERR(NLW_tm_reg_0_INJECTSBITERR_UNCONNECTED),
        .RDADDRECC(NLW_tm_reg_0_RDADDRECC_UNCONNECTED[8:0]),
        .REGCEAREGCE(1'b0),
        .REGCEB(1'b0),
        .RSTRAMARSTRAM(1'b0),
        .RSTRAMB(1'b0),
        .RSTREGARSTREG(1'b0),
        .RSTREGB(1'b0),
        .SBITERR(NLW_tm_reg_0_SBITERR_UNCONNECTED),
        .WEA({WEA,WEA,WEA,1'b1}),
        .WEBWE({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}));
  LUT1 #(
    .INIT(2'h2)) 
    tm_reg_0_i_10
       (.I0(\Ymap_reg[3] [6]),
        .O(tm_reg_0_1[3]));
  LUT1 #(
    .INIT(2'h2)) 
    tm_reg_0_i_11
       (.I0(\Ymap_reg[3] [5]),
        .O(tm_reg_0_1[2]));
  LUT1 #(
    .INIT(2'h2)) 
    tm_reg_0_i_12
       (.I0(\Ymap_reg[3] [4]),
        .O(tm_reg_0_1[1]));
  LUT1 #(
    .INIT(2'h2)) 
    tm_reg_0_i_13
       (.I0(\Ymap_reg[3] [3]),
        .O(tm_reg_0_1[0]));
  LUT1 #(
    .INIT(2'h2)) 
    tm_reg_0_i_14
       (.I0(\Ymap_reg[3] [2]),
        .O(tm_reg_0_0[3]));
  LUT2 #(
    .INIT(4'h6)) 
    tm_reg_0_i_15
       (.I0(\Xmap_reg[6] [2]),
        .I1(\Ymap_reg[3] [1]),
        .O(tm_reg_0_0[2]));
  LUT2 #(
    .INIT(4'h6)) 
    tm_reg_0_i_16
       (.I0(\Xmap_reg[6] [1]),
        .I1(\Ymap_reg[3] [0]),
        .O(tm_reg_0_0[1]));
  LUT2 #(
    .INIT(4'h6)) 
    tm_reg_0_i_17
       (.I0(\Xmap_reg[6] [0]),
        .I1(Q),
        .O(tm_reg_0_0[0]));
  LUT1 #(
    .INIT(2'h2)) 
    tm_reg_0_i_19
       (.I0(\addr_Y_reg[3] [6]),
        .O(tm_reg_0_3[3]));
  LUT1 #(
    .INIT(2'h2)) 
    tm_reg_0_i_20
       (.I0(\addr_Y_reg[3] [5]),
        .O(tm_reg_0_3[2]));
  LUT1 #(
    .INIT(2'h2)) 
    tm_reg_0_i_21
       (.I0(\addr_Y_reg[3] [4]),
        .O(tm_reg_0_3[1]));
  LUT1 #(
    .INIT(2'h2)) 
    tm_reg_0_i_22
       (.I0(\addr_Y_reg[3] [3]),
        .O(tm_reg_0_3[0]));
  LUT1 #(
    .INIT(2'h2)) 
    tm_reg_0_i_23
       (.I0(\addr_Y_reg[3] [2]),
        .O(tm_reg_0_2[3]));
  LUT1 #(
    .INIT(2'h2)) 
    tm_reg_0_i_24
       (.I0(\addr_Y_reg[3] [1]),
        .O(tm_reg_0_2[2]));
  LUT2 #(
    .INIT(4'h6)) 
    tm_reg_0_i_25
       (.I0(\addr_X_reg[5] [1]),
        .I1(\addr_Y_reg[3] [0]),
        .O(tm_reg_0_2[1]));
  LUT2 #(
    .INIT(4'h6)) 
    tm_reg_0_i_26
       (.I0(\addr_X_reg[5] [0]),
        .I1(\addr_Y_reg[5] [0]),
        .O(tm_reg_0_2[0]));
  LUT2 #(
    .INIT(4'h6)) 
    tm_reg_0_i_4
       (.I0(\Xmap_reg[6] [0]),
        .I1(Q),
        .O(tm_reg_0_i_4_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    tm_reg_0_i_40
       (.I0(\addr_Y_reg[5] [3]),
        .I1(\addr_Y_reg[5] [5]),
        .O(tm_reg_0_4));
  LUT2 #(
    .INIT(4'h6)) 
    tm_reg_0_i_41
       (.I0(\addr_Y_reg[5] [2]),
        .I1(\addr_Y_reg[5] [4]),
        .O(S[2]));
  LUT2 #(
    .INIT(4'h6)) 
    tm_reg_0_i_42
       (.I0(\addr_Y_reg[5] [1]),
        .I1(\addr_Y_reg[5] [3]),
        .O(S[1]));
  LUT2 #(
    .INIT(4'h6)) 
    tm_reg_0_i_43
       (.I0(\addr_Y_reg[5] [0]),
        .I1(\addr_Y_reg[5] [2]),
        .O(S[0]));
  LUT2 #(
    .INIT(4'h6)) 
    tm_reg_0_i_8
       (.I0(\addr_X_reg[5] [0]),
        .I1(\addr_Y_reg[5] [0]),
        .O(tm_reg_0_i_8_n_0));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_top
   (fetch,
    data_type,
    led0,
    led1,
    led2,
    led3,
    O,
    \Xmap_reg[4] ,
    \Xmap_reg[4]_0 ,
    \Xmap_reg[4]_1 ,
    \Xmap_reg[4]_2 ,
    \Xmap_reg[4]_3 ,
    \Xmap_reg[4]_4 ,
    \Xmap_reg[4]_5 ,
    \Xmap_reg[4]_6 ,
    \Xmap_reg[4]_7 ,
    \Xmap_reg[4]_8 ,
    \Xmap_reg[4]_9 ,
    \Xmap_reg[4]_10 ,
    \Xmap_reg[4]_11 ,
    \Xmap_reg[4]_12 ,
    \Xmap_reg[4]_13 ,
    \Xmap_reg[4]_14 ,
    \Xmap_reg[4]_15 ,
    \Xmap_reg[4]_16 ,
    \Xmap_reg[4]_17 ,
    \Xmap_reg[4]_18 ,
    \Xmap_reg[4]_19 ,
    \Xmap_reg[4]_20 ,
    \Xmap_reg[4]_21 ,
    \Xmap_reg[4]_22 ,
    \Xmap_reg[4]_23 ,
    \Xmap_reg[4]_24 ,
    map_id,
    vga_r,
    vga_g,
    vga_b,
    vga_hs,
    vga_vs,
    clk_0,
    clk,
    packet_in,
    fetching,
    S,
    \cnt_reg[2]_0 ,
    \cnt_reg[17] ,
    \cnt_reg[21] ,
    \cnt_reg[20] ,
    \cnt_reg[1]_0 ,
    \cnt_reg[1]_1 ,
    DI,
    \cnt_reg[2]_1 ,
    \cnt_reg[17]_0 ,
    \cnt_reg[2]_2 ,
    \cnt_reg[20]_0 ,
    \cnt_reg[20]_1 ,
    sw,
    \cnt_reg[20]_2 ,
    pixel_clk,
    lopt);
  output fetch;
  output data_type;
  output led0;
  output led1;
  output led2;
  output led3;
  output [3:0]O;
  output [3:0]\Xmap_reg[4] ;
  output [3:0]\Xmap_reg[4]_0 ;
  output [2:0]\Xmap_reg[4]_1 ;
  output [2:0]\Xmap_reg[4]_2 ;
  output [3:0]\Xmap_reg[4]_3 ;
  output [0:0]\Xmap_reg[4]_4 ;
  output [2:0]\Xmap_reg[4]_5 ;
  output [3:0]\Xmap_reg[4]_6 ;
  output [3:0]\Xmap_reg[4]_7 ;
  output [3:0]\Xmap_reg[4]_8 ;
  output [0:0]\Xmap_reg[4]_9 ;
  output [3:0]\Xmap_reg[4]_10 ;
  output [3:0]\Xmap_reg[4]_11 ;
  output [3:0]\Xmap_reg[4]_12 ;
  output [0:0]\Xmap_reg[4]_13 ;
  output [3:0]\Xmap_reg[4]_14 ;
  output [3:0]\Xmap_reg[4]_15 ;
  output [3:0]\Xmap_reg[4]_16 ;
  output [3:0]\Xmap_reg[4]_17 ;
  output [3:0]\Xmap_reg[4]_18 ;
  output [3:0]\Xmap_reg[4]_19 ;
  output [3:0]\Xmap_reg[4]_20 ;
  output [3:0]\Xmap_reg[4]_21 ;
  output [3:0]\Xmap_reg[4]_22 ;
  output [0:0]\Xmap_reg[4]_23 ;
  output [2:0]\Xmap_reg[4]_24 ;
  output [6:0]map_id;
  output [2:0]vga_r;
  output [3:0]vga_g;
  output [2:0]vga_b;
  output vga_hs;
  output vga_vs;
  input clk_0;
  input clk;
  input [5:0]packet_in;
  input fetching;
  input [2:0]S;
  input [3:0]\cnt_reg[2]_0 ;
  input [3:0]\cnt_reg[17] ;
  input [3:0]\cnt_reg[21] ;
  input [0:0]\cnt_reg[20] ;
  input [3:0]\cnt_reg[1]_0 ;
  input [3:0]\cnt_reg[1]_1 ;
  input [3:0]DI;
  input [3:0]\cnt_reg[2]_1 ;
  input [3:0]\cnt_reg[17]_0 ;
  input [3:0]\cnt_reg[2]_2 ;
  input [1:0]\cnt_reg[20]_0 ;
  input [2:0]\cnt_reg[20]_1 ;
  input [2:0]sw;
  input [2:0]\cnt_reg[20]_2 ;
  input pixel_clk;
  input lopt;

  wire [3:0]DI;
  wire [3:0]O;
  wire [2:0]S;
  wire [6:0]Xmap;
  wire [3:0]\Xmap_reg[4] ;
  wire [3:0]\Xmap_reg[4]_0 ;
  wire [2:0]\Xmap_reg[4]_1 ;
  wire [3:0]\Xmap_reg[4]_10 ;
  wire [3:0]\Xmap_reg[4]_11 ;
  wire [3:0]\Xmap_reg[4]_12 ;
  wire [0:0]\Xmap_reg[4]_13 ;
  wire [3:0]\Xmap_reg[4]_14 ;
  wire [3:0]\Xmap_reg[4]_15 ;
  wire [3:0]\Xmap_reg[4]_16 ;
  wire [3:0]\Xmap_reg[4]_17 ;
  wire [3:0]\Xmap_reg[4]_18 ;
  wire [3:0]\Xmap_reg[4]_19 ;
  wire [2:0]\Xmap_reg[4]_2 ;
  wire [3:0]\Xmap_reg[4]_20 ;
  wire [3:0]\Xmap_reg[4]_21 ;
  wire [3:0]\Xmap_reg[4]_22 ;
  wire [0:0]\Xmap_reg[4]_23 ;
  wire [2:0]\Xmap_reg[4]_24 ;
  wire [3:0]\Xmap_reg[4]_3 ;
  wire [0:0]\Xmap_reg[4]_4 ;
  wire [2:0]\Xmap_reg[4]_5 ;
  wire [3:0]\Xmap_reg[4]_6 ;
  wire [3:0]\Xmap_reg[4]_7 ;
  wire [3:0]\Xmap_reg[4]_8 ;
  wire [0:0]\Xmap_reg[4]_9 ;
  wire [0:0]Ymap;
  wire [5:0]addr_X;
  wire [5:0]addr_Y;
  wire boot_n_100;
  wire boot_n_109;
  wire boot_n_110;
  wire boot_n_111;
  wire boot_n_112;
  wire boot_n_113;
  wire boot_n_114;
  wire boot_n_115;
  wire boot_n_116;
  wire clk;
  wire clk_0;
  wire [5:0]cnt;
  wire \cnt_reg[0]_i_1_n_0 ;
  wire [3:0]\cnt_reg[17] ;
  wire [3:0]\cnt_reg[17]_0 ;
  wire [3:0]\cnt_reg[1]_0 ;
  wire [3:0]\cnt_reg[1]_1 ;
  wire \cnt_reg[1]_i_1_n_0 ;
  wire [0:0]\cnt_reg[20] ;
  wire [1:0]\cnt_reg[20]_0 ;
  wire [2:0]\cnt_reg[20]_1 ;
  wire [2:0]\cnt_reg[20]_2 ;
  wire [3:0]\cnt_reg[21] ;
  wire [3:0]\cnt_reg[2]_0 ;
  wire [3:0]\cnt_reg[2]_1 ;
  wire [3:0]\cnt_reg[2]_2 ;
  wire \cnt_reg[2]_i_1_n_0 ;
  wire \cnt_reg[3]_i_1_n_0 ;
  wire \cnt_reg[4]_i_1_n_0 ;
  wire \cnt_reg[5]_i_1_n_0 ;
  wire \cnt_reg[5]_i_2_n_0 ;
  wire [2:0]current_tile;
  wire [1:0]current_tile__0;
  wire data_type;
  wire fetch;
  wire fetching;
  wire fetching_sprites;
  wire [3:0]h_cnt;
  wire [11:11]ind_reg;
  wire led0;
  wire led1;
  wire led2;
  wire led3;
  wire line_complete0_out;
  wire lopt;
  wire [6:0]map_id;
  wire [3:0]out_tile;
  wire [11:5]out_tile2;
  wire [11:5]p_0_out;
  wire [9:4]p_0_out_0;
  wire [5:0]packet_in;
  wire [5:0]pixel;
  wire [15:2]pixel_bus;
  wire pixel_clk;
  wire [6:0]random;
  wire \random_reg[6]_i_1_n_0 ;
  wire \random_reg[6]_i_2_n_0 ;
  wire rend_n_1;
  wire rend_n_16;
  wire rend_n_17;
  wire rend_n_2;
  wire rend_n_21;
  wire rend_n_24;
  wire rend_n_25;
  wire rend_n_26;
  wire rend_n_27;
  wire rend_n_28;
  wire rend_n_29;
  wire rend_n_30;
  wire rend_n_31;
  wire rend_n_48;
  wire rend_n_49;
  wire render_enable;
  wire [2:0]sw;
  wire [3:0]tile_in;
  wire tm_n_10;
  wire tm_n_11;
  wire tm_n_12;
  wire tm_n_13;
  wire tm_n_14;
  wire tm_n_15;
  wire tm_n_16;
  wire tm_n_17;
  wire tm_n_18;
  wire tm_n_19;
  wire tm_n_20;
  wire tm_n_21;
  wire tm_n_22;
  wire tm_n_23;
  wire tm_n_4;
  wire tm_n_5;
  wire tm_n_6;
  wire tm_n_7;
  wire tm_n_8;
  wire tm_n_9;
  wire [6:0]tmp_rand;
  wire [3:0]v_cnt;
  wire [2:0]vga_b;
  wire [3:0]vga_g;
  wire vga_hs;
  wire vga_n_0;
  wire vga_n_10;
  wire vga_n_14;
  wire vga_n_15;
  wire vga_n_2;
  wire vga_n_22;
  wire vga_n_23;
  wire vga_n_24;
  wire vga_n_25;
  wire vga_n_26;
  wire vga_n_27;
  wire vga_n_28;
  wire vga_n_29;
  wire vga_n_3;
  wire vga_n_42;
  wire vga_n_43;
  wire vga_n_44;
  wire vga_n_45;
  wire vga_n_46;
  wire vga_n_47;
  wire vga_n_48;
  wire vga_n_49;
  wire vga_n_50;
  wire vga_n_51;
  wire [2:0]vga_r;
  wire vga_vs;
  wire write_enable;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_booting boot
       (.ADDRARDADDR({boot_n_109,boot_n_110,boot_n_111,boot_n_112,boot_n_113,boot_n_114,boot_n_115,boot_n_116,Xmap[3:0]}),
        .D(tmp_rand),
        .DI(DI),
        .O(O),
        .O34(ind_reg),
        .Q(Ymap),
        .S(S),
        .WEA(write_enable),
        .\Xmap_reg[4]_0 (\Xmap_reg[4] ),
        .\Xmap_reg[4]_1 (\Xmap_reg[4]_0 ),
        .\Xmap_reg[4]_10 (\Xmap_reg[4]_9 ),
        .\Xmap_reg[4]_11 (\Xmap_reg[4]_10 ),
        .\Xmap_reg[4]_12 (\Xmap_reg[4]_11 ),
        .\Xmap_reg[4]_13 (\Xmap_reg[4]_12 ),
        .\Xmap_reg[4]_14 (\Xmap_reg[4]_13 ),
        .\Xmap_reg[4]_15 (\Xmap_reg[4]_14 ),
        .\Xmap_reg[4]_16 (\Xmap_reg[4]_15 ),
        .\Xmap_reg[4]_17 (\Xmap_reg[4]_16 ),
        .\Xmap_reg[4]_18 (\Xmap_reg[4]_17 ),
        .\Xmap_reg[4]_19 (\Xmap_reg[4]_18 ),
        .\Xmap_reg[4]_2 (\Xmap_reg[4]_1 ),
        .\Xmap_reg[4]_20 (\Xmap_reg[4]_19 ),
        .\Xmap_reg[4]_21 (\Xmap_reg[4]_20 ),
        .\Xmap_reg[4]_22 (\Xmap_reg[4]_21 ),
        .\Xmap_reg[4]_23 (\Xmap_reg[4]_22 ),
        .\Xmap_reg[4]_24 (\Xmap_reg[4]_23 ),
        .\Xmap_reg[4]_25 (\Xmap_reg[4]_24 ),
        .\Xmap_reg[4]_3 (\Xmap_reg[4]_2 ),
        .\Xmap_reg[4]_4 (\Xmap_reg[4]_3 ),
        .\Xmap_reg[4]_5 (\Xmap_reg[4]_4 ),
        .\Xmap_reg[4]_6 (\Xmap_reg[4]_5 ),
        .\Xmap_reg[4]_7 (\Xmap_reg[4]_6 ),
        .\Xmap_reg[4]_8 (\Xmap_reg[4]_7 ),
        .\Xmap_reg[4]_9 (\Xmap_reg[4]_8 ),
        .\Ymap_reg[2]_0 ({tm_n_4,tm_n_5,tm_n_6,tm_n_7}),
        .\Ymap_reg[3]_0 ({tm_n_8,tm_n_9,tm_n_10,tm_n_11}),
        .clk(clk),
        .\cnt_reg[17]_0 (\cnt_reg[17] ),
        .\cnt_reg[17]_1 (\cnt_reg[17]_0 ),
        .\cnt_reg[1]_0 (\cnt_reg[1]_0 ),
        .\cnt_reg[1]_1 (\cnt_reg[1]_1 ),
        .\cnt_reg[20]_0 (\cnt_reg[20] ),
        .\cnt_reg[20]_1 (\cnt_reg[20]_0 ),
        .\cnt_reg[20]_2 (\cnt_reg[20]_1 ),
        .\cnt_reg[20]_3 (\cnt_reg[20]_2 ),
        .\cnt_reg[21]_0 (\cnt_reg[21] ),
        .\cnt_reg[2]_0 (\cnt_reg[2]_0 ),
        .\cnt_reg[2]_1 (\cnt_reg[2]_1 ),
        .\cnt_reg[2]_2 (\cnt_reg[2]_2 ),
        .data_type(data_type),
        .fetch(fetch),
        .fetching(fetching),
        .fetching_sprites(fetching_sprites),
        .\ind_reg[0] (boot_n_100),
        .led0(led0),
        .led1(led1),
        .led2(led2),
        .led3(led3),
        .map_id(map_id),
        .packet_in(packet_in),
        .pixel(pixel),
        .sw(sw),
        .tm_reg_0(p_0_out),
        .tm_reg_0_0(Xmap[6:4]),
        .tm_reg_0_1(tile_in),
        .\tmp_rand_reg[6]_0 (random));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_reg[0] 
       (.CLR(1'b0),
        .D(\cnt_reg[0]_i_1_n_0 ),
        .G(\cnt_reg[5]_i_2_n_0 ),
        .GE(1'b1),
        .Q(cnt[0]));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt_reg[0]_i_1 
       (.I0(cnt[0]),
        .O(\cnt_reg[0]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_reg[1] 
       (.CLR(1'b0),
        .D(\cnt_reg[1]_i_1_n_0 ),
        .G(\cnt_reg[5]_i_2_n_0 ),
        .GE(1'b1),
        .Q(cnt[1]));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \cnt_reg[1]_i_1 
       (.I0(cnt[0]),
        .I1(cnt[1]),
        .O(\cnt_reg[1]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_reg[2] 
       (.CLR(1'b0),
        .D(\cnt_reg[2]_i_1_n_0 ),
        .G(\cnt_reg[5]_i_2_n_0 ),
        .GE(1'b1),
        .Q(cnt[2]));
  (* SOFT_HLUTNM = "soft_lutpair45" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \cnt_reg[2]_i_1 
       (.I0(cnt[0]),
        .I1(cnt[1]),
        .I2(cnt[2]),
        .O(\cnt_reg[2]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_reg[3] 
       (.CLR(1'b0),
        .D(\cnt_reg[3]_i_1_n_0 ),
        .G(\cnt_reg[5]_i_2_n_0 ),
        .GE(1'b1),
        .Q(cnt[3]));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \cnt_reg[3]_i_1 
       (.I0(cnt[1]),
        .I1(cnt[0]),
        .I2(cnt[2]),
        .I3(cnt[3]),
        .O(\cnt_reg[3]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_reg[4] 
       (.CLR(1'b0),
        .D(\cnt_reg[4]_i_1_n_0 ),
        .G(\cnt_reg[5]_i_2_n_0 ),
        .GE(1'b1),
        .Q(cnt[4]));
  (* SOFT_HLUTNM = "soft_lutpair43" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \cnt_reg[4]_i_1 
       (.I0(cnt[2]),
        .I1(cnt[0]),
        .I2(cnt[1]),
        .I3(cnt[3]),
        .I4(cnt[4]),
        .O(\cnt_reg[4]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \cnt_reg[5] 
       (.CLR(1'b0),
        .D(\cnt_reg[5]_i_1_n_0 ),
        .G(\cnt_reg[5]_i_2_n_0 ),
        .GE(1'b1),
        .Q(cnt[5]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \cnt_reg[5]_i_1 
       (.I0(cnt[3]),
        .I1(cnt[1]),
        .I2(cnt[0]),
        .I3(cnt[2]),
        .I4(cnt[4]),
        .I5(cnt[5]),
        .O(\cnt_reg[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT3 #(
    .INIT(8'hE0)) 
    \cnt_reg[5]_i_2 
       (.I0(sw[1]),
        .I1(sw[0]),
        .I2(\random_reg[6]_i_2_n_0 ),
        .O(\cnt_reg[5]_i_2_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \random_reg[0] 
       (.CLR(1'b0),
        .D(tmp_rand[0]),
        .G(\random_reg[6]_i_1_n_0 ),
        .GE(1'b1),
        .Q(random[0]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \random_reg[1] 
       (.CLR(1'b0),
        .D(tmp_rand[1]),
        .G(\random_reg[6]_i_1_n_0 ),
        .GE(1'b1),
        .Q(random[1]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \random_reg[2] 
       (.CLR(1'b0),
        .D(tmp_rand[2]),
        .G(\random_reg[6]_i_1_n_0 ),
        .GE(1'b1),
        .Q(random[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \random_reg[3] 
       (.CLR(1'b0),
        .D(tmp_rand[3]),
        .G(\random_reg[6]_i_1_n_0 ),
        .GE(1'b1),
        .Q(random[3]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \random_reg[4] 
       (.CLR(1'b0),
        .D(tmp_rand[4]),
        .G(\random_reg[6]_i_1_n_0 ),
        .GE(1'b1),
        .Q(random[4]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \random_reg[5] 
       (.CLR(1'b0),
        .D(tmp_rand[5]),
        .G(\random_reg[6]_i_1_n_0 ),
        .GE(1'b1),
        .Q(random[5]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \random_reg[6] 
       (.CLR(1'b0),
        .D(tmp_rand[6]),
        .G(\random_reg[6]_i_1_n_0 ),
        .GE(1'b1),
        .Q(random[6]));
  (* SOFT_HLUTNM = "soft_lutpair44" *) 
  LUT3 #(
    .INIT(8'h0E)) 
    \random_reg[6]_i_1 
       (.I0(sw[1]),
        .I1(sw[0]),
        .I2(\random_reg[6]_i_2_n_0 ),
        .O(\random_reg[6]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00007FFF)) 
    \random_reg[6]_i_2 
       (.I0(cnt[4]),
        .I1(cnt[3]),
        .I2(cnt[1]),
        .I3(cnt[2]),
        .I4(cnt[5]),
        .O(\random_reg[6]_i_2_n_0 ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_renderer rend
       (.ADDRA({vga_n_48,vga_n_49}),
        .ADDRB({vga_n_50,vga_n_51}),
        .ADDRBWRADDR({rend_n_24,rend_n_25,rend_n_26,rend_n_27,rend_n_28,rend_n_29,rend_n_30,rend_n_31,addr_X[3:0]}),
        .ADDRC({v_cnt[1:0],h_cnt}),
        .D({vga_n_26,vga_n_27}),
        .I7(vga_n_10),
        .O34(ind_reg),
        .Q(rend_n_2),
        .S({tm_n_20,tm_n_21,tm_n_22}),
        .SR(line_complete0_out),
        .\addr_X_reg[0]_0 (rend_n_1),
        .\addr_Y_reg[0]_0 (rend_n_17),
        .\addr_Y_reg[0]_1 (rend_n_21),
        .\addr_Y_reg[2]_0 ({tm_n_12,tm_n_13,tm_n_14,tm_n_15}),
        .\addr_Y_reg[3]_0 (tm_n_23),
        .\addr_Y_reg[3]_1 ({tm_n_16,tm_n_17,tm_n_18,tm_n_19}),
        .clk(clk),
        .clk_0(clk_0),
        .\current_tile_reg[1]_0 (current_tile__0),
        .\current_tile_reg[1]_1 (vga_n_23),
        .\current_tile_reg[3]_0 (current_tile),
        .fetching_sprites(fetching_sprites),
        .fetching_sprites_reg(boot_n_100),
        .\h_cnt_reg[1] (vga_n_28),
        .\h_cnt_reg[1]_0 (vga_n_29),
        .\h_cnt_reg[6] (vga_n_3),
        .\h_cnt_reg[6]_0 (vga_n_24),
        .\h_cnt_reg[7] (vga_n_25),
        .\isFinder_reg[0]_0 (rend_n_48),
        .\isFinder_reg[1]_0 (rend_n_49),
        .lopt(lopt),
        .pixel(pixel),
        .pixel_bus({pixel_bus[15:12],pixel_bus[9:7],pixel_bus[4:2]}),
        .\pixel_bus_reg[2]_0 (rend_n_16),
        .pixel_clk(pixel_clk),
        .render_enable(render_enable),
        .render_enable_reg(vga_n_2),
        .render_enable_reg_0(vga_n_0),
        .sw(sw[2]),
        .tile_id(out_tile),
        .tm_reg_0(addr_Y),
        .tm_reg_0_0(out_tile2),
        .tm_reg_0_1(addr_X[5:4]),
        .v_cnt(v_cnt[3:2]),
        .\v_cnt_reg[1]_rep ({vga_n_42,vga_n_43,vga_n_44,vga_n_45,vga_n_46,vga_n_47}),
        .\v_cnt_reg[1]_rep__1 ({vga_n_14,vga_n_15}),
        .\v_cnt_reg[8] (vga_n_22),
        .\v_cnt_reg[9] (p_0_out_0));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_tile_manager tm
       (.ADDRARDADDR({boot_n_109,boot_n_110,boot_n_111,boot_n_112,boot_n_113,boot_n_114,boot_n_115,boot_n_116,Xmap[3:0]}),
        .ADDRBWRADDR({rend_n_24,rend_n_25,rend_n_26,rend_n_27,rend_n_28,rend_n_29,rend_n_30,rend_n_31,addr_X[3:0]}),
        .Q(Ymap),
        .S({tm_n_20,tm_n_21,tm_n_22}),
        .WEA(write_enable),
        .\Xmap_reg[6] (Xmap[6:4]),
        .\Ymap_reg[3] (p_0_out),
        .\addr_X_reg[5] (addr_X[5:4]),
        .\addr_Y_reg[3] (out_tile2),
        .\addr_Y_reg[5] (addr_Y),
        .clk(clk),
        .clk_0(clk_0),
        .tile_id(out_tile),
        .\tile_out_reg[3] (tile_in),
        .tm_reg_0_0({tm_n_4,tm_n_5,tm_n_6,tm_n_7}),
        .tm_reg_0_1({tm_n_8,tm_n_9,tm_n_10,tm_n_11}),
        .tm_reg_0_2({tm_n_12,tm_n_13,tm_n_14,tm_n_15}),
        .tm_reg_0_3({tm_n_16,tm_n_17,tm_n_18,tm_n_19}),
        .tm_reg_0_4(tm_n_23));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vga_connector vga
       (.ADDRA({vga_n_48,vga_n_49}),
        .ADDRB({vga_n_50,vga_n_51}),
        .ADDRC({v_cnt[1:0],h_cnt}),
        .D({vga_n_26,vga_n_27}),
        .I7(vga_n_10),
        .Q(v_cnt[3:2]),
        .SR(line_complete0_out),
        .\addr_Y_reg[0] (vga_n_3),
        .\addr_Y_reg[0]_0 (vga_n_25),
        .\current_tile_reg[1] (current_tile__0),
        .\current_tile_reg[2] (vga_n_28),
        .\current_tile_reg[2]_0 (current_tile),
        .\current_tile_reg[3] (vga_n_24),
        .\current_tile_reg[3]_0 (rend_n_16),
        .\current_tile_reg[5] (vga_n_29),
        .\isFinder_reg[0] (rend_n_48),
        .\isFinder_reg[1] (rend_n_49),
        .line_complete_reg(rend_n_17),
        .pixel_bus({pixel_bus[15:12],pixel_bus[9:7],pixel_bus[4:2]}),
        .\pixel_bus_reg[14] ({vga_n_42,vga_n_43,vga_n_44,vga_n_45,vga_n_46,vga_n_47}),
        .\pixel_bus_reg[2] (vga_n_23),
        .\pixel_bus_reg[7] (vga_n_0),
        .pixel_clk(pixel_clk),
        .render_enable(render_enable),
        .\tile_row_write_counter_reg[0] ({vga_n_14,vga_n_15}),
        .\tile_row_write_counter_reg[3] (rend_n_21),
        .\tile_row_write_counter_reg[5] (p_0_out_0),
        .\tile_row_write_counter_reg[5]_0 (vga_n_22),
        .\tile_row_write_counter_reg[5]_1 (rend_n_2),
        .tile_wrote_reg(vga_n_2),
        .tile_wrote_reg_0(rend_n_1),
        .vga_b(vga_b),
        .vga_g(vga_g),
        .vga_hs(vga_hs),
        .vga_r(vga_r),
        .vga_vs(vga_vs));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vga_connector
   (\pixel_bus_reg[7] ,
    render_enable,
    tile_wrote_reg,
    \addr_Y_reg[0] ,
    ADDRC,
    I7,
    SR,
    Q,
    \tile_row_write_counter_reg[0] ,
    \tile_row_write_counter_reg[5] ,
    \tile_row_write_counter_reg[5]_0 ,
    \pixel_bus_reg[2] ,
    \current_tile_reg[3] ,
    \addr_Y_reg[0]_0 ,
    D,
    \current_tile_reg[2] ,
    \current_tile_reg[5] ,
    vga_r,
    vga_g,
    vga_b,
    vga_hs,
    vga_vs,
    \pixel_bus_reg[14] ,
    ADDRA,
    ADDRB,
    \current_tile_reg[3]_0 ,
    tile_wrote_reg_0,
    line_complete_reg,
    \tile_row_write_counter_reg[3] ,
    \current_tile_reg[2]_0 ,
    \isFinder_reg[1] ,
    \isFinder_reg[0] ,
    \tile_row_write_counter_reg[5]_1 ,
    \current_tile_reg[1] ,
    pixel_clk,
    pixel_bus);
  output \pixel_bus_reg[7] ;
  output render_enable;
  output tile_wrote_reg;
  output \addr_Y_reg[0] ;
  output [5:0]ADDRC;
  output I7;
  output [0:0]SR;
  output [1:0]Q;
  output [1:0]\tile_row_write_counter_reg[0] ;
  output [5:0]\tile_row_write_counter_reg[5] ;
  output [0:0]\tile_row_write_counter_reg[5]_0 ;
  output \pixel_bus_reg[2] ;
  output \current_tile_reg[3] ;
  output \addr_Y_reg[0]_0 ;
  output [1:0]D;
  output \current_tile_reg[2] ;
  output \current_tile_reg[5] ;
  output [2:0]vga_r;
  output [3:0]vga_g;
  output [2:0]vga_b;
  output vga_hs;
  output vga_vs;
  output [5:0]\pixel_bus_reg[14] ;
  output [1:0]ADDRA;
  output [1:0]ADDRB;
  input \current_tile_reg[3]_0 ;
  input tile_wrote_reg_0;
  input line_complete_reg;
  input \tile_row_write_counter_reg[3] ;
  input [2:0]\current_tile_reg[2]_0 ;
  input \isFinder_reg[1] ;
  input \isFinder_reg[0] ;
  input [0:0]\tile_row_write_counter_reg[5]_1 ;
  input [1:0]\current_tile_reg[1] ;
  input pixel_clk;
  input [9:0]pixel_bus;

  wire [1:0]ADDRA;
  wire [1:0]ADDRB;
  wire [5:0]ADDRC;
  wire [1:0]D;
  wire HSYNC_i_1_n_0;
  wire HSYNC_i_2_n_0;
  wire I7;
  wire [1:0]Q;
  wire [0:0]SR;
  wire VSYNC_i_1_n_0;
  wire VSYNC_i_2_n_0;
  wire \addr_X[5]_i_10_n_0 ;
  wire \addr_X[5]_i_11_n_0 ;
  wire \addr_X[5]_i_5_n_0 ;
  wire \addr_X[5]_i_6_n_0 ;
  wire \addr_X[5]_i_9_n_0 ;
  wire \addr_Y_reg[0] ;
  wire \addr_Y_reg[0]_0 ;
  wire [1:0]cnt;
  wire \cnt[0]_i_1_n_0 ;
  wire \cnt[1]_i_1_n_0 ;
  wire [1:0]\current_tile_reg[1] ;
  wire \current_tile_reg[2] ;
  wire [2:0]\current_tile_reg[2]_0 ;
  wire \current_tile_reg[3] ;
  wire \current_tile_reg[3]_0 ;
  wire \current_tile_reg[5] ;
  wire [9:4]h_cnt;
  wire \isFinder_reg[0] ;
  wire \isFinder_reg[1] ;
  wire line_complete_reg;
  wire [9:0]p_0_in;
  wire [9:0]pixel_bus;
  wire \pixel_bus[4]_i_28_n_0 ;
  wire \pixel_bus[4]_i_29_n_0 ;
  wire \pixel_bus[4]_i_54_n_0 ;
  wire \pixel_bus[4]_i_55_n_0 ;
  wire [5:0]\pixel_bus_reg[14] ;
  wire \pixel_bus_reg[2] ;
  wire \pixel_bus_reg[7] ;
  wire pixel_clk;
  wire render_enable;
  wire render_enable1;
  wire render_enable_i_1_n_0;
  wire \tile_row_write_counter[5]_i_4_n_0 ;
  wire \tile_row_write_counter[5]_i_5_n_0 ;
  wire \tile_row_write_counter[5]_i_6_n_0 ;
  wire [1:0]\tile_row_write_counter_reg[0] ;
  wire \tile_row_write_counter_reg[3] ;
  wire [5:0]\tile_row_write_counter_reg[5] ;
  wire [0:0]\tile_row_write_counter_reg[5]_0 ;
  wire [0:0]\tile_row_write_counter_reg[5]_1 ;
  wire tile_wrote_reg;
  wire tile_wrote_reg_0;
  wire [9:4]v_cnt;
  wire [2:0]vga_b;
  wire [3:0]vga_g;
  wire vga_hs;
  wire [2:0]vga_r;
  wire \vga_r[4]_i_1_n_0 ;
  wire \vga_r[4]_i_2_n_0 ;
  wire \vga_r[4]_i_3_n_0 ;
  wire vga_vs;
  wire x;
  wire \x[9]_i_3_n_0 ;
  wire [9:0]x_reg__0;
  wire [9:0]y;
  wire \y[0]_i_2_n_0 ;
  wire \y[0]_i_3_n_0 ;
  wire \y[0]_i_4_n_0 ;
  wire \y[0]_i_5_n_0 ;
  wire \y[0]_i_6_n_0 ;
  wire \y[3]_i_2_n_0 ;
  wire \y[4]_i_2_n_0 ;
  wire \y[8]_i_2_n_0 ;
  wire \y[9]_i_3_n_0 ;
  wire \y[9]_i_4_n_0 ;
  wire \y_reg_n_0_[0] ;
  wire \y_reg_n_0_[1] ;
  wire \y_reg_n_0_[2] ;
  wire \y_reg_n_0_[3] ;
  wire \y_reg_n_0_[4] ;
  wire \y_reg_n_0_[5] ;
  wire \y_reg_n_0_[6] ;
  wire \y_reg_n_0_[7] ;
  wire \y_reg_n_0_[8] ;
  wire \y_reg_n_0_[9] ;

  LUT6 #(
    .INIT(64'hD000000DDDDDDDDD)) 
    HSYNC_i_1
       (.I0(\vga_r[4]_i_2_n_0 ),
        .I1(vga_hs),
        .I2(x_reg__0[6]),
        .I3(x_reg__0[4]),
        .I4(x_reg__0[5]),
        .I5(HSYNC_i_2_n_0),
        .O(HSYNC_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT5 #(
    .INIT(32'h08000000)) 
    HSYNC_i_2
       (.I0(x_reg__0[7]),
        .I1(x_reg__0[9]),
        .I2(x_reg__0[8]),
        .I3(cnt[1]),
        .I4(cnt[0]),
        .O(HSYNC_i_2_n_0));
  FDRE #(
    .INIT(1'b1)) 
    HSYNC_reg
       (.C(pixel_clk),
        .CE(1'b1),
        .D(HSYNC_i_1_n_0),
        .Q(vga_hs),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFCFAAAAAAAAAAAA)) 
    VSYNC_i_1
       (.I0(vga_vs),
        .I1(\vga_r[4]_i_3_n_0 ),
        .I2(VSYNC_i_2_n_0),
        .I3(\y_reg_n_0_[9] ),
        .I4(cnt[0]),
        .I5(cnt[1]),
        .O(VSYNC_i_1_n_0));
  LUT4 #(
    .INIT(16'h0400)) 
    VSYNC_i_2
       (.I0(\y_reg_n_0_[2] ),
        .I1(\y_reg_n_0_[1] ),
        .I2(\y_reg_n_0_[4] ),
        .I3(\y_reg_n_0_[3] ),
        .O(VSYNC_i_2_n_0));
  FDRE #(
    .INIT(1'b1)) 
    VSYNC_reg
       (.C(pixel_clk),
        .CE(1'b1),
        .D(VSYNC_i_1_n_0),
        .Q(vga_vs),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0808080808080008)) 
    \addr_X[5]_i_1 
       (.I0(line_complete_reg),
        .I1(\tile_row_write_counter_reg[3] ),
        .I2(\addr_X[5]_i_5_n_0 ),
        .I3(\addr_X[5]_i_6_n_0 ),
        .I4(h_cnt[6]),
        .I5(h_cnt[8]),
        .O(\addr_Y_reg[0] ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \addr_X[5]_i_10 
       (.I0(Q[0]),
        .I1(Q[1]),
        .O(\addr_X[5]_i_10_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \addr_X[5]_i_11 
       (.I0(v_cnt[7]),
        .I1(v_cnt[6]),
        .I2(v_cnt[4]),
        .I3(v_cnt[8]),
        .O(\addr_X[5]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'h88880F0088880000)) 
    \addr_X[5]_i_5 
       (.I0(v_cnt[8]),
        .I1(\addr_X[5]_i_9_n_0 ),
        .I2(\addr_X[5]_i_10_n_0 ),
        .I3(v_cnt[9]),
        .I4(v_cnt[5]),
        .I5(\addr_X[5]_i_11_n_0 ),
        .O(\addr_X[5]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \addr_X[5]_i_6 
       (.I0(ADDRC[1]),
        .I1(ADDRC[0]),
        .I2(ADDRC[2]),
        .I3(h_cnt[4]),
        .I4(h_cnt[5]),
        .I5(ADDRC[3]),
        .O(\addr_X[5]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'h000E0000)) 
    \addr_X[5]_i_8 
       (.I0(h_cnt[7]),
        .I1(h_cnt[8]),
        .I2(render_enable),
        .I3(\tile_row_write_counter_reg[5]_1 ),
        .I4(h_cnt[9]),
        .O(\addr_Y_reg[0]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT3 #(
    .INIT(8'h40)) 
    \addr_X[5]_i_9 
       (.I0(v_cnt[9]),
        .I1(v_cnt[7]),
        .I2(v_cnt[6]),
        .O(\addr_X[5]_i_9_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT2 #(
    .INIT(4'hB)) 
    \cnt[0]_i_1 
       (.I0(cnt[1]),
        .I1(cnt[0]),
        .O(\cnt[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \cnt[1]_i_1 
       (.I0(cnt[0]),
        .I1(cnt[1]),
        .O(\cnt[1]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[0] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\cnt[0]_i_1_n_0 ),
        .Q(cnt[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[1] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(\cnt[1]_i_1_n_0 ),
        .Q(cnt[1]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \current_tile[0]_i_1 
       (.I0(\current_tile_reg[1] [0]),
        .I1(ADDRC[3]),
        .I2(ADDRC[2]),
        .I3(ADDRC[0]),
        .I4(ADDRC[1]),
        .O(D[0]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \current_tile[1]_i_1 
       (.I0(\current_tile_reg[1] [1]),
        .I1(ADDRC[3]),
        .I2(ADDRC[2]),
        .I3(ADDRC[0]),
        .I4(ADDRC[1]),
        .I5(\current_tile_reg[1] [0]),
        .O(D[1]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \current_tile[4]_i_2 
       (.I0(\current_tile_reg[1] [0]),
        .I1(ADDRC[1]),
        .I2(ADDRC[0]),
        .I3(ADDRC[2]),
        .I4(ADDRC[3]),
        .O(\current_tile_reg[2] ));
  LUT6 #(
    .INIT(64'h0000000000000002)) 
    \current_tile[5]_i_2 
       (.I0(\addr_X[5]_i_6_n_0 ),
        .I1(h_cnt[6]),
        .I2(h_cnt[5]),
        .I3(h_cnt[9]),
        .I4(h_cnt[8]),
        .I5(h_cnt[7]),
        .O(\current_tile_reg[3] ));
  LUT2 #(
    .INIT(4'h8)) 
    \current_tile[5]_i_4 
       (.I0(ADDRC[1]),
        .I1(ADDRC[0]),
        .O(\current_tile_reg[5] ));
  (* ORIG_CELL_NAME = "h_cnt_reg[0]" *) 
  FDRE \h_cnt_reg[0] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(x_reg__0[0]),
        .Q(ADDRC[0]),
        .R(1'b0));
  (* ORIG_CELL_NAME = "h_cnt_reg[0]" *) 
  FDRE \h_cnt_reg[0]_rep 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(x_reg__0[0]),
        .Q(\pixel_bus_reg[14] [0]),
        .R(1'b0));
  (* ORIG_CELL_NAME = "h_cnt_reg[1]" *) 
  FDRE \h_cnt_reg[1] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(x_reg__0[1]),
        .Q(ADDRC[1]),
        .R(1'b0));
  (* ORIG_CELL_NAME = "h_cnt_reg[1]" *) 
  FDRE \h_cnt_reg[1]_rep 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(x_reg__0[1]),
        .Q(\pixel_bus_reg[14] [1]),
        .R(1'b0));
  (* ORIG_CELL_NAME = "h_cnt_reg[2]" *) 
  FDRE \h_cnt_reg[2] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(x_reg__0[2]),
        .Q(ADDRC[2]),
        .R(1'b0));
  (* ORIG_CELL_NAME = "h_cnt_reg[2]" *) 
  FDRE \h_cnt_reg[2]_rep 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(x_reg__0[2]),
        .Q(\pixel_bus_reg[14] [2]),
        .R(1'b0));
  (* ORIG_CELL_NAME = "h_cnt_reg[3]" *) 
  FDRE \h_cnt_reg[3] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(x_reg__0[3]),
        .Q(ADDRC[3]),
        .R(1'b0));
  (* ORIG_CELL_NAME = "h_cnt_reg[3]" *) 
  FDRE \h_cnt_reg[3]_rep 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(x_reg__0[3]),
        .Q(\pixel_bus_reg[14] [3]),
        .R(1'b0));
  FDRE \h_cnt_reg[4] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(x_reg__0[4]),
        .Q(h_cnt[4]),
        .R(1'b0));
  FDRE \h_cnt_reg[5] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(x_reg__0[5]),
        .Q(h_cnt[5]),
        .R(1'b0));
  FDRE \h_cnt_reg[6] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(x_reg__0[6]),
        .Q(h_cnt[6]),
        .R(1'b0));
  FDRE \h_cnt_reg[7] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(x_reg__0[7]),
        .Q(h_cnt[7]),
        .R(1'b0));
  FDRE \h_cnt_reg[8] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(x_reg__0[8]),
        .Q(h_cnt[8]),
        .R(1'b0));
  FDRE \h_cnt_reg[9] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(x_reg__0[9]),
        .Q(h_cnt[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \pixel_bus[15]_i_1 
       (.I0(render_enable),
        .I1(\current_tile_reg[3]_0 ),
        .O(\pixel_bus_reg[7] ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFACF)) 
    \pixel_bus[4]_i_15 
       (.I0(\pixel_bus[4]_i_28_n_0 ),
        .I1(\pixel_bus[4]_i_29_n_0 ),
        .I2(\current_tile_reg[2]_0 [1]),
        .I3(\current_tile_reg[2]_0 [0]),
        .I4(\current_tile_reg[3] ),
        .I5(\current_tile_reg[2]_0 [2]),
        .O(\pixel_bus_reg[2] ));
  LUT6 #(
    .INIT(64'hFFFEA888FFFEAAAA)) 
    \pixel_bus[4]_i_28 
       (.I0(\pixel_bus[4]_i_54_n_0 ),
        .I1(ADDRC[2]),
        .I2(ADDRC[1]),
        .I3(ADDRC[0]),
        .I4(ADDRC[3]),
        .I5(\pixel_bus[4]_i_55_n_0 ),
        .O(\pixel_bus[4]_i_28_n_0 ));
  LUT6 #(
    .INIT(64'h2B3B3BBBABBBBBBB)) 
    \pixel_bus[4]_i_29 
       (.I0(\pixel_bus[4]_i_54_n_0 ),
        .I1(ADDRC[3]),
        .I2(ADDRC[2]),
        .I3(ADDRC[0]),
        .I4(ADDRC[1]),
        .I5(\pixel_bus[4]_i_55_n_0 ),
        .O(\pixel_bus[4]_i_29_n_0 ));
  LUT6 #(
    .INIT(64'hF7FFF7EFF7EFFFEF)) 
    \pixel_bus[4]_i_54 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(\isFinder_reg[1] ),
        .I3(\isFinder_reg[0] ),
        .I4(ADDRC[5]),
        .I5(ADDRC[4]),
        .O(\pixel_bus[4]_i_54_n_0 ));
  LUT6 #(
    .INIT(64'h0000EAAA55570000)) 
    \pixel_bus[4]_i_55 
       (.I0(Q[1]),
        .I1(ADDRC[5]),
        .I2(ADDRC[4]),
        .I3(Q[0]),
        .I4(\isFinder_reg[1] ),
        .I5(\isFinder_reg[0] ),
        .O(\pixel_bus[4]_i_55_n_0 ));
  LUT6 #(
    .INIT(64'h000000000000222A)) 
    render_enable_i_1
       (.I0(\vga_r[4]_i_3_n_0 ),
        .I1(x_reg__0[9]),
        .I2(x_reg__0[7]),
        .I3(x_reg__0[8]),
        .I4(\y_reg_n_0_[9] ),
        .I5(\vga_r[4]_i_2_n_0 ),
        .O(render_enable_i_1_n_0));
  FDRE render_enable_reg
       (.C(pixel_clk),
        .CE(1'b1),
        .D(render_enable_i_1_n_0),
        .Q(render_enable),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \tile_row_write_counter[0]_i_1 
       (.I0(v_cnt[4]),
        .I1(\tile_row_write_counter_reg[0] [0]),
        .I2(\tile_row_write_counter_reg[0] [1]),
        .I3(Q[1]),
        .I4(Q[0]),
        .O(\tile_row_write_counter_reg[5] [0]));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \tile_row_write_counter[1]_i_1 
       (.I0(v_cnt[5]),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(\tile_row_write_counter_reg[0] [1]),
        .I4(\tile_row_write_counter_reg[0] [0]),
        .I5(v_cnt[4]),
        .O(\tile_row_write_counter_reg[5] [1]));
  LUT2 #(
    .INIT(4'h9)) 
    \tile_row_write_counter[2]_i_1 
       (.I0(\tile_row_write_counter[5]_i_6_n_0 ),
        .I1(v_cnt[6]),
        .O(\tile_row_write_counter_reg[5] [2]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT3 #(
    .INIT(8'h9A)) 
    \tile_row_write_counter[3]_i_1 
       (.I0(v_cnt[7]),
        .I1(\tile_row_write_counter[5]_i_6_n_0 ),
        .I2(v_cnt[6]),
        .O(\tile_row_write_counter_reg[5] [3]));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT4 #(
    .INIT(16'hAA6A)) 
    \tile_row_write_counter[4]_i_1 
       (.I0(v_cnt[8]),
        .I1(v_cnt[6]),
        .I2(v_cnt[7]),
        .I3(\tile_row_write_counter[5]_i_6_n_0 ),
        .O(\tile_row_write_counter_reg[5] [4]));
  LUT6 #(
    .INIT(64'h0000000000020000)) 
    \tile_row_write_counter[5]_i_1 
       (.I0(v_cnt[8]),
        .I1(\tile_row_write_counter[5]_i_4_n_0 ),
        .I2(v_cnt[9]),
        .I3(v_cnt[5]),
        .I4(render_enable),
        .I5(\tile_row_write_counter[5]_i_5_n_0 ),
        .O(\tile_row_write_counter_reg[5]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT5 #(
    .INIT(32'h80000000)) 
    \tile_row_write_counter[5]_i_2 
       (.I0(render_enable),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(\tile_row_write_counter_reg[0] [1]),
        .I4(\tile_row_write_counter_reg[0] [0]),
        .O(SR));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT5 #(
    .INIT(32'h9AAAAAAA)) 
    \tile_row_write_counter[5]_i_3 
       (.I0(v_cnt[9]),
        .I1(\tile_row_write_counter[5]_i_6_n_0 ),
        .I2(v_cnt[7]),
        .I3(v_cnt[6]),
        .I4(v_cnt[8]),
        .O(\tile_row_write_counter_reg[5] [5]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \tile_row_write_counter[5]_i_4 
       (.I0(v_cnt[6]),
        .I1(v_cnt[7]),
        .O(\tile_row_write_counter[5]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT5 #(
    .INIT(32'h7FFFFFFF)) 
    \tile_row_write_counter[5]_i_5 
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(\tile_row_write_counter_reg[0] [1]),
        .I3(\tile_row_write_counter_reg[0] [0]),
        .I4(v_cnt[4]),
        .O(\tile_row_write_counter[5]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \tile_row_write_counter[5]_i_6 
       (.I0(v_cnt[4]),
        .I1(\tile_row_write_counter_reg[0] [0]),
        .I2(\tile_row_write_counter_reg[0] [1]),
        .I3(Q[1]),
        .I4(Q[0]),
        .I5(v_cnt[5]),
        .O(\tile_row_write_counter[5]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'h0D)) 
    tile_wrote_i_1
       (.I0(render_enable),
        .I1(tile_wrote_reg_0),
        .I2(\addr_Y_reg[0] ),
        .O(tile_wrote_reg));
  LUT2 #(
    .INIT(4'h1)) 
    tiles_reg_0_63_0_2_i_1
       (.I0(render_enable),
        .I1(tile_wrote_reg_0),
        .O(I7));
  (* ORIG_CELL_NAME = "v_cnt_reg[0]" *) 
  FDRE \v_cnt_reg[0] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(\y_reg_n_0_[0] ),
        .Q(ADDRC[4]),
        .R(1'b0));
  (* ORIG_CELL_NAME = "v_cnt_reg[0]" *) 
  FDRE \v_cnt_reg[0]_rep 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(\y_reg_n_0_[0] ),
        .Q(\pixel_bus_reg[14] [4]),
        .R(1'b0));
  (* ORIG_CELL_NAME = "v_cnt_reg[0]" *) 
  FDRE \v_cnt_reg[0]_rep__0 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(\y_reg_n_0_[0] ),
        .Q(ADDRA[0]),
        .R(1'b0));
  (* ORIG_CELL_NAME = "v_cnt_reg[0]" *) 
  FDRE \v_cnt_reg[0]_rep__1 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(\y_reg_n_0_[0] ),
        .Q(\tile_row_write_counter_reg[0] [0]),
        .R(1'b0));
  (* ORIG_CELL_NAME = "v_cnt_reg[0]" *) 
  FDRE \v_cnt_reg[0]_rep__2 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(\y_reg_n_0_[0] ),
        .Q(ADDRB[0]),
        .R(1'b0));
  (* ORIG_CELL_NAME = "v_cnt_reg[1]" *) 
  FDRE \v_cnt_reg[1] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(\y_reg_n_0_[1] ),
        .Q(ADDRC[5]),
        .R(1'b0));
  (* ORIG_CELL_NAME = "v_cnt_reg[1]" *) 
  FDRE \v_cnt_reg[1]_rep 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(\y_reg_n_0_[1] ),
        .Q(\pixel_bus_reg[14] [5]),
        .R(1'b0));
  (* ORIG_CELL_NAME = "v_cnt_reg[1]" *) 
  FDRE \v_cnt_reg[1]_rep__0 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(\y_reg_n_0_[1] ),
        .Q(ADDRA[1]),
        .R(1'b0));
  (* ORIG_CELL_NAME = "v_cnt_reg[1]" *) 
  FDRE \v_cnt_reg[1]_rep__1 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(\y_reg_n_0_[1] ),
        .Q(\tile_row_write_counter_reg[0] [1]),
        .R(1'b0));
  (* ORIG_CELL_NAME = "v_cnt_reg[1]" *) 
  FDRE \v_cnt_reg[1]_rep__2 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(\y_reg_n_0_[1] ),
        .Q(ADDRB[1]),
        .R(1'b0));
  FDRE \v_cnt_reg[2] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(\y_reg_n_0_[2] ),
        .Q(Q[0]),
        .R(1'b0));
  FDRE \v_cnt_reg[3] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(\y_reg_n_0_[3] ),
        .Q(Q[1]),
        .R(1'b0));
  FDRE \v_cnt_reg[4] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(\y_reg_n_0_[4] ),
        .Q(v_cnt[4]),
        .R(1'b0));
  FDRE \v_cnt_reg[5] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(\y_reg_n_0_[5] ),
        .Q(v_cnt[5]),
        .R(1'b0));
  FDRE \v_cnt_reg[6] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(\y_reg_n_0_[6] ),
        .Q(v_cnt[6]),
        .R(1'b0));
  FDRE \v_cnt_reg[7] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(\y_reg_n_0_[7] ),
        .Q(v_cnt[7]),
        .R(1'b0));
  FDRE \v_cnt_reg[8] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(\y_reg_n_0_[8] ),
        .Q(v_cnt[8]),
        .R(1'b0));
  FDRE \v_cnt_reg[9] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(\y_reg_n_0_[9] ),
        .Q(v_cnt[9]),
        .R(1'b0));
  FDRE \vga_b_reg[2] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(pixel_bus[3]),
        .Q(vga_b[0]),
        .R(\vga_r[4]_i_1_n_0 ));
  FDRE \vga_b_reg[3] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(pixel_bus[4]),
        .Q(vga_b[1]),
        .R(\vga_r[4]_i_1_n_0 ));
  FDRE \vga_b_reg[4] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(pixel_bus[5]),
        .Q(vga_b[2]),
        .R(\vga_r[4]_i_1_n_0 ));
  FDRE \vga_g_reg[2] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(pixel_bus[6]),
        .Q(vga_g[0]),
        .R(\vga_r[4]_i_1_n_0 ));
  FDRE \vga_g_reg[3] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(pixel_bus[7]),
        .Q(vga_g[1]),
        .R(\vga_r[4]_i_1_n_0 ));
  FDRE \vga_g_reg[4] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(pixel_bus[8]),
        .Q(vga_g[2]),
        .R(\vga_r[4]_i_1_n_0 ));
  FDRE \vga_g_reg[5] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(pixel_bus[9]),
        .Q(vga_g[3]),
        .R(\vga_r[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFE0FFFFFFFF)) 
    \vga_r[4]_i_1 
       (.I0(x_reg__0[8]),
        .I1(x_reg__0[7]),
        .I2(x_reg__0[9]),
        .I3(\vga_r[4]_i_2_n_0 ),
        .I4(\y_reg_n_0_[9] ),
        .I5(\vga_r[4]_i_3_n_0 ),
        .O(\vga_r[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair42" *) 
  LUT2 #(
    .INIT(4'h7)) 
    \vga_r[4]_i_2 
       (.I0(cnt[0]),
        .I1(cnt[1]),
        .O(\vga_r[4]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \vga_r[4]_i_3 
       (.I0(\y_reg_n_0_[7] ),
        .I1(\y_reg_n_0_[5] ),
        .I2(\y_reg_n_0_[6] ),
        .I3(\y_reg_n_0_[8] ),
        .O(\vga_r[4]_i_3_n_0 ));
  FDRE \vga_r_reg[2] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(pixel_bus[0]),
        .Q(vga_r[0]),
        .R(\vga_r[4]_i_1_n_0 ));
  FDRE \vga_r_reg[3] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(pixel_bus[1]),
        .Q(vga_r[1]),
        .R(\vga_r[4]_i_1_n_0 ));
  FDRE \vga_r_reg[4] 
       (.C(pixel_clk),
        .CE(1'b1),
        .D(pixel_bus[2]),
        .Q(vga_r[2]),
        .R(\vga_r[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \x[0]_i_1 
       (.I0(x_reg__0[0]),
        .O(p_0_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \x[1]_i_1 
       (.I0(x_reg__0[0]),
        .I1(x_reg__0[1]),
        .O(p_0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \x[2]_i_1 
       (.I0(x_reg__0[1]),
        .I1(x_reg__0[0]),
        .I2(x_reg__0[2]),
        .O(p_0_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \x[3]_i_1 
       (.I0(x_reg__0[2]),
        .I1(x_reg__0[0]),
        .I2(x_reg__0[1]),
        .I3(x_reg__0[3]),
        .O(p_0_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \x[4]_i_1 
       (.I0(x_reg__0[3]),
        .I1(x_reg__0[1]),
        .I2(x_reg__0[0]),
        .I3(x_reg__0[2]),
        .I4(x_reg__0[4]),
        .O(p_0_in[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \x[5]_i_1 
       (.I0(x_reg__0[4]),
        .I1(x_reg__0[2]),
        .I2(x_reg__0[0]),
        .I3(x_reg__0[1]),
        .I4(x_reg__0[3]),
        .I5(x_reg__0[5]),
        .O(p_0_in[5]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT3 #(
    .INIT(8'hD2)) 
    \x[6]_i_1 
       (.I0(x_reg__0[5]),
        .I1(\x[9]_i_3_n_0 ),
        .I2(x_reg__0[6]),
        .O(p_0_in[6]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT4 #(
    .INIT(16'hBF40)) 
    \x[7]_i_1 
       (.I0(\x[9]_i_3_n_0 ),
        .I1(x_reg__0[5]),
        .I2(x_reg__0[6]),
        .I3(x_reg__0[7]),
        .O(p_0_in[7]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT5 #(
    .INIT(32'hBFFF4000)) 
    \x[8]_i_1 
       (.I0(\x[9]_i_3_n_0 ),
        .I1(x_reg__0[6]),
        .I2(x_reg__0[5]),
        .I3(x_reg__0[7]),
        .I4(x_reg__0[8]),
        .O(p_0_in[8]));
  LUT3 #(
    .INIT(8'h08)) 
    \x[9]_i_1 
       (.I0(cnt[1]),
        .I1(cnt[0]),
        .I2(\y[4]_i_2_n_0 ),
        .O(x));
  LUT6 #(
    .INIT(64'hBFFFFFFF40000000)) 
    \x[9]_i_2 
       (.I0(\x[9]_i_3_n_0 ),
        .I1(x_reg__0[8]),
        .I2(x_reg__0[6]),
        .I3(x_reg__0[5]),
        .I4(x_reg__0[7]),
        .I5(x_reg__0[9]),
        .O(p_0_in[9]));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT5 #(
    .INIT(32'h7FFFFFFF)) 
    \x[9]_i_3 
       (.I0(x_reg__0[3]),
        .I1(x_reg__0[1]),
        .I2(x_reg__0[0]),
        .I3(x_reg__0[2]),
        .I4(x_reg__0[4]),
        .O(\x[9]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \x_reg[0] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(p_0_in[0]),
        .Q(x_reg__0[0]),
        .R(x));
  FDRE #(
    .INIT(1'b0)) 
    \x_reg[1] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(p_0_in[1]),
        .Q(x_reg__0[1]),
        .R(x));
  FDRE #(
    .INIT(1'b0)) 
    \x_reg[2] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(p_0_in[2]),
        .Q(x_reg__0[2]),
        .R(x));
  FDRE #(
    .INIT(1'b0)) 
    \x_reg[3] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(p_0_in[3]),
        .Q(x_reg__0[3]),
        .R(x));
  FDRE #(
    .INIT(1'b0)) 
    \x_reg[4] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(p_0_in[4]),
        .Q(x_reg__0[4]),
        .R(x));
  FDRE #(
    .INIT(1'b0)) 
    \x_reg[5] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(p_0_in[5]),
        .Q(x_reg__0[5]),
        .R(x));
  FDRE #(
    .INIT(1'b0)) 
    \x_reg[6] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(p_0_in[6]),
        .Q(x_reg__0[6]),
        .R(x));
  FDRE #(
    .INIT(1'b0)) 
    \x_reg[7] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(p_0_in[7]),
        .Q(x_reg__0[7]),
        .R(x));
  FDRE #(
    .INIT(1'b0)) 
    \x_reg[8] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(p_0_in[8]),
        .Q(x_reg__0[8]),
        .R(x));
  FDRE #(
    .INIT(1'b0)) 
    \x_reg[9] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(p_0_in[9]),
        .Q(x_reg__0[9]),
        .R(x));
  LUT6 #(
    .INIT(64'hFF00FF00F700FF00)) 
    \y[0]_i_1 
       (.I0(\y_reg_n_0_[3] ),
        .I1(\y_reg_n_0_[2] ),
        .I2(\y_reg_n_0_[1] ),
        .I3(\y[0]_i_2_n_0 ),
        .I4(\y_reg_n_0_[9] ),
        .I5(\y[0]_i_3_n_0 ),
        .O(y[0]));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT5 #(
    .INIT(32'hBC8C8C8C)) 
    \y[0]_i_2 
       (.I0(\y[0]_i_4_n_0 ),
        .I1(\y_reg_n_0_[0] ),
        .I2(x_reg__0[9]),
        .I3(\y[0]_i_5_n_0 ),
        .I4(x_reg__0[0]),
        .O(\y[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \y[0]_i_3 
       (.I0(\y_reg_n_0_[4] ),
        .I1(\y_reg_n_0_[7] ),
        .I2(\y_reg_n_0_[8] ),
        .I3(\y_reg_n_0_[6] ),
        .I4(\y_reg_n_0_[5] ),
        .O(\y[0]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT5 #(
    .INIT(32'hFFFFFEFF)) 
    \y[0]_i_4 
       (.I0(x_reg__0[5]),
        .I1(x_reg__0[6]),
        .I2(x_reg__0[7]),
        .I3(x_reg__0[8]),
        .I4(\x[9]_i_3_n_0 ),
        .O(\y[0]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'h00100000)) 
    \y[0]_i_5 
       (.I0(x_reg__0[5]),
        .I1(x_reg__0[6]),
        .I2(x_reg__0[8]),
        .I3(x_reg__0[7]),
        .I4(\y[0]_i_6_n_0 ),
        .O(\y[0]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT4 #(
    .INIT(16'h8000)) 
    \y[0]_i_6 
       (.I0(x_reg__0[4]),
        .I1(x_reg__0[3]),
        .I2(x_reg__0[2]),
        .I3(x_reg__0[1]),
        .O(\y[0]_i_6_n_0 ));
  LUT3 #(
    .INIT(8'hD2)) 
    \y[1]_i_1 
       (.I0(\y_reg_n_0_[0] ),
        .I1(\y[4]_i_2_n_0 ),
        .I2(\y_reg_n_0_[1] ),
        .O(y[1]));
  LUT6 #(
    .INIT(64'hFFFF202000552020)) 
    \y[2]_i_1 
       (.I0(\y_reg_n_0_[1] ),
        .I1(\y[4]_i_2_n_0 ),
        .I2(\y_reg_n_0_[0] ),
        .I3(\y_reg_n_0_[3] ),
        .I4(\y_reg_n_0_[2] ),
        .I5(\y[3]_i_2_n_0 ),
        .O(y[2]));
  LUT6 #(
    .INIT(64'hFFFF080055550800)) 
    \y[3]_i_1 
       (.I0(\y_reg_n_0_[2] ),
        .I1(\y_reg_n_0_[1] ),
        .I2(\y[4]_i_2_n_0 ),
        .I3(\y_reg_n_0_[0] ),
        .I4(\y_reg_n_0_[3] ),
        .I5(\y[3]_i_2_n_0 ),
        .O(y[3]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT5 #(
    .INIT(32'hF3FFF37D)) 
    \y[3]_i_2 
       (.I0(\y_reg_n_0_[9] ),
        .I1(\y_reg_n_0_[0] ),
        .I2(\y[4]_i_2_n_0 ),
        .I3(\y_reg_n_0_[1] ),
        .I4(\y[0]_i_3_n_0 ),
        .O(\y[3]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hFF7FFFFF00800000)) 
    \y[4]_i_1 
       (.I0(\y_reg_n_0_[2] ),
        .I1(\y_reg_n_0_[3] ),
        .I2(\y_reg_n_0_[0] ),
        .I3(\y[4]_i_2_n_0 ),
        .I4(\y_reg_n_0_[1] ),
        .I5(\y_reg_n_0_[4] ),
        .O(y[4]));
  LUT6 #(
    .INIT(64'hFFFFFFFBFFFFFFFF)) 
    \y[4]_i_2 
       (.I0(\x[9]_i_3_n_0 ),
        .I1(x_reg__0[8]),
        .I2(x_reg__0[7]),
        .I3(x_reg__0[6]),
        .I4(x_reg__0[5]),
        .I5(x_reg__0[9]),
        .O(\y[4]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \y[5]_i_1 
       (.I0(\y[8]_i_2_n_0 ),
        .I1(\y_reg_n_0_[5] ),
        .O(y[5]));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT3 #(
    .INIT(8'hD2)) 
    \y[6]_i_1 
       (.I0(\y_reg_n_0_[5] ),
        .I1(\y[8]_i_2_n_0 ),
        .I2(\y_reg_n_0_[6] ),
        .O(y[6]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT4 #(
    .INIT(16'hF708)) 
    \y[7]_i_1 
       (.I0(\y_reg_n_0_[5] ),
        .I1(\y_reg_n_0_[6] ),
        .I2(\y[8]_i_2_n_0 ),
        .I3(\y_reg_n_0_[7] ),
        .O(y[7]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT5 #(
    .INIT(32'hFF7F0080)) 
    \y[8]_i_1 
       (.I0(\y_reg_n_0_[6] ),
        .I1(\y_reg_n_0_[5] ),
        .I2(\y_reg_n_0_[7] ),
        .I3(\y[8]_i_2_n_0 ),
        .I4(\y_reg_n_0_[8] ),
        .O(y[8]));
  LUT6 #(
    .INIT(64'hFF7FFFFFFFFFFFFF)) 
    \y[8]_i_2 
       (.I0(\y_reg_n_0_[2] ),
        .I1(\y_reg_n_0_[3] ),
        .I2(\y_reg_n_0_[0] ),
        .I3(\y[4]_i_2_n_0 ),
        .I4(\y_reg_n_0_[1] ),
        .I5(\y_reg_n_0_[4] ),
        .O(\y[8]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \y[9]_i_1 
       (.I0(cnt[1]),
        .I1(cnt[0]),
        .O(render_enable1));
  LUT5 #(
    .INIT(32'hFFFFBF00)) 
    \y[9]_i_2 
       (.I0(\y[9]_i_3_n_0 ),
        .I1(\y_reg_n_0_[3] ),
        .I2(\y_reg_n_0_[2] ),
        .I3(\y_reg_n_0_[9] ),
        .I4(\y[9]_i_4_n_0 ),
        .O(y[9]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT4 #(
    .INIT(16'hE3FE)) 
    \y[9]_i_3 
       (.I0(\y[0]_i_3_n_0 ),
        .I1(\y_reg_n_0_[1] ),
        .I2(\y[4]_i_2_n_0 ),
        .I3(\y_reg_n_0_[0] ),
        .O(\y[9]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hCC44000F)) 
    \y[9]_i_4 
       (.I0(\y_reg_n_0_[4] ),
        .I1(\y_reg_n_0_[1] ),
        .I2(\y[8]_i_2_n_0 ),
        .I3(\vga_r[4]_i_3_n_0 ),
        .I4(\y_reg_n_0_[9] ),
        .O(\y[9]_i_4_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \y_reg[0] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(y[0]),
        .Q(\y_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \y_reg[1] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(y[1]),
        .Q(\y_reg_n_0_[1] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \y_reg[2] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(y[2]),
        .Q(\y_reg_n_0_[2] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \y_reg[3] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(y[3]),
        .Q(\y_reg_n_0_[3] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \y_reg[4] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(y[4]),
        .Q(\y_reg_n_0_[4] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \y_reg[5] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(y[5]),
        .Q(\y_reg_n_0_[5] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \y_reg[6] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(y[6]),
        .Q(\y_reg_n_0_[6] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \y_reg[7] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(y[7]),
        .Q(\y_reg_n_0_[7] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \y_reg[8] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(y[8]),
        .Q(\y_reg_n_0_[8] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \y_reg[9] 
       (.C(pixel_clk),
        .CE(render_enable1),
        .D(y[9]),
        .Q(\y_reg_n_0_[9] ),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
