// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.1 (lin64) Build 1846317 Fri Apr 14 18:54:47 MDT 2017
// Date        : Sat Jun 10 17:27:17 2017
// Host        : surprise running 64-bit Linux Mint 18.1 Serena
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_rendVgaTmBoot_0_2_stub.v
// Design      : design_1_rendVgaTmBoot_0_2
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z010clg400-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "top,Vivado 2017.1" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(clk, pixel_clk, sw, vga_r, vga_g, vga_b, vga_hs, 
  vga_vs, fetch, data_type, map_id, packet_in, fetching)
/* synthesis syn_black_box black_box_pad_pin="clk,pixel_clk,sw[3:0],vga_r[4:0],vga_g[5:0],vga_b[4:0],vga_hs,vga_vs,fetch,data_type,map_id[7:0],packet_in[31:0],fetching" */;
  input clk;
  input pixel_clk;
  input [3:0]sw;
  output [4:0]vga_r;
  output [5:0]vga_g;
  output [4:0]vga_b;
  output vga_hs;
  output vga_vs;
  output fetch;
  output data_type;
  output [7:0]map_id;
  input [31:0]packet_in;
  input fetching;
endmodule
