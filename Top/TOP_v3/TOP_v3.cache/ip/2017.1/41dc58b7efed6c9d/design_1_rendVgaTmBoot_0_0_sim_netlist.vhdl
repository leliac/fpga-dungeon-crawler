-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.1 (lin64) Build 1846317 Fri Apr 14 18:54:47 MDT 2017
-- Date        : Sat Jun 17 04:05:00 2017
-- Host        : surprise running 64-bit Linux Mint 18.1 Serena
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_rendVgaTmBoot_0_0_sim_netlist.vhdl
-- Design      : design_1_rendVgaTmBoot_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z010clg400-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_booting is
  port (
    fetching_sprites : out STD_LOGIC;
    fetch_complete : out STD_LOGIC;
    fetch : out STD_LOGIC;
    data_type : out STD_LOGIC;
    led0 : out STD_LOGIC;
    led1 : out STD_LOGIC;
    led2 : out STD_LOGIC;
    led3 : out STD_LOGIC;
    addra0 : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 6 downto 0 );
    dina : out STD_LOGIC_VECTOR ( 5 downto 0 );
    map_id : out STD_LOGIC_VECTOR ( 6 downto 0 );
    clk : in STD_LOGIC;
    ind_reg : in STD_LOGIC_VECTOR ( 0 to 0 );
    Q : in STD_LOGIC_VECTOR ( 6 downto 0 );
    fetching : in STD_LOGIC;
    sw : in STD_LOGIC_VECTOR ( 2 downto 0 );
    packet_in : in STD_LOGIC_VECTOR ( 5 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_booting;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_booting is
  signal \FSM_sequential_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_state[1]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_state[2]_i_10_n_0\ : STD_LOGIC;
  signal \FSM_sequential_state[2]_i_11_n_0\ : STD_LOGIC;
  signal \FSM_sequential_state[2]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_state[2]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_state[2]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_sequential_state[2]_i_5_n_0\ : STD_LOGIC;
  signal \FSM_sequential_state[2]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_sequential_state[2]_i_7_n_0\ : STD_LOGIC;
  signal \FSM_sequential_state[2]_i_8_n_0\ : STD_LOGIC;
  signal \FSM_sequential_state[2]_i_9_n_0\ : STD_LOGIC;
  signal \FSM_sequential_state_reg[2]_i_2_n_0\ : STD_LOGIC;
  signal cnt : STD_LOGIC_VECTOR ( 30 downto 0 );
  signal \cnt0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \cnt0_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \cnt0_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \cnt0_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \cnt0_carry__0_n_0\ : STD_LOGIC;
  signal \cnt0_carry__0_n_1\ : STD_LOGIC;
  signal \cnt0_carry__0_n_2\ : STD_LOGIC;
  signal \cnt0_carry__0_n_3\ : STD_LOGIC;
  signal \cnt0_carry__0_n_4\ : STD_LOGIC;
  signal \cnt0_carry__0_n_5\ : STD_LOGIC;
  signal \cnt0_carry__0_n_6\ : STD_LOGIC;
  signal \cnt0_carry__0_n_7\ : STD_LOGIC;
  signal \cnt0_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \cnt0_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \cnt0_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \cnt0_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \cnt0_carry__1_n_0\ : STD_LOGIC;
  signal \cnt0_carry__1_n_1\ : STD_LOGIC;
  signal \cnt0_carry__1_n_2\ : STD_LOGIC;
  signal \cnt0_carry__1_n_3\ : STD_LOGIC;
  signal \cnt0_carry__1_n_4\ : STD_LOGIC;
  signal \cnt0_carry__1_n_5\ : STD_LOGIC;
  signal \cnt0_carry__1_n_6\ : STD_LOGIC;
  signal \cnt0_carry__1_n_7\ : STD_LOGIC;
  signal \cnt0_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \cnt0_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \cnt0_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \cnt0_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \cnt0_carry__2_n_0\ : STD_LOGIC;
  signal \cnt0_carry__2_n_1\ : STD_LOGIC;
  signal \cnt0_carry__2_n_2\ : STD_LOGIC;
  signal \cnt0_carry__2_n_3\ : STD_LOGIC;
  signal \cnt0_carry__2_n_4\ : STD_LOGIC;
  signal \cnt0_carry__2_n_5\ : STD_LOGIC;
  signal \cnt0_carry__2_n_6\ : STD_LOGIC;
  signal \cnt0_carry__2_n_7\ : STD_LOGIC;
  signal \cnt0_carry__3_i_1_n_0\ : STD_LOGIC;
  signal \cnt0_carry__3_i_2_n_0\ : STD_LOGIC;
  signal \cnt0_carry__3_i_3_n_0\ : STD_LOGIC;
  signal \cnt0_carry__3_i_4_n_0\ : STD_LOGIC;
  signal \cnt0_carry__3_n_0\ : STD_LOGIC;
  signal \cnt0_carry__3_n_1\ : STD_LOGIC;
  signal \cnt0_carry__3_n_2\ : STD_LOGIC;
  signal \cnt0_carry__3_n_3\ : STD_LOGIC;
  signal \cnt0_carry__3_n_4\ : STD_LOGIC;
  signal \cnt0_carry__3_n_5\ : STD_LOGIC;
  signal \cnt0_carry__3_n_6\ : STD_LOGIC;
  signal \cnt0_carry__3_n_7\ : STD_LOGIC;
  signal \cnt0_carry__4_i_1_n_0\ : STD_LOGIC;
  signal \cnt0_carry__4_i_2_n_0\ : STD_LOGIC;
  signal \cnt0_carry__4_i_3_n_0\ : STD_LOGIC;
  signal \cnt0_carry__4_i_4_n_0\ : STD_LOGIC;
  signal \cnt0_carry__4_n_0\ : STD_LOGIC;
  signal \cnt0_carry__4_n_1\ : STD_LOGIC;
  signal \cnt0_carry__4_n_2\ : STD_LOGIC;
  signal \cnt0_carry__4_n_3\ : STD_LOGIC;
  signal \cnt0_carry__4_n_4\ : STD_LOGIC;
  signal \cnt0_carry__4_n_5\ : STD_LOGIC;
  signal \cnt0_carry__4_n_6\ : STD_LOGIC;
  signal \cnt0_carry__4_n_7\ : STD_LOGIC;
  signal \cnt0_carry__5_i_1_n_0\ : STD_LOGIC;
  signal \cnt0_carry__5_i_2_n_0\ : STD_LOGIC;
  signal \cnt0_carry__5_i_3_n_0\ : STD_LOGIC;
  signal \cnt0_carry__5_i_4_n_0\ : STD_LOGIC;
  signal \cnt0_carry__5_n_0\ : STD_LOGIC;
  signal \cnt0_carry__5_n_1\ : STD_LOGIC;
  signal \cnt0_carry__5_n_2\ : STD_LOGIC;
  signal \cnt0_carry__5_n_3\ : STD_LOGIC;
  signal \cnt0_carry__5_n_4\ : STD_LOGIC;
  signal \cnt0_carry__5_n_5\ : STD_LOGIC;
  signal \cnt0_carry__5_n_6\ : STD_LOGIC;
  signal \cnt0_carry__5_n_7\ : STD_LOGIC;
  signal \cnt0_carry__6_i_1_n_0\ : STD_LOGIC;
  signal \cnt0_carry__6_i_2_n_0\ : STD_LOGIC;
  signal \cnt0_carry__6_n_3\ : STD_LOGIC;
  signal \cnt0_carry__6_n_6\ : STD_LOGIC;
  signal \cnt0_carry__6_n_7\ : STD_LOGIC;
  signal cnt0_carry_i_1_n_0 : STD_LOGIC;
  signal cnt0_carry_i_2_n_0 : STD_LOGIC;
  signal cnt0_carry_i_3_n_0 : STD_LOGIC;
  signal cnt0_carry_i_4_n_0 : STD_LOGIC;
  signal cnt0_carry_n_0 : STD_LOGIC;
  signal cnt0_carry_n_1 : STD_LOGIC;
  signal cnt0_carry_n_2 : STD_LOGIC;
  signal cnt0_carry_n_3 : STD_LOGIC;
  signal cnt0_carry_n_4 : STD_LOGIC;
  signal cnt0_carry_n_5 : STD_LOGIC;
  signal cnt0_carry_n_6 : STD_LOGIC;
  signal cnt0_carry_n_7 : STD_LOGIC;
  signal \cnt[30]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_reg_n_0_[0]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[10]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[11]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[12]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[13]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[14]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[15]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[16]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[17]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[18]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[19]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[1]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[20]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[21]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[22]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[23]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[24]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[25]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[26]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[27]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[28]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[29]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[2]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[30]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[3]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[4]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[5]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[6]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[7]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[8]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[9]\ : STD_LOGIC;
  signal \^data_type\ : STD_LOGIC;
  signal data_type_i_1_n_0 : STD_LOGIC;
  signal \^fetch\ : STD_LOGIC;
  signal \^fetch_complete\ : STD_LOGIC;
  signal fetch_complete_i_1_n_0 : STD_LOGIC;
  signal fetch_complete_i_2_n_0 : STD_LOGIC;
  signal fetch_complete_i_3_n_0 : STD_LOGIC;
  signal fetch_complete_i_4_n_0 : STD_LOGIC;
  signal fetch_complete_i_5_n_0 : STD_LOGIC;
  signal fetch_complete_i_6_n_0 : STD_LOGIC;
  signal fetch_complete_i_7_n_0 : STD_LOGIC;
  signal fetch_complete_i_8_n_0 : STD_LOGIC;
  signal fetch_i_1_n_0 : STD_LOGIC;
  signal fetch_i_3_n_0 : STD_LOGIC;
  signal fetch_i_4_n_0 : STD_LOGIC;
  signal \^fetching_sprites\ : STD_LOGIC;
  signal fetching_sprites_i_2_n_0 : STD_LOGIC;
  signal fetching_sprites_i_3_n_0 : STD_LOGIC;
  signal fetching_sprites_i_4_n_0 : STD_LOGIC;
  signal fetching_sprites_i_5_n_0 : STD_LOGIC;
  signal \^led0\ : STD_LOGIC;
  signal led0_i_1_n_0 : STD_LOGIC;
  signal \^led1\ : STD_LOGIC;
  signal led1_i_1_n_0 : STD_LOGIC;
  signal led1_i_2_n_0 : STD_LOGIC;
  signal \^led2\ : STD_LOGIC;
  signal led2_i_1_n_0 : STD_LOGIC;
  signal \^led3\ : STD_LOGIC;
  signal led3_i_1_n_0 : STD_LOGIC;
  signal led3_i_2_n_0 : STD_LOGIC;
  signal \map_id[6]_i_1_n_0\ : STD_LOGIC;
  signal \map_id[6]_i_2_n_0\ : STD_LOGIC;
  signal \pixel_out[0]_i_1_n_0\ : STD_LOGIC;
  signal \pixel_out[1]_i_1_n_0\ : STD_LOGIC;
  signal \pixel_out[2]_i_1_n_0\ : STD_LOGIC;
  signal \pixel_out[3]_i_1_n_0\ : STD_LOGIC;
  signal \pixel_out[4]_i_1_n_0\ : STD_LOGIC;
  signal \pixel_out[5]_i_1_n_0\ : STD_LOGIC;
  signal \pixel_out[5]_i_2_n_0\ : STD_LOGIC;
  signal \pixel_out[5]_i_3_n_0\ : STD_LOGIC;
  signal rand : STD_LOGIC;
  signal rand0 : STD_LOGIC_VECTOR ( 6 downto 1 );
  signal \rand[6]_i_3_n_0\ : STD_LOGIC;
  signal \rand_reg_n_0_[0]\ : STD_LOGIC;
  signal \rand_reg_n_0_[1]\ : STD_LOGIC;
  signal \rand_reg_n_0_[2]\ : STD_LOGIC;
  signal \rand_reg_n_0_[3]\ : STD_LOGIC;
  signal \rand_reg_n_0_[4]\ : STD_LOGIC;
  signal \rand_reg_n_0_[5]\ : STD_LOGIC;
  signal \rand_reg_n_0_[6]\ : STD_LOGIC;
  signal state : STD_LOGIC_VECTOR ( 2 downto 0 );
  attribute RTL_KEEP : string;
  attribute RTL_KEEP of state : signal is "yes";
  signal state16_out : STD_LOGIC;
  signal state18_out : STD_LOGIC;
  signal state2 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \tmp_rand[0]_i_1_n_0\ : STD_LOGIC;
  signal \tmp_rand[1]_i_1_n_0\ : STD_LOGIC;
  signal \tmp_rand[2]_i_1_n_0\ : STD_LOGIC;
  signal \tmp_rand[3]_i_1_n_0\ : STD_LOGIC;
  signal \tmp_rand[4]_i_1_n_0\ : STD_LOGIC;
  signal \tmp_rand[5]_i_1_n_0\ : STD_LOGIC;
  signal \tmp_rand[6]_i_1_n_0\ : STD_LOGIC;
  signal \tmp_rand[6]_i_2_n_0\ : STD_LOGIC;
  signal \tmp_rand[6]_i_3_n_0\ : STD_LOGIC;
  signal \tmp_rand[6]_i_4_n_0\ : STD_LOGIC;
  signal \NLW_cnt0_carry__6_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_cnt0_carry__6_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  attribute KEEP : string;
  attribute KEEP of \FSM_sequential_state_reg[0]\ : label is "yes";
  attribute KEEP of \FSM_sequential_state_reg[1]\ : label is "yes";
  attribute KEEP of \FSM_sequential_state_reg[2]\ : label is "yes";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of fetch_complete_i_5 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of fetch_complete_i_7 : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of fetch_complete_i_8 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of fetch_i_2 : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of led3_i_2 : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \pixel_out[0]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \pixel_out[1]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \pixel_out[2]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \pixel_out[3]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \pixel_out[4]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \pixel_out[5]_i_3\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \rand[1]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \rand[2]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \rand[3]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \rand[4]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \rand[6]_i_2\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \tmp_rand[0]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \tmp_rand[1]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \tmp_rand[2]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \tmp_rand[3]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \tmp_rand[4]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \tmp_rand[6]_i_4\ : label is "soft_lutpair0";
begin
  data_type <= \^data_type\;
  fetch <= \^fetch\;
  fetch_complete <= \^fetch_complete\;
  fetching_sprites <= \^fetching_sprites\;
  led0 <= \^led0\;
  led1 <= \^led1\;
  led2 <= \^led2\;
  led3 <= \^led3\;
\FSM_sequential_state[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFAA0F0FCFAAAAAA"
    )
        port map (
      I0 => state(0),
      I1 => state(1),
      I2 => state(0),
      I3 => \FSM_sequential_state_reg[2]_i_2_n_0\,
      I4 => state(2),
      I5 => \FSM_sequential_state[2]_i_3_n_0\,
      O => \FSM_sequential_state[0]_i_1_n_0\
    );
\FSM_sequential_state[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6F666FFF60666000"
    )
        port map (
      I0 => state(0),
      I1 => state(1),
      I2 => \FSM_sequential_state_reg[2]_i_2_n_0\,
      I3 => state(2),
      I4 => \FSM_sequential_state[2]_i_3_n_0\,
      I5 => state(1),
      O => \FSM_sequential_state[1]_i_1_n_0\
    );
\FSM_sequential_state[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF88FFFFF088F000"
    )
        port map (
      I0 => state(1),
      I1 => state(0),
      I2 => \FSM_sequential_state_reg[2]_i_2_n_0\,
      I3 => state(2),
      I4 => \FSM_sequential_state[2]_i_3_n_0\,
      I5 => state(2),
      O => \FSM_sequential_state[2]_i_1_n_0\
    );
\FSM_sequential_state[2]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => \rand_reg_n_0_[3]\,
      I1 => \rand_reg_n_0_[6]\,
      I2 => \rand_reg_n_0_[4]\,
      I3 => fetch_complete_i_3_n_0,
      O => \FSM_sequential_state[2]_i_10_n_0\
    );
\FSM_sequential_state[2]_i_11\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \cnt_reg_n_0_[14]\,
      I1 => \cnt_reg_n_0_[13]\,
      I2 => fetching_sprites_i_2_n_0,
      I3 => fetching_sprites_i_3_n_0,
      I4 => fetching_sprites_i_4_n_0,
      I5 => fetching_sprites_i_5_n_0,
      O => \FSM_sequential_state[2]_i_11_n_0\
    );
\FSM_sequential_state[2]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BFB0"
    )
        port map (
      I0 => led1_i_2_n_0,
      I1 => state(0),
      I2 => state(1),
      I3 => \FSM_sequential_state[2]_i_6_n_0\,
      O => \FSM_sequential_state[2]_i_3_n_0\
    );
\FSM_sequential_state[2]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => state(0),
      I1 => \FSM_sequential_state[2]_i_7_n_0\,
      I2 => \FSM_sequential_state[2]_i_8_n_0\,
      I3 => led1_i_2_n_0,
      I4 => \cnt_reg_n_0_[12]\,
      I5 => \FSM_sequential_state[2]_i_9_n_0\,
      O => \FSM_sequential_state[2]_i_4_n_0\
    );
\FSM_sequential_state[2]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0BFF0B000BFF0BFF"
    )
        port map (
      I0 => fetch_complete_i_5_n_0,
      I1 => fetch_complete_i_4_n_0,
      I2 => \FSM_sequential_state[2]_i_10_n_0\,
      I3 => state(0),
      I4 => \FSM_sequential_state[2]_i_11_n_0\,
      I5 => fetch_i_3_n_0,
      O => \FSM_sequential_state[2]_i_5_n_0\
    );
\FSM_sequential_state[2]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFB0BFBFBFB0B0B0"
    )
        port map (
      I0 => Q(6),
      I1 => \rand[6]_i_3_n_0\,
      I2 => state(0),
      I3 => sw(1),
      I4 => sw(2),
      I5 => sw(0),
      O => \FSM_sequential_state[2]_i_6_n_0\
    );
\FSM_sequential_state[2]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFE000000"
    )
        port map (
      I0 => \cnt_reg_n_0_[4]\,
      I1 => \cnt_reg_n_0_[3]\,
      I2 => \cnt_reg_n_0_[2]\,
      I3 => \cnt_reg_n_0_[5]\,
      I4 => \cnt_reg_n_0_[6]\,
      I5 => fetch_i_4_n_0,
      O => \FSM_sequential_state[2]_i_7_n_0\
    );
\FSM_sequential_state[2]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \cnt_reg_n_0_[13]\,
      I1 => \cnt_reg_n_0_[14]\,
      O => \FSM_sequential_state[2]_i_8_n_0\
    );
\FSM_sequential_state[2]_i_9\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \cnt_reg_n_0_[9]\,
      I1 => \cnt_reg_n_0_[8]\,
      I2 => \cnt_reg_n_0_[7]\,
      O => \FSM_sequential_state[2]_i_9_n_0\
    );
\FSM_sequential_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_sequential_state[0]_i_1_n_0\,
      Q => state(0),
      R => '0'
    );
\FSM_sequential_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_sequential_state[1]_i_1_n_0\,
      Q => state(1),
      R => '0'
    );
\FSM_sequential_state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_sequential_state[2]_i_1_n_0\,
      Q => state(2),
      R => '0'
    );
\FSM_sequential_state_reg[2]_i_2\: unisim.vcomponents.MUXF7
     port map (
      I0 => \FSM_sequential_state[2]_i_4_n_0\,
      I1 => \FSM_sequential_state[2]_i_5_n_0\,
      O => \FSM_sequential_state_reg[2]_i_2_n_0\,
      S => state(1)
    );
cnt0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => cnt0_carry_n_0,
      CO(2) => cnt0_carry_n_1,
      CO(1) => cnt0_carry_n_2,
      CO(0) => cnt0_carry_n_3,
      CYINIT => \cnt_reg_n_0_[0]\,
      DI(3 downto 0) => B"0000",
      O(3) => cnt0_carry_n_4,
      O(2) => cnt0_carry_n_5,
      O(1) => cnt0_carry_n_6,
      O(0) => cnt0_carry_n_7,
      S(3) => cnt0_carry_i_1_n_0,
      S(2) => cnt0_carry_i_2_n_0,
      S(1) => cnt0_carry_i_3_n_0,
      S(0) => cnt0_carry_i_4_n_0
    );
\cnt0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => cnt0_carry_n_0,
      CO(3) => \cnt0_carry__0_n_0\,
      CO(2) => \cnt0_carry__0_n_1\,
      CO(1) => \cnt0_carry__0_n_2\,
      CO(0) => \cnt0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt0_carry__0_n_4\,
      O(2) => \cnt0_carry__0_n_5\,
      O(1) => \cnt0_carry__0_n_6\,
      O(0) => \cnt0_carry__0_n_7\,
      S(3) => \cnt0_carry__0_i_1_n_0\,
      S(2) => \cnt0_carry__0_i_2_n_0\,
      S(1) => \cnt0_carry__0_i_3_n_0\,
      S(0) => \cnt0_carry__0_i_4_n_0\
    );
\cnt0_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cnt_reg_n_0_[8]\,
      O => \cnt0_carry__0_i_1_n_0\
    );
\cnt0_carry__0_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cnt_reg_n_0_[7]\,
      O => \cnt0_carry__0_i_2_n_0\
    );
\cnt0_carry__0_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cnt_reg_n_0_[6]\,
      O => \cnt0_carry__0_i_3_n_0\
    );
\cnt0_carry__0_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cnt_reg_n_0_[5]\,
      O => \cnt0_carry__0_i_4_n_0\
    );
\cnt0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt0_carry__0_n_0\,
      CO(3) => \cnt0_carry__1_n_0\,
      CO(2) => \cnt0_carry__1_n_1\,
      CO(1) => \cnt0_carry__1_n_2\,
      CO(0) => \cnt0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt0_carry__1_n_4\,
      O(2) => \cnt0_carry__1_n_5\,
      O(1) => \cnt0_carry__1_n_6\,
      O(0) => \cnt0_carry__1_n_7\,
      S(3) => \cnt0_carry__1_i_1_n_0\,
      S(2) => \cnt0_carry__1_i_2_n_0\,
      S(1) => \cnt0_carry__1_i_3_n_0\,
      S(0) => \cnt0_carry__1_i_4_n_0\
    );
\cnt0_carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cnt_reg_n_0_[12]\,
      O => \cnt0_carry__1_i_1_n_0\
    );
\cnt0_carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cnt_reg_n_0_[11]\,
      O => \cnt0_carry__1_i_2_n_0\
    );
\cnt0_carry__1_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cnt_reg_n_0_[10]\,
      O => \cnt0_carry__1_i_3_n_0\
    );
\cnt0_carry__1_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cnt_reg_n_0_[9]\,
      O => \cnt0_carry__1_i_4_n_0\
    );
\cnt0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt0_carry__1_n_0\,
      CO(3) => \cnt0_carry__2_n_0\,
      CO(2) => \cnt0_carry__2_n_1\,
      CO(1) => \cnt0_carry__2_n_2\,
      CO(0) => \cnt0_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt0_carry__2_n_4\,
      O(2) => \cnt0_carry__2_n_5\,
      O(1) => \cnt0_carry__2_n_6\,
      O(0) => \cnt0_carry__2_n_7\,
      S(3) => \cnt0_carry__2_i_1_n_0\,
      S(2) => \cnt0_carry__2_i_2_n_0\,
      S(1) => \cnt0_carry__2_i_3_n_0\,
      S(0) => \cnt0_carry__2_i_4_n_0\
    );
\cnt0_carry__2_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cnt_reg_n_0_[16]\,
      O => \cnt0_carry__2_i_1_n_0\
    );
\cnt0_carry__2_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cnt_reg_n_0_[15]\,
      O => \cnt0_carry__2_i_2_n_0\
    );
\cnt0_carry__2_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cnt_reg_n_0_[14]\,
      O => \cnt0_carry__2_i_3_n_0\
    );
\cnt0_carry__2_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cnt_reg_n_0_[13]\,
      O => \cnt0_carry__2_i_4_n_0\
    );
\cnt0_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt0_carry__2_n_0\,
      CO(3) => \cnt0_carry__3_n_0\,
      CO(2) => \cnt0_carry__3_n_1\,
      CO(1) => \cnt0_carry__3_n_2\,
      CO(0) => \cnt0_carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt0_carry__3_n_4\,
      O(2) => \cnt0_carry__3_n_5\,
      O(1) => \cnt0_carry__3_n_6\,
      O(0) => \cnt0_carry__3_n_7\,
      S(3) => \cnt0_carry__3_i_1_n_0\,
      S(2) => \cnt0_carry__3_i_2_n_0\,
      S(1) => \cnt0_carry__3_i_3_n_0\,
      S(0) => \cnt0_carry__3_i_4_n_0\
    );
\cnt0_carry__3_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cnt_reg_n_0_[20]\,
      O => \cnt0_carry__3_i_1_n_0\
    );
\cnt0_carry__3_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cnt_reg_n_0_[19]\,
      O => \cnt0_carry__3_i_2_n_0\
    );
\cnt0_carry__3_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cnt_reg_n_0_[18]\,
      O => \cnt0_carry__3_i_3_n_0\
    );
\cnt0_carry__3_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cnt_reg_n_0_[17]\,
      O => \cnt0_carry__3_i_4_n_0\
    );
\cnt0_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt0_carry__3_n_0\,
      CO(3) => \cnt0_carry__4_n_0\,
      CO(2) => \cnt0_carry__4_n_1\,
      CO(1) => \cnt0_carry__4_n_2\,
      CO(0) => \cnt0_carry__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt0_carry__4_n_4\,
      O(2) => \cnt0_carry__4_n_5\,
      O(1) => \cnt0_carry__4_n_6\,
      O(0) => \cnt0_carry__4_n_7\,
      S(3) => \cnt0_carry__4_i_1_n_0\,
      S(2) => \cnt0_carry__4_i_2_n_0\,
      S(1) => \cnt0_carry__4_i_3_n_0\,
      S(0) => \cnt0_carry__4_i_4_n_0\
    );
\cnt0_carry__4_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cnt_reg_n_0_[24]\,
      O => \cnt0_carry__4_i_1_n_0\
    );
\cnt0_carry__4_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cnt_reg_n_0_[23]\,
      O => \cnt0_carry__4_i_2_n_0\
    );
\cnt0_carry__4_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cnt_reg_n_0_[22]\,
      O => \cnt0_carry__4_i_3_n_0\
    );
\cnt0_carry__4_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cnt_reg_n_0_[21]\,
      O => \cnt0_carry__4_i_4_n_0\
    );
\cnt0_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt0_carry__4_n_0\,
      CO(3) => \cnt0_carry__5_n_0\,
      CO(2) => \cnt0_carry__5_n_1\,
      CO(1) => \cnt0_carry__5_n_2\,
      CO(0) => \cnt0_carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \cnt0_carry__5_n_4\,
      O(2) => \cnt0_carry__5_n_5\,
      O(1) => \cnt0_carry__5_n_6\,
      O(0) => \cnt0_carry__5_n_7\,
      S(3) => \cnt0_carry__5_i_1_n_0\,
      S(2) => \cnt0_carry__5_i_2_n_0\,
      S(1) => \cnt0_carry__5_i_3_n_0\,
      S(0) => \cnt0_carry__5_i_4_n_0\
    );
\cnt0_carry__5_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cnt_reg_n_0_[28]\,
      O => \cnt0_carry__5_i_1_n_0\
    );
\cnt0_carry__5_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cnt_reg_n_0_[27]\,
      O => \cnt0_carry__5_i_2_n_0\
    );
\cnt0_carry__5_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cnt_reg_n_0_[26]\,
      O => \cnt0_carry__5_i_3_n_0\
    );
\cnt0_carry__5_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cnt_reg_n_0_[25]\,
      O => \cnt0_carry__5_i_4_n_0\
    );
\cnt0_carry__6\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt0_carry__5_n_0\,
      CO(3 downto 1) => \NLW_cnt0_carry__6_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \cnt0_carry__6_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 2) => \NLW_cnt0_carry__6_O_UNCONNECTED\(3 downto 2),
      O(1) => \cnt0_carry__6_n_6\,
      O(0) => \cnt0_carry__6_n_7\,
      S(3 downto 2) => B"00",
      S(1) => \cnt0_carry__6_i_1_n_0\,
      S(0) => \cnt0_carry__6_i_2_n_0\
    );
\cnt0_carry__6_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cnt_reg_n_0_[30]\,
      O => \cnt0_carry__6_i_1_n_0\
    );
\cnt0_carry__6_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cnt_reg_n_0_[29]\,
      O => \cnt0_carry__6_i_2_n_0\
    );
cnt0_carry_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cnt_reg_n_0_[4]\,
      O => cnt0_carry_i_1_n_0
    );
cnt0_carry_i_2: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cnt_reg_n_0_[3]\,
      O => cnt0_carry_i_2_n_0
    );
cnt0_carry_i_3: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cnt_reg_n_0_[2]\,
      O => cnt0_carry_i_3_n_0
    );
cnt0_carry_i_4: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \cnt_reg_n_0_[1]\,
      O => cnt0_carry_i_4_n_0
    );
\cnt[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00A000CF"
    )
        port map (
      I0 => state18_out,
      I1 => state(2),
      I2 => state(1),
      I3 => \cnt_reg_n_0_[0]\,
      I4 => state(0),
      O => cnt(0)
    );
\cnt[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A000CF00"
    )
        port map (
      I0 => state18_out,
      I1 => state(2),
      I2 => state(1),
      I3 => \cnt0_carry__1_n_6\,
      I4 => state(0),
      O => cnt(10)
    );
\cnt[11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A000CF00"
    )
        port map (
      I0 => state18_out,
      I1 => state(2),
      I2 => state(1),
      I3 => \cnt0_carry__1_n_5\,
      I4 => state(0),
      O => cnt(11)
    );
\cnt[12]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A000CF00"
    )
        port map (
      I0 => state18_out,
      I1 => state(2),
      I2 => state(1),
      I3 => \cnt0_carry__1_n_4\,
      I4 => state(0),
      O => cnt(12)
    );
\cnt[13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A000CF00"
    )
        port map (
      I0 => state18_out,
      I1 => state(2),
      I2 => state(1),
      I3 => \cnt0_carry__2_n_7\,
      I4 => state(0),
      O => cnt(13)
    );
\cnt[14]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A000CF00"
    )
        port map (
      I0 => state18_out,
      I1 => state(2),
      I2 => state(1),
      I3 => \cnt0_carry__2_n_6\,
      I4 => state(0),
      O => cnt(14)
    );
\cnt[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A000CF00"
    )
        port map (
      I0 => state18_out,
      I1 => state(2),
      I2 => state(1),
      I3 => \cnt0_carry__2_n_5\,
      I4 => state(0),
      O => cnt(15)
    );
\cnt[16]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A000CF00"
    )
        port map (
      I0 => state18_out,
      I1 => state(2),
      I2 => state(1),
      I3 => \cnt0_carry__2_n_4\,
      I4 => state(0),
      O => cnt(16)
    );
\cnt[17]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A000CF00"
    )
        port map (
      I0 => state18_out,
      I1 => state(2),
      I2 => state(1),
      I3 => \cnt0_carry__3_n_7\,
      I4 => state(0),
      O => cnt(17)
    );
\cnt[18]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A000CF00"
    )
        port map (
      I0 => state18_out,
      I1 => state(2),
      I2 => state(1),
      I3 => \cnt0_carry__3_n_6\,
      I4 => state(0),
      O => cnt(18)
    );
\cnt[19]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A000CF00"
    )
        port map (
      I0 => state18_out,
      I1 => state(2),
      I2 => state(1),
      I3 => \cnt0_carry__3_n_5\,
      I4 => state(0),
      O => cnt(19)
    );
\cnt[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A000CF00"
    )
        port map (
      I0 => state18_out,
      I1 => state(2),
      I2 => state(1),
      I3 => cnt0_carry_n_7,
      I4 => state(0),
      O => cnt(1)
    );
\cnt[20]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A000CF00"
    )
        port map (
      I0 => state18_out,
      I1 => state(2),
      I2 => state(1),
      I3 => \cnt0_carry__3_n_4\,
      I4 => state(0),
      O => cnt(20)
    );
\cnt[21]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A000CF00"
    )
        port map (
      I0 => state18_out,
      I1 => state(2),
      I2 => state(1),
      I3 => \cnt0_carry__4_n_7\,
      I4 => state(0),
      O => cnt(21)
    );
\cnt[22]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A000CF00"
    )
        port map (
      I0 => state18_out,
      I1 => state(2),
      I2 => state(1),
      I3 => \cnt0_carry__4_n_6\,
      I4 => state(0),
      O => cnt(22)
    );
\cnt[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A000CF00"
    )
        port map (
      I0 => state18_out,
      I1 => state(2),
      I2 => state(1),
      I3 => \cnt0_carry__4_n_5\,
      I4 => state(0),
      O => cnt(23)
    );
\cnt[24]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A000CF00"
    )
        port map (
      I0 => state18_out,
      I1 => state(2),
      I2 => state(1),
      I3 => \cnt0_carry__4_n_4\,
      I4 => state(0),
      O => cnt(24)
    );
\cnt[25]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A000CF00"
    )
        port map (
      I0 => state18_out,
      I1 => state(2),
      I2 => state(1),
      I3 => \cnt0_carry__5_n_7\,
      I4 => state(0),
      O => cnt(25)
    );
\cnt[26]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A000CF00"
    )
        port map (
      I0 => state18_out,
      I1 => state(2),
      I2 => state(1),
      I3 => \cnt0_carry__5_n_6\,
      I4 => state(0),
      O => cnt(26)
    );
\cnt[27]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A000CF00"
    )
        port map (
      I0 => state18_out,
      I1 => state(2),
      I2 => state(1),
      I3 => \cnt0_carry__5_n_5\,
      I4 => state(0),
      O => cnt(27)
    );
\cnt[28]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A000CF00"
    )
        port map (
      I0 => state18_out,
      I1 => state(2),
      I2 => state(1),
      I3 => \cnt0_carry__5_n_4\,
      I4 => state(0),
      O => cnt(28)
    );
\cnt[29]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A000CF00"
    )
        port map (
      I0 => state18_out,
      I1 => state(2),
      I2 => state(1),
      I3 => \cnt0_carry__6_n_7\,
      I4 => state(0),
      O => cnt(29)
    );
\cnt[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A000CF00"
    )
        port map (
      I0 => state18_out,
      I1 => state(2),
      I2 => state(1),
      I3 => cnt0_carry_n_6,
      I4 => state(0),
      O => cnt(2)
    );
\cnt[30]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00F0FFFFEEFF00FF"
    )
        port map (
      I0 => led1_i_2_n_0,
      I1 => fetching,
      I2 => state16_out,
      I3 => state(0),
      I4 => state(1),
      I5 => state(2),
      O => \cnt[30]_i_1_n_0\
    );
\cnt[30]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A000CF00"
    )
        port map (
      I0 => state18_out,
      I1 => state(2),
      I2 => state(1),
      I3 => \cnt0_carry__6_n_6\,
      I4 => state(0),
      O => cnt(30)
    );
\cnt[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A000CF00"
    )
        port map (
      I0 => state18_out,
      I1 => state(2),
      I2 => state(1),
      I3 => cnt0_carry_n_5,
      I4 => state(0),
      O => cnt(3)
    );
\cnt[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A000CF00"
    )
        port map (
      I0 => state18_out,
      I1 => state(2),
      I2 => state(1),
      I3 => cnt0_carry_n_4,
      I4 => state(0),
      O => cnt(4)
    );
\cnt[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A000CF00"
    )
        port map (
      I0 => state18_out,
      I1 => state(2),
      I2 => state(1),
      I3 => \cnt0_carry__0_n_7\,
      I4 => state(0),
      O => cnt(5)
    );
\cnt[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A000CF00"
    )
        port map (
      I0 => state18_out,
      I1 => state(2),
      I2 => state(1),
      I3 => \cnt0_carry__0_n_6\,
      I4 => state(0),
      O => cnt(6)
    );
\cnt[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A000CF00"
    )
        port map (
      I0 => state18_out,
      I1 => state(2),
      I2 => state(1),
      I3 => \cnt0_carry__0_n_5\,
      I4 => state(0),
      O => cnt(7)
    );
\cnt[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A000CF00"
    )
        port map (
      I0 => state18_out,
      I1 => state(2),
      I2 => state(1),
      I3 => \cnt0_carry__0_n_4\,
      I4 => state(0),
      O => cnt(8)
    );
\cnt[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A000CF00"
    )
        port map (
      I0 => state18_out,
      I1 => state(2),
      I2 => state(1),
      I3 => \cnt0_carry__1_n_7\,
      I4 => state(0),
      O => cnt(9)
    );
\cnt_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \cnt[30]_i_1_n_0\,
      D => cnt(0),
      Q => \cnt_reg_n_0_[0]\,
      R => '0'
    );
\cnt_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \cnt[30]_i_1_n_0\,
      D => cnt(10),
      Q => \cnt_reg_n_0_[10]\,
      R => '0'
    );
\cnt_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \cnt[30]_i_1_n_0\,
      D => cnt(11),
      Q => \cnt_reg_n_0_[11]\,
      R => '0'
    );
\cnt_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \cnt[30]_i_1_n_0\,
      D => cnt(12),
      Q => \cnt_reg_n_0_[12]\,
      R => '0'
    );
\cnt_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \cnt[30]_i_1_n_0\,
      D => cnt(13),
      Q => \cnt_reg_n_0_[13]\,
      R => '0'
    );
\cnt_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \cnt[30]_i_1_n_0\,
      D => cnt(14),
      Q => \cnt_reg_n_0_[14]\,
      R => '0'
    );
\cnt_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \cnt[30]_i_1_n_0\,
      D => cnt(15),
      Q => \cnt_reg_n_0_[15]\,
      R => '0'
    );
\cnt_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \cnt[30]_i_1_n_0\,
      D => cnt(16),
      Q => \cnt_reg_n_0_[16]\,
      R => '0'
    );
\cnt_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \cnt[30]_i_1_n_0\,
      D => cnt(17),
      Q => \cnt_reg_n_0_[17]\,
      R => '0'
    );
\cnt_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \cnt[30]_i_1_n_0\,
      D => cnt(18),
      Q => \cnt_reg_n_0_[18]\,
      R => '0'
    );
\cnt_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \cnt[30]_i_1_n_0\,
      D => cnt(19),
      Q => \cnt_reg_n_0_[19]\,
      R => '0'
    );
\cnt_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \cnt[30]_i_1_n_0\,
      D => cnt(1),
      Q => \cnt_reg_n_0_[1]\,
      R => '0'
    );
\cnt_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \cnt[30]_i_1_n_0\,
      D => cnt(20),
      Q => \cnt_reg_n_0_[20]\,
      R => '0'
    );
\cnt_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \cnt[30]_i_1_n_0\,
      D => cnt(21),
      Q => \cnt_reg_n_0_[21]\,
      R => '0'
    );
\cnt_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \cnt[30]_i_1_n_0\,
      D => cnt(22),
      Q => \cnt_reg_n_0_[22]\,
      R => '0'
    );
\cnt_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \cnt[30]_i_1_n_0\,
      D => cnt(23),
      Q => \cnt_reg_n_0_[23]\,
      R => '0'
    );
\cnt_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \cnt[30]_i_1_n_0\,
      D => cnt(24),
      Q => \cnt_reg_n_0_[24]\,
      R => '0'
    );
\cnt_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \cnt[30]_i_1_n_0\,
      D => cnt(25),
      Q => \cnt_reg_n_0_[25]\,
      R => '0'
    );
\cnt_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \cnt[30]_i_1_n_0\,
      D => cnt(26),
      Q => \cnt_reg_n_0_[26]\,
      R => '0'
    );
\cnt_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \cnt[30]_i_1_n_0\,
      D => cnt(27),
      Q => \cnt_reg_n_0_[27]\,
      R => '0'
    );
\cnt_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \cnt[30]_i_1_n_0\,
      D => cnt(28),
      Q => \cnt_reg_n_0_[28]\,
      R => '0'
    );
\cnt_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \cnt[30]_i_1_n_0\,
      D => cnt(29),
      Q => \cnt_reg_n_0_[29]\,
      R => '0'
    );
\cnt_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \cnt[30]_i_1_n_0\,
      D => cnt(2),
      Q => \cnt_reg_n_0_[2]\,
      R => '0'
    );
\cnt_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \cnt[30]_i_1_n_0\,
      D => cnt(30),
      Q => \cnt_reg_n_0_[30]\,
      R => '0'
    );
\cnt_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \cnt[30]_i_1_n_0\,
      D => cnt(3),
      Q => \cnt_reg_n_0_[3]\,
      R => '0'
    );
\cnt_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \cnt[30]_i_1_n_0\,
      D => cnt(4),
      Q => \cnt_reg_n_0_[4]\,
      R => '0'
    );
\cnt_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \cnt[30]_i_1_n_0\,
      D => cnt(5),
      Q => \cnt_reg_n_0_[5]\,
      R => '0'
    );
\cnt_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \cnt[30]_i_1_n_0\,
      D => cnt(6),
      Q => \cnt_reg_n_0_[6]\,
      R => '0'
    );
\cnt_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \cnt[30]_i_1_n_0\,
      D => cnt(7),
      Q => \cnt_reg_n_0_[7]\,
      R => '0'
    );
\cnt_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \cnt[30]_i_1_n_0\,
      D => cnt(8),
      Q => \cnt_reg_n_0_[8]\,
      R => '0'
    );
\cnt_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => \cnt[30]_i_1_n_0\,
      D => cnt(9),
      Q => \cnt_reg_n_0_[9]\,
      R => '0'
    );
data_type_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DF04"
    )
        port map (
      I0 => state(0),
      I1 => state(1),
      I2 => state(2),
      I3 => \^data_type\,
      O => data_type_i_1_n_0
    );
data_type_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => data_type_i_1_n_0,
      Q => \^data_type\,
      R => '0'
    );
fetch_complete_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BFFC8000"
    )
        port map (
      I0 => fetch_complete_i_2_n_0,
      I1 => state(0),
      I2 => state(1),
      I3 => state(2),
      I4 => \^fetch_complete\,
      O => fetch_complete_i_1_n_0
    );
fetch_complete_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00010001FFFF0001"
    )
        port map (
      I0 => fetch_complete_i_3_n_0,
      I1 => \rand_reg_n_0_[4]\,
      I2 => \rand_reg_n_0_[6]\,
      I3 => \rand_reg_n_0_[3]\,
      I4 => fetch_complete_i_4_n_0,
      I5 => fetch_complete_i_5_n_0,
      O => fetch_complete_i_2_n_0
    );
fetch_complete_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \rand_reg_n_0_[2]\,
      I1 => \rand_reg_n_0_[0]\,
      I2 => \rand_reg_n_0_[5]\,
      I3 => \rand_reg_n_0_[1]\,
      O => fetch_complete_i_3_n_0
    );
fetch_complete_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1401004000401401"
    )
        port map (
      I0 => fetch_complete_i_6_n_0,
      I1 => \rand_reg_n_0_[4]\,
      I2 => fetch_complete_i_7_n_0,
      I3 => Q(4),
      I4 => \rand_reg_n_0_[5]\,
      I5 => Q(5),
      O => fetch_complete_i_4_n_0
    );
fetch_complete_i_5: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF9E9EFF"
    )
        port map (
      I0 => \rand_reg_n_0_[6]\,
      I1 => \rand[6]_i_3_n_0\,
      I2 => Q(6),
      I3 => \rand_reg_n_0_[3]\,
      I4 => fetch_complete_i_8_n_0,
      O => fetch_complete_i_5_n_0
    );
fetch_complete_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF6FF6FFF9FFFF6F"
    )
        port map (
      I0 => Q(2),
      I1 => \rand_reg_n_0_[2]\,
      I2 => Q(0),
      I3 => \rand_reg_n_0_[0]\,
      I4 => \rand_reg_n_0_[1]\,
      I5 => Q(1),
      O => fetch_complete_i_6_n_0
    );
fetch_complete_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => Q(0),
      I1 => Q(1),
      I2 => Q(3),
      I3 => Q(2),
      O => fetch_complete_i_7_n_0
    );
fetch_complete_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"5556"
    )
        port map (
      I0 => Q(3),
      I1 => Q(1),
      I2 => Q(0),
      I3 => Q(2),
      O => fetch_complete_i_8_n_0
    );
fetch_complete_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => fetch_complete_i_1_n_0,
      Q => \^fetch_complete\,
      R => '0'
    );
fetch_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9EDEBEFE18181818"
    )
        port map (
      I0 => state(2),
      I1 => state(0),
      I2 => state(1),
      I3 => state18_out,
      I4 => state16_out,
      I5 => \^fetch\,
      O => fetch_i_1_n_0
    );
fetch_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1000"
    )
        port map (
      I0 => \cnt_reg_n_0_[13]\,
      I1 => \cnt_reg_n_0_[14]\,
      I2 => state18_out,
      I3 => fetch_i_3_n_0,
      O => state16_out
    );
fetch_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"000007FFFFFFFFFF"
    )
        port map (
      I0 => \cnt_reg_n_0_[6]\,
      I1 => \cnt_reg_n_0_[7]\,
      I2 => \cnt_reg_n_0_[8]\,
      I3 => \cnt_reg_n_0_[9]\,
      I4 => fetch_i_4_n_0,
      I5 => \cnt_reg_n_0_[12]\,
      O => fetch_i_3_n_0
    );
fetch_i_4: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \cnt_reg_n_0_[10]\,
      I1 => \cnt_reg_n_0_[11]\,
      O => fetch_i_4_n_0
    );
fetch_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => fetch_i_1_n_0,
      Q => \^fetch\,
      R => '0'
    );
fetching_sprites_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000002"
    )
        port map (
      I0 => fetching,
      I1 => fetching_sprites_i_2_n_0,
      I2 => fetching_sprites_i_3_n_0,
      I3 => fetching_sprites_i_4_n_0,
      I4 => fetching_sprites_i_5_n_0,
      O => state18_out
    );
fetching_sprites_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \cnt_reg_n_0_[24]\,
      I1 => \cnt_reg_n_0_[23]\,
      I2 => \cnt_reg_n_0_[26]\,
      I3 => \cnt_reg_n_0_[25]\,
      O => fetching_sprites_i_2_n_0
    );
fetching_sprites_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \cnt_reg_n_0_[28]\,
      I1 => \cnt_reg_n_0_[27]\,
      I2 => \cnt_reg_n_0_[30]\,
      I3 => \cnt_reg_n_0_[29]\,
      O => fetching_sprites_i_3_n_0
    );
fetching_sprites_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \cnt_reg_n_0_[16]\,
      I1 => \cnt_reg_n_0_[15]\,
      I2 => \cnt_reg_n_0_[18]\,
      I3 => \cnt_reg_n_0_[17]\,
      O => fetching_sprites_i_4_n_0
    );
fetching_sprites_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \cnt_reg_n_0_[20]\,
      I1 => \cnt_reg_n_0_[19]\,
      I2 => \cnt_reg_n_0_[22]\,
      I3 => \cnt_reg_n_0_[21]\,
      O => fetching_sprites_i_5_n_0
    );
fetching_sprites_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \pixel_out[5]_i_2_n_0\,
      D => state18_out,
      Q => \^fetching_sprites\,
      R => \pixel_out[5]_i_1_n_0\
    );
\ind[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^fetching_sprites\,
      I1 => ind_reg(0),
      O => addra0
    );
led0_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE02"
    )
        port map (
      I0 => state(1),
      I1 => state(0),
      I2 => state(2),
      I3 => \^led0\,
      O => led0_i_1_n_0
    );
led0_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => led0_i_1_n_0,
      Q => \^led0\,
      R => '0'
    );
led1_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEFE4000"
    )
        port map (
      I0 => state(2),
      I1 => state(1),
      I2 => state(0),
      I3 => led1_i_2_n_0,
      I4 => \^led1\,
      O => led1_i_1_n_0
    );
led1_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => fetching_sprites_i_5_n_0,
      I1 => fetching_sprites_i_4_n_0,
      I2 => fetching_sprites_i_3_n_0,
      I3 => fetching_sprites_i_2_n_0,
      O => led1_i_2_n_0
    );
led1_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => led1_i_1_n_0,
      Q => \^led1\,
      R => '0'
    );
led2_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE40"
    )
        port map (
      I0 => state(1),
      I1 => state(2),
      I2 => state(0),
      I3 => \^led2\,
      O => led2_i_1_n_0
    );
led2_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => led2_i_1_n_0,
      Q => \^led2\,
      R => '0'
    );
led3_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FEFE4000"
    )
        port map (
      I0 => state(0),
      I1 => state(2),
      I2 => state(1),
      I3 => led3_i_2_n_0,
      I4 => \^led3\,
      O => led3_i_1_n_0
    );
led3_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEFF"
    )
        port map (
      I0 => led1_i_2_n_0,
      I1 => \cnt_reg_n_0_[13]\,
      I2 => \cnt_reg_n_0_[14]\,
      I3 => fetch_i_3_n_0,
      O => led3_i_2_n_0
    );
led3_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => led3_i_1_n_0,
      Q => \^led3\,
      R => '0'
    );
\map_id[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => state(1),
      I1 => state(0),
      I2 => state(2),
      O => \map_id[6]_i_1_n_0\
    );
\map_id[6]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"24"
    )
        port map (
      I0 => state(0),
      I1 => state(1),
      I2 => state(2),
      O => \map_id[6]_i_2_n_0\
    );
\map_id_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \map_id[6]_i_2_n_0\,
      D => \rand_reg_n_0_[0]\,
      Q => map_id(0),
      R => \map_id[6]_i_1_n_0\
    );
\map_id_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \map_id[6]_i_2_n_0\,
      D => \rand_reg_n_0_[1]\,
      Q => map_id(1),
      R => \map_id[6]_i_1_n_0\
    );
\map_id_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \map_id[6]_i_2_n_0\,
      D => \rand_reg_n_0_[2]\,
      Q => map_id(2),
      R => \map_id[6]_i_1_n_0\
    );
\map_id_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \map_id[6]_i_2_n_0\,
      D => \rand_reg_n_0_[3]\,
      Q => map_id(3),
      R => \map_id[6]_i_1_n_0\
    );
\map_id_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \map_id[6]_i_2_n_0\,
      D => \rand_reg_n_0_[4]\,
      Q => map_id(4),
      R => \map_id[6]_i_1_n_0\
    );
\map_id_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \map_id[6]_i_2_n_0\,
      D => \rand_reg_n_0_[5]\,
      Q => map_id(5),
      R => \map_id[6]_i_1_n_0\
    );
\map_id_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \map_id[6]_i_2_n_0\,
      D => \rand_reg_n_0_[6]\,
      Q => map_id(6),
      R => \map_id[6]_i_1_n_0\
    );
\pixel_out[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state18_out,
      I1 => packet_in(0),
      O => \pixel_out[0]_i_1_n_0\
    );
\pixel_out[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state18_out,
      I1 => packet_in(1),
      O => \pixel_out[1]_i_1_n_0\
    );
\pixel_out[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state18_out,
      I1 => packet_in(2),
      O => \pixel_out[2]_i_1_n_0\
    );
\pixel_out[3]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state18_out,
      I1 => packet_in(3),
      O => \pixel_out[3]_i_1_n_0\
    );
\pixel_out[4]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state18_out,
      I1 => packet_in(4),
      O => \pixel_out[4]_i_1_n_0\
    );
\pixel_out[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => state(2),
      I1 => state(1),
      I2 => state(0),
      O => \pixel_out[5]_i_1_n_0\
    );
\pixel_out[5]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"41414101"
    )
        port map (
      I0 => state(2),
      I1 => state(1),
      I2 => state(0),
      I3 => led1_i_2_n_0,
      I4 => fetching,
      O => \pixel_out[5]_i_2_n_0\
    );
\pixel_out[5]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state18_out,
      I1 => packet_in(5),
      O => \pixel_out[5]_i_3_n_0\
    );
\pixel_out_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \pixel_out[5]_i_2_n_0\,
      D => \pixel_out[0]_i_1_n_0\,
      Q => dina(0),
      R => \pixel_out[5]_i_1_n_0\
    );
\pixel_out_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \pixel_out[5]_i_2_n_0\,
      D => \pixel_out[1]_i_1_n_0\,
      Q => dina(1),
      R => \pixel_out[5]_i_1_n_0\
    );
\pixel_out_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \pixel_out[5]_i_2_n_0\,
      D => \pixel_out[2]_i_1_n_0\,
      Q => dina(2),
      R => \pixel_out[5]_i_1_n_0\
    );
\pixel_out_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \pixel_out[5]_i_2_n_0\,
      D => \pixel_out[3]_i_1_n_0\,
      Q => dina(3),
      R => \pixel_out[5]_i_1_n_0\
    );
\pixel_out_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \pixel_out[5]_i_2_n_0\,
      D => \pixel_out[4]_i_1_n_0\,
      Q => dina(4),
      R => \pixel_out[5]_i_1_n_0\
    );
\pixel_out_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \pixel_out[5]_i_2_n_0\,
      D => \pixel_out[5]_i_3_n_0\,
      Q => dina(5),
      R => \pixel_out[5]_i_1_n_0\
    );
\rand[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => Q(0),
      O => state2(0)
    );
\rand[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => Q(0),
      I1 => Q(1),
      O => rand0(1)
    );
\rand[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E1"
    )
        port map (
      I0 => Q(1),
      I1 => Q(0),
      I2 => Q(2),
      O => rand0(2)
    );
\rand[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE01"
    )
        port map (
      I0 => Q(2),
      I1 => Q(0),
      I2 => Q(1),
      I3 => Q(3),
      O => rand0(3)
    );
\rand[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0001"
    )
        port map (
      I0 => Q(0),
      I1 => Q(1),
      I2 => Q(3),
      I3 => Q(2),
      I4 => Q(4),
      O => rand0(4)
    );
\rand[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAAAAAAAAAAA9"
    )
        port map (
      I0 => Q(5),
      I1 => Q(4),
      I2 => Q(0),
      I3 => Q(1),
      I4 => Q(3),
      I5 => Q(2),
      O => rand0(5)
    );
\rand[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => state(1),
      I1 => state(2),
      I2 => state(0),
      O => rand
    );
\rand[6]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => Q(6),
      I1 => \rand[6]_i_3_n_0\,
      O => rand0(6)
    );
\rand[6]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => Q(5),
      I1 => Q(2),
      I2 => Q(3),
      I3 => Q(1),
      I4 => Q(0),
      I5 => Q(4),
      O => \rand[6]_i_3_n_0\
    );
\rand_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => rand,
      D => state2(0),
      Q => \rand_reg_n_0_[0]\,
      R => '0'
    );
\rand_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => rand,
      D => rand0(1),
      Q => \rand_reg_n_0_[1]\,
      R => '0'
    );
\rand_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => rand,
      D => rand0(2),
      Q => \rand_reg_n_0_[2]\,
      R => '0'
    );
\rand_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => rand,
      D => rand0(3),
      Q => \rand_reg_n_0_[3]\,
      R => '0'
    );
\rand_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => rand,
      D => rand0(4),
      Q => \rand_reg_n_0_[4]\,
      R => '0'
    );
\rand_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => rand,
      D => rand0(5),
      Q => \rand_reg_n_0_[5]\,
      R => '0'
    );
\rand_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => rand,
      D => rand0(6),
      Q => \rand_reg_n_0_[6]\,
      R => '0'
    );
\tmp_rand[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \cnt_reg_n_0_[0]\,
      O => \tmp_rand[0]_i_1_n_0\
    );
\tmp_rand[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \cnt_reg_n_0_[0]\,
      I1 => \cnt_reg_n_0_[1]\,
      O => \tmp_rand[1]_i_1_n_0\
    );
\tmp_rand[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \cnt_reg_n_0_[1]\,
      I1 => \cnt_reg_n_0_[0]\,
      I2 => \cnt_reg_n_0_[2]\,
      O => \tmp_rand[2]_i_1_n_0\
    );
\tmp_rand[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \cnt_reg_n_0_[2]\,
      I1 => \cnt_reg_n_0_[0]\,
      I2 => \cnt_reg_n_0_[1]\,
      I3 => \cnt_reg_n_0_[3]\,
      O => \tmp_rand[3]_i_1_n_0\
    );
\tmp_rand[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \cnt_reg_n_0_[3]\,
      I1 => \cnt_reg_n_0_[1]\,
      I2 => \cnt_reg_n_0_[0]\,
      I3 => \cnt_reg_n_0_[2]\,
      I4 => \cnt_reg_n_0_[4]\,
      O => \tmp_rand[4]_i_1_n_0\
    );
\tmp_rand[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \cnt_reg_n_0_[4]\,
      I1 => \cnt_reg_n_0_[2]\,
      I2 => \cnt_reg_n_0_[0]\,
      I3 => \cnt_reg_n_0_[1]\,
      I4 => \cnt_reg_n_0_[3]\,
      I5 => \cnt_reg_n_0_[5]\,
      O => \tmp_rand[5]_i_1_n_0\
    );
\tmp_rand[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => state(0),
      I1 => state(2),
      I2 => state(1),
      O => \tmp_rand[6]_i_1_n_0\
    );
\tmp_rand[6]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0000005D"
    )
        port map (
      I0 => state(0),
      I1 => \rand[6]_i_3_n_0\,
      I2 => Q(6),
      I3 => state(2),
      I4 => state(1),
      O => \tmp_rand[6]_i_2_n_0\
    );
\tmp_rand[6]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => \cnt_reg_n_0_[5]\,
      I1 => \tmp_rand[6]_i_4_n_0\,
      I2 => \cnt_reg_n_0_[6]\,
      O => \tmp_rand[6]_i_3_n_0\
    );
\tmp_rand[6]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFFFFFF"
    )
        port map (
      I0 => \cnt_reg_n_0_[3]\,
      I1 => \cnt_reg_n_0_[1]\,
      I2 => \cnt_reg_n_0_[0]\,
      I3 => \cnt_reg_n_0_[2]\,
      I4 => \cnt_reg_n_0_[4]\,
      O => \tmp_rand[6]_i_4_n_0\
    );
\tmp_rand_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \tmp_rand[6]_i_2_n_0\,
      D => \tmp_rand[0]_i_1_n_0\,
      Q => D(0),
      R => \tmp_rand[6]_i_1_n_0\
    );
\tmp_rand_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \tmp_rand[6]_i_2_n_0\,
      D => \tmp_rand[1]_i_1_n_0\,
      Q => D(1),
      R => \tmp_rand[6]_i_1_n_0\
    );
\tmp_rand_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \tmp_rand[6]_i_2_n_0\,
      D => \tmp_rand[2]_i_1_n_0\,
      Q => D(2),
      R => \tmp_rand[6]_i_1_n_0\
    );
\tmp_rand_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \tmp_rand[6]_i_2_n_0\,
      D => \tmp_rand[3]_i_1_n_0\,
      Q => D(3),
      R => \tmp_rand[6]_i_1_n_0\
    );
\tmp_rand_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \tmp_rand[6]_i_2_n_0\,
      D => \tmp_rand[4]_i_1_n_0\,
      Q => D(4),
      R => \tmp_rand[6]_i_1_n_0\
    );
\tmp_rand_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \tmp_rand[6]_i_2_n_0\,
      D => \tmp_rand[5]_i_1_n_0\,
      Q => D(5),
      R => \tmp_rand[6]_i_1_n_0\
    );
\tmp_rand_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \tmp_rand[6]_i_2_n_0\,
      D => \tmp_rand[6]_i_3_n_0\,
      Q => D(6),
      R => \tmp_rand[6]_i_1_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vga_connector is
  port (
    render_enable : out STD_LOGIC;
    vga_hs : out STD_LOGIC;
    vga_vs : out STD_LOGIC;
    \addra_reg[14]_P\ : out STD_LOGIC;
    \addra_reg[14]_C\ : out STD_LOGIC;
    \addra_reg[13]_P\ : out STD_LOGIC;
    \addra_reg[13]_C\ : out STD_LOGIC;
    \addra_reg[12]_P\ : out STD_LOGIC;
    \addra_reg[12]_C\ : out STD_LOGIC;
    \addra_reg[11]_P\ : out STD_LOGIC;
    \addra_reg[11]_C\ : out STD_LOGIC;
    \addra_reg[10]_P\ : out STD_LOGIC;
    \addra_reg[10]_C\ : out STD_LOGIC;
    \addra_reg[9]_P\ : out STD_LOGIC;
    \addra_reg[9]_C\ : out STD_LOGIC;
    \addra_reg[8]_P\ : out STD_LOGIC;
    \addra_reg[8]_C\ : out STD_LOGIC;
    \addra_reg[7]_P\ : out STD_LOGIC;
    \addra_reg[7]_C\ : out STD_LOGIC;
    \addra_reg[6]_P\ : out STD_LOGIC;
    \addra_reg[6]_C\ : out STD_LOGIC;
    \addra_reg[5]_P\ : out STD_LOGIC;
    \addra_reg[5]_C\ : out STD_LOGIC;
    \addra_reg[4]_P\ : out STD_LOGIC;
    \addra_reg[4]_C\ : out STD_LOGIC;
    \addra_reg[3]_P\ : out STD_LOGIC;
    \addra_reg[3]_C\ : out STD_LOGIC;
    \addra_reg[2]_P\ : out STD_LOGIC;
    \addra_reg[2]_C\ : out STD_LOGIC;
    \addra_reg[1]_P\ : out STD_LOGIC;
    \addra_reg[1]_C\ : out STD_LOGIC;
    \addra_reg[0]_P\ : out STD_LOGIC;
    \addra_reg[0]_C\ : out STD_LOGIC;
    vga_r : out STD_LOGIC_VECTOR ( 2 downto 0 );
    vga_g : out STD_LOGIC_VECTOR ( 3 downto 0 );
    vga_b : out STD_LOGIC_VECTOR ( 2 downto 0 );
    pixel_clk : in STD_LOGIC;
    fetch_complete : in STD_LOGIC;
    \addra_reg[14]__0\ : in STD_LOGIC_VECTOR ( 14 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 9 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vga_connector;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vga_connector is
  signal HSYNC02_out : STD_LOGIC;
  signal HSYNC_i_1_n_0 : STD_LOGIC;
  signal HSYNC_i_2_n_0 : STD_LOGIC;
  signal VSYNC_i_1_n_0 : STD_LOGIC;
  signal VSYNC_i_2_n_0 : STD_LOGIC;
  signal \cnt[0]_i_1_n_0\ : STD_LOGIC;
  signal \cnt[1]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_reg_n_0_[0]\ : STD_LOGIC;
  signal \cnt_reg_n_0_[1]\ : STD_LOGIC;
  signal \^render_enable\ : STD_LOGIC;
  signal render_enable_i_1_n_0 : STD_LOGIC;
  signal render_enable_i_2_n_0 : STD_LOGIC;
  signal \^vga_hs\ : STD_LOGIC;
  signal \vga_r[4]_i_1_n_0\ : STD_LOGIC;
  signal \vga_r[4]_i_2_n_0\ : STD_LOGIC;
  signal \vga_r[4]_i_3_n_0\ : STD_LOGIC;
  signal \^vga_vs\ : STD_LOGIC;
  signal x0 : STD_LOGIC;
  signal \x[0]_i_1_n_0\ : STD_LOGIC;
  signal \x[1]_i_1_n_0\ : STD_LOGIC;
  signal \x[2]_i_1_n_0\ : STD_LOGIC;
  signal \x[3]_i_1_n_0\ : STD_LOGIC;
  signal \x[4]_i_1_n_0\ : STD_LOGIC;
  signal \x[5]_i_1_n_0\ : STD_LOGIC;
  signal \x[6]_i_1_n_0\ : STD_LOGIC;
  signal \x[7]_i_1_n_0\ : STD_LOGIC;
  signal \x[8]_i_1_n_0\ : STD_LOGIC;
  signal \x[9]_i_2_n_0\ : STD_LOGIC;
  signal \x[9]_i_3_n_0\ : STD_LOGIC;
  signal \x_reg__0\ : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal y : STD_LOGIC_VECTOR ( 9 downto 0 );
  signal \y[0]_i_2_n_0\ : STD_LOGIC;
  signal \y[0]_i_3_n_0\ : STD_LOGIC;
  signal \y[0]_i_4_n_0\ : STD_LOGIC;
  signal \y[0]_i_5_n_0\ : STD_LOGIC;
  signal \y[0]_i_6_n_0\ : STD_LOGIC;
  signal \y[3]_i_2_n_0\ : STD_LOGIC;
  signal \y[4]_i_2_n_0\ : STD_LOGIC;
  signal \y[9]_i_3_n_0\ : STD_LOGIC;
  signal \y[9]_i_4_n_0\ : STD_LOGIC;
  signal \y[9]_i_5_n_0\ : STD_LOGIC;
  signal \y[9]_i_6_n_0\ : STD_LOGIC;
  signal \y_reg_n_0_[0]\ : STD_LOGIC;
  signal \y_reg_n_0_[1]\ : STD_LOGIC;
  signal \y_reg_n_0_[2]\ : STD_LOGIC;
  signal \y_reg_n_0_[3]\ : STD_LOGIC;
  signal \y_reg_n_0_[4]\ : STD_LOGIC;
  signal \y_reg_n_0_[5]\ : STD_LOGIC;
  signal \y_reg_n_0_[6]\ : STD_LOGIC;
  signal \y_reg_n_0_[7]\ : STD_LOGIC;
  signal \y_reg_n_0_[8]\ : STD_LOGIC;
  signal \y_reg_n_0_[9]\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \addra_reg[0]_LDC_i_1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \addra_reg[0]_LDC_i_2\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \addra_reg[10]_LDC_i_1\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \addra_reg[10]_LDC_i_2\ : label is "soft_lutpair34";
  attribute SOFT_HLUTNM of \addra_reg[11]_LDC_i_1\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \addra_reg[11]_LDC_i_2\ : label is "soft_lutpair36";
  attribute SOFT_HLUTNM of \addra_reg[12]_LDC_i_1\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \addra_reg[12]_LDC_i_2\ : label is "soft_lutpair37";
  attribute SOFT_HLUTNM of \addra_reg[13]_LDC_i_1\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \addra_reg[13]_LDC_i_2\ : label is "soft_lutpair38";
  attribute SOFT_HLUTNM of \addra_reg[14]_LDC_i_1\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \addra_reg[14]_LDC_i_2\ : label is "soft_lutpair39";
  attribute SOFT_HLUTNM of \addra_reg[1]_LDC_i_1\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \addra_reg[1]_LDC_i_2\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \addra_reg[2]_LDC_i_1\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \addra_reg[2]_LDC_i_2\ : label is "soft_lutpair30";
  attribute SOFT_HLUTNM of \addra_reg[3]_LDC_i_1\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \addra_reg[3]_LDC_i_2\ : label is "soft_lutpair31";
  attribute SOFT_HLUTNM of \addra_reg[4]_LDC_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \addra_reg[4]_LDC_i_2\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \addra_reg[5]_LDC_i_1\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \addra_reg[5]_LDC_i_2\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \addra_reg[6]_LDC_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \addra_reg[6]_LDC_i_2\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \addra_reg[7]_LDC_i_1\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \addra_reg[7]_LDC_i_2\ : label is "soft_lutpair35";
  attribute SOFT_HLUTNM of \addra_reg[8]_LDC_i_1\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \addra_reg[8]_LDC_i_2\ : label is "soft_lutpair32";
  attribute SOFT_HLUTNM of \addra_reg[9]_LDC_i_1\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \addra_reg[9]_LDC_i_2\ : label is "soft_lutpair33";
  attribute SOFT_HLUTNM of \cnt[0]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \cnt[1]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \vga_r[4]_i_2\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \x[0]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \x[1]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \x[2]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \x[3]_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \x[4]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \x[6]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \x[7]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \x[8]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \x[9]_i_3\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \y[0]_i_2\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \y[0]_i_3\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \y[0]_i_4\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \y[0]_i_6\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \y[3]_i_2\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \y[5]_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \y[6]_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \y[7]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \y[8]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \y[9]_i_3\ : label is "soft_lutpair18";
begin
  render_enable <= \^render_enable\;
  vga_hs <= \^vga_hs\;
  vga_vs <= \^vga_vs\;
HSYNC_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"E000000EEEEEEEEE"
    )
        port map (
      I0 => \^vga_hs\,
      I1 => HSYNC02_out,
      I2 => \x_reg__0\(6),
      I3 => \x_reg__0\(4),
      I4 => \x_reg__0\(5),
      I5 => HSYNC_i_2_n_0,
      O => HSYNC_i_1_n_0
    );
HSYNC_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0080000000000000"
    )
        port map (
      I0 => \x_reg__0\(9),
      I1 => fetch_complete,
      I2 => \x_reg__0\(7),
      I3 => \x_reg__0\(8),
      I4 => \cnt_reg_n_0_[1]\,
      I5 => \cnt_reg_n_0_[0]\,
      O => HSYNC_i_2_n_0
    );
HSYNC_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => pixel_clk,
      CE => '1',
      D => HSYNC_i_1_n_0,
      Q => \^vga_hs\,
      R => '0'
    );
VSYNC_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"EAAA0AAAEAAAEAAA"
    )
        port map (
      I0 => \^vga_vs\,
      I1 => fetch_complete,
      I2 => \cnt_reg_n_0_[1]\,
      I3 => \cnt_reg_n_0_[0]\,
      I4 => \y_reg_n_0_[9]\,
      I5 => VSYNC_i_2_n_0,
      O => VSYNC_i_1_n_0
    );
VSYNC_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000002000000"
    )
        port map (
      I0 => fetch_complete,
      I1 => \y_reg_n_0_[4]\,
      I2 => \y_reg_n_0_[2]\,
      I3 => \y_reg_n_0_[3]\,
      I4 => \y_reg_n_0_[1]\,
      I5 => \vga_r[4]_i_2_n_0\,
      O => VSYNC_i_2_n_0
    );
VSYNC_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => pixel_clk,
      CE => '1',
      D => VSYNC_i_1_n_0,
      Q => \^vga_vs\,
      R => '0'
    );
\addra_reg[0]_LDC_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^render_enable\,
      I1 => \addra_reg[14]__0\(0),
      O => \addra_reg[0]_P\
    );
\addra_reg[0]_LDC_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^render_enable\,
      I1 => \addra_reg[14]__0\(0),
      O => \addra_reg[0]_C\
    );
\addra_reg[10]_LDC_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^render_enable\,
      I1 => \addra_reg[14]__0\(10),
      O => \addra_reg[10]_P\
    );
\addra_reg[10]_LDC_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^render_enable\,
      I1 => \addra_reg[14]__0\(10),
      O => \addra_reg[10]_C\
    );
\addra_reg[11]_LDC_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^render_enable\,
      I1 => \addra_reg[14]__0\(11),
      O => \addra_reg[11]_P\
    );
\addra_reg[11]_LDC_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^render_enable\,
      I1 => \addra_reg[14]__0\(11),
      O => \addra_reg[11]_C\
    );
\addra_reg[12]_LDC_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^render_enable\,
      I1 => \addra_reg[14]__0\(12),
      O => \addra_reg[12]_P\
    );
\addra_reg[12]_LDC_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^render_enable\,
      I1 => \addra_reg[14]__0\(12),
      O => \addra_reg[12]_C\
    );
\addra_reg[13]_LDC_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^render_enable\,
      I1 => \addra_reg[14]__0\(13),
      O => \addra_reg[13]_P\
    );
\addra_reg[13]_LDC_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^render_enable\,
      I1 => \addra_reg[14]__0\(13),
      O => \addra_reg[13]_C\
    );
\addra_reg[14]_LDC_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^render_enable\,
      I1 => \addra_reg[14]__0\(14),
      O => \addra_reg[14]_P\
    );
\addra_reg[14]_LDC_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^render_enable\,
      I1 => \addra_reg[14]__0\(14),
      O => \addra_reg[14]_C\
    );
\addra_reg[1]_LDC_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^render_enable\,
      I1 => \addra_reg[14]__0\(1),
      O => \addra_reg[1]_P\
    );
\addra_reg[1]_LDC_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^render_enable\,
      I1 => \addra_reg[14]__0\(1),
      O => \addra_reg[1]_C\
    );
\addra_reg[2]_LDC_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^render_enable\,
      I1 => \addra_reg[14]__0\(2),
      O => \addra_reg[2]_P\
    );
\addra_reg[2]_LDC_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^render_enable\,
      I1 => \addra_reg[14]__0\(2),
      O => \addra_reg[2]_C\
    );
\addra_reg[3]_LDC_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^render_enable\,
      I1 => \addra_reg[14]__0\(3),
      O => \addra_reg[3]_P\
    );
\addra_reg[3]_LDC_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^render_enable\,
      I1 => \addra_reg[14]__0\(3),
      O => \addra_reg[3]_C\
    );
\addra_reg[4]_LDC_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^render_enable\,
      I1 => \addra_reg[14]__0\(4),
      O => \addra_reg[4]_P\
    );
\addra_reg[4]_LDC_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^render_enable\,
      I1 => \addra_reg[14]__0\(4),
      O => \addra_reg[4]_C\
    );
\addra_reg[5]_LDC_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^render_enable\,
      I1 => \addra_reg[14]__0\(5),
      O => \addra_reg[5]_P\
    );
\addra_reg[5]_LDC_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^render_enable\,
      I1 => \addra_reg[14]__0\(5),
      O => \addra_reg[5]_C\
    );
\addra_reg[6]_LDC_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^render_enable\,
      I1 => \addra_reg[14]__0\(6),
      O => \addra_reg[6]_P\
    );
\addra_reg[6]_LDC_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^render_enable\,
      I1 => \addra_reg[14]__0\(6),
      O => \addra_reg[6]_C\
    );
\addra_reg[7]_LDC_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^render_enable\,
      I1 => \addra_reg[14]__0\(7),
      O => \addra_reg[7]_P\
    );
\addra_reg[7]_LDC_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^render_enable\,
      I1 => \addra_reg[14]__0\(7),
      O => \addra_reg[7]_C\
    );
\addra_reg[8]_LDC_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^render_enable\,
      I1 => \addra_reg[14]__0\(8),
      O => \addra_reg[8]_P\
    );
\addra_reg[8]_LDC_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^render_enable\,
      I1 => \addra_reg[14]__0\(8),
      O => \addra_reg[8]_C\
    );
\addra_reg[9]_LDC_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^render_enable\,
      I1 => \addra_reg[14]__0\(9),
      O => \addra_reg[9]_P\
    );
\addra_reg[9]_LDC_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^render_enable\,
      I1 => \addra_reg[14]__0\(9),
      O => \addra_reg[9]_C\
    );
\cnt[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"DA"
    )
        port map (
      I0 => fetch_complete,
      I1 => \cnt_reg_n_0_[1]\,
      I2 => \cnt_reg_n_0_[0]\,
      O => \cnt[0]_i_1_n_0\
    );
\cnt[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => \cnt_reg_n_0_[1]\,
      I1 => \cnt_reg_n_0_[0]\,
      I2 => fetch_complete,
      O => \cnt[1]_i_1_n_0\
    );
\cnt_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => '1',
      D => \cnt[0]_i_1_n_0\,
      Q => \cnt_reg_n_0_[0]\,
      R => '0'
    );
\cnt_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => '1',
      D => \cnt[1]_i_1_n_0\,
      Q => \cnt_reg_n_0_[1]\,
      R => '0'
    );
render_enable_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000AAAA00C0AAAA"
    )
        port map (
      I0 => \^render_enable\,
      I1 => \vga_r[4]_i_2_n_0\,
      I2 => \vga_r[4]_i_3_n_0\,
      I3 => \y_reg_n_0_[9]\,
      I4 => fetch_complete,
      I5 => render_enable_i_2_n_0,
      O => render_enable_i_1_n_0
    );
render_enable_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \cnt_reg_n_0_[0]\,
      I1 => \cnt_reg_n_0_[1]\,
      O => render_enable_i_2_n_0
    );
render_enable_reg: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => '1',
      D => render_enable_i_1_n_0,
      Q => \^render_enable\,
      R => '0'
    );
\vga_b_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => fetch_complete,
      D => Q(3),
      Q => vga_b(0),
      R => \vga_r[4]_i_1_n_0\
    );
\vga_b_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => fetch_complete,
      D => Q(4),
      Q => vga_b(1),
      R => \vga_r[4]_i_1_n_0\
    );
\vga_b_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => fetch_complete,
      D => Q(5),
      Q => vga_b(2),
      R => \vga_r[4]_i_1_n_0\
    );
\vga_g_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => fetch_complete,
      D => Q(6),
      Q => vga_g(0),
      R => \vga_r[4]_i_1_n_0\
    );
\vga_g_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => fetch_complete,
      D => Q(7),
      Q => vga_g(1),
      R => \vga_r[4]_i_1_n_0\
    );
\vga_g_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => fetch_complete,
      D => Q(8),
      Q => vga_g(2),
      R => \vga_r[4]_i_1_n_0\
    );
\vga_g_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => fetch_complete,
      D => Q(9),
      Q => vga_g(3),
      R => \vga_r[4]_i_1_n_0\
    );
\vga_r[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A2AAAAAAAAAAAAAA"
    )
        port map (
      I0 => fetch_complete,
      I1 => \vga_r[4]_i_2_n_0\,
      I2 => \y_reg_n_0_[9]\,
      I3 => \cnt_reg_n_0_[0]\,
      I4 => \cnt_reg_n_0_[1]\,
      I5 => \vga_r[4]_i_3_n_0\,
      O => \vga_r[4]_i_1_n_0\
    );
\vga_r[4]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => \y_reg_n_0_[7]\,
      I1 => \y_reg_n_0_[5]\,
      I2 => \y_reg_n_0_[6]\,
      I3 => \y_reg_n_0_[8]\,
      O => \vga_r[4]_i_2_n_0\
    );
\vga_r[4]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"1F"
    )
        port map (
      I0 => \x_reg__0\(8),
      I1 => \x_reg__0\(7),
      I2 => \x_reg__0\(9),
      O => \vga_r[4]_i_3_n_0\
    );
\vga_r_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => fetch_complete,
      D => Q(0),
      Q => vga_r(0),
      R => \vga_r[4]_i_1_n_0\
    );
\vga_r_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => fetch_complete,
      D => Q(1),
      Q => vga_r(1),
      R => \vga_r[4]_i_1_n_0\
    );
\vga_r_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => pixel_clk,
      CE => fetch_complete,
      D => Q(2),
      Q => vga_r(2),
      R => \vga_r[4]_i_1_n_0\
    );
\x[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \x_reg__0\(0),
      O => \x[0]_i_1_n_0\
    );
\x[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \x_reg__0\(0),
      I1 => \x_reg__0\(1),
      O => \x[1]_i_1_n_0\
    );
\x[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \x_reg__0\(1),
      I1 => \x_reg__0\(0),
      I2 => \x_reg__0\(2),
      O => \x[2]_i_1_n_0\
    );
\x[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \x_reg__0\(2),
      I1 => \x_reg__0\(0),
      I2 => \x_reg__0\(1),
      I3 => \x_reg__0\(3),
      O => \x[3]_i_1_n_0\
    );
\x[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \x_reg__0\(3),
      I1 => \x_reg__0\(1),
      I2 => \x_reg__0\(0),
      I3 => \x_reg__0\(2),
      I4 => \x_reg__0\(4),
      O => \x[4]_i_1_n_0\
    );
\x[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \x_reg__0\(4),
      I1 => \x_reg__0\(2),
      I2 => \x_reg__0\(0),
      I3 => \x_reg__0\(1),
      I4 => \x_reg__0\(3),
      I5 => \x_reg__0\(5),
      O => \x[5]_i_1_n_0\
    );
\x[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => \x_reg__0\(5),
      I1 => \x[9]_i_3_n_0\,
      I2 => \x_reg__0\(6),
      O => \x[6]_i_1_n_0\
    );
\x[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"BF40"
    )
        port map (
      I0 => \x[9]_i_3_n_0\,
      I1 => \x_reg__0\(5),
      I2 => \x_reg__0\(6),
      I3 => \x_reg__0\(7),
      O => \x[7]_i_1_n_0\
    );
\x[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BFFF4000"
    )
        port map (
      I0 => \x[9]_i_3_n_0\,
      I1 => \x_reg__0\(6),
      I2 => \x_reg__0\(5),
      I3 => \x_reg__0\(7),
      I4 => \x_reg__0\(8),
      O => \x[8]_i_1_n_0\
    );
\x[9]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => \cnt_reg_n_0_[0]\,
      I1 => \cnt_reg_n_0_[1]\,
      I2 => fetch_complete,
      I3 => \y[4]_i_2_n_0\,
      O => x0
    );
\x[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"BFFFFFFF40000000"
    )
        port map (
      I0 => \x[9]_i_3_n_0\,
      I1 => \x_reg__0\(8),
      I2 => \x_reg__0\(6),
      I3 => \x_reg__0\(5),
      I4 => \x_reg__0\(7),
      I5 => \x_reg__0\(9),
      O => \x[9]_i_2_n_0\
    );
\x[9]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFFFFFF"
    )
        port map (
      I0 => \x_reg__0\(3),
      I1 => \x_reg__0\(1),
      I2 => \x_reg__0\(0),
      I3 => \x_reg__0\(2),
      I4 => \x_reg__0\(4),
      O => \x[9]_i_3_n_0\
    );
\x_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => HSYNC02_out,
      D => \x[0]_i_1_n_0\,
      Q => \x_reg__0\(0),
      R => x0
    );
\x_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => HSYNC02_out,
      D => \x[1]_i_1_n_0\,
      Q => \x_reg__0\(1),
      R => x0
    );
\x_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => HSYNC02_out,
      D => \x[2]_i_1_n_0\,
      Q => \x_reg__0\(2),
      R => x0
    );
\x_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => HSYNC02_out,
      D => \x[3]_i_1_n_0\,
      Q => \x_reg__0\(3),
      R => x0
    );
\x_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => HSYNC02_out,
      D => \x[4]_i_1_n_0\,
      Q => \x_reg__0\(4),
      R => x0
    );
\x_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => HSYNC02_out,
      D => \x[5]_i_1_n_0\,
      Q => \x_reg__0\(5),
      R => x0
    );
\x_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => HSYNC02_out,
      D => \x[6]_i_1_n_0\,
      Q => \x_reg__0\(6),
      R => x0
    );
\x_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => HSYNC02_out,
      D => \x[7]_i_1_n_0\,
      Q => \x_reg__0\(7),
      R => x0
    );
\x_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => HSYNC02_out,
      D => \x[8]_i_1_n_0\,
      Q => \x_reg__0\(8),
      R => x0
    );
\x_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => HSYNC02_out,
      D => \x[9]_i_2_n_0\,
      Q => \x_reg__0\(9),
      R => x0
    );
\y[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF00FF00F700FF00"
    )
        port map (
      I0 => \y_reg_n_0_[3]\,
      I1 => \y_reg_n_0_[2]\,
      I2 => \y_reg_n_0_[1]\,
      I3 => \y[0]_i_2_n_0\,
      I4 => \y_reg_n_0_[9]\,
      I5 => \y[0]_i_3_n_0\,
      O => y(0)
    );
\y[0]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BC8C8C8C"
    )
        port map (
      I0 => \y[0]_i_4_n_0\,
      I1 => \y_reg_n_0_[0]\,
      I2 => \x_reg__0\(9),
      I3 => \y[0]_i_5_n_0\,
      I4 => \x_reg__0\(0),
      O => \y[0]_i_2_n_0\
    );
\y[0]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \y_reg_n_0_[4]\,
      I1 => \y_reg_n_0_[7]\,
      I2 => \y_reg_n_0_[8]\,
      I3 => \y_reg_n_0_[6]\,
      I4 => \y_reg_n_0_[5]\,
      O => \y[0]_i_3_n_0\
    );
\y[0]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFEFF"
    )
        port map (
      I0 => \x_reg__0\(5),
      I1 => \x_reg__0\(6),
      I2 => \x_reg__0\(7),
      I3 => \x_reg__0\(8),
      I4 => \x[9]_i_3_n_0\,
      O => \y[0]_i_4_n_0\
    );
\y[0]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00100000"
    )
        port map (
      I0 => \x_reg__0\(5),
      I1 => \x_reg__0\(6),
      I2 => \x_reg__0\(8),
      I3 => \x_reg__0\(7),
      I4 => \y[0]_i_6_n_0\,
      O => \y[0]_i_5_n_0\
    );
\y[0]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8000"
    )
        port map (
      I0 => \x_reg__0\(4),
      I1 => \x_reg__0\(3),
      I2 => \x_reg__0\(2),
      I3 => \x_reg__0\(1),
      O => \y[0]_i_6_n_0\
    );
\y[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => \y_reg_n_0_[0]\,
      I1 => \y[4]_i_2_n_0\,
      I2 => \y_reg_n_0_[1]\,
      O => y(1)
    );
\y[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF202000552020"
    )
        port map (
      I0 => \y_reg_n_0_[1]\,
      I1 => \y[4]_i_2_n_0\,
      I2 => \y_reg_n_0_[0]\,
      I3 => \y_reg_n_0_[3]\,
      I4 => \y_reg_n_0_[2]\,
      I5 => \y[3]_i_2_n_0\,
      O => y(2)
    );
\y[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF080055550800"
    )
        port map (
      I0 => \y_reg_n_0_[2]\,
      I1 => \y_reg_n_0_[1]\,
      I2 => \y[4]_i_2_n_0\,
      I3 => \y_reg_n_0_[0]\,
      I4 => \y_reg_n_0_[3]\,
      I5 => \y[3]_i_2_n_0\,
      O => y(3)
    );
\y[3]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F3FFF37D"
    )
        port map (
      I0 => \y_reg_n_0_[9]\,
      I1 => \y_reg_n_0_[0]\,
      I2 => \y[4]_i_2_n_0\,
      I3 => \y_reg_n_0_[1]\,
      I4 => \y[0]_i_3_n_0\,
      O => \y[3]_i_2_n_0\
    );
\y[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF7FFFFF00800000"
    )
        port map (
      I0 => \y_reg_n_0_[2]\,
      I1 => \y_reg_n_0_[3]\,
      I2 => \y_reg_n_0_[0]\,
      I3 => \y[4]_i_2_n_0\,
      I4 => \y_reg_n_0_[1]\,
      I5 => \y_reg_n_0_[4]\,
      O => y(4)
    );
\y[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFBFFFFFFFF"
    )
        port map (
      I0 => \x[9]_i_3_n_0\,
      I1 => \x_reg__0\(8),
      I2 => \x_reg__0\(7),
      I3 => \x_reg__0\(6),
      I4 => \x_reg__0\(5),
      I5 => \x_reg__0\(9),
      O => \y[4]_i_2_n_0\
    );
\y[5]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \y[9]_i_6_n_0\,
      I1 => \y_reg_n_0_[5]\,
      O => y(5)
    );
\y[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D2"
    )
        port map (
      I0 => \y_reg_n_0_[5]\,
      I1 => \y[9]_i_6_n_0\,
      I2 => \y_reg_n_0_[6]\,
      O => y(6)
    );
\y[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F708"
    )
        port map (
      I0 => \y_reg_n_0_[5]\,
      I1 => \y_reg_n_0_[6]\,
      I2 => \y[9]_i_6_n_0\,
      I3 => \y_reg_n_0_[7]\,
      O => y(7)
    );
\y[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF7F0080"
    )
        port map (
      I0 => \y_reg_n_0_[6]\,
      I1 => \y_reg_n_0_[5]\,
      I2 => \y_reg_n_0_[7]\,
      I3 => \y[9]_i_6_n_0\,
      I4 => \y_reg_n_0_[8]\,
      O => y(8)
    );
\y[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => fetch_complete,
      I1 => \cnt_reg_n_0_[1]\,
      I2 => \cnt_reg_n_0_[0]\,
      O => HSYNC02_out
    );
\y[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFE0FFE0FFE0FFEF"
    )
        port map (
      I0 => \y[9]_i_3_n_0\,
      I1 => \y[9]_i_4_n_0\,
      I2 => \y_reg_n_0_[9]\,
      I3 => \y[9]_i_5_n_0\,
      I4 => \y[9]_i_6_n_0\,
      I5 => \vga_r[4]_i_2_n_0\,
      O => y(9)
    );
\y[9]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"E3FE"
    )
        port map (
      I0 => \y[0]_i_3_n_0\,
      I1 => \y_reg_n_0_[1]\,
      I2 => \y[4]_i_2_n_0\,
      I3 => \y_reg_n_0_[0]\,
      O => \y[9]_i_3_n_0\
    );
\y[9]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \y_reg_n_0_[2]\,
      I1 => \y_reg_n_0_[3]\,
      O => \y[9]_i_4_n_0\
    );
\y[9]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8808"
    )
        port map (
      I0 => \y_reg_n_0_[1]\,
      I1 => \y_reg_n_0_[9]\,
      I2 => \y_reg_n_0_[4]\,
      I3 => \vga_r[4]_i_2_n_0\,
      O => \y[9]_i_5_n_0\
    );
\y[9]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FF7FFFFFFFFFFFFF"
    )
        port map (
      I0 => \y_reg_n_0_[2]\,
      I1 => \y_reg_n_0_[3]\,
      I2 => \y_reg_n_0_[0]\,
      I3 => \y[4]_i_2_n_0\,
      I4 => \y_reg_n_0_[1]\,
      I5 => \y_reg_n_0_[4]\,
      O => \y[9]_i_6_n_0\
    );
\y_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => HSYNC02_out,
      D => y(0),
      Q => \y_reg_n_0_[0]\,
      R => '0'
    );
\y_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => HSYNC02_out,
      D => y(1),
      Q => \y_reg_n_0_[1]\,
      R => '0'
    );
\y_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => HSYNC02_out,
      D => y(2),
      Q => \y_reg_n_0_[2]\,
      R => '0'
    );
\y_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => HSYNC02_out,
      D => y(3),
      Q => \y_reg_n_0_[3]\,
      R => '0'
    );
\y_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => HSYNC02_out,
      D => y(4),
      Q => \y_reg_n_0_[4]\,
      R => '0'
    );
\y_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => HSYNC02_out,
      D => y(5),
      Q => \y_reg_n_0_[5]\,
      R => '0'
    );
\y_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => HSYNC02_out,
      D => y(6),
      Q => \y_reg_n_0_[6]\,
      R => '0'
    );
\y_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => HSYNC02_out,
      D => y(7),
      Q => \y_reg_n_0_[7]\,
      R => '0'
    );
\y_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => HSYNC02_out,
      D => y(8),
      Q => \y_reg_n_0_[8]\,
      R => '0'
    );
\y_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => pixel_clk,
      CE => HSYNC02_out,
      D => y(9),
      Q => \y_reg_n_0_[9]\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper is
  port (
    douta : out STD_LOGIC_VECTOR ( 0 to 0 );
    clka : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 0 to 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper is
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram\: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_40 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_41 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_42 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_43 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_44 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_45 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_46 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_47 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_48 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_49 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 1,
      READ_WIDTH_B => 1,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "NONE",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 1,
      WRITE_WIDTH_B => 1
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 0) => addra(14 downto 0),
      ADDRBWRADDR(15 downto 0) => B"0000000000000000",
      CASCADEINA => '0',
      CASCADEINB => '0',
      CASCADEOUTA => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\,
      CASCADEOUTB => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\,
      CLKARDCLK => clka,
      CLKBWRCLK => clka,
      DBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\,
      DIADI(31 downto 1) => B"0000000000000000000000000000000",
      DIADI(0) => dina(0),
      DIBDI(31 downto 0) => B"00000000000000000000000000000000",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"0000",
      DOADO(31 downto 1) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\(31 downto 1),
      DOADO(0) => douta(0),
      DOBDO(31 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\(31 downto 0),
      DOPADOP(3 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\(3 downto 0),
      DOPBDOP(3 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\(3 downto 0),
      ECCPARITY(7 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\(7 downto 0),
      ENARDEN => '1',
      ENBWREN => '0',
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\,
      WEA(3) => wea(0),
      WEA(2) => wea(0),
      WEA(1) => wea(0),
      WEA(0) => wea(0),
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper__parameterized0\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 0 to 0 );
    clka : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 0 to 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper__parameterized0\ : entity is "blk_mem_gen_prim_wrapper";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper__parameterized0\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper__parameterized0\ is
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram\: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_40 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_41 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_42 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_43 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_44 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_45 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_46 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_47 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_48 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_49 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 1,
      READ_WIDTH_B => 1,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "NONE",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 1,
      WRITE_WIDTH_B => 1
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 0) => addra(14 downto 0),
      ADDRBWRADDR(15 downto 0) => B"0000000000000000",
      CASCADEINA => '0',
      CASCADEINB => '0',
      CASCADEOUTA => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\,
      CASCADEOUTB => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\,
      CLKARDCLK => clka,
      CLKBWRCLK => clka,
      DBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\,
      DIADI(31 downto 1) => B"0000000000000000000000000000000",
      DIADI(0) => dina(0),
      DIBDI(31 downto 0) => B"00000000000000000000000000000000",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"0000",
      DOADO(31 downto 1) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\(31 downto 1),
      DOADO(0) => douta(0),
      DOBDO(31 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\(31 downto 0),
      DOPADOP(3 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\(3 downto 0),
      DOPBDOP(3 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\(3 downto 0),
      ECCPARITY(7 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\(7 downto 0),
      ENARDEN => '1',
      ENBWREN => '0',
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\,
      WEA(3) => wea(0),
      WEA(2) => wea(0),
      WEA(1) => wea(0),
      WEA(0) => wea(0),
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper__parameterized1\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 0 to 0 );
    clka : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 0 to 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper__parameterized1\ : entity is "blk_mem_gen_prim_wrapper";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper__parameterized1\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper__parameterized1\ is
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram\: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_40 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_41 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_42 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_43 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_44 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_45 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_46 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_47 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_48 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_49 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 1,
      READ_WIDTH_B => 1,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "NONE",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 1,
      WRITE_WIDTH_B => 1
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 0) => addra(14 downto 0),
      ADDRBWRADDR(15 downto 0) => B"0000000000000000",
      CASCADEINA => '0',
      CASCADEINB => '0',
      CASCADEOUTA => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\,
      CASCADEOUTB => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\,
      CLKARDCLK => clka,
      CLKBWRCLK => clka,
      DBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\,
      DIADI(31 downto 1) => B"0000000000000000000000000000000",
      DIADI(0) => dina(0),
      DIBDI(31 downto 0) => B"00000000000000000000000000000000",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"0000",
      DOADO(31 downto 1) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\(31 downto 1),
      DOADO(0) => douta(0),
      DOBDO(31 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\(31 downto 0),
      DOPADOP(3 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\(3 downto 0),
      DOPBDOP(3 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\(3 downto 0),
      ECCPARITY(7 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\(7 downto 0),
      ENARDEN => '1',
      ENBWREN => '0',
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\,
      WEA(3) => wea(0),
      WEA(2) => wea(0),
      WEA(1) => wea(0),
      WEA(0) => wea(0),
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper__parameterized2\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 0 to 0 );
    clka : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 0 to 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper__parameterized2\ : entity is "blk_mem_gen_prim_wrapper";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper__parameterized2\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper__parameterized2\ is
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram\: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_40 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_41 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_42 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_43 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_44 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_45 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_46 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_47 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_48 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_49 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 1,
      READ_WIDTH_B => 1,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "NONE",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 1,
      WRITE_WIDTH_B => 1
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 0) => addra(14 downto 0),
      ADDRBWRADDR(15 downto 0) => B"0000000000000000",
      CASCADEINA => '0',
      CASCADEINB => '0',
      CASCADEOUTA => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\,
      CASCADEOUTB => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\,
      CLKARDCLK => clka,
      CLKBWRCLK => clka,
      DBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\,
      DIADI(31 downto 1) => B"0000000000000000000000000000000",
      DIADI(0) => dina(0),
      DIBDI(31 downto 0) => B"00000000000000000000000000000000",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"0000",
      DOADO(31 downto 1) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\(31 downto 1),
      DOADO(0) => douta(0),
      DOBDO(31 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\(31 downto 0),
      DOPADOP(3 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\(3 downto 0),
      DOPBDOP(3 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\(3 downto 0),
      ECCPARITY(7 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\(7 downto 0),
      ENARDEN => '1',
      ENBWREN => '0',
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\,
      WEA(3) => wea(0),
      WEA(2) => wea(0),
      WEA(1) => wea(0),
      WEA(0) => wea(0),
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper__parameterized3\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 0 to 0 );
    clka : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 0 to 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper__parameterized3\ : entity is "blk_mem_gen_prim_wrapper";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper__parameterized3\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper__parameterized3\ is
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram\: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_40 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_41 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_42 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_43 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_44 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_45 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_46 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_47 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_48 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_49 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 1,
      READ_WIDTH_B => 1,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "NONE",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 1,
      WRITE_WIDTH_B => 1
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 0) => addra(14 downto 0),
      ADDRBWRADDR(15 downto 0) => B"0000000000000000",
      CASCADEINA => '0',
      CASCADEINB => '0',
      CASCADEOUTA => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\,
      CASCADEOUTB => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\,
      CLKARDCLK => clka,
      CLKBWRCLK => clka,
      DBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\,
      DIADI(31 downto 1) => B"0000000000000000000000000000000",
      DIADI(0) => dina(0),
      DIBDI(31 downto 0) => B"00000000000000000000000000000000",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"0000",
      DOADO(31 downto 1) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\(31 downto 1),
      DOADO(0) => douta(0),
      DOBDO(31 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\(31 downto 0),
      DOPADOP(3 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\(3 downto 0),
      DOPBDOP(3 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\(3 downto 0),
      ECCPARITY(7 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\(7 downto 0),
      ENARDEN => '1',
      ENBWREN => '0',
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\,
      WEA(3) => wea(0),
      WEA(2) => wea(0),
      WEA(1) => wea(0),
      WEA(0) => wea(0),
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper__parameterized4\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 0 to 0 );
    clka : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 0 to 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper__parameterized4\ : entity is "blk_mem_gen_prim_wrapper";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper__parameterized4\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper__parameterized4\ is
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\ : STD_LOGIC;
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\ : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  attribute box_type : string;
  attribute box_type of \DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram\ : label is "PRIMITIVE";
begin
\DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram\: unisim.vcomponents.RAMB36E1
    generic map(
      DOA_REG => 0,
      DOB_REG => 0,
      EN_ECC_READ => false,
      EN_ECC_WRITE => false,
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_40 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_41 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_42 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_43 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_44 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_45 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_46 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_47 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_48 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_49 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_4F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_50 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_51 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_52 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_53 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_54 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_55 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_56 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_57 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_58 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_59 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_5F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_60 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_61 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_62 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_63 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_64 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_65 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_66 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_67 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_68 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_69 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_6F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_70 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_71 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_72 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_73 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_74 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_75 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_76 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_77 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_78 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_79 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_7F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      INIT_FILE => "NONE",
      IS_CLKARDCLK_INVERTED => '0',
      IS_CLKBWRCLK_INVERTED => '0',
      IS_ENARDEN_INVERTED => '0',
      IS_ENBWREN_INVERTED => '0',
      IS_RSTRAMARSTRAM_INVERTED => '0',
      IS_RSTRAMB_INVERTED => '0',
      IS_RSTREGARSTREG_INVERTED => '0',
      IS_RSTREGB_INVERTED => '0',
      RAM_EXTENSION_A => "NONE",
      RAM_EXTENSION_B => "NONE",
      RAM_MODE => "TDP",
      RDADDR_COLLISION_HWCONFIG => "PERFORMANCE",
      READ_WIDTH_A => 1,
      READ_WIDTH_B => 1,
      RSTREG_PRIORITY_A => "REGCE",
      RSTREG_PRIORITY_B => "REGCE",
      SIM_COLLISION_CHECK => "NONE",
      SIM_DEVICE => "7SERIES",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      WRITE_WIDTH_A => 1,
      WRITE_WIDTH_B => 1
    )
        port map (
      ADDRARDADDR(15) => '1',
      ADDRARDADDR(14 downto 0) => addra(14 downto 0),
      ADDRBWRADDR(15 downto 0) => B"0000000000000000",
      CASCADEINA => '0',
      CASCADEINB => '0',
      CASCADEOUTA => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTA_UNCONNECTED\,
      CASCADEOUTB => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_CASCADEOUTB_UNCONNECTED\,
      CLKARDCLK => clka,
      CLKBWRCLK => clka,
      DBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DBITERR_UNCONNECTED\,
      DIADI(31 downto 1) => B"0000000000000000000000000000000",
      DIADI(0) => dina(0),
      DIBDI(31 downto 0) => B"00000000000000000000000000000000",
      DIPADIP(3 downto 0) => B"0000",
      DIPBDIP(3 downto 0) => B"0000",
      DOADO(31 downto 1) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOADO_UNCONNECTED\(31 downto 1),
      DOADO(0) => douta(0),
      DOBDO(31 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOBDO_UNCONNECTED\(31 downto 0),
      DOPADOP(3 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPADOP_UNCONNECTED\(3 downto 0),
      DOPBDOP(3 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_DOPBDOP_UNCONNECTED\(3 downto 0),
      ECCPARITY(7 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_ECCPARITY_UNCONNECTED\(7 downto 0),
      ENARDEN => '1',
      ENBWREN => '0',
      INJECTDBITERR => '0',
      INJECTSBITERR => '0',
      RDADDRECC(8 downto 0) => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_RDADDRECC_UNCONNECTED\(8 downto 0),
      REGCEAREGCE => '0',
      REGCEB => '0',
      RSTRAMARSTRAM => '0',
      RSTRAMB => '0',
      RSTREGARSTREG => '0',
      RSTREGB => '0',
      SBITERR => \NLW_DEVICE_7SERIES.NO_BMM_INFO.SP.SIMPLE_PRIM36.ram_SBITERR_UNCONNECTED\,
      WEA(3) => wea(0),
      WEA(2) => wea(0),
      WEA(1) => wea(0),
      WEA(0) => wea(0),
      WEBWE(7 downto 0) => B"00000000"
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width is
  port (
    douta : out STD_LOGIC_VECTOR ( 0 to 0 );
    clka : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 0 to 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width is
begin
\prim_noinit.ram\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper
     port map (
      addra(14 downto 0) => addra(14 downto 0),
      clka => clka,
      dina(0) => dina(0),
      douta(0) => douta(0),
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized0\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 0 to 0 );
    clka : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 0 to 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized0\ : entity is "blk_mem_gen_prim_width";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized0\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized0\ is
begin
\prim_noinit.ram\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper__parameterized0\
     port map (
      addra(14 downto 0) => addra(14 downto 0),
      clka => clka,
      dina(0) => dina(0),
      douta(0) => douta(0),
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized1\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 0 to 0 );
    clka : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 0 to 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized1\ : entity is "blk_mem_gen_prim_width";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized1\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized1\ is
begin
\prim_noinit.ram\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper__parameterized1\
     port map (
      addra(14 downto 0) => addra(14 downto 0),
      clka => clka,
      dina(0) => dina(0),
      douta(0) => douta(0),
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized2\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 0 to 0 );
    clka : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 0 to 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized2\ : entity is "blk_mem_gen_prim_width";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized2\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized2\ is
begin
\prim_noinit.ram\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper__parameterized2\
     port map (
      addra(14 downto 0) => addra(14 downto 0),
      clka => clka,
      dina(0) => dina(0),
      douta(0) => douta(0),
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized3\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 0 to 0 );
    clka : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 0 to 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized3\ : entity is "blk_mem_gen_prim_width";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized3\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized3\ is
begin
\prim_noinit.ram\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper__parameterized3\
     port map (
      addra(14 downto 0) => addra(14 downto 0),
      clka => clka,
      dina(0) => dina(0),
      douta(0) => douta(0),
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized4\ is
  port (
    douta : out STD_LOGIC_VECTOR ( 0 to 0 );
    clka : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 0 to 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized4\ : entity is "blk_mem_gen_prim_width";
end \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized4\;

architecture STRUCTURE of \decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized4\ is
begin
\prim_noinit.ram\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_wrapper__parameterized4\
     port map (
      addra(14 downto 0) => addra(14 downto 0),
      clka => clka,
      dina(0) => dina(0),
      douta(0) => douta(0),
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr is
  port (
    douta : out STD_LOGIC_VECTOR ( 5 downto 0 );
    clka : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 5 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr is
begin
\ramloop[0].ram.r\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width
     port map (
      addra(14 downto 0) => addra(14 downto 0),
      clka => clka,
      dina(0) => dina(0),
      douta(0) => douta(0),
      wea(0) => wea(0)
    );
\ramloop[1].ram.r\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized0\
     port map (
      addra(14 downto 0) => addra(14 downto 0),
      clka => clka,
      dina(0) => dina(1),
      douta(0) => douta(1),
      wea(0) => wea(0)
    );
\ramloop[2].ram.r\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized1\
     port map (
      addra(14 downto 0) => addra(14 downto 0),
      clka => clka,
      dina(0) => dina(2),
      douta(0) => douta(2),
      wea(0) => wea(0)
    );
\ramloop[3].ram.r\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized2\
     port map (
      addra(14 downto 0) => addra(14 downto 0),
      clka => clka,
      dina(0) => dina(3),
      douta(0) => douta(3),
      wea(0) => wea(0)
    );
\ramloop[4].ram.r\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized3\
     port map (
      addra(14 downto 0) => addra(14 downto 0),
      clka => clka,
      dina(0) => dina(4),
      douta(0) => douta(4),
      wea(0) => wea(0)
    );
\ramloop[5].ram.r\: entity work.\decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_prim_width__parameterized4\
     port map (
      addra(14 downto 0) => addra(14 downto 0),
      clka => clka,
      dina(0) => dina(5),
      douta(0) => douta(5),
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top is
  port (
    douta : out STD_LOGIC_VECTOR ( 5 downto 0 );
    clka : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 5 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top is
begin
\valid.cstr\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_generic_cstr
     port map (
      addra(14 downto 0) => addra(14 downto 0),
      clka => clka,
      dina(5 downto 0) => dina(5 downto 0),
      douta(5 downto 0) => douta(5 downto 0),
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6_synth is
  port (
    douta : out STD_LOGIC_VECTOR ( 5 downto 0 );
    clka : in STD_LOGIC;
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 5 downto 0 );
    wea : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6_synth;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6_synth is
begin
\gnbram.gnativebmg.native_blk_mem_gen\: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_top
     port map (
      addra(14 downto 0) => addra(14 downto 0),
      clka => clka,
      dina(5 downto 0) => dina(5 downto 0),
      douta(5 downto 0) => douta(5 downto 0),
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 is
  port (
    clka : in STD_LOGIC;
    rsta : in STD_LOGIC;
    ena : in STD_LOGIC;
    regcea : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 5 downto 0 );
    douta : out STD_LOGIC_VECTOR ( 5 downto 0 );
    clkb : in STD_LOGIC;
    rstb : in STD_LOGIC;
    enb : in STD_LOGIC;
    regceb : in STD_LOGIC;
    web : in STD_LOGIC_VECTOR ( 0 to 0 );
    addrb : in STD_LOGIC_VECTOR ( 14 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 5 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 5 downto 0 );
    injectsbiterr : in STD_LOGIC;
    injectdbiterr : in STD_LOGIC;
    eccpipece : in STD_LOGIC;
    sbiterr : out STD_LOGIC;
    dbiterr : out STD_LOGIC;
    rdaddrecc : out STD_LOGIC_VECTOR ( 14 downto 0 );
    sleep : in STD_LOGIC;
    deepsleep : in STD_LOGIC;
    shutdown : in STD_LOGIC;
    rsta_busy : out STD_LOGIC;
    rstb_busy : out STD_LOGIC;
    s_aclk : in STD_LOGIC;
    s_aresetn : in STD_LOGIC;
    s_axi_awid : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 5 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_wlast : in STD_LOGIC;
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_bid : out STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_arid : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axi_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rid : out STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_rdata : out STD_LOGIC_VECTOR ( 5 downto 0 );
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rlast : out STD_LOGIC;
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    s_axi_injectsbiterr : in STD_LOGIC;
    s_axi_injectdbiterr : in STD_LOGIC;
    s_axi_sbiterr : out STD_LOGIC;
    s_axi_dbiterr : out STD_LOGIC;
    s_axi_rdaddrecc : out STD_LOGIC_VECTOR ( 14 downto 0 )
  );
  attribute C_ADDRA_WIDTH : integer;
  attribute C_ADDRA_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 15;
  attribute C_ADDRB_WIDTH : integer;
  attribute C_ADDRB_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 15;
  attribute C_ALGORITHM : integer;
  attribute C_ALGORITHM of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 1;
  attribute C_AXI_ID_WIDTH : integer;
  attribute C_AXI_ID_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 4;
  attribute C_AXI_SLAVE_TYPE : integer;
  attribute C_AXI_SLAVE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_AXI_TYPE : integer;
  attribute C_AXI_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 1;
  attribute C_BYTE_SIZE : integer;
  attribute C_BYTE_SIZE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 9;
  attribute C_COMMON_CLK : integer;
  attribute C_COMMON_CLK of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_COUNT_18K_BRAM : string;
  attribute C_COUNT_18K_BRAM of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is "0";
  attribute C_COUNT_36K_BRAM : string;
  attribute C_COUNT_36K_BRAM of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is "6";
  attribute C_CTRL_ECC_ALGO : string;
  attribute C_CTRL_ECC_ALGO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is "NONE";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is "0";
  attribute C_DISABLE_WARN_BHV_COLL : integer;
  attribute C_DISABLE_WARN_BHV_COLL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 1;
  attribute C_DISABLE_WARN_BHV_RANGE : integer;
  attribute C_DISABLE_WARN_BHV_RANGE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is "./";
  attribute C_ENABLE_32BIT_ADDRESS : integer;
  attribute C_ENABLE_32BIT_ADDRESS of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_EN_DEEPSLEEP_PIN : integer;
  attribute C_EN_DEEPSLEEP_PIN of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_EN_ECC_PIPE : integer;
  attribute C_EN_ECC_PIPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_EN_RDADDRA_CHG : integer;
  attribute C_EN_RDADDRA_CHG of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_EN_RDADDRB_CHG : integer;
  attribute C_EN_RDADDRB_CHG of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_EN_SAFETY_CKT : integer;
  attribute C_EN_SAFETY_CKT of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_EN_SHUTDOWN_PIN : integer;
  attribute C_EN_SHUTDOWN_PIN of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_EN_SLEEP_PIN : integer;
  attribute C_EN_SLEEP_PIN of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_EST_POWER_SUMMARY : string;
  attribute C_EST_POWER_SUMMARY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is "Estimated Power for IP     :     14.315701 mW";
  attribute C_FAMILY : string;
  attribute C_FAMILY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is "zynq";
  attribute C_HAS_AXI_ID : integer;
  attribute C_HAS_AXI_ID of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_HAS_ENA : integer;
  attribute C_HAS_ENA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_HAS_ENB : integer;
  attribute C_HAS_ENB of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_HAS_INJECTERR : integer;
  attribute C_HAS_INJECTERR of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_A : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_B : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_A : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_B : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_HAS_REGCEA : integer;
  attribute C_HAS_REGCEA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_HAS_REGCEB : integer;
  attribute C_HAS_REGCEB of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_HAS_RSTA : integer;
  attribute C_HAS_RSTA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_HAS_RSTB : integer;
  attribute C_HAS_RSTB of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_HAS_SOFTECC_INPUT_REGS_A : integer;
  attribute C_HAS_SOFTECC_INPUT_REGS_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B : integer;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_INITA_VAL : string;
  attribute C_INITA_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is "0";
  attribute C_INITB_VAL : string;
  attribute C_INITB_VAL of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is "0";
  attribute C_INIT_FILE : string;
  attribute C_INIT_FILE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is "blk_mem_gen_0.mem";
  attribute C_INIT_FILE_NAME : string;
  attribute C_INIT_FILE_NAME of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is "no_coe_file_loaded";
  attribute C_INTERFACE_TYPE : integer;
  attribute C_INTERFACE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_LOAD_INIT_FILE : integer;
  attribute C_LOAD_INIT_FILE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_MEM_TYPE : integer;
  attribute C_MEM_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_MUX_PIPELINE_STAGES : integer;
  attribute C_MUX_PIPELINE_STAGES of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_PRIM_TYPE : integer;
  attribute C_PRIM_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 1;
  attribute C_READ_DEPTH_A : integer;
  attribute C_READ_DEPTH_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 32768;
  attribute C_READ_DEPTH_B : integer;
  attribute C_READ_DEPTH_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 32768;
  attribute C_READ_WIDTH_A : integer;
  attribute C_READ_WIDTH_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 6;
  attribute C_READ_WIDTH_B : integer;
  attribute C_READ_WIDTH_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 6;
  attribute C_RSTRAM_A : integer;
  attribute C_RSTRAM_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_RSTRAM_B : integer;
  attribute C_RSTRAM_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_RST_PRIORITY_A : string;
  attribute C_RST_PRIORITY_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is "CE";
  attribute C_RST_PRIORITY_B : string;
  attribute C_RST_PRIORITY_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is "CE";
  attribute C_SIM_COLLISION_CHECK : string;
  attribute C_SIM_COLLISION_CHECK of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is "NONE";
  attribute C_USE_BRAM_BLOCK : integer;
  attribute C_USE_BRAM_BLOCK of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_USE_BYTE_WEA : integer;
  attribute C_USE_BYTE_WEA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_USE_BYTE_WEB : integer;
  attribute C_USE_BYTE_WEB of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_USE_DEFAULT_DATA : integer;
  attribute C_USE_DEFAULT_DATA of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_USE_ECC : integer;
  attribute C_USE_ECC of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_USE_SOFTECC : integer;
  attribute C_USE_SOFTECC of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_USE_URAM : integer;
  attribute C_USE_URAM of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 0;
  attribute C_WEA_WIDTH : integer;
  attribute C_WEA_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 1;
  attribute C_WEB_WIDTH : integer;
  attribute C_WEB_WIDTH of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 1;
  attribute C_WRITE_DEPTH_A : integer;
  attribute C_WRITE_DEPTH_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 32768;
  attribute C_WRITE_DEPTH_B : integer;
  attribute C_WRITE_DEPTH_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 32768;
  attribute C_WRITE_MODE_A : string;
  attribute C_WRITE_MODE_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is "WRITE_FIRST";
  attribute C_WRITE_MODE_B : string;
  attribute C_WRITE_MODE_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is "WRITE_FIRST";
  attribute C_WRITE_WIDTH_A : integer;
  attribute C_WRITE_WIDTH_A of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 6;
  attribute C_WRITE_WIDTH_B : integer;
  attribute C_WRITE_WIDTH_B of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is 6;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is "zynq";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 : entity is "yes";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6 is
  signal \<const0>\ : STD_LOGIC;
begin
  dbiterr <= \<const0>\;
  doutb(5) <= \<const0>\;
  doutb(4) <= \<const0>\;
  doutb(3) <= \<const0>\;
  doutb(2) <= \<const0>\;
  doutb(1) <= \<const0>\;
  doutb(0) <= \<const0>\;
  rdaddrecc(14) <= \<const0>\;
  rdaddrecc(13) <= \<const0>\;
  rdaddrecc(12) <= \<const0>\;
  rdaddrecc(11) <= \<const0>\;
  rdaddrecc(10) <= \<const0>\;
  rdaddrecc(9) <= \<const0>\;
  rdaddrecc(8) <= \<const0>\;
  rdaddrecc(7) <= \<const0>\;
  rdaddrecc(6) <= \<const0>\;
  rdaddrecc(5) <= \<const0>\;
  rdaddrecc(4) <= \<const0>\;
  rdaddrecc(3) <= \<const0>\;
  rdaddrecc(2) <= \<const0>\;
  rdaddrecc(1) <= \<const0>\;
  rdaddrecc(0) <= \<const0>\;
  rsta_busy <= \<const0>\;
  rstb_busy <= \<const0>\;
  s_axi_arready <= \<const0>\;
  s_axi_awready <= \<const0>\;
  s_axi_bid(3) <= \<const0>\;
  s_axi_bid(2) <= \<const0>\;
  s_axi_bid(1) <= \<const0>\;
  s_axi_bid(0) <= \<const0>\;
  s_axi_bresp(1) <= \<const0>\;
  s_axi_bresp(0) <= \<const0>\;
  s_axi_bvalid <= \<const0>\;
  s_axi_dbiterr <= \<const0>\;
  s_axi_rdaddrecc(14) <= \<const0>\;
  s_axi_rdaddrecc(13) <= \<const0>\;
  s_axi_rdaddrecc(12) <= \<const0>\;
  s_axi_rdaddrecc(11) <= \<const0>\;
  s_axi_rdaddrecc(10) <= \<const0>\;
  s_axi_rdaddrecc(9) <= \<const0>\;
  s_axi_rdaddrecc(8) <= \<const0>\;
  s_axi_rdaddrecc(7) <= \<const0>\;
  s_axi_rdaddrecc(6) <= \<const0>\;
  s_axi_rdaddrecc(5) <= \<const0>\;
  s_axi_rdaddrecc(4) <= \<const0>\;
  s_axi_rdaddrecc(3) <= \<const0>\;
  s_axi_rdaddrecc(2) <= \<const0>\;
  s_axi_rdaddrecc(1) <= \<const0>\;
  s_axi_rdaddrecc(0) <= \<const0>\;
  s_axi_rdata(5) <= \<const0>\;
  s_axi_rdata(4) <= \<const0>\;
  s_axi_rdata(3) <= \<const0>\;
  s_axi_rdata(2) <= \<const0>\;
  s_axi_rdata(1) <= \<const0>\;
  s_axi_rdata(0) <= \<const0>\;
  s_axi_rid(3) <= \<const0>\;
  s_axi_rid(2) <= \<const0>\;
  s_axi_rid(1) <= \<const0>\;
  s_axi_rid(0) <= \<const0>\;
  s_axi_rlast <= \<const0>\;
  s_axi_rresp(1) <= \<const0>\;
  s_axi_rresp(0) <= \<const0>\;
  s_axi_rvalid <= \<const0>\;
  s_axi_sbiterr <= \<const0>\;
  s_axi_wready <= \<const0>\;
  sbiterr <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
inst_blk_mem_gen: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6_synth
     port map (
      addra(14 downto 0) => addra(14 downto 0),
      clka => clka,
      dina(5 downto 0) => dina(5 downto 0),
      douta(5 downto 0) => douta(5 downto 0),
      wea(0) => wea(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_0 is
  port (
    clka : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    addra : in STD_LOGIC_VECTOR ( 14 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 5 downto 0 );
    douta : out STD_LOGIC_VECTOR ( 5 downto 0 )
  );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_0 : entity is "blk_mem_gen_0,blk_mem_gen_v8_3_6,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_0 : entity is "blk_mem_gen_v8_3_6,Vivado 2017.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_0 is
  signal NLW_U0_dbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_rsta_busy_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_rstb_busy_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_arready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_awready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_bvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_dbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_rlast_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_rvalid_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_sbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_s_axi_wready_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_sbiterr_UNCONNECTED : STD_LOGIC;
  signal NLW_U0_doutb_UNCONNECTED : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal NLW_U0_rdaddrecc_UNCONNECTED : STD_LOGIC_VECTOR ( 14 downto 0 );
  signal NLW_U0_s_axi_bid_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_U0_s_axi_bresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal NLW_U0_s_axi_rdaddrecc_UNCONNECTED : STD_LOGIC_VECTOR ( 14 downto 0 );
  signal NLW_U0_s_axi_rdata_UNCONNECTED : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal NLW_U0_s_axi_rid_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_U0_s_axi_rresp_UNCONNECTED : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute C_ADDRA_WIDTH : integer;
  attribute C_ADDRA_WIDTH of U0 : label is 15;
  attribute C_ADDRB_WIDTH : integer;
  attribute C_ADDRB_WIDTH of U0 : label is 15;
  attribute C_ALGORITHM : integer;
  attribute C_ALGORITHM of U0 : label is 1;
  attribute C_AXI_ID_WIDTH : integer;
  attribute C_AXI_ID_WIDTH of U0 : label is 4;
  attribute C_AXI_SLAVE_TYPE : integer;
  attribute C_AXI_SLAVE_TYPE of U0 : label is 0;
  attribute C_AXI_TYPE : integer;
  attribute C_AXI_TYPE of U0 : label is 1;
  attribute C_BYTE_SIZE : integer;
  attribute C_BYTE_SIZE of U0 : label is 9;
  attribute C_COMMON_CLK : integer;
  attribute C_COMMON_CLK of U0 : label is 0;
  attribute C_COUNT_18K_BRAM : string;
  attribute C_COUNT_18K_BRAM of U0 : label is "0";
  attribute C_COUNT_36K_BRAM : string;
  attribute C_COUNT_36K_BRAM of U0 : label is "6";
  attribute C_CTRL_ECC_ALGO : string;
  attribute C_CTRL_ECC_ALGO of U0 : label is "NONE";
  attribute C_DEFAULT_DATA : string;
  attribute C_DEFAULT_DATA of U0 : label is "0";
  attribute C_DISABLE_WARN_BHV_COLL : integer;
  attribute C_DISABLE_WARN_BHV_COLL of U0 : label is 1;
  attribute C_DISABLE_WARN_BHV_RANGE : integer;
  attribute C_DISABLE_WARN_BHV_RANGE of U0 : label is 1;
  attribute C_ELABORATION_DIR : string;
  attribute C_ELABORATION_DIR of U0 : label is "./";
  attribute C_ENABLE_32BIT_ADDRESS : integer;
  attribute C_ENABLE_32BIT_ADDRESS of U0 : label is 0;
  attribute C_EN_DEEPSLEEP_PIN : integer;
  attribute C_EN_DEEPSLEEP_PIN of U0 : label is 0;
  attribute C_EN_ECC_PIPE : integer;
  attribute C_EN_ECC_PIPE of U0 : label is 0;
  attribute C_EN_RDADDRA_CHG : integer;
  attribute C_EN_RDADDRA_CHG of U0 : label is 0;
  attribute C_EN_RDADDRB_CHG : integer;
  attribute C_EN_RDADDRB_CHG of U0 : label is 0;
  attribute C_EN_SAFETY_CKT : integer;
  attribute C_EN_SAFETY_CKT of U0 : label is 0;
  attribute C_EN_SHUTDOWN_PIN : integer;
  attribute C_EN_SHUTDOWN_PIN of U0 : label is 0;
  attribute C_EN_SLEEP_PIN : integer;
  attribute C_EN_SLEEP_PIN of U0 : label is 0;
  attribute C_EST_POWER_SUMMARY : string;
  attribute C_EST_POWER_SUMMARY of U0 : label is "Estimated Power for IP     :     14.315701 mW";
  attribute C_FAMILY : string;
  attribute C_FAMILY of U0 : label is "zynq";
  attribute C_HAS_AXI_ID : integer;
  attribute C_HAS_AXI_ID of U0 : label is 0;
  attribute C_HAS_ENA : integer;
  attribute C_HAS_ENA of U0 : label is 0;
  attribute C_HAS_ENB : integer;
  attribute C_HAS_ENB of U0 : label is 0;
  attribute C_HAS_INJECTERR : integer;
  attribute C_HAS_INJECTERR of U0 : label is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_A : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_MEM_OUTPUT_REGS_B : integer;
  attribute C_HAS_MEM_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_A : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_MUX_OUTPUT_REGS_B : integer;
  attribute C_HAS_MUX_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_HAS_REGCEA : integer;
  attribute C_HAS_REGCEA of U0 : label is 0;
  attribute C_HAS_REGCEB : integer;
  attribute C_HAS_REGCEB of U0 : label is 0;
  attribute C_HAS_RSTA : integer;
  attribute C_HAS_RSTA of U0 : label is 0;
  attribute C_HAS_RSTB : integer;
  attribute C_HAS_RSTB of U0 : label is 0;
  attribute C_HAS_SOFTECC_INPUT_REGS_A : integer;
  attribute C_HAS_SOFTECC_INPUT_REGS_A of U0 : label is 0;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B : integer;
  attribute C_HAS_SOFTECC_OUTPUT_REGS_B of U0 : label is 0;
  attribute C_INITA_VAL : string;
  attribute C_INITA_VAL of U0 : label is "0";
  attribute C_INITB_VAL : string;
  attribute C_INITB_VAL of U0 : label is "0";
  attribute C_INIT_FILE : string;
  attribute C_INIT_FILE of U0 : label is "blk_mem_gen_0.mem";
  attribute C_INIT_FILE_NAME : string;
  attribute C_INIT_FILE_NAME of U0 : label is "no_coe_file_loaded";
  attribute C_INTERFACE_TYPE : integer;
  attribute C_INTERFACE_TYPE of U0 : label is 0;
  attribute C_LOAD_INIT_FILE : integer;
  attribute C_LOAD_INIT_FILE of U0 : label is 0;
  attribute C_MEM_TYPE : integer;
  attribute C_MEM_TYPE of U0 : label is 0;
  attribute C_MUX_PIPELINE_STAGES : integer;
  attribute C_MUX_PIPELINE_STAGES of U0 : label is 0;
  attribute C_PRIM_TYPE : integer;
  attribute C_PRIM_TYPE of U0 : label is 1;
  attribute C_READ_DEPTH_A : integer;
  attribute C_READ_DEPTH_A of U0 : label is 32768;
  attribute C_READ_DEPTH_B : integer;
  attribute C_READ_DEPTH_B of U0 : label is 32768;
  attribute C_READ_WIDTH_A : integer;
  attribute C_READ_WIDTH_A of U0 : label is 6;
  attribute C_READ_WIDTH_B : integer;
  attribute C_READ_WIDTH_B of U0 : label is 6;
  attribute C_RSTRAM_A : integer;
  attribute C_RSTRAM_A of U0 : label is 0;
  attribute C_RSTRAM_B : integer;
  attribute C_RSTRAM_B of U0 : label is 0;
  attribute C_RST_PRIORITY_A : string;
  attribute C_RST_PRIORITY_A of U0 : label is "CE";
  attribute C_RST_PRIORITY_B : string;
  attribute C_RST_PRIORITY_B of U0 : label is "CE";
  attribute C_SIM_COLLISION_CHECK : string;
  attribute C_SIM_COLLISION_CHECK of U0 : label is "NONE";
  attribute C_USE_BRAM_BLOCK : integer;
  attribute C_USE_BRAM_BLOCK of U0 : label is 0;
  attribute C_USE_BYTE_WEA : integer;
  attribute C_USE_BYTE_WEA of U0 : label is 0;
  attribute C_USE_BYTE_WEB : integer;
  attribute C_USE_BYTE_WEB of U0 : label is 0;
  attribute C_USE_DEFAULT_DATA : integer;
  attribute C_USE_DEFAULT_DATA of U0 : label is 0;
  attribute C_USE_ECC : integer;
  attribute C_USE_ECC of U0 : label is 0;
  attribute C_USE_SOFTECC : integer;
  attribute C_USE_SOFTECC of U0 : label is 0;
  attribute C_USE_URAM : integer;
  attribute C_USE_URAM of U0 : label is 0;
  attribute C_WEA_WIDTH : integer;
  attribute C_WEA_WIDTH of U0 : label is 1;
  attribute C_WEB_WIDTH : integer;
  attribute C_WEB_WIDTH of U0 : label is 1;
  attribute C_WRITE_DEPTH_A : integer;
  attribute C_WRITE_DEPTH_A of U0 : label is 32768;
  attribute C_WRITE_DEPTH_B : integer;
  attribute C_WRITE_DEPTH_B of U0 : label is 32768;
  attribute C_WRITE_MODE_A : string;
  attribute C_WRITE_MODE_A of U0 : label is "WRITE_FIRST";
  attribute C_WRITE_MODE_B : string;
  attribute C_WRITE_MODE_B of U0 : label is "WRITE_FIRST";
  attribute C_WRITE_WIDTH_A : integer;
  attribute C_WRITE_WIDTH_A of U0 : label is 6;
  attribute C_WRITE_WIDTH_B : integer;
  attribute C_WRITE_WIDTH_B of U0 : label is 6;
  attribute C_XDEVICEFAMILY : string;
  attribute C_XDEVICEFAMILY of U0 : label is "zynq";
  attribute downgradeipidentifiedwarnings of U0 : label is "yes";
begin
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_v8_3_6
     port map (
      addra(14 downto 0) => addra(14 downto 0),
      addrb(14 downto 0) => B"000000000000000",
      clka => clka,
      clkb => '0',
      dbiterr => NLW_U0_dbiterr_UNCONNECTED,
      deepsleep => '0',
      dina(5 downto 0) => dina(5 downto 0),
      dinb(5 downto 0) => B"000000",
      douta(5 downto 0) => douta(5 downto 0),
      doutb(5 downto 0) => NLW_U0_doutb_UNCONNECTED(5 downto 0),
      eccpipece => '0',
      ena => '0',
      enb => '0',
      injectdbiterr => '0',
      injectsbiterr => '0',
      rdaddrecc(14 downto 0) => NLW_U0_rdaddrecc_UNCONNECTED(14 downto 0),
      regcea => '0',
      regceb => '0',
      rsta => '0',
      rsta_busy => NLW_U0_rsta_busy_UNCONNECTED,
      rstb => '0',
      rstb_busy => NLW_U0_rstb_busy_UNCONNECTED,
      s_aclk => '0',
      s_aresetn => '0',
      s_axi_araddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_arburst(1 downto 0) => B"00",
      s_axi_arid(3 downto 0) => B"0000",
      s_axi_arlen(7 downto 0) => B"00000000",
      s_axi_arready => NLW_U0_s_axi_arready_UNCONNECTED,
      s_axi_arsize(2 downto 0) => B"000",
      s_axi_arvalid => '0',
      s_axi_awaddr(31 downto 0) => B"00000000000000000000000000000000",
      s_axi_awburst(1 downto 0) => B"00",
      s_axi_awid(3 downto 0) => B"0000",
      s_axi_awlen(7 downto 0) => B"00000000",
      s_axi_awready => NLW_U0_s_axi_awready_UNCONNECTED,
      s_axi_awsize(2 downto 0) => B"000",
      s_axi_awvalid => '0',
      s_axi_bid(3 downto 0) => NLW_U0_s_axi_bid_UNCONNECTED(3 downto 0),
      s_axi_bready => '0',
      s_axi_bresp(1 downto 0) => NLW_U0_s_axi_bresp_UNCONNECTED(1 downto 0),
      s_axi_bvalid => NLW_U0_s_axi_bvalid_UNCONNECTED,
      s_axi_dbiterr => NLW_U0_s_axi_dbiterr_UNCONNECTED,
      s_axi_injectdbiterr => '0',
      s_axi_injectsbiterr => '0',
      s_axi_rdaddrecc(14 downto 0) => NLW_U0_s_axi_rdaddrecc_UNCONNECTED(14 downto 0),
      s_axi_rdata(5 downto 0) => NLW_U0_s_axi_rdata_UNCONNECTED(5 downto 0),
      s_axi_rid(3 downto 0) => NLW_U0_s_axi_rid_UNCONNECTED(3 downto 0),
      s_axi_rlast => NLW_U0_s_axi_rlast_UNCONNECTED,
      s_axi_rready => '0',
      s_axi_rresp(1 downto 0) => NLW_U0_s_axi_rresp_UNCONNECTED(1 downto 0),
      s_axi_rvalid => NLW_U0_s_axi_rvalid_UNCONNECTED,
      s_axi_sbiterr => NLW_U0_s_axi_sbiterr_UNCONNECTED,
      s_axi_wdata(5 downto 0) => B"000000",
      s_axi_wlast => '0',
      s_axi_wready => NLW_U0_s_axi_wready_UNCONNECTED,
      s_axi_wstrb(0) => '0',
      s_axi_wvalid => '0',
      sbiterr => NLW_U0_sbiterr_UNCONNECTED,
      shutdown => '0',
      sleep => '0',
      wea(0) => wea(0),
      web(0) => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_renderer is
  port (
    \addra_reg[14]_P_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    Q : out STD_LOGIC_VECTOR ( 9 downto 0 );
    \addra_reg[14]_P_1\ : out STD_LOGIC_VECTOR ( 14 downto 0 );
    clk : in STD_LOGIC;
    fetching_sprites : in STD_LOGIC;
    dina : in STD_LOGIC_VECTOR ( 5 downto 0 );
    addra0 : in STD_LOGIC;
    clk_0 : in STD_LOGIC;
    render_enable_reg : in STD_LOGIC;
    render_enable_reg_0 : in STD_LOGIC;
    render_enable_reg_1 : in STD_LOGIC;
    render_enable_reg_2 : in STD_LOGIC;
    render_enable_reg_3 : in STD_LOGIC;
    render_enable_reg_4 : in STD_LOGIC;
    render_enable_reg_5 : in STD_LOGIC;
    render_enable_reg_6 : in STD_LOGIC;
    render_enable_reg_7 : in STD_LOGIC;
    render_enable_reg_8 : in STD_LOGIC;
    render_enable_reg_9 : in STD_LOGIC;
    render_enable_reg_10 : in STD_LOGIC;
    render_enable_reg_11 : in STD_LOGIC;
    render_enable_reg_12 : in STD_LOGIC;
    render_enable_reg_13 : in STD_LOGIC;
    render_enable_reg_14 : in STD_LOGIC;
    render_enable_reg_15 : in STD_LOGIC;
    render_enable_reg_16 : in STD_LOGIC;
    render_enable_reg_17 : in STD_LOGIC;
    render_enable_reg_18 : in STD_LOGIC;
    render_enable_reg_19 : in STD_LOGIC;
    render_enable_reg_20 : in STD_LOGIC;
    render_enable_reg_21 : in STD_LOGIC;
    render_enable_reg_22 : in STD_LOGIC;
    render_enable_reg_23 : in STD_LOGIC;
    render_enable_reg_24 : in STD_LOGIC;
    render_enable_reg_25 : in STD_LOGIC;
    render_enable_reg_26 : in STD_LOGIC;
    render_enable_reg_27 : in STD_LOGIC;
    render_enable_reg_28 : in STD_LOGIC;
    render_enable : in STD_LOGIC;
    pixel_clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_renderer;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_renderer is
  signal addra : STD_LOGIC_VECTOR ( 14 downto 0 );
  signal \addra[0]_C_i_1_n_0\ : STD_LOGIC;
  signal \addra[10]_C_i_1_n_0\ : STD_LOGIC;
  signal \addra[11]_C_i_1_n_0\ : STD_LOGIC;
  signal \addra[12]_C_i_1_n_0\ : STD_LOGIC;
  signal \addra[13]_C_i_1_n_0\ : STD_LOGIC;
  signal \addra[14]_C_i_1_n_0\ : STD_LOGIC;
  signal \addra[1]_C_i_1_n_0\ : STD_LOGIC;
  signal \addra[2]_C_i_1_n_0\ : STD_LOGIC;
  signal \addra[3]_C_i_1_n_0\ : STD_LOGIC;
  signal \addra[4]_C_i_1_n_0\ : STD_LOGIC;
  signal \addra[5]_C_i_1_n_0\ : STD_LOGIC;
  signal \addra[6]_C_i_1_n_0\ : STD_LOGIC;
  signal \addra[7]_C_i_1_n_0\ : STD_LOGIC;
  signal \addra[8]_C_i_1_n_0\ : STD_LOGIC;
  signal \addra[9]_C_i_1_n_0\ : STD_LOGIC;
  signal \addra_reg[0]_C_n_0\ : STD_LOGIC;
  signal \addra_reg[0]_LDC_n_0\ : STD_LOGIC;
  signal \addra_reg[0]_P_n_0\ : STD_LOGIC;
  signal \addra_reg[10]_C_n_0\ : STD_LOGIC;
  signal \addra_reg[10]_LDC_n_0\ : STD_LOGIC;
  signal \addra_reg[10]_P_n_0\ : STD_LOGIC;
  signal \addra_reg[11]_C_n_0\ : STD_LOGIC;
  signal \addra_reg[11]_LDC_n_0\ : STD_LOGIC;
  signal \addra_reg[11]_P_n_0\ : STD_LOGIC;
  signal \addra_reg[12]_C_n_0\ : STD_LOGIC;
  signal \addra_reg[12]_LDC_n_0\ : STD_LOGIC;
  signal \addra_reg[12]_P_n_0\ : STD_LOGIC;
  signal \addra_reg[13]_C_n_0\ : STD_LOGIC;
  signal \addra_reg[13]_LDC_n_0\ : STD_LOGIC;
  signal \addra_reg[13]_P_n_0\ : STD_LOGIC;
  signal \addra_reg[14]_C_n_0\ : STD_LOGIC;
  signal \addra_reg[14]_LDC_n_0\ : STD_LOGIC;
  signal \^addra_reg[14]_p_0\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \addra_reg[14]_P_n_0\ : STD_LOGIC;
  signal \addra_reg[1]_C_n_0\ : STD_LOGIC;
  signal \addra_reg[1]_LDC_n_0\ : STD_LOGIC;
  signal \addra_reg[1]_P_n_0\ : STD_LOGIC;
  signal \addra_reg[2]_C_n_0\ : STD_LOGIC;
  signal \addra_reg[2]_LDC_n_0\ : STD_LOGIC;
  signal \addra_reg[2]_P_n_0\ : STD_LOGIC;
  signal \addra_reg[3]_C_n_0\ : STD_LOGIC;
  signal \addra_reg[3]_LDC_n_0\ : STD_LOGIC;
  signal \addra_reg[3]_P_n_0\ : STD_LOGIC;
  signal \addra_reg[4]_C_n_0\ : STD_LOGIC;
  signal \addra_reg[4]_LDC_n_0\ : STD_LOGIC;
  signal \addra_reg[4]_P_n_0\ : STD_LOGIC;
  signal \addra_reg[5]_C_n_0\ : STD_LOGIC;
  signal \addra_reg[5]_LDC_n_0\ : STD_LOGIC;
  signal \addra_reg[5]_P_n_0\ : STD_LOGIC;
  signal \addra_reg[6]_C_n_0\ : STD_LOGIC;
  signal \addra_reg[6]_LDC_n_0\ : STD_LOGIC;
  signal \addra_reg[6]_P_n_0\ : STD_LOGIC;
  signal \addra_reg[7]_C_n_0\ : STD_LOGIC;
  signal \addra_reg[7]_LDC_n_0\ : STD_LOGIC;
  signal \addra_reg[7]_P_n_0\ : STD_LOGIC;
  signal \addra_reg[8]_C_n_0\ : STD_LOGIC;
  signal \addra_reg[8]_LDC_n_0\ : STD_LOGIC;
  signal \addra_reg[8]_P_n_0\ : STD_LOGIC;
  signal \addra_reg[9]_C_n_0\ : STD_LOGIC;
  signal \addra_reg[9]_LDC_n_0\ : STD_LOGIC;
  signal \addra_reg[9]_P_n_0\ : STD_LOGIC;
  signal douta : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal \ind[0]_i_4_n_0\ : STD_LOGIC;
  signal \ind[0]_i_5_n_0\ : STD_LOGIC;
  signal \ind[0]_i_6_n_0\ : STD_LOGIC;
  signal \ind[0]_i_7_n_0\ : STD_LOGIC;
  signal \ind[12]_i_2_n_0\ : STD_LOGIC;
  signal \ind[12]_i_3_n_0\ : STD_LOGIC;
  signal \ind[12]_i_4_n_0\ : STD_LOGIC;
  signal \ind[4]_i_2_n_0\ : STD_LOGIC;
  signal \ind[4]_i_3_n_0\ : STD_LOGIC;
  signal \ind[4]_i_4_n_0\ : STD_LOGIC;
  signal \ind[4]_i_5_n_0\ : STD_LOGIC;
  signal \ind[8]_i_2_n_0\ : STD_LOGIC;
  signal \ind[8]_i_3_n_0\ : STD_LOGIC;
  signal \ind[8]_i_4_n_0\ : STD_LOGIC;
  signal \ind[8]_i_5_n_0\ : STD_LOGIC;
  signal ind_reg : STD_LOGIC_VECTOR ( 13 downto 0 );
  signal \ind_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \ind_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \ind_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \ind_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \ind_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \ind_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \ind_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \ind_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \ind_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \ind_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \ind_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \ind_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \ind_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \ind_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \ind_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \ind_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \ind_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \ind_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \ind_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \ind_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \ind_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \ind_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \ind_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \ind_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \ind_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \ind_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \ind_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \ind_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \ind_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal \pixel_bus[12]_i_1_n_0\ : STD_LOGIC;
  signal \pixel_bus[14]_i_1_n_0\ : STD_LOGIC;
  signal \pixel_bus[2]_i_1_n_0\ : STD_LOGIC;
  signal \pixel_bus[4]_i_1_n_0\ : STD_LOGIC;
  signal \pixel_bus[7]_i_1_n_0\ : STD_LOGIC;
  signal \pixel_bus[9]_i_1_n_0\ : STD_LOGIC;
  signal \NLW_ind_reg[12]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_ind_reg[12]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \addra_reg[0]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \addra_reg[10]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \addra_reg[11]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \addra_reg[12]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \addra_reg[13]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \addra_reg[14]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \addra_reg[1]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \addra_reg[2]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \addra_reg[3]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \addra_reg[4]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \addra_reg[5]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \addra_reg[6]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \addra_reg[7]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \addra_reg[8]_LDC\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \addra_reg[9]_LDC\ : label is "LDC";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \pixel_bus[12]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \pixel_bus[14]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \pixel_bus[2]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \pixel_bus[4]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \pixel_bus[7]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \pixel_bus[9]_i_1\ : label is "soft_lutpair12";
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of spritesBram : label is "blk_mem_gen_0,blk_mem_gen_v8_3_6,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of spritesBram : label is "yes";
  attribute x_core_info : string;
  attribute x_core_info of spritesBram : label is "blk_mem_gen_v8_3_6,Vivado 2017.1";
begin
  \addra_reg[14]_P_0\(0) <= \^addra_reg[14]_p_0\(0);
\addra[0]_C_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => ind_reg(0),
      I1 => fetching_sprites,
      I2 => \^addra_reg[14]_p_0\(0),
      I3 => \addra_reg[0]_C_n_0\,
      O => \addra[0]_C_i_1_n_0\
    );
\addra[10]_C_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => ind_reg(10),
      I1 => fetching_sprites,
      I2 => \^addra_reg[14]_p_0\(0),
      I3 => \addra_reg[10]_C_n_0\,
      O => \addra[10]_C_i_1_n_0\
    );
\addra[11]_C_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => ind_reg(11),
      I1 => fetching_sprites,
      I2 => \^addra_reg[14]_p_0\(0),
      I3 => \addra_reg[11]_C_n_0\,
      O => \addra[11]_C_i_1_n_0\
    );
\addra[12]_C_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => ind_reg(12),
      I1 => fetching_sprites,
      I2 => \^addra_reg[14]_p_0\(0),
      I3 => \addra_reg[12]_C_n_0\,
      O => \addra[12]_C_i_1_n_0\
    );
\addra[13]_C_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => ind_reg(13),
      I1 => fetching_sprites,
      I2 => \^addra_reg[14]_p_0\(0),
      I3 => \addra_reg[13]_C_n_0\,
      O => \addra[13]_C_i_1_n_0\
    );
\addra[14]_C_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"D0"
    )
        port map (
      I0 => fetching_sprites,
      I1 => \^addra_reg[14]_p_0\(0),
      I2 => \addra_reg[14]_C_n_0\,
      O => \addra[14]_C_i_1_n_0\
    );
\addra[1]_C_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => ind_reg(1),
      I1 => fetching_sprites,
      I2 => \^addra_reg[14]_p_0\(0),
      I3 => \addra_reg[1]_C_n_0\,
      O => \addra[1]_C_i_1_n_0\
    );
\addra[2]_C_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => ind_reg(2),
      I1 => fetching_sprites,
      I2 => \^addra_reg[14]_p_0\(0),
      I3 => \addra_reg[2]_C_n_0\,
      O => \addra[2]_C_i_1_n_0\
    );
\addra[3]_C_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => ind_reg(3),
      I1 => fetching_sprites,
      I2 => \^addra_reg[14]_p_0\(0),
      I3 => \addra_reg[3]_C_n_0\,
      O => \addra[3]_C_i_1_n_0\
    );
\addra[4]_C_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => ind_reg(4),
      I1 => fetching_sprites,
      I2 => \^addra_reg[14]_p_0\(0),
      I3 => \addra_reg[4]_C_n_0\,
      O => \addra[4]_C_i_1_n_0\
    );
\addra[5]_C_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => ind_reg(5),
      I1 => fetching_sprites,
      I2 => \^addra_reg[14]_p_0\(0),
      I3 => \addra_reg[5]_C_n_0\,
      O => \addra[5]_C_i_1_n_0\
    );
\addra[6]_C_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => ind_reg(6),
      I1 => fetching_sprites,
      I2 => \^addra_reg[14]_p_0\(0),
      I3 => \addra_reg[6]_C_n_0\,
      O => \addra[6]_C_i_1_n_0\
    );
\addra[7]_C_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => ind_reg(7),
      I1 => fetching_sprites,
      I2 => \^addra_reg[14]_p_0\(0),
      I3 => \addra_reg[7]_C_n_0\,
      O => \addra[7]_C_i_1_n_0\
    );
\addra[8]_C_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => ind_reg(8),
      I1 => fetching_sprites,
      I2 => \^addra_reg[14]_p_0\(0),
      I3 => \addra_reg[8]_C_n_0\,
      O => \addra[8]_C_i_1_n_0\
    );
\addra[9]_C_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FB08"
    )
        port map (
      I0 => ind_reg(9),
      I1 => fetching_sprites,
      I2 => \^addra_reg[14]_p_0\(0),
      I3 => \addra_reg[9]_C_n_0\,
      O => \addra[9]_C_i_1_n_0\
    );
\addra_reg[0]_C\: unisim.vcomponents.FDCE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => '1',
      CLR => render_enable_reg_28,
      D => \addra[0]_C_i_1_n_0\,
      Q => \addra_reg[0]_C_n_0\
    );
\addra_reg[0]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => render_enable_reg_28,
      D => '1',
      G => render_enable_reg_27,
      GE => '1',
      Q => \addra_reg[0]_LDC_n_0\
    );
\addra_reg[0]_P\: unisim.vcomponents.FDPE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => addra0,
      D => ind_reg(0),
      PRE => render_enable_reg_27,
      Q => \addra_reg[0]_P_n_0\
    );
\addra_reg[0]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => addra(0),
      Q => \addra_reg[14]_P_1\(0),
      R => '0'
    );
\addra_reg[10]_C\: unisim.vcomponents.FDCE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => '1',
      CLR => render_enable_reg_8,
      D => \addra[10]_C_i_1_n_0\,
      Q => \addra_reg[10]_C_n_0\
    );
\addra_reg[10]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => render_enable_reg_8,
      D => '1',
      G => render_enable_reg_7,
      GE => '1',
      Q => \addra_reg[10]_LDC_n_0\
    );
\addra_reg[10]_P\: unisim.vcomponents.FDPE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => addra0,
      D => ind_reg(10),
      PRE => render_enable_reg_7,
      Q => \addra_reg[10]_P_n_0\
    );
\addra_reg[10]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => addra(10),
      Q => \addra_reg[14]_P_1\(10),
      R => '0'
    );
\addra_reg[11]_C\: unisim.vcomponents.FDCE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => '1',
      CLR => render_enable_reg_6,
      D => \addra[11]_C_i_1_n_0\,
      Q => \addra_reg[11]_C_n_0\
    );
\addra_reg[11]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => render_enable_reg_6,
      D => '1',
      G => render_enable_reg_5,
      GE => '1',
      Q => \addra_reg[11]_LDC_n_0\
    );
\addra_reg[11]_P\: unisim.vcomponents.FDPE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => addra0,
      D => ind_reg(11),
      PRE => render_enable_reg_5,
      Q => \addra_reg[11]_P_n_0\
    );
\addra_reg[11]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => addra(11),
      Q => \addra_reg[14]_P_1\(11),
      R => '0'
    );
\addra_reg[12]_C\: unisim.vcomponents.FDCE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => '1',
      CLR => render_enable_reg_4,
      D => \addra[12]_C_i_1_n_0\,
      Q => \addra_reg[12]_C_n_0\
    );
\addra_reg[12]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => render_enable_reg_4,
      D => '1',
      G => render_enable_reg_3,
      GE => '1',
      Q => \addra_reg[12]_LDC_n_0\
    );
\addra_reg[12]_P\: unisim.vcomponents.FDPE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => addra0,
      D => ind_reg(12),
      PRE => render_enable_reg_3,
      Q => \addra_reg[12]_P_n_0\
    );
\addra_reg[12]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => addra(12),
      Q => \addra_reg[14]_P_1\(12),
      R => '0'
    );
\addra_reg[13]_C\: unisim.vcomponents.FDCE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => '1',
      CLR => render_enable_reg_2,
      D => \addra[13]_C_i_1_n_0\,
      Q => \addra_reg[13]_C_n_0\
    );
\addra_reg[13]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => render_enable_reg_2,
      D => '1',
      G => render_enable_reg_1,
      GE => '1',
      Q => \addra_reg[13]_LDC_n_0\
    );
\addra_reg[13]_P\: unisim.vcomponents.FDPE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => addra0,
      D => ind_reg(13),
      PRE => render_enable_reg_1,
      Q => \addra_reg[13]_P_n_0\
    );
\addra_reg[13]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => addra(13),
      Q => \addra_reg[14]_P_1\(13),
      R => '0'
    );
\addra_reg[14]_C\: unisim.vcomponents.FDCE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => '1',
      CLR => render_enable_reg_0,
      D => \addra[14]_C_i_1_n_0\,
      Q => \addra_reg[14]_C_n_0\
    );
\addra_reg[14]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => render_enable_reg_0,
      D => '1',
      G => render_enable_reg,
      GE => '1',
      Q => \addra_reg[14]_LDC_n_0\
    );
\addra_reg[14]_P\: unisim.vcomponents.FDPE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => addra0,
      D => \^addra_reg[14]_p_0\(0),
      PRE => render_enable_reg,
      Q => \addra_reg[14]_P_n_0\
    );
\addra_reg[14]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => addra(14),
      Q => \addra_reg[14]_P_1\(14),
      R => '0'
    );
\addra_reg[1]_C\: unisim.vcomponents.FDCE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => '1',
      CLR => render_enable_reg_26,
      D => \addra[1]_C_i_1_n_0\,
      Q => \addra_reg[1]_C_n_0\
    );
\addra_reg[1]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => render_enable_reg_26,
      D => '1',
      G => render_enable_reg_25,
      GE => '1',
      Q => \addra_reg[1]_LDC_n_0\
    );
\addra_reg[1]_P\: unisim.vcomponents.FDPE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => addra0,
      D => ind_reg(1),
      PRE => render_enable_reg_25,
      Q => \addra_reg[1]_P_n_0\
    );
\addra_reg[1]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => addra(1),
      Q => \addra_reg[14]_P_1\(1),
      R => '0'
    );
\addra_reg[2]_C\: unisim.vcomponents.FDCE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => '1',
      CLR => render_enable_reg_24,
      D => \addra[2]_C_i_1_n_0\,
      Q => \addra_reg[2]_C_n_0\
    );
\addra_reg[2]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => render_enable_reg_24,
      D => '1',
      G => render_enable_reg_23,
      GE => '1',
      Q => \addra_reg[2]_LDC_n_0\
    );
\addra_reg[2]_P\: unisim.vcomponents.FDPE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => addra0,
      D => ind_reg(2),
      PRE => render_enable_reg_23,
      Q => \addra_reg[2]_P_n_0\
    );
\addra_reg[2]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => addra(2),
      Q => \addra_reg[14]_P_1\(2),
      R => '0'
    );
\addra_reg[3]_C\: unisim.vcomponents.FDCE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => '1',
      CLR => render_enable_reg_22,
      D => \addra[3]_C_i_1_n_0\,
      Q => \addra_reg[3]_C_n_0\
    );
\addra_reg[3]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => render_enable_reg_22,
      D => '1',
      G => render_enable_reg_21,
      GE => '1',
      Q => \addra_reg[3]_LDC_n_0\
    );
\addra_reg[3]_P\: unisim.vcomponents.FDPE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => addra0,
      D => ind_reg(3),
      PRE => render_enable_reg_21,
      Q => \addra_reg[3]_P_n_0\
    );
\addra_reg[3]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => addra(3),
      Q => \addra_reg[14]_P_1\(3),
      R => '0'
    );
\addra_reg[4]_C\: unisim.vcomponents.FDCE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => '1',
      CLR => render_enable_reg_20,
      D => \addra[4]_C_i_1_n_0\,
      Q => \addra_reg[4]_C_n_0\
    );
\addra_reg[4]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => render_enable_reg_20,
      D => '1',
      G => render_enable_reg_19,
      GE => '1',
      Q => \addra_reg[4]_LDC_n_0\
    );
\addra_reg[4]_P\: unisim.vcomponents.FDPE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => addra0,
      D => ind_reg(4),
      PRE => render_enable_reg_19,
      Q => \addra_reg[4]_P_n_0\
    );
\addra_reg[4]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => addra(4),
      Q => \addra_reg[14]_P_1\(4),
      R => '0'
    );
\addra_reg[5]_C\: unisim.vcomponents.FDCE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => '1',
      CLR => render_enable_reg_18,
      D => \addra[5]_C_i_1_n_0\,
      Q => \addra_reg[5]_C_n_0\
    );
\addra_reg[5]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => render_enable_reg_18,
      D => '1',
      G => render_enable_reg_17,
      GE => '1',
      Q => \addra_reg[5]_LDC_n_0\
    );
\addra_reg[5]_P\: unisim.vcomponents.FDPE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => addra0,
      D => ind_reg(5),
      PRE => render_enable_reg_17,
      Q => \addra_reg[5]_P_n_0\
    );
\addra_reg[5]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => addra(5),
      Q => \addra_reg[14]_P_1\(5),
      R => '0'
    );
\addra_reg[6]_C\: unisim.vcomponents.FDCE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => '1',
      CLR => render_enable_reg_16,
      D => \addra[6]_C_i_1_n_0\,
      Q => \addra_reg[6]_C_n_0\
    );
\addra_reg[6]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => render_enable_reg_16,
      D => '1',
      G => render_enable_reg_15,
      GE => '1',
      Q => \addra_reg[6]_LDC_n_0\
    );
\addra_reg[6]_P\: unisim.vcomponents.FDPE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => addra0,
      D => ind_reg(6),
      PRE => render_enable_reg_15,
      Q => \addra_reg[6]_P_n_0\
    );
\addra_reg[6]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => addra(6),
      Q => \addra_reg[14]_P_1\(6),
      R => '0'
    );
\addra_reg[7]_C\: unisim.vcomponents.FDCE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => '1',
      CLR => render_enable_reg_14,
      D => \addra[7]_C_i_1_n_0\,
      Q => \addra_reg[7]_C_n_0\
    );
\addra_reg[7]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => render_enable_reg_14,
      D => '1',
      G => render_enable_reg_13,
      GE => '1',
      Q => \addra_reg[7]_LDC_n_0\
    );
\addra_reg[7]_P\: unisim.vcomponents.FDPE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => addra0,
      D => ind_reg(7),
      PRE => render_enable_reg_13,
      Q => \addra_reg[7]_P_n_0\
    );
\addra_reg[7]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => addra(7),
      Q => \addra_reg[14]_P_1\(7),
      R => '0'
    );
\addra_reg[8]_C\: unisim.vcomponents.FDCE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => '1',
      CLR => render_enable_reg_12,
      D => \addra[8]_C_i_1_n_0\,
      Q => \addra_reg[8]_C_n_0\
    );
\addra_reg[8]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => render_enable_reg_12,
      D => '1',
      G => render_enable_reg_11,
      GE => '1',
      Q => \addra_reg[8]_LDC_n_0\
    );
\addra_reg[8]_P\: unisim.vcomponents.FDPE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => addra0,
      D => ind_reg(8),
      PRE => render_enable_reg_11,
      Q => \addra_reg[8]_P_n_0\
    );
\addra_reg[8]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => addra(8),
      Q => \addra_reg[14]_P_1\(8),
      R => '0'
    );
\addra_reg[9]_C\: unisim.vcomponents.FDCE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => '1',
      CLR => render_enable_reg_10,
      D => \addra[9]_C_i_1_n_0\,
      Q => \addra_reg[9]_C_n_0\
    );
\addra_reg[9]_LDC\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => render_enable_reg_10,
      D => '1',
      G => render_enable_reg_9,
      GE => '1',
      Q => \addra_reg[9]_LDC_n_0\
    );
\addra_reg[9]_P\: unisim.vcomponents.FDPE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => clk,
      CE => addra0,
      D => ind_reg(9),
      PRE => render_enable_reg_9,
      Q => \addra_reg[9]_P_n_0\
    );
\addra_reg[9]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => addra(9),
      Q => \addra_reg[14]_P_1\(9),
      R => '0'
    );
\ind[0]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => ind_reg(3),
      O => \ind[0]_i_4_n_0\
    );
\ind[0]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => ind_reg(2),
      O => \ind[0]_i_5_n_0\
    );
\ind[0]_i_6\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => ind_reg(1),
      O => \ind[0]_i_6_n_0\
    );
\ind[0]_i_7\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ind_reg(0),
      O => \ind[0]_i_7_n_0\
    );
\ind[12]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^addra_reg[14]_p_0\(0),
      O => \ind[12]_i_2_n_0\
    );
\ind[12]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => ind_reg(13),
      O => \ind[12]_i_3_n_0\
    );
\ind[12]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => ind_reg(12),
      O => \ind[12]_i_4_n_0\
    );
\ind[4]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => ind_reg(7),
      O => \ind[4]_i_2_n_0\
    );
\ind[4]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => ind_reg(6),
      O => \ind[4]_i_3_n_0\
    );
\ind[4]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => ind_reg(5),
      O => \ind[4]_i_4_n_0\
    );
\ind[4]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => ind_reg(4),
      O => \ind[4]_i_5_n_0\
    );
\ind[8]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => ind_reg(11),
      O => \ind[8]_i_2_n_0\
    );
\ind[8]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => ind_reg(10),
      O => \ind[8]_i_3_n_0\
    );
\ind[8]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => ind_reg(9),
      O => \ind[8]_i_4_n_0\
    );
\ind[8]_i_5\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => ind_reg(8),
      O => \ind[8]_i_5_n_0\
    );
\ind_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_0,
      CE => addra0,
      D => \ind_reg[0]_i_2_n_7\,
      Q => ind_reg(0),
      R => '0'
    );
\ind_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \ind_reg[0]_i_2_n_0\,
      CO(2) => \ind_reg[0]_i_2_n_1\,
      CO(1) => \ind_reg[0]_i_2_n_2\,
      CO(0) => \ind_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \ind_reg[0]_i_2_n_4\,
      O(2) => \ind_reg[0]_i_2_n_5\,
      O(1) => \ind_reg[0]_i_2_n_6\,
      O(0) => \ind_reg[0]_i_2_n_7\,
      S(3) => \ind[0]_i_4_n_0\,
      S(2) => \ind[0]_i_5_n_0\,
      S(1) => \ind[0]_i_6_n_0\,
      S(0) => \ind[0]_i_7_n_0\
    );
\ind_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_0,
      CE => addra0,
      D => \ind_reg[8]_i_1_n_5\,
      Q => ind_reg(10),
      R => '0'
    );
\ind_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_0,
      CE => addra0,
      D => \ind_reg[8]_i_1_n_4\,
      Q => ind_reg(11),
      R => '0'
    );
\ind_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_0,
      CE => addra0,
      D => \ind_reg[12]_i_1_n_7\,
      Q => ind_reg(12),
      R => '0'
    );
\ind_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \ind_reg[8]_i_1_n_0\,
      CO(3 downto 2) => \NLW_ind_reg[12]_i_1_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \ind_reg[12]_i_1_n_2\,
      CO(0) => \ind_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_ind_reg[12]_i_1_O_UNCONNECTED\(3),
      O(2) => \ind_reg[12]_i_1_n_5\,
      O(1) => \ind_reg[12]_i_1_n_6\,
      O(0) => \ind_reg[12]_i_1_n_7\,
      S(3) => '0',
      S(2) => \ind[12]_i_2_n_0\,
      S(1) => \ind[12]_i_3_n_0\,
      S(0) => \ind[12]_i_4_n_0\
    );
\ind_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_0,
      CE => addra0,
      D => \ind_reg[12]_i_1_n_6\,
      Q => ind_reg(13),
      R => '0'
    );
\ind_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_0,
      CE => addra0,
      D => \ind_reg[12]_i_1_n_5\,
      Q => \^addra_reg[14]_p_0\(0),
      R => '0'
    );
\ind_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_0,
      CE => addra0,
      D => \ind_reg[0]_i_2_n_6\,
      Q => ind_reg(1),
      R => '0'
    );
\ind_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_0,
      CE => addra0,
      D => \ind_reg[0]_i_2_n_5\,
      Q => ind_reg(2),
      R => '0'
    );
\ind_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_0,
      CE => addra0,
      D => \ind_reg[0]_i_2_n_4\,
      Q => ind_reg(3),
      R => '0'
    );
\ind_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_0,
      CE => addra0,
      D => \ind_reg[4]_i_1_n_7\,
      Q => ind_reg(4),
      R => '0'
    );
\ind_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \ind_reg[0]_i_2_n_0\,
      CO(3) => \ind_reg[4]_i_1_n_0\,
      CO(2) => \ind_reg[4]_i_1_n_1\,
      CO(1) => \ind_reg[4]_i_1_n_2\,
      CO(0) => \ind_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \ind_reg[4]_i_1_n_4\,
      O(2) => \ind_reg[4]_i_1_n_5\,
      O(1) => \ind_reg[4]_i_1_n_6\,
      O(0) => \ind_reg[4]_i_1_n_7\,
      S(3) => \ind[4]_i_2_n_0\,
      S(2) => \ind[4]_i_3_n_0\,
      S(1) => \ind[4]_i_4_n_0\,
      S(0) => \ind[4]_i_5_n_0\
    );
\ind_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_0,
      CE => addra0,
      D => \ind_reg[4]_i_1_n_6\,
      Q => ind_reg(5),
      R => '0'
    );
\ind_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_0,
      CE => addra0,
      D => \ind_reg[4]_i_1_n_5\,
      Q => ind_reg(6),
      R => '0'
    );
\ind_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_0,
      CE => addra0,
      D => \ind_reg[4]_i_1_n_4\,
      Q => ind_reg(7),
      R => '0'
    );
\ind_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_0,
      CE => addra0,
      D => \ind_reg[8]_i_1_n_7\,
      Q => ind_reg(8),
      R => '0'
    );
\ind_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \ind_reg[4]_i_1_n_0\,
      CO(3) => \ind_reg[8]_i_1_n_0\,
      CO(2) => \ind_reg[8]_i_1_n_1\,
      CO(1) => \ind_reg[8]_i_1_n_2\,
      CO(0) => \ind_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \ind_reg[8]_i_1_n_4\,
      O(2) => \ind_reg[8]_i_1_n_5\,
      O(1) => \ind_reg[8]_i_1_n_6\,
      O(0) => \ind_reg[8]_i_1_n_7\,
      S(3) => \ind[8]_i_2_n_0\,
      S(2) => \ind[8]_i_3_n_0\,
      S(1) => \ind[8]_i_4_n_0\,
      S(0) => \ind[8]_i_5_n_0\
    );
\ind_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_0,
      CE => addra0,
      D => \ind_reg[8]_i_1_n_6\,
      Q => ind_reg(9),
      R => '0'
    );
\pixel_bus[12]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => douta(5),
      I1 => douta(4),
      O => \pixel_bus[12]_i_1_n_0\
    );
\pixel_bus[14]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => douta(5),
      I1 => douta(4),
      O => \pixel_bus[14]_i_1_n_0\
    );
\pixel_bus[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => douta(1),
      I1 => douta(0),
      O => \pixel_bus[2]_i_1_n_0\
    );
\pixel_bus[4]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => douta(1),
      I1 => douta(0),
      O => \pixel_bus[4]_i_1_n_0\
    );
\pixel_bus[7]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => douta(3),
      I1 => douta(2),
      O => \pixel_bus[7]_i_1_n_0\
    );
\pixel_bus[9]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => douta(3),
      I1 => douta(2),
      O => \pixel_bus[9]_i_1_n_0\
    );
\pixel_bus_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => pixel_clk,
      CE => render_enable,
      D => \pixel_bus[12]_i_1_n_0\,
      Q => Q(6),
      R => '0'
    );
\pixel_bus_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => pixel_clk,
      CE => render_enable,
      D => douta(4),
      Q => Q(7),
      R => '0'
    );
\pixel_bus_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => pixel_clk,
      CE => render_enable,
      D => \pixel_bus[14]_i_1_n_0\,
      Q => Q(8),
      R => '0'
    );
\pixel_bus_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => pixel_clk,
      CE => render_enable,
      D => douta(5),
      Q => Q(9),
      R => '0'
    );
\pixel_bus_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => pixel_clk,
      CE => render_enable,
      D => \pixel_bus[2]_i_1_n_0\,
      Q => Q(0),
      R => '0'
    );
\pixel_bus_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => pixel_clk,
      CE => render_enable,
      D => douta(0),
      Q => Q(1),
      R => '0'
    );
\pixel_bus_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => pixel_clk,
      CE => render_enable,
      D => \pixel_bus[4]_i_1_n_0\,
      Q => Q(2),
      R => '0'
    );
\pixel_bus_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => pixel_clk,
      CE => render_enable,
      D => \pixel_bus[7]_i_1_n_0\,
      Q => Q(3),
      R => '0'
    );
\pixel_bus_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => pixel_clk,
      CE => render_enable,
      D => douta(2),
      Q => Q(4),
      R => '0'
    );
\pixel_bus_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => pixel_clk,
      CE => render_enable,
      D => \pixel_bus[9]_i_1_n_0\,
      Q => Q(5),
      R => '0'
    );
spritesBram: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_blk_mem_gen_0
     port map (
      addra(14 downto 0) => addra(14 downto 0),
      clka => clk,
      dina(5 downto 0) => dina(5 downto 0),
      douta(5 downto 0) => douta(5 downto 0),
      wea(0) => fetching_sprites
    );
spritesBram_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \addra_reg[14]_P_n_0\,
      I1 => \addra_reg[14]_LDC_n_0\,
      I2 => \addra_reg[14]_C_n_0\,
      O => addra(14)
    );
spritesBram_i_10: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \addra_reg[5]_P_n_0\,
      I1 => \addra_reg[5]_LDC_n_0\,
      I2 => \addra_reg[5]_C_n_0\,
      O => addra(5)
    );
spritesBram_i_11: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \addra_reg[4]_P_n_0\,
      I1 => \addra_reg[4]_LDC_n_0\,
      I2 => \addra_reg[4]_C_n_0\,
      O => addra(4)
    );
spritesBram_i_12: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \addra_reg[3]_P_n_0\,
      I1 => \addra_reg[3]_LDC_n_0\,
      I2 => \addra_reg[3]_C_n_0\,
      O => addra(3)
    );
spritesBram_i_13: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \addra_reg[2]_P_n_0\,
      I1 => \addra_reg[2]_LDC_n_0\,
      I2 => \addra_reg[2]_C_n_0\,
      O => addra(2)
    );
spritesBram_i_14: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \addra_reg[1]_P_n_0\,
      I1 => \addra_reg[1]_LDC_n_0\,
      I2 => \addra_reg[1]_C_n_0\,
      O => addra(1)
    );
spritesBram_i_15: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \addra_reg[0]_P_n_0\,
      I1 => \addra_reg[0]_LDC_n_0\,
      I2 => \addra_reg[0]_C_n_0\,
      O => addra(0)
    );
spritesBram_i_2: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \addra_reg[13]_P_n_0\,
      I1 => \addra_reg[13]_LDC_n_0\,
      I2 => \addra_reg[13]_C_n_0\,
      O => addra(13)
    );
spritesBram_i_3: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \addra_reg[12]_P_n_0\,
      I1 => \addra_reg[12]_LDC_n_0\,
      I2 => \addra_reg[12]_C_n_0\,
      O => addra(12)
    );
spritesBram_i_4: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \addra_reg[11]_P_n_0\,
      I1 => \addra_reg[11]_LDC_n_0\,
      I2 => \addra_reg[11]_C_n_0\,
      O => addra(11)
    );
spritesBram_i_5: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \addra_reg[10]_P_n_0\,
      I1 => \addra_reg[10]_LDC_n_0\,
      I2 => \addra_reg[10]_C_n_0\,
      O => addra(10)
    );
spritesBram_i_6: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \addra_reg[9]_P_n_0\,
      I1 => \addra_reg[9]_LDC_n_0\,
      I2 => \addra_reg[9]_C_n_0\,
      O => addra(9)
    );
spritesBram_i_7: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \addra_reg[8]_P_n_0\,
      I1 => \addra_reg[8]_LDC_n_0\,
      I2 => \addra_reg[8]_C_n_0\,
      O => addra(8)
    );
spritesBram_i_8: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \addra_reg[7]_P_n_0\,
      I1 => \addra_reg[7]_LDC_n_0\,
      I2 => \addra_reg[7]_C_n_0\,
      O => addra(7)
    );
spritesBram_i_9: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \addra_reg[6]_P_n_0\,
      I1 => \addra_reg[6]_LDC_n_0\,
      I2 => \addra_reg[6]_C_n_0\,
      O => addra(6)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_top is
  port (
    vga_hs : out STD_LOGIC;
    vga_vs : out STD_LOGIC;
    fetch : out STD_LOGIC;
    data_type : out STD_LOGIC;
    led0 : out STD_LOGIC;
    led1 : out STD_LOGIC;
    led2 : out STD_LOGIC;
    led3 : out STD_LOGIC;
    vga_r : out STD_LOGIC_VECTOR ( 2 downto 0 );
    vga_g : out STD_LOGIC_VECTOR ( 3 downto 0 );
    vga_b : out STD_LOGIC_VECTOR ( 2 downto 0 );
    map_id : out STD_LOGIC_VECTOR ( 6 downto 0 );
    clk : in STD_LOGIC;
    clk_0 : in STD_LOGIC;
    pixel_clk : in STD_LOGIC;
    sw : in STD_LOGIC_VECTOR ( 2 downto 0 );
    fetching : in STD_LOGIC;
    packet_in : in STD_LOGIC_VECTOR ( 5 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_top;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_top is
  signal addra0 : STD_LOGIC;
  signal addra_reg : STD_LOGIC_VECTOR ( 14 downto 0 );
  signal cnt : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal \cnt_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_reg[1]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_reg[2]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_reg[5]_i_1_n_0\ : STD_LOGIC;
  signal \cnt_reg[5]_i_2_n_0\ : STD_LOGIC;
  signal fetch_complete : STD_LOGIC;
  signal fetching_sprites : STD_LOGIC;
  signal ind_reg : STD_LOGIC_VECTOR ( 14 to 14 );
  signal pixel : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal pixel_bus : STD_LOGIC_VECTOR ( 15 downto 2 );
  signal random : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \random_reg[6]_i_1_n_0\ : STD_LOGIC;
  signal \random_reg[6]_i_2_n_0\ : STD_LOGIC;
  signal render_enable : STD_LOGIC;
  signal tmp_rand : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal vga_n_10 : STD_LOGIC;
  signal vga_n_11 : STD_LOGIC;
  signal vga_n_12 : STD_LOGIC;
  signal vga_n_13 : STD_LOGIC;
  signal vga_n_14 : STD_LOGIC;
  signal vga_n_15 : STD_LOGIC;
  signal vga_n_16 : STD_LOGIC;
  signal vga_n_17 : STD_LOGIC;
  signal vga_n_18 : STD_LOGIC;
  signal vga_n_19 : STD_LOGIC;
  signal vga_n_20 : STD_LOGIC;
  signal vga_n_21 : STD_LOGIC;
  signal vga_n_22 : STD_LOGIC;
  signal vga_n_23 : STD_LOGIC;
  signal vga_n_24 : STD_LOGIC;
  signal vga_n_25 : STD_LOGIC;
  signal vga_n_26 : STD_LOGIC;
  signal vga_n_27 : STD_LOGIC;
  signal vga_n_28 : STD_LOGIC;
  signal vga_n_29 : STD_LOGIC;
  signal vga_n_3 : STD_LOGIC;
  signal vga_n_30 : STD_LOGIC;
  signal vga_n_31 : STD_LOGIC;
  signal vga_n_32 : STD_LOGIC;
  signal vga_n_4 : STD_LOGIC;
  signal vga_n_5 : STD_LOGIC;
  signal vga_n_6 : STD_LOGIC;
  signal vga_n_7 : STD_LOGIC;
  signal vga_n_8 : STD_LOGIC;
  signal vga_n_9 : STD_LOGIC;
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \cnt_reg[0]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \cnt_reg[1]\ : label is "LD";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \cnt_reg[1]_i_1\ : label is "soft_lutpair42";
  attribute XILINX_LEGACY_PRIM of \cnt_reg[2]\ : label is "LD";
  attribute SOFT_HLUTNM of \cnt_reg[2]_i_1\ : label is "soft_lutpair42";
  attribute XILINX_LEGACY_PRIM of \cnt_reg[3]\ : label is "LD";
  attribute SOFT_HLUTNM of \cnt_reg[3]_i_1\ : label is "soft_lutpair40";
  attribute XILINX_LEGACY_PRIM of \cnt_reg[4]\ : label is "LD";
  attribute SOFT_HLUTNM of \cnt_reg[4]_i_1\ : label is "soft_lutpair40";
  attribute XILINX_LEGACY_PRIM of \cnt_reg[5]\ : label is "LD";
  attribute SOFT_HLUTNM of \cnt_reg[5]_i_2\ : label is "soft_lutpair41";
  attribute XILINX_LEGACY_PRIM of \random_reg[0]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \random_reg[1]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \random_reg[2]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \random_reg[3]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \random_reg[4]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \random_reg[5]\ : label is "LD";
  attribute XILINX_LEGACY_PRIM of \random_reg[6]\ : label is "LD";
  attribute SOFT_HLUTNM of \random_reg[6]_i_1\ : label is "soft_lutpair41";
begin
boot: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_booting
     port map (
      D(6 downto 0) => tmp_rand(6 downto 0),
      Q(6 downto 0) => random(6 downto 0),
      addra0 => addra0,
      clk => clk,
      data_type => data_type,
      dina(5 downto 0) => pixel(5 downto 0),
      fetch => fetch,
      fetch_complete => fetch_complete,
      fetching => fetching,
      fetching_sprites => fetching_sprites,
      ind_reg(0) => ind_reg(14),
      led0 => led0,
      led1 => led1,
      led2 => led2,
      led3 => led3,
      map_id(6 downto 0) => map_id(6 downto 0),
      packet_in(5 downto 0) => packet_in(5 downto 0),
      sw(2 downto 0) => sw(2 downto 0)
    );
\cnt_reg[0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \cnt_reg[0]_i_1_n_0\,
      G => \cnt_reg[5]_i_2_n_0\,
      GE => '1',
      Q => cnt(0)
    );
\cnt_reg[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt(0),
      O => \cnt_reg[0]_i_1_n_0\
    );
\cnt_reg[1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \cnt_reg[1]_i_1_n_0\,
      G => \cnt_reg[5]_i_2_n_0\,
      GE => '1',
      Q => cnt(1)
    );
\cnt_reg[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => cnt(0),
      I1 => cnt(1),
      O => \cnt_reg[1]_i_1_n_0\
    );
\cnt_reg[2]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \cnt_reg[2]_i_1_n_0\,
      G => \cnt_reg[5]_i_2_n_0\,
      GE => '1',
      Q => cnt(2)
    );
\cnt_reg[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => cnt(0),
      I1 => cnt(1),
      I2 => cnt(2),
      O => \cnt_reg[2]_i_1_n_0\
    );
\cnt_reg[3]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \cnt_reg[3]_i_1_n_0\,
      G => \cnt_reg[5]_i_2_n_0\,
      GE => '1',
      Q => cnt(3)
    );
\cnt_reg[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => cnt(1),
      I1 => cnt(0),
      I2 => cnt(2),
      I3 => cnt(3),
      O => \cnt_reg[3]_i_1_n_0\
    );
\cnt_reg[4]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \cnt_reg[4]_i_1_n_0\,
      G => \cnt_reg[5]_i_2_n_0\,
      GE => '1',
      Q => cnt(4)
    );
\cnt_reg[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => cnt(2),
      I1 => cnt(0),
      I2 => cnt(1),
      I3 => cnt(3),
      I4 => cnt(4),
      O => \cnt_reg[4]_i_1_n_0\
    );
\cnt_reg[5]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => \cnt_reg[5]_i_1_n_0\,
      G => \cnt_reg[5]_i_2_n_0\,
      GE => '1',
      Q => cnt(5)
    );
\cnt_reg[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => cnt(3),
      I1 => cnt(1),
      I2 => cnt(0),
      I3 => cnt(2),
      I4 => cnt(4),
      I5 => cnt(5),
      O => \cnt_reg[5]_i_1_n_0\
    );
\cnt_reg[5]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"E0"
    )
        port map (
      I0 => sw(1),
      I1 => sw(0),
      I2 => \random_reg[6]_i_2_n_0\,
      O => \cnt_reg[5]_i_2_n_0\
    );
\random_reg[0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => tmp_rand(0),
      G => \random_reg[6]_i_1_n_0\,
      GE => '1',
      Q => random(0)
    );
\random_reg[1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => tmp_rand(1),
      G => \random_reg[6]_i_1_n_0\,
      GE => '1',
      Q => random(1)
    );
\random_reg[2]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => tmp_rand(2),
      G => \random_reg[6]_i_1_n_0\,
      GE => '1',
      Q => random(2)
    );
\random_reg[3]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => tmp_rand(3),
      G => \random_reg[6]_i_1_n_0\,
      GE => '1',
      Q => random(3)
    );
\random_reg[4]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => tmp_rand(4),
      G => \random_reg[6]_i_1_n_0\,
      GE => '1',
      Q => random(4)
    );
\random_reg[5]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => tmp_rand(5),
      G => \random_reg[6]_i_1_n_0\,
      GE => '1',
      Q => random(5)
    );
\random_reg[6]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => '0',
      D => tmp_rand(6),
      G => \random_reg[6]_i_1_n_0\,
      GE => '1',
      Q => random(6)
    );
\random_reg[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"0E"
    )
        port map (
      I0 => sw(1),
      I1 => sw(0),
      I2 => \random_reg[6]_i_2_n_0\,
      O => \random_reg[6]_i_1_n_0\
    );
\random_reg[6]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00007FFF"
    )
        port map (
      I0 => cnt(4),
      I1 => cnt(3),
      I2 => cnt(1),
      I3 => cnt(2),
      I4 => cnt(5),
      O => \random_reg[6]_i_2_n_0\
    );
rend: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_renderer
     port map (
      Q(9 downto 6) => pixel_bus(15 downto 12),
      Q(5 downto 3) => pixel_bus(9 downto 7),
      Q(2 downto 0) => pixel_bus(4 downto 2),
      addra0 => addra0,
      \addra_reg[14]_P_0\(0) => ind_reg(14),
      \addra_reg[14]_P_1\(14 downto 0) => addra_reg(14 downto 0),
      clk => clk,
      clk_0 => clk_0,
      dina(5 downto 0) => pixel(5 downto 0),
      fetching_sprites => fetching_sprites,
      pixel_clk => pixel_clk,
      render_enable => render_enable,
      render_enable_reg => vga_n_3,
      render_enable_reg_0 => vga_n_4,
      render_enable_reg_1 => vga_n_5,
      render_enable_reg_10 => vga_n_14,
      render_enable_reg_11 => vga_n_15,
      render_enable_reg_12 => vga_n_16,
      render_enable_reg_13 => vga_n_17,
      render_enable_reg_14 => vga_n_18,
      render_enable_reg_15 => vga_n_19,
      render_enable_reg_16 => vga_n_20,
      render_enable_reg_17 => vga_n_21,
      render_enable_reg_18 => vga_n_22,
      render_enable_reg_19 => vga_n_23,
      render_enable_reg_2 => vga_n_6,
      render_enable_reg_20 => vga_n_24,
      render_enable_reg_21 => vga_n_25,
      render_enable_reg_22 => vga_n_26,
      render_enable_reg_23 => vga_n_27,
      render_enable_reg_24 => vga_n_28,
      render_enable_reg_25 => vga_n_29,
      render_enable_reg_26 => vga_n_30,
      render_enable_reg_27 => vga_n_31,
      render_enable_reg_28 => vga_n_32,
      render_enable_reg_3 => vga_n_7,
      render_enable_reg_4 => vga_n_8,
      render_enable_reg_5 => vga_n_9,
      render_enable_reg_6 => vga_n_10,
      render_enable_reg_7 => vga_n_11,
      render_enable_reg_8 => vga_n_12,
      render_enable_reg_9 => vga_n_13
    );
vga: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_vga_connector
     port map (
      Q(9 downto 6) => pixel_bus(15 downto 12),
      Q(5 downto 3) => pixel_bus(9 downto 7),
      Q(2 downto 0) => pixel_bus(4 downto 2),
      \addra_reg[0]_C\ => vga_n_32,
      \addra_reg[0]_P\ => vga_n_31,
      \addra_reg[10]_C\ => vga_n_12,
      \addra_reg[10]_P\ => vga_n_11,
      \addra_reg[11]_C\ => vga_n_10,
      \addra_reg[11]_P\ => vga_n_9,
      \addra_reg[12]_C\ => vga_n_8,
      \addra_reg[12]_P\ => vga_n_7,
      \addra_reg[13]_C\ => vga_n_6,
      \addra_reg[13]_P\ => vga_n_5,
      \addra_reg[14]_C\ => vga_n_4,
      \addra_reg[14]_P\ => vga_n_3,
      \addra_reg[14]__0\(14 downto 0) => addra_reg(14 downto 0),
      \addra_reg[1]_C\ => vga_n_30,
      \addra_reg[1]_P\ => vga_n_29,
      \addra_reg[2]_C\ => vga_n_28,
      \addra_reg[2]_P\ => vga_n_27,
      \addra_reg[3]_C\ => vga_n_26,
      \addra_reg[3]_P\ => vga_n_25,
      \addra_reg[4]_C\ => vga_n_24,
      \addra_reg[4]_P\ => vga_n_23,
      \addra_reg[5]_C\ => vga_n_22,
      \addra_reg[5]_P\ => vga_n_21,
      \addra_reg[6]_C\ => vga_n_20,
      \addra_reg[6]_P\ => vga_n_19,
      \addra_reg[7]_C\ => vga_n_18,
      \addra_reg[7]_P\ => vga_n_17,
      \addra_reg[8]_C\ => vga_n_16,
      \addra_reg[8]_P\ => vga_n_15,
      \addra_reg[9]_C\ => vga_n_14,
      \addra_reg[9]_P\ => vga_n_13,
      fetch_complete => fetch_complete,
      pixel_clk => pixel_clk,
      render_enable => render_enable,
      vga_b(2 downto 0) => vga_b(2 downto 0),
      vga_g(3 downto 0) => vga_g(3 downto 0),
      vga_hs => vga_hs,
      vga_r(2 downto 0) => vga_r(2 downto 0),
      vga_vs => vga_vs
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    clk : in STD_LOGIC;
    pixel_clk : in STD_LOGIC;
    sw : in STD_LOGIC_VECTOR ( 3 downto 0 );
    vga_r : out STD_LOGIC_VECTOR ( 4 downto 0 );
    vga_g : out STD_LOGIC_VECTOR ( 5 downto 0 );
    vga_b : out STD_LOGIC_VECTOR ( 4 downto 0 );
    vga_hs : out STD_LOGIC;
    vga_vs : out STD_LOGIC;
    fetch : out STD_LOGIC;
    data_type : out STD_LOGIC;
    map_id : out STD_LOGIC_VECTOR ( 7 downto 0 );
    packet_in : in STD_LOGIC_VECTOR ( 31 downto 0 );
    fetching : in STD_LOGIC;
    led0 : out STD_LOGIC;
    led1 : out STD_LOGIC;
    led2 : out STD_LOGIC;
    led3 : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_rendVgaTmBoot_0_0,top,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "top,Vivado 2017.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  signal \ind_reg[0]_i_3_n_0\ : STD_LOGIC;
  signal \^map_id\ : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal \^vga_b\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \^vga_g\ : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal \^vga_r\ : STD_LOGIC_VECTOR ( 2 downto 0 );
begin
  map_id(7) <= \<const0>\;
  map_id(6 downto 0) <= \^map_id\(6 downto 0);
  vga_b(4) <= \^vga_b\(0);
  vga_b(3) <= \^vga_b\(1);
  vga_b(2 downto 0) <= \^vga_b\(2 downto 0);
  vga_g(5) <= \^vga_g\(5);
  vga_g(4) <= \^vga_g\(0);
  vga_g(3) <= \^vga_g\(1);
  vga_g(2 downto 0) <= \^vga_g\(2 downto 0);
  vga_r(4) <= \^vga_r\(0);
  vga_r(3) <= \^vga_r\(1);
  vga_r(2 downto 0) <= \^vga_r\(2 downto 0);
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
U0: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_top
     port map (
      clk => clk,
      clk_0 => \ind_reg[0]_i_3_n_0\,
      data_type => data_type,
      fetch => fetch,
      fetching => fetching,
      led0 => led0,
      led1 => led1,
      led2 => led2,
      led3 => led3,
      map_id(6 downto 0) => \^map_id\(6 downto 0),
      packet_in(5 downto 0) => packet_in(5 downto 0),
      pixel_clk => pixel_clk,
      sw(2 downto 0) => sw(3 downto 1),
      vga_b(2) => \^vga_b\(0),
      vga_b(1) => \^vga_b\(1),
      vga_b(0) => \^vga_b\(2),
      vga_g(3) => \^vga_g\(5),
      vga_g(2) => \^vga_g\(0),
      vga_g(1) => \^vga_g\(1),
      vga_g(0) => \^vga_g\(2),
      vga_hs => vga_hs,
      vga_r(2) => \^vga_r\(0),
      vga_r(1) => \^vga_r\(1),
      vga_r(0) => \^vga_r\(2),
      vga_vs => vga_vs
    );
\ind_reg[0]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => clk,
      O => \ind_reg[0]_i_3_n_0\
    );
end STRUCTURE;
