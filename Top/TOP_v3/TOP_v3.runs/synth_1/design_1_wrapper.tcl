# 
# Synthesis run script generated by Vivado
# 

set_msg_config -id {HDL-1065} -limit 10000
create_project -in_memory -part xc7z010clg400-1

set_param project.singleFileAddWarning.threshold 0
set_param project.compositeFile.enableAutoGeneration 0
set_param synth.vivado.isSynthRun true
set_msg_config -source 4 -id {IP_Flow 19-2162} -severity warning -new_severity info
set_property webtalk.parent_dir /home/andreas/Desktop/computer_architecture/project/gitlab/TOP/TOP_v3/TOP_v3.cache/wt [current_project]
set_property parent.project_path /home/andreas/Desktop/computer_architecture/project/gitlab/TOP/TOP_v3/TOP_v3.xpr [current_project]
set_property XPM_LIBRARIES {XPM_FIFO XPM_MEMORY} [current_project]
set_property default_lib xil_defaultlib [current_project]
set_property target_language Verilog [current_project]
set_property board_part digilentinc.com:zybo:part0:1.0 [current_project]
set_property ip_repo_paths {
  /home/andreas/Desktop/computer_architecture/project/gitlab/rendVgaTmBoot
  /home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher
} [current_project]
set_property ip_output_repo /home/andreas/Desktop/computer_architecture/project/gitlab/TOP/TOP_v3/TOP_v3.cache/ip [current_project]
set_property ip_cache_permissions {read write} [current_project]
read_verilog -library xil_defaultlib /home/andreas/Desktop/computer_architecture/project/gitlab/TOP/TOP_v3/TOP_v3.srcs/sources_1/bd/design_1/hdl/design_1_wrapper.v
add_files /home/andreas/Desktop/computer_architecture/project/gitlab/TOP/TOP_v3/TOP_v3.srcs/sources_1/bd/design_1/design_1.bd
set_property used_in_implementation false [get_files -all /home/andreas/Desktop/computer_architecture/project/gitlab/TOP/TOP_v3/TOP_v3.srcs/sources_1/bd/design_1/ip/design_1_processing_system7_0_0/design_1_processing_system7_0_0.xdc]
set_property used_in_implementation false [get_files -all /home/andreas/Desktop/computer_architecture/project/gitlab/TOP/TOP_v3/TOP_v3.srcs/sources_1/bd/design_1/ip/design_1_axi_smc_1/bd_0/ip/ip_5/bd_6f02_s00a2s_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/andreas/Desktop/computer_architecture/project/gitlab/TOP/TOP_v3/TOP_v3.srcs/sources_1/bd/design_1/ip/design_1_axi_smc_1/bd_0/ip/ip_11/bd_6f02_m00s2a_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/andreas/Desktop/computer_architecture/project/gitlab/TOP/TOP_v3/TOP_v3.srcs/sources_1/bd/design_1/ip/design_1_axi_smc_1/bd_0/ip/ip_6/bd_6f02_sarn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/andreas/Desktop/computer_architecture/project/gitlab/TOP/TOP_v3/TOP_v3.srcs/sources_1/bd/design_1/ip/design_1_axi_smc_1/bd_0/ip/ip_7/bd_6f02_srn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/andreas/Desktop/computer_architecture/project/gitlab/TOP/TOP_v3/TOP_v3.srcs/sources_1/bd/design_1/ip/design_1_axi_smc_1/bd_0/ip/ip_8/bd_6f02_sawn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/andreas/Desktop/computer_architecture/project/gitlab/TOP/TOP_v3/TOP_v3.srcs/sources_1/bd/design_1/ip/design_1_axi_smc_1/bd_0/ip/ip_9/bd_6f02_swn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/andreas/Desktop/computer_architecture/project/gitlab/TOP/TOP_v3/TOP_v3.srcs/sources_1/bd/design_1/ip/design_1_axi_smc_1/bd_0/ip/ip_10/bd_6f02_sbn_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/andreas/Desktop/computer_architecture/project/gitlab/TOP/TOP_v3/TOP_v3.srcs/sources_1/bd/design_1/ip/design_1_axi_smc_1/bd_0/ip/ip_1/bd_6f02_psr_aclk_0_board.xdc]
set_property used_in_implementation false [get_files -all /home/andreas/Desktop/computer_architecture/project/gitlab/TOP/TOP_v3/TOP_v3.srcs/sources_1/bd/design_1/ip/design_1_axi_smc_1/bd_0/ip/ip_1/bd_6f02_psr_aclk_0.xdc]
set_property used_in_implementation false [get_files -all /home/andreas/Desktop/computer_architecture/project/gitlab/TOP/TOP_v3/TOP_v3.srcs/sources_1/bd/design_1/ip/design_1_axi_smc_1/ooc.xdc]
set_property used_in_implementation false [get_files -all /home/andreas/Desktop/computer_architecture/project/gitlab/TOP/TOP_v3/TOP_v3.srcs/sources_1/bd/design_1/ip/design_1_rst_ps7_0_50M_0/design_1_rst_ps7_0_50M_0_board.xdc]
set_property used_in_implementation false [get_files -all /home/andreas/Desktop/computer_architecture/project/gitlab/TOP/TOP_v3/TOP_v3.srcs/sources_1/bd/design_1/ip/design_1_rst_ps7_0_50M_0/design_1_rst_ps7_0_50M_0.xdc]
set_property used_in_implementation false [get_files -all /home/andreas/Desktop/computer_architecture/project/gitlab/TOP/TOP_v3/TOP_v3.srcs/sources_1/bd/design_1/ip/design_1_rst_ps7_0_50M_0/design_1_rst_ps7_0_50M_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/andreas/Desktop/computer_architecture/project/gitlab/TOP/TOP_v3/TOP_v3.srcs/sources_1/bd/design_1/ip/design_1_rendVgaTmBoot_0_0/booting.srcs/sources_1/ip/blk_mem_gen_0/blk_mem_gen_0_ooc.xdc]
set_property used_in_implementation false [get_files -all /home/andreas/Desktop/computer_architecture/project/gitlab/TOP/TOP_v3/TOP_v3.srcs/sources_1/bd/design_1/design_1_ooc.xdc]
set_property is_locked true [get_files /home/andreas/Desktop/computer_architecture/project/gitlab/TOP/TOP_v3/TOP_v3.srcs/sources_1/bd/design_1/design_1.bd]

# Mark all dcp files as not used in implementation to prevent them from being
# stitched into the results of this synthesis run. Any black boxes in the
# design are intentionally left as such for best results. Dcp files will be
# stitched into the design at a later time, either when this synthesis run is
# opened, or when it is stitched into a dependent implementation run.
foreach dcp [get_files -quiet -all -filter file_type=="Design\ Checkpoint"] {
  set_property used_in_implementation false $dcp
}
read_xdc /home/andreas/Desktop/computer_architecture/project/gitlab/TOP/TOP_v3/TOP_v3.srcs/constrs_1/imports/software/ZYBO_Master.xdc
set_property used_in_implementation false [get_files /home/andreas/Desktop/computer_architecture/project/gitlab/TOP/TOP_v3/TOP_v3.srcs/constrs_1/imports/software/ZYBO_Master.xdc]

read_xdc dont_touch.xdc
set_property used_in_implementation false [get_files dont_touch.xdc]

synth_design -top design_1_wrapper -part xc7z010clg400-1


write_checkpoint -force -noxdef design_1_wrapper.dcp

catch { report_utilization -file design_1_wrapper_utilization_synth.rpt -pb design_1_wrapper_utilization_synth.pb }
