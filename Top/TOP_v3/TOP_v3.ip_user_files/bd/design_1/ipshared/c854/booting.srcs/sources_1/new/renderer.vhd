----------------------------------------------------------------------------------
-- University: Politecnico di Torino
-- Authors: Gabriele Cazzato, Stefano Negri Merlo, Gabriele Monaco, Alexandru Valentin Onica, Gabriele Ponzio, Roberto Stagi
-- 
-- Create Date: 20/04/2017 11:02:05 AM
-- Design Name: TOP
-- Module Name: Renderer
-- Project Name: Computer Architecture Project 2017
-- Target Devices: Digilent ZYBO
-- Tool Versions: Vivado 2017.1
-- Description:
--
-- Dependencies:
-- * pixel_pack.vhd
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use work.pixel_pack.ALL;

entity renderer is
generic (
    MAP_NUM_TILES_X : NATURAL := 80;
    MAP_NUM_TILES_Y : NATURAL := 60;
    VIEW_NUM_TILES_X : NATURAL := 80/2;
    VIEW_NUM_TILES_Y : NATURAL := 60/2;
    TILE_NUM_BITS : NATURAL := 19;
    SPRITES_NUM : NATURAL := 128;
    SPRITE_NUM_PIXELS : NATURAL := 16*16;
    PIXEL_NUM_BITS : NATURAL := 6
);
port (
    addr_X : out INTEGER range 0 to MAP_NUM_TILES_X+1; --coords that are sent to the tilemap data queue to retrieve tile_id
    addr_Y : out INTEGER range 0 to MAP_NUM_TILES_Y+3;
    pixel_bus : out STD_LOGIC_VECTOR (15 downto 0);
    this_X : in INTEGER range 0 to MAP_NUM_TILES_X+1; --player position
    this_Y : in INTEGER range 0 to MAP_NUM_TILES_Y+3;
    mode : in STD_LOGIC; --whether to print the finder
    canput : in STD_LOGIC; --to set the color of the finder
    screen_X : in INTEGER range 0 to 900; --calculated inside the vga module
    screen_Y : in INTEGER range 0 to 530;
    tile_in : in STD_LOGIC_VECTOR (TILE_NUM_BITS-1 downto 0); --tile_in: value stored in the tilemap at addr_X, addr_Y
    output_enable : in STD_LOGIC;
    pixel_in : in STD_LOGIC_VECTOR (PIXEL_NUM_BITS-1 downto 0); --6 bit color
    fetching_sprites : in STD_LOGIC;
    clk: in STD_LOGIC;
    pixel_clk: in STD_LOGIC
);
end renderer;

architecture Behavioral of renderer is
component blk_mem_gen_0 is
port (
    clka : IN STD_LOGIC;
    wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    --addra : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
    addra : in STD_LOGIC_VECTOR(natural(ceil(log2(real(SPRITES_NUM*SPRITE_NUM_PIXELS))))-1 downto 0);
    --dina : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    dina : in STD_LOGIC_VECTOR(PIXEL_NUM_BITS-1 downto 0);
    clkb : IN STD_LOGIC;
    --addrb : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
    addrb : in STD_LOGIC_VECTOR(natural(ceil(log2(real(SPRITES_NUM*SPRITE_NUM_PIXELS))))-1 downto 0);
    --doutb : OUT STD_LOGIC_VECTOR(5 DOWNTO 0)
    doutb : out STD_LOGIC_VECTOR(PIXEL_NUM_BITS-1 downto 0)
);
end component;

--sprites bram
--signal addra : STD_LOGIC_VECTOR(14 downto 0) := (others => '0');
signal addra : STD_LOGIC_VECTOR(natural(ceil(log2(real(SPRITES_NUM*SPRITE_NUM_PIXELS))))-1 downto 0) := (others => '0');
--signal addrb : STD_LOGIC_VECTOR(14 downto 0) := (others => '0');
signal addrb : STD_LOGIC_VECTOR(natural(ceil(log2(real(SPRITES_NUM*SPRITE_NUM_PIXELS))))-1 downto 0) := (others => '0');
signal doutb : STD_LOGIC_VECTOR(5 downto 0);

--tileram
type tileram is array (0 to VIEW_NUM_TILES_X-1) of std_logic_vector(TILE_NUM_BITS-1 downto 0);
signal tiles : tileram := (others=>(others=>'0'));
attribute ram_style : string;
attribute ram_style of tiles : signal is "distributed";
--attribute ram_style of sprites_data : signal is "block";

signal isFinder : STD_LOGIC_VECTOR (1 downto 0) := (others=>'0'); --tell if i need to write the finder
constant TILES_IN_LINE : INTEGER range 0 to VIEW_NUM_TILES_X := 40;
constant LINES_IN_FRAME : INTEGER range 0 to 28 := 27;
constant LINES_IN_HUD_AND_FRAME : INTEGER range 0 to 31 := 30;
constant TILEMAP_HUD_START_MINUS_27 : INTEGER range 0 to 34 := 60-27;
constant H_PIXELS_MAX : INTEGER range 0 to 641 := 640;
constant v_PIXELS_MAX : INTEGER range 0 to 481 := 480;
constant V_PIXELS_IN_TILE : INTEGER range 0 to 16 := 16;
constant ground_sprite_id : INTEGER range 0 to SPRITES_NUM := 0; --this will probably change

--bnd part
signal boundary_X, boundary_Y : INTEGER range 0 to 63 := 0;
constant LeftX : NATURAL := 20;
constant RightX: NATURAL := 60;
constant TopY: NATURAL := 15;
constant BotY: NATURAL := 45;
constant centralRightX: NATURAL := (LeftX + RightX)/2;
constant centralBotY: NATURAL := (TopY+BotY)/2;
--signal sprite_x, sprite_x_rev, sprite_y, sprite_y_rev : INTEGER range 0 to 15 := 0;
--signal current_tile : INTEGER range 0 to 40 := 0;

begin
    sprites_bram : blk_mem_gen_0
    port map(
        clka => clk,
        wea(0) => fetching_sprites,
        addra => addra,
        dina => pixel_in,
        clkb => pixel_clk,
        addrb => addrb,
        doutb => doutb
    );

    sprites_write : process(clk)
    variable cnt : NATURAL range 0 to SPRITES_NUM*SPRITE_NUM_PIXELS := 0;    
    
    begin
        if rising_edge(clk) then
            if fetching_sprites = '1' and cnt < SPRITES_NUM*SPRITE_NUM_PIXELS then
                addra <= std_logic_vector(to_unsigned(cnt,addra'length));
                cnt := cnt+1;
            end if;
        end if;
    end process;

    --processes required to prepare the output buffer for output
    read_tile_from_map: process(clk)
    variable line_complete : STD_LOGIC := '0';
    variable tile_wrote : STD_LOGIC := '1';
    variable tile_column_write_counter, tile_row_write_counter: INTEGER range 0 to 50 := 0;
    
    begin
    if rising_edge(clk) then
        if tile_wrote = '0' and output_enable = '0' then
            tiles(tile_column_write_counter) <= tile_in; --fetch from tilemap
            tile_wrote := '1';
            tile_column_write_counter := tile_column_write_counter+1; --inc on last time it is used
            if tile_column_write_counter = TILES_IN_LINE then
                line_complete := '1';
            end if;
        end if;
        if output_enable = '1' then
            if (screen_Y mod 16) = 15 then --reinit only when line has been written
                line_complete := '0';
                tile_column_write_counter :=0;
                tile_row_write_counter := (screen_Y+1)/16;
                if tile_row_write_counter = 30 then
                    tile_row_write_counter :=0;
                    if this_x < LeftX then --reinit the boundaries
                        boundary_X <= 0;
                    elsif this_x > RightX then
                        boundary_X <= centralRightX;
                    else
                        boundary_X <= this_x-LeftX;
                    end if;
                    if this_y < TopY then
                       boundary_Y <= 0;
                    elsif this_y > BotY then
                        boundary_Y <= centralBotY;
                    else
                        boundary_Y <= this_y-TopY;
                    end if;
                end if;
            end if;
        elsif screen_X > 640 and (screen_Y < 480 or screen_Y > 523) then --only on wanted blanking time
            if line_complete ='0' and tile_wrote ='1' then
                --if tile_row_write_counter < LINES_IN_FRAME then
                if tile_row_write_counter < LINES_IN_HUD_AND_FRAME then
                    addr_X <= boundary_X+tile_column_write_counter;
                    addr_Y <= boundary_Y+tile_row_write_counter;
                    --finder part
                    if this_Y = boundary_Y+tile_row_write_counter then
                        isFinder <= "10"; --first line of finder
                    elsif this_Y = boundary_Y+tile_row_write_counter-1 then
                        isFinder <= "01"; --second line of finder
                    else
                        isFinder <= "00"; --not inside the finder
                    end if;
                    tile_wrote := '0';
                    --elsif tile_row_write_counter < LINES_IN_HUD_AND_FRAME then
                        --addr_X <= tile_column_write_counter;
                        --addr_Y <= TILEMAP_HUD_START_MINUS_27+tile_row_write_counter;
                        --addr_change <= '1';
                end if;
            end if;
        elsif screen_Y > 480 then --start with the next (0 is done in the end)
        end if;
    end if;
    end process;

    out_creator: process(pixel_clk)
    variable pixel : std_logic_vector (15 downto 0) := (others=>'0');
    variable pixel_id : INTEGER range 0 to (SPRITES_NUM-1)*SPRITE_NUM_PIXELS;
    variable sprite_x, sprite_x_rev, sprite_y, sprite_y_rev : INTEGER range 0 to 15 := 0;
    variable current_tile : INTEGER range 0 to VIEW_NUM_TILES_X := 0;
    variable temp_tile : std_logic_vector (TILE_NUM_BITS-1 downto 0);
    
    begin
    --output_enable means that output is required. hsync and vsync are handled through internal counters, but not the blanking period
    if output_enable = '1' then
        --with dual port blockram and 2 clocks
        if rising_edge(pixel_clk) then
            if screen_X = 0 then
                current_tile := 0;
            end if;
            pixel_id := to_integer(unsigned(tiles(current_tile)(6 downto 0)))*SPRITE_NUM_PIXELS;
            --if the sprite is meant to be flipped, I do so by subtracting the x position to 15
            sprite_x := screen_X mod 16; --correct shift
            sprite_y := screen_Y mod 16;
            if tiles(current_tile)(11) = '1' then
                sprite_y_rev := 15-sprite_y;
            end if;
            if tiles(current_tile)(10) = '1' then --reverse
                sprite_x_rev := 15-sprite_x; --sprite_x is used as index, should not have strange values
            end if;
            if tiles(current_tile)(11) = '0' then
                if tiles(current_tile)(10) = '1' then
                    addrb <= std_logic_vector(to_unsigned(pixel_id + sprite_x_rev+16*sprite_y,15));
                else
                    addrb <= std_logic_vector(to_unsigned(pixel_id + sprite_x+16*sprite_y,15));
                end if;
            else
                if tiles(current_tile)(10) = '1' then
                    addrb <= std_logic_vector(to_unsigned(pixel_id + sprite_x_rev+16*sprite_y_rev,15));
                else
                    addrb <= std_logic_vector(to_unsigned(pixel_id + sprite_x+16*sprite_y_rev,15));
                end if;
            end if;
        end if;

        --finder part
        if falling_edge(pixel_clk) then
            if screen_X = 0 then
                current_tile := 0;
            end if;
            pixel := b6to16(doutb);
            if mode = '0' then --needs to print the finder
                if isFinder = "10" then --print the first line
                    if this_X = current_tile+boundary_X then --in the first column
                        if (sprite_x < 9 and sprite_y < 3) or (sprite_x < 3 and sprite_y < 9) then
                            if canput = '1' then
                                pixel := "1111110000000000"; --set green
                            else
                                pixel := "0000000000011111"; --set red
                            end if;
                        end if;
                    elsif this_X = current_tile+boundary_X-1 then --in the second column
                        if (sprite_x > 6 and sprite_y < 3) or (sprite_x > 12 and sprite_y < 9) then
                            if canput = '1' then
                                pixel := "1111110000000000"; --set green
                            else
                                pixel := "0000000000011111"; --set red
                            end if;
                        end if;
                    end if;
                elsif isFinder = "01" then --second line
                    if this_X = current_tile+boundary_X then --in the first column
                        if (sprite_x < 9 and sprite_y > 12) or (sprite_x < 3 and sprite_y > 6) then
                            if canput = '1' then
                                pixel := "1111110000000000"; --set green
                            else
                                pixel := "0000000000011111"; --set red
                            end if;
                        end if;
                    elsif this_X = current_tile+boundary_X-1 then --in the second column
                        if (sprite_x > 6 and sprite_y > 12) or (sprite_x > 12 and sprite_y > 6) then
                            if canput = '1' then
                                pixel := "1111110000000000"; --set green
                            else
                                pixel := "0000000000011111"; --set red
                            end if;
                        end if;
                    end if;
                end if;
            end if;
            if sprite_x = 15  then --only to reinitialize
                current_tile := current_tile+1;
            end if;
            pixel_bus <= pixel;
        end if;
        --elsif screen_X > 700 then
        --current_tile := 0;
        --curr_tile <= current_tile; --debug
    end if;
end process;
end Behavioral;