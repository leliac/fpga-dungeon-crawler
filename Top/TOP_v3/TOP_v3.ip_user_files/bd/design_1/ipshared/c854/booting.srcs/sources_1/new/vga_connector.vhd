----------------------------------------------------------------------------------
-- University: Politecnico di Torino
-- Authors: Gabriele Cazzato, Stefano Negri Merlo, Gabriele Monaco, Alexandru Valentin Onica, Gabriele Ponzio, Roberto Stagi
-- 
-- Create Date: 20/04/2017 11:02:05 AM
-- Design Name: TOP
-- Module Name: VGA Connector
-- Project Name: Computer Architecture Project 2017
-- Target Devices: Digilent ZYBO
-- Tool Versions: Vivado 2017.1
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity vga_connector is
port (
    pixel_bus : in STD_LOGIC_VECTOR (15 downto 0);
    clock : in STD_LOGIC;
    vga_r : out STD_LOGIC_VECTOR (4 downto 0);
    vga_g : out STD_LOGIC_VECTOR (5 downto 0);
    vga_b : out STD_LOGIC_VECTOR (4 downto 0);
    vga_hs : out STD_LOGIC;
    vga_vs : out STD_LOGIC;
    h_cnt : out INTEGER range 0 to 900; --can be used by other modules
    v_cnt : out INTEGER range 0 to 530;
    render_enable: out STD_LOGIC;
    anim_enable: out STD_LOGIC;
    fetch_complete : in STD_LOGIC
);
end vga_connector;

architecture Behavioral of vga_connector is
begin
    vga_out : process(clock)
    variable HSYNC: STD_LOGIC := '1';
    variable VSYNC: STD_LOGIC := '1';
    variable x : INTEGER range 0 to 900 := 0; 
    variable y : INTEGER range 0 to 530 := 0;
    variable cnt : INTEGER range 0 to 3 := 0;
    begin
        vga_hs <=HSYNC;
        vga_vs <=VSYNC;
    
        --if rising_edge(clock) then -- the clock passed must be correct (40ns period)
        if rising_edge(clock) and fetch_complete = '1' then -- the clock passed must be correct (40ns period)          
            if cnt > 2 then
                --start pixel out
                if x < 640 and y < 480 then --use color inside the screen
                    render_enable <= '1';
                    vga_r <= pixel_bus(4 downto 0);
                    vga_b <= pixel_bus(9 downto 5);
                    vga_g <= pixel_bus(15 downto 10);
--              elsif x = 799 then
--                  render_enable <= '1'; --enable immediately before starting     
                else 
                --blanks pixels if they aren't part of the row
                    render_enable <= '0';
                    vga_r <= (others=>'0');
                    vga_b <= (others=>'0');
                    vga_g <= (others=>'0');
                end if;
                --start sync block
                --640 pixels in a line + 16 porch + 96 sync + 48 backporch = 800 total
                --480 rows + 10 porch + 2 sync + 33 back = 525 total
                if x >= 656 and x < 752 then  --send sync during the proper period
                    HSYNC := '0';
                else
                    HSYNC := '1';
                end if;          
                if y > 489 and y < 492 then  --send sync during the proper period
                    VSYNC := '0'; 
                else
                    VSYNC := '1';
                end if;
            
                --enable for animations
                if y > 480 then
                    anim_enable <= '1';
                else
                    anim_enable <= '0';
                end if;
                h_cnt <= x;
                v_cnt <= y;
                --counters block
                x := x+1;          
                if x = 800 then --very end of row
                    x := 0;
                    y := y + 1;
                end if;          
                if y = 525 then --very end of screen
                    y := 0;
                end if;
            else
                cnt := cnt+1;
                render_enable <= '0';
                anim_enable <= '0';
                vga_r <= (others=>'0');
                vga_b <= (others=>'0');
                vga_g <= (others=>'0');
            end if;
        end if;
    end process;
end Behavioral;