//Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2017.1 (lin64) Build 1846317 Fri Apr 14 18:54:47 MDT 2017
//Date        : Sun Jun 18 20:12:08 2017
//Host        : surprise running 64-bit Linux Mint 18.1 Serena
//Command     : generate_target design_1.bd
//Design      : design_1
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CORE_GENERATION_INFO = "design_1,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_1,x_ipVersion=1.00.a,x_ipLanguage=VERILOG,numBlks=5,numReposBlks=5,numNonXlnxBlks=2,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=0,numPkgbdBlks=0,bdsource=USER,da_axi4_cnt=2,da_clkrst_cnt=4,da_ps7_cnt=1,synth_mode=OOC_per_IP}" *) (* HW_HANDOFF = "design_1.hwdef" *) 
module design_1
   (DDR_addr,
    DDR_ba,
    DDR_cas_n,
    DDR_ck_n,
    DDR_ck_p,
    DDR_cke,
    DDR_cs_n,
    DDR_dm,
    DDR_dq,
    DDR_dqs_n,
    DDR_dqs_p,
    DDR_odt,
    DDR_ras_n,
    DDR_reset_n,
    DDR_we_n,
    FIXED_IO_ddr_vrn,
    FIXED_IO_ddr_vrp,
    FIXED_IO_mio,
    FIXED_IO_ps_clk,
    FIXED_IO_ps_porb,
    FIXED_IO_ps_srstb,
    led0,
    led1,
    led2,
    led3,
    sw,
    vga_b,
    vga_g,
    vga_hs,
    vga_r,
    vga_vs);
  inout [14:0]DDR_addr;
  inout [2:0]DDR_ba;
  inout DDR_cas_n;
  inout DDR_ck_n;
  inout DDR_ck_p;
  inout DDR_cke;
  inout DDR_cs_n;
  inout [3:0]DDR_dm;
  inout [31:0]DDR_dq;
  inout [3:0]DDR_dqs_n;
  inout [3:0]DDR_dqs_p;
  inout DDR_odt;
  inout DDR_ras_n;
  inout DDR_reset_n;
  inout DDR_we_n;
  inout FIXED_IO_ddr_vrn;
  inout FIXED_IO_ddr_vrp;
  inout [53:0]FIXED_IO_mio;
  inout FIXED_IO_ps_clk;
  inout FIXED_IO_ps_porb;
  inout FIXED_IO_ps_srstb;
  output led0;
  output led1;
  output led2;
  output led3;
  input [3:0]sw;
  output [4:0]vga_b;
  output [5:0]vga_g;
  output vga_hs;
  output [4:0]vga_r;
  output vga_vs;

  wire [31:0]axiDataFetcher_0_dataPacket;
  wire axiDataFetcher_0_fetching;
  wire [31:0]axiDataFetcher_0_m_axi_ARADDR;
  wire [1:0]axiDataFetcher_0_m_axi_ARBURST;
  wire [3:0]axiDataFetcher_0_m_axi_ARCACHE;
  wire [7:0]axiDataFetcher_0_m_axi_ARLEN;
  wire [2:0]axiDataFetcher_0_m_axi_ARPROT;
  wire axiDataFetcher_0_m_axi_ARREADY;
  wire [2:0]axiDataFetcher_0_m_axi_ARSIZE;
  wire axiDataFetcher_0_m_axi_ARVALID;
  wire [31:0]axiDataFetcher_0_m_axi_AWADDR;
  wire [1:0]axiDataFetcher_0_m_axi_AWBURST;
  wire [3:0]axiDataFetcher_0_m_axi_AWCACHE;
  wire [7:0]axiDataFetcher_0_m_axi_AWLEN;
  wire [2:0]axiDataFetcher_0_m_axi_AWPROT;
  wire axiDataFetcher_0_m_axi_AWREADY;
  wire [2:0]axiDataFetcher_0_m_axi_AWSIZE;
  wire axiDataFetcher_0_m_axi_AWVALID;
  wire axiDataFetcher_0_m_axi_BREADY;
  wire [1:0]axiDataFetcher_0_m_axi_BRESP;
  wire axiDataFetcher_0_m_axi_BVALID;
  wire [31:0]axiDataFetcher_0_m_axi_RDATA;
  wire axiDataFetcher_0_m_axi_RLAST;
  wire axiDataFetcher_0_m_axi_RREADY;
  wire [1:0]axiDataFetcher_0_m_axi_RRESP;
  wire axiDataFetcher_0_m_axi_RVALID;
  wire [31:0]axiDataFetcher_0_m_axi_WDATA;
  wire axiDataFetcher_0_m_axi_WLAST;
  wire axiDataFetcher_0_m_axi_WREADY;
  wire [7:0]axiDataFetcher_0_m_axi_WSTRB;
  wire axiDataFetcher_0_m_axi_WVALID;
  wire [31:0]axi_smc_M00_AXI_ARADDR;
  wire [1:0]axi_smc_M00_AXI_ARBURST;
  wire [3:0]axi_smc_M00_AXI_ARCACHE;
  wire [3:0]axi_smc_M00_AXI_ARLEN;
  wire [1:0]axi_smc_M00_AXI_ARLOCK;
  wire [2:0]axi_smc_M00_AXI_ARPROT;
  wire [3:0]axi_smc_M00_AXI_ARQOS;
  wire axi_smc_M00_AXI_ARREADY;
  wire [2:0]axi_smc_M00_AXI_ARSIZE;
  wire axi_smc_M00_AXI_ARVALID;
  wire [31:0]axi_smc_M00_AXI_AWADDR;
  wire [1:0]axi_smc_M00_AXI_AWBURST;
  wire [3:0]axi_smc_M00_AXI_AWCACHE;
  wire [3:0]axi_smc_M00_AXI_AWLEN;
  wire [1:0]axi_smc_M00_AXI_AWLOCK;
  wire [2:0]axi_smc_M00_AXI_AWPROT;
  wire [3:0]axi_smc_M00_AXI_AWQOS;
  wire axi_smc_M00_AXI_AWREADY;
  wire [2:0]axi_smc_M00_AXI_AWSIZE;
  wire axi_smc_M00_AXI_AWVALID;
  wire axi_smc_M00_AXI_BREADY;
  wire [1:0]axi_smc_M00_AXI_BRESP;
  wire axi_smc_M00_AXI_BVALID;
  wire [63:0]axi_smc_M00_AXI_RDATA;
  wire axi_smc_M00_AXI_RLAST;
  wire axi_smc_M00_AXI_RREADY;
  wire [1:0]axi_smc_M00_AXI_RRESP;
  wire axi_smc_M00_AXI_RVALID;
  wire [63:0]axi_smc_M00_AXI_WDATA;
  wire axi_smc_M00_AXI_WLAST;
  wire axi_smc_M00_AXI_WREADY;
  wire [7:0]axi_smc_M00_AXI_WSTRB;
  wire axi_smc_M00_AXI_WVALID;
  wire [14:0]processing_system7_0_DDR_ADDR;
  wire [2:0]processing_system7_0_DDR_BA;
  wire processing_system7_0_DDR_CAS_N;
  wire processing_system7_0_DDR_CKE;
  wire processing_system7_0_DDR_CK_N;
  wire processing_system7_0_DDR_CK_P;
  wire processing_system7_0_DDR_CS_N;
  wire [3:0]processing_system7_0_DDR_DM;
  wire [31:0]processing_system7_0_DDR_DQ;
  wire [3:0]processing_system7_0_DDR_DQS_N;
  wire [3:0]processing_system7_0_DDR_DQS_P;
  wire processing_system7_0_DDR_ODT;
  wire processing_system7_0_DDR_RAS_N;
  wire processing_system7_0_DDR_RESET_N;
  wire processing_system7_0_DDR_WE_N;
  wire processing_system7_0_FCLK_CLK0;
  wire processing_system7_0_FCLK_CLK1;
  wire processing_system7_0_FCLK_RESET0_N;
  wire processing_system7_0_FIXED_IO_DDR_VRN;
  wire processing_system7_0_FIXED_IO_DDR_VRP;
  wire [53:0]processing_system7_0_FIXED_IO_MIO;
  wire processing_system7_0_FIXED_IO_PS_CLK;
  wire processing_system7_0_FIXED_IO_PS_PORB;
  wire processing_system7_0_FIXED_IO_PS_SRSTB;
  wire [7:0]rendVgaTmBoot_0_data_id;
  wire rendVgaTmBoot_0_data_type;
  wire rendVgaTmBoot_0_fetch_start;
  wire rendVgaTmBoot_0_led0;
  wire rendVgaTmBoot_0_led1;
  wire rendVgaTmBoot_0_led2;
  wire rendVgaTmBoot_0_led3;
  wire [4:0]rendVgaTmBoot_0_vga_b;
  wire [5:0]rendVgaTmBoot_0_vga_g;
  wire rendVgaTmBoot_0_vga_hs;
  wire [4:0]rendVgaTmBoot_0_vga_r;
  wire rendVgaTmBoot_0_vga_vs;
  wire [0:0]rst_ps7_0_50M_peripheral_aresetn;
  wire [3:0]sw_1;

  assign led0 = rendVgaTmBoot_0_led0;
  assign led1 = rendVgaTmBoot_0_led1;
  assign led2 = rendVgaTmBoot_0_led2;
  assign led3 = rendVgaTmBoot_0_led3;
  assign sw_1 = sw[3:0];
  assign vga_b[4:0] = rendVgaTmBoot_0_vga_b;
  assign vga_g[5:0] = rendVgaTmBoot_0_vga_g;
  assign vga_hs = rendVgaTmBoot_0_vga_hs;
  assign vga_r[4:0] = rendVgaTmBoot_0_vga_r;
  assign vga_vs = rendVgaTmBoot_0_vga_vs;
  design_1_axiDataFetcher_0_1 axiDataFetcher_0
       (.dataId(rendVgaTmBoot_0_data_id),
        .dataPacket(axiDataFetcher_0_dataPacket),
        .dataType(rendVgaTmBoot_0_data_type),
        .ena(rendVgaTmBoot_0_fetch_start),
        .fetching(axiDataFetcher_0_fetching),
        .m_axi_aclk(processing_system7_0_FCLK_CLK0),
        .m_axi_araddr(axiDataFetcher_0_m_axi_ARADDR),
        .m_axi_arburst(axiDataFetcher_0_m_axi_ARBURST),
        .m_axi_arcache(axiDataFetcher_0_m_axi_ARCACHE),
        .m_axi_aresetn(rst_ps7_0_50M_peripheral_aresetn),
        .m_axi_arlen(axiDataFetcher_0_m_axi_ARLEN),
        .m_axi_arprot(axiDataFetcher_0_m_axi_ARPROT),
        .m_axi_arready(axiDataFetcher_0_m_axi_ARREADY),
        .m_axi_arsize(axiDataFetcher_0_m_axi_ARSIZE),
        .m_axi_arvalid(axiDataFetcher_0_m_axi_ARVALID),
        .m_axi_awaddr(axiDataFetcher_0_m_axi_AWADDR),
        .m_axi_awburst(axiDataFetcher_0_m_axi_AWBURST),
        .m_axi_awcache(axiDataFetcher_0_m_axi_AWCACHE),
        .m_axi_awlen(axiDataFetcher_0_m_axi_AWLEN),
        .m_axi_awprot(axiDataFetcher_0_m_axi_AWPROT),
        .m_axi_awready(axiDataFetcher_0_m_axi_AWREADY),
        .m_axi_awsize(axiDataFetcher_0_m_axi_AWSIZE),
        .m_axi_awvalid(axiDataFetcher_0_m_axi_AWVALID),
        .m_axi_bready(axiDataFetcher_0_m_axi_BREADY),
        .m_axi_bresp(axiDataFetcher_0_m_axi_BRESP),
        .m_axi_bvalid(axiDataFetcher_0_m_axi_BVALID),
        .m_axi_rdata(axiDataFetcher_0_m_axi_RDATA),
        .m_axi_rlast(axiDataFetcher_0_m_axi_RLAST),
        .m_axi_rready(axiDataFetcher_0_m_axi_RREADY),
        .m_axi_rresp(axiDataFetcher_0_m_axi_RRESP),
        .m_axi_rvalid(axiDataFetcher_0_m_axi_RVALID),
        .m_axi_wdata(axiDataFetcher_0_m_axi_WDATA),
        .m_axi_wlast(axiDataFetcher_0_m_axi_WLAST),
        .m_axi_wready(axiDataFetcher_0_m_axi_WREADY),
        .m_axi_wstrb(axiDataFetcher_0_m_axi_WSTRB),
        .m_axi_wvalid(axiDataFetcher_0_m_axi_WVALID));
  design_1_axi_smc_1 axi_smc
       (.M00_AXI_araddr(axi_smc_M00_AXI_ARADDR),
        .M00_AXI_arburst(axi_smc_M00_AXI_ARBURST),
        .M00_AXI_arcache(axi_smc_M00_AXI_ARCACHE),
        .M00_AXI_arlen(axi_smc_M00_AXI_ARLEN),
        .M00_AXI_arlock(axi_smc_M00_AXI_ARLOCK),
        .M00_AXI_arprot(axi_smc_M00_AXI_ARPROT),
        .M00_AXI_arqos(axi_smc_M00_AXI_ARQOS),
        .M00_AXI_arready(axi_smc_M00_AXI_ARREADY),
        .M00_AXI_arsize(axi_smc_M00_AXI_ARSIZE),
        .M00_AXI_arvalid(axi_smc_M00_AXI_ARVALID),
        .M00_AXI_awaddr(axi_smc_M00_AXI_AWADDR),
        .M00_AXI_awburst(axi_smc_M00_AXI_AWBURST),
        .M00_AXI_awcache(axi_smc_M00_AXI_AWCACHE),
        .M00_AXI_awlen(axi_smc_M00_AXI_AWLEN),
        .M00_AXI_awlock(axi_smc_M00_AXI_AWLOCK),
        .M00_AXI_awprot(axi_smc_M00_AXI_AWPROT),
        .M00_AXI_awqos(axi_smc_M00_AXI_AWQOS),
        .M00_AXI_awready(axi_smc_M00_AXI_AWREADY),
        .M00_AXI_awsize(axi_smc_M00_AXI_AWSIZE),
        .M00_AXI_awvalid(axi_smc_M00_AXI_AWVALID),
        .M00_AXI_bready(axi_smc_M00_AXI_BREADY),
        .M00_AXI_bresp(axi_smc_M00_AXI_BRESP),
        .M00_AXI_bvalid(axi_smc_M00_AXI_BVALID),
        .M00_AXI_rdata(axi_smc_M00_AXI_RDATA),
        .M00_AXI_rlast(axi_smc_M00_AXI_RLAST),
        .M00_AXI_rready(axi_smc_M00_AXI_RREADY),
        .M00_AXI_rresp(axi_smc_M00_AXI_RRESP),
        .M00_AXI_rvalid(axi_smc_M00_AXI_RVALID),
        .M00_AXI_wdata(axi_smc_M00_AXI_WDATA),
        .M00_AXI_wlast(axi_smc_M00_AXI_WLAST),
        .M00_AXI_wready(axi_smc_M00_AXI_WREADY),
        .M00_AXI_wstrb(axi_smc_M00_AXI_WSTRB),
        .M00_AXI_wvalid(axi_smc_M00_AXI_WVALID),
        .S00_AXI_araddr(axiDataFetcher_0_m_axi_ARADDR),
        .S00_AXI_arburst(axiDataFetcher_0_m_axi_ARBURST),
        .S00_AXI_arcache(axiDataFetcher_0_m_axi_ARCACHE),
        .S00_AXI_arlen(axiDataFetcher_0_m_axi_ARLEN),
        .S00_AXI_arlock(1'b0),
        .S00_AXI_arprot(axiDataFetcher_0_m_axi_ARPROT),
        .S00_AXI_arqos({1'b0,1'b0,1'b0,1'b0}),
        .S00_AXI_arready(axiDataFetcher_0_m_axi_ARREADY),
        .S00_AXI_arsize(axiDataFetcher_0_m_axi_ARSIZE),
        .S00_AXI_arvalid(axiDataFetcher_0_m_axi_ARVALID),
        .S00_AXI_awaddr(axiDataFetcher_0_m_axi_AWADDR),
        .S00_AXI_awburst(axiDataFetcher_0_m_axi_AWBURST),
        .S00_AXI_awcache(axiDataFetcher_0_m_axi_AWCACHE),
        .S00_AXI_awlen(axiDataFetcher_0_m_axi_AWLEN),
        .S00_AXI_awlock(1'b0),
        .S00_AXI_awprot(axiDataFetcher_0_m_axi_AWPROT),
        .S00_AXI_awqos({1'b0,1'b0,1'b0,1'b0}),
        .S00_AXI_awready(axiDataFetcher_0_m_axi_AWREADY),
        .S00_AXI_awsize(axiDataFetcher_0_m_axi_AWSIZE),
        .S00_AXI_awvalid(axiDataFetcher_0_m_axi_AWVALID),
        .S00_AXI_bready(axiDataFetcher_0_m_axi_BREADY),
        .S00_AXI_bresp(axiDataFetcher_0_m_axi_BRESP),
        .S00_AXI_bvalid(axiDataFetcher_0_m_axi_BVALID),
        .S00_AXI_rdata(axiDataFetcher_0_m_axi_RDATA),
        .S00_AXI_rlast(axiDataFetcher_0_m_axi_RLAST),
        .S00_AXI_rready(axiDataFetcher_0_m_axi_RREADY),
        .S00_AXI_rresp(axiDataFetcher_0_m_axi_RRESP),
        .S00_AXI_rvalid(axiDataFetcher_0_m_axi_RVALID),
        .S00_AXI_wdata(axiDataFetcher_0_m_axi_WDATA),
        .S00_AXI_wlast(axiDataFetcher_0_m_axi_WLAST),
        .S00_AXI_wready(axiDataFetcher_0_m_axi_WREADY),
        .S00_AXI_wstrb(axiDataFetcher_0_m_axi_WSTRB[3:0]),
        .S00_AXI_wvalid(axiDataFetcher_0_m_axi_WVALID),
        .aclk(processing_system7_0_FCLK_CLK0),
        .aresetn(rst_ps7_0_50M_peripheral_aresetn));
  design_1_processing_system7_0_0 processing_system7_0
       (.DDR_Addr(DDR_addr[14:0]),
        .DDR_BankAddr(DDR_ba[2:0]),
        .DDR_CAS_n(DDR_cas_n),
        .DDR_CKE(DDR_cke),
        .DDR_CS_n(DDR_cs_n),
        .DDR_Clk(DDR_ck_p),
        .DDR_Clk_n(DDR_ck_n),
        .DDR_DM(DDR_dm[3:0]),
        .DDR_DQ(DDR_dq[31:0]),
        .DDR_DQS(DDR_dqs_p[3:0]),
        .DDR_DQS_n(DDR_dqs_n[3:0]),
        .DDR_DRSTB(DDR_reset_n),
        .DDR_ODT(DDR_odt),
        .DDR_RAS_n(DDR_ras_n),
        .DDR_VRN(FIXED_IO_ddr_vrn),
        .DDR_VRP(FIXED_IO_ddr_vrp),
        .DDR_WEB(DDR_we_n),
        .ENET0_MDIO_I(1'b0),
        .FCLK_CLK0(processing_system7_0_FCLK_CLK0),
        .FCLK_CLK1(processing_system7_0_FCLK_CLK1),
        .FCLK_RESET0_N(processing_system7_0_FCLK_RESET0_N),
        .MIO(FIXED_IO_mio[53:0]),
        .M_AXI_GP0_ACLK(processing_system7_0_FCLK_CLK0),
        .M_AXI_GP0_ARREADY(1'b0),
        .M_AXI_GP0_AWREADY(1'b0),
        .M_AXI_GP0_BID({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .M_AXI_GP0_BRESP({1'b0,1'b0}),
        .M_AXI_GP0_BVALID(1'b0),
        .M_AXI_GP0_RDATA({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .M_AXI_GP0_RID({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .M_AXI_GP0_RLAST(1'b0),
        .M_AXI_GP0_RRESP({1'b0,1'b0}),
        .M_AXI_GP0_RVALID(1'b0),
        .M_AXI_GP0_WREADY(1'b0),
        .PS_CLK(FIXED_IO_ps_clk),
        .PS_PORB(FIXED_IO_ps_porb),
        .PS_SRSTB(FIXED_IO_ps_srstb),
        .SDIO0_WP(1'b0),
        .S_AXI_HP0_ACLK(processing_system7_0_FCLK_CLK0),
        .S_AXI_HP0_ARADDR(axi_smc_M00_AXI_ARADDR),
        .S_AXI_HP0_ARBURST(axi_smc_M00_AXI_ARBURST),
        .S_AXI_HP0_ARCACHE(axi_smc_M00_AXI_ARCACHE),
        .S_AXI_HP0_ARID({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .S_AXI_HP0_ARLEN(axi_smc_M00_AXI_ARLEN),
        .S_AXI_HP0_ARLOCK(axi_smc_M00_AXI_ARLOCK),
        .S_AXI_HP0_ARPROT(axi_smc_M00_AXI_ARPROT),
        .S_AXI_HP0_ARQOS(axi_smc_M00_AXI_ARQOS),
        .S_AXI_HP0_ARREADY(axi_smc_M00_AXI_ARREADY),
        .S_AXI_HP0_ARSIZE(axi_smc_M00_AXI_ARSIZE),
        .S_AXI_HP0_ARVALID(axi_smc_M00_AXI_ARVALID),
        .S_AXI_HP0_AWADDR(axi_smc_M00_AXI_AWADDR),
        .S_AXI_HP0_AWBURST(axi_smc_M00_AXI_AWBURST),
        .S_AXI_HP0_AWCACHE(axi_smc_M00_AXI_AWCACHE),
        .S_AXI_HP0_AWID({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .S_AXI_HP0_AWLEN(axi_smc_M00_AXI_AWLEN),
        .S_AXI_HP0_AWLOCK(axi_smc_M00_AXI_AWLOCK),
        .S_AXI_HP0_AWPROT(axi_smc_M00_AXI_AWPROT),
        .S_AXI_HP0_AWQOS(axi_smc_M00_AXI_AWQOS),
        .S_AXI_HP0_AWREADY(axi_smc_M00_AXI_AWREADY),
        .S_AXI_HP0_AWSIZE(axi_smc_M00_AXI_AWSIZE),
        .S_AXI_HP0_AWVALID(axi_smc_M00_AXI_AWVALID),
        .S_AXI_HP0_BREADY(axi_smc_M00_AXI_BREADY),
        .S_AXI_HP0_BRESP(axi_smc_M00_AXI_BRESP),
        .S_AXI_HP0_BVALID(axi_smc_M00_AXI_BVALID),
        .S_AXI_HP0_RDATA(axi_smc_M00_AXI_RDATA),
        .S_AXI_HP0_RDISSUECAP1_EN(1'b0),
        .S_AXI_HP0_RLAST(axi_smc_M00_AXI_RLAST),
        .S_AXI_HP0_RREADY(axi_smc_M00_AXI_RREADY),
        .S_AXI_HP0_RRESP(axi_smc_M00_AXI_RRESP),
        .S_AXI_HP0_RVALID(axi_smc_M00_AXI_RVALID),
        .S_AXI_HP0_WDATA(axi_smc_M00_AXI_WDATA),
        .S_AXI_HP0_WID({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .S_AXI_HP0_WLAST(axi_smc_M00_AXI_WLAST),
        .S_AXI_HP0_WREADY(axi_smc_M00_AXI_WREADY),
        .S_AXI_HP0_WRISSUECAP1_EN(1'b0),
        .S_AXI_HP0_WSTRB(axi_smc_M00_AXI_WSTRB),
        .S_AXI_HP0_WVALID(axi_smc_M00_AXI_WVALID),
        .USB0_VBUS_PWRFAULT(1'b0));
  design_1_rendVgaTmBoot_0_0 rendVgaTmBoot_0
       (.clk(processing_system7_0_FCLK_CLK0),
        .data_id(rendVgaTmBoot_0_data_id),
        .data_type(rendVgaTmBoot_0_data_type),
        .fetch_start(rendVgaTmBoot_0_fetch_start),
        .fetching(axiDataFetcher_0_fetching),
        .led0(rendVgaTmBoot_0_led0),
        .led1(rendVgaTmBoot_0_led1),
        .led2(rendVgaTmBoot_0_led2),
        .led3(rendVgaTmBoot_0_led3),
        .packet_in(axiDataFetcher_0_dataPacket),
        .pixel_clk(processing_system7_0_FCLK_CLK1),
        .sw(sw_1),
        .vga_b(rendVgaTmBoot_0_vga_b),
        .vga_g(rendVgaTmBoot_0_vga_g),
        .vga_hs(rendVgaTmBoot_0_vga_hs),
        .vga_r(rendVgaTmBoot_0_vga_r),
        .vga_vs(rendVgaTmBoot_0_vga_vs));
  design_1_rst_ps7_0_50M_0 rst_ps7_0_50M
       (.aux_reset_in(1'b1),
        .dcm_locked(1'b1),
        .ext_reset_in(processing_system7_0_FCLK_RESET0_N),
        .mb_debug_sys_rst(1'b0),
        .peripheral_aresetn(rst_ps7_0_50M_peripheral_aresetn),
        .slowest_sync_clk(processing_system7_0_FCLK_CLK0));
endmodule
