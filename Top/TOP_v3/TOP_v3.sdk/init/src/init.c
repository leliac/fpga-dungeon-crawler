/*
 * init.c
 *
 *  Created on: May 17, 2017
 *      Author: Gabriele Cazzato
 */


#include <stdio.h>
#include "ff.h" // FatFs library

/* Assuming 1 byte per destination address */

#define DEBUG 0
#define DATA_SRC_DRIVE "0:/"
//#define DATA_DEST_ADDR 0x01000000 // was 0x00400000

#define MAPS_NUM 128
#define MAP_NUM_TILES 80*60
#define TILE_NUM_BYTES 4 // Includes non-significant bits
#define MAPS_FILE_NAME "maps.bin"
#define MAPS_DEST_ADDR 0x01000000 // was DATA_DEST_ADDR

#define SPRITES_NUM 128
#define SPRITE_NUM_PIXELS 16*16
#define PIXEL_NUM_BYTES 4 // Includes non-significant bits
#define SPRITES_FILE_NAME "sprites.bin"
#define SPRITES_DEST_ADDR 0x01800000 // was DATA_DEST_ADDR+MAPS_NUM*MAP_NUM_TILES

int transferData(char *fileName, u32* dest, UINT bytesToTransfer);


int main() {
	char result;
	FATFS fileSystem;


	/* Register source drive
     */
	result = f_mount(&fileSystem, DATA_SRC_DRIVE, 0);
	if(result != FR_OK) {
		printf("f_mount: fail\n\r");
		return 1;
	}
	printf("f_mount: pass\n\r");

	result = transferData(SPRITES_FILE_NAME, (u32*)SPRITES_DEST_ADDR, SPRITES_NUM*SPRITE_NUM_PIXELS*PIXEL_NUM_BYTES);
	if(result) {
		printf("transferData(sprites): fail\n\r");
		return 1;
	}
	printf("transferData(sprites): pass\n\r");

	result = transferData(MAPS_FILE_NAME, (u32*)MAPS_DEST_ADDR, MAPS_NUM*MAP_NUM_TILES*TILE_NUM_BYTES);
	if(result) {
		printf("transferData(maps): fail\n\r");
		return 1;
	}
	printf("transferData(maps): pass\n\r");


	return 0;
}


int transferData(char *fileName, u32* dest, UINT bytesToTransfer) {
	FRESULT result;
	FIL file;
	UINT bytesTransfered, i;

	/* Open file with read permissions
	 */
	result = f_open(&file, fileName, FA_READ);
	if(result) {
		printf("f_open: fail\n\r");
		return 1;
	}
	printf("f_open: pass\n\r");

	/* Point to beginning of file
	 */
	result = f_lseek(&file, 0);
	if(result) {
		printf("f_lseek: fail\n\r");
		return 1;
	}
	printf("f_lseek: pass\n\r");

	/* Copy data from file to destination
	 */
	result = f_read(&file, (void*)dest, bytesToTransfer, &bytesTransfered);
	if(result || bytesTransfered != bytesToTransfer) {
		printf("f_read: fail\n\r");
		return 1;
	}
	printf("f_read: pass\n\r");

	/* Close file
	 */
	result = f_close(&file);
	if(result) {
		printf("f_close: fail\n\r");
		return 1;
	}
	printf("f_close: pass\n\r");

	/* Print transfered data
	 */
	if(DEBUG) {
		printf("transfered data:\n\r");
		for(i = 0; i < 2*4800; ++i) {
			printf("data%d = %lu@%p\n\r", i, dest[i], &dest[i]);
		}
	}

	return 0;

}
