----------------------------------------------------------------------------------
-- University: Politecnico di Torino
-- Authors: Gabriele Cazzato, Stefano Negri Merlo, Gabriele Monaco, Alexandru Valentin Onica, Gabriele Ponzio, Roberto Stagi
-- 
-- Create Date: 04/22/2017 11:01:42 AM
-- Design Name: TOP
-- Module Name: Tile Manager
-- Project Name: Computer Architecture Project 2017
-- Target Devices: Digilent ZYBO
-- Tool Versions: Vivado 2017.1
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity tile_manager is
    generic (
        MAP_NUM_TILES_X : NATURAL := 80;
        MAP_NUM_TILES_Y : NATURAL := 60;
        TILE_NUM_BITS : NATURAL := 19
    );
    port (
        clock : in STD_LOGIC;
        we : in STD_LOGIC;
        x_r : in NATURAL range 0 to MAP_NUM_TILES_X;
        y_r : in NATURAL range 0 to MAP_NUM_TILES_Y;
        x_w : in NATURAL range 0 to MAP_NUM_TILES_X;
        y_w : in NATURAL range 0 to MAP_NUM_TILES_Y;
        in_tile : in STD_LOGIC_VECTOR (TILE_NUM_BITS-1 downto 0);
        out_tile : out STD_LOGIC_VECTOR (TILE_NUM_BITS-1 downto 0)
    );
end tile_manager;

architecture Behavioral of tile_manager is
--channels for reading and writing are separed
--both actions run in different instances to avoid overlapping
--the matrix MAP_NUM_TILES_XxMAP_NUM_TILES_Y is linearized into an array
--array_index := x + MAP_NUM_TILES_Y*y 
subtype TILE is STD_LOGIC_VECTOR (TILE_NUM_BITS-1 downto 0);
type TILEMAP is array (MAP_NUM_TILES_X*MAP_NUM_TILES_Y-1 downto 0) of TILE; --TODO: expand memory to include HUD

signal tm : TILEMAP := (others=>(others=>'0'));
attribute ram_style : string;
attribute ram_style of tm : signal is "block";

begin
    reading : process(clock)
    begin
        --read on clock 0
        --use the coordinates for reading and send the tile in output
        if falling_edge(clock) then
            out_tile <= tm(x_r + y_r*MAP_NUM_TILES_X);
        end if;
    end process;

    writing : process(clock)
    begin
        --write on clock 1
        --use the coordinates for writing and override the tile
        if rising_edge(clock) and we='1' then
            tm(x_w + y_w*MAP_NUM_TILES_X) <= in_tile;
        end if;
    end process;
end Behavioral;