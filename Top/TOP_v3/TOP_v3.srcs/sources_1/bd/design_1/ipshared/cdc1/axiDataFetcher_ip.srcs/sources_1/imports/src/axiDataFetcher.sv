//////////////////////////////////////////////////////////////////////////////////
// University: Politecnico di Torino
// Authors: Gabriele Cazzato, Stefano Negri Merlo, Gabriele Monaco, Alexandru Valentin Onica, Gabriele Ponzio, Roberto Stagi
//
// Create Date: 05/11/2017
// Design Name: TOP
// Module Name: AXI Data Fetcher - Top
// Project Name: Computer Architecture Project 2017
// Target Devices: Digilent ZYBO
// Tool Versions: Vivado 2017.1
// Description:
// An AXI4 Full master peripheral
// that translates simple read requests
// issued to a controller component
// into AXI burst read transactions
// with the aid of an AXI Master Burst component
// 
// Dependencies:
// * axiDataFetcher_controller.sv
// * axi_master_burst.vhd
//   * axi_master_burst_v2_0_7
//     * lib_srl_fifo_v1_0_2
//       * lib_pkg_v1_0_2
//
//////////////////////////////////////////////////////////////////////////////////

//`include "axiDataFetcher_defines.sv"

module axiDataFetcher #(

//////////////////////////////////////////////////////
// Parameters
//////////////////////////////////////////////////////
parameter PACKET_NUM_BITS = 32,
parameter MAPS_SRC_ADDR = 'h1000000,
parameter MAPS_NUM = 128,
parameter MAP_NUM_TILES = 80*60,
parameter SPRITES_SRC_ADDR = 'h1800000,
parameter SPRITES_NUM = 128,
parameter SPRITE_NUM_PIXELS = 16*16
) (

////////////////////////////////////////////////////// 
// Ports
//////////////////////////////////////////////////////

// Game logic
input   wire           ena,           // Transfer starts on rising edge
input   wire           dataType,      // 0 for one map, 1 for all sprites
input   wire [7:0]     dataId,        // Map identifier
output  wire [31:0]    dataPacket,    // Tile if type is map, pixel if type is sprites (includes non-significant bits)
output  wire           fetching,      // Is 1 when data packets are being transfered
//output  wire         dataPacketClk, // Rises when a new data packet is available

// AXI bus
input 	wire            m_axi_arready,
output 	wire   	        m_axi_arvalid,
output 	wire [31:0]     m_axi_araddr,
output 	wire [7:0]      m_axi_arlen,
output 	wire [2:0]      m_axi_arsize,
output 	wire [1:0]      m_axi_arburst,
output 	wire [2:0]      m_axi_arprot,
output 	wire [3:0]      m_axi_arcache,
output 	wire 	        m_axi_rready,
input 	wire 	        m_axi_rvalid,
input 	wire [31:0]	    m_axi_rdata,
input 	wire [1:0]	    m_axi_rresp,
input 	wire 		    m_axi_rlast,
input 	wire	        m_axi_awready,
output 	wire 	        m_axi_awvalid,
output 	wire [31:0]     m_axi_awaddr,
output 	wire [7:0]      m_axi_awlen,
output 	wire [2:0]      m_axi_awsize,
output 	wire [1:0]      m_axi_awburst,
output 	wire [2:0]      m_axi_awprot,
output 	wire [3:0]      m_axi_awcache,
input 	wire 		    m_axi_wready,
output 	wire 	        m_axi_wvalid,
output 	wire [31:0]     m_axi_wdata,
output 	wire [7:0]      m_axi_wstrb,
output 	wire 		    m_axi_wlast,
output 	wire 		    m_axi_bready,
input 	wire 		    m_axi_bvalid,
input 	wire [1:0]      m_axi_bresp,

// Clock & reset
input   wire            m_axi_aclk,
input 	wire            m_axi_aresetn // Resets module when low
);

//////////////////////////////////////////////////////////
// Internal signals (Controller <-> AXI Master Burst)
//////////////////////////////////////////////////////////
wire                    md_error;
wire                    ip2bus_mstrd_req;
wire 			        ip2bus_mstwr_req;
wire [31:0]	            ip2bus_mst_addr;
wire [19:0] 	        ip2bus_mst_length;
wire [3:0] 	            ip2bus_mst_be;
wire 			        ip2bus_mst_type;
wire 			        ip2bus_mst_lock;
wire 			        ip2bus_mst_reset;
wire 			        bus2ip_mst_cmdack;
wire 			        bus2ip_mst_cmplt;
wire 			        bus2ip_mst_error;
wire 			        bus2ip_mst_rearbitrate;
wire 			        bus2ip_mst_cmd_timeout;
wire [31:0]	            bus2ip_mstrd_d;
wire [7:0]	            bus2ip_mstrd_rem;
wire 			        bus2ip_mstrd_sof_n;
wire 			        bus2ip_mstrd_eof_n;
wire 			        bus2ip_mstrd_src_rdy_n;
wire 			        bus2ip_mstrd_src_dsc_n;
wire 			        ip2bus_mstrd_dst_rdy_n;
wire 			        ip2bus_mstrd_dst_dsc_n;
wire [31:0]             ip2bus_mstwr_d;
wire [7:0]              ip2bus_mstwr_rem;
wire 			        ip2bus_mstwr_sof_n;
wire 		         	ip2bus_mstwr_eof_n;
wire 			        ip2bus_mstwr_src_rdy_n;
wire 		         	ip2bus_mstwr_src_dsc_n;
wire 			        bus2ip_mstwr_dst_rdy_n;
wire 			        bus2ip_mstwr_dst_dsc_n;

//////////////////////////////////////////////////////////
// AXI Master Burst
//////////////////////////////////////////////////////////
axi_master_burst #( 
.C_M_AXI_ADDR_WIDTH     ( 32                     ),
.C_M_AXI_DATA_WIDTH     ( 32                     ),
.C_MAX_BURST_LEN        ( 256                    ),
.C_ADDR_PIPE_DEPTH      ( 1                      ),
.C_NATIVE_DATA_WIDTH    ( 32                     ),
.C_LENGTH_WIDTH			( 20                     ),
.C_FAMILY               ( "zynq7000"             ) 
) axiMasterBurst (
.md_error               ( md_error               ),  
.m_axi_arready          ( m_axi_arready          ),  
.m_axi_arvalid          ( m_axi_arvalid          ),  
.m_axi_araddr           ( m_axi_araddr           ),  
.m_axi_arlen            ( m_axi_arlen            ),
.m_axi_arsize           ( m_axi_arsize           ),  
.m_axi_arburst          ( m_axi_arburst          ),  
.m_axi_arprot           ( m_axi_arprot           ),  
.m_axi_arcache          ( m_axi_arcache          ),  
.m_axi_rready           ( m_axi_rready           ),  
.m_axi_rvalid           ( m_axi_rvalid           ),  
.m_axi_rdata            ( m_axi_rdata            ),  
.m_axi_rresp            ( m_axi_rresp            ),  
.m_axi_rlast            ( m_axi_rlast            ),  
.m_axi_awready          ( m_axi_awready          ),  
.m_axi_awvalid          ( m_axi_awvalid          ),  
.m_axi_awaddr           ( m_axi_awaddr           ),  
.m_axi_awlen            ( m_axi_awlen            ),  
.m_axi_awsize           ( m_axi_awsize           ),  
.m_axi_awburst          ( m_axi_awburst          ),  
.m_axi_awprot           ( m_axi_awprot           ),  
.m_axi_awcache          ( m_axi_awcache          ),  
.m_axi_wready           ( m_axi_wready           ),  
.m_axi_wvalid           ( m_axi_wvalid           ),  
.m_axi_wdata            ( m_axi_wdata            ),  
.m_axi_wstrb            ( m_axi_wstrb            ),  
.m_axi_wlast            ( m_axi_wlast            ),  
.m_axi_bready           ( m_axi_bready           ),  
.m_axi_bvalid           ( m_axi_bvalid           ),  
.m_axi_bresp            ( m_axi_bresp            ),  

.ip2bus_mstrd_req       ( ip2bus_mstrd_req       ),  
.ip2bus_mstwr_req       ( ip2bus_mstwr_req       ),  
.ip2bus_mst_addr        ( ip2bus_mst_addr        ),  
.ip2bus_mst_length      ( ip2bus_mst_length      ),  
.ip2bus_mst_be          ( ip2bus_mst_be          ),  
.ip2bus_mst_type        ( ip2bus_mst_type        ),  
.ip2bus_mst_lock        ( ip2bus_mst_lock        ),  
.ip2bus_mst_reset       ( ip2bus_mst_reset       ),  
.bus2ip_mst_cmdack      ( bus2ip_mst_cmdack      ),  
.bus2ip_mst_cmplt       ( bus2ip_mst_cmplt       ),  
.bus2ip_mst_error       ( bus2ip_mst_error       ),  
.bus2ip_mst_rearbitrate ( bus2ip_mst_rearbitrate ),  
.bus2ip_mst_cmd_timeout ( bus2ip_mst_cmd_timeout ),  
.bus2ip_mstrd_d         ( bus2ip_mstrd_d         ),  
.bus2ip_mstrd_rem       ( bus2ip_mstrd_rem       ),  
.bus2ip_mstrd_sof_n     ( bus2ip_mstrd_sof_n     ),  
.bus2ip_mstrd_eof_n     ( bus2ip_mstrd_eof_n     ),  
.bus2ip_mstrd_src_rdy_n ( bus2ip_mstrd_src_rdy_n ),  
.bus2ip_mstrd_src_dsc_n ( bus2ip_mstrd_src_dsc_n ),  
.ip2bus_mstrd_dst_rdy_n ( ip2bus_mstrd_dst_rdy_n ),  
.ip2bus_mstrd_dst_dsc_n ( ip2bus_mstrd_dst_dsc_n ),  
.ip2bus_mstwr_d         ( ip2bus_mstwr_d         ),  
.ip2bus_mstwr_rem       ( ip2bus_mstwr_rem       ),  
.ip2bus_mstwr_sof_n     ( ip2bus_mstwr_sof_n     ),  
.ip2bus_mstwr_eof_n     ( ip2bus_mstwr_eof_n     ),  
.ip2bus_mstwr_src_rdy_n ( ip2bus_mstwr_src_rdy_n ),  
.ip2bus_mstwr_src_dsc_n ( ip2bus_mstwr_src_dsc_n ),  
.bus2ip_mstwr_dst_rdy_n ( bus2ip_mstwr_dst_rdy_n ),  
.bus2ip_mstwr_dst_dsc_n ( bus2ip_mstwr_dst_dsc_n ),

.m_axi_aclk             ( m_axi_aclk             ),  
.m_axi_aresetn          ( m_axi_aresetn          )
);

//////////////////////////////////////////////////////////
// Controller
//////////////////////////////////////////////////////////
axiDataFetcher_controller #(
.PACKET_NUM_BITS        ( PACKET_NUM_BITS        ),
.MAPS_SRC_ADDR          ( MAPS_SRC_ADDR          ),
.MAPS_NUM               ( MAPS_NUM               ),
.MAP_NUM_TILES          ( MAP_NUM_TILES          ),
.SPRITES_SRC_ADDR       ( SPRITES_SRC_ADDR       ),
.SPRITES_NUM            ( SPRITES_NUM            ),
.SPRITE_NUM_PIXELS      ( SPRITE_NUM_PIXELS      )
) controller (
.ena                    ( ena                    ),
.dataType               ( dataType               ),
.dataId                 ( dataId                 ),
.dataPacket             ( dataPacket             ),
.fetching               ( fetching               ),
//.dataPacketClk        ( dataPacketClk          ),

.ip2bus_mstrd_req       ( ip2bus_mstrd_req       ),  
.ip2bus_mstwr_req       ( ip2bus_mstwr_req       ),  
.ip2bus_mst_addr        ( ip2bus_mst_addr        ),  
.ip2bus_mst_length      ( ip2bus_mst_length      ),  
.ip2bus_mst_be          ( ip2bus_mst_be          ),  
.ip2bus_mst_type        ( ip2bus_mst_type        ),  
.ip2bus_mst_lock        ( ip2bus_mst_lock        ),  
.ip2bus_mst_reset       ( ip2bus_mst_reset       ),  
.bus2ip_mst_cmdack      ( bus2ip_mst_cmdack      ),  
.bus2ip_mst_cmplt       ( bus2ip_mst_cmplt       ),  
.bus2ip_mst_error       ( bus2ip_mst_error       ),  
.bus2ip_mst_rearbitrate ( bus2ip_mst_rearbitrate ),  
.bus2ip_mst_cmd_timeout ( bus2ip_mst_cmd_timeout ),  
.bus2ip_mstrd_d         ( bus2ip_mstrd_d         ),  
.bus2ip_mstrd_rem       ( bus2ip_mstrd_rem       ),  
.bus2ip_mstrd_sof_n     ( bus2ip_mstrd_sof_n     ),  
.bus2ip_mstrd_eof_n     ( bus2ip_mstrd_eof_n     ),  
.bus2ip_mstrd_src_rdy_n ( bus2ip_mstrd_src_rdy_n ),  
.bus2ip_mstrd_src_dsc_n ( bus2ip_mstrd_src_dsc_n ),  
.ip2bus_mstrd_dst_rdy_n ( ip2bus_mstrd_dst_rdy_n ),  
.ip2bus_mstrd_dst_dsc_n ( ip2bus_mstrd_dst_dsc_n ),  
.ip2bus_mstwr_d         ( ip2bus_mstwr_d         ),  
.ip2bus_mstwr_rem       ( ip2bus_mstwr_rem       ),  
.ip2bus_mstwr_sof_n     ( ip2bus_mstwr_sof_n     ),  
.ip2bus_mstwr_eof_n     ( ip2bus_mstwr_eof_n     ),  
.ip2bus_mstwr_src_rdy_n ( ip2bus_mstwr_src_rdy_n ),  
.ip2bus_mstwr_src_dsc_n ( ip2bus_mstwr_src_dsc_n ),  
.bus2ip_mstwr_dst_rdy_n ( bus2ip_mstwr_dst_rdy_n ),  
.bus2ip_mstwr_dst_dsc_n ( bus2ip_mstwr_dst_dsc_n ),

.clk				    ( m_axi_aclk             ),
.rstN				    ( m_axi_aresetn          )
); 
endmodule 
