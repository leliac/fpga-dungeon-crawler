//////////////////////////////////////////////////////////////////////////////////
// University: Politecnico di Torino
// Authors: Gabriele Cazzato, Stefano Negri Merlo, Gabriele Monaco, Alexandru Valentin Onica, Gabriele Ponzio, Roberto Stagi
//
// Create Date: 05/11/2017
// Design Name: TOP
// Module Name: AXI Data Fetcher - Controller
// Project Name: Computer Architecture Project 2017
// Target Devices: Digilent ZYBO
// Tool Versions: Vivado 2017.1
// Description:
// Controller component of the AXI Data Fetcher
// 
// Additional Comments: see axiDataFetcher_top.sv for more information
//
//////////////////////////////////////////////////////////////////////////////////

//`include "axiDataFetcher_defines.sv"
`define MAP          0
`define SPRITES      1
`define IDLE		 4'd0 
`define SEND_REQ	 4'd1
`define WAIT_ACK	 4'd2
`define WAIT_CMPLT   4'd3

module axiDataFetcher_controller #(

//////////////////////////////////////////////////////
// Parameters
//////////////////////////////////////////////////////
parameter PACKET_NUM_BITS = 32,
parameter MAPS_SRC_ADDR = 'h1000000, // was `DATA_SRC_ADDR
parameter MAPS_NUM = 128,
parameter MAP_NUM_TILES = 80*60,
parameter SPRITES_SRC_ADDR = 'h1800000, // was `DATA_SRC_ADDR+`MAPS_NUM*`MAP_NUM_TILES
parameter SPRITES_NUM = 128,
parameter SPRITE_NUM_PIXELS = 16*16
)
(

////////////////////////////////////////////////////// 
// Ports
//////////////////////////////////////////////////////

// Game logic
input   wire         ena,
input   wire         dataType,
input   wire [7:0]   dataId,
output  wire [31:0]  dataPacket,
output  wire         fetching,
//output  wire         dataPacketClk,

// AXI Master Burst 
output 	reg 	     ip2bus_mstrd_req,
output 	reg          ip2bus_mstwr_req,
output 	reg  [31:0]  ip2bus_mst_addr,
output  wire [19:0]  ip2bus_mst_length,
output 	wire [3:0]   ip2bus_mst_be,
output 	wire 		 ip2bus_mst_type,
output 	wire 		 ip2bus_mst_lock,
output 	wire 		 ip2bus_mst_reset,
input 	wire         bus2ip_mst_cmdack,
input 	wire 		 bus2ip_mst_cmplt,
input 	wire 		 bus2ip_mst_error,
input 	wire 		 bus2ip_mst_rearbitrate,
input 	wire 		 bus2ip_mst_cmd_timeout,
input 	wire [31:0]  bus2ip_mstrd_d,
input 	wire [7:0]   bus2ip_mstrd_rem,
input 	wire 		 bus2ip_mstrd_sof_n,
input 	wire 		 bus2ip_mstrd_eof_n,
input 	wire 		 bus2ip_mstrd_src_rdy_n,
input 	wire 		 bus2ip_mstrd_src_dsc_n,
output 	wire 		 ip2bus_mstrd_dst_rdy_n,
output 	wire 		 ip2bus_mstrd_dst_dsc_n,
output 	reg  [31:0]	 ip2bus_mstwr_d,
output 	wire [7:0]	 ip2bus_mstwr_rem,
output 	reg 		 ip2bus_mstwr_sof_n,
output 	reg 		 ip2bus_mstwr_eof_n,
output 	reg 		 ip2bus_mstwr_src_rdy_n,
output 	wire 		 ip2bus_mstwr_src_dsc_n,
input 	wire 		 bus2ip_mstwr_dst_rdy_n,
input 	wire 		 bus2ip_mstwr_dst_dsc_n,

// Clock & reset
input 	wire 		 clk,
input 	wire 		 rstN
);

//////////////////////////////////////////////////////
// Registers
//////////////////////////////////////////////////////
//reg                  enaReg;
//reg                dataPacketClkReg;
reg [19:0]           ip2bus_mst_length_Reg;
reg	[3:0]	         state;

//////////////////////////////////////////////////////
// Constant signals
//////////////////////////////////////////////////////

// Game logic
assign dataPacket = bus2ip_mstrd_d;
//assign dataPacketClk = clk & ~bus2ip_mstrd_src_rdy_n;
assign fetching = ~bus2ip_mstrd_src_rdy_n;

// AXI Master Burst
assign ip2bus_mst_length = ip2bus_mst_length_Reg;
assign ip2bus_mst_type = 1; // Transfers are always performed in bursts
assign ip2bus_mst_lock = 0; 
assign ip2bus_mstrd_dst_dsc_n = 1; // Transfers are never discontinued
assign ip2bus_mstrd_dst_rdy_n = 0; // Always ready to receive data
assign ip2bus_mst_be = 4'b1111;	// All packet bytes are always meaningful
assign ip2bus_mstwr_rem = 0; 
assign ip2bus_mst_reset = 0; 
assign ip2bus_mstwr_src_dsc_n = 1;
assign ip2bus_mstwr_req = 0; // Never write to memory

//////////////////////////////////////////////////////
// FSM
//////////////////////////////////////////////////////
always @(posedge clk)
	if ( ! rstN ) begin
        // Reset
	    //enaReg <= 0;
	    //dataPacketClkReg <= 0;
        ip2bus_mstrd_req <= 0; 
		ip2bus_mst_addr <= 0;
		ip2bus_mst_length_Reg <= 0;
		state <= `IDLE;
	end 
	else begin
	    //if ( ! ena ) begin
        //    enaReg <= 0;
        //end
		case ( state )
		`IDLE : begin
		    // Enter SEND_REQ state if ena has risen
			//if ( ena && ! enaReg ) begin
			if ( ena ) begin
			    //enaReg <= 1;
				state <= `SEND_REQ;
			end
		end
		`SEND_REQ: begin // TODO: try removing this case and moving code to case IDLE (saves one clock cycle)
		    // Raise request signal, set source address and transfer length based on dataType
			ip2bus_mstrd_req <= 1;
			case ( dataType )
			`MAP : begin
			    ip2bus_mst_addr <= MAPS_SRC_ADDR + dataId*MAP_NUM_TILES*PACKET_NUM_BITS/8; // was `MAPS_SRC_ADDR + dataId*`MAP_NUM_TILES*`TILE_NUM_BITS/`DATA_SRC_WIDTH;
			    ip2bus_mst_length_Reg <= MAP_NUM_TILES*PACKET_NUM_BITS/8;
			end
			`SPRITES : begin
			    ip2bus_mst_addr <= SPRITES_SRC_ADDR;
			    ip2bus_mst_length_Reg <= SPRITES_NUM*SPRITE_NUM_PIXELS*PACKET_NUM_BITS/8;
			end
			endcase
			state <= `WAIT_ACK;
		end 
		`WAIT_ACK: begin
		    // If request acknowledged, lower request signal, reset source address and transfer length
			if ( bus2ip_mst_cmdack ) begin 
				ip2bus_mstrd_req <= 0; 
				ip2bus_mst_addr <= 0;
				ip2bus_mst_length_Reg <= 0;
				state <= `WAIT_CMPLT;
			end
		end 
		`WAIT_CMPLT: begin
			//if ( ! bus2ip_mstrd_src_rdy_n ) begin
            //    dataPacketClkReg <= dataPacketClkReg + 1;
            //end
            //else
			if ( bus2ip_mst_cmplt ) begin
			    //dataPacketClkReg <= 0;
			    state <= `IDLE; 
			end 
		end
		endcase  
	end
endmodule 


