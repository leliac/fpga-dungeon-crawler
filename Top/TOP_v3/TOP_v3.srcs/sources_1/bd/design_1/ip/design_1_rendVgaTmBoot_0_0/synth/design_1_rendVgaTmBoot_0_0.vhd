-- (c) Copyright 1995-2017 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: A3GRS:CA_Project_2017:rendVgaTmBoot:2.0
-- IP Revision: 2

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY design_1_rendVgaTmBoot_0_0 IS
  PORT (
    clk : IN STD_LOGIC;
    pixel_clk : IN STD_LOGIC;
    vga_r : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    vga_g : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
    vga_b : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
    vga_hs : OUT STD_LOGIC;
    vga_vs : OUT STD_LOGIC;
    fetch_start : OUT STD_LOGIC;
    data_type : OUT STD_LOGIC;
    data_id : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    packet_in : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    fetching : IN STD_LOGIC;
    sw : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    led0 : OUT STD_LOGIC;
    led1 : OUT STD_LOGIC;
    led2 : OUT STD_LOGIC;
    led3 : OUT STD_LOGIC
  );
END design_1_rendVgaTmBoot_0_0;

ARCHITECTURE design_1_rendVgaTmBoot_0_0_arch OF design_1_rendVgaTmBoot_0_0 IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : STRING;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF design_1_rendVgaTmBoot_0_0_arch: ARCHITECTURE IS "yes";
  COMPONENT rendVgaTmBoot IS
    GENERIC (
      DATA_ID_NUM_BITS : INTEGER;
      PACKET_IN_NUM_BITS : INTEGER;
      MAPS_NUM : INTEGER;
      MAP_NUM_TILES_X : INTEGER;
      MAP_NUM_TILES_Y : INTEGER;
      TILE_NUM_BITS : INTEGER;
      SPRITES_NUM : INTEGER;
      SPRITE_NUM_PIXELS : INTEGER;
      PIXEL_NUM_BITS : INTEGER
    );
    PORT (
      clk : IN STD_LOGIC;
      pixel_clk : IN STD_LOGIC;
      vga_r : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
      vga_g : OUT STD_LOGIC_VECTOR(5 DOWNTO 0);
      vga_b : OUT STD_LOGIC_VECTOR(4 DOWNTO 0);
      vga_hs : OUT STD_LOGIC;
      vga_vs : OUT STD_LOGIC;
      fetch_start : OUT STD_LOGIC;
      data_type : OUT STD_LOGIC;
      data_id : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
      packet_in : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      fetching : IN STD_LOGIC;
      sw : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      led0 : OUT STD_LOGIC;
      led1 : OUT STD_LOGIC;
      led2 : OUT STD_LOGIC;
      led3 : OUT STD_LOGIC
    );
  END COMPONENT rendVgaTmBoot;
  ATTRIBUTE X_CORE_INFO : STRING;
  ATTRIBUTE X_CORE_INFO OF design_1_rendVgaTmBoot_0_0_arch: ARCHITECTURE IS "rendVgaTmBoot,Vivado 2017.1";
  ATTRIBUTE CHECK_LICENSE_TYPE : STRING;
  ATTRIBUTE CHECK_LICENSE_TYPE OF design_1_rendVgaTmBoot_0_0_arch : ARCHITECTURE IS "design_1_rendVgaTmBoot_0_0,rendVgaTmBoot,{}";
  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_INFO OF clk: SIGNAL IS "xilinx.com:signal:clock:1.0 clk CLK";
  ATTRIBUTE X_INTERFACE_INFO OF pixel_clk: SIGNAL IS "xilinx.com:signal:clock:1.0 pixel_clk CLK";
BEGIN
  U0 : rendVgaTmBoot
    GENERIC MAP (
      DATA_ID_NUM_BITS => 8,
      PACKET_IN_NUM_BITS => 32,
      MAPS_NUM => 128,
      MAP_NUM_TILES_X => 80,
      MAP_NUM_TILES_Y => 60,
      TILE_NUM_BITS => 19,
      SPRITES_NUM => 128,
      SPRITE_NUM_PIXELS => 256,
      PIXEL_NUM_BITS => 6
    )
    PORT MAP (
      clk => clk,
      pixel_clk => pixel_clk,
      vga_r => vga_r,
      vga_g => vga_g,
      vga_b => vga_b,
      vga_hs => vga_hs,
      vga_vs => vga_vs,
      fetch_start => fetch_start,
      data_type => data_type,
      data_id => data_id,
      packet_in => packet_in,
      fetching => fetching,
      sw => sw,
      led0 => led0,
      led1 => led1,
      led2 => led2,
      led3 => led3
    );
END design_1_rendVgaTmBoot_0_0_arch;
