# Description
An AXI4 Full master peripheral that translates simple read requests issued to a controller component into AXI burst read transactions with the aid of an AXI Master Burst component

# Ports
- IN    1bit    ena:             Transfer starts on rising edge
- IN    1bit    dataType:        0 for one map, 1 for all sprites
- IN    8bits   dataId:          Map identifier 
- OUT   32bits  dataPacket:      Tile if type is map, pixel if type is sprites (includes non-significant bits)
- OUT   1bit    fetching:        Is 1 when data packets are being transfered
- INOUT 192bits m_axi:           AXI bus signals
- IN    1bit    m_axi_aclk:      PS clock
- IN    1bit    m_axi_aresetn:   PS reset

# Notes
- Data packets (tiles/pixels) need to be stored in source memory as 32-bit values ([0 ... 0] MSB ... LSB), i.e. one packet per DDR3 address
- Data to be stored starting from address 0x00400000 (points to DDR3)
- 'm_axi' to be connected to Zynq PS 'S_AXI_HP0' (via AXI interconnect)
- 'm_axi_aclk' to be connected to Zynq PS 'FCLK_CLK0'
- 'm_axi_aresetn' to be connected to Zynq PS 'FCLK_RESET0_N' (warning, reset always active until FSBL is loaded)