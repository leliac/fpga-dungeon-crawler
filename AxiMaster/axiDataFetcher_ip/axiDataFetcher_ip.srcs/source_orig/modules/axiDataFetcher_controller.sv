//////////////////////////////////////////////////////////////////////////////////
// Company: Politecnico di Torino
// Engineer: Gabriele Cazzato
// 
// Create Date: 05/11/2017
// Design Name: AXI Data Fetcher
// Module Name: Controller
// Project Name: Computer Architecture Project
// Target Devices: Zybo
// Tool Versions: Vivado 2016.4
// Description: Main controller of the AXI Data Fetcher. Translates PL read requests into proper commands to the AXI Master Burst
// 
// Dependencies: axiDataFetcher_defines.sv
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`include "axiDataFetcher_defines.sv" 

module axiDataFetcher_controller (

//////////////////////////////////////////////////////
// 
// Ports
//
//////////////////////////////////////////////////////

// Game logic
input   wire         ena,
input   wire [7:0]   dataId,
output  wire [13:0]  dataPacket,
output  wire         dataPacketClk,

// AXI Master Burst 
output 	reg 	     ip2bus_mstrd_req,
output 	reg          ip2bus_mstwr_req,
output 	reg  [31:0]  ip2bus_mst_addr,
output  wire [19:0]  ip2bus_mst_length,
output 	wire [3:0]   ip2bus_mst_be,
output 	wire 		 ip2bus_mst_type,
output 	wire 		 ip2bus_mst_lock,
output 	wire 		 ip2bus_mst_reset,
input 	wire         bus2ip_mst_cmdack,
input 	wire 		 bus2ip_mst_cmplt,
input 	wire 		 bus2ip_mst_error,
input 	wire 		 bus2ip_mst_rearbitrate,
input 	wire 		 bus2ip_mst_cmd_timeout,
input 	wire [31:0]  bus2ip_mstrd_d,
input 	wire [7:0]   bus2ip_mstrd_rem,
input 	wire 		 bus2ip_mstrd_sof_n,
input 	wire 		 bus2ip_mstrd_eof_n,
input 	wire 		 bus2ip_mstrd_src_rdy_n,
input 	wire 		 bus2ip_mstrd_src_dsc_n,
output 	wire 		 ip2bus_mstrd_dst_rdy_n,
output 	wire 		 ip2bus_mstrd_dst_dsc_n,
output 	reg  [31:0]	 ip2bus_mstwr_d,
output 	wire [7:0]	 ip2bus_mstwr_rem,
output 	reg 		 ip2bus_mstwr_sof_n,
output 	reg 		 ip2bus_mstwr_eof_n,
output 	reg 		 ip2bus_mstwr_src_rdy_n,
output 	wire 		 ip2bus_mstwr_src_dsc_n,
input 	wire 		 bus2ip_mstwr_dst_rdy_n,
input 	wire 		 bus2ip_mstwr_dst_dsc_n,

// Clock & reset
input 	wire 		 clk,
input 	wire 		 rstN
);

//////////////////////////////////////////////////////
//
// Registers
//
///////////////////////////////////////////////////////

reg                  dataPacketClkReg;
reg	[3:0]	         fsmState;

//////////////////////////////////////////////////////
// 
// Logic
//
//////////////////////////////////////////////////////

//////////////////////////////////////////////////////
// Constant signals
//////////////////////////////////////////////////////

// Game logic
assign dataPacket = bus2ip_mstrd_d[13:0];
assign dataPacketClk = dataPacketClkReg;

// AXI Master Burst
assign ip2bus_mst_length = `TRNSF_NUM_PACKETS * `TRNSF_PACKET_SIZE / 8; // Every transfer has the same length
assign ip2bus_mst_type = 1; // Transfers are always performed in bursts
assign ip2bus_mst_lock = 0; 
assign ip2bus_mstrd_dst_dsc_n = 1; // Transfers are never discontinued
assign ip2bus_mstrd_dst_rdy_n = 0; // Always ready to receive the data
assign ip2bus_mst_be = 4'b1111;	// All bytes of a packet are always meaningful
assign ip2bus_mstwr_rem = 0; 
assign ip2bus_mst_reset = 0; 
assign ip2bus_mstwr_src_dsc_n = 1;
assign ip2bus_mstwr_req = 0; // Never write to memory

//////////////////////////////////////////////////////
// Finite state machine
//////////////////////////////////////////////////////

always @(posedge clk)

    // PS reset
	if ( ! rstN ) begin
	    dataPacketClkReg <= 0;
        ip2bus_mstrd_req <= 0; 
		ip2bus_mst_addr <= 0;
		fsmState <= `FSM_IDLE;
	end 
	else begin 
		case ( fsmState )
		`FSM_IDLE : begin
		    // If enabled, exit idleness
			if ( ena ) begin 
				fsmState <= `FSM_SEND_REQ;
			end
		end
		`FSM_SEND_REQ: begin
		    // Raise request signal and set source address
			ip2bus_mstrd_req <= 1; 
			ip2bus_mst_addr <= `TRNSF_SRC_BASE_ADDR + dataId*`TRNSF_NUM_PACKETS*`TRNSF_PACKET_SIZE/`TRNSF_SRC_WIDTH;
			fsmState <= `FSM_WAIT_ACK;
		end 
		`FSM_WAIT_ACK: begin
		    // If request acknowledged, reset request signal and source address
			if ( bus2ip_mst_cmdack ) begin 
				ip2bus_mstrd_req <= 0; 
				ip2bus_mst_addr <= 0;
				fsmState <= `FSM_WAIT_CMPLT;
			end
		end 
		`FSM_WAIT_CMPLT: begin
		    // If receiving packets, invert packet clock
			if ( ! bus2ip_mstrd_src_rdy_n ) begin
                dataPacketClkReg <= dataPacketClkReg + 1;
            end
            // If transfer complete, reset packet clock
			else if ( bus2ip_mst_cmplt ) begin
			    dataPacketClkReg <= 0;
			    fsmState <= `FSM_IDLE; 
			end 
		end
		endcase  
	end

endmodule 


