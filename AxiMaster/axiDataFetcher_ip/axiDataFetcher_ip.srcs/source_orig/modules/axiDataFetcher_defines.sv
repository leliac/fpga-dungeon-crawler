//////////////////////////////////////////////////////////////////////////////////
// Company: Politecnico di Torino
// Engineer: Gabriele Cazzato
// 
// Create Date: 05/11/2017
// Design Name: AXI Data Fetcher
// Module Name: Defines
// Project Name: Computer Architecture Project
// Target Devices: Zybo
// Tool Versions: Vivado 2016.4
// Description: Definitions for the AXI Data Fetcher Controller
// 
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

//`define SIMULATION 1

// Data transfer definitions
`define TRNSF_SRC_BASE_ADDR 'h80000
`define TRNSF_SRC_WIDTH     32
`define TRNSF_NUM_PACKETS   4800
`define TRNSF_PACKET_SIZE   32

// Finite state machine statuses
`define FSM_IDLE			4'd0 
`define FSM_SEND_REQ	    4'd1
`define FSM_WAIT_ACK	    4'd2
`define FSM_WAIT_CMPLT      4'd3