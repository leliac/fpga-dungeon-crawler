//////////////////////////////////////////////////////////////////////////////////
// University: Politecnico di Torino
// Author: Gabriele Cazzato
//
// Create Date: 05/11/2017
// Design Name: AXI Data Fetcher
// Module Name: AXI Data Fetcher - Controller
// Project Name: Computer Architecture Project 2017
// Target Devices: Digilent ZYBO
// Tool Versions: Vivado 2017.1
// Description:
// Definitions for the AXI Data Fetcher Controller
// 
// Additional Comments: see axiDataFetcher_top.sv for more information
//
//////////////////////////////////////////////////////////////////////////////////

`define DATA_SRC_ADDR       'h1000000 // was 'h400000
`define DATA_SRC_WIDTH      32 // TODO: delete

`define MAP                 0
`define MAPS_SRC_ADDR       'h1000000 // was `DATA_SRC_ADDR
`define MAPS_NUM            128
`define MAP_NUM_TILES       80*60
`define TILE_NUM_BITS       32 // TODO: unify with PIXEL_NUM bits

`define SPRITES             1
`define SPRITES_SRC_ADDR    'h1800000 // was `DATA_SRC_ADDR+`MAPS_NUM*`MAP_NUM_TILES
`define SPRITES_NUM         128
`define SPRITE_NUM_PIXELS   16*16
`define PIXEL_NUM_BITS      32

`define IDLE			    4'd0 
`define SEND_REQ	        4'd1
`define WAIT_ACK	        4'd2
`define WAIT_CMPLT          4'd3