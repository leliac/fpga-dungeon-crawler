# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  ipgui::add_page $IPINST -name "Page 0"

  ipgui::add_param $IPINST -name "PACKET_NUM_BITS"
  ipgui::add_param $IPINST -name "MAPS_SRC_ADDR"
  ipgui::add_param $IPINST -name "MAPS_NUM"
  ipgui::add_param $IPINST -name "MAP_NUM_TILES"
  ipgui::add_param $IPINST -name "SPRITES_SRC_ADDR"
  ipgui::add_param $IPINST -name "SPRITES_NUM"
  ipgui::add_param $IPINST -name "SPRITE_NUM_PIXELS"

}

proc update_PARAM_VALUE.MAPS_NUM { PARAM_VALUE.MAPS_NUM } {
	# Procedure called to update MAPS_NUM when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.MAPS_NUM { PARAM_VALUE.MAPS_NUM } {
	# Procedure called to validate MAPS_NUM
	return true
}

proc update_PARAM_VALUE.MAPS_SRC_ADDR { PARAM_VALUE.MAPS_SRC_ADDR } {
	# Procedure called to update MAPS_SRC_ADDR when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.MAPS_SRC_ADDR { PARAM_VALUE.MAPS_SRC_ADDR } {
	# Procedure called to validate MAPS_SRC_ADDR
	return true
}

proc update_PARAM_VALUE.MAP_NUM_TILES { PARAM_VALUE.MAP_NUM_TILES } {
	# Procedure called to update MAP_NUM_TILES when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.MAP_NUM_TILES { PARAM_VALUE.MAP_NUM_TILES } {
	# Procedure called to validate MAP_NUM_TILES
	return true
}

proc update_PARAM_VALUE.PACKET_NUM_BITS { PARAM_VALUE.PACKET_NUM_BITS } {
	# Procedure called to update PACKET_NUM_BITS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.PACKET_NUM_BITS { PARAM_VALUE.PACKET_NUM_BITS } {
	# Procedure called to validate PACKET_NUM_BITS
	return true
}

proc update_PARAM_VALUE.SPRITES_NUM { PARAM_VALUE.SPRITES_NUM } {
	# Procedure called to update SPRITES_NUM when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SPRITES_NUM { PARAM_VALUE.SPRITES_NUM } {
	# Procedure called to validate SPRITES_NUM
	return true
}

proc update_PARAM_VALUE.SPRITES_SRC_ADDR { PARAM_VALUE.SPRITES_SRC_ADDR } {
	# Procedure called to update SPRITES_SRC_ADDR when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SPRITES_SRC_ADDR { PARAM_VALUE.SPRITES_SRC_ADDR } {
	# Procedure called to validate SPRITES_SRC_ADDR
	return true
}

proc update_PARAM_VALUE.SPRITE_NUM_PIXELS { PARAM_VALUE.SPRITE_NUM_PIXELS } {
	# Procedure called to update SPRITE_NUM_PIXELS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SPRITE_NUM_PIXELS { PARAM_VALUE.SPRITE_NUM_PIXELS } {
	# Procedure called to validate SPRITE_NUM_PIXELS
	return true
}


proc update_MODELPARAM_VALUE.PACKET_NUM_BITS { MODELPARAM_VALUE.PACKET_NUM_BITS PARAM_VALUE.PACKET_NUM_BITS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.PACKET_NUM_BITS}] ${MODELPARAM_VALUE.PACKET_NUM_BITS}
}

proc update_MODELPARAM_VALUE.MAPS_SRC_ADDR { MODELPARAM_VALUE.MAPS_SRC_ADDR PARAM_VALUE.MAPS_SRC_ADDR } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.MAPS_SRC_ADDR}] ${MODELPARAM_VALUE.MAPS_SRC_ADDR}
}

proc update_MODELPARAM_VALUE.MAPS_NUM { MODELPARAM_VALUE.MAPS_NUM PARAM_VALUE.MAPS_NUM } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.MAPS_NUM}] ${MODELPARAM_VALUE.MAPS_NUM}
}

proc update_MODELPARAM_VALUE.MAP_NUM_TILES { MODELPARAM_VALUE.MAP_NUM_TILES PARAM_VALUE.MAP_NUM_TILES } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.MAP_NUM_TILES}] ${MODELPARAM_VALUE.MAP_NUM_TILES}
}

proc update_MODELPARAM_VALUE.SPRITES_SRC_ADDR { MODELPARAM_VALUE.SPRITES_SRC_ADDR PARAM_VALUE.SPRITES_SRC_ADDR } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SPRITES_SRC_ADDR}] ${MODELPARAM_VALUE.SPRITES_SRC_ADDR}
}

proc update_MODELPARAM_VALUE.SPRITES_NUM { MODELPARAM_VALUE.SPRITES_NUM PARAM_VALUE.SPRITES_NUM } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SPRITES_NUM}] ${MODELPARAM_VALUE.SPRITES_NUM}
}

proc update_MODELPARAM_VALUE.SPRITE_NUM_PIXELS { MODELPARAM_VALUE.SPRITE_NUM_PIXELS PARAM_VALUE.SPRITE_NUM_PIXELS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SPRITE_NUM_PIXELS}] ${MODELPARAM_VALUE.SPRITE_NUM_PIXELS}
}

