
{
Command: %s
53*	vivadotcl2J
6synth_design -top axiDataFetcher -part xc7z010clg400-12default:defaultZ4-113h px� 
:
Starting synth_design
149*	vivadotclZ4-321h px� 
�
@Attempting to get a license for feature '%s' and/or device '%s'
308*common2
	Synthesis2default:default2"
xc7z010-clg4002default:defaultZ17-347h px� 
�
0Got license for feature '%s' and/or device '%s'
310*common2
	Synthesis2default:default2"
xc7z010-clg4002default:defaultZ17-349h px� 
�
%s*synth2�
�Starting Synthesize : Time (s): cpu = 00:00:11 ; elapsed = 00:00:12 . Memory (MB): peak = 1153.934 ; gain = 56.957 ; free physical = 118 ; free virtual = 4914
2default:defaulth px� 
�
synthesizing module '%s'638*oasys2"
axiDataFetcher2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/src/axiDataFetcher.sv2default:default2
292default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter PACKET_NUM_BITS bound to: 32 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter MAPS_SRC_ADDR bound to: 16777216 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter MAPS_NUM bound to: 128 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter MAP_NUM_TILES bound to: 4800 - type: integer 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter SPRITES_SRC_ADDR bound to: 25165824 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter SPRITES_NUM bound to: 128 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter SPRITE_NUM_PIXELS bound to: 256 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2$
axi_master_burst2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/src/axi_master_burst.vhd2default:default2
3502default:default8@Z8-638h px� 
h
%s
*synth2P
<	Parameter C_M_AXI_ADDR_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_M_AXI_DATA_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter C_MAX_BURST_LEN bound to: 256 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter C_ADDR_PIPE_DEPTH bound to: 1 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_NATIVE_DATA_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter C_LENGTH_WIDTH bound to: 20 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_FAMILY bound to: zynq7000 - type: string 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2*
axi_master_burst_reset2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_reset.vhd2default:default2
1342default:default8@Z8-638h px� 
�
"Detected attribute (* %s = "%s" *)3982*oasys2
keep2default:default2
TRUE2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_reset.vhd2default:default2
1622default:default8@Z8-5534h px� 
�
"Detected attribute (* %s = "%s" *)3982*oasys2
keep2default:default2
TRUE2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_reset.vhd2default:default2
1632default:default8@Z8-5534h px� 
�
"Detected attribute (* %s = "%s" *)3982*oasys2
keep2default:default2
TRUE2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_reset.vhd2default:default2
1642default:default8@Z8-5534h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2*
axi_master_burst_reset2default:default2
12default:default2
12default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_reset.vhd2default:default2
1342default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2/
axi_master_burst_cmd_status2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_cmd_status.vhd2default:default2
2392default:default8@Z8-638h px� 
b
%s
*synth2J
6	Parameter C_ADDR_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_NATIVE_DWIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_CMD_WIDTH bound to: 68 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_CMD_BTT_USED_WIDTH bound to: 20 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_STS_WIDTH bound to: 8 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_FAMILY bound to: zynq7000 - type: string 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys25
!axi_master_burst_first_stb_offset2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_first_stb_offset.vhd2default:default2
1162default:default8@Z8-638h px� 
c
%s
*synth2K
7	Parameter C_STROBE_WIDTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_OFFSET_WIDTH bound to: 8 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys25
!axi_master_burst_first_stb_offset2default:default2
22default:default2
12default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_first_stb_offset.vhd2default:default2
1162default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2-
axi_master_burst_stbs_set2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_stbs_set.vhd2default:default2
1152default:default8@Z8-638h px� 
c
%s
*synth2K
7	Parameter C_STROBE_WIDTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2-
axi_master_burst_stbs_set2default:default2
32default:default2
12default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_stbs_set.vhd2default:default2
1152default:default8@Z8-256h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2(
sig_stat_tag_reg_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_cmd_status.vhd2default:default2
9822default:default8@Z8-6014h px� 
�
RFound unconnected internal register '%s' and it is trimmed from '%s' to '%s' bits.3455*oasys2(
sig_muxed_status_reg2default:default2
82default:default2
72default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_cmd_status.vhd2default:default2
9242default:default8@Z8-3936h px�
�
merging register '%s' into '%s'3619*oasys2&
sig_pop_status_reg2default:default2)
sig_cmd_cmplt_reg_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_cmd_status.vhd2default:default2
5622default:default8@Z8-4471h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2&
sig_pop_status_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_cmd_status.vhd2default:default2
5622default:default8@Z8-6014h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2/
axi_master_burst_cmd_status2default:default2
42default:default2
12default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_cmd_status.vhd2default:default2
2392default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
axi_master_burst_rd_wr_cntlr2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_rd_wr_cntlr.vhd2default:default2
3682default:default8@Z8-638h px� 
`
%s
*synth2H
4	Parameter C_RDWR_ARID bound to: 0 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter C_RDWR_ID_WIDTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter C_RDWR_ADDR_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_RDWR_MDATA_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_RDWR_SDATA_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter C_RDWR_MAX_BURST_LEN bound to: 256 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_RDWR_BTT_USED bound to: 20 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter C_RDWR_ADDR_PIPE_DEPTH bound to: 1 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_RDWR_PCC_CMD_WIDTH bound to: 68 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_RDWR_STATUS_WIDTH bound to: 8 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_FAMILY bound to: zynq7000 - type: string 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2(
axi_master_burst_pcc2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_pcc.vhd2default:default2
3132default:default8@Z8-638h px� 
f
%s
*synth2N
:	Parameter C_DRE_ALIGN_WIDTH bound to: 1 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_SEL_ADDR_WIDTH bound to: 2 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter C_ADDR_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_STREAM_DWIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter C_MAX_BURST_LEN bound to: 256 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_CMD_WIDTH bound to: 68 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_TAG_WIDTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_BTT_USED bound to: 20 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_SUPPORT_INDET_BTT bound to: 0 - type: integer 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2-
axi_master_burst_strb_gen2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_strb_gen.vhd2default:default2
1182default:default8@Z8-638h px� 
`
%s
*synth2H
4	Parameter C_ADDR_MODE bound to: 0 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_STRB_WIDTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_OFFSET_WIDTH bound to: 2 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter C_NUM_BYTES_WIDTH bound to: 3 - type: integer 
2default:defaulth p
x
� 
�
+Unused sequential element %s was removed. 
4326*oasys23
GEN_OFFSET_MODE.strt_offset_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_strb_gen.vhd2default:default2
2072default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys22
GEN_OFFSET_MODE.end_offset_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_strb_gen.vhd2default:default2
2222default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys26
"GEN_OFFSET_MODE.sig_strb_value_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_strb_gen.vhd2default:default2
1672default:default8@Z8-6014h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2-
axi_master_burst_strb_gen2default:default2
52default:default2
12default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_strb_gen.vhd2default:default2
1182default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2=
)axi_master_burst_strb_gen__parameterized02default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_strb_gen.vhd2default:default2
1182default:default8@Z8-638h px� 
`
%s
*synth2H
4	Parameter C_ADDR_MODE bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_STRB_WIDTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_OFFSET_WIDTH bound to: 2 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter C_NUM_BYTES_WIDTH bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
+Unused sequential element %s was removed. 
4326*oasys21
GEN_ADDR_MODE.strt_offset_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_strb_gen.vhd2default:default2
3422default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys20
GEN_ADDR_MODE.end_offset_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_strb_gen.vhd2default:default2
3582default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys24
 GEN_ADDR_MODE.sig_strb_value_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_strb_gen.vhd2default:default2
2992default:default8@Z8-6014h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2=
)axi_master_burst_strb_gen__parameterized02default:default2
52default:default2
12default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_strb_gen.vhd2default:default2
1182default:default8@Z8-256h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2*
sig_input_reg_full_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_pcc.vhd2default:default2
7832default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2)
sig_xfer_reg_full_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_pcc.vhd2default:default2
10832default:default8@Z8-6014h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2(
axi_master_burst_pcc2default:default2
62default:default2
12default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_pcc.vhd2default:default2
3132default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2.
axi_master_burst_addr_cntl2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_addr_cntl.vhd2default:default2
3092default:default8@Z8-638h px� 
f
%s
*synth2N
:	Parameter C_ADDR_FIFO_DEPTH bound to: 1 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter C_ADDR_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
^
%s
*synth2F
2	Parameter C_ADDR_ID bound to: 0 - type: integer 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter C_ADDR_ID_WIDTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_TAG_WIDTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter C_FAMILY bound to: virtex7 - type: string 
2default:defaulth p
x
� 
�
"Detected attribute (* %s = "%s" *)3982*oasys2
keep2default:default2
TRUE2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_addr_cntl.vhd2default:default2
3832default:default8@Z8-5534h px� 
�
"Detected attribute (* %s = "%s" *)3982*oasys2
keep2default:default2
TRUE2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_addr_cntl.vhd2default:default2
3852default:default8@Z8-5534h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2(
sig_next_tag_reg_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_addr_cntl.vhd2default:default2
6752default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2.
sig_next_cmd_cmplt_reg_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_addr_cntl.vhd2default:default2
6802default:default8@Z8-6014h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2.
axi_master_burst_addr_cntl2default:default2
72default:default2
12default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_addr_cntl.vhd2default:default2
3092default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
axi_master_burst_rddata_cntl2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_rddata_cntl.vhd2default:default2
3582default:default8@Z8-638h px� 
b
%s
*synth2J
6	Parameter C_INCLUDE_DRE bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter C_ALIGN_WIDTH bound to: 1 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_SEL_ADDR_WIDTH bound to: 2 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter C_DATA_CNTL_FIFO_DEPTH bound to: 1 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_MMAP_DWIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_STREAM_DWIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_TAG_WIDTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_FAMILY bound to: zynq7000 - type: string 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2*
axi_master_burst_rdmux2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_rdmux.vhd2default:default2
1372default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter C_SEL_ADDR_WIDTH bound to: 2 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_MMAP_DWIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_STREAM_DWIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2*
axi_master_burst_rdmux2default:default2
82default:default2
12default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_rdmux.vhd2default:default2
1372default:default8@Z8-256h px� 
�
+Unused sequential element %s was removed. 
4326*oasys22
sig_next_dre_src_align_reg_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_rddata_cntl.vhd2default:default2
10322default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys23
sig_next_dre_dest_align_reg_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_rddata_cntl.vhd2default:default2
10332default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2,
sig_coelsc_reg_empty_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_rddata_cntl.vhd2default:default2
13712default:default8@Z8-6014h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
axi_master_burst_rddata_cntl2default:default2
92default:default2
12default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_rddata_cntl.vhd2default:default2
3582default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys23
axi_master_burst_rd_status_cntl2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_rd_status_cntl.vhd2default:default2
2012default:default8@Z8-638h px� 
`
%s
*synth2H
4	Parameter C_STS_WIDTH bound to: 8 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_TAG_WIDTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys23
axi_master_burst_rd_status_cntl2default:default2
102default:default2
12default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_rd_status_cntl.vhd2default:default2
2012default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2-
axi_master_burst_skid_buf2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_skid_buf.vhd2default:default2
1462default:default8@Z8-638h px� 
c
%s
*synth2K
7	Parameter C_WDATA_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
�
"Detected attribute (* %s = "%s" *)3982*oasys2
keep2default:default2
TRUE2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_skid_buf.vhd2default:default2
1682default:default8@Z8-5534h px� 
�
"Detected attribute (* %s = "%s" *)3982*oasys2
keep2default:default2
TRUE2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_skid_buf.vhd2default:default2
1692default:default8@Z8-5534h px� 
�
"Detected attribute (* %s = "%s" *)3982*oasys2
keep2default:default2
TRUE2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_skid_buf.vhd2default:default2
1712default:default8@Z8-5534h px� 
�
"Detected attribute (* %s = "%s" *)3982*oasys2
keep2default:default2
TRUE2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_skid_buf.vhd2default:default2
1722default:default8@Z8-5534h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2-
axi_master_burst_skid_buf2default:default2
112default:default2
12default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_skid_buf.vhd2default:default2
1462default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
axi_master_burst_wrdata_cntl2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_wrdata_cntl.vhd2default:default2
3812default:default8@Z8-638h px� 
i
%s
*synth2Q
=	Parameter C_REALIGNER_INCLUDED bound to: 0 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter C_ENABLE_STORE_FORWARD bound to: 0 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter C_SF_BYTES_RCVD_WIDTH bound to: 20 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_SEL_ADDR_WIDTH bound to: 2 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter C_DATA_CNTL_FIFO_DEPTH bound to: 1 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_MMAP_DWIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_STREAM_DWIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_TAG_WIDTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_FAMILY bound to: zynq7000 - type: string 
2default:defaulth p
x
� 
�
+Unused sequential element %s was removed. 
4326*oasys2(
sig_single_dbeat_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_wrdata_cntl.vhd2default:default2
18912default:default8@Z8-6014h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
axi_master_burst_wrdata_cntl2default:default2
122default:default2
12default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_wrdata_cntl.vhd2default:default2
3812default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys23
axi_master_burst_wr_status_cntl2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_wr_status_cntl.vhd2default:default2
2542default:default8@Z8-638h px� 
k
%s
*synth2S
?	Parameter C_ENABLE_STORE_FORWARD bound to: 0 - type: integer 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter C_SF_BYTES_RCVD_WIDTH bound to: 20 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_STS_FIFO_DEPTH bound to: 3 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_STS_WIDTH bound to: 8 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_TAG_WIDTH bound to: 4 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_FAMILY bound to: zynq7000 - type: string 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2)
axi_master_burst_fifo2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_fifo.vhd2default:default2
1622default:default8@Z8-638h px� 
]
%s
*synth2E
1	Parameter C_DWIDTH bound to: 2 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter C_DEPTH bound to: 3 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_IS_ASYNC bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_PRIM_TYPE bound to: 2 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_FAMILY bound to: zynq7000 - type: string 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2

srl_fifo_f2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/libSrlFifo/srl_fifo_f.vhd2default:default2
1612default:default8@Z8-638h px� 
]
%s
*synth2E
1	Parameter C_DWIDTH bound to: 2 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter C_DEPTH bound to: 3 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_FAMILY bound to: zynq7000 - type: string 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2"
srl_fifo_rbu_f2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/libSrlFifo/srl_fifo_rbu_f.vhd2default:default2
1922default:default8@Z8-638h px� 
]
%s
*synth2E
1	Parameter C_DWIDTH bound to: 2 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter C_DEPTH bound to: 3 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_FAMILY bound to: zynq7000 - type: string 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2)
cntr_incr_decr_addn_f2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/libSrlFifo/cntr_incr_decr_addn_f.vhd2default:default2
1432default:default8@Z8-638h px� 
[
%s
*synth2C
/	Parameter C_SIZE bound to: 3 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_FAMILY bound to: zynq7000 - type: string 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2)
cntr_incr_decr_addn_f2default:default2
132default:default2
12default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/libSrlFifo/cntr_incr_decr_addn_f.vhd2default:default2
1432default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2

dynshreg_f2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/libSrlFifo/dynshreg_f.vhd2default:default2
1532default:default8@Z8-638h px� 
\
%s
*synth2D
0	Parameter C_DEPTH bound to: 3 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_DWIDTH bound to: 2 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_FAMILY bound to: zynq7000 - type: string 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2

dynshreg_f2default:default2
142default:default2
12default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/libSrlFifo/dynshreg_f.vhd2default:default2
1532default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2"
srl_fifo_rbu_f2default:default2
152default:default2
12default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/libSrlFifo/srl_fifo_rbu_f.vhd2default:default2
1922default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2

srl_fifo_f2default:default2
162default:default2
12default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/libSrlFifo/srl_fifo_f.vhd2default:default2
1612default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2)
axi_master_burst_fifo2default:default2
172default:default2
12default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_fifo.vhd2default:default2
1622default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys29
%axi_master_burst_fifo__parameterized02default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_fifo.vhd2default:default2
1622default:default8@Z8-638h px� 
]
%s
*synth2E
1	Parameter C_DWIDTH bound to: 7 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter C_DEPTH bound to: 3 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_IS_ASYNC bound to: 0 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_PRIM_TYPE bound to: 2 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_FAMILY bound to: zynq7000 - type: string 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2.
srl_fifo_f__parameterized02default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/libSrlFifo/srl_fifo_f.vhd2default:default2
1612default:default8@Z8-638h px� 
]
%s
*synth2E
1	Parameter C_DWIDTH bound to: 7 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter C_DEPTH bound to: 3 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_FAMILY bound to: zynq7000 - type: string 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys22
srl_fifo_rbu_f__parameterized02default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/libSrlFifo/srl_fifo_rbu_f.vhd2default:default2
1922default:default8@Z8-638h px� 
]
%s
*synth2E
1	Parameter C_DWIDTH bound to: 7 - type: integer 
2default:defaulth p
x
� 
\
%s
*synth2D
0	Parameter C_DEPTH bound to: 3 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_FAMILY bound to: zynq7000 - type: string 
2default:defaulth p
x
� 
�
synthesizing module '%s'638*oasys2.
dynshreg_f__parameterized02default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/libSrlFifo/dynshreg_f.vhd2default:default2
1532default:default8@Z8-638h px� 
\
%s
*synth2D
0	Parameter C_DEPTH bound to: 3 - type: integer 
2default:defaulth p
x
� 
]
%s
*synth2E
1	Parameter C_DWIDTH bound to: 7 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_FAMILY bound to: zynq7000 - type: string 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2.
dynshreg_f__parameterized02default:default2
172default:default2
12default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/libSrlFifo/dynshreg_f.vhd2default:default2
1532default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys22
srl_fifo_rbu_f__parameterized02default:default2
172default:default2
12default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/libSrlFifo/srl_fifo_rbu_f.vhd2default:default2
1922default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2.
srl_fifo_f__parameterized02default:default2
172default:default2
12default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/libSrlFifo/srl_fifo_f.vhd2default:default2
1612default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys29
%axi_master_burst_fifo__parameterized02default:default2
172default:default2
12default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_fifo.vhd2default:default2
1622default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys23
axi_master_burst_wr_status_cntl2default:default2
182default:default2
12default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_wr_status_cntl.vhd2default:default2
2542default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys20
axi_master_burst_skid2mm_buf2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_skid2mm_buf.vhd2default:default2
1482default:default8@Z8-638h px� 
c
%s
*synth2K
7	Parameter C_MDATA_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_SDATA_WIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_ADDR_LSB_WIDTH bound to: 2 - type: integer 
2default:defaulth p
x
� 
�
"Detected attribute (* %s = "%s" *)3982*oasys2
keep2default:default2
TRUE2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_skid2mm_buf.vhd2default:default2
1732default:default8@Z8-5534h px� 
�
"Detected attribute (* %s = "%s" *)3982*oasys2
keep2default:default2
TRUE2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_skid2mm_buf.vhd2default:default2
1742default:default8@Z8-5534h px� 
�
"Detected attribute (* %s = "%s" *)3982*oasys2
keep2default:default2
TRUE2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_skid2mm_buf.vhd2default:default2
1762default:default8@Z8-5534h px� 
�
"Detected attribute (* %s = "%s" *)3982*oasys2
keep2default:default2
TRUE2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_skid2mm_buf.vhd2default:default2
1772default:default8@Z8-5534h px� 
�
synthesizing module '%s'638*oasys2-
axi_master_burst_wr_demux2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_wr_demux.vhd2default:default2
1372default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter C_SEL_ADDR_WIDTH bound to: 2 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_MMAP_DWIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_STREAM_DWIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2-
axi_master_burst_wr_demux2default:default2
192default:default2
12default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_wr_demux.vhd2default:default2
1372default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
axi_master_burst_skid2mm_buf2default:default2
202default:default2
12default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_skid2mm_buf.vhd2default:default2
1482default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys20
axi_master_burst_rd_wr_cntlr2default:default2
212default:default2
12default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_rd_wr_cntlr.vhd2default:default2
3682default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2-
axi_master_burst_rd_llink2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_rd_llink.vhd2default:default2
1782default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter C_NATIVE_DWIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2-
axi_master_burst_rd_llink2default:default2
222default:default2
12default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_rd_llink.vhd2default:default2
1782default:default8@Z8-256h px� 
�
synthesizing module '%s'638*oasys2-
axi_master_burst_wr_llink2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_wr_llink.vhd2default:default2
1782default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter C_NATIVE_DWIDTH bound to: 32 - type: integer 
2default:defaulth p
x
� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2-
axi_master_burst_wr_llink2default:default2
232default:default2
12default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_wr_llink.vhd2default:default2
1782default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2$
axi_master_burst2default:default2
242default:default2
12default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/src/axi_master_burst.vhd2default:default2
3502default:default8@Z8-256h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
82default:default2
m_axi_wstrb2default:default2
42default:default2$
axi_master_burst2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/src/axiDataFetcher.sv2default:default2
1622default:default8@Z8-689h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
82default:default2$
bus2ip_mstrd_rem2default:default2
42default:default2$
axi_master_burst2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/src/axiDataFetcher.sv2default:default2
1822default:default8@Z8-689h px� 
�
Pwidth (%s) of port connection '%s' does not match port width (%s) of module '%s'689*oasys2
82default:default2$
ip2bus_mstwr_rem2default:default2
42default:default2$
axi_master_burst2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/src/axiDataFetcher.sv2default:default2
1902default:default8@Z8-689h px� 
�
synthesizing module '%s'638*oasys2-
axiDataFetcher_controller2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/src/axiDataFetcher_controller.sv2default:default2
262default:default8@Z8-638h px� 
e
%s
*synth2M
9	Parameter PACKET_NUM_BITS bound to: 32 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter MAPS_SRC_ADDR bound to: 16777216 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter MAPS_NUM bound to: 128 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter MAP_NUM_TILES bound to: 4800 - type: integer 
2default:defaulth p
x
� 
l
%s
*synth2T
@	Parameter SPRITES_SRC_ADDR bound to: 25165824 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter SPRITES_NUM bound to: 128 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter SPRITE_NUM_PIXELS bound to: 256 - type: integer 
2default:defaulth p
x
� 
�
-case statement is not full and has no default155*oasys2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/src/axiDataFetcher_controller.sv2default:default2
1352default:default8@Z8-155h px� 
�
0Net %s in module/entity %s does not have driver.3422*oasys2"
ip2bus_mstwr_d2default:default2-
axiDataFetcher_controller2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/src/axiDataFetcher_controller.sv2default:default2
752default:default8@Z8-3848h px� 
�
0Net %s in module/entity %s does not have driver.3422*oasys2&
ip2bus_mstwr_sof_n2default:default2-
axiDataFetcher_controller2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/src/axiDataFetcher_controller.sv2default:default2
772default:default8@Z8-3848h px� 
�
0Net %s in module/entity %s does not have driver.3422*oasys2&
ip2bus_mstwr_eof_n2default:default2-
axiDataFetcher_controller2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/src/axiDataFetcher_controller.sv2default:default2
782default:default8@Z8-3848h px� 
�
0Net %s in module/entity %s does not have driver.3422*oasys2*
ip2bus_mstwr_src_rdy_n2default:default2-
axiDataFetcher_controller2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/src/axiDataFetcher_controller.sv2default:default2
792default:default8@Z8-3848h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2-
axiDataFetcher_controller2default:default2
252default:default2
12default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/src/axiDataFetcher_controller.sv2default:default2
262default:default8@Z8-256h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2"
axiDataFetcher2default:default2
262default:default2
12default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/src/axiDataFetcher.sv2default:default2
292default:default8@Z8-256h px� 
�
+design %s has port %s driven by constant %s3447*oasys2"
axiDataFetcher2default:default2"
m_axi_wstrb[7]2default:default2
02default:defaultZ8-3917h px� 
�
+design %s has port %s driven by constant %s3447*oasys2"
axiDataFetcher2default:default2"
m_axi_wstrb[6]2default:default2
02default:defaultZ8-3917h px� 
�
+design %s has port %s driven by constant %s3447*oasys2"
axiDataFetcher2default:default2"
m_axi_wstrb[5]2default:default2
02default:defaultZ8-3917h px� 
�
+design %s has port %s driven by constant %s3447*oasys2"
axiDataFetcher2default:default2"
m_axi_wstrb[4]2default:default2
02default:defaultZ8-3917h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2&
ip2bus_mstwr_d[31]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2&
ip2bus_mstwr_d[30]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2&
ip2bus_mstwr_d[29]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2&
ip2bus_mstwr_d[28]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2&
ip2bus_mstwr_d[27]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2&
ip2bus_mstwr_d[26]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2&
ip2bus_mstwr_d[25]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2&
ip2bus_mstwr_d[24]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2&
ip2bus_mstwr_d[23]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2&
ip2bus_mstwr_d[22]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2&
ip2bus_mstwr_d[21]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2&
ip2bus_mstwr_d[20]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2&
ip2bus_mstwr_d[19]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2&
ip2bus_mstwr_d[18]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2&
ip2bus_mstwr_d[17]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2&
ip2bus_mstwr_d[16]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2&
ip2bus_mstwr_d[15]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2&
ip2bus_mstwr_d[14]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2&
ip2bus_mstwr_d[13]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2&
ip2bus_mstwr_d[12]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2&
ip2bus_mstwr_d[11]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2&
ip2bus_mstwr_d[10]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2%
ip2bus_mstwr_d[9]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2%
ip2bus_mstwr_d[8]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2%
ip2bus_mstwr_d[7]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2%
ip2bus_mstwr_d[6]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2%
ip2bus_mstwr_d[5]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2%
ip2bus_mstwr_d[4]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2%
ip2bus_mstwr_d[3]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2%
ip2bus_mstwr_d[2]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2%
ip2bus_mstwr_d[1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2%
ip2bus_mstwr_d[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2&
ip2bus_mstwr_sof_n2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2&
ip2bus_mstwr_eof_n2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2*
ip2bus_mstwr_src_rdy_n2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2$
bus2ip_mst_error2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2*
bus2ip_mst_rearbitrate2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2*
bus2ip_mst_cmd_timeout2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2'
bus2ip_mstrd_rem[7]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2'
bus2ip_mstrd_rem[6]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2'
bus2ip_mstrd_rem[5]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2'
bus2ip_mstrd_rem[4]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2'
bus2ip_mstrd_rem[3]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2'
bus2ip_mstrd_rem[2]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2'
bus2ip_mstrd_rem[1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2'
bus2ip_mstrd_rem[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2&
bus2ip_mstrd_sof_n2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2&
bus2ip_mstrd_eof_n2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2*
bus2ip_mstrd_src_dsc_n2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2*
bus2ip_mstwr_dst_rdy_n2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axiDataFetcher_controller2default:default2*
bus2ip_mstwr_dst_dsc_n2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axi_master_burst_wr_llink2default:default2+
wrllink_addr_req_posted2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axi_master_burst_wr_llink2default:default2&
wrllink_xfer_cmplt2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axi_master_burst_wr_llink2default:default2'
ip2bus_mstwr_rem[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axi_master_burst_wr_llink2default:default2'
ip2bus_mstwr_rem[1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axi_master_burst_wr_llink2default:default2'
ip2bus_mstwr_rem[2]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axi_master_burst_wr_llink2default:default2'
ip2bus_mstwr_rem[3]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axi_master_burst_wr_llink2default:default2&
ip2bus_mstwr_sof_n2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axi_master_burst_wr_llink2default:default2*
ip2bus_mstwr_src_dsc_n2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axi_master_burst_rd_llink2default:default2+
rdllink_addr_req_posted2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axi_master_burst_rd_llink2default:default2&
rdllink_xfer_cmplt2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axi_master_burst_rd_llink2default:default2*
ip2bus_mstrd_dst_dsc_n2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axi_master_burst_wr_demux2default:default2'
debeat_saddr_lsb[1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2-
axi_master_burst_wr_demux2default:default2'
debeat_saddr_lsb[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys29
%axi_master_burst_fifo__parameterized02default:default2'
fifo_async_rd_reset2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys29
%axi_master_burst_fifo__parameterized02default:default2%
fifo_async_rd_clk2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2)
axi_master_burst_fifo2default:default2'
fifo_async_rd_reset2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2)
axi_master_burst_fifo2default:default2%
fifo_async_rd_clk2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
axi_master_burst_wr_status_cntl2default:default2'
calc2wsc_calc_error2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
axi_master_burst_wr_status_cntl2default:default2'
addr2wsc_fifo_empty2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
axi_master_burst_wr_status_cntl2default:default2 
data2wsc_eop2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
axi_master_burst_wr_status_cntl2default:default2+
data2wsc_bytes_rcvd[19]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
axi_master_burst_wr_status_cntl2default:default2+
data2wsc_bytes_rcvd[18]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
axi_master_burst_wr_status_cntl2default:default2+
data2wsc_bytes_rcvd[17]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
axi_master_burst_wr_status_cntl2default:default2+
data2wsc_bytes_rcvd[16]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
axi_master_burst_wr_status_cntl2default:default2+
data2wsc_bytes_rcvd[15]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
axi_master_burst_wr_status_cntl2default:default2+
data2wsc_bytes_rcvd[14]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
axi_master_burst_wr_status_cntl2default:default2+
data2wsc_bytes_rcvd[13]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
axi_master_burst_wr_status_cntl2default:default2+
data2wsc_bytes_rcvd[12]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
axi_master_burst_wr_status_cntl2default:default2+
data2wsc_bytes_rcvd[11]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
axi_master_burst_wr_status_cntl2default:default2+
data2wsc_bytes_rcvd[10]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
axi_master_burst_wr_status_cntl2default:default2*
data2wsc_bytes_rcvd[9]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
axi_master_burst_wr_status_cntl2default:default2*
data2wsc_bytes_rcvd[8]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
axi_master_burst_wr_status_cntl2default:default2*
data2wsc_bytes_rcvd[7]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
axi_master_burst_wr_status_cntl2default:default2*
data2wsc_bytes_rcvd[6]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
axi_master_burst_wr_status_cntl2default:default2*
data2wsc_bytes_rcvd[5]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
axi_master_burst_wr_status_cntl2default:default2*
data2wsc_bytes_rcvd[4]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
axi_master_burst_wr_status_cntl2default:default2*
data2wsc_bytes_rcvd[3]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
axi_master_burst_wr_status_cntl2default:default2*
data2wsc_bytes_rcvd[2]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
axi_master_burst_wr_status_cntl2default:default2*
data2wsc_bytes_rcvd[1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys23
axi_master_burst_wr_status_cntl2default:default2*
data2wsc_bytes_rcvd[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys20
axi_master_burst_wrdata_cntl2default:default2&
s2mm_strm_wstrb[3]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys20
axi_master_burst_wrdata_cntl2default:default2&
s2mm_strm_wstrb[2]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys20
axi_master_burst_wrdata_cntl2default:default2&
s2mm_strm_wstrb[1]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys20
axi_master_burst_wrdata_cntl2default:default2&
s2mm_strm_wstrb[0]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys20
axi_master_burst_wrdata_cntl2default:default2!
s2mm_strm_eop2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys20
axi_master_burst_wrdata_cntl2default:default2)
s2mm_stbs_asserted[7]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys20
axi_master_burst_wrdata_cntl2default:default2)
s2mm_stbs_asserted[6]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys20
axi_master_burst_wrdata_cntl2default:default2)
s2mm_stbs_asserted[5]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys20
axi_master_burst_wrdata_cntl2default:default2)
s2mm_stbs_asserted[4]2default:defaultZ8-3331h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-33312default:default2
1002default:defaultZ17-14h px� 
�
%s*synth2�
�Finished Synthesize : Time (s): cpu = 00:00:14 ; elapsed = 00:00:17 . Memory (MB): peak = 1202.434 ; gain = 105.457 ; free physical = 168 ; free virtual = 4947
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Constraint Validation : Time (s): cpu = 00:00:15 ; elapsed = 00:00:17 . Memory (MB): peak = 1202.434 ; gain = 105.457 ; free physical = 170 ; free virtual = 4949
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
V
%s
*synth2>
*Start Loading Part and Timing Information
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
J
%s
*synth22
Loading part: xc7z010clg400-1
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Loading Part and Timing Information : Time (s): cpu = 00:00:15 ; elapsed = 00:00:17 . Memory (MB): peak = 1210.438 ; gain = 113.461 ; free physical = 170 ; free virtual = 4949
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2%
lvar_first_be_set2default:default2
42default:default2
52default:defaultZ8-5544h px� 
|
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2 
lvar_num_set2default:defaultZ8-5546h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2'
sig_tag_counter_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_cmd_status.vhd2default:default2
4572default:default8@Z8-6014h px� 
V
Loading part %s157*device2#
xc7z010clg400-12default:defaultZ21-403h px� 
�
3inferred FSM for state register '%s' in module '%s'802*oasys2(
sig_pcc_sm_state_reg2default:default2(
axi_master_burst_pcc2default:defaultZ8-802h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2(
sig_pcc_sm_state_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_pcc.vhd2default:default2
22082default:default8@Z8-6014h px� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2(
sig_brst_cnt_eq_zero2default:defaultZ8-5546h px� 

8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2#
sig_btt_is_zero2default:defaultZ8-5546h px� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2%
sig_xfer_len_eq_02default:defaultZ8-5546h px� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2'
sig_brst_cnt_eq_one2default:defaultZ8-5546h px� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2&
sig_no_btt_residue2default:defaultZ8-5546h px� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2$
sig_addr_aligned2default:defaultZ8-5546h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2)
sig_sm_ld_xfer_reg_ns2default:default2
32default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2*
sig_sm_ld_calc2_reg_ns2default:default2
32default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2'
sig_pcc_sm_state_ns2default:default2
12default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2'
sig_pcc_sm_state_ns2default:default2
12default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2'
sig_pcc_sm_state_ns2default:default2
12default:default2
52default:defaultZ8-5544h px� 
�
+Unused sequential element %s was removed. 
4326*oasys25
!GEN_ADDR_32.sig_addr_cntr_msh_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_pcc.vhd2default:default2
12612default:default8@Z8-6014h px� 
�
merging register '%s' into '%s'3619*oasys20
sig_coelsc_cmd_cmplt_reg_reg2default:default2+
sig_coelsc_reg_full_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_rddata_cntl.vhd2default:default2
6012default:default8@Z8-4471h px� 
�
+Unused sequential element %s was removed. 
4326*oasys20
sig_coelsc_cmd_cmplt_reg_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_rddata_cntl.vhd2default:default2
6012default:default8@Z8-6014h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2-
sig_addr_posted_cntr_eq_02default:default2
32default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2,
sig_addr_posted_cntr_max2default:default2
32default:default2
52default:defaultZ8-5544h px� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2$
sig_new_len_eq_02default:defaultZ8-5546h px� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2'
sig_dbeat_cntr_eq_12default:defaultZ8-5546h px� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2'
sig_dbeat_cntr_eq_02default:defaultZ8-5546h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2

sig_decerr2default:default2
22default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2

sig_slverr2default:default2
22default:default2
52default:defaultZ8-5544h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2&
sig_dbeat_cntr_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_rddata_cntl.vhd2default:default2
12452default:default8@Z8-6014h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2-
sig_addr_posted_cntr_eq_02default:default2
32default:default2
52default:defaultZ8-5544h px� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2'
sig_dbeat_cntr_eq_02default:defaultZ8-5546h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2-
sig_addr_posted_cntr_eq_12default:default2
32default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2,
sig_addr_posted_cntr_max2default:default2
32default:default2
52default:defaultZ8-5544h px� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2$
sig_new_len_eq_02default:defaultZ8-5546h px� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2'
sig_dbeat_cntr_eq_12default:defaultZ8-5546h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2&
sig_dbeat_cntr_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_wrdata_cntl.vhd2default:default2
19472default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2,
INFERRED_GEN.data_reg[0]2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/libSrlFifo/dynshreg_f.vhd2default:default2
2462default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2,
INFERRED_GEN.data_reg[1]2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/libSrlFifo/dynshreg_f.vhd2default:default2
2462default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2,
INFERRED_GEN.data_reg[2]2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/libSrlFifo/dynshreg_f.vhd2default:default2
2462default:default8@Z8-6014h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2 
fifo_full_p12default:default2
32default:default2
52default:defaultZ8-5544h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2,
INFERRED_GEN.data_reg[0]2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/libSrlFifo/dynshreg_f.vhd2default:default2
2462default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2,
INFERRED_GEN.data_reg[1]2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/libSrlFifo/dynshreg_f.vhd2default:default2
2462default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2,
INFERRED_GEN.data_reg[2]2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/libSrlFifo/dynshreg_f.vhd2default:default2
2462default:default8@Z8-6014h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2 
fifo_full_p12default:default2
32default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2-
sig_addr_posted_cntr_eq_02default:default2
32default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2-
sig_addr_posted_cntr_eq_12default:default2
32default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2

sig_decerr2default:default2
22default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2

sig_slverr2default:default2
22default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2&
sig_statcnt_eq_max2default:default2
32default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2$
sig_statcnt_eq_02default:default2
32default:default2
52default:defaultZ8-5544h px� 
�
[ROM "%s" won't be mapped to Block RAM because address size (%s) smaller than threshold (%s)3996*oasys2,
sig_addr_posted_cntr_max2default:default2
32default:default2
52default:defaultZ8-5544h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2(
sig_pcc_sm_state_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_pcc.vhd2default:default2
22082default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2(
sig_pcc_sm_state_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_pcc.vhd2default:default2
22082default:default8@Z8-6014h px� 
�
%s
*synth2x
d---------------------------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2t
`                   State |                     New Encoding |                Previous Encoding 
2default:defaulth p
x
� 
�
%s
*synth2x
d---------------------------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2s
_                    init |                              000 |                              000
2default:defaulth p
x
� 
�
%s
*synth2s
_            wait_for_cmd |                              001 |                              001
2default:defaulth p
x
� 
�
%s
*synth2s
_                  calc_1 |                              010 |                              010
2default:defaulth p
x
� 
�
%s
*synth2s
_                  calc_2 |                              011 |                              011
2default:defaulth p
x
� 
�
%s
*synth2s
_       wait_on_xfer_push |                              100 |                              100
2default:defaulth p
x
� 
�
%s
*synth2s
_             chk_if_done |                              101 |                              101
2default:defaulth p
x
� 
�
%s
*synth2s
_              error_trap |                              110 |                              110
2default:defaulth p
x
� 
�
%s
*synth2x
d---------------------------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
Gencoded FSM with state register '%s' using encoding '%s' in module '%s'3353*oasys2(
sig_pcc_sm_state_reg2default:default2

sequential2default:default2(
axi_master_burst_pcc2default:defaultZ8-3354h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2(
sig_pcc_sm_state_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_pcc.vhd2default:default2
22082default:default8@Z8-6014h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished RTL Optimization Phase 2 : Time (s): cpu = 00:00:20 ; elapsed = 00:00:21 . Memory (MB): peak = 1242.492 ; gain = 145.516 ; free physical = 113 ; free virtual = 4852
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
L
%s
*synth24
 Start RTL Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     25 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input     20 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     16 Bit       Adders := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     10 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      8 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit       Adders := 5     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      3 Bit       Adders := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit       Adders := 3     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               32 Bit    Registers := 10    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               20 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               16 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                8 Bit    Registers := 5     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                6 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 27    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                3 Bit    Registers := 8     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 5     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 174   
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 5     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input     32 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     25 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     20 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input     20 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     18 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     16 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      8 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      7 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   9 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 13    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	  12 Input      3 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      3 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      2 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 6     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 72    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   7 Input      1 Bit        Muxes := 5     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      1 Bit        Muxes := 4     
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Finished RTL Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Y
%s
*synth2A
-Start RTL Hierarchical Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Hierarchical RTL Component report 
2default:defaulth p
x
� 
K
%s
*synth23
Module axi_master_burst_reset 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 12    
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
V
%s
*synth2>
*Module axi_master_burst_first_stb_offset 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      2 Bit        Muxes := 1     
2default:defaulth p
x
� 
N
%s
*synth26
"Module axi_master_burst_stbs_set 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   9 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
P
%s
*synth28
$Module axi_master_burst_cmd_status 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               32 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               20 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 16    
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     20 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      7 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module axi_master_burst_strb_gen 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      5 Bit       Adders := 2     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 1     
2default:defaulth p
x
� 
^
%s
*synth2F
2Module axi_master_burst_strb_gen__parameterized0 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit       Adders := 2     
2default:defaulth p
x
� 
I
%s
*synth21
Module axi_master_burst_pcc 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input     20 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     16 Bit       Adders := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     10 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               32 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               20 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               16 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                8 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                6 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 6     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                3 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 25    
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     20 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     16 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	  12 Input      3 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      3 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 10    
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   7 Input      1 Bit        Muxes := 5     
2default:defaulth p
x
� 
O
%s
*synth27
#Module axi_master_burst_addr_cntl 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               32 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                8 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                3 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 7     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module axi_master_burst_rddata_cntl 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      8 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                8 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                3 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 20    
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      8 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 14    
2default:defaulth p
x
� 
T
%s
*synth2<
(Module axi_master_burst_rd_status_cntl 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 6     
2default:defaulth p
x
� 
N
%s
*synth26
"Module axi_master_burst_skid_buf 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               32 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 10    
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 2     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module axi_master_burst_wrdata_cntl 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      8 Bit       Adders := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                8 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                3 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                2 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 23    
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      8 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 13    
2default:defaulth p
x
� 
J
%s
*synth22
Module cntr_incr_decr_addn_f 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   3 Input      3 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                3 Bit    Registers := 1     
2default:defaulth p
x
� 
C
%s
*synth2+
Module srl_fifo_rbu_f 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 3     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
J
%s
*synth22
Module axi_master_burst_fifo 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 4     
2default:defaulth p
x
� 
S
%s
*synth2;
'Module srl_fifo_rbu_f__parameterized0 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 3     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
Z
%s
*synth2B
.Module axi_master_burst_fifo__parameterized0 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 4     
2default:defaulth p
x
� 
T
%s
*synth2<
(Module axi_master_burst_wr_status_cntl 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit       Adders := 3     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                3 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 11    
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      3 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      2 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 9     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module axi_master_burst_skid2mm_buf 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               32 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 2     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 7     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 2     
2default:defaulth p
x
� 
Q
%s
*synth29
%Module axi_master_burst_rd_wr_cntlr 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 2     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 3     
2default:defaulth p
x
� 
N
%s
*synth26
"Module axi_master_burst_rd_llink 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 5     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 2     
2default:defaulth p
x
� 
N
%s
*synth26
"Module axi_master_burst_wr_llink 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 5     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 1     
2default:defaulth p
x
� 
N
%s
*synth26
"Module axiDataFetcher_controller 
2default:defaulth p
x
� 
K
%s
*synth23
Detailed RTL Component Info : 
2default:defaulth p
x
� 
:
%s
*synth2"
+---Adders : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     25 Bit       Adders := 1     
2default:defaulth p
x
� 
=
%s
*synth2%
+---Registers : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               32 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	               20 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                4 Bit    Registers := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	                1 Bit    Registers := 1     
2default:defaulth p
x
� 
9
%s
*synth2!
+---Muxes : 
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     32 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input     32 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     25 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     20 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input     20 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input     18 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      4 Bit        Muxes := 1     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   2 Input      1 Bit        Muxes := 4     
2default:defaulth p
x
� 
Z
%s
*synth2B
.	   4 Input      1 Bit        Muxes := 4     
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
[
%s
*synth2C
/Finished RTL Hierarchical Component Statistics
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
H
%s
*synth20
Start Part Resource Summary
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2j
VPart Resources:
DSPs: 80 (col length:40)
BRAMs: 120 (col length: RAMB18 40 RAMB36 20)
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Finished Part Resource Summary
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
W
%s
*synth2?
+Start Cross Boundary and Area Optimization
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 

8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2#
sig_btt_is_zero2default:defaultZ8-5546h px� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2(
sig_brst_cnt_eq_zero2default:defaultZ8-5546h px� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2&
sig_no_btt_residue2default:defaultZ8-5546h px� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2$
sig_addr_aligned2default:defaultZ8-5546h px� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2'
sig_brst_cnt_eq_one2default:defaultZ8-5546h px� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2%
sig_xfer_len_eq_02default:defaultZ8-5546h px� 
�
+Unused sequential element %s was removed. 
4326*oasys25
!GEN_ADDR_32.sig_addr_cntr_msh_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_pcc.vhd2default:default2
12612default:default8@Z8-6014h px� 
�
merging register '%s' into '%s'3619*oasys2)
sig_rd_xfer_cmplt_reg2default:default2/
sig_last_mmap_dbeat_reg_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_rddata_cntl.vhd2default:default2
6532default:default8@Z8-4471h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2)
sig_rd_xfer_cmplt_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_rddata_cntl.vhd2default:default2
6532default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2(
sig_ls_addr_cntr_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_rddata_cntl.vhd2default:default2
10672default:default8@Z8-6014h px� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2'
sig_dbeat_cntr_eq_02default:defaultZ8-5546h px� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2'
sig_dbeat_cntr_eq_12default:defaultZ8-5546h px� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2$
sig_new_len_eq_02default:defaultZ8-5546h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2&
sig_dbeat_cntr_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_rddata_cntl.vhd2default:default2
12452default:default8@Z8-6014h px� 
�
merging register '%s' into '%s'3619*oasys2(
sig_stop_request_reg2default:default2+
sig_sready_stop_reg_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_skid_buf.vhd2default:default2
2242default:default8@Z8-4471h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2(
sig_stop_request_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_skid_buf.vhd2default:default2
2242default:default8@Z8-6014h px� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2'
sig_dbeat_cntr_eq_02default:defaultZ8-5546h px� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2'
sig_dbeat_cntr_eq_12default:defaultZ8-5546h px� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2$
sig_new_len_eq_02default:defaultZ8-5546h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2&
sig_dbeat_cntr_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_wrdata_cntl.vhd2default:default2
19472default:default8@Z8-6014h px� 
�
merging register '%s' into '%s'3619*oasys2S
?GEN_OMIT_STORE_FORWARD.I_DATA_CNTL_STATUS_FIFO/sig_init_reg_reg2default:default28
$I_WRESP_STATUS_FIFO/sig_init_reg_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_fifo.vhd2default:default2
2382default:default8@Z8-4471h px� 
�
merging register '%s' into '%s'3619*oasys2T
@GEN_OMIT_STORE_FORWARD.I_DATA_CNTL_STATUS_FIFO/sig_init_reg2_reg2default:default29
%I_WRESP_STATUS_FIFO/sig_init_reg2_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_fifo.vhd2default:default2
2392default:default8@Z8-4471h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2S
?GEN_OMIT_STORE_FORWARD.I_DATA_CNTL_STATUS_FIFO/sig_init_reg_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_fifo.vhd2default:default2
2382default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2T
@GEN_OMIT_STORE_FORWARD.I_DATA_CNTL_STATUS_FIFO/sig_init_reg2_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_fifo.vhd2default:default2
2392default:default8@Z8-6014h px� 
�
merging register '%s' into '%s'3619*oasys2J
6axiMasterBurst/I_WR_LLINK_ADAPTER/sig_wr_error_reg_reg2default:default2J
6axiMasterBurst/I_RD_LLINK_ADAPTER/sig_rd_error_reg_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_wr_llink.vhd2default:default2
3692default:default8@Z8-4471h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2M
9axiMasterBurst/I_CMD_STATUS_MODULE/sig_stat_error_reg_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_cmd_status.vhd2default:default2
9182default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2H
4axiMasterBurst/I_RD_LLINK_ADAPTER/sig_stream_sof_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_rd_llink.vhd2default:default2
2152default:default8@Z8-6014h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2J
6axiMasterBurst/I_WR_LLINK_ADAPTER/sig_wr_error_reg_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_wr_llink.vhd2default:default2
3692default:default8@Z8-6014h px� 
�
8ROM "%s" won't be mapped to RAM because it is too sparse3998*oasys2P
<axiMasterBurst/I_CMD_STATUS_MODULE/I_GET_BE_SET/lvar_num_set2default:defaultZ8-5546h px� 
�
+Unused sequential element %s was removed. 
4326*oasys2J
6axiMasterBurst/I_CMD_STATUS_MODULE/sig_tag_counter_reg2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.srcs/sources_1/imports/axiMasterBurst/axi_master_burst_cmd_status.vhd2default:default2
4572default:default8@Z8-6014h px� 
�
%s
*synth2m
YDSP Report: Generating DSP controller/ip2bus_mst_addr3, operation Mode is: (A:0x12c0)*B.
2default:defaulth p
x
� 
�
%s
*synth2w
cDSP Report: operator controller/ip2bus_mst_addr3 is absorbed into DSP controller/ip2bus_mst_addr3.
2default:defaulth p
x
� 
�
+design %s has port %s driven by constant %s3447*oasys2"
axiDataFetcher2default:default2"
m_axi_wstrb[7]2default:default2
02default:defaultZ8-3917h px� 
�
+design %s has port %s driven by constant %s3447*oasys2"
axiDataFetcher2default:default2"
m_axi_wstrb[6]2default:default2
02default:defaultZ8-3917h px� 
�
+design %s has port %s driven by constant %s3447*oasys2"
axiDataFetcher2default:default2"
m_axi_wstrb[5]2default:default2
02default:defaultZ8-3917h px� 
�
+design %s has port %s driven by constant %s3447*oasys2"
axiDataFetcher2default:default2"
m_axi_wstrb[4]2default:default2
02default:defaultZ8-3917h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2[
GaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_input_tag_reg_reg[3]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2[
GaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_input_tag_reg_reg[2]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2[
GaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_input_tag_reg_reg[1]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2[
GaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_input_tag_reg_reg[0]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2Z
FaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_xfer_tag_reg_reg[3]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2Z
FaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_xfer_tag_reg_reg[2]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2Z
FaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_xfer_tag_reg_reg[1]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2Z
FaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_xfer_tag_reg_reg[0]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2q
]axiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[11]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2q
]axiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/GEN_ADDR_32.sig_adjusted_addr_incr_reg_reg[10]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2X
DaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_input_drr_reg_reg2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2W
CaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_xfer_drr_reg_reg2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2[
GaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_input_dsa_reg_reg[5]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2[
GaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_input_dsa_reg_reg[4]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2[
GaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_input_dsa_reg_reg[3]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2[
GaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_input_dsa_reg_reg[2]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2[
GaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_input_dsa_reg_reg[1]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2[
GaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_input_dsa_reg_reg[0]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2Z
FaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_xfer_dsa_reg_reg[5]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2Z
FaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_xfer_dsa_reg_reg[4]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2Z
FaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_xfer_dsa_reg_reg[3]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2Z
FaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_xfer_dsa_reg_reg[2]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2Z
FaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_xfer_dsa_reg_reg[1]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2Z
FaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_xfer_dsa_reg_reg[0]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2[
GaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_xfer_btt_reg_reg[19]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2[
GaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_xfer_btt_reg_reg[18]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2[
GaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_xfer_btt_reg_reg[17]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2[
GaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_xfer_btt_reg_reg[16]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2[
GaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_xfer_btt_reg_reg[15]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2[
GaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_xfer_btt_reg_reg[14]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2[
GaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_xfer_btt_reg_reg[13]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2[
GaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_xfer_btt_reg_reg[12]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2[
GaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_xfer_btt_reg_reg[11]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2[
GaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_xfer_btt_reg_reg[10]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2Z
FaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_xfer_btt_reg_reg[9]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2Z
FaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_xfer_btt_reg_reg[8]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2Z
FaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_xfer_btt_reg_reg[7]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2Z
FaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_xfer_btt_reg_reg[6]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2Z
FaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_xfer_btt_reg_reg[5]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2Z
FaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_xfer_btt_reg_reg[4]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2Z
FaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_xfer_btt_reg_reg[3]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2Z
FaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_xfer_btt_reg_reg[2]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2Z
FaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_xfer_btt_reg_reg[1]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2Z
FaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_xfer_btt_reg_reg[0]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2[
GaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_MSTR_PCC/sig_xfer_dre_eof_reg_reg2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2d
PaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_RD_DATA_CNTL/sig_next_last_strb_reg_reg[3]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2d
PaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_RD_DATA_CNTL/sig_next_last_strb_reg_reg[2]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2d
PaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_RD_DATA_CNTL/sig_next_last_strb_reg_reg[1]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2d
PaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_RD_DATA_CNTL/sig_next_last_strb_reg_reg[0]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2Z
FaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_RD_DATA_CNTL/sig_first_dbeat_reg2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2d
PaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_RD_DATA_CNTL/sig_next_strt_strb_reg_reg[3]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2d
PaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_RD_DATA_CNTL/sig_next_strt_strb_reg_reg[2]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2d
PaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_RD_DATA_CNTL/sig_next_strt_strb_reg_reg[1]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2d
PaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_RD_DATA_CNTL/sig_next_strt_strb_reg_reg[0]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2^
JaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_RD_DATA_CNTL/sig_next_tag_reg_reg[3]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2^
JaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_RD_DATA_CNTL/sig_next_tag_reg_reg[2]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2^
JaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_RD_DATA_CNTL/sig_next_tag_reg_reg[1]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2^
JaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_RD_DATA_CNTL/sig_next_tag_reg_reg[0]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2`
LaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_RD_DATA_CNTL/sig_coelsc_tag_reg_reg[3]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2`
LaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_RD_DATA_CNTL/sig_coelsc_tag_reg_reg[2]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2`
LaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_RD_DATA_CNTL/sig_coelsc_tag_reg_reg[1]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2`
LaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_RD_DATA_CNTL/sig_coelsc_tag_reg_reg[0]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2^
JaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_RD_DATA_CNTL/sig_coelsc_okay_reg_reg2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2`
LaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_RD_DATA_CNTL/sig_coelsc_decerr_reg_reg2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2`
LaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_RD_DATA_CNTL/sig_coelsc_slverr_reg_reg2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2i
UaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_READ_STREAM_SKID_BUF/sig_sstrb_stop_mask_reg[3]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2i
UaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_READ_STREAM_SKID_BUF/sig_sstrb_stop_mask_reg[2]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2i
UaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_READ_STREAM_SKID_BUF/sig_sstrb_stop_mask_reg[1]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2i
UaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_READ_STREAM_SKID_BUF/sig_sstrb_stop_mask_reg[0]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2g
SaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_READ_STREAM_SKID_BUF/sig_strb_skid_reg_reg[3]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2g
SaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_READ_STREAM_SKID_BUF/sig_strb_skid_reg_reg[2]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2g
SaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_READ_STREAM_SKID_BUF/sig_strb_skid_reg_reg[1]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2g
SaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_READ_STREAM_SKID_BUF/sig_strb_skid_reg_reg[0]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2f
RaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_READ_STREAM_SKID_BUF/sig_strb_reg_out_reg[3]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2f
RaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_READ_STREAM_SKID_BUF/sig_strb_reg_out_reg[2]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2f
RaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_READ_STREAM_SKID_BUF/sig_strb_reg_out_reg[1]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2f
RaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_READ_STREAM_SKID_BUF/sig_strb_reg_out_reg[0]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2\
HaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_wr_xfer_cmplt_reg2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2^
JaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_s2mm_ld_nxt_len_reg2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2]
IaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_s2mm_wr_len_reg[7]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2]
IaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_s2mm_wr_len_reg[6]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2]
IaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_s2mm_wr_len_reg[5]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2]
IaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_s2mm_wr_len_reg[4]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2]
IaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_s2mm_wr_len_reg[3]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2]
IaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_s2mm_wr_len_reg[2]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2]
IaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_s2mm_wr_len_reg[1]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2]
IaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_s2mm_wr_len_reg[0]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2^
JaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_ls_addr_cntr_reg[1]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2^
JaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_ls_addr_cntr_reg[0]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2^
JaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_next_tag_reg_reg[3]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2^
JaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_next_tag_reg_reg[2]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2^
JaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_next_tag_reg_reg[1]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2^
JaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_next_tag_reg_reg[0]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2^
JaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_data2wsc_tag_reg[3]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2^
JaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_data2wsc_tag_reg[2]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2^
JaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_data2wsc_tag_reg[1]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2^
JaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_data2wsc_tag_reg[0]2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2�
�axiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_STATUS_CNTLR/I_WRESP_STATUS_FIFO/USE_SRL_FIFO.I_SYNC_FIFO/I_SRL_FIFO_RBU_F/underflow_i_reg2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2�
�axiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_STATUS_CNTLR/I_WRESP_STATUS_FIFO/USE_SRL_FIFO.I_SYNC_FIFO/I_SRL_FIFO_RBU_F/overflow_i_reg2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
ESequential element (%s) is unused and will be removed from module %s.3332*oasys2�
�axiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_STATUS_CNTLR/GEN_OMIT_STORE_FORWARD.I_DATA_CNTL_STATUS_FIFO/USE_SRL_FIFO.I_SYNC_FIFO/I_SRL_FIFO_RBU_F/underflow_i_reg2default:default2"
axiDataFetcher2default:defaultZ8-3332h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-33322default:default2
1002default:defaultZ17-14h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2_
KaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_reset_reg_reg2default:default2
FD2default:default2_
KaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_MMAP_SKID_BUF/sig_reset_reg_reg2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys25
!controller/ip2bus_mst_addr_reg[0]2default:default2
FDRE2default:default25
!controller/ip2bus_mst_addr_reg[1]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys25
!controller/ip2bus_mst_addr_reg[1]2default:default2
FDRE2default:default26
"controller/ip2bus_mst_addr_reg[25]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys26
"controller/ip2bus_mst_addr_reg[25]2default:default2
FDRE2default:default26
"controller/ip2bus_mst_addr_reg[26]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys26
"controller/ip2bus_mst_addr_reg[26]2default:default2
FDRE2default:default26
"controller/ip2bus_mst_addr_reg[27]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys26
"controller/ip2bus_mst_addr_reg[27]2default:default2
FDRE2default:default26
"controller/ip2bus_mst_addr_reg[28]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys26
"controller/ip2bus_mst_addr_reg[28]2default:default2
FDRE2default:default26
"controller/ip2bus_mst_addr_reg[29]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys26
"controller/ip2bus_mst_addr_reg[29]2default:default2
FDRE2default:default26
"controller/ip2bus_mst_addr_reg[30]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys26
"controller/ip2bus_mst_addr_reg[30]2default:default2
FDRE2default:default26
"controller/ip2bus_mst_addr_reg[31]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys26
"controller/ip2bus_mst_addr_reg[31]2default:default2
FDRE2default:default2<
(controller/ip2bus_mst_length_Reg_reg[19]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2<
(controller/ip2bus_mst_length_Reg_reg[18]2default:default2
FDRE2default:default2<
(controller/ip2bus_mst_length_Reg_reg[19]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2<
(controller/ip2bus_mst_length_Reg_reg[19]2default:default2
FDRE2default:default2<
(controller/ip2bus_mst_length_Reg_reg[16]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2<
(controller/ip2bus_mst_length_Reg_reg[16]2default:default2
FDRE2default:default2<
(controller/ip2bus_mst_length_Reg_reg[15]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2<
(controller/ip2bus_mst_length_Reg_reg[15]2default:default2
FDRE2default:default2<
(controller/ip2bus_mst_length_Reg_reg[13]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2<
(controller/ip2bus_mst_length_Reg_reg[14]2default:default2
FDRE2default:default2<
(controller/ip2bus_mst_length_Reg_reg[11]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2<
(controller/ip2bus_mst_length_Reg_reg[13]2default:default2
FDRE2default:default2<
(controller/ip2bus_mst_length_Reg_reg[12]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2<
(controller/ip2bus_mst_length_Reg_reg[12]2default:default2
FDRE2default:default2<
(controller/ip2bus_mst_length_Reg_reg[10]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2<
(controller/ip2bus_mst_length_Reg_reg[11]2default:default2
FDRE2default:default2;
'controller/ip2bus_mst_length_Reg_reg[9]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2<
(controller/ip2bus_mst_length_Reg_reg[10]2default:default2
FDRE2default:default2;
'controller/ip2bus_mst_length_Reg_reg[7]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2;
'controller/ip2bus_mst_length_Reg_reg[9]2default:default2
FDRE2default:default2;
'controller/ip2bus_mst_length_Reg_reg[8]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2;
'controller/ip2bus_mst_length_Reg_reg[7]2default:default2
FDRE2default:default2;
'controller/ip2bus_mst_length_Reg_reg[6]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2;
'controller/ip2bus_mst_length_Reg_reg[6]2default:default2
FDRE2default:default2;
'controller/ip2bus_mst_length_Reg_reg[5]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2;
'controller/ip2bus_mst_length_Reg_reg[5]2default:default2
FDRE2default:default2;
'controller/ip2bus_mst_length_Reg_reg[4]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2;
'controller/ip2bus_mst_length_Reg_reg[4]2default:default2
FDRE2default:default2;
'controller/ip2bus_mst_length_Reg_reg[3]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2;
'controller/ip2bus_mst_length_Reg_reg[3]2default:default2
FDRE2default:default2;
'controller/ip2bus_mst_length_Reg_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2;
'controller/ip2bus_mst_length_Reg_reg[2]2default:default2
FDRE2default:default2;
'controller/ip2bus_mst_length_Reg_reg[1]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2;
'controller/ip2bus_mst_length_Reg_reg[1]2default:default2
FDRE2default:default2;
'controller/ip2bus_mst_length_Reg_reg[0]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2=
)\controller/ip2bus_mst_length_Reg_reg[0] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2+
controller/state_reg[2]2default:default2
FDRE2default:default2+
controller/state_reg[3]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2-
\controller/state_reg[3] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2_
KaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_MMAP_SKID_BUF/sig_reset_reg_reg2default:default2
FD2default:default2n
ZaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_STATUS_CNTLR/I_WRESP_STATUS_FIFO/sig_init_reg_reg2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2N
:axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_addr_reg[0]2default:default2
FDRE2default:default2N
:axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_addr_reg[1]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2N
:axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_addr_reg[1]2default:default2
FDRE2default:default2O
;axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_addr_reg[25]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2O
;axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_addr_reg[25]2default:default2
FDRE2default:default2O
;axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_addr_reg[26]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2O
;axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_addr_reg[26]2default:default2
FDRE2default:default2O
;axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_addr_reg[27]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2O
;axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_addr_reg[27]2default:default2
FDRE2default:default2O
;axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_addr_reg[28]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2O
;axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_addr_reg[28]2default:default2
FDRE2default:default2O
;axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_addr_reg[29]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2O
;axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_addr_reg[29]2default:default2
FDRE2default:default2O
;axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_addr_reg[30]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2O
;axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_addr_reg[30]2default:default2
FDRE2default:default2O
;axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_addr_reg[31]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2O
;axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_addr_reg[31]2default:default2
FDRE2default:default2P
<axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2Q
=axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[18]2default:default2
FDRE2default:default2P
<axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2Q
=axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[19]2default:default2
FDRE2default:default2P
<axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2Q
=axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[16]2default:default2
FDRE2default:default2P
<axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2Q
=axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[15]2default:default2
FDRE2default:default2P
<axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2Q
=axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[14]2default:default2
FDRE2default:default2P
<axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[8]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2Q
=axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[13]2default:default2
FDRE2default:default2P
<axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2Q
=axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[12]2default:default2
FDRE2default:default2P
<axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2Q
=axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[11]2default:default2
FDRE2default:default2P
<axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[8]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2Q
=axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[10]2default:default2
FDRE2default:default2P
<axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2P
<axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[9]2default:default2
FDRE2default:default2P
<axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[8]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2P
<axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[7]2default:default2
FDRE2default:default2P
<axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2P
<axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[6]2default:default2
FDRE2default:default2P
<axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2P
<axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[5]2default:default2
FDRE2default:default2P
<axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2P
<axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[4]2default:default2
FDRE2default:default2P
<axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2P
<axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[3]2default:default2
FDRE2default:default2P
<axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2P
<axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[2]2default:default2
FDRE2default:default2P
<axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2P
<axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[1]2default:default2
FDRE2default:default2P
<axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2P
<axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[0]2default:default2
FDRE2default:default2L
8axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mstwr_req_reg2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2K
7axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_type_req_reg2default:default2
FDRE2default:default2L
8axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_be_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2L
8axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_be_reg[1]2default:default2
FDRE2default:default2L
8axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_be_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2L
8axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_be_reg[2]2default:default2
FDRE2default:default2L
8axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_be_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2L
8axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_be_reg[3]2default:default2
FDRE2default:default2L
8axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_be_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2`
LaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_READ_STREAM_SKID_BUF/sig_reset_reg_reg2default:default2
FD2default:default2n
ZaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_STATUS_CNTLR/I_WRESP_STATUS_FIFO/sig_init_reg_reg2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2N
:\axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mstwr_req_reg 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2W
CaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_halt_reg_reg2default:default2
FDRE2default:default2Z
FaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_STATUS_CNTLR/sig_halt_reg_reg2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2Z
FaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_STATUS_CNTLR/sig_halt_reg_reg2default:default2
FDRE2default:default2W
CaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_RD_DATA_CNTL/sig_halt_reg_reg2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Y
E\axiMasterBurst/I_RD_WR_CNTRL_MODULE/I_RD_DATA_CNTL/sig_halt_reg_reg 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2]
IaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_ADDR_CNTL/sig_next_burst_reg_reg[1]2default:default2
FDRE2default:default2\
HaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_ADDR_CNTL/sig_next_size_reg_reg[0]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2\
HaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_ADDR_CNTL/sig_next_size_reg_reg[0]2default:default2
FDRE2default:default2\
HaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_ADDR_CNTL/sig_next_size_reg_reg[2]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2^
J\axiMasterBurst/I_RD_WR_CNTRL_MODULE/I_ADDR_CNTL/sig_next_size_reg_reg[2] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2c
O\axiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_next_cmd_cmplt_reg_reg 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2_
K\axiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_ld_new_cmd_reg_reg 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2\
HaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_halt_reg_dly3_reg2default:default2
FDR2default:default2O
;axiMasterBurst/I_RD_WR_CNTRL_MODULE/sig_doing_write_reg_reg2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2\
HaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_halt_reg_dly2_reg2default:default2
FDR2default:default2O
;axiMasterBurst/I_RD_WR_CNTRL_MODULE/sig_doing_write_reg_reg2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2\
HaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_halt_reg_dly1_reg2default:default2
FDR2default:default2O
;axiMasterBurst/I_RD_WR_CNTRL_MODULE/sig_doing_write_reg_reg2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2\
HaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_RD_DATA_CNTL/sig_halt_reg_dly3_reg2default:default2
FDR2default:default2O
;axiMasterBurst/I_RD_WR_CNTRL_MODULE/sig_doing_write_reg_reg2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2\
HaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_RD_DATA_CNTL/sig_halt_reg_dly2_reg2default:default2
FDR2default:default2O
;axiMasterBurst/I_RD_WR_CNTRL_MODULE/sig_doing_write_reg_reg2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2\
HaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_RD_DATA_CNTL/sig_halt_reg_dly1_reg2default:default2
FDR2default:default2O
;axiMasterBurst/I_RD_WR_CNTRL_MODULE/sig_doing_write_reg_reg2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2Q
=\axiMasterBurst/I_RD_WR_CNTRL_MODULE/sig_doing_write_reg_reg 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2d
P\axiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_next_sequential_reg_reg 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2]
I\axiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_next_eof_reg_reg 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2d
P\axiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_next_calc_error_reg_reg 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2J
6\axiMasterBurst/I_WR_LLINK_ADAPTER/sig_llink_busy_reg 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2_
K\axiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_dqual_reg_full_reg 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2f
R\axiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_next_strt_strb_reg_reg[0] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2f
R\axiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_next_last_strb_reg_reg[0] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2f
R\axiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_next_strt_strb_reg_reg[1] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2f
R\axiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_next_last_strb_reg_reg[1] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2f
R\axiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_next_strt_strb_reg_reg[2] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2f
R\axiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_next_last_strb_reg_reg[2] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2f
R\axiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_next_strt_strb_reg_reg[3] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2f
R\axiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_next_last_strb_reg_reg[3] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2\
H\axiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_first_dbeat_reg 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2d
P\axiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_addr_posted_cntr_reg[1] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2d
P\axiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_addr_posted_cntr_reg[2] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2d
P\axiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_addr_posted_cntr_reg[0] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2f
RaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_MMAP_SKID_BUF/sig_strb_skid_reg_reg[0]2default:default2
FDRE2default:default2f
RaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_MMAP_SKID_BUF/sig_strb_skid_reg_reg[1]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2f
RaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_MMAP_SKID_BUF/sig_strb_skid_reg_reg[1]2default:default2
FDRE2default:default2f
RaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_MMAP_SKID_BUF/sig_strb_skid_reg_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2f
RaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_MMAP_SKID_BUF/sig_strb_skid_reg_reg[2]2default:default2
FDRE2default:default2f
RaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_MMAP_SKID_BUF/sig_strb_skid_reg_reg[3]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2[
G\axiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WR_DATA_CNTL/sig_last_dbeat_reg 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2e
QaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_MMAP_SKID_BUF/sig_strb_reg_out_reg[0]2default:default2
FDRE2default:default2e
QaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_MMAP_SKID_BUF/sig_strb_reg_out_reg[1]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2e
QaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_MMAP_SKID_BUF/sig_strb_reg_out_reg[1]2default:default2
FDRE2default:default2e
QaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_MMAP_SKID_BUF/sig_strb_reg_out_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2e
QaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_MMAP_SKID_BUF/sig_strb_reg_out_reg[2]2default:default2
FDRE2default:default2e
QaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_MMAP_SKID_BUF/sig_strb_reg_out_reg[3]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys26
"controller/ip2bus_mst_addr_reg[23]2default:default2
FDRE2default:default2<
(controller/ip2bus_mst_length_Reg_reg[17]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2O
;axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_addr_reg[23]2default:default2
FDRE2default:default2Q
=axiMasterBurst/I_CMD_STATUS_MODULE/sig_cmd_mst_length_reg[17]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2f
RaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[0]2default:default2
FDE2default:default2f
RaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[1]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2f
RaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[1]2default:default2
FDE2default:default2f
RaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2f
RaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[2]2default:default2
FDE2default:default2f
RaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[3]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2f
RaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[3]2default:default2
FDE2default:default2f
RaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[4]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2f
RaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[4]2default:default2
FDE2default:default2f
RaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[5]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2f
RaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[5]2default:default2
FDE2default:default2f
RaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[6]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2f
RaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[6]2default:default2
FDE2default:default2f
RaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[7]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2f
RaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[7]2default:default2
FDE2default:default2f
RaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[8]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2f
RaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[8]2default:default2
FDE2default:default2f
RaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[9]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2f
RaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[9]2default:default2
FDE2default:default2g
SaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[10]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2g
SaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[10]2default:default2
FDE2default:default2g
SaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[11]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2g
SaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[11]2default:default2
FDE2default:default2g
SaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[12]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2g
SaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[12]2default:default2
FDE2default:default2g
SaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[13]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2g
SaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[13]2default:default2
FDE2default:default2g
SaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[14]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2g
SaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[14]2default:default2
FDE2default:default2g
SaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[15]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2g
SaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[15]2default:default2
FDE2default:default2g
SaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[16]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2g
SaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[16]2default:default2
FDE2default:default2g
SaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[17]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2g
SaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[17]2default:default2
FDE2default:default2g
SaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[18]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2g
SaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[18]2default:default2
FDE2default:default2g
SaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[19]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2g
SaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[19]2default:default2
FDE2default:default2g
SaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[20]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2g
SaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[20]2default:default2
FDE2default:default2g
SaxiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[21]2default:defaultZ8-3886h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-38862default:default2
1002default:defaultZ17-14h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2i
U\axiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_skid_reg_reg[31] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2h
T\axiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_STRM_SKID_BUF/sig_data_reg_out_reg[31] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2h
T\axiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_MMAP_SKID_BUF/sig_data_skid_reg_reg[0] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2h
T\axiMasterBurst/I_RD_WR_CNTRL_MODULE/I_WRITE_MMAP_SKID_BUF/sig_data_reg_out_reg[31] 2default:defaultZ8-3333h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Cross Boundary and Area Optimization : Time (s): cpu = 00:00:32 ; elapsed = 00:00:37 . Memory (MB): peak = 1329.523 ; gain = 232.547 ; free physical = 117 ; free virtual = 4683
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�---------------------------------------------------------------------------------
Start ROM, RAM, DSP and Shift Register Reporting
2default:defaulth px� 
~
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px� 
_
%s*synth2G
3
DSP: Preliminary Mapping  Report (see note below)
2default:defaulth px� 
�
%s*synth2�
�+--------------------------+--------------+--------+--------+--------+--------+--------+------+------+------+------+-------+------+------+
2default:defaulth px� 
�
%s*synth2�
�|Module Name               | DSP Mapping  | A Size | B Size | C Size | D Size | P Size | AREG | BREG | CREG | DREG | ADREG | MREG | PREG | 
2default:defaulth px� 
�
%s*synth2�
�+--------------------------+--------------+--------+--------+--------+--------+--------+------+------+------+------+-------+------+------+
2default:defaulth px� 
�
%s*synth2�
�|axiDataFetcher_controller | (A:0x12c0)*B | 8      | 13     | -      | -      | 21     | 0    | 0    | -    | -    | -     | 0    | 0    | 
2default:defaulth px� 
�
%s*synth2�
�+--------------------------+--------------+--------+--------+--------+--------+--------+------+------+------+------+-------+------+------+

2default:defaulth px� 
�
%s*synth2�
�Note: The table above is a preliminary report that shows the DSPs inferred at the current stage of the synthesis flow. Some DSP may be reimplemented as non DSP primitives later in the synthesis flow. Multiple instantiated DSPs are reported only once.
2default:defaulth px� 
�
%s*synth2�
�---------------------------------------------------------------------------------
Finished ROM, RAM, DSP and Shift Register Reporting
2default:defaulth px� 
~
%s*synth2f
R---------------------------------------------------------------------------------
2default:defaulth px� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
F
%s
*synth2.
Start Timing Optimization
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Timing Optimization : Time (s): cpu = 00:00:32 ; elapsed = 00:00:38 . Memory (MB): peak = 1329.523 ; gain = 232.547 ; free physical = 118 ; free virtual = 4684
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-
Start Technology Mapping
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Technology Mapping : Time (s): cpu = 00:00:33 ; elapsed = 00:00:38 . Memory (MB): peak = 1340.539 ; gain = 243.562 ; free physical = 130 ; free virtual = 4689
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
?
%s
*synth2'
Start IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Q
%s
*synth29
%Start Flattening Before IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
T
%s
*synth2<
(Finished Flattening Before IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
H
%s
*synth20
Start Final Netlist Cleanup
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Finished Final Netlist Cleanup
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished IO Insertion : Time (s): cpu = 00:00:34 ; elapsed = 00:00:40 . Memory (MB): peak = 1340.539 ; gain = 243.562 ; free physical = 129 ; free virtual = 4689
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
D
%s
*synth2,

Report Check Netlist: 
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|      |Item              |Errors |Warnings |Status |Description       |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|1     |multi_driven_nets |      0|        0|Passed |Multi driven nets |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Start Renaming Generated Instances
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Instances : Time (s): cpu = 00:00:34 ; elapsed = 00:00:40 . Memory (MB): peak = 1340.539 ; gain = 243.562 ; free physical = 129 ; free virtual = 4689
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
L
%s
*synth24
 Start Rebuilding User Hierarchy
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Rebuilding User Hierarchy : Time (s): cpu = 00:00:35 ; elapsed = 00:00:40 . Memory (MB): peak = 1340.539 ; gain = 243.562 ; free physical = 130 ; free virtual = 4689
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Start Renaming Generated Ports
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Ports : Time (s): cpu = 00:00:35 ; elapsed = 00:00:40 . Memory (MB): peak = 1340.539 ; gain = 243.562 ; free physical = 130 ; free virtual = 4689
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
M
%s
*synth25
!Start Handling Custom Attributes
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Handling Custom Attributes : Time (s): cpu = 00:00:35 ; elapsed = 00:00:41 . Memory (MB): peak = 1340.539 ; gain = 243.562 ; free physical = 130 ; free virtual = 4689
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
J
%s
*synth22
Start Renaming Generated Nets
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Nets : Time (s): cpu = 00:00:35 ; elapsed = 00:00:41 . Memory (MB): peak = 1340.539 ; gain = 243.562 ; free physical = 130 ; free virtual = 4689
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2�
�---------------------------------------------------------------------------------
Start ROM, RAM, DSP and Shift Register Reporting
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
L
%s
*synth24
 
Dynamic Shift Register Report:
2default:defaulth p
x
� 
�
%s
*synth2�
n+------------+--------------------------+--------+------------+--------+---------+--------+--------+--------+
2default:defaulth p
x
� 
�
%s
*synth2�
o|Module Name | RTL Name                 | Length | Data Width | SRL16E | SRLC32E | Mux F7 | Mux F8 | Mux F9 | 
2default:defaulth p
x
� 
�
%s
*synth2�
n+------------+--------------------------+--------+------------+--------+---------+--------+--------+--------+
2default:defaulth p
x
� 
�
%s
*synth2�
o|dsrl        | INFERRED_GEN.data_reg[2] | 4      | 2          | 2      | 0       | 0      | 0      | 0      | 
2default:defaulth p
x
� 
�
%s
*synth2�
o|dsrl__1     | INFERRED_GEN.data_reg[2] | 4      | 7          | 7      | 0       | 0      | 0      | 0      | 
2default:defaulth p
x
� 
�
%s
*synth2�
o+------------+--------------------------+--------+------------+--------+---------+--------+--------+--------+

2default:defaulth p
x
� 
�
%s
*synth2�
�---------------------------------------------------------------------------------
Finished ROM, RAM, DSP and Shift Register Reporting
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Start Writing Synthesis Report
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
A
%s
*synth2)

Report BlackBoxes: 
2default:defaulth p
x
� 
J
%s
*synth22
+-+--------------+----------+
2default:defaulth p
x
� 
J
%s
*synth22
| |BlackBox name |Instances |
2default:defaulth p
x
� 
J
%s
*synth22
+-+--------------+----------+
2default:defaulth p
x
� 
J
%s
*synth22
+-+--------------+----------+
2default:defaulth p
x
� 
A
%s*synth2)

Report Cell Usage: 
2default:defaulth px� 
E
%s*synth2-
+------+--------+------+
2default:defaulth px� 
E
%s*synth2-
|      |Cell    |Count |
2default:defaulth px� 
E
%s*synth2-
+------+--------+------+
2default:defaulth px� 
E
%s*synth2-
|1     |BUFG    |     1|
2default:defaulth px� 
E
%s*synth2-
|2     |CARRY4  |    19|
2default:defaulth px� 
E
%s*synth2-
|3     |DSP48E1 |     1|
2default:defaulth px� 
E
%s*synth2-
|4     |LUT1    |    16|
2default:defaulth px� 
E
%s*synth2-
|5     |LUT2    |    42|
2default:defaulth px� 
E
%s*synth2-
|6     |LUT3    |    69|
2default:defaulth px� 
E
%s*synth2-
|7     |LUT4    |    54|
2default:defaulth px� 
E
%s*synth2-
|8     |LUT5    |    66|
2default:defaulth px� 
E
%s*synth2-
|9     |LUT6    |    76|
2default:defaulth px� 
E
%s*synth2-
|10    |FDRE    |   385|
2default:defaulth px� 
E
%s*synth2-
|11    |FDSE    |     7|
2default:defaulth px� 
E
%s*synth2-
|12    |IBUF    |    49|
2default:defaulth px� 
E
%s*synth2-
|13    |OBUF    |   183|
2default:defaulth px� 
E
%s*synth2-
+------+--------+------+
2default:defaulth px� 
E
%s
*synth2-

Report Instance Areas: 
2default:defaulth p
x
� 
�
%s
*synth2l
X+------+--------------------------------------+--------------------------------+------+
2default:defaulth p
x
� 
�
%s
*synth2l
X|      |Instance                              |Module                          |Cells |
2default:defaulth p
x
� 
�
%s
*synth2l
X+------+--------------------------------------+--------------------------------+------+
2default:defaulth p
x
� 
�
%s
*synth2l
X|1     |top                                   |                                |   968|
2default:defaulth p
x
� 
�
%s
*synth2l
X|2     |  axiMasterBurst                      |axi_master_burst                |   679|
2default:defaulth p
x
� 
�
%s
*synth2l
X|3     |    I_CMD_STATUS_MODULE               |axi_master_burst_cmd_status     |    51|
2default:defaulth p
x
� 
�
%s
*synth2l
X|4     |    I_RD_LLINK_ADAPTER                |axi_master_burst_rd_llink       |     8|
2default:defaulth p
x
� 
�
%s
*synth2l
X|5     |    I_RD_WR_CNTRL_MODULE              |axi_master_burst_rd_wr_cntlr    |   605|
2default:defaulth p
x
� 
�
%s
*synth2l
X|6     |      I_ADDR_CNTL                     |axi_master_burst_addr_cntl      |    52|
2default:defaulth p
x
� 
�
%s
*synth2l
X|7     |      I_MSTR_PCC                      |axi_master_burst_pcc            |   348|
2default:defaulth p
x
� 
�
%s
*synth2l
X|8     |      I_RD_DATA_CNTL                  |axi_master_burst_rddata_cntl    |    54|
2default:defaulth p
x
� 
�
%s
*synth2l
X|9     |      I_RD_STATUS_CNTLR               |axi_master_burst_rd_status_cntl |     6|
2default:defaulth p
x
� 
�
%s
*synth2l
X|10    |      I_READ_STREAM_SKID_BUF          |axi_master_burst_skid_buf       |   109|
2default:defaulth p
x
� 
�
%s
*synth2l
X|11    |      I_WRITE_MMAP_SKID_BUF           |axi_master_burst_skid2mm_buf    |     9|
2default:defaulth p
x
� 
�
%s
*synth2l
X|12    |      I_WRITE_STRM_SKID_BUF           |axi_master_burst_skid_buf_0     |     6|
2default:defaulth p
x
� 
�
%s
*synth2l
X|13    |      I_WR_STATUS_CNTLR               |axi_master_burst_wr_status_cntl |    16|
2default:defaulth p
x
� 
�
%s
*synth2l
X|14    |        I_WRESP_STATUS_FIFO           |axi_master_burst_fifo           |    16|
2default:defaulth p
x
� 
�
%s
*synth2l
X|15    |          \USE_SRL_FIFO.I_SYNC_FIFO   |srl_fifo_f                      |     9|
2default:defaulth p
x
� 
�
%s
*synth2l
X|16    |            I_SRL_FIFO_RBU_F          |srl_fifo_rbu_f                  |     9|
2default:defaulth p
x
� 
�
%s
*synth2l
X|17    |              CNTR_INCR_DECR_ADDN_F_I |cntr_incr_decr_addn_f           |     6|
2default:defaulth p
x
� 
�
%s
*synth2l
X|18    |    I_RESET_MODULE                    |axi_master_burst_reset          |    15|
2default:defaulth p
x
� 
�
%s
*synth2l
X|19    |  controller                          |axiDataFetcher_controller       |    56|
2default:defaulth p
x
� 
�
%s
*synth2l
X+------+--------------------------------------+--------------------------------+------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Writing Synthesis Report : Time (s): cpu = 00:00:35 ; elapsed = 00:00:41 . Memory (MB): peak = 1340.539 ; gain = 243.562 ; free physical = 129 ; free virtual = 4689
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
t
%s
*synth2\
HSynthesis finished with 0 errors, 0 critical warnings and 470 warnings.
2default:defaulth p
x
� 
�
%s
*synth2�
�Synthesis Optimization Runtime : Time (s): cpu = 00:00:35 ; elapsed = 00:00:41 . Memory (MB): peak = 1340.539 ; gain = 243.562 ; free physical = 131 ; free virtual = 4691
2default:defaulth p
x
� 
�
%s
*synth2�
�Synthesis Optimization Complete : Time (s): cpu = 00:00:35 ; elapsed = 00:00:41 . Memory (MB): peak = 1340.547 ; gain = 243.562 ; free physical = 131 ; free virtual = 4691
2default:defaulth p
x
� 
B
 Translating synthesized netlist
350*projectZ1-571h px� 
f
-Analyzing %s Unisim elements for replacement
17*netlist2
692default:defaultZ29-17h px� 
j
2Unisim Transformation completed in %s CPU seconds
28*netlist2
02default:defaultZ29-28h px� 
K
)Preparing netlist for logic optimization
349*projectZ1-570h px� 
u
)Pushed %s inverter(s) to %s load pin(s).
98*opt2
02default:default2
02default:defaultZ31-138h px� 
~
!Unisim Transformation Summary:
%s111*project29
%No Unisim elements were transformed.
2default:defaultZ1-111h px� 
�
G%s Infos, %s Warnings, %s Critical Warnings and %s Errors encountered.
28*	vivadotcl2
2752default:default2
2582default:default2
02default:default2
02default:defaultZ4-41h px� 
^
%s completed successfully
29*	vivadotcl2 
synth_design2default:defaultZ4-42h px� 
�
r%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s ; free physical = %s ; free virtual = %s
480*common2"
synth_design: 2default:default2
00:00:422default:default2
00:00:522default:default2
1453.3482default:default2
368.9962default:default2
1552default:default2
46272default:defaultZ17-722h px� 
�
 The %s '%s' has been generated.
621*common2

checkpoint2default:default2�
�/home/andreas/Desktop/computer_architecture/project/gitlab/axiDataFetcher/axiDataFetcher_ip/axiDataFetcher_ip.runs/synth_1/axiDataFetcher.dcp2default:defaultZ17-1381h px� 
�
�report_utilization: Time (s): cpu = 00:00:00.28 ; elapsed = 00:00:01 . Memory (MB): peak = 1477.359 ; gain = 0.000 ; free physical = 147 ; free virtual = 4629
*commonh px� 
�
Exiting %s at %s...
206*common2
Vivado2default:default2,
Sun Jun 18 20:04:04 20172default:defaultZ17-206h px� 


End Record