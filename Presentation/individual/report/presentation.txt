Politecnico di Torino
Academic year 2016-2017
Bachelor’s degree in Computer Engineering
Computer Architecture course (professors Paolo Montuschi and Luca Sterpone)

PROJECT PRESENTATION
Gabriele Elia Cazzato


Contents
0. Design overview and rationale
1. Top module
	1.1  Main net
	1.2 Clocking net
	1.3 Reset net
2. SD boot and DDR initialization
3. AXI Master peripheral: actuator and controller


0. DESIGN OVERVIEW AND RATIONALE

The development board available to my team, the Digilent ZYBO, is rich in peripherals and features a complex System-on-Chip, the Xilinx Zynq Z-7010, which integrates Xilinx 7-series Field Programmable Gate Array logic with a Processing System built around a 650 MHz dual-core ARM Cortex-A9 processor. While the main focus of the project was programming the FPGA to realize a simple video-game, it seemed appropriate to put this Processing System to work, using it without the support of an operating system as a bridge between some of the board’s hardware: specifically, the Zynq’s Programmable Logic, the Micro Secure Digital slot and the Double Data Rate 3 Random-Access Memory (32-bit width, 512 MiB capacity and 1050 Mbps maximum bandwidth). For testing purposes, the USB-UART bridge and basic I/O hardware – LEDs and switches – were also involved in my design.

One of my goals was configuring a MicroSD card from which the Zynq could boot and transfer at startup into the DDR memory the game’s maps and sprites. This separation between data and logic makes the game easily moddable, allowing to alter its appearance and dynamics by just replacing a couple of files on the SD and thus without re-synthesizing code, which can be a lengthy operation. 

On the Programmable Logic side, I designed an Advanced eXtensible Interface master peripheral, which can be queried at runtime by the actual game modules to access DDR data. Having to coordinate Programmable Logic and Processing System operations, I was also in charge of putting together the project’s top Block Design in Xilinx Vivado Design Suite 2017.1. From the AXI Master’s and the Game’s source files, I created and parametrized two Intellectual Property Cores, which were properly wired to each other and to the Processing System Core.


1. TOP MODULE

1.1 Main net

The AXI Master is connected, through a dedicated Interconnect Core, to one of the High Performance AXI Ports of the Processing System, which in turn accesses the DDR via its DRAM controller. The Game can request data by sending the AXI Master an Enable signal, together with a 1-bit Data Type signal and an 8-bit Data ID signal. If Data Type is 0, the map whose index is specified by Data ID is requested; if Data Type is 1, all sprites are requested, regardless of Data ID. Data is received from the AXI Master in 32-bit “packets” – which can be map tiles or sprite pixels – and stored in two Programmable Logic Block RAMs for fast runtime access;  a signal indicating whether a transfer is taking place is also received.
The Game queries the AXI Master through an internal Initialization Module, written in VHDL together with other team members, which requests all sprites and one random map at the beginning of the game; another random map is requested whenever a new game level is reached. The game’s enabling signal can be simulated with one of the board’s switches and the different phases of the transfer are highlighted by making LEDs turn on.

1.2 Clocking net

Three different clocks are used in the design: a 25 MHz clock needed to handle the Video Graphics Array, a 100 MHz clock for the sprites’ Block RAM and a main 50 MHz clock used by all other modules. To synchronize the Processing System and Programmable Logic, these clocks are all generated from the Processing System’s Phase-Locked Loop using a dedicated Clock Generator Core. The Processing System itself is fed by the 50 MHz oscillator on the ZYBO.

1.3 Reset net

The AXI Master’s signals and registers are resettable: whenever the Processing System is reset, for example at power-on or when pressing the ZYBO’s System Reset button, it sends a signal to a dedicated Reset Handler Core, which forwards it to the AXI Master and to the Interconnect as well.


2. BOOT AND DDR INITIALIZATION

A binary BOOT file is packaged with Xilinx SDK 2017.1, containing the Zynq’s First Stage Bootloader, the FPGA-programming Bitstream and a standalone C program with instructions to perform the SD to DDR data transfer. The file is placed on a FAT32-formatted MicroSD card along with the maps and sprites binary files. With the card inserted in the slot and the programming mode jumper set to “SD”, the board is turned on and, as a last step of the boot sequence, the C program is executed.

The program uses the FatFs library to mount the card’s file system, open in sequence the two binary files and copy their content to two different addresses in a range (0010_0000 to 3FFF_FFFF) specifically reserved for the DDR and accessible both by the CPU and the Programmable Logic. Printed debug messages can be displayed on a PC connected to the board’s serial port.


3. AXI MASTER PERIPHERAL: ACTUATOR AND CONTROLLER

The purpose of the AXI Master is to act as a simple, read-only interface of the DDR for the Game, which doesn’t need to understand the details of a Full AXI4 Burst Read Transaction. The module is parametrized with the number of maps and sprites, their sizes (in number of tiles and pixels respectively) sizes and their base DDR addresses, making it easily adaptable to different situations.

The AXI Master is composed by an Actuator and by its Controller. The Actuator was built using source code from Xilinx AXI Master Burst Core, which helps to make AXI signals more manageable; the very slight delays caused by the use of this core are not relevant for the project’s purposes.
The Controller, written in SystemVerilog, acts an interpreter between the Game and the Actuator. When it is enabled by the Game, it sends the Actuator a Request signal and - depending on whether the Game has requested all sprites or one map – sets appropriate Address and Length signals. It then waits for a Command Acknowledged signal from the Actuator, upon which the Request, Address and Length are reset. A Source Ready signal from the Actuator is forwarded to the Game to indicate it can start reading packets from the Data channel. At the end of the transfer, the Controller receives a Complete signal from the Actuator and returns idle. Other signals are sent to the Actuator for the transaction to happen as intended, but they can be kept constant the whole time.


References and credits
- ZYBO rev. B Reference Manual - February 14, 2014
- Zynq-7000 All Programmable SoC Technical Reference Manual - UG585 (v1.11) September 27, 2016
- LogiCORE IP AXI Master Burst v2.0 Product Guide for Vivado Design Suite - PG162 December 18, 2013
Special thanks to Dr. Mohammad S. Sadri of the Microelectronic Systems Design Research Group at Technische Universität Kaiserslautern for his precious tutorials on AXI and the Zynq.
