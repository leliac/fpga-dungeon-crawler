----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/29/2017 12:38:46 PM
-- Design Name: 
-- Module Name: checkDir - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- computes the best direction to reach the object and outputs it to be checked
-- by the collisionChecker, while pending the direction is 1111 
-- The clever AI adjusts the direction until it can move, after some iterations
-- it stops (that means the mob cannot move in any direction)
-- The dumb one finds wether it can reach the target using the same direction and
-- stops if it can not or if the target is too far (30 steps)
-- Each time the collision checker is called it must be passed the coordinates and 
-- the direction computed here, then should wait for a result to continue
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity chasingAi is
    Port ( clock : in STD_LOGIC;
           targetX : in NATURAL range 0 to 80;
           targetY : in NATURAL range 0 to 60;
           thisX : in NATURAL range 0 to 80;
           thisY : in NATURAL range 0 to 60;
           thisType : in STD_LOGIC; --indicates the type of Ai to compute (should be defined)
           isFree : in STD_LOGIC; --received from collisionChecker
           dirToTest :out STD_LOGIC_VECTOR (3 downto 0);
           testX : out NATURAL range 0 to 80; --to pass to collisionChecker
           testY : out NATURAL range 0 to 60;
           direction : out STD_LOGIC_VECTOR (3 downto 0));
end chasingAi;
-- moving directions
--   2
-- 1   3
--   0
-- right - up -  left - down
architecture Behavioral of chasingAi is
function computeDir(targetX,targetY,thisX,thisY : INTEGER)
return STD_LOGIC_VECTOR is
variable dir : STD_LOGIC_VECTOR (3 downto 0); 
begin
  if thisX<targetX then
    if thisY<targetY then
      dir := "1001"; --go down-right
    elsif thisY>targetY then
      dir := "1100"; --go up-right
    else --if thisY=targetY
      dir := "1000"; --go right
    end if;
  elsif thisX>targetX then
    if thisY<targetY then
      dir := "0011"; --go down-left
    elsif thisY>targetY then
      dir := "0110"; --go up-left
    else --if thisY=targetY
      dir := "0010"; --go left
    end if;
  else --if thisX=targetX
    if thisY<targetY then
      dir := "0001"; --go down
    elsif thisY>targetY then
      dir := "0100"; --go up
    else --if thisY=targetY
      dir := "0000"; --found
    end if;
  end if; 
  
  return dir;
end function;

begin
  adjust : process(clock)
  variable computing : BOOLEAN := true;
  variable x,y,tmpX,tmpY : INTEGER range -81 to 161;
  variable chXm,chYm,chXp,chYp : INTEGER range 0 to 80;
  variable tmpdir,bestdir : STD_LOGIC_VECTOR (3 downto 0) := (others=>'1'); --to init
  variable cnt : INTEGER range 0 to 31 := 0;
  begin
    
    --update/init variables
    if targetX/=chXp or targetY/=chYp or thisX/=chXm or thisY/=chYm or tmpdir="1111" then
      if thisType='1' then --clever Ai
        x:=targetX;
        y:=targetY; 
        tmpX:=x; tmpY:=y;
      elsif thisType='0' then --dumb Ai
        x:=thisX;
        y:=thisY;
      end if;
      chXm:=thisX; chYm:=thisY; --to intercept changes
      chXp:=targetX; chYp:=targetY;
      tmpdir := (others=>'0'); --no more in initialization state
      computing := true; --needs to compute
      bestdir := computeDir(targetX,targetY,thisX,thisY);
      cnt := 0;
    end if;
  
    if rising_edge(clock) then
      if abs(thisX-targetX)<=2 and abs(thisY-targetY)<=2 then
        direction <= (others=>'0'); --stop while found
      elsif computing then
      
        if thisType='1' then --clever Ai
          if isFree='0' and cnt<10 then --cannot move
            tmpdir := computeDir(x,y,thisX,thisY);
            if (bestdir(3) xor bestdir(2) xor bestdir(1) xor bestdir(0))='0' then --diagonal movements
              if (cnt mod 3)=0 then
                x := tmpX; y := thisY; --first try to adjust horizontally
              elsif (cnt mod 3)=1 then
                x := thisX; y := tmpY; --then vertically
              elsif (cnt mod 3)=2 then
                if (cnt mod 2)=0 then --change diagonal direction
                  x := 2*thisX-targetX; y := targetY;
                else
                  x := targetX; y := 2*thisY-targetY;
                end if;
                tmpX:=x; tmpY:=y; --save temporary target position
              end if;
            else --pure movement
              if (cnt mod 3)=0 then
                if tmpY=thisY then --horizontal move
                  y := tmpY + 20; --try diagonally on one side
                else --vertical move
                  x := tmpX + 20; --try diagonally on one side
                end if;
              elsif (cnt mod 3)=1 then
                if tmpY=thisY then
                  y := tmpY - 20; --try the other
                else
                  x := tmpX - 20; --try the other
                end if;                
              elsif (cnt mod 3)=2 then
                if (cnt mod 2)=0 then --change pure direction
                  if targetY=thisY then
                    x := thisX; y := targetY + 20;
                  else
                    x := targetX + 20; y := thisY;
                  end if;
                else
                  if targetY=thisY then
                    x := thisX; y := targetY - 20;
                  else
                    x := targetX - 20; y := thisY;
                  end if;                  
                end if;
                tmpX:=x; tmpY:=y; --save temporary target position
              end if;
            end if;
            cnt := cnt + 1;
            direction <= (others=>'1'); --correct direction still not found
          else
            if cnt=10 then
              direction <= (others=>'0'); --stop trying, there's no way out
            else
              direction <= tmpdir; --now it's correct
            end if;
            computing := false; --found result
            tmpX:=targetX; tmpY:=targetY; --used to track event here
          end if;
          dirToTest <= tmpdir;
          testX <= thisX; testY <= thisY; --no change here
          
        elsif thisType='0' then --dumb Ai
          testX <= x; testY <= y; --pass updated
          if isFree='1' and cnt<30 then
            tmpdir := computeDir(targetX,targetY,x,y);
            if tmpdir(3)='1' then --update the position
              x := x + 1;
            elsif tmpdir(1)='1' then
              x := x - 1;
            end if;
            if tmpdir(2)='1' then --update the position
              y := y - 1;
            elsif tmpdir(0)='1' then
              y := y + 1;
            end if;
            dirToTest <= tmpdir;
            if abs(x-targetX)<=2 and abs(y-targetY)<=2 then
              direction <= bestdir; --can reach it, pass initial direction
              computing := false; --found result
            else
              direction <= (others=>'1'); --still pending
            end if;
            cnt := cnt + 1;
          else
            direction <= (others=>'0'); --there's an obstacle, stop
            computing := false; --found result
          end if;
        end if;
      end if;
    end if;
  end process;
end Behavioral;