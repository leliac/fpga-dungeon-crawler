----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    08:01:51 05/08/2017 
-- Design Name: 
-- Module Name:    mob_generation - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.typemobpackage.ALL;

entity mob_generation is
	port(
			re: in std_logic;
			data_out: out mobprop; --PACKAGE
			count: in natural range 0 to 24;
			enable: in std_logic);

			
end mob_generation;

architecture Behavioral of mob_generation is


	type mobram is array (0 to 24) of mobprop;
	signal mobs: mobram;
	attribute ram_style:string;
	attribute ram_style of mobs: signal is "distributed";  --PORCODIO

begin
	create_array:process(enable)
		variable re1 : integer;
		variable seed1 :positive;
		variable seed2 :positive;
		variable i: integer range 0 to 25:=0;
		begin
		i:=0;
		while (i<25) loop
			uniform(seed1,seed2,re1);
			mobs(i).prop(2 downto 0) <= std_logic_vector ( to_unsigned (re1,3));
			i:=i+1;
		end loop;
	end process;
	
	output_mob:process(count)
	begin
		data_out<=mobs(count);
	end process;
end Behavioral;

