----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 30.04.2017 20:47:49
-- Design Name: 
-- Module Name: ostacle_assign - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.math_real.ALL;   --to add

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ostacle_assign is
    generic(
        dim1 : natural; --to declare
        dim2 : natural --to declare
);
        port(
            clk : in std_logic;
            moves : in STD_LOGIC_VECTOR (7 downto 0);
            mob_1 : inout STD_LOGIC_VECTOR (2 downto 0);
            mob_2 : inout STD_LOGIC_VECTOR (2 downto 0);
            trap : inout STD_LOGIC_VECTOR (1 downto 0));
end ostacle_assign;

architecture Behavioral of ostacle_assign is
signal count_mobs : natural:=-1;
signal count_traps : natural:=-1;
signal switch_1 : std_logic;
signal switch_2 : std_logic;
signal mob_3: STD_LOGIC_VECTOR (2 downto 0);
signal trap_2 : STD_LOGIC_VECTOR (1 downto 0);
begin
    mob_spawn: process(switch_1)

    variable seed1 :positive;
    variable seed2 :positive;
    variable re1 : integer;
    variable re2 : real;
    
    begin
        uniform (seed1,seed2,re2);
        re1 := integer (re2 * real(2*dim1-1)); --to review
        if(mob_1 = "000")  then               --to change syntax to adapt to dim1
          mob_1 <= std_logic_vector ( to_unsigned (re1,3));
        elsif(mob_2 = "000") then
          mob_2 <= std_logic_vector ( to_unsigned (re1,3)); 
        else
          mob_3 <= std_logic_vector ( to_unsigned (re1,3)); 
        end if;
        if(moves(4)='1') then
        mob_1 <= mob_2;
        mob_2 <= mob_3;
        mob_3 <= "000";
        end if;
    end process;
    
    trap_spawn: process(switch_2)

    variable seed3 :positive;
    variable seed4 :positive;
    variable re3 : integer;
    variable re4 : real;
    begin
        uniform (seed3,seed4,re4);
        re3 := integer (re4 * real(2*dim2-1));   -- to review
        if(trap = "00")  then
          trap <= std_logic_vector ( to_unsigned (re3,2));
        else
          trap_2 <= std_logic_vector ( to_unsigned (re3,2)); 
        end if;
        if(moves(5)='1') then
        trap <= trap_2;
        trap_2 <= "00";
        end if;
    end process;
    
    clk_count : process(clk)
    begin
        if rising_edge(clk) then
            count_mobs<=(count_mobs+1) mod 250000000;     --2 seconds, maybe change with time variable
            count_traps<=(count_traps+1) mod 1250000000;  --10 seconds, same
            if(count_mobs=0) then
                switch_1 <= not switch_1;
            end if;
            if(count_traps=0) then
                switch_2 <= not switch_2;
            end if;
        end if;
      end process;
end Behavioral;
