----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/04/2017 05:23:31 PM
-- Design Name: 
-- Module Name: connInterface - Behavioral
-- Project Name: 
-- Target Devices: 
-- Description: 
-- This module uses 5 physical pins as input and 5 as output:
-- [one is for the clock] and the others are for the buttons bus
-- (double), [a 4 bit bus to send additional information] and the
-- 2 enables (may use different connectors).
-- For stability reasons the clock used is always the system clock
-- (won't be a problem since both boards work at the same frequency)
-- The bits are sent in series at each rising edge of the
-- clock attached, while receiving they are saved in a vector
-- all computations of these data (only regarding inputs) are
-- to be done on board. Regarding button bus the rx/tx is done
-- by 2 pins, splitting the bus in 2 halves
-- To signal for communications the pins ena_{tx,rx} are used
-- 1 means the communication is active, 0 is off (both directions)
-- The buses are transmitted reversed (damned downto reference)
-- but on receiving they are flipped again
-- The transmission is done 3 times consecutively, at receiption
-- the signal is received 3 times and the best option is selected
-- with majority vote (bit by bit) 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

entity connInterface is
    Port ( sys_clk : in STD_LOGIC; --will be transmitted
           start : in STD_LOGIC; --trigger from the top module
           ena_rx : in STD_LOGIC := '0'; --other side is transmitting
--           clk_rx : in STD_LOGIC := '0'; --from other board to read the signal
           channel_rx_up : in STD_LOGIC := '0'; --listening pin (7 downto 4)
           channel_rx_down : in STD_LOGIC := '0'; --listening pin (3 downto 0)
--           opt_rx : in STD_LOGIC := '0'; --optional pin
           butts_rx : out STD_LOGIC_VECTOR (7 downto 0) := (others=>'0'); --received
--           special_rx : out STD_LOGIC_VECTOR (3 downto 0) := (others=>'0'); --additional info
           ena_tx : out STD_LOGIC := '0'; --to inform other board
--           clk_tx : out STD_LOGIC := '0'; --from this board to the other
           channel_tx_up : out STD_LOGIC := '0'; --transmitting pin (7 downto 4)
           channel_tx_down : out STD_LOGIC := '0'; --transmitting pin (3 downto 0)
--           opt_tx : out STD_LOGIC := '0'; --optional pin
           butts_tx : in STD_LOGIC_VECTOR (7 downto 0) := (others=>'0')); --to send
--           special_tx : in STD_LOGIC_VECTOR (3 downto 0) := (others=>'0')); --additional info
end connInterface;
architecture Behavioral of connInterface is
type buses is array (0 to 2) of STD_LOGIC_VECTOR (3 downto 0);

function findCorrect(test : buses) return STD_LOGIC_VECTOR is
variable final : STD_LOGIC_VECTOR (3 downto 0);
begin
  for i in 0 to 3 loop
    final(i) := (test(0)(i) and test(1)(i)) or (test(1)(i) and test(2)(i)) or (test(0)(i) and test(2)(i));
  end loop;
  return final;
end function;

begin
  
  transmitting : process(sys_clk)
  variable cnt : INTEGER range 0 to 6:= 5;
  variable butts : STD_LOGIC_VECTOR (7 downto 0) := (others=>'0');
  variable phase : INTEGER range 0 to 3 := 0;
  begin
    
--    if rising_edge(sys_clk) then --sampling clock
--      clk_tx<='1';
--    end if;
--    if falling_edge(sys_clk) then
--      clk_tx<='0';
--    end if;
    
    if start='0' then --init
      cnt := 0; --restart
      phase := 0;
    elsif rising_edge(sys_clk) then --can start
      if cnt<4 then --transmission not ended 
        ena_tx<='1'; --warn on the other side
        channel_tx_up <= butts_tx(cnt+4); --send correct bit
        channel_tx_down <= butts_tx(cnt); --send correct bit
--          opt_tx <= opt_tx(cnt);
        cnt := cnt + 1;
        if phase<2 and cnt=4 then --still need to send
          cnt := 0; --send it 3 times
          phase := phase + 1;
        end if;
      elsif cnt=4 then
        ena_tx<='0'; --warn on the other side
        channel_tx_up <= '0';
        channel_tx_down <= '0';
--        opt_tx <= '0';
        cnt := cnt + 1;
      end if;
    end if;
  end process;
  
  receiving : process(sys_clk,ena_rx) --use clk_rx to restore dependency
--  receiving : process(clk_rx,ena_rx) --use clk_rx to restore dependency
  variable cnt : INTEGER range 0 to 6 := 0;
  variable butts_up,butts_down,opt : buses;
  variable phase : INTEGER range 0 to 3 := 0;
  begin
    if ena_rx='0' then
      cnt := 0; --restart 
      phase := 0;         
    elsif rising_edge(sys_clk) then --sample the input with own system clock
--    elsif rising_edge(clk_rx) then
      if cnt<4 then --active connection
        butts_up(phase)(cnt) := channel_rx_up;
        butts_down(phase)(cnt) := channel_rx_down;
--        opt(phase)(cnt) := opt_rx;
        cnt := cnt + 1;
      end if;
      if cnt=4 then
        if phase<2 then
          cnt := 0; --ready to receive it 3 times
          phase := phase + 1;
        else
          if ena_rx='1' then
            phase := phase + 1;
            butts_rx <= findCorrect(butts_up) & findCorrect(butts_down);
--            special_rx <= findCorrect(opt);
            cnt := cnt + 1;
          end if;
        end if;
      end if;
    end if;
  end process;
end Behavioral;