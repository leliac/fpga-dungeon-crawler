----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04.06.2017 00:20:06
-- Design Name: 
-- Module Name: typelib - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
package typelib is
    type mob is record
    x : NATURAL range 0 to 80;
    y : NATURAL range 0 to 60;
    life : NATURAL range 0 to 5;
    spriteID : STD_LOGIC_VECTOR (6 downto 0);
    bulx : NATURAL range 0 to 80;
    buly : NATURAL range 0 to 60;
    buldir: std_logic_vector (3 downto 0);
--    idle: std_logic;
end record mob;
constant EmptyEntity : mob := (x => 80,
                              y => 60,
                              life => 0,
                              spriteID => (others=>'0'),
                              bulx => 80,
                              buly => 60,
                              buldir => "0000");
--                              idle => '0');
procedure UNIFORM(variable SEED1,SEED2:inout INTEGER;variable X:out INTEGER);
end typelib;                              

package body typelib is
procedure UNIFORM(variable SEED1,SEED2:inout INTEGER;variable X:out INTEGER) is
        variable Z,K: INTEGER range 0 to 8191;
        variable TSEED1 : INTEGER range 0 to 8191:= INTEGER'(SEED1);
        variable TSEED2 : INTEGER range 0 to 8191:= INTEGER'(SEED2);
   begin
        K := TSEED1/6709;
   TSEED1 := 5002 * (TSEED1 - K * 6709) - K * 1527;

   if TSEED1 < 0  then
           TSEED1 := TSEED1 + 8192;
   end if;

   K := TSEED2/6597;
   TSEED2 := 5093 * (TSEED2 - K * 6597) - K * 3791;

   if TSEED2 < 0  then
           TSEED2 := TSEED2 + 8192;
   end if;

   Z := TSEED1 - TSEED2;
   if Z < 1 then
           Z := Z + 8192;
   end if;
        -- Get output values
        SEED1 := INTEGER'(TSEED1);
        SEED2 := INTEGER'(TSEED2);
        X :=  INTEGER'(Z mod 8);
    end UNIFORM;
end typelib;