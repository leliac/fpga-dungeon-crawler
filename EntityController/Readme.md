    Port (
    
    command_in: in STD_LOGIC_VECTOR(2 downto 0);
    
    coord_x : in NATURAL;
    
    coord_y: in NATURAL;
    
    sprite_id : in STD_LOGIC_VECTOR(6 downto 0);
    
    dir: in STD_LOGIC_VECTOR(3 downto 0);  
    
    damage: in NATURAL;            
    
    mov: out STD_LOGIC; --1 if sprite can move            
    
    sprite_out: out STD_LOGIC_VECTOR(6 downto 0);            
    
    coord_xout: out NATURAL; --coords of sprite at begin of movement            
    
    coord_yout: out NATURAL;            
    
    coord_xdest: out NATURAL; --coord of sprite at end of movement            
    
    coord_ydest: out NATURAL;            
    
    destroyed: out STD_LOGIC; --1 if sprite has been destroyed            
    
    );



command_in 3 bit: in (crea, distruggi, muovi, danno, leggi)

command data: in (coordX, coordY)

command_out: {
    1 bit: movimento sì/no (0=sì, 1=no)
    3 bit: direzione/comando
    7 bit: spriteID
    7 bit: coordX
    7 bit: coordY
    #Extra?
} => tot 25 bit + extra


contenuto ram

coordX_in 7 bit

coordY 7 bit

spriteID 7 bit

health 2 bit

direction 4 bit

animation 1 bit (enable per il movimento)


-----------------------------------------------------------------------

SIGNAL SEED1,SEED2: INTEGER RANGE 0 to 8191 (changable, beliving that 
overflowing returns SEED mod 8192):= first_map_index/2;

