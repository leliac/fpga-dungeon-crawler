----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/29/2017 12:38:46 PM
-- Design Name: 
-- Module Name: checkDir - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- computes the best direction to reach the object and outputs it to be checked
-- by the collisionChecker, while pending the direction is 1111 
-- The clever AI adjusts the direction until it can move, after some iterations
-- it stops (that means the mob cannot move in any direction)
-- The dumb one finds wether it can reach the target using the same direction and
-- stops if it can not or if the target is too far (30 steps)
-- Each time the collision checker is called it must be passed the coordinates and 
-- the direction computed here, then should wait for a result to continue
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.typelib.ALL;
use IEEE.NUMERIC_STD.ALL;

entity entityController is
    Port ( clock : in STD_LOGIC;
           enable : in STD_LOGIC; --used to track frames
           enable_mob : in STD_LOGIC;
           enable_pl : in STD_LOGIC;
           player_pos : in STD_LOGIC; --flag to use find coord
           
           command_pl : in std_logic_vector (2 downto 0); --input from player
           dir_pl : in STD_LOGIC_VECTOR (3 downto 0) := (others=>'0');
           findX : in INTEGER range 0 to 80;
           findY : in INTEGER range 0 to 60;
           lifepoints : in INTEGER range 0 to 7 := 0; --to create or damage
           
           isFree : in STD_LOGIC; --received from collisionChecker
           ready : in STD_LOGIC;
           dirToTest :out STD_LOGIC_VECTOR (3 downto 0) := (others=>'0');
           c_enable : out STD_LOGIC := '0';
           testX : out INTEGER range 0 to 80; --to pass to collisionChecker
           testY : out INTEGER range 0 to 60;
           
           id_out : out STD_LOGIC_VECTOR (6 downto 0);
           x_out : out INTEGER range 0 to 80;
           y_out : out INTEGER range 0 to 60;
           command_anim : out STD_LOGIC_VECTOR (3 downto 0); --to animator
           move : out STD_LOGIC);
end entityController;
--           mob_out: out mob;
--           mobs_alive: out natural range 0 to 100;
--           traps_on:out natural range 0 to 99;
--           --index_out: out NATURAL range 0 to 101;
--           damage: in NATURAL range 0 to 3;
--           level: in natural range 0 to 99;
--           dir: in std_logic_vector (3 downto 0));

-- moving directions
--   2
-- 1   3
--   0
-- right - up -  left - down
---------------------------------
-- mob type
-- cleverness - attack type - random
-- 100 is clever short attack
-- 000 is dumb short attack
-- 010 is dumb long attack
-- 001 is ramdomly moving short
-- 011 is ramdomly moving long
-- 110 not implemented but may be too strong this way
-- 1x1 may be avoided
---------------------------------
architecture Behavioral of entityController is
function computeDir(targetX,targetY,thisX,thisY : INTEGER)
return STD_LOGIC_VECTOR is
variable dir : STD_LOGIC_VECTOR (3 downto 0); 
begin
  if thisX<targetX then
    if thisY<targetY then
      dir := "1001"; --go down-right
    elsif thisY>targetY then
      dir := "1100"; --go up-right
    else --if thisY=targetY
      dir := "1000"; --go right
    end if;
  elsif thisX>targetX then
    if thisY<targetY then
      dir := "0011"; --go down-left
    elsif thisY>targetY then
      dir := "0110"; --go up-left
    else --if thisY=targetY
      dir := "0010"; --go left
    end if;
  else --if thisX=targetX
    if thisY<targetY then
      dir := "0001"; --go down
    elsif thisY>targetY then
      dir := "0100"; --go up
    else --if thisY=targetY
      dir := "0000"; --found
    end if;
  end if; 
  return dir;
end function;

constant NUM_MOBS : INTEGER := 50;
--constant NUM_GEN : INTEGER := 25;
--sprite IDs
constant HERO : STD_LOGIC_VECTOR (6 downto 0) := "0000001";
constant ARCHER : STD_LOGIC_VECTOR (6 downto 0) := "0000010";
--..--
signal ai_ena : STD_LOGIC := '0';
signal ind : INTEGER range 0 to NUM_MOBS+1:= 0;
signal damage_mob : NATURAL range 0 to 3;
signal dir_mob : STD_LOGIC_VECTOR (3 downto 0) := (others=>'0');
signal command_mob : STD_LOGIC_VECTOR (2 downto 0) := (others=>'0');
signal speedCnt : INTEGER range 0 to 60 := 0;

type mobarray is array(0 to NUM_MOBS) of mob;
signal ea : mobarray := (others=>EmptyEntity);
--type mobram is array (0 to NUM_GEN-1) of INTEGER range 0 to 7;
--signal genmob:mobram;
--attribute ram_style: string;
--attribute ram_style of ea,genmob: signal is "distributed";
signal score_r,score_m: INTEGER range 0 to 999999:=0;
--signal mobsal:natural range 0 to 100:=0;
signal trapon:INTEGER range 0 to NUM_MOBS:=0;

impure function canMove(id : STD_LOGIC_VECTOR (6 downto 0)) return BOOLEAN is
begin --should set speed on multiples of 5 depending on the id
--  if (speedCnt mod 5)=0 then --minimum lasting time
--    return true;
--  end if;
--  return false;
  return true;
end function; 

impure function getIndex(x : in NATURAL range 0 to 80; y : in NATURAL range 0 to 60; choice : std_logic) return integer is
variable out_data : integer range 0 to NUM_MOBS := NUM_MOBS;
begin
    for i in 0 to NUM_MOBS loop
        if i=NUM_MOBS or --missed 
        (((ea(i).x=x and ea(i).y=y) or (ea(i).x=x-1 and ea(i).y = y) or (ea(i).x=x and ea(i).y=y-1) or (ea(i).x=x-1 and ea(i).y=y-1))
         and choice='0') or (ea(i).life=0 and choice='1' and i/=0)  then
            out_data := i;
            exit;
        end if;
    end loop;
return out_data;
end function;

begin

--  create_array : process(player_pos,clock) --on each level change
  	
--  	variable re1 : integer;
--  	variable seed1 :positive:=10;
--  	variable seed2 :positive:=1;
--  	variable i: integer range 0 to NUM_GEN:=0;
--  	variable start : boolean := false;
--  	begin
--  	if rising_edge(player_pos) then
--      start:=true;
----      seed1:= seed1;
----      seed2:= seed2;
--      if i=NUM_GEN then
--        start:=false;
--      end if;
--    end if;

--    if rising_edge(clock) then
--      if not start then
--        i:=0;
--      elsif i<NUM_GEN then
--        uniform(seed1,seed2,re1);
--        genmob(i) <= re1;
----        genmob(i) <= 5-i; --try
--        i:=i+1;
--      end if;
--    end if;
--  end process;
  
  frm_cnt : process(enable)
  begin --counting frames to set speed
    if rising_edge(enable) then
      speedCnt <= (speedCnt+1) mod 60;
    end if;
  end process;
  
  starting : process(clock) --selecting the mob for the ia
  variable first : BOOLEAN := true;
  variable done : BOOLEAN := false;
  variable i : INTEGER range 0 to NUM_MOBS;
  begin
    if rising_edge(clock) then
      if enable='0' then
        ind<=1; --0 is the player
        done:=false;
      elsif enable_mob='0' then
        first := true;
        ai_ena <= '0'; --stop the ai
      elsif not done then
        if first then
          for i in 0 to NUM_MOBS loop --find the first to compute
            if i+ind=NUM_MOBS or ea(i+ind).spriteID="000" then
              done:=true; --no more mobs
              exit;
            end if;
            if canMove(ea(i+ind).spriteID) and ea(i+ind).life>0 then
              ind<=i+ind;
              exit;
            end if;
          end loop;
        else --leave the time to load the data 
          ai_ena <= '1'; --start the ai
        end if;
        first:=false;
      end if;
    end if;
  end process;

  adjust : process(clock,ai_ena) --actual artificial intelligence
  variable computing,rand,first,toSend : BOOLEAN := true;
  variable x,y,tmpX,tmpY : INTEGER range -81 to 161;
  variable thisType : STD_LOGIC_VECTOR (2 downto 0) := (others=>'0');
  variable tmpdir,bestdir : STD_LOGIC_VECTOR (3 downto 0) := (others=>'1'); --to init
  variable cnt : INTEGER range 0 to 31 := 0;
  variable currMob : mob;
  begin
    
    if ai_ena='0' then
      --update/init variables
        currMob := ea(ind);
        if thisType(2)='1' then --clever Ai
          x:=ea(0).x;
          y:=ea(0).y; 
          tmpX:=x; tmpY:=y;
        elsif thisType(2)='0' then --dumb Ai
          x:=currMob.x;
          y:=currMob.y;
        end if;
        computing := true; --needs to compute
        rand := false;
        first := true; --track first computation of this ai
--        toSend:=true; --send the command only for a clock cycle
        bestdir := computeDir(ea(0).x,ea(0).y,currMob.x,currMob.y);
        tmpdir := bestdir; --first to try
        cnt := 0;
        command_mob<="000"; --empty command while disabled
        --set thisType based on currMob.spriteID
        thisType:="100";
    elsif falling_edge(clock) then --only work when needed
--      if not computing then
--        command_mob<="000"; --after first time stop sending command
      if abs(currMob.x-ea(0).x)<=2 and abs(currMob.y-ea(0).y)<=2 then
        --dir_mob <= (others=>'0'); --stop while found
        dir_mob <= bestdir; --send attach direction
        c_enable <= '0'; --prevent from starting
--        if thisType(1)='1' then
        command_mob<="011"; --attack
        --toSend:=false; --send the command only for a clock cycle
        damage_mob<=1;
--        end if;
        computing:=false;
      elsif computing and ready='1' then
        if thisType(2)='1' then --clever Ai
          if first then --first time passes and computes data
            c_enable <= '0'; --stop the collision checker
            first := false; --the signals are ready
            if isFree='0' and cnt<10 then --cannot move
              if cnt/=0 then --change from the second time on
                if (bestdir(3) xor bestdir(2) xor bestdir(1) xor bestdir(0))='0' then --diagonal movements
                  if (cnt mod 3)=1 then
                    x := tmpX; y := currMob.y; --first try to adjust horizontally
                  elsif (cnt mod 3)=2 then
                    x := currMob.x; y := tmpY; --then vertically
                  elsif (cnt mod 3)=0 then
                    if (cnt mod 2)=0 then --change diagonal direction
                      x := 2*currMob.x-ea(0).x; y := ea(0).y;
                    else
                      x := ea(0).x; y := 2*currMob.y-ea(0).y;
                    end if;
                    tmpX:=x; tmpY:=y; --save temporary target position
                  end if;
                else --pure movement
                  if (cnt mod 3)=1 then
                    if tmpY=currMob.y then --horizontal move
                      y := tmpY + 20; --try diagonally on one side
                    else --vertical move
                      x := tmpX + 20; --try diagonally on one side
                    end if;
                  elsif (cnt mod 3)=2 then
                    if tmpY=currMob.y then
                      y := tmpY - 20; --try the other
                    else
                      x := tmpX - 20; --try the other
                    end if;                
                  elsif (cnt mod 3)=0 then
                    if (cnt mod 2)=0 then --change pure direction
                      if ea(0).y=currMob.y then
                        x := currMob.x; y := ea(0).y+ 20;
                      else
                        x := ea(0).x + 20; y := currMob.y;
                      end if;
                    else
                      if ea(0).y=currMob.y then
                        x := currMob.x; y := ea(0).y - 20;
                      else
                        x := ea(0).x - 20; y := currMob.y;
                      end if;                  
                    end if;
                    tmpX:=x; tmpY:=y; --save temporary target position
                  end if;
                end if;
              end if;
              tmpdir := computeDir(x,y,currMob.x,currMob.y);
              dir_mob <= (others=>'1'); --correct direction still not found
              cnt := cnt + 1;
            else
              if cnt=10 then
                dir_mob <= (others=>'0'); --stop trying, there's no way out
                command_mob<="000"; --idle
              else
                dir_mob <= tmpdir; --now it's correct
                command_mob<="010"; --move
              end if;
              computing := false; --found result
              c_enable <= '0'; --no more needed
            end if;
            dirToTest <= tmpdir; --send this
            testX <= currMob.x; testY <= currMob.y; --no change here          
          else --after having computed start all
            first := true; --for next time
            c_enable <= '1'; --start the collision checker
          end if;          
        elsif thisType(2)='0' then --dumb Ai
          if first then
            c_enable <= '0'; --let time to load the data
            first := false;
            if not rand then
              if isFree='1' and cnt<20 then --still need to compute path
                if tmpdir(3)='1' then --update the position
                  x := x + 1;
                elsif tmpdir(1)='1' then
                  x := x - 1;
                end if;
                if tmpdir(2)='1' then --update the position
                  y := y - 1;
                elsif tmpdir(0)='1' then
                  y := y + 1;
                end if;
                tmpdir := computeDir(ea(0).x,ea(0).y,x,y);
                if abs(x-ea(0).x)<=2 and abs(y-ea(0).y)<=2 then
                  dir_mob <= bestdir; --can reach it, pass initial direction
                  if thisType(1)='1' then
                    if currMob.buldir="0000" then --can shot
                      command_mob<="110"; --shot
                      damage_mob<=1;
                    else
                      command_mob<="010"; --move instead (opposite direction)
                      dir_mob <= bestdir(1 downto 0) & bestdir(3 downto 2);
                    end if;
                  else
                    command_mob<="010"; --move
                  end if;
                  computing := false; --found result
                else
                  dir_mob <= (others=>'1'); --still pending
                end if;
              elsif cnt/=0 then --always need to compute the first time
                tmpdir := (others=>'0');
                if thisType(0)='1' then
                  rand := true; --compute random dir
                else
                  dir_mob <= (others=>'0'); --there's an obstacle, stop
                  command_mob <= "000"; --idle
                  computing := false; --found result
                end if;
              end if;
            else --compute random dir
              if tmpdir="0000" then
                tmpdir := bestdir; --first try the best
                cnt:=0; --reinit there
              end if;
              x:=currMob.x; y:=currMob.y; --restore position
              if isFree='0' and cnt<3 then
                tmpdir := tmpdir(2 downto 0) & tmpdir(3); --rotate
                cnt := cnt+1;
              else
                if cnt=3 then
                  dir_mob <= (others=>'0'); --stop trying, there's no way out
                  command_mob <= "000"; --idle
                else
                  dir_mob <= tmpdir; --now it's correct
                  command_mob<="010"; --move
                end if;
              computing := false; --found result
              c_enable <= '0'; --no more needed
              rand := false;          
              end if;
            end if;
            cnt := cnt + 1;
            dirToTest <= tmpdir; --send this
            testX <= x; testY <= y; --pass updated
          else
            first := true; --for next time
            c_enable <= '1'; --stop the collision checker
          end if;
        end if;
      end if;
    end if;
  end process;
    
  inout_command:process (clock) --execute and prepare commands for the animator
  variable pointer,pnt1,pnt2 : INTEGER range 0 to NUM_MOBS := 0;
  variable damage: NATURAL range 0 to 7 := 0;
  variable q_pointer,s_pointer,inc : NATURAL range 0 to 3 := 0;
  variable cmd : STD_LOGIC_VECTOR (2 downto 0) := (others=>'0');
  variable dir,q0,q1,q2 : STD_LOGIC_VECTOR (3 downto 0) := (others=>'0');
  variable done : BOOLEAN := false;
  variable x,y : INTEGER range 0 to 80;
  variable seed1,seed2,res : integer;
  type stack is array (0 to 2) of INTEGER range 0 to 7; 
  variable mStack : stack := (others=>0); --mob stack for HUD (may be output)
  begin
  
    if rising_edge(clock) then
      if player_pos='1' then --on level change
        for i in 1 to NUM_MOBS-1 loop
          ea(i)<=emptyEntity; --reinitialize
        end loop;
        ea(0).x<=findX; --use these
        ea(0).y<=findY; --use these
        ea(0).spriteID<=HERO; --may put correct
        ea(0).life<=5;
        for i in 0 to 2 loop --fill initial stack
          uniform(seed1,seed2,res);
          mStack(i):=res;
        end loop;
        s_pointer:=0;
      elsif (enable_mob or enable_pl)='0' then
        cmd:="000";
        done:=false;
        q0:=(others=>'0'); --reinit queue
        q1:=(others=>'0'); --queue for commands to send out
        q2:=(others=>'0');
        pnt1:=0; pnt2:=0;
      elsif enable_mob='1' then
        pointer := ind; --ind has already advanced
        dir:=dir_mob;
        cmd:=command_mob;
        damage:=damage_mob;
      else --enable_pl='1'
        pointer := 0; --player
        dir:=dir_pl;
        cmd:=command_pl;
        damage:=lifepoints;
      end if;
      
      id_out <= ea(pointer).spriteID; --send out first
      x_out <=ea(pointer).x;
      y_out <=ea(pointer).y;
      if cmd="011" then --on attack
        inc:=2; --could attack itself otherwise
      else
        inc:=1;
      end if;
      if dir(3)='1' then --right
        x := ea(pointer).x + inc;
      elsif dir(1)='1' then --left
        x := ea(pointer).x - inc;
      else
        x := ea(pointer).x;
      end if;
      if dir(2)='1' then --up
        y := ea(pointer).y - inc;
      elsif dir(0)='1' then --down
        y := ea(pointer).y + inc;
      else
        y := ea(pointer).y;
      end if;
      if not done then --do it only once
        move<='0'; --default
        done:=true; --set as default
        q_pointer:=0; --restart queue 
        case cmd is
          when "000" =>
            done:=false; --nothing has been done
          when "001" => -- Create
            pointer:=getIndex(0,0,'1'); --get the first free
            ea(pointer).x <= findX;
            ea(pointer).y <= findY;
            ea(pointer).life <= lifepoints;
            uniform(seed1,seed2,res);
            ea(pointer).spriteID <= std_logic_vector(to_unsigned(mStack(s_pointer),7)); --pick it
            id_out <= std_logic_vector(to_unsigned(mStack(s_pointer),7)); --send out new
            x_out <=findX;
            y_out <=findY;
            mStack(s_pointer):=res; --substitute just used
            s_pointer:=(s_pointer+1) mod 3; --advance for next time
            --genCnt := (genCnt + 1) mod NUM_GEN;
            --mobsal<=mobsal+1;
            q0 := '0' & cmd;
          when "011" => -- attack
            pointer := getIndex(x,y,'0');
            if pointer<NUM_MOBS then --no mob
              q0 := dir(1) & cmd; --send also the flip
              if(ea(pointer).life-damage>0) then --reduce life
                ea(pointer).life <= ea(pointer).life - damage; --first animation will be attack
              else --die
                q1 := "0100"; --send as second in the queue
                pnt1:=pointer; --will affect this one
                ea(pointer).life<=0;
                if pointer=0 then
                --GAME OVER
                end if;
    --            ea(pointer).idle<='1'; --it's animating
              end if;
            end if;
--          when "100" => -- Destroy
--            pointer := getIndex(x,y,'0');
--            if pointer<NUM_MOBS then
--              ea(pointer).life <= 0;
--              q0 := '0' & cmd;
--            end if;
  --          ea(pointer).idle<='1';
          when "110" => --shot
            ea(pointer).bulx<=x; --put the bullet in first position
            ea(pointer).buly<=y;
            ea(pointer).buldir<=dir;
            q0 := dir(1) & "011"; --like attack
          when "010" => --move
            ea(pointer).x <= x;
            ea(pointer).y <= y;
  --          ea(pointer).idle<='1';
            move<='1';
            q0 := dir;
          when others => null;
        end case;
      end if;
      if q0="0000" then
        q_pointer:=1; --do not transmit if empty
      end if;
      if q1="0000" and q_pointer=1 then
        q_pointer:=2; --do not transmit if empty
      end if;
      if q_pointer=0 then
        command_anim<=q0; --send to animator
      elsif q_pointer=1 then
        command_anim<=q1; --send to animator
        id_out <= ea(pnt1).spriteID; --send this out
        x_out <=ea(pnt1).x;
        y_out <=ea(pnt1).y;
      elsif q_pointer=2 then
        command_anim<=q2; --send to animator
        id_out <= ea(pnt2).spriteID; --send this out
        x_out <=ea(pnt2).x;
        y_out <=ea(pnt2).y;
      end if;
      q_pointer:=q_pointer+1; --advance for next time
    end if;
  end process;  

end Behavioral;