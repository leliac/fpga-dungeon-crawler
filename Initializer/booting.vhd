----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/22/2017 03:16:37 PM
-- Design Name: 
-- Module Name: booting - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- This module starts in the boot time, the board waits for the other
-- taking care the mode is correct, while waiting it counts the time,
-- this number can be used as random being multiplied by the one
-- obtained from the other board (to share the same constant). This calc
-- is done in the top module, one of the 2 numbers will be 1 (the lowest)
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

entity booting is
    Port ( clock : in STD_LOGIC;
           mode : in STD_LOGIC;
           final_rand : in INTEGER range 0 to 80;
           ch_rx0 : in STD_LOGIC;
           ch_rx1 : in STD_LOGIC;
           ch_tx0 : out STD_LOGIC;
           ch_tx1 : out STD_LOGIC;
           Xmap : out INTEGER range 0 to 80;
           Ymap : out INTEGER range 0 to 60;
           write_enable : out STD_LOGIC;
           tile_out : out STD_LOGIC_VECTOR (7 downto 0);
           tmp_rand : out INTEGER range 0 to 80);
end booting;

architecture Behavioral of booting is
constant MAPS_NUM : INTEGER := 80;
begin

  initiating : process(clock)
  variable cnt : NATURAL := 0;
  variable rand : INTEGER range 0 to 80 := 0;
  variable syncing : BOOLEAN := true;
  variable initMap : BOOLEAN := false;
  begin

    if syncing then
      if mode='1' then
        ch_tx0 <= 'Z'; --send idle
        ch_tx1 <= '1'; --send communication
        if ch_rx0='1' then --other board is trying to communicate
          syncing := false;
        end if;
      else
        ch_tx0 <= '1'; --send communication
        ch_tx1 <= 'Z'; --send idle
        if ch_rx1='1' then --other board is trying to communicate
          syncing := false;
        end if;      
      end if;
      cnt := cnt+1; --advancing
      tmp_rand <= 0; --still waiting
    elsif final_rand=0 then --connection established
      tmp_rand <= (cnt mod MAPS_NUM) + 1;
    elsif not initMap then
      rand := final_rand - 1; --to use 0 as idle the constant is summed by 1
      --chosenMap := allMaps(rand); --to define how to take it
      cnt := 0;
      initMap := true;
    elsif cnt<4800 then --start initializing the map
      if falling_edge(clock) then
        Xmap <= cnt mod 80;
        Ymap <= cnt / 80;
        --tile_out <= chosenMap(cnt)(29 downto 0); --to define how to take it
        write_enable <= '1';
        cnt := cnt+1;
      end if;
    else
      write_enable <= '0';
    end if;
    
    if rand/=(final_rand-1) and rand/=0 then
      initMap := false; --reinit if random constant changes
    end if;

  end process;

end Behavioral;