----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/22/2017 03:16:37 PM
-- Design Name: 
-- Module Name: booting - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- This module starts in the boot time, the board waits for the other
-- taking care the mode is correct, while waiting it counts the time,
-- this number can be used as random being multiplied by the one
-- obtained from the other board (to share the same constant). This calc
-- is done in the top module, one of the 2 numbers will be 1 (the lowest)
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL; --needed to transform integer 'rand' into std_logic_vector 'map_id'
entity booting is
    Port ( clock : in STD_LOGIC;
           mode : in STD_LOGIC;
           final_rand : in INTEGER range 0 to 80;
           ch_rx0 : in STD_LOGIC;
           ch_rx1 : in STD_LOGIC;
           ch_tx0 : out STD_LOGIC;
           ch_tx1 : out STD_LOGIC;
           Xmap : out INTEGER range 0 to 80;
           Ymap : out INTEGER range 0 to 60;
           write_enable : out STD_LOGIC;
           tile_out : out STD_LOGIC_VECTOR (18 downto 0); --number of tile bits changed from 8 to 19
           tmp_rand : out INTEGER range 0 to 80;
           
           --AXI Data Fetcher ports
           map_transfer : out STD_LOGIC; --set to 1 to start transfer, then set back to 0 before transfer is over
           map_id : out STD_LOGIC_VECTOR (7 downto 0); --relates to map position number in SD file (thus in DDR)
           tile_in : in STD_LOGIC_VECTOR (31 downto 0); --entire 32-bit data packet, discard useless bits
           tile_in_clock : in STD_LOGIC --rises every time a new data packet is sent
    );
end booting;
architecture Behavioral of booting is
constant MAPS_NUM : INTEGER := 80;
begin
  initiating : process(clock)
  variable cnt,cntMap : NATURAL := 0;
  variable rand : INTEGER range 0 to 80 := 0;
  variable syncing : BOOLEAN := true;
  variable initMap : BOOLEAN := false;
  
  begin
    if rising_edge(clock) then
      if syncing then
        map_transfer <= '0'; --don't transfer map yet
        if mode='1' then
          ch_tx0 <= '0'; --send idle
          ch_tx1 <= '1'; --send communication
          if ch_rx0='1' then --other board is trying to communicate
            syncing := false;
          end if;
        else
          ch_tx0 <= '1'; --send communication
          ch_tx1 <= '0'; --send idle
          if ch_rx1='1' then --other board is trying to communicate
            syncing := false;
          end if;      
        end if;
        cnt := cnt+1; --advancing
        tmp_rand <= 0; --still waiting
      elsif final_rand=0 then --connection established
        tmp_rand <= (cnt mod MAPS_NUM) + 1;
      elsif not initMap then
        rand := final_rand - 1; --to use 0 as idle the constant is summed by 1
        -- How is 'map_id' to be determined from 'rand'? Is it simply 'map_id' = 'rand'?
        map_id <= std_logic_vector(to_unsigned(rand, map_id'length));
        map_transfer <= '1'; --start map transfer
        if cntMap=1 then
          map_transfer <= '0'; --lower enable signal, otherwise a new transfer will start when the current one ends
        end if;
        initMap := true;
      end if;
      
      if rand/=(final_rand-1) and rand/=0 then
        initMap := false; --reinit if random constant changes
      end if;
    end if;
    --transfer from map (supposing slower clock)
    if rising_edge(tile_in_clock) then
      if not initMap then
        cntMap:=0; --initialize while disabled
      else
        if cntMap<4800 then
          Xmap <= cntMap mod 80;
          Ymap <= cntMap / 80; --why 80? Isn't the map size 80x60? Indeed.
          tile_out <= tile_in(18 downto 0); --select significant bits only
          write_enable <= '1';
          cntMap := cntMap+1;
        elsif cntMap=4800 then
          write_enable <= '0';
        end if;
      end if;
    end if;
  end process;
end Behavioral;