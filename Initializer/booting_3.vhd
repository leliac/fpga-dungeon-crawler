----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/22/2017 03:16:37 PM
-- Design Name: 
-- Module Name: booting - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- This module starts in the boot time, the board waits for the other
-- taking care the mode is correct, while waiting it counts the time,
-- this number can be used as random being multiplied by the one
-- obtained from the other board (to share the same constant). This calc
-- is done in the top module, one of the 2 numbers will be 1 (the lowest)
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL; --needed to transform integer 'rand' into std_logic_vector 'map_id'
entity booting is
    Port (
        clock : in STD_LOGIC; --using same clock for ps and pl
        mode : in STD_LOGIC;
        final_rand : in INTEGER range 0 to 80;
        ch_rx0 : in STD_LOGIC;
        ch_rx1 : in STD_LOGIC;
        ch_tx0 : out STD_LOGIC;
        ch_tx1 : out STD_LOGIC;
        Xmap : out INTEGER range 0 to 80;
        Ymap : out INTEGER range 0 to 60;
        write_enable : out STD_LOGIC;
        tile_out : out STD_LOGIC_VECTOR (18 downto 0);
        tmp_rand : out INTEGER range 0 to 80;
           
        --AXI Data Fetcher ports
        map_transfer : out STD_LOGIC; --set to 1 to start map transfer
        map_id : out STD_LOGIC_VECTOR (7 downto 0); --map index
        tile_in : in STD_LOGIC_VECTOR (31 downto 0); --32-bit tile (discard useless bits)
        fetching : in STD_LOGIC --is 1 while tiles are being tranfered
    );
end booting;
architecture Behavioral of booting is
constant MAPS_NUM : INTEGER := 80;
constant pattern : STD_LOGIC_VECTOR (3 downto 0) := "1110";
signal pr, pt : INTEGER;
signal s : BOOLEAN;
begin
    initiating : process(clock)
    variable cnt : NATURAL := 0;
    variable rand : INTEGER range 0 to 80 := 0;
    variable syncing : BOOLEAN := true;
    variable initMap : BOOLEAN := false;
    variable pnt_rx, pnt_tx : INTEGER range 0 to 3 := 0; --indexes in the pattern
    variable transm : INTEGER range 0 to 20 := 0; --used to slow transmission
  
    begin
        if rising_edge(clock) then
          if syncing then
            map_transfer <= '0'; --don't transfer map yet
            if transm=1 then
              if mode='1' then
                ch_tx0 <= '0'; --send idle
                ch_tx1 <= pattern(pnt_tx); --send communication
                pnt_tx:=(pnt_tx+1) mod 4; --send all sequence
                if ch_rx0=pattern(pnt_rx) then --other board is trying to communicate
                  pnt_rx:=pnt_rx+1; --advance in sequence
                elsif ch_rx0=pattern(0) then --has restarted on the other side
                  pnt_rx:=1;
                else
                  pnt_rx:=0; --restart
                end if;
              else
                ch_tx0 <= pattern(pnt_tx); --send communication
                ch_tx1 <= '0'; --send idle
                pnt_tx:=(pnt_tx+1) mod 4; --send all sequence
                if ch_rx1=pattern(pnt_rx) then --other board is trying to communicate
                  pnt_rx:=pnt_rx+1; --advance in sequence
                elsif ch_rx1=pattern(0) then --has restarted on the other side
                  pnt_rx:=1;
                else
                  pnt_rx:=0; --restart
                end if;      
              end if;
            end if;
            transm:=(transm+1) mod 20;
            if pnt_rx=4 then --sequence recognized
              syncing := false;
            end if;
            cnt := cnt+1; --advancing
            tmp_rand <= 0; --still waiting
          elsif final_rand=0 then --connection established
            tmp_rand <= (cnt mod MAPS_NUM) + 1;
          elsif not initMap then
            rand := final_rand - 1; --to use 0 as idle the constant is summed by 1
            cnt:=0;
            map_id <= std_logic_vector(to_unsigned(rand, map_id'length)); --is the desired behavior?
            map_transfer <= '1'; --start map transfer
            initMap := true;
          
          --Map transfer
          elsif fetching='1' then
            if cnt<4800 then
              Xmap <= cnt mod 80;
              Ymap <= cnt / 80;
              tile_out <= tile_in(18 downto 0); --selecting significant bits only
              write_enable <= '1';
              cnt := cnt+1;
            elsif cnt=4800 then
              write_enable <= '0';
              map_transfer <= '0'; --this isn't strictly necessary anymore (unless a new transfer is required)
            end if;
          end if;
    
          if rand/=(final_rand-1) and rand/=0 then
            initMap := false; --reinit if random constant changes
          end if;
        end if;
        pt<=pnt_tx; pr<=pnt_rx;
        s<=syncing;  
    end process;
end Behavioral;