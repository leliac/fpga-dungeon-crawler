Presentazione:

Il modulo serve per la gestione delle animazioni sulla TileMap.
Esso prende in ingresso il comando arrivato dall'EntityController, 
lo mette in coda e alla fine di ogni fotogramma (enW collegato al modulo VGA) lo scrive sulla TileMap,
utlizzando un'altra coda per mandare una per clock le 4 tile del fotogramma.


Dati in ingresso:
    
    enable      => 1=comando in ingresso; 0=altrimenti
    
    move        => 1=movimento; 0=altra animazione
    
    spriteID
    
    coordX
    
    coordY
    
    command_in  => se move=1 => direzione movimento; se move=0 => comando
    
    enW         => 1=fine rendering fotogramma, puoi scrivere sulla TileMap; 0=non puoi scrivere sulla TileMap
    
    full        => 1=coda piena; 0=altrimenti
    
LEGENDA COMANDI:

    move=1 indica un movimento; command_in indica la direzione:
        
        "1001" => --down-right
        
        "1100" => --up-right
        
        "1000" => --right
        
        "0011" => --down-left
        
        "0110" => --up-left
        
        "0010" => --left
        
        "0001" => --down
        
        "0100" => --up
    
    
    move=0 indica gli altri comandi; command_in indica il comando:
        
        "0001" =>  --create
        
        "0011" =>  --damage
        
        "0100" =>  --destroy
        
        "0110" =>  --attack




Dati in output:

    full        => 1=coda piena; 0=altrimenti
    
    tile        => Tile in output (collegata a TileMap)
    
    coordX_out
    
    coordY_out


Descrizione del funzionamento generale:


La struttura principale utilizzata è un array di STD_LOGIC_VECTOR, con tecnologia FIFO, in distributed RAM impostato come segue:

35 bit totali suddivisi in 

    Parsed          => 1 bit    => 0=ancora da scrivere per il fotogramma corrente; 1=già scritto;
    
    SpriteID        => 7 bit
    
    Move            => 1 bit
    
    Command         => 4 bit
    
    CoordX          => 7 bit
    
    CoordY          => 7 bit
    
    CurrentFrame    => 4 bit    => indica il a quale frame dell'animazione siamo
    
    TotFrames       => 4 bit    => indica il numero totale di frame che può durare l'animazione


Poi vi è la struttura che contiene le Tile da mandare ad ogni clock alla TileMap, anche qui gestita con tecnologia FIFO,
che è semplicemente un Array di 4 elementi STD_LOGIC_VECTOR che contengono le 4 Tile con coordinate dell'ultimo fotogramma analizzato.



Al rising edge del clock, se l'enable è settato a 1 e la coda ancora non è piena (full/='1') allora mette in coda il comando, seguendo le seguenti operazioni:

    1- definisce il numero di frame che dovrà durare l'animazione
    
    2- mette in coda il comando come segue
    
        {     "0"       => da scrivere per il fotogramma corrente
        
            & spriteID
            
            & move
            
            & command_in
            
            & coordX_in (convertito in 7 bit)
            
            & coordY_in (convertito in 7 bit)
            
            & "0000"    => comincia dal frame 0
            
            & totFrames
            
        }
        
    3- aggiorna gli indici di Read e Write della coda, controllando se è piena
    
    

Al falling edge del clock, controlla se l'array che contiene le Tile è vuoto. Se è vuoto agisce come segue

    1- controlla se la coda dei comandi è vuota. Se non lo è e se il prossimo comando è ancora da analizzare, prende il prossimo comando
    
    2- analizza il comando e genera le 4 tile di conseguenza
    
    3- se siamo all'ultimo frame, genererà le 4 tile dell'animazione completata (togliendo quindi eventuali effetti)
    
    4- rimette in coda il comando, con il bit parsed settato a 1 e il currentFrame aggiornato (a meno che non siamo sull'ultimo frame, in tal caso non lo mette in coda)
    
Dopo di ché il processo controlla che l'array di Tile sia ancora vuoto. Se non lo è, manda in output la prossima Tile (seguendo l'indice di read).


Infine, al falling edge dell'enW (e quindi quando è finito il rendering del fotogramma corrente), il processo setta a 0 tutti i bit "parsed" della coda, 
in modo tale da poterli scrivere sulla TileMap al prossimo fotogramma. Inoltre setta rdT a 0 in modo da ricominciare con le Tile (eventualmente) ancora non mandate,
ed avere un'animazione completa pur saltando un fotogramma. Possibilità rara (dati i numerosi cicli di clock tra un fotogramma e l'altro), ma comunque gestita.
