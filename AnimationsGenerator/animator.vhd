----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 14.05.2017 17:56:53
-- Design Name: 
-- Module Name: Animator - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity Animator is
  Port (   clk: in STD_LOGIC;
           enable: in STD_LOGIC;
           move: in STD_LOGIC;
           spriteID: in STD_LOGIC_VECTOR(7 downto 0);
           coordX_in: in INTEGER range 0 to 80;
           coordY_in: in INTEGER range 0 to 60;
           command_in: in STD_LOGIC_VECTOR(3 downto 0);
           enW: in STD_LOGIC;
           tile: out STD_LOGIC_VECTOR(18 downto 0);
           coordX_out: out INTEGER range 0 to 80;
           coordY_out: out INTEGER range 0 to 60 );
end Animator;

architecture Behavioral of Animator is

--constants
constant MAX_ELEMENTS : INTEGER range 0 to 127   := 100;
constant MAX_TILES : INTEGER range 0 to 15        := 8;
constant N_BITS_PER_CMD : INTEGER range 0 to 63  := 36;                 --Parsed & CurrentFrame & TotFrames & SpriteID & Move & Command & CoordX & CoordY
constant N_BITS_TILE : INTEGER range 0 to 31 := 19;                     --Tile
constant N_BITS_PER_TILE : INTEGER range 0 to 63 := 33;                 --Tile & CoordX & CoordY
constant BULLET : INTEGER range 0 to 255 := 94;
constant EMPTY_COMMAND : STD_LOGIC_VECTOR(N_BITS_PER_CMD-1 downto 0) := (others=>'0');
constant DESTROY_EFFECT : INTEGER range 0 to 15 := 1;
constant CREATE_EFFECT  : INTEGER range 0 to 15 := 5;
constant DAMAGE_EFFECT  : INTEGER range 0 to 15 := 9;

--types
subtype CMD is STD_LOGIC_VECTOR(N_BITS_PER_CMD-1 downto 0);
type CMD_QUEUE is array(MAX_ELEMENTS-1 downto 0) of CMD;
subtype TILE_C is STD_LOGIC_VECTOR(N_BITS_PER_TILE-1 downto 0);
type TILE_QUEUE is array(MAX_TILES-1 downto 0) of TILE_C;

--RAM
signal cmds : CMD_QUEUE := (others=>(others=>'0'));
attribute ram_style: string;
attribute ram_style of cmds : signal is "distributed";

--other signals or variables
signal tiles : TILE_QUEUE := (others=>(others=>'0'));
signal rdC   : INTEGER range 0 to MAX_ELEMENTS := 0;    --cmds reading pointer
signal rdT   : INTEGER range 0 to MAX_TILES    := 0;    --tiles reading pointer
signal tilesEmpty : STD_LOGIC                  := '1';  --tiles empty flag
signal cmdsEmpty  : STD_LOGIC                  := '1';  --cmds empty flag
signal read       : STD_LOGIC                  := '1';
signal command    : STD_LOGIC_VECTOR(N_BITS_PER_CMD-1 downto 0) := EMPTY_COMMAND;

begin
parseCommand: process(clk)
variable currentFrame : INTEGER range 0 to 15;
variable totFrames  : INTEGER range 0 to 15;
variable wrC   : INTEGER range 0 to MAX_ELEMENTS := 0;    --cmds writing pointer
variable full        : STD_LOGIC                 := '0';
variable reset       : STD_LOGIC                 := '1';
begin
    if rising_edge(clk) then
        if read='1' then
            currentFrame := to_integer(unsigned(command(34 downto 31)));
            totFrames    := to_integer(unsigned(command(30 downto 27)));
            if currentFrame<totFrames and full='0' then
                cmds(wrC) <= "1" & std_logic_vector(to_unsigned(currentFrame+1, 4)) & command(30 downto 0);
                if rdC = ((wrC+1) mod MAX_ELEMENTS) then
                    full := '1';
                end if;
                wrC := (wrC+1) mod MAX_ELEMENTS;
                
                cmdsEmpty <= '0';
            end if;
            
            if cmdsEmpty='0' and cmds(rdC)(35)='0' then
                command <= cmds(rdC); --pop
                full := '0';
                if ((rdC+1) mod MAX_ELEMENTS) = wrC then
                    cmdsEmpty <= '1';
                end if;
                rdC <= (rdC+1) mod MAX_ELEMENTS;
             else
                command <= EMPTY_COMMAND;
            end if;
        end if;
        
        if enable='1' and full='0' then        
            --push the command into the queue
            if move='1' then
                if to_integer(unsigned(spriteID))=BULLET then
                    totFrames := 0;
                else
                    totFrames := 9;
                end if;
            else
                totFrames := 4;
            end if;
            cmds(wrC) <= "00000"    --Parsed & CurrentFrame
                         & std_logic_vector(to_unsigned(totFrames, 4))
                         & spriteID
                         & move
                         & command_in
                         & std_logic_vector(to_unsigned(coordX_in, 7)) 
                         & std_logic_vector(to_unsigned(coordY_in, 7));
            
            if rdC = ((wrC+1) mod MAX_ELEMENTS) then
                full := '1';
            end if;
            wrC := (wrC+1) mod MAX_ELEMENTS;
            
            cmdsEmpty <= '0';
        end if;
        
        if enW='1' then
            reset := '0';
        elsif reset='0' then
            for i in 0 to MAX_ELEMENTS-1 loop
                cmds(i)(35) <= '0';
            end loop;
            reset := '1';
        end if;

    end if;
end process;

sendTiles: process(clk)
variable coordX, coordY : INTEGER range 0 to 127;
variable sprite         : INTEGER range 0 to 255;
variable tempTile       : STD_LOGIC_VECTOR(N_BITS_TILE-1 downto 0);
variable move           : STD_LOGIC;
variable command_in     : STD_LOGIC_VECTOR(3 downto 0);
variable last           : INTEGER range 0 to 15;
variable currentFrame   : INTEGER range 0 to 15;
variable totFrames      : INTEGER range 0 to 15;
variable canput         : STD_LOGIC;
variable effect         : INTEGER range 0 to 15;
begin
    if falling_edge(clk) then
        if command/=EMPTY_COMMAND then
            if tilesEmpty='1' then
                currentFrame:= to_integer(unsigned(command(34 downto 31)));
                totFrames   := to_integer(unsigned(command(30 downto 27)));
                sprite      := to_integer(unsigned(command(26 downto 19)));
                move        := command(18);
                command_in  := command(17 downto 14);
                coordX      := to_integer(unsigned(command(13 downto 7)));
                coordY      := to_integer(unsigned(command(6 downto 0)));
                tempTile    := (others=>'0');
                canput      := '0';
                last        := 0;
                effect      := 0;
                
                if currentFrame=0 or currentFrame=totFrames then
                    canput:='1';
                end if;
                if(move='1') then   --movement
                    if currentFrame=totFrames or currentFrame=((totFrames-1)/2) then
                        if totFrames=0 or currentFrame<totFrames then
                            --Reset old tiles
                            canput := '1';
                            tiles(last) <= tempTile
                                          & std_logic_vector(to_unsigned(coordX, 7))
                                          & std_logic_vector(to_unsigned(coordY, 7));
                            last := last+1;
                            tiles(last) <= tempTile
                                          & std_logic_vector(to_unsigned(coordX+1, 7))
                                          & std_logic_vector(to_unsigned(coordY, 7));
                            last := last+1;
                            tiles(last) <= tempTile
                                        & std_logic_vector(to_unsigned(coordX, 7))
                                        & std_logic_vector(to_unsigned(coordY+1, 7));
                            last := last+1;
                            tiles(last) <= tempTile
                                          & std_logic_vector(to_unsigned(coordX+1, 7))
                                          & std_logic_vector(to_unsigned(coordY+1, 7));
                            last := last+1;
                            if totFrames>0 then
                                sprite := sprite+8;
                            end if;
                        end if;
                        case command_in is
                           when "1001" =>   --down-right
                               coordX := coordX+1;
                               coordY := coordY+1;
                           when "1100" =>   --up-right
                               coordX := coordX+1;
                               coordY := coordY-1;
                           when "1000" =>   --right
                               coordX := coordX+1;
                           when "0011" =>   --down-left
                               coordX := coordX-1;
                               coordY := coordY+1;
                               tempTile(9) := '1';
                           when "0110" =>   --up-left
                               coordX := coordX-1;
                               coordY := coordY-1;
                               tempTile(9) := '1';
                           when "0010" =>   --left
                               coordX := coordX-1;
                               tempTile(9) := '1';
                           when "0001" =>   --down
                               coordY := coordY+1;
                           when "0100" =>   --up
                               coordY := coordY-1;
                           when others =>
                        end case;
                    elsif currentFrame=0 then
                        sprite := sprite+4;     --1st movement sprite
                    end if;
                elsif currentFrame=0 then    --not a movement
                    if command_in(2 downto 0)="110" then    --Attack
                        if command_in(3)='1' then   --Flipped attack
                            tempTile(9):='1';
                        end if;
                        sprite := sprite+12;    --attack sprite
                    elsif command_in="0100" then    --Destroy
                        sprite := 0;
                        effect := DESTROY_EFFECT;
                    elsif command_in="0001" then    --Create
                        effect := CREATE_EFFECT;
                    elsif command_in="0011" then    --Damage
                        effect := DAMAGE_EFFECT;
                    end if;
                elsif currentFrame=totFrames and command_in="0100" then --destroy
                    sprite := 0;
                end if;
                
                if canput='1' then
                    --print out the tiles
                    if tempTile(9)='1' then    --HFlip
                        sprite := sprite+1;
                    end if;
                    if effect>0 then
                        tempTile(3 downto 0) := std_logic_vector(to_unsigned(effect, 4));
                        effect := effect+1;
                    end if;
                    tempTile(18 downto 11) := std_logic_vector(to_unsigned(sprite, 8));
                    tiles(last) <= tempTile
                               & std_logic_vector(to_unsigned(coordX, 7))
                               & std_logic_vector(to_unsigned(coordY, 7));
                    last := last+1;
                    
                    if sprite>0 and sprite/=BULLET then
                        if tempTile(9)='1' then    --HFlip
                           sprite := sprite-1;
                        else
                           sprite := sprite+1;
                        end if;
                        tempTile(18 downto 11) := std_logic_vector(to_unsigned(sprite, 8));
                    elsif sprite=BULLET then
                        tempTile(10 downto 9) := "01";
                    end if;
                    if effect>0 then
                        tempTile(3 downto 0) := std_logic_vector(to_unsigned(effect, 4));
                        effect := effect+1;
                    end if;
                    tiles(last) <=   tempTile
                              & std_logic_vector(to_unsigned(coordX+1, 7))
                              & std_logic_vector(to_unsigned(coordY, 7));
                    last := last+1;
                    
                    
                    if sprite>0 and sprite/=BULLET then
                        if tempTile(9)='1' then    --HFlip
                           sprite := sprite+3;
                        else
                           sprite := sprite+1;
                        end if;
                        tempTile(18 downto 11) := std_logic_vector(to_unsigned(sprite, 8));
                    elsif sprite=BULLET then
                        tempTile(10 downto 9) := "10";
                    end if;
                    if effect>0 then
                        tempTile(3 downto 0) := std_logic_vector(to_unsigned(effect, 4));
                        effect := effect+1;
                    end if;
                    tiles(last) <=   tempTile
                               & std_logic_vector(to_unsigned(coordX, 7))
                               & std_logic_vector(to_unsigned(coordY+1, 7));
                    last := last+1;
                    
                    
                    if sprite>0 and sprite/=BULLET then
                        if tempTile(9)='1' then    --HFlip
                           sprite := sprite-1;
                        else
                           sprite := sprite+1;
                        end if;
                        tempTile(18 downto 11) := std_logic_vector(to_unsigned(sprite, 8));
                    elsif sprite=BULLET then
                        tempTile(10 downto 9) := "11";
                    end if;
                    if effect>0 then
                        tempTile(3 downto 0) := std_logic_vector(to_unsigned(effect, 4));
                        effect := effect+1;
                    end if;
                    tiles(last) <=   tempTile
                               & std_logic_vector(to_unsigned(coordX+1, 7))
                               & std_logic_vector(to_unsigned(coordY+1, 7));
                    last := last+1;
                    
                    if last<MAX_TILES then
                        tiles(last)(13 downto 7) <= std_logic_vector(to_unsigned(85, 7));
                    end if;
                    tilesEmpty <= '0';
                end if;
                read <= '1';
            elsif tilesEmpty='0' then
                read <= '0';
            end if;
        end if;
            
        if tilesEmpty='0' and enW='1' then
            --Tiles is not empty. Send the next Tile to the TileMap
            tempTile := tiles(rdT)(32 downto 14);
            coordX := to_integer( unsigned( tiles(rdT)(13 downto 7) ) );
            coordY := to_integer( unsigned( tiles(rdT)(6 downto 0) ) );
            tiles(rdT) <= (others=>'0');
            if coordX<=80 then
                tile         <= tempTile;
                coordX_out   <= coordX;
                coordY_out   <= coordY;
            end if;
            if rdT+1 = MAX_TILES or coordX>80 then
                rdT <= 0;
                tilesEmpty <= '1';
            else
                rdT <= rdT+1;
            end if;
        end if;
     end if;
end process;

end Behavioral;