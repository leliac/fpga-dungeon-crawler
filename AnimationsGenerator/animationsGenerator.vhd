----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 14.05.2017 17:56:53
-- Design Name: 
-- Module Name: AnimationsGenerator - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity AnimationsGenerator is
  Port (   clk: in STD_LOGIC;
           enable: in STD_LOGIC;
           move: in STD_LOGIC;
           spriteID: in STD_LOGIC_VECTOR(6 downto 0);
           coordX_in: in INTEGER range 0 to 80;
           coordY_in: in INTEGER range 0 to 60;
           command_in: in STD_LOGIC_VECTOR(3 downto 0);
           enW: in STD_LOGIC;
           full: inout STD_LOGIC;
           tile: out STD_LOGIC_VECTOR(18 downto 0);
           coordX_out: out INTEGER range 0 to 80;
           coordY_out: out INTEGER range 0 to 60 );
end AnimationsGenerator;

architecture Behavioral of AnimationsGenerator is

--constants
constant MAX_ELEMENTS : INTEGER range 0 to 127   := 100;
constant MAX_TILES : INTEGER range 0 to 7        := 4;
constant N_BITS_PER_CMD : INTEGER range 0 to 63  := 35;                 --Parse & SpriteID & Move & Command & CoordX & CoordY & CurrentFrame & TotFrames
constant N_BITS_TILE : INTEGER range 0 to 31 := 19;                     --Tile
constant N_BITS_EFFECT : INTEGER range 0 to 7 := 4;                     --Effect  
constant N_BITS_PER_TILE : INTEGER range 0 to 63 := N_BITS_TILE + 14;   --Tile & CoordX & CoordY
constant MAX_MOV_SPRITES : INTEGER range 0 to 7  := 3;                  --Max number of different sprites for the movement
constant N_SPRITES_MOV   : INTEGER range 0 to 3  := 2;
constant N_SPRITES_ATT   : INTEGER range 0 to 3  := 2;
constant N_SPRITES_DAM   : INTEGER range 0 to 3  := 2;
constant N_FRAMES_MOV    : INTEGER range 0 to 15 := 8; --to be decided
constant N_FRAMES_ATT    : INTEGER range 0 to 15 := 4; --to be decided
constant N_FRAMES_DAM    : INTEGER range 0 to 15 := 4; --to be decided
constant N_FRAMES_DES    : INTEGER range 0 to 15 := 4; --to be decided
constant N_FRAMES_CRE    : INTEGER range 0 to 15 := 4; --to be decided
constant MOVE_EFFECT     : INTEGER range 0 to 15 := 0; --1; --MODIFICA
constant DESTROY_EFFECT  : INTEGER range 0 to 15 := 0; --5; --MODIFICA
constant ATTACK_EFFECT   : INTEGER range 0 to 15 := 0; --9; --MODIFICA

--types
subtype CMD is STD_LOGIC_VECTOR(N_BITS_PER_CMD-1 downto 0);
type CMD_QUEUE is array(MAX_ELEMENTS-1 downto 0) of CMD;
subtype TILE_C is STD_LOGIC_VECTOR(N_BITS_PER_TILE-1 downto 0);
type TILE_QUEUE is array(MAX_TILES-1 downto 0) of TILE_C;

--RAM
signal cmds : CMD_QUEUE := (others=>(others=>'0'));
attribute ram_style: string;
attribute ram_style of cmds : signal is "distributed";

--other signals or variables
signal tiles : TILE_QUEUE := (others=>(others=>'0'));
signal rdC   : INTEGER range 0 to MAX_ELEMENTS := 0;    --cmds reading pointer
signal wrC   : INTEGER range 0 to MAX_ELEMENTS := 0;    --cmds writing pointer
signal rdT   : INTEGER range 0 to MAX_TILES    := 0;    --tiles reading pointer
signal tilesEmpty : STD_LOGIC                  := '1';  --tiles empty flag
signal cmdsEmpty  : STD_LOGIC                  := '1';  --cmds empty flag

begin
parseCommand: process(clk, enW)
variable currentFrame   : INTEGER range 0 to 15;
variable coordX         : INTEGER range 0 to 80;
variable coordY         : INTEGER range 0 to 60;
variable sprite         : INTEGER range 0 to 127;
variable totFrames      : INTEGER range 0 to 15;
variable tempTile       : STD_LOGIC_VECTOR(N_BITS_TILE-1 downto 0);
variable offset         : INTEGER range 0 to 15;
variable effect         : INTEGER range 0 to 15;
variable command        : STD_LOGIC_VECTOR(N_BITS_PER_CMD-1 downto 0);
begin
    if rising_edge(clk) and enable='1' and full/='1' then
        --set number of total frames
        if move='1' then
            totFrames := N_FRAMES_MOV;
        else
            case command_in is
                when "0001" => totFrames := N_FRAMES_CRE;
                when "0011" => totFrames := N_FRAMES_DAM;
                when "0100" => totFrames := N_FRAMES_DES;
                when "0110" => totFrames := N_FRAMES_ATT;
                when others =>
            end case;
        end if;
        
        --push the command into the queue
        totFrames := 0; --MODIFICA
        cmds(wrC) <= "0"
                     & spriteID 
                     & move
                     & command_in
                     & std_logic_vector(to_unsigned(coordX_in, 7)) 
                     & std_logic_vector(to_unsigned(coordY_in, 7)) 
                     & "0000"   --currentFrame 
                     &  std_logic_vector(to_unsigned(totFrames, 4));
                     
        if rdC = ((wrC+1) mod MAX_ELEMENTS) then
            full <= '1';
        end if;
        wrC <= (wrC+1) mod MAX_ELEMENTS;
        
        cmdsEmpty <= '0';
    end if;
    
    if falling_edge(clk) then
        
        if tilesEmpty='1' and cmdsEmpty='0' and cmds(rdC)(34)='0' then
            --generate the 4 tiles of the next frame (of the next command)
            command := cmds(rdC); --pop
            full <= '0';
            if ((rdC+1) mod MAX_ELEMENTS) = wrC then
                cmdsEmpty <= '1';
            end if;
            rdC <= (rdC+1) mod MAX_ELEMENTS;
            sprite := to_integer(unsigned(command(33 downto 27)));
            coordX := to_integer(unsigned(command(21 downto 15)));
            coordY := to_integer(unsigned(command(14 downto 8)));
            currentFrame := to_integer(unsigned(command(7 downto 4)));
            totFrames := to_integer(unsigned(command(3 downto 0)));
            tempTile := (others=>'0');
            effect := 0;
            
            if command(26) = '1' then --movement
                if currentFrame<totFrames then
                    effect := MOVE_EFFECT;
                    offset := currentFrame*(16/totFrames);
                    case command(25 downto 22) is --direction
                        when "1001" => --down-right
                        tempTile(9 downto 4) := "11"
                                               & std_logic_vector(to_unsigned(offset ,4));
                        when "1100" => --up-right
                        coordY := coordY-1;
                        offset := 15-offset;
                        tempTile(9 downto 4) := "00"
                                               & std_logic_vector(to_unsigned(offset ,4));
                        when "1000" => --right
                        tempTile(9 downto 4) := "01" & std_logic_vector(to_unsigned(offset ,4));
                        when "0011" => --down-left
                        coordX := coordX-1;
                        tempTile(10 downto 4) := "1"    --HFlip
                                                     & "00"
                                                     & std_logic_vector(to_unsigned(offset ,4));
                        when "0110" => --up-left
                        coordX := coordX-1;
                        coordY := coordY-1;
                        offset := 15-offset;
                        tempTile(10 downto 4) := "1"    --HFlip
                                                     & "11"
                                                     & std_logic_vector(to_unsigned(offset ,4));
                        when "0010" => --left
                        offset := 15-offset;
                        coordX := coordX-1;
                        tempTile(10 downto 4) := "101" & std_logic_vector(to_unsigned(offset ,4));
                        when "0001" => --down
                        tempTile(9 downto 4) := "10" & std_logic_vector(to_unsigned(offset ,4));
                        when "0100" => --up
                        offset := 15-offset;
                        coordY := coordY-1;
                        tempTile(9 downto 4) := "10" & std_logic_vector(to_unsigned(offset ,4));
                        
                        when others =>
                    end case; 
                    sprite := sprite+4+4*(currentFrame mod 2);
                else
                    case command(25 downto 22) is --direction
                    when "1001" =>   --down-right
                    coordX := coordX+1;
                    coordY := coordY+1;
                    when "1100" =>   --up-right
                    coordX := coordX+1;
                    coordY := coordY-1;
                    when "1000" =>   --right
                    coordX := coordX+1;
                    when "0011" =>   --down-left
                    coordX := coordX-1;
                    coordY := coordY+1;
                    tempTile(10) := '1';
                    when "0110" =>   --up-left
                    coordX := coordX-1;
                    coordY := coordY-1;
                    tempTile(10) := '1';
                    when "0010" =>   --left
                    coordX := coordX-1;
                    tempTile(10) := '1';
                    when "0001" =>   --down
                    coordY := coordY+1;
                    when "0100" =>   --up
                    coordY := coordY-1;
                    when others =>
                    end case;
                end if;
                if tempTile(10)='1' then    --HFlip
                    sprite := sprite+1;
                end if;
                tempTile(18 downto 12) := std_logic_vector(to_unsigned(sprite, 7));
                if(effect>0) then
                    tempTile(3 downto 0)   := std_logic_vector(to_unsigned(effect, 4));
                end if;
                tiles(0) <=   tempTile
                           & std_logic_vector(to_unsigned(coordX, 7))
                           & std_logic_vector(to_unsigned(coordY, 7));
                if tempTile(10)='1' then    --HFlip
                    sprite := sprite-1;
                else
                    sprite := sprite+1;
                end if;
                tempTile(18 downto 12) := std_logic_vector(to_unsigned(sprite, 7));
                if(effect>0) then
                    effect := effect+1;
                    tempTile(3 downto 0)   := std_logic_vector(to_unsigned(effect, 4));
                end if;
                tiles(1) <=   tempTile
                           & std_logic_vector(to_unsigned(coordX+1, 7))
                           & std_logic_vector(to_unsigned(coordY, 7));
                if tempTile(10)='1' then    --HFlip
                   sprite := sprite+3;
                else
                   sprite := sprite+1;
                end if;
                tempTile(18 downto 12) := std_logic_vector(to_unsigned(sprite, 7));
                if(effect>0) then
                    effect := effect+1;
                    tempTile(3 downto 0)   := std_logic_vector(to_unsigned(effect, 4));
                end if;
                tiles(2) <=   tempTile
                           & std_logic_vector(to_unsigned(coordX, 7))
                           & std_logic_vector(to_unsigned(coordY+1, 7));
                if tempTile(10)='1' then    --HFlip
                   sprite := sprite-1;
                else
                   sprite := sprite+1;
                end if;
                tempTile(18 downto 12) := std_logic_vector(to_unsigned(sprite, 7));
                if(effect>0) then
                    effect := effect+1;
                    tempTile(3 downto 0)   := std_logic_vector(to_unsigned(effect, 4));
                end if;
                tiles(3) <=   tempTile
                           & std_logic_vector(to_unsigned(coordX+1, 7))
                           & std_logic_vector(to_unsigned(coordY+1, 7));
                
            else --not a movement
                if currentFrame < totFrames then
                    case command(24 downto 22) is --command
                        when "001" =>  --create
                        when "011" =>   --damage
                             sprite := sprite+8+4*(currentFrame mod 2);    
                        when "100" =>    --destroy
                             effect := DESTROY_EFFECT;
                        when "110" =>   --attack
                            sprite := sprite+8+4*(currentFrame mod 2);
                            effect := ATTACK_EFFECT;
                            if(command_in(3)='1') then
                                tempTile(10):= '1';
                                sprite := sprite+1; --second tile has to be the first
                            end if;
                        when others =>
                    end case;
                    tempTile(18 downto 12) := std_logic_vector(to_unsigned(sprite, 7));
                    if(effect>0) then
                        tempTile(3 downto 0)   := std_logic_vector(to_unsigned(effect, 4));
                    end if;
                    tiles(0) <=   tempTile
                               & std_logic_vector(to_unsigned(coordX, 7))
                               & std_logic_vector(to_unsigned(coordY, 7));
                    if tempTile(10)='1' then    --HFlip for Attack
                        sprite := sprite-1;
                    else
                        sprite := sprite+1;
                    end if;
                    tempTile(18 downto 12) := std_logic_vector(to_unsigned(sprite, 7));
                    if(effect>0) then
                        effect := effect+1;
                        tempTile(3 downto 0)   := std_logic_vector(to_unsigned(effect, 4));
                    end if;
                    tiles(1) <=   tempTile
                               & std_logic_vector(to_unsigned(coordX+1, 7))
                               & std_logic_vector(to_unsigned(coordY, 7));
                    if tempTile(10)='1' then    --HFlip for Attack
                       sprite := sprite+3;
                    else
                       sprite := sprite+1;
                    end if;
                    tempTile(18 downto 12) := std_logic_vector(to_unsigned(sprite, 7));
                    if(effect>0) then
                        effect := effect+1;
                        tempTile(3 downto 0)   := std_logic_vector(to_unsigned(effect, 4));
                    end if;
                    tiles(2) <=   tempTile
                               & std_logic_vector(to_unsigned(coordX, 7))
                               & std_logic_vector(to_unsigned(coordY+1, 7));
                    if tempTile(10)='1' then    --HFlip for Attack
                       sprite := sprite-1;
                    else
                       sprite := sprite+1;
                    end if;
                    tempTile(18 downto 12) := std_logic_vector(to_unsigned(sprite, 7));
                    if(effect>0) then
                        effect := effect+1;
                        tempTile(3 downto 0)   := std_logic_vector(to_unsigned(effect, 4));
                    end if;
                    tiles(3) <=   tempTile
                               & std_logic_vector(to_unsigned(coordX+1, 7))
                               & std_logic_vector(to_unsigned(coordY+1, 7));
               else --lastFrame
                    if command(25 downto 22)="0100" then --destroy
                        tiles(0) <= (others=>'0');
                        tiles(1) <= (others=>'0');
                        tiles(2) <= (others=>'0');
                        tiles(3) <= (others=>'0');
                    else
                        tempTile(18 downto 12) := std_logic_vector(to_unsigned(sprite, 7));
                        tiles(0) <=   tempTile
                                    & std_logic_vector(to_unsigned(coordX, 7))
                                    & std_logic_vector(to_unsigned(coordY, 7));
                        sprite := sprite+1;
                        tempTile(18 downto 12) := std_logic_vector(to_unsigned(sprite, 7));
                        tiles(1) <=   tempTile
                                    & std_logic_vector(to_unsigned(coordX+1, 7))
                                    & std_logic_vector(to_unsigned(coordY, 7));
                        sprite := sprite+1;
                        tempTile(18 downto 12) := std_logic_vector(to_unsigned(sprite, 7));
                        tiles(2) <=   tempTile
                                    & std_logic_vector(to_unsigned(coordX, 7))
                                    & std_logic_vector(to_unsigned(coordY+1, 7));
                        sprite := sprite+1;
                        tempTile(18 downto 12) := std_logic_vector(to_unsigned(sprite, 7));
                        tiles(3) <=   tempTile
                                    & std_logic_vector(to_unsigned(coordX+1, 7))
                                    & std_logic_vector(to_unsigned(coordY+1, 7));
                    end if;
               end if;
            end if;
            
            if currentFrame < totFrames then
                currentFrame := currentFrame+1;
                command(34) := '1';
                command(7 downto 4) := std_logic_vector(to_unsigned(currentFrame, 4)); --update Current frame
                cmds(wrC) <= command;   --push again in the queue
                wrC <= wrC+1;
                if wrC = MAX_ELEMENTS then
                    wrC <= 0;
                end if;
                if wrC = rdC then
                    full <= '1';
                end if;
                cmdsEmpty <= '0';
            end if;
            tilesEmpty <= '0';
            
        end if;
        
        if tilesEmpty='0' and enW='1' then
            --Tiles is not empty. Send the next Tile to the TileMap
            coordX_out   <= to_integer( unsigned( tiles(rdT)(6 downto 0) ) );
            coordY_out   <= to_integer( unsigned( tiles(rdT)(13 downto 7) ) );
            tile         <= tiles(rdT)(N_BITS_PER_TILE-1 downto 14);
            if rdT+1 = MAX_TILES then
                rdT <= 0;
                tilesEmpty <= '1';
            else
                rdT <= rdT+1;
            end if;
        else
            --reset output signals
            coordX_out <= 0;
            coordY_out <= 0;
            tile       <= (others=>'0');
        end if;
    end if;
    
    if falling_edge(enW) then   --can't write. Reset to send current Tiles
        rdT <= 0;
        for i in MAX_ELEMENTS-1 downto 0 loop --set to 0 parsed bits
            cmds(i)(34) <= '0';
        end loop;
    end if;
end process;

end Behavioral;
