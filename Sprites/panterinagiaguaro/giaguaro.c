/*
 * giaguaro.c
 * 
 * Copyright 2017 Stefano <pmonkey@garden>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#define ROWS 80
#define COLS 60

#define WALL 'X'
#define RIVER '='
#define LAVA '_'
#define HILL '('
#define TOWER '@'

#define SWALL 1
#define SRIVER 2
#define SLAVA 3
#define SHILL 4
#define STOWER 5

int rows;
int cols;
char** map_raw;
uint32_t** map;

void readMap(char* name) { //Costumize this function from read ascii file
	int x, y;
	char* filename = strdup(name);
	strcat(filename, ".txt");
	FILE* f_in = fopen(filename, "r");
	map_raw = malloc(ROWS*sizeof(char*));
	for (y=0; y<ROWS; y++) {
		map_raw[y] = malloc(COLS*sizeof(char));
		for (x=0; x<COLS; x++) {
			fscanf(f_in, "%c", &map_raw[y][x]);
			if (map_raw[y][x] == '\n')
				fscanf(f_in, "%c", &map_raw[y][x]);
		}
	}
	fclose(f_in);
}

void writeMap(char* name) { //Costumize for write binary map
	int x, y;
	char* filename = strdup(name);
	strcat(filename, ".bin");
	FILE* f_out = fopen(filename, "wb");
	for (y=0; y<ROWS; y++) {
		for (x = 0; x<COLS; x++) {
			fwrite(&map[y][x], sizeof(uint32_t), 1, f_out);
		}
	}
	fclose(f_out);
}


uint32_t evaluateCell(int x, int y) {
	switch(map_raw[y][x]) {
		case WALL:
			return (uint32_t) SWALL;
			break;
		case RIVER:
			return (uint32_t) SRIVER;
			break;
		case LAVA:
			return (uint32_t) SLAVA;
			break;
		case HILL:
			return (uint32_t) SHILL;
			break;
		case TOWER:
			return (uint32_t) STOWER;
			break;
		default:
			return (uint32_t) 0;
	}
}

int main(int argc, char **argv)
{
	int x, y;
	readMap(argv[1]);
	printf("asd");
	map = malloc(ROWS*sizeof(uint32_t*));
	for (y=0; y<ROWS; y++) {
		map[y] = malloc(COLS*sizeof(uint32_t));
		for (x=0; x<COLS; x++) {
			map[y][x] = evaluateCell(x,y);
		}
	}
	writeMap(argv[1]);
	return 0;
}

