/*
 * giaguaro.c
 * 
 * Copyright 2017 Stefano <pmonkey@garden>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#define ROWS 30
#define COLS 40

#define HF 512
#define VF 1024

#define GRASS '.'
#define WALL '#'
#define RIVER '='
#define LAVA '_'
#define PORTAL '>'

//C -> right-top

#define SGRASS 0
#define SBORDER 5
#define SWALL 5
#define CWALL 6
#define SRIVER 8
#define CRIVER 9
#define SLAVA 11
#define CLAVA 12
#define CPORTAL 4

#define TILEMULT 2048

int rows;
int cols;
char** map_raw;
uint32_t** map;

void readMap(char* name) { //Costumize this function from read ascii file
	int x, y;
	char* filename = strdup(name);
	strcat(filename, ".txt");
	FILE* f_in = fopen(filename, "r");
	map_raw = malloc(ROWS*sizeof(char*));
	for (y=0; y<ROWS; y++) {
		map_raw[y] = malloc(COLS*sizeof(char));
		for (x=0; x<COLS; x++) {
			fscanf(f_in, "%c", &map_raw[y][x]);
			if (map_raw[y][x] == '\n')
				fscanf(f_in, "%c", &map_raw[y][x]);
		}
	}
	fclose(f_in);
}

void writeMap(char* name) { //Costumize for write binary map
	int x, y;
	char* filename = strdup(name);
	strcat(filename, ".bin");
	FILE* f_out = fopen(filename, "wb");
	for (y=0; y<ROWS*2; y++) {
		for (x = 0; x<COLS*2; x++) {
			fwrite(&map[y][x], sizeof(uint32_t), 1, f_out);
		}
	}
	fclose(f_out);
}

int evaluateLikeWall(int x, int y, char cell, int val, int valcorner, int tile) {
	int value = val * TILEMULT;
	int tiletl = valcorner * TILEMULT + HF;
	int tilebl = valcorner * TILEMULT;
	int tilebr = valcorner * TILEMULT + VF;
	int tiletr = valcorner * TILEMULT + VF + HF;
	//0b right top left bottom
	if (x+1 < COLS && map_raw[y][x+1] == cell) {
		tiletr = value;
		tilebr = value;
	}
	if (y+1 < ROWS && map_raw[y+1][x] == cell) {
		tilebl = value;
		tilebr = value;
	}
	if (x-1 >= 0 && map_raw[y][x-1] == cell) {
		tiletl = value;
		tilebl = value;
	}
	if (y-1 >= 0 && map_raw[y-1][x] == cell) {
		tiletr = value;
		tiletl = value;
	}
	switch (tile) {
		case 0:
			return tiletl;
		break;
		case 1:
			return tilebl;
		break;
		case 2:
			return tiletr;
		break;
		case 3:
			return tilebr;
		break;
		default:
			return value;
	}
}

uint32_t evaluatePortal(int tile) {
	switch (tile) {
		case 0:
			return CPORTAL*TILEMULT + HF;
		break;
		case 1:
			return CPORTAL*TILEMULT;
		break;
		case 2:
			return CPORTAL*TILEMULT + HF + VF;
		break;
		case 3:
			return CPORTAL*TILEMULT + VF;
		break;
		default:
		return CPORTAL*TILEMULT;
	}
}

uint32_t evaluateCell(int x, int y, int tile) {
	switch(map_raw[y][x]) {
		case GRASS:
			return (uint32_t) SGRASS * TILEMULT;
			break;
		case WALL:
			return (uint32_t) evaluateLikeWall(x, y, WALL, SWALL, CWALL, tile);
			break;
		case RIVER:
			return (uint32_t) evaluateLikeWall(x, y, RIVER, SRIVER, CWALL, tile);
			break;
		case LAVA:
			return (uint32_t) evaluateLikeWall(x, y, LAVA, SLAVA, CWALL, tile);
			break;
		case PORTAL:
			return (uint32_t) evaluatePortal(tile);
			break;
		default:
			return (uint32_t) 0;
	}
}

int main(int argc, char **argv)
{
	int x, y;
	readMap(argv[1]);
	map = malloc(ROWS*2*sizeof(uint32_t*));
	for (y=0; y<ROWS; y++) {
		map[y*2] = malloc(COLS*2*sizeof(uint32_t));
		map[y*2+1] = malloc(COLS*2*sizeof(uint32_t));
		for (x=0; x<COLS; x++) {
			map[y*2][x*2] = evaluateCell(x,y,0);
			map[y*2+1][x*2] = evaluateCell(x,y,1);
			map[y*2][x*2+1] = evaluateCell(x,y,2);
			map[y*2+1][x*2+1] = evaluateCell(x,y,3); 
		}
	}
	writeMap(argv[1]);
	return 0;
}

