#!/bin/bash
for i in `seq 0 127`;
do
	./generate > ./maps/map$i.txt
done
cd ./maps
for j in `seq 0 127`;
do
	./convert map$j
	cat map$j.bin >> ../randmaps.bin
done
