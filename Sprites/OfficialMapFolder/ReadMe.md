./randmaps.sh - 128 random maps inside randmaps.bin
./singlemap.sh - 1 random map 128 time inside singlemap.bin

#define ROWS 30
#define COLS 40

#define HF 2
#define VF 1

#define GRASS '.'
#define WALL '#'
#define RIVER '='
#define LAVA '_'
#define PORTAL '>'

//C -> right-top

#define SGRASS 0
#define SBORDER 1
#define SWALL 1
#define CWALL 2
#define SRIVER 5
#define CRIVER 6
#define SLAVA 9
#define CLAVA 10
#define CPORTAL 0 // <-tmp
