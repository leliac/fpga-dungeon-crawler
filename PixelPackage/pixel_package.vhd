----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04.05.2017 13:11:39
-- Design Name: 
-- Module Name: pixels_p - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
package pixels_p is
    --type sprite_row is array(0 to 15) of std_logic_vector;
    --type sprite_t is array ( 0 to 15) of std_logic_vector;
    type sprite_t is array (0 to 255) of std_logic_vector(5 downto 0);
    type sprites_array is array (0 to 126) of sprite_t;
    
    function initSprite(x : natural) return sprites_array;

    function get_pixel(x, y: integer;
                       sprite: sprite_t) return std_logic_vector;
    function b6to16(pixel_in : STD_LOGIC_VECTOR(5 downto 0)) return STD_LOGIC_VECTOR;
    
end pixels_p;
package body pixels_p is
    
    function get_pixel( x, y: integer;
                        sprite: sprite_t) return std_logic_vector is
    variable pixel:std_logic_vector(5 downto 0);
    variable pixel16:std_logic_vector(15 downto 0);
    begin
        pixel:=sprite(x*16+y);
        pixel16:=b6to16(pixel);        
        return pixel16;
    end function get_pixel;
    
    function b6to16(pixel_in : STD_LOGIC_VECTOR(5 downto 0)) return STD_LOGIC_VECTOR is
    variable i: integer:=0;
    variable a, b: std_logic;
    variable pixel_bus:STD_LOGIC_VECTOR(15 downto 0);
    begin
    --for is used since there aren't many iterations and 
       while(i<3) loop
            a := pixel_in(2*i);
            b := pixel_in(2*i+1);
            pixel_bus(5*i):=a AND b;
            pixel_bus(5*i+4) := a AND b;
            pixel_bus(5*i+1) := a;
            pixel_bus(5*i+3) := a;
            pixel_bus(5*i+2) := a OR b;
            i:=i+1;
        end loop;
        i:=0;
        pixel_bus(15) := b;
        return pixel_bus;
    end function b6to16;
    
    function initSprite(x : natural) return sprites_array is
    variable data : sprites_array;
    variable color : std_logic_vector(5 downto 0) := (others=>'0');
    begin
      for i in 0 to 126 loop
        data(i) := (others=>color);
        color := std_logic_vector(unsigned(color) + 1);
      end loop;
      return data;
    end function;
    
end package body;