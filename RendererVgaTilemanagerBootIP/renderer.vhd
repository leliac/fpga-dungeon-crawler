----------------------------------------------------------------------------------
-- University: Politecnico di Torino
-- Authors: Gabriele Cazzato, Stefano Negri Merlo, Gabriele Monaco, Alexandru Valentin Onica, Gabriele Ponzio, Roberto Stagi
-- 
-- Create Date: 20/04/2017 11:02:05 AM
-- Design Name: TOP
-- Module Name: Renderer
-- Project Name: Computer Architecture Project 2017
-- Target Devices: Digilent ZYBO
-- Tool Versions: Vivado 2017.1
-- Description:
--
-- Dependencies:
-- * pixel_pack.vhd
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
--use IEEE.MATH_REAL.ALL;
use work.pixels_p.ALL;

entity renderer is
generic (
    MAP_NUM_TILES_X : NATURAL := 80;
    MAP_NUM_TILES_Y : NATURAL := 60;
    --VIEW_NUM_TILES_X : NATURAL := 80/2;
    --VIEW_NUM_TILES_Y : NATURAL := 60/2;
    TILE_NUM_BITS : NATURAL := 19;
    SPRITES_NUM : NATURAL := 128;
    --SPRITE_NUM_PIXELS : NATURAL := 16*16;
    SPRITE_NUM_PIXELS_X : NATURAL := 16;
    SPRITE_NUM_PIXELS_Y : NATURAL := 16;
    PIXEL_NUM_BITS : NATURAL := 6;
    PIXEL_BUS_NUM_BITS : NATURAL := 16
);
port (
    clk: in STD_LOGIC;
    pixel_clk: in STD_LOGIC;
    bram_clk: in STD_LOGIC;
    --output_enable : in STD_LOGIC;
    fetching_sprites : in STD_LOGIC;
    addr_X : out NATURAL range 0 to MAP_NUM_TILES_X+1; --coords that are sent to the tilemap data queue to retrieve tile_in
    addr_Y : out NATURAL range 0 to MAP_NUM_TILES_Y+3;
    this_X : in NATURAL range 0 to MAP_NUM_TILES_X+1; --player position
    this_Y : in NATURAL range 0 to MAP_NUM_TILES_Y+3;
    screen_X : in NATURAL range 0 to 900; --calculated inside the vga module
    screen_Y : in NATURAL range 0 to 530;
    tile_in : in STD_LOGIC_VECTOR (TILE_NUM_BITS-1 downto 0); --tile_in: value stored in the tilemap at addr_X, addr_Y
    pixel_in : in STD_LOGIC_VECTOR (PIXEL_NUM_BITS-1 downto 0); --6 bit color
    pixel_bus : out STD_LOGIC_VECTOR (PIXEL_BUS_NUM_BITS-1 downto 0);
    mode : in STD_LOGIC; --whether to print the finder
    canput : in STD_LOGIC; --to set the color of the finder
    level : in INTEGER range 0 to 31

);
end renderer;

architecture Behavioral of renderer is
constant SPRITE_NUM_PIXELS : NATURAL := SPRITE_NUM_PIXELS_X*SPRITE_NUM_PIXELS_Y;
--constant TILES_IN_LINE : INTEGER range 0 to 41 := 40;
constant VIEW_NUM_TILES_X : NATURAL := MAP_NUM_TILES_X/2;
--constant LINES_IN_HUD_AND_FRAME : INTEGER range 0 to 31 := 30;
constant VIEW_NUM_TILES_Y : NATURAL := MAP_NUM_TILES_Y/2;
constant HUD_BINARY : INTEGER := (98+27);
constant HUD_NUM_TILES_Y : NATURAL := 4;
--constant LINES_IN_FRAME : INTEGER range 0 to 28 := 27;
constant FRAME_NUM_TILES_Y : NATURAL := VIEW_NUM_TILES_Y-HUD_NUM_TILES_Y;
constant TILEMAP_HUD_START_MINUS_27 : NATURAL range 0 to 34 := 60-27;
--constant H_PIXELS_MAX : INTEGER range 0 to 641 := 640;
constant VIEW_NUM_PIXELS_X : NATURAL := VIEW_NUM_TILES_X*SPRITE_NUM_PIXELS_X;
--constant V_PIXELS_MAX : INTEGER range 0 to 481 := 480;
constant VIEW_NUM_PIXELS_Y : NATURAL := VIEW_NUM_TILES_Y*SPRITE_NUM_PIXELS_Y;
--constant V_PIXELS_IN_TILE : INTEGER range 0 to 16 := 16;
constant GROUND_SPRITE_ID : NATURAL := 0; --this will probably change
--constant BRAM_ADDR_NUM_BITS : NATURAL := natural(ceil(log2(real(SPRITES_NUM*SPRITE_NUM_PIXELS))));
constant BRAM_ADDR_NUM_BITS : NATURAL := 16;

--simple dual port and dual clock bram (port A for writing port B for reading)
component sprites_bram is
port (
    clka : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR (0 downto 0);
    addra : in STD_LOGIC_VECTOR (15 downto 0);
    --addra : in STD_LOGIC_VECTOR (BRAM_ADDR_NUM_BITS-1 downto 0);
    dina : in STD_LOGIC_VECTOR (5 downto 0);
    --dina : in STD_LOGIC_VECTOR (PIXEL_NUM_BITS-1 downto 0);
    clkb : in STD_LOGIC;
    addrb : in STD_LOGIC_VECTOR (15 downto 0);
    --addrb : in STD_LOGIC_VECTOR (BRAM_ADDR_NUM_BITS-1 downto 0);
    doutb : out STD_LOGIC_VECTOR (5 downto 0)
    --doutb : out STD_LOGIC_VECTOR (PIXEL_NUM_BITS-1 downto 0)
);
end component;

--sprites bram
signal addra : STD_LOGIC_VECTOR (15 downto 0) := (others => '0');
--signal addra : STD_LOGIC_VECTOR (BRAM_ADDR_NUM_BITS-1 downto 0) := (others => '0');
signal addrb : STD_LOGIC_VECTOR (15 downto 0) := (others => '0');
--signal addrb : STD_LOGIC_VECTOR (BRAM_ADDR_NUM_BITS-1 downto 0) := (others => '0');
signal doutb : STD_LOGIC_VECTOR (5 downto 0);
--signal doutb : STD_LOGIC_VECTOR (PIXEL_NUM_BITS-1 downto 0);

--tile line
type tileram is array (0 to VIEW_NUM_TILES_X-1) of STD_LOGIC_VECTOR (TILE_NUM_BITS-1 downto 0);
signal tiles : tileram := (others=>(others=>'0'));
attribute ram_style : string;
attribute ram_style of tiles : signal is "distributed";
--attribute ram_style of sprites_data : signal is "block";

signal isFinder : STD_LOGIC_VECTOR (1 downto 0) := (others=>'0'); --tell if i need to write the finder

--bnd part
signal boundary_X, boundary_Y : NATURAL range 0 to 63 := 0;
constant LeftX : NATURAL := 20;
constant RightX: NATURAL := 60;
constant TopY: NATURAL := 15;
constant BotY: NATURAL := 45;
constant centralRightX: NATURAL := (LeftX+RightX)/2;
constant centralBotY: NATURAL := (TopY+BotY)/2;
--signal sprite_x, sprite_x_rev, sprite_y, sprite_y_rev : INTEGER range 0 to 15 := 0;
--signal current_tile : INTEGER range 0 to 40 := 0;
type groundram is array (0 to 255) of STD_LOGIC_VECTOR (5 downto 0);
signal ground : groundram:= (others=>(others=>'0'));
signal effects_data: effects_arr := initEffect(0);
signal sprites_bin: sprites_arr_bin := initBinSprite(0);
attribute ram_style of effects_data: signal is "block";

begin
    sprites : sprites_bram
    port map(
        clka => clk,
        wea(0) => fetching_sprites,
        addra => addra,
        dina => pixel_in,
        clkb => bram_clk,
        --clkb => clk,
        addrb => addrb,
        doutb => doutb
    );

    sprites_write : process(clk)
    --variable cnt : NATURAL range 0 to SPRITES_NUM*SPRITE_NUM_PIXELS := 1;
    variable cnt : NATURAL range 0 to SPRITES_NUM*SPRITE_NUM_PIXELS-1 := 0;
           
    begin
        --addra <= std_logic_vector(to_unsigned(cnt,addra'length));
        --if rising_edge(clk) then
        if rising_edge(clk) then
            if fetching_sprites = '1' and cnt < SPRITES_NUM*SPRITE_NUM_PIXELS-1 then
                cnt := cnt+1;
                addra <= std_logic_vector(to_unsigned(cnt,addra'length));
                --cnt := cnt+1;
                
            end if;
        end if;
    end process;
    
    --processes required to prepare the output buffer for output
    read_tile_from_map : process(clk)
    variable line_complete : STD_LOGIC := '0';
    variable tile_wrote : STD_LOGIC := '1';
    variable tile_column_write_counter, tile_row_write_counter: NATURAL range 0 to 50 := 0;
    
    begin
    if rising_edge(clk) then
        --if tile_wrote = '0' and output_enable = '0' then
        if tile_wrote = '0' and (screen_X >= 640 or screen_Y >= 480) then
            tiles(tile_column_write_counter) <= tile_in; --fetch from tilemap
            tile_wrote := '1';
            tile_column_write_counter := tile_column_write_counter+1; --inc on last time it is used
            if tile_column_write_counter = VIEW_NUM_TILES_X then
                line_complete := '1';
            end if;
        end if;
        --if output_enable = '1' then
        if screen_X < 640 and screen_Y < 480 then
            if (screen_Y mod 16) = 15 then --reinit only when line has been written
                line_complete := '0';
                tile_column_write_counter := 0;
                tile_row_write_counter := (screen_Y+1)/16;
                if tile_row_write_counter = 30 then
                    tile_row_write_counter := 0;
                    if this_x < LeftX then --reinit the boundaries
                        boundary_X <= 0;
                    elsif this_x > RightX then
                        boundary_X <= centralRightX;
                    else
                        boundary_X <= this_x-LeftX;
                    end if;
                    if this_y < TopY then
                       boundary_Y <= 0;
                    elsif this_y > BotY then
                        boundary_Y <= centralBotY;
                    else
                        boundary_Y <= this_y-TopY;
                    end if;
                end if;
            end if;
        elsif screen_X > 640 and (screen_Y < 480 or screen_Y > 523) then --only on wanted blanking time
            if line_complete = '0' and tile_wrote ='1' then
                --if tile_row_write_counter < LINES_IN_FRAME then
                if tile_row_write_counter < FRAME_NUM_TILES_Y then
                    addr_X <= boundary_X+tile_column_write_counter;
                    addr_Y <= boundary_Y+tile_row_write_counter;
                    --finder part
                    if this_Y = boundary_Y+tile_row_write_counter then
                        isFinder <= "10"; --first line of finder
                    elsif this_Y = boundary_Y+tile_row_write_counter-1 then
                        isFinder <= "01"; --second line of finder
                    else
                        isFinder <= "00"; --not inside the finder
                    end if;
          elsif(tile_row_write_counter<VIEW_NUM_TILES_Y) then
          --            if (tile_row_write_counter mod 2)=0 then --26 and 28
          --              addr_X<=tile_column_write_counter;
          --              addr_Y<=LINES_IN_FRAME+LINES_IN_HUD+tile_row_write_counter;
          --            else
          --              addr_X<=tile_column_write_counter+TILES_IN_LINE;
          --              addr_Y<=LINES_IN_FRAME+2+tile_row_write_counter;
          --            end if;
                      addr_X<=tile_column_write_counter+VIEW_NUM_TILES_X*(tile_row_write_counter mod 2);
                      addr_Y<=2*VIEW_NUM_TILES_Y+((tile_row_write_counter-26) / 2);
                    end if;
                    tile_wrote:='0';
                  end if;
                elsif screen_Y>480 then --start with the next (0 is done in the end)
                  null;
                end if;
              end if;
    end process;

    out_creator : process(clk)
    variable pixel : STD_LOGIC_VECTOR (PIXEL_BUS_NUM_BITS-1 downto 0) := (others=>'0');
    variable pixel_id : NATURAL range 0 to (SPRITES_NUM-1)*SPRITE_NUM_PIXELS;
    variable sprite_x, tx : NATURAL range 0 to SPRITE_NUM_PIXELS_X-1 := 0;
    variable sprite_y, ty : NATURAL range 0 to SPRITE_NUM_PIXELS_Y-1 := 0;
    variable current_tile : NATURAL range 0 to VIEW_NUM_TILES_X-1 := 0;
    --variable temp_tile : STD_LOGIC_VECTOR (TILE_NUM_BITS-1 downto 0);
    variable count: natural range 0 to 256:=0;

    begin
        --pixel_bus <= pixel;
        
        --output_enable means that output is required. hsync and vsync are handled through internal counters, but not the blanking period
    if screen_X < 640 and screen_Y < 480 and falling_edge(clk) then

        if pixel_clk = '1' then
            sprite_x := screen_X mod SPRITE_NUM_PIXELS_X; --correct shift
            sprite_y := screen_Y mod SPRITE_NUM_PIXELS_Y;

            if screen_X = 0 then
                current_tile := 0;
            --end if;            
            elsif screen_X mod SPRITE_NUM_PIXELS_X = 0 then --only to reinitialize
                current_tile := current_tile+1;
            end if;
            tx:=sprite_x;
            ty:=sprite_y;
            --identifying first pixel of sprite in current_tile
            pixel_id := to_integer(unsigned(tiles(current_tile)(TILE_NUM_BITS-1 downto 11)))*SPRITE_NUM_PIXELS;
            if tiles(current_tile)(3 downto 0)/="0000" then   --check for the effect
                pixel := b6to16(effects_data((to_integer(unsigned(tiles(current_tile)(3 downto 0)))-1)*256 + tx+16*ty));
            else
                pixel := (others=>'0');
            end if;
            
            if pixel="0000000000000000" then  --effect transparency
              if tiles(current_tile)(9)='1' then --hflip
                tx:=15-sprite_x;
              end if;
              if tiles(current_tile)(10)='1' then --vflip
                ty:=15-sprite_y;
              end if;
            --assigning pixel address
                if pixel_id<HUD_BINARY*256 then
                    addrb <= std_logic_vector(to_unsigned(pixel_id + tx+16*ty,addrb'length));
                else
                    pixel:=(others=>sprites_bin(pixel_id-HUD_BINARY*256 + tx+16*ty));
                end if;
            end if;
         
        end if;

        if pixel_clk = '0' then
--            if screen_X = 0 then
--                current_tile := 0;
--            end if;
            if tiles(current_tile)(3 downto 0)="0000" and pixel_id>= HUD_BINARY*256 then
                pixel :=  b6to16(doutb);
            end if;
            
            if screen_Y<(FRAME_NUM_TILES_Y)*16 then --inside active screen
              --check if the pixel should be replaced with a background sprite
              if pixel="0000000000000000" or pixel_id=ground_sprite_id*256 then
                 pixel :=  b6to16(ground(sprite_x+16*sprite_y));
              end if; 
              --finder part 
              if mode='0' then --needs to print the finder
                if isFinder="10" then --print the first line
                  if this_X=current_tile+boundary_X then --in the first column
                    if (sprite_x<9 and sprite_y<3) or (sprite_x<3 and sprite_y<9) then
                      if canput='1' then
                        pixel:="1111110000000000"; --set green
                      else
                        pixel:="0000000000011111"; --set red
                      end if; 
                    end if;
                  elsif this_X=current_tile+boundary_X-1 then --in the second column
                    if (sprite_x>6 and sprite_y<3) or (sprite_x>12 and sprite_y<9) then
                      if canput='1' then
                        pixel:="1111110000000000"; --set green
                      else
                        pixel:="0000000000011111"; --set red
                      end if; 
                    end if;
                  end if;
                elsif isFinder="01" then --second line
                  if this_X=current_tile+boundary_X then --in the first column
                    if (sprite_x<9 and sprite_y>12) or (sprite_x<3 and sprite_y>6) then
                      if canput='1' then
                        pixel:="1111110000000000"; --set green
                      else
                        pixel:="0000000000011111"; --set red
                      end if; 
                    end if;
                  elsif this_X=current_tile+boundary_X-1 then --in the second column
                    if (sprite_x>6 and sprite_y>12) or (sprite_x>12 and sprite_y>6) then
                      if canput='1' then
                        pixel:="1111110000000000"; --set green
                      else
                        pixel:="0000000000011111"; --set red
                      end if; 
                    end if;
                  end if;          
                end if;
              end if;
            else --inside HUD
              if pixel_id=ground_sprite_id*256 then
                pixel:=(others=>'0');
              end if;
            end if;
            if(sprite_x = 15) then --only to reinitialize
              current_tile:=current_tile+1;
            end if;
            pixel_bus<=pixel;
         end if;

    end if;
    if fetching_sprites='1' then
        count:=0;
    end if;
    if(screen_X>=640 or screen_Y >= 480) and count<256 and fetching_sprites='0' then
        if rising_edge(clk) then
            addrb<=std_logic_vector(to_unsigned((ground_sprite_id+(level mod 5))*256+count,addrb'length));
        end if;
        if falling_edge(clk) then
            ground(count)<=doutb;
            count:=count+1;
        end if;
    end if;

end process;
end Behavioral;