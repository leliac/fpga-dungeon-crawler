***************************************************************************************
*                      PROJECT ARCHIVE SUMMARY REPORT
*
*                      (archive_project_summary.txt)
*
*  PLEASE READ THIS REPORT TO GET THE DETAILED INFORMATION ABOUT THE PROJECT DATA THAT
*  WAS ARCHIVED FOR THE CURRENT PROJECT
*
* The report is divided into following five sections:-
*
* Section (1) - PROJECT INFORMATION
*  This section provides the details of the current project that was archived
*
* Section (2) - INCLUDED/EXCLUDED RUNS
*  This section summarizes the list of design runs for which the results were included
*  or excluded from the archive
*
* Section (3) - ARCHIVED SOURCES
*  This section summarizes the list of files that were added to the archive
*
* Section (3.1) - INCLUDE FILES
*  This section summarizes the list of 'include' files that were added to the archive
*
* Section (3.1.1) - INCLUDE_DIRS SETTINGS
*  This section summarizes the 'verilog include directory' path settings, if any
*
* Section (3.2) - REMOTE SOURCES
*  This section summarizes the list of referenced 'remote' files that were 'imported'
*  into the archived project
*
* Section (3.3) - SOURCES SUMMARY
*  This section summarizes the list of all the files present in the archive
*
* Section (3.4) - REMOTE IP DEFINITIONS
*  This section summarizes the list of all the remote IP's present in the archive
*
* Section (4) - JOURNAL/LOG FILES
*  This section summarizes the list of journal/log files that were added to the archive
*
* Section (5) - CONFIGURATION SETTINGS/FILES
*  This section summarizes the configuration settings/files that were added to the archive
*
***************************************************************************************

Section (1) - PROJECT INFORMATION
---------------------------------
Name      = booting
Directory = /home/andreas/Desktop/computer_architecture/project/progettone/initializationProcess/booting_ip

WARNING: Please verify the compiled library directory path for the following property in the
         current project. The path may point to an invalid location after opening this project.
         This could happen if the project was unarchived in a location where this path is not
         accessible. To resolve this issue, please set this property with the desired path
         before launching simulation:-

Property = compxlib.xsim_compiled_library_dir
Path     = 

Section (2) - INCLUDED RUNS
---------------------------
The run results were included for the following runs in the archived project:-

<synth_1>
<clk_gen_synth_1>
<impl_1>

Section (3) - ARCHIVED SOURCES
------------------------------
The following sub-sections describes the list of sources that were archived for the current project:-

Section (3.1) - INCLUDE FILES
-----------------------------
List of referenced 'RTL Include' files that were 'imported' into the archived project:-

None

Section (3.1.1) - INCLUDE_DIRS SETTINGS
---------------------------------------
List of the "INCLUDE_DIRS" fileset property settings that may or may not be applicable in the archived
project, since most the 'RTL Include' files referenced in the original project were 'imported' into the
archived project.

<sources_1> fileset RTL include directory paths (INCLUDE_DIRS):-
None

<sim_1> fileset RTL include directory paths (INCLUDE_DIRS):-
None

Section (3.2) - REMOTE SOURCES
------------------------------
List of referenced 'remote' design files that were 'imported' into the archived project:-

<clk_gen>
None

<constrs_1>
None

<sim_1>
None

<sources_1>
/home/andreas/Desktop/computer_architecture/project/progettone/initializationProcess/booting_ip/booting.srcs/component.xml

Section (3.3) - SOURCES SUMMARY
-------------------------------
List of all the source files present in the archived project:-

<sources_1>
./booting.srcs/sources_1/imports/initializationProcess/booting_3.vhd
./booting.srcs/sources_1/new/pixel_pack.vhd
./booting.srcs/sources_1/new/renderer.vhd
./booting.srcs/sources_1/new/tilemap.vhd
./booting.srcs/sources_1/new/vga_connector.vhd
./booting.srcs/sources_1/new/top.vhd
./booting.srcs/sources_1/imports/booting.srcs/component.xml

<constrs_1>
None

<sim_1>
None

<clk_gen>
./booting.srcs/sources_1/ip/clk_gen/clk_gen.xci
./booting.srcs/sources_1/ip/clk_gen/clk_gen_board.xdc
./booting.srcs/sources_1/ip/clk_gen/clk_gen.vho
./booting.srcs/sources_1/ip/clk_gen/clk_gen.dcp
./booting.srcs/sources_1/ip/clk_gen/clk_gen_stub.v
./booting.srcs/sources_1/ip/clk_gen/clk_gen_stub.vhdl
./booting.srcs/sources_1/ip/clk_gen/clk_gen_sim_netlist.v
./booting.srcs/sources_1/ip/clk_gen/clk_gen_sim_netlist.vhdl
./booting.srcs/sources_1/ip/clk_gen/clk_gen.xdc
./booting.srcs/sources_1/ip/clk_gen/clk_gen_ooc.xdc
./booting.srcs/sources_1/ip/clk_gen/mmcm_pll_drp_func_7s_mmcm.vh
./booting.srcs/sources_1/ip/clk_gen/mmcm_pll_drp_func_7s_pll.vh
./booting.srcs/sources_1/ip/clk_gen/mmcm_pll_drp_func_us_mmcm.vh
./booting.srcs/sources_1/ip/clk_gen/mmcm_pll_drp_func_us_pll.vh
./booting.srcs/sources_1/ip/clk_gen/mmcm_pll_drp_func_us_plus_pll.vh
./booting.srcs/sources_1/ip/clk_gen/mmcm_pll_drp_func_us_plus_mmcm.vh
./booting.srcs/sources_1/ip/clk_gen/clk_gen_clk_wiz.v
./booting.srcs/sources_1/ip/clk_gen/clk_gen.v
./booting.srcs/sources_1/ip/clk_gen/clk_gen.xml

Section (3.4) - REMOTE IP DEFINITIONS
-------------------------------------
List of all the remote IP's present in the archived project:-

<sources_1>
None

<clk_gen>
None

Section (4) - JOURNAL/LOG FILES
-------------------------------
List of Journal/Log files that were added to the archived project:-

Source File = /home/andreas/vivado.jou
Archived Location = ./booting/vivado.jou

Source File = /home/andreas/vivado.log
Archived Location = ./booting/vivado.log

Section (5) - CONFIGURATION SETTINGS/FILES
------------------------------------------
List of configuration settings/files that were added to the archived project:-


