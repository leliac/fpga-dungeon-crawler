# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "DATA_ID_NUM_BITS" -parent ${Page_0}
  ipgui::add_param $IPINST -name "MAPS_NUM" -parent ${Page_0}
  ipgui::add_param $IPINST -name "MAP_NUM_TILES_X" -parent ${Page_0}
  ipgui::add_param $IPINST -name "MAP_NUM_TILES_Y" -parent ${Page_0}
  ipgui::add_param $IPINST -name "PACKET_IN_NUM_BITS" -parent ${Page_0}
  ipgui::add_param $IPINST -name "PIXEL_NUM_BITS" -parent ${Page_0}
  ipgui::add_param $IPINST -name "SPRITES_NUM" -parent ${Page_0}
  ipgui::add_param $IPINST -name "SPRITE_NUM_PIXELS" -parent ${Page_0}
  ipgui::add_param $IPINST -name "TILE_NUM_BITS" -parent ${Page_0}


}

proc update_PARAM_VALUE.DATA_ID_NUM_BITS { PARAM_VALUE.DATA_ID_NUM_BITS } {
	# Procedure called to update DATA_ID_NUM_BITS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DATA_ID_NUM_BITS { PARAM_VALUE.DATA_ID_NUM_BITS } {
	# Procedure called to validate DATA_ID_NUM_BITS
	return true
}

proc update_PARAM_VALUE.MAPS_NUM { PARAM_VALUE.MAPS_NUM } {
	# Procedure called to update MAPS_NUM when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.MAPS_NUM { PARAM_VALUE.MAPS_NUM } {
	# Procedure called to validate MAPS_NUM
	return true
}

proc update_PARAM_VALUE.MAP_NUM_TILES_X { PARAM_VALUE.MAP_NUM_TILES_X } {
	# Procedure called to update MAP_NUM_TILES_X when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.MAP_NUM_TILES_X { PARAM_VALUE.MAP_NUM_TILES_X } {
	# Procedure called to validate MAP_NUM_TILES_X
	return true
}

proc update_PARAM_VALUE.MAP_NUM_TILES_Y { PARAM_VALUE.MAP_NUM_TILES_Y } {
	# Procedure called to update MAP_NUM_TILES_Y when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.MAP_NUM_TILES_Y { PARAM_VALUE.MAP_NUM_TILES_Y } {
	# Procedure called to validate MAP_NUM_TILES_Y
	return true
}

proc update_PARAM_VALUE.PACKET_IN_NUM_BITS { PARAM_VALUE.PACKET_IN_NUM_BITS } {
	# Procedure called to update PACKET_IN_NUM_BITS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.PACKET_IN_NUM_BITS { PARAM_VALUE.PACKET_IN_NUM_BITS } {
	# Procedure called to validate PACKET_IN_NUM_BITS
	return true
}

proc update_PARAM_VALUE.PIXEL_NUM_BITS { PARAM_VALUE.PIXEL_NUM_BITS } {
	# Procedure called to update PIXEL_NUM_BITS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.PIXEL_NUM_BITS { PARAM_VALUE.PIXEL_NUM_BITS } {
	# Procedure called to validate PIXEL_NUM_BITS
	return true
}

proc update_PARAM_VALUE.SPRITES_NUM { PARAM_VALUE.SPRITES_NUM } {
	# Procedure called to update SPRITES_NUM when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SPRITES_NUM { PARAM_VALUE.SPRITES_NUM } {
	# Procedure called to validate SPRITES_NUM
	return true
}

proc update_PARAM_VALUE.SPRITE_NUM_PIXELS { PARAM_VALUE.SPRITE_NUM_PIXELS } {
	# Procedure called to update SPRITE_NUM_PIXELS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SPRITE_NUM_PIXELS { PARAM_VALUE.SPRITE_NUM_PIXELS } {
	# Procedure called to validate SPRITE_NUM_PIXELS
	return true
}

proc update_PARAM_VALUE.TILE_NUM_BITS { PARAM_VALUE.TILE_NUM_BITS } {
	# Procedure called to update TILE_NUM_BITS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.TILE_NUM_BITS { PARAM_VALUE.TILE_NUM_BITS } {
	# Procedure called to validate TILE_NUM_BITS
	return true
}


proc update_MODELPARAM_VALUE.DATA_ID_NUM_BITS { MODELPARAM_VALUE.DATA_ID_NUM_BITS PARAM_VALUE.DATA_ID_NUM_BITS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DATA_ID_NUM_BITS}] ${MODELPARAM_VALUE.DATA_ID_NUM_BITS}
}

proc update_MODELPARAM_VALUE.PACKET_IN_NUM_BITS { MODELPARAM_VALUE.PACKET_IN_NUM_BITS PARAM_VALUE.PACKET_IN_NUM_BITS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.PACKET_IN_NUM_BITS}] ${MODELPARAM_VALUE.PACKET_IN_NUM_BITS}
}

proc update_MODELPARAM_VALUE.MAPS_NUM { MODELPARAM_VALUE.MAPS_NUM PARAM_VALUE.MAPS_NUM } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.MAPS_NUM}] ${MODELPARAM_VALUE.MAPS_NUM}
}

proc update_MODELPARAM_VALUE.MAP_NUM_TILES_X { MODELPARAM_VALUE.MAP_NUM_TILES_X PARAM_VALUE.MAP_NUM_TILES_X } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.MAP_NUM_TILES_X}] ${MODELPARAM_VALUE.MAP_NUM_TILES_X}
}

proc update_MODELPARAM_VALUE.MAP_NUM_TILES_Y { MODELPARAM_VALUE.MAP_NUM_TILES_Y PARAM_VALUE.MAP_NUM_TILES_Y } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.MAP_NUM_TILES_Y}] ${MODELPARAM_VALUE.MAP_NUM_TILES_Y}
}

proc update_MODELPARAM_VALUE.TILE_NUM_BITS { MODELPARAM_VALUE.TILE_NUM_BITS PARAM_VALUE.TILE_NUM_BITS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.TILE_NUM_BITS}] ${MODELPARAM_VALUE.TILE_NUM_BITS}
}

proc update_MODELPARAM_VALUE.SPRITES_NUM { MODELPARAM_VALUE.SPRITES_NUM PARAM_VALUE.SPRITES_NUM } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SPRITES_NUM}] ${MODELPARAM_VALUE.SPRITES_NUM}
}

proc update_MODELPARAM_VALUE.SPRITE_NUM_PIXELS { MODELPARAM_VALUE.SPRITE_NUM_PIXELS PARAM_VALUE.SPRITE_NUM_PIXELS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SPRITE_NUM_PIXELS}] ${MODELPARAM_VALUE.SPRITE_NUM_PIXELS}
}

proc update_MODELPARAM_VALUE.PIXEL_NUM_BITS { MODELPARAM_VALUE.PIXEL_NUM_BITS PARAM_VALUE.PIXEL_NUM_BITS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.PIXEL_NUM_BITS}] ${MODELPARAM_VALUE.PIXEL_NUM_BITS}
}

