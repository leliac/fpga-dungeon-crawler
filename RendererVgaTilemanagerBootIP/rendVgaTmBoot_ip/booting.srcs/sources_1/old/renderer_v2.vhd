----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 20.04.2017 11:02:05
-- Design Name: 
-- Module Name: tilemap - Behavioral
-- Target Devices: Zybo
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.pixels_p.ALL;
----------
--boundary_X: received from the boundary writer x coord of the leftmost upper tile
--boundary_Y: received from the boundary writer y coord of the leftmost upper tile
--tile_id: value stored in the tilemap at addr_X, addr_Y
--addr_X/addr_Y: coords that are sent to the tilemap data queue to retrieve tile_id
entity renderer is
   Port ( addr_X : out INTEGER range 0 to 81;
          addr_Y : out INTEGER range 0 to 63;
          pixel_bus : out STD_LOGIC_VECTOR(15 downto 0);
          this_X : in INTEGER range 0 to 81; --player position
          this_Y : in INTEGER range 0 to 63;
          mode : in STD_LOGIC; --whether to print the finder
          canput : in STD_LOGIC; --to set the color of the finder
          screen_X : in INTEGER range 0 to 900; --calculated inside the vga module
          screen_Y : in INTEGER range 0 to 530;
          tile_id : in STD_LOGIC_VECTOR(18 downto 0); --size subject to change
          clk: in STD_LOGIC;
          output_enable : in STD_LOGIC;
          pixel_clk: in STD_LOGIC;
          pixel_in : in STD_LOGIC_VECTOR (5 downto 0); --6 bit color
          fetching : in STD_LOGIC); --is 1 while tiles are being tranfered
end renderer;

architecture Behavioral of renderer is

component blk_mem_gen_0 is
  port (
    clka : IN STD_LOGIC;
    wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    addra : IN STD_LOGIC_VECTOR(14 DOWNTO 0);
    dina : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
    douta : OUT STD_LOGIC_VECTOR(5 DOWNTO 0)
  );
end component;

--declare distributed rams (tileline, pixel line)
--pixel matrix uses more ram than the previously used pixelram
--however, it pixel ram couldn't be implemented as distributed ram, so the actual usage doesn't matter, it uses 32kb either way
constant SPRITES_NUM: INTEGER range 0 to 256 := 64;
type tileram is array (0 to 39) of std_logic_vector(18 downto 0);
signal tiles: tileram:=(others=>(others=>'0'));
type sprites_arr is array(0 to SPRITES_NUM*256) of STD_LOGIC_VECTOR(5 downto 0);
signal sprites_data: sprites_arr := (others=>(others=>'0'));
--signal sprites_data: sprites_arr := initSprite(0);
signal isFinder : STD_LOGIC_VECTOR (1 downto 0) := (others=>'0'); --tell if i need to write the finder
attribute ram_style:string;
attribute ram_style of tiles: signal is "distributed";
attribute ram_style of sprites_data: signal is "block";
constant TILES_IN_LINE: INTEGER range 0 to 40 := 40;
constant LINES_IN_FRAME: INTEGER range 0 to 28:= 27;
constant LINES_IN_HUD_AND_FRAME: INTEGER range 0 to 31:= 30;
constant TILEMAP_HUD_START_MINUS_27:INTEGER range 0 to 34:=60-27;
constant H_PIXELS_MAX:INTEGER range 0 to 641:= 640;
constant v_PIXELS_MAX:INTEGER range 0 to 481:= 480;
constant V_PIXELS_IN_TILE:INTEGER range 0 to 16 := 16;
constant ground_sprite_id: INTEGER range 0 to 128:= 0; --this will probably change.
--bnd part
signal boundary_X, boundary_Y : INTEGER range 0 to 63 := 0;
constant LeftX : natural := 20;
constant RightX: natural := 60;
constant TopY: natural :=15;
constant BotY: natural :=45;
constant centralRightX: natural :=(LeftX + RightX)/2;
constant centralBotY: natural :=(TopY+BotY)/2;
signal ind : INTEGER range 0 to SPRITES_NUM*256 := 0;

signal wea : STD_LOGIC_VECTOR(0 downto 0);
signal addra : STD_LOGIC_VECTOR(14 downto 0);
signal douta : STD_LOGIC_VECTOR(5 downto 0);

begin
  
  wea(0) <= fetching;
  
  spritesBram : blk_mem_gen_0 port map(clka=>clk, wea=>wea, addra=>addra, dina=>pixel_in, douta=>douta);
  
  sprites_fetch : process(clk)
  begin
    if falling_edge(clk) then
      if fetching='1' and ind<SPRITES_NUM*256 then
        sprites_data(ind) <= pixel_in;
        ind<=ind+1;
      end if;
    end if;
  end process;
  
  --processes required to prepare the output buffer for output
  read_tile_from_map: process(clk)
  variable line_complete : STD_LOGIC := '0';
  variable tile_wrote : STD_LOGIC := '1';
  variable tile_column_write_counter, tile_row_write_counter: INTEGER range 0 to 50:=0;
  begin
    if(rising_edge(clk) ) then
      if(tile_wrote='0' and output_enable='0') then
        tiles(tile_column_write_counter)<=tile_id; --fetch from tilemap
        tile_wrote:='1';
        tile_column_write_counter:=tile_column_write_counter+1; --inc on last time it is used
        if(tile_column_write_counter = TILES_IN_LINE)then
          line_complete:='1';
        end if;
      end if;
      if(output_enable='1') then
        if (screen_Y mod 16)=15 then --reinit only when line has been written
          line_complete:='0';
          tile_column_write_counter:=0;
          tile_row_write_counter:=(screen_Y+1)/16;
          if tile_row_write_counter=30 then 
            tile_row_write_counter:=0;
            if( this_x < LeftX) then --reinit the boundaries
                boundary_X <= 0;
            elsif(this_x > RightX) then
                boundary_X <= centralRightX;
            else
                boundary_X <= (this_x-LeftX);
            end if;
            if( this_y < TopY) then
                boundary_Y <= 0;
            elsif(this_y > BotY) then
                boundary_Y <= centralBotY;
            else
                boundary_Y <= (this_y-TopY);
            end if; 
          end if;
        end if;
      elsif screen_X>640 and (screen_Y<480 or screen_Y>523) then --only on wanted blanking time
        if(line_complete='0' and tile_wrote='1') then
          --if(tile_row_write_counter<LINES_IN_FRAME) then
          if(tile_row_write_counter<LINES_IN_HUD_AND_FRAME) then
            addr_X<=boundary_X+tile_column_write_counter;
            addr_Y<=boundary_Y+tile_row_write_counter;
            --finder part
            if this_Y=boundary_Y+tile_row_write_counter then
              isFinder<="10"; --first line of finder
            elsif this_Y=boundary_Y+tile_row_write_counter-1 then
              isFinder<="01"; --second line of finder
            else
              isFinder<="00"; --not inside the finder
            end if;
            
            tile_wrote:='0';
--           elsif(tile_row_write_counter<LINES_IN_HUD_AND_FRAME) then
--             addr_X<=tile_column_write_counter;
--             addr_Y<=TILEMAP_HUD_START_MINUS_27+tile_row_write_counter;
--             addr_change<='1';
          end if;
        end if;
      elsif screen_Y>480 then --start with the next (0 is done in the end)
      end if;
    end if;
  end process;
      
  out_creator: process(pixel_clk)
  variable pixel: std_logic_vector(15 downto 0) := (others=>'0');
  variable sprite_x, sprite_x_rev, sprite_y: integer range 0 to 15:=0;
  variable current_tile : integer range 0 to 40:=0;
  variable temp_tile: std_logic_vector(18 downto 0);
  variable idsprite : INTEGER range 0 to 127;
  begin
  
      --output_enable means that output is required. hsync and vsync are handled through internal counters, but not the blanking period
      if(output_enable = '1') then
        if(falling_edge(pixel_clk)) then 
           
          if(screen_X=0) then
            current_tile:=0;
          end if;
          idsprite:=to_integer(unsigned(tiles(current_tile)(6 downto 0)));
          --if the sprite is meant to be flipped, I do so by subtracting the x position to 15
          sprite_x:=screen_X mod 16; --correct shift
          sprite_y:=screen_Y mod 16;
          if(tiles(current_tile)(0) = '1') then --reverse
              sprite_x_rev:=15-sprite_x; --sprite_x is used as index, should not have strange values
          end if;
          if tiles(current_tile)(0)='1' then
            pixel :=  b6to16(sprites_data(idsprite*256 + sprite_x_rev+16*sprite_y));
          else
            pixel :=  b6to16(sprites_data(idsprite*256 + sprite_x+16*sprite_y));
          end if;
          
          --check if the pixel should be replaced with a background sprite
          if(pixel = "0000000000000000") then
             pixel :=  b6to16(sprites_data(ground_sprite_id*256 + sprite_x+16*sprite_y));
          end if;
          --finder part 
          if mode='0' then --needs to print the finder
            if isFinder="10" then --print the first line
              if this_X=current_tile+boundary_X then --in the first column
                if (sprite_x<9 and sprite_y<3) or (sprite_x<3 and sprite_y<9) then
                  if canput='1' then
                    pixel:="1111110000000000"; --set green
                  else
                    pixel:="0000000000011111"; --set red
                  end if; 
                end if;
              elsif this_X=current_tile+boundary_X-1 then --in the second column
                if (sprite_x>6 and sprite_y<3) or (sprite_x>12 and sprite_y<9) then
                  if canput='1' then
                    pixel:="1111110000000000"; --set green
                  else
                    pixel:="0000000000011111"; --set red
                  end if; 
                end if;
              end if;
            elsif isFinder="01" then --second line
              if this_X=current_tile+boundary_X then --in the first column
                if (sprite_x<9 and sprite_y>12) or (sprite_x<3 and sprite_y>6) then
                  if canput='1' then
                    pixel:="1111110000000000"; --set green
                  else
                    pixel:="0000000000011111"; --set red
                  end if; 
                end if;
              elsif this_X=current_tile+boundary_X-1 then --in the second column
                if (sprite_x>6 and sprite_y>12) or (sprite_x>12 and sprite_y>6) then
                  if canput='1' then
                    pixel:="1111110000000000"; --set green
                  else
                    pixel:="0000000000011111"; --set red
                  end if; 
                end if;
              end if;          
            end if;
          end if;
          
          if(sprite_x = 15) then --only to reinitialize
            current_tile:=current_tile+1;
          end if;
          pixel_bus<=pixel;
        end if;
--      elsif screen_X>700 then
--        current_tile:=0;  
--        curr_tile<=current_tile; --debug
      end if;
  end process;
end Behavioral;