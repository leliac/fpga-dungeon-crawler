----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 20.04.2017 11:02:05
-- Design Name: 
-- Module Name: tilemap - Behavioral
-- Target Devices: Zybo
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.pixels_p.ALL;
----------
--boundary_X: received from the boundary writer x coord of the leftmost upper tile
--boundary_Y: received from the boundary writer y coord of the leftmost upper tile
--tile_id: value stored in the tilemap at addr_X, addr_Y
--addr_X/addr_Y: coords that are sent to the tilemap data queue to retrieve tile_id
entity renderer_old is
   Port ( addr_X : out INTEGER range 0 to 81;
          addr_Y : out INTEGER range 0 to 63;
          pixel_bus : out STD_LOGIC_VECTOR(15 downto 0);
          boundary_X : in INTEGER range 0 to 81;
          boundary_Y : in INTEGER range 0 to 63;
          screen_X : in INTEGER range 0 to 900; --calculated inside the vga module
          screen_Y : in INTEGER range 0 to 530;
          tile_id : in STD_LOGIC_VECTOR(18 downto 0); --changed to 19 bits
          clk: in STD_LOGIC;
          output_enable : in STD_LOGIC;
          pixel_clk: in STD_LOGIC);
end renderer_old;
architecture Behavioral of renderer_old is
--declare distributed rams (tileline, pixel line)
--pixel matrix uses more ram than the previously used pixelram
--however, it pixel ram couldn't be implemented as distributed ram, so the actual usage doesn't matter, it uses 32kb either way
type tileram is array (0 to 39) of std_logic_vector(18 downto 0);
signal tiles: tileram:=(others=>(others=>'0'));
--signal tiles: tileram:=((others=>'0'),(others=>'0'),"00110110","11110010","00000001",(others=>'1'),others=>(others=>'0'));
signal sprites_data: sprites_array := initSprite(0);
attribute ram_style:string;
attribute ram_style of tiles: signal is "distributed";
attribute ram_style of sprites_data: signal is "distributed";
constant TILES_IN_LINE: INTEGER range 0 to 40 := 40;
constant LINES_IN_FRAME: INTEGER range 0 to 28:= 27;
constant LINES_IN_HUD_AND_FRAME: INTEGER range 0 to 31:= 30;
constant TILEMAP_HUD_START_MINUS_27:INTEGER range 0 to 34:=60-27;
constant H_PIXELS_MAX:INTEGER range 0 to 641:= 640;
constant v_PIXELS_MAX:INTEGER range 0 to 481:= 480;
constant V_PIXELS_IN_TILE:INTEGER range 0 to 16 := 16;
constant ground_sprite_id: INTEGER range 0 to 128:= 0; --this will probably change.
--debugging
signal curr_tile, sx, sy : INTEGER;
signal cx, cy : INTEGER;
signal line_c, tile_w : STD_LOGIC;
begin
  
  --processes required to prepare the output buffer for output
  read_tile_from_map: process(clk)
  variable line_complete : STD_LOGIC := '0';
  variable tile_wrote : STD_LOGIC := '1';
  variable tile_column_write_counter, tile_row_write_counter: INTEGER range 0 to 50:=0;
  begin
    if(rising_edge(clk)) then

      if(tile_wrote='0' and output_enable='0') then
        tiles(tile_column_write_counter)<=tile_id;
        tile_wrote:='1';
        tile_column_write_counter:=tile_column_write_counter+1; --inc on last time it is used
        if(tile_column_write_counter = TILES_IN_LINE)then
          line_complete:='1';
        end if;
      end if;

      if(output_enable='1') then
        if (screen_Y mod 16)=15 then --reinit only when line has been written
          line_complete:='0';
          tile_column_write_counter:=0;
          tile_row_write_counter:=(screen_Y+1)/16;
          if tile_row_write_counter=30 then tile_row_write_counter:=0; end if;
        end if;
      elsif screen_X>640 then --only on wanted blanking time
        if(line_complete='0' and tile_wrote='1') then
          --if(tile_row_write_counter<LINES_IN_FRAME) then
          if(tile_row_write_counter<LINES_IN_HUD_AND_FRAME) then
            addr_X<=boundary_X+tile_column_write_counter;
            addr_Y<=boundary_Y+tile_row_write_counter;
            tile_wrote:='0';
--           elsif(tile_row_write_counter<LINES_IN_HUD_AND_FRAME) then
--             addr_X<=tile_column_write_counter;
--             addr_Y<=TILEMAP_HUD_START_MINUS_27+tile_row_write_counter;
--             addr_change<='1';
          end if;
        end if;
      elsif screen_Y>480 then --start with the next (0 is done in the end)
      end if;
    end if;

    cx<=tile_column_write_counter; cy<=tile_row_write_counter;
    line_c <= line_complete; tile_w <= tile_wrote;

  end process;
      
  out_creator: process(pixel_clk)
  variable pixel: std_logic_vector(15 downto 0) := (others=>'0');
  variable sprite_x, sprite_x_rev, sprite_y: integer range 0 to 15:=0;
  variable current_tile : integer range 0 to 40:=0;
  variable temp_tile: std_logic_vector(18 downto 0);
  begin
  
      --output_enable means that output is required. hsync and vsync are handled through internal counters, but not the blanking period
      if(output_enable = '1') then
        if(falling_edge(pixel_clk)) then 
        
          if(screen_X=0) then
            current_tile:=0;
            curr_tile<=current_tile; --debug
          end if;
          --if the sprite is meant to be flipped, I do so by subtracting the x position to 15
          sprite_x:=screen_X mod 16; --correct shift
          sprite_y:=screen_Y mod 16;
          if(tiles(current_tile)(0) = '1') then --reverse
              sprite_x_rev:=15-sprite_x; --sprite_x is used as index, should not have strange values
          end if;

          if tiles(current_tile)(0)='1' then
            pixel := get_pixel(sprite_x_rev, sprite_y, sprites_data(to_integer(unsigned(tiles(current_tile)(7 downto 1)))));
          else
            pixel := get_pixel(sprite_x, sprite_y, sprites_data(to_integer(unsigned(tiles(current_tile)(7 downto 1)))));
          end if;
          
          --check if the pixel should be replaced with a background sprite
           if(pixel = "0000000000000000") then
              pixel:=get_pixel(sprite_x, sprite_y, sprites_data(ground_sprite_id));
           end if;
          
          if(sprite_x = 15) then --only to reinitialize
            current_tile:=current_tile+1;
            curr_tile<=current_tile; --debug
          end if;
          pixel_bus<=pixel;
        end if;
--      elsif screen_X>700 then
--        current_tile:=0;  
--        curr_tile<=current_tile; --debug
      end if;
      sx<=sprite_X; sy<=sprite_Y;
  end process;
end Behavioral;