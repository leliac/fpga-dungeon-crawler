----------------------------------------------------------------------------------
-- University: Politecnico di Torino 
-- Authors: Gabriele Cazzato, Gabriele Monaco
-- 
-- Create Date: 05/22/2017 03:16:37 PM
-- Design Name: Renderer, VGA Connector, Tile Manager, Booting
-- Module Name: Booting - Behavioral
-- Project Name: Computer Architecture Project 2017
-- Target Devices: Digilent ZYBO
-- Tool Versions: Vivado 2017.1
-- Description: 
-- This module is used at boot time and every time a new map needs to be loaded.
-- At boot time, the board sends a signal to the other one
-- and waits for a signal from it (these signals depend on the mode).
-- While waiting, a counter is increased, which is used as a random map selector
-- after being multiplied by the one obtained from the other board (to share the same constant).
-- This calculation is done in the top module and one of the two numbers (the lowest) will be 1.
-- The module then asks the AXI Data Fetcher to send all sprites
-- stored in DDR3 memory, which are received pixel by pixel.
-- When the transfer is over, the AXI Data Fetcher is invoked again to transfer
-- the randomly selected map, which is received tile by tile.
-- The module then enters an idle state, which is abandoned when a new map transfer is needed.
--
-- Dependencies: 
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity booting is
    Generic (
        MAP_ID_NUM_BITS : INTEGER := 8;
        PACKET_IN_NUM_BITS : INTEGER := 32;
        MAPS_NUM : INTEGER := 128;
        MAP_NUM_TILES_X : INTEGER := 80;
        MAP_NUM_TILES_Y : INTEGER := 60;
        TILE_NUM_BITS : INTEGER := 19;
        SPRITES_NUM : INTEGER := 128;
        SPRITE_NUM_PIXELS : INTEGER := 16*16;
        PIXEL_NUM_BITS : INTEGER := 6
    );
    Port (
        clock : in STD_LOGIC;
        mode : in STD_LOGIC;
        ch_rx0 : in STD_LOGIC;
        ch_rx1 : in STD_LOGIC;
        ch_tx0 : out STD_LOGIC;
        ch_tx1 : out STD_LOGIC;
        tmp_rand : out INTEGER range 0 to MAPS_NUM-1;
        final_rand : in INTEGER range 0 to MAPS_NUM-1;
        Xmap : out INTEGER range 0 to MAP_NUM_TILES_X-1;
        Ymap : out INTEGER range 0 to MAP_NUM_TILES_Y-1;
        tile_out : out STD_LOGIC_VECTOR (TILE_NUM_BITS-1 downto 0);
        pixel_out : out STD_LOGIC_VECTOR (PIXEL_NUM_BITS-1 downto 0);
        write_enable : out STD_LOGIC;
        fetching_sprites : out STD_LOGIC;
        
        --AXI Data Fetcher
        fetch : out STD_LOGIC; --set to 1 to start transfer
        data_type : out STD_LOGIC; --0 for map, 1 for all sprites
        map_id : out STD_LOGIC_VECTOR (MAP_ID_NUM_BITS-1 downto 0); --map index
        packet_in : in STD_LOGIC_VECTOR (PACKET_IN_NUM_BITS-1 downto 0); --32-bit tile/pixel (discard useless bits)
        fetching : in STD_LOGIC; --is 1 while packets are being tranfered
        
        --Debug
        led0 : out STD_LOGIC;
        led1 : out STD_LOGIC;
        led2 : out STD_LOGIC;
        led3 : out STD_LOGIC
    );
end booting;

architecture Behavioral of booting is

constant \MAP\ : STD_LOGIC := '0';
constant SPRITES : STD_LOGIC := '1';

constant CONNECTING : INTEGER := 0;
constant CONNECTED : INTEGER := 1;
constant SPRITES_TRNSF_INIT : INTEGER := 2;
constant SPRITES_TRNSF : INTEGER := 3;
constant MAP_TRNSF_INIT : INTEGER := 4;
constant MAP_TRNSF : INTEGER := 5;
constant IDLE : INTEGER := 6;
constant SLEEP : INTEGER := 7;

begin
    initiating : process(clock)
    variable cnt : NATURAL := 0;
    variable rand : INTEGER range 0 to MAPS_NUM-1 := 0;
    variable state : INTEGER range 0 to 7 := CONNECTING;
  
    begin
        if rising_edge(clock) then
            case ( state ) is
            when CONNECTING =>
                if mode = '1' then
                    ch_tx0 <= '0'; --send idle
                    ch_tx1 <= '1'; --send communication
                    if ch_rx0 = '1' then --other board is trying to communicate
                        state := CONNECTED;
                    end if;
                else
                    ch_tx0 <= '1'; --send communication
                    ch_tx1 <= '0'; --send idle
                    if ch_rx1 = '1' then --other board is trying to communicate
                        state := CONNECTED;
                    end if;      
                end if;
                fetch <= '0'; --don't start transfer yet
                tmp_rand <= 0; --still waiting
                fetching_sprites <= '0';
                pixel_out <= "000000";
                cnt := cnt+1; --advancing
                --debug
                led0 <= '0';
                led1 <= '0';
                led2 <= '0';
                led3 <= '0';
            when CONNECTED =>
                if final_rand = 0 then --connection established
                    tmp_rand <= (cnt mod MAPS_NUM) + 1;
                else
                    state := SPRITES_TRNSF_INIT;
                end if;                
            when SPRITES_TRNSF_INIT =>
                rand := final_rand - 1; --to use 0 as idle the constant is summed by 1
                cnt := 0;
                data_type <= SPRITES;
                map_id <= "00000000";
                fetch <= '1'; --start transfer
                state := SPRITES_TRNSF;
                led0 <= '1'; --debug: sprites transfer init
            when SPRITES_TRNSF =>
                if fetching = '1' then
                    fetch <= '0';
                    fetching_sprites <= '1';
                    pixel_out <= packet_in(PIXEL_NUM_BITS-1 downto 0); --selecting significant bits only
                    cnt := cnt+1;
                end if;
                if cnt >= SPRITES_NUM*SPRITE_NUM_PIXELS then
                    fetching_sprites <= '0';
                    pixel_out <= "000000";
                    cnt := 0;
                    state := SLEEP;
                    led1 <= '1'; --debug: sprites transfer complete
                end if;
            when SLEEP =>
                if (cnt >= 100) then
                    state := MAP_TRNSF_INIT;
                end if;
                cnt := cnt+1;
            when MAP_TRNSF_INIT =>
                --rand := final_rand - 1; --to use 0 as idle the constant is summed by 1
                cnt := 0;
                data_type <= \MAP\;
                map_id <= std_logic_vector(to_unsigned(rand, map_id'length)); --is this the desired behavior?
                fetch <= '1'; --start map transfer
                state := MAP_TRNSF;
                led2 <= '1'; --debug: map transfer init
            when MAP_TRNSF =>
                if fetching ='1' then
                    fetch <= '0';
                    Xmap <= cnt mod MAP_NUM_TILES_X;
                    Ymap <= cnt / MAP_NUM_TILES_X;
                    tile_out <= packet_in(TILE_NUM_BITS-1 downto 0); --selecting significant bits only
                    write_enable <= '1';
                    cnt := cnt+1;
                end if;
                if cnt >= MAP_NUM_TILES_X*MAP_NUM_TILES_Y then
                    write_enable <= '0';
                    state := IDLE;
                    led3 <= '1'; --debug: map transfer complete
                end if;
            when IDLE =>
                if rand /= (final_rand-1) and rand /= 0 then
                    state := MAP_TRNSF_INIT; --re-init map transfer if random constant changes
                end if;
            end case;
        end if;
    end process;
end Behavioral;
