----------------------------------------------------------------------------------
-- University: Politecnico di Torino 
-- Authors: Gabriele Cazzato, Stefano Negri Merlo, Gabriele Monaco, Alexandru Valentin Onica, Gabriele Ponzio, Roberto Stagi
-- 
-- Create Date: 05/09/2017 10:47:09 AM
-- Design Name: TOP 
-- Module Name: Renderer+VGA Connector+Tile Manager+Booting
-- Project Name: Computer Architecture Project 2017
-- Target Devices: Digilent ZYBO
-- Tool Versions: Vivado 2017.1
-- Description: 
-- This module connects together the sub-modules
-- Renderer, VGA Connector, Tile Manager and Booting 
--
-- Dependencies: 
-- * renderer.vhd
--   * pixel_pack.vhd
-- * vga_connector.vhd
-- * tile_manager.vhd
-- * booting.vhd
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

entity rendVgaTmBoot is
generic (
    DATA_ID_NUM_BITS : NATURAL := 8;
    PACKET_IN_NUM_BITS : NATURAL := 32;
    MAPS_NUM : NATURAL := 128;
    MAP_NUM_TILES_X : NATURAL := 80;
    MAP_NUM_TILES_Y : NATURAL := 60;
    TILE_NUM_BITS : NATURAL := 19;
    SPRITES_NUM : NATURAL := 128;
    SPRITE_NUM_PIXELS : NATURAL := 16*16;
    PIXEL_NUM_BITS : NATURAL := 6
);
port (
    clk : in STD_LOGIC;
    pixel_clk : in STD_LOGIC;
    
    vga_r : out STD_LOGIC_VECTOR (4 downto 0);
    vga_g : out STD_LOGIC_VECTOR (5 downto 0);
    vga_b : out STD_LOGIC_VECTOR (4 downto 0);
    vga_hs : out STD_LOGIC;
    vga_vs : out STD_LOGIC;
    
    -- AXI Data Fetcher
    fetch_start : out STD_LOGIC;
    data_type : out STD_LOGIC;
    data_id : out STD_LOGIC_VECTOR (DATA_ID_NUM_BITS-1 downto 0);
    packet_in : in STD_LOGIC_VECTOR (PACKET_IN_NUM_BITS-1 downto 0);
    fetching : in STD_LOGIC;
    
    sw : in STD_LOGIC_VECTOR (3 downto 0); --can be used to simulate other board interaction
    led0 : out STD_LOGIC;
    led1 : out STD_LOGIC;
    led2 : out STD_LOGIC;
    led3 : out STD_LOGIC
);
end rendVgaTmBoot;

-- sw(3) indicates the mode
-- 2 and 1 are the input channel:
-- mode=1 waits for sw(2)
-- mode=0 waits for sw(1)
-- to be on

architecture Behavioral of rendVgaTmBoot is
component tile_manager is
generic (
    MAP_NUM_TILES_X : NATURAL;
    MAP_NUM_TILES_Y : NATURAL;
    TILE_NUM_BITS : NATURAL
);
port (
    clock : in STD_LOGIC;
    we : in STD_LOGIC;
    x_r : in NATURAL range 0 to MAP_NUM_TILES_X;
    y_r : in NATURAL range 0 to MAP_NUM_TILES_Y;
    x_w : in NATURAL range 0 to MAP_NUM_TILES_X;
    y_w : in NATURAL range 0 to MAP_NUM_TILES_Y;
    in_tile : in STD_LOGIC_VECTOR (TILE_NUM_BITS-1 downto 0);
    out_tile : out STD_LOGIC_VECTOR (TILE_NUM_BITS-1 downto 0)
);
end component;

component renderer is
generic (
    MAP_NUM_TILES_X : NATURAL;
    MAP_NUM_TILES_Y : NATURAL;
    VIEW_NUM_TILES_X : NATURAL;
    VIEW_NUM_TILES_Y : NATURAL;
    TILE_NUM_BITS : NATURAL;
    SPRITES_NUM : NATURAL;
    SPRITE_NUM_PIXELS : NATURAL;
    PIXEL_NUM_BITS : NATURAL
);
port (
    addr_X : out INTEGER range 0 to MAP_NUM_TILES_X+1;
    addr_Y : out INTEGER range 0 to MAP_NUM_TILES_Y+3;
    pixel_bus : out STD_LOGIC_VECTOR (15 downto 0);
    this_X : in INTEGER range 0 to MAP_NUM_TILES_X+1; --player position
    this_Y : in INTEGER range 0 to MAP_NUM_TILES_Y+3;
    mode : in STD_LOGIC; --whether to print the finder
    canput : in STD_LOGIC; --to set the color of the finder
    screen_X : in INTEGER range 0 to 900; --calculated inside the vga module
    screen_Y : in INTEGER range 0 to 530;
    tile_in : in STD_LOGIC_VECTOR (TILE_NUM_BITS-1 downto 0);
    clk: in STD_LOGIC;
    output_enable : in STD_LOGIC;
    pixel_clk: in STD_LOGIC;
    pixel_in : in STD_LOGIC_VECTOR (PIXEL_NUM_BITS-1 downto 0); --6 bit color
    fetching_sprites : in STD_LOGIC
);
end component;

component vga_connector is
port (
    pixel_bus : in STD_LOGIC_VECTOR (15 downto 0);
    clock : in STD_LOGIC;
    vga_r : out STD_LOGIC_VECTOR (4 downto 0);
    vga_g : out STD_LOGIC_VECTOR (5 downto 0);
    vga_b : out STD_LOGIC_VECTOR (4 downto 0);
    vga_hs : out STD_LOGIC;
    vga_vs : out STD_LOGIC;
    h_cnt : out NATURAL; --can be used by other modules
    v_cnt : out NATURAL;
    render_enable: out STD_LOGIC;
    anim_enable: out STD_LOGIC;
    fetch_complete : in STD_LOGIC
);
end component;

--component connInterface is
--port (
--    sys_clk : in STD_LOGIC; --will be transmitted
--    clk_rx : in STD_LOGIC; --from other board to read the signal
--    channel_rx_up : in STD_LOGIC; --listening pin (7 downto 4)
--    channel_rx_down : in STD_LOGIC; --listening pin (3 downto 0)
--    opt_rx : in STD_LOGIC; --optional pin
--    butts_rx : out STD_LOGIC_VECTOR (7 downto 0); --received
--    special_rx : out STD_LOGIC_VECTOR (3 downto 0); --additional info
--    clk_tx : out STD_LOGIC; --from this board to the other
--    channel_tx_up : out STD_LOGIC; --transmitting pin (7 downto 4)
--    channel_tx_down : out STD_LOGIC; --transmitting pin (3 downto 0)
--    opt_tx : out STD_LOGIC; --optional pin
--    butts_tx : in STD_LOGIC_VECTOR (7 downto 0); --to send
--    special_tx : in STD_LOGIC_VECTOR (3 downto 0) --additional info
--);
--end component;

component booting is
generic (
    DATA_ID_NUM_BITS : NATURAL;
    PACKET_IN_NUM_BITS : NATURAL;
    MAPS_NUM : NATURAL;
    MAP_NUM_TILES_X : NATURAL;
    MAP_NUM_TILES_Y : NATURAL;
    TILE_NUM_BITS : NATURAL;
    SPRITES_NUM : NATURAL;
    SPRITE_NUM_PIXELS : NATURAL;
    PIXEL_NUM_BITS : NATURAL
);
port ( 
    clock : in STD_LOGIC;
    mode : in STD_LOGIC;
    ch_rx0 : in STD_LOGIC;
    ch_rx1 : in STD_LOGIC;
    ch_tx0 : out STD_LOGIC;
    ch_tx1 : out STD_LOGIC;
    tmp_rand : out INTEGER range 0 to MAPS_NUM-1;
    final_rand : in INTEGER range 0 to MAPS_NUM-1;
    Xmap : out INTEGER range 0 to MAP_NUM_TILES_X-1;
    Ymap : out INTEGER range 0 to MAP_NUM_TILES_Y-1;
    tile_out : out STD_LOGIC_VECTOR (TILE_NUM_BITS-1 downto 0);
    pixel_out : out STD_LOGIC_VECTOR (PIXEL_NUM_BITS-1 downto 0);
    fetching_map : out STD_LOGIC;
    fetching_sprites : out STD_LOGIC;
    fetch_complete : out STD_LOGIC;
    fetch_start : out STD_LOGIC;
    data_type : out STD_LOGIC;
    data_id : out STD_LOGIC_VECTOR (DATA_ID_NUM_BITS-1 downto 0);
    packet_in : in STD_LOGIC_VECTOR (PACKET_IN_NUM_BITS-1 downto 0);
    fetching : in STD_LOGIC;
    led0 : out STD_LOGIC;
    led1 : out STD_LOGIC;
    led2 : out STD_LOGIC;
    led3 : out STD_LOGIC
);
end component;

signal pbus : STD_LOGIC_VECTOR (15 downto 0) := (others => '0');
signal r_ena,w_ena,addr_ch,new_tile : STD_LOGIC := '0';
signal x,y,x_w,y_w,x_r,y_r : INTEGER := 0;
signal ch_in0,ch_in1,ch_out0,ch_out1 : STD_LOGIC := '0'; --may consider using specific channel
signal random, tmprand : INTEGER := 0;
--signal clk_in,clk_out,ch_in_up,ch_in_down,ch_out_up,ch_out_down, opt_in, opt_out: STD_LOGIC := '0';
--signal butts_in,butts_out : STD_LOGIC_VECTOR (7 downto 0) := (others=>'0');
--signal sp_in,sp_out : STD_LOGIC_VECTOR (3 downto 0) := (others=>'0');
signal tile_in, tile_out : STD_LOGIC_VECTOR (TILE_NUM_BITS-1 downto 0) := (others=>'0');
signal pixel : STD_LOGIC_VECTOR (PIXEL_NUM_BITS-1 downto 0);
signal fetching_sprites, fetch_complete : STD_LOGIC;

begin
    rend : renderer
    generic map (
        MAP_NUM_TILES_X => MAP_NUM_TILES_X,
        MAP_NUM_TILES_Y => MAP_NUM_TILES_Y,
        VIEW_NUM_TILES_X => MAP_NUM_TILES_X/2,
        VIEW_NUM_TILES_Y => MAP_NUM_TILES_Y/2,
        TILE_NUM_BITS => TILE_NUM_BITS,
        SPRITES_NUM => SPRITES_NUM,
        SPRITE_NUM_PIXELS => SPRITE_NUM_PIXELS,
        PIXEL_NUM_BITS => PIXEL_NUM_BITS
    )
    port map (
        this_X => 1,
        this_Y => 0,
        tile_in => tile_out,
        clk => clk,
        screen_X => x,
        screen_Y => y,
        pixel_clk => pixel_clk, 
        output_enable => r_ena,
        pixel_bus => pbus,
        addr_X => x_r,
        addr_Y => y_r,
        fetching_sprites => fetching_sprites,
        pixel_in => pixel,
        mode => sw(3),
        canput => '0'
    );

    vga : vga_connector
    port map (
        pixel_bus => pbus,
        clock => pixel_clk, 
        h_cnt => x,
        v_cnt => y,
        vga_r => vga_r,
        vga_g => vga_g,
        vga_b => vga_b,
        vga_hs => vga_hs,
        vga_vs => vga_vs,
        render_enable => r_ena,
        fetch_complete => fetch_complete
    );

    tm : tile_manager
    generic map (
        MAP_NUM_TILES_X => MAP_NUM_TILES_X,
        MAP_NUM_TILES_Y => MAP_NUM_TILES_Y,
        TILE_NUM_BITS => TILE_NUM_BITS
    )
    port map (
        clock => clk,
        we => w_ena,
        x_r => x_r,
        y_r => y_r, 
        x_w => x_w,
        y_w => y_w,
        in_tile => tile_in,
        out_tile => tile_out
    );
       
    boot : booting
    generic map (
        DATA_ID_NUM_BITS => DATA_ID_NUM_BITS,
        PACKET_IN_NUM_BITS => PACKET_IN_NUM_BITS,
        MAPS_NUM => MAPS_NUM,
        MAP_NUM_TILES_X => MAP_NUM_TILES_X,
        MAP_NUM_TILES_Y => MAP_NUM_TILES_Y,
        TILE_NUM_BITS => TILE_NUM_BITS,
        SPRITES_NUM => SPRITES_NUM,
        SPRITE_NUM_PIXELS => SPRITE_NUM_PIXELS,
        PIXEL_NUM_BITS => PIXEL_NUM_BITS
    )
    port map (
        clock => clk,
        mode => sw(3),
        ch_rx0 => sw(2),
        ch_rx1 => sw(1),
        ch_tx0 => ch_out0,
        ch_tx1 => ch_out1,
        tmp_rand => tmprand,
        final_rand => random,
        Xmap => x_w,
        Ymap => y_w,
        tile_out => tile_in,
        pixel_out => pixel,
        fetching_map => w_ena,
        fetching_sprites => fetching_sprites,
        fetch_complete => fetch_complete,
        fetch_start => fetch_start,
        data_type => data_type,
        data_id => data_id,
        packet_in => packet_in,
        fetching => fetching,
        led0 => led0,
        led1 => led1,
        led2 => led2,
        led3 => led3
    );
   
--    conn : connInterface
--    port map (
--        sys_clk => clk,
--        clk_rx => clk_in,
--        channel_rx_up => ch_in_up,
--        channel_rx_down => ch_in_down,
--        opt_rx => opt_in,
--        butts_rx => butts_out, 
--        special_rx => sp_out,
--        clk_tx => clk_out,
--        channel_tx_up => ch_out_up, 
--        channel_tx_down => ch_out_down,
--        opt_tx => opt_out,
--        butts_tx => butts_in,
--        special_tx => sp_in
--    );
  
    testing : process(clk)
    variable cnt : INTEGER range 0 to 60;
    begin
        if sw(1)='1' or sw(2)='1' then --other board is on
            if cnt<30 then --simulates a delay in communication
                cnt:=cnt+1;
            else
                --final rand will be determined by the product between the number obtained on one board and the other
                --here suppose the other is 1 (likely would go this way)
                random <= tmprand * 1;
            end if;
        end if;
    end process;
end Behavioral;