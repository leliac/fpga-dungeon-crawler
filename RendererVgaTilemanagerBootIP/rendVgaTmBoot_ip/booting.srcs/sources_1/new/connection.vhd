----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/04/2017 05:23:31 PM
-- Design Name: 
-- Module Name: connInterface - Behavioral
-- Project Name: 
-- Target Devices: 
-- Description: 
-- This module uses 4 physical pins as input and 4 as output: 
-- one is for the clock and the others are for the buttons bus
-- (double) and a 4 bit bus to send additional information
-- The bits are sent in series at each rising edge of the
-- clock attached, while receiving they are saved in a vector
-- all computations of these data (only regarding inputs) are
-- to be done on board. Regarding button bus the rx/tx is done
-- by 2 pins, splitting the bus in 2 halves
-- When the channel is idle sets the pins as high impedance 'Z'
-- The buses are transmitted reversed (damned downto reference)
-- but on receiving they are flipped again
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;
-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
entity connInterface is
    Port ( sys_clk : in STD_LOGIC; --will be transmitted
           clk_rx : in STD_LOGIC; --from other board to read the signal
           channel_rx_up : in STD_LOGIC; --listening pin (7 downto 4)
           channel_rx_down : in STD_LOGIC; --listening pin (3 downto 0)
           opt_rx : in STD_LOGIC; --optional pin
           butts_rx : out STD_LOGIC_VECTOR (7 downto 0); --received
           special_rx : out STD_LOGIC_VECTOR (3 downto 0); --additional info
           clk_tx : out STD_LOGIC; --from this board to the other
           channel_tx_up : out STD_LOGIC; --transmitting pin (7 downto 4)
           channel_tx_down : out STD_LOGIC; --transmitting pin (3 downto 0)
           opt_tx : out STD_LOGIC; --optional pin
           butts_tx : in STD_LOGIC_VECTOR (7 downto 0); --to send
           special_tx : in STD_LOGIC_VECTOR (3 downto 0)); --additional info
end connInterface;
architecture Behavioral of connInterface is
signal en_tx : STD_LOGIC := '0';
signal en_rx : STD_LOGIC := '0';
--signal butts : STD_LOGIC_VECTOR (7 downto 0);
--signal opt : STD_LOGIC_VECTOR (3 downto 0);  
begin
  enable_tx : process(butts_tx)
  begin
    if butts_tx="00000000" or butts_tx(7)='Z' then --nothing to transmit
      en_tx <= '0';
      clk_tx <= 'Z'; --set channel as idle
      channel_tx_up <= 'Z';
      channel_tx_down <= 'Z';
      opt_tx <= 'Z';
    else
      en_tx <= '1';
    end if;
  end process;
  transmitting : process(sys_clk,en_tx)
  variable cnt : NATURAL := 4; --start only when needed
  variable butts : STD_LOGIC_VECTOR (7 downto 0);
  variable opt : STD_LOGIC_VECTOR (3 downto 0);
  begin
    
    if rising_edge(en_tx) then
      cnt := 0; --restart
      butts := butts_tx; --save it here
      opt := special_tx; --to avoid changes during transmission
    end if;
    
    -- use 'and' if button bus stays active for the entire transmission
    if en_tx='1' and cnt<4 then --transmission active and not ended
      
      if sys_clk'event then
      clk_tx <= sys_clk; end if;
    
      if rising_edge(sys_clk) then
        channel_tx_up <= butts(cnt+4); --send correct bit
        channel_tx_down <= butts(cnt); --send correct bit
        opt_tx <= opt(cnt);
        cnt := cnt + 1;
      end if;
    elsif cnt=4 and rising_edge(sys_clk) then
      clk_tx <= 'Z'; --set channel as idle
      channel_tx_up <= 'Z';
      channel_tx_down <= 'Z';
      opt_tx <= 'Z';
      cnt := cnt + 1;
    end if;
  end process;
  
  enable_rx : process(clk_rx)
  begin
  if clk_rx='Z' then -- nothing to listen to
    en_rx <= '0';
    butts_rx <= (others=>'Z'); --receiving nothing
    special_rx <= (others=>'Z');
  else
    en_rx <= '1';
  end if;
  end process;
  
  receiving : process(clk_rx,en_rx)
  variable cnt : NATURAL := 0;
  variable butts : STD_LOGIC_VECTOR (7 downto 0);
  variable opt : STD_LOGIC_VECTOR (3 downto 0);
  begin
  if rising_edge(en_rx) then
    cnt := 0; --restart
  end if;
  if en_rx='1' and cnt<4 then --active connection
    -- from 'Z' to '1' is no more a rising edge!  
    if clk_rx='1' then 
      butts(cnt+4) := channel_rx_up;
      butts(cnt) := channel_rx_down;
      opt(cnt) := opt_rx;
      cnt := cnt + 1;
    end if;
  end if;
  
  if cnt=4 then
    if en_rx='1' then
      butts_rx <= butts; --send it for a single clock cycle
      special_rx <= opt; 
    else
      butts_rx <= (others=>'Z'); --remove this to change behaviour
      special_rx <= (others=>'Z');    
    end if;
  end if;
  
  end process;
end Behavioral;