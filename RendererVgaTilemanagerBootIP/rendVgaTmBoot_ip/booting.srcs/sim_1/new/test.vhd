----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/04/2017 06:27:50 PM
-- Design Name: 
-- Module Name: test - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity test is
end test;

architecture Behavioral of test is

component connInterface is
    Port ( sys_clk : in STD_LOGIC; --will be transmitted
       clk_rx : in STD_LOGIC; --from other board to read the signal
       channel_rx_up : in STD_LOGIC; --listening pin (7 downto 4)
       channel_rx_down : in STD_LOGIC; --listening pin (3 downto 0)
       opt_rx : in STD_LOGIC; --optional pin
       butts_rx : out STD_LOGIC_VECTOR (7 downto 0); --received
       special_rx : out STD_LOGIC_VECTOR (3 downto 0); --additional info
       clk_tx : out STD_LOGIC; --from this board to the other
       channel_tx_up : out STD_LOGIC; --transmitting pin (7 downto 4)
       channel_tx_down : out STD_LOGIC; --transmitting pin (3 downto 0)
       opt_tx : out STD_LOGIC; --optional pin
       butts_tx : in STD_LOGIC_VECTOR (7 downto 0); --to send
       special_tx : in STD_LOGIC_VECTOR (3 downto 0)); --additional info
end component;

component booting is
    Port ( clock : in STD_LOGIC;
           mode : in STD_LOGIC;
           final_rand : in INTEGER range 0 to 80;
           ch_rx0 : in STD_LOGIC;
           ch_rx1 : in STD_LOGIC;
           ch_tx0 : out STD_LOGIC;
           ch_tx1 : out STD_LOGIC;
           Xmap : out INTEGER range 0 to 80;
           Ymap : out INTEGER range 0 to 60;
           write_enable : out STD_LOGIC;
           tile_out : out STD_LOGIC_VECTOR (7 downto 0);
           tmp_rand : out INTEGER range 0 to 80);
end component;

signal clk : STD_LOGIC := '0';
signal clk_in,clk_out,ch_in_up,ch_in_down,ch_out_up,ch_out_down, opt_in, opt_out: STD_LOGIC := 'Z';
signal ch_in0,ch_in1,ch_out0,ch_out1 : STD_LOGIC := 'Z'; --may consider using specific channel
--signal ja0,ja1,jb0,jb1 : STD_LOGIC := 'Z'; --actual ports
signal butts_in,butts_out : STD_LOGIC_VECTOR (7 downto 0) := (others=>'Z');
signal sp_in,sp_out : STD_LOGIC_VECTOR (3 downto 0) := (others=>'Z');
signal random, tmprand : INTEGER := 0;

begin

  conn : connInterface port map(sys_clk=>clk, clk_rx=>clk_in, channel_rx_up=>ch_in_up,
        channel_rx_down=>ch_in_down, opt_rx=>opt_in, butts_rx=>butts_out, special_rx=>sp_out,
        clk_tx=>clk_out, channel_tx_up=>ch_out_up, channel_tx_down=>ch_out_down, opt_tx=>opt_out,
        butts_tx=>butts_in, special_tx=>sp_in);
        
  boot : booting port map(clock=>clk, mode=>'1', final_rand=>random, ch_rx0=>ch_in0,
         ch_rx1=>ch_in1, ch_tx0=>ch_out0, ch_tx1=>ch_out1, tmp_rand=>tmprand);
       
  testing : process
  constant period : TIME := 1 ns;
  begin
  
  clk <= '1';
  wait for period;
  
  clk <= '0';
  wait for period;
  
  clk <= '1';
  wait for period;
  
  clk <= '0';
  wait for period;
  
  clk <= '1';
  ch_in0 <= '1'; --start on the other side
  --clk_in <= '1';
  wait for period;
  
  clk <= '0';
  wait for period;
  
  clk <= '1';
  wait for period;
  
  clk <= '0';
  wait for period;
  
  clk <= '1';
  wait for period;
    
  clk <= '0';
  wait for period;
  
  clk <= '1';
  wait for period;
  
  clk <= '0';
  wait for period;
  
  clk <= '1';
  wait for period;
  
  clk <= '0';
  wait for period;
  
  clk <= '1';
  --butts_out<="00001011"; --other computed
  wait for period;
  
  clk <= '0';
  wait for period;
  
  clk <= '1';
  wait for period;
  
  clk <= '0';
  wait for period;
  
  wait;
  
  end process;
  
  sending_const: process(clk) --would be the scheduler
  variable first : BOOLEAN := true;
  begin
    if first then
--      ch_in0<=ja0; ch_in1<=ja1; --initially used by boot
--      jb0<=ch_out0; jb1<=ch_out1;    
      if tmprand/=0 then --they are ready
        butts_in <= std_logic_vector(to_unsigned(tmprand, butts_in'length));
      elsif butts_out(0)/='Z' and butts_out(0)/='U' then --received
        random <= (to_integer(unsigned(butts_in)) * tmprand) mod 80 + 1; --to avoid 0
        first := false; --has computed the constant, can start filling the map
      end if;
    else
--      ch_in_up<=ja0; ch_in_down<=ja1; --then by the connection
--      jb0<=ch_out_up; jb1<=ch_out_down;
    end if;
  end process;

end Behavioral;
