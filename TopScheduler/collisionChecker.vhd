--------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/22/2017 07:59:37 PM
-- Design Name: 
-- Module Name: collisionChecker - collisionChecker
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
--------------------------------------------------------------------------------
-- 
-- Direction Bus (4 bits):
-- right - top - left - bottom
--
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;
-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
entity collisionChecker is
    Port ( clock : in STD_LOGIC;
           enable : in STD_LOGIC;
           heroX : in INTEGER range 0 to 80;
           heroY : in INTEGER range 0 to 60;
           x_in : in INTEGER range 0 to 80;
           y_in : in INTEGER range 0 to 60;
           dir : in STD_LOGIC_VECTOR (3 downto 0);
           x_out : out INTEGER range 0 to 80; -- sent to
           y_out : out INTEGER range 0 to 60; -- tilemap
           tile : in STD_LOGIC_VECTOR (18 downto 0);
           level_up : out STD_LOGIC := '0';
           check : out STD_LOGIC := '0'; --1 if free
           ready : out STD_LOGIC := '1'); --1 if the result can be used
end collisionChecker;
architecture Behavioral of collisionChecker is
-- Screen resolution
constant maxX : INTEGER := 80;
constant maxY : INTEGER := 60;
constant FREE_TILE : STD_LOGIC_VECTOR (18 downto 0) := (others=>'0');
constant PORTAL : STD_LOGIC_VECTOR (7 downto 0) := "01100000";

begin

  process (clock)
  variable chk,stable,wait_for_tm : STD_LOGIC := '0';
  variable stage : INTEGER range 0 to 3 := 0;
  variable x,x_1,x_2,x_3,x_4: INTEGER range -2 to 82;
  variable y,y_1,y_2,y_3,y_4: INTEGER range -2 to 62;
  variable dirtmp : STD_LOGIC_VECTOR (3 downto 0);
  variable diag : STD_LOGIC; --1 if diagonal move
  begin
    
    if enable='0' then --to initialize
      stable:='0'; --computing
      chk:='0';
      stage:=0; wait_for_tm:='0';
      check<='0'; --next one needs to compute it
      level_up<='0';
  
      case dir is
        when "0000" => --idle
          chk := '1';
          stable := '1'; --found it
        when "0001" => --bottom
          x_1 := x_in;
          y_1 := y_in + 2;
          x_2 := x_in + 1;
          y_2 := y_in + 2;
          diag := '0';
        when "0010" => -- left
          x_1 := x_in - 1;
          y_1 := y_in;
          x_2 := x_in - 1;
          y_2 := y_in + 1;
          diag := '0';
        when "0011" => --bottom left
          x_1 := x_in - 1;
          y_1 := y_in + 1;
          x_2 := x_in - 1;
          y_2 := y_in + 2;
          x_3 := x_in;
          y_3 := y_in + 2;
          diag := '1';
        when "0100" => -- top
          x_1 := x_in;
          y_1 := y_in - 1;
          x_2 := x_in + 1;
          y_2 := y_in - 1;
          diag := '0';
        when "1100" => --top right
          x_1 := x_in + 1;
          y_1 := y_in - 1;
          x_2 := x_in + 2;
          y_2 := y_in - 1;
          x_3 := x_in + 2;
          y_3 := y_in;
          diag := '1';
        when "1000" => --right
          x_1 := x_in + 2;
          y_1 := y_in;
          x_2 := x_in + 2;
          y_2 := y_in + 1;
          diag := '0';
        when "1001" => --bottom right
          x_1 := x_in + 2;
          y_1 := y_in + 1;
          x_2 := x_in + 2;
          y_2 := y_in + 2;
          x_3 := x_in + 1;
          y_3 := y_in + 2;
          diag := '1';
        when "0110" => --top left
          x_1 := x_in;
          y_1 := y_in - 1;
          x_2 := x_in - 1;
          y_2 := y_in - 1;
          x_3 := x_in - 1;
          y_3 := y_in;
          diag := '1';
        when "1111" => --used to check curr position (finder)
          x_1 := x_in;
          y_1 := y_in;
          x_2 := x_in + 1;
          y_2 := y_in;
          x_3 := x_in;
          y_3 := y_in + 1;
          x_4 := x_in + 1;
          y_4 := y_in + 1;
--          if abs(x_in-heroX)<7 and abs(y_in-heroY)<7 then --too close
--            chk:='0'; --cannot place mob
--            stable:='1';
--          end if;
        when others => null;
      end case;
    
    elsif rising_edge(clock) then      
      if stable='0' then --on pending operation
        
        if wait_for_tm='1' then --still need to check
          if tile=FREE_TILE or--received tile is free
          (tile(18 downto 11)=PORTAL and x_in=heroX and y_in=heroY) then --only the hero can enter the portal
            if tile(18 downto 11)=PORTAL then
              level_up<='1'; --change level while overstepping the portal
--              stable:='1';
            end if;
            if stage=0 then --check second in any case
              stage:=1;
            elsif stage=1 then
              if diag='1' or dir="1111" then
                stage:=2; --need to check third
              else
                chk:='1'; --it's enough
                stable := '1'; --found it
              end if;
            elsif stage=2 then
              if dir="1111" then
                stage:=3; --the only case
              else
                chk:='1'; --it's enough
                stable := '1'; --found it
              end if;
            else --stage=3
              chk:='1'; --it's enough
              stable := '1'; --found it
            end if;
          else --occupied cell
            chk := '0';
            stable := '1'; --found it
          end if;
          wait_for_tm := '0'; --reinit
        end if;
        
        if stage=0 then --compute current cell
          x:=x_1; y:=y_1;
        elsif stage=1 then
          x:=x_2; y:=y_2;
        elsif stage=2 then
          x:=x_3; y:=y_3;
        elsif stage=3 then
          x:=x_4; y:=y_4;
        end if;
          
        if x>=0 and x<maxX and y>=0 and y<maxY then --current cell is inside the screen
          x_out <= x;
          y_out <= y;
          wait_for_tm := '1'; --check result on next rising edge
        else
          chk := '0';
          stable := '1'; --found it
        end if;
        check <= chk; --send out
        ready <= stable;
      end if;
    end if;
  end process;
end Behavioral;