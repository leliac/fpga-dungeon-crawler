----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/09/2017 10:47:09 AM
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity top is
    Port ( clk : in STD_LOGIC;
           btn : in STD_LOGIC_VECTOR (3 downto 0); --test
           jd_p : in STD_LOGIC_VECTOR (3 downto 0); --dir 
           jd_n : in STD_LOGIC_VECTOR (3 downto 0); --others
           sw : in STD_LOGIC_VECTOR (2 downto 0); --mode
           led : out STD_LOGIC_VECTOR (3 downto 0);
           jb_p : in STD_LOGIC_VECTOR (3 downto 0); --used by connection
           jb_n : in STD_LOGIC_VECTOR (3 downto 0); --partially used by boot
           jc_p : out STD_LOGIC_VECTOR (3 downto 0);
           jc_n : out STD_LOGIC_VECTOR (3 downto 0);
           vga_r : out STD_LOGIC_VECTOR (4 downto 0);
           vga_g : out STD_LOGIC_VECTOR (5 downto 0);
           vga_b : out STD_LOGIC_VECTOR (4 downto 0);
           vga_hs : out STD_LOGIC;
           vga_vs : out STD_LOGIC);
end top;

architecture Behavioral of top is

constant TILE_WIDTH : INTEGER := 19;

component tilemanager is
    Port ( clock : in STD_LOGIC;
           we : in STD_LOGIC;
           x_r : in NATURAL range 0 to 80;
           y_r : in NATURAL range 0 to 60;
           x_w : in NATURAL range 0 to 80;
           y_w : in NATURAL range 0 to 60;
           in_tile : in STD_LOGIC_VECTOR(TILE_WIDTH-1 downto 0);
           out_tile : out STD_LOGIC_VECTOR(TILE_WIDTH-1 downto 0));
end component;

component renderer_v2 is
    Port ( addr_X : out INTEGER range 0 to 81;
          addr_Y : out INTEGER range 0 to 63;
          pixel_bus : out STD_LOGIC_VECTOR(15 downto 0);
          this_X : in INTEGER range 0 to 81; --player position
          this_Y : in INTEGER range 0 to 63;
          mode : in STD_LOGIC; --whether to print the finder
          canput : in STD_LOGIC; --to set the color of the finder
          screen_X : in INTEGER range 0 to 900; --calculated inside the vga module
          screen_Y : in INTEGER range 0 to 530;
          tile_id : in STD_LOGIC_VECTOR(TILE_WIDTH-1 downto 0); --size subject to change
          clk: in STD_LOGIC;
          level : in INTEGER range 0 to 31; --to change terrain
          output_enable : in STD_LOGIC;
          pixel_clk: in STD_LOGIC);
end component;

component vga_connector is
    Port ( pixel_bus : in STD_LOGIC_VECTOR (15 downto 0);
           clock : in STD_LOGIC;
           vga_r : out STD_LOGIC_VECTOR (4 downto 0);
           vga_g : out STD_LOGIC_VECTOR (5 downto 0);
           vga_b : out STD_LOGIC_VECTOR (4 downto 0);
           vga_hs : out STD_LOGIC;
           vga_vs : out STD_LOGIC;
           h_cnt : out INTEGER range 0 to 900; --can be used by other modules
           v_cnt : out INTEGER range 0 to 530;
           render_enable: out STD_LOGIC;
           fetch_complete : in STD_LOGIC);
end component;

component clk_gen is
    Port( clk_in : in STD_LOGIC;
          clk_out : out STD_LOGIC);
--          sys_clk_out : out STD_LOGIC);
end component;

component entityController is
    Port ( clock : in STD_LOGIC;
           enable : in STD_LOGIC; --used to track frames
           enable_mob : in STD_LOGIC;
           enable_pl : in STD_LOGIC;
           player_pos : in STD_LOGIC; --flag to use find coord
           
           command_pl : in std_logic_vector (2 downto 0); --input from player
           dir_pl : in STD_LOGIC_VECTOR (3 downto 0) := (others=>'0');
           findX : in INTEGER range 0 to 80;
           findY : in INTEGER range 0 to 60;
           lifepoints : in INTEGER range 0 to 7 := 0; --to create or damage
           
           score_mob : out INTEGER range 0 to 999999;
           score_run : out INTEGER range 0 to 999999;
           random : in INTEGER range 0 to 127;
           mob0 : out INTEGER range 0 to 127;
           mob1 : out INTEGER range 0 to 127;           
           
           isFree : in STD_LOGIC; --received from collisionChecker
           ready : in STD_LOGIC;
           dirToTest :out STD_LOGIC_VECTOR (3 downto 0) := (others=>'0');
           c_enable : out STD_LOGIC := '0';
           testX : out INTEGER range 0 to 80; --to pass to collisionChecker
           testY : out INTEGER range 0 to 60;
           numMobs : out INTEGER range 0 to 63;
           anim_enable : out STD_LOGIC := '0'; --to inform a command is arriving
           id_out : out STD_LOGIC_VECTOR (7 downto 0);
           x_out : out INTEGER range 0 to 80;
           y_out : out INTEGER range 0 to 60;
           command_anim : out STD_LOGIC_VECTOR (3 downto 0); --to animator
           move : out STD_LOGIC);
end component;

component collisionChecker is
    Port ( clock : in STD_LOGIC;
           enable : in STD_LOGIC;
           heroX : in INTEGER range 0 to 80;
           heroY : in INTEGER range 0 to 60;
           x_in : in INTEGER range 0 to 80;
           y_in : in INTEGER range 0 to 60;
           dir : in STD_LOGIC_VECTOR (3 downto 0);
           x_out : out INTEGER range 0 to 80; -- sent to
           y_out : out INTEGER range 0 to 60; -- tilemap
           tile : in STD_LOGIC_VECTOR (18 downto 0);
           level_up : out STD_LOGIC := '0';
           check : out STD_LOGIC := '0'; --1 if free
           ready : out STD_LOGIC := '1'); --1 if the result can be used
end component;

component buttProc is
    Port ( buttons : in STD_LOGIC_VECTOR (7 downto 0); --from board
           clock : in STD_LOGIC; --may be used as enabler (not system clock)
           enable : in STD_LOGIC;
           enable_rev : in STD_LOGIC;
           butt_out : out STD_LOGIC_VECTOR (7 downto 0); --send to other board 
           butt_in : in STD_LOGIC_VECTOR (7 downto 0); --from other board
           mode : in STD_LOGIC;
           isFree : in STD_LOGIC;
           ready : in STD_LOGIC;
           dirToTest : out STD_LOGIC_VECTOR (3 downto 0) := (others=>'0');
           coll_ena : out STD_LOGIC := '0';
           direction : out STD_LOGIC_VECTOR (3 downto 0) := (others=>'0');
           dir_rev : out STD_LOGIC_VECTOR (3 downto 0) := (others=>'0');
           canput : out STD_LOGIC := '0'; --to color the target of the mob-spawner
           command : out STD_LOGIC_VECTOR (2 downto 0) := (others=>'0');
           damage : out INTEGER range 0 to 7 := 0);
end component;

component hud_manager is
  Port (
        tclk:in std_logic; 
        xp:in natural range 0 to 81; --x runner
        yp:in natural range 0 to 61; --y runner
        lifep:in natural range 0 to 7; --life runner
        manap: in natural range 0 to 11; --mana runner
        score1: in natural range 0 to 999999; --score runner
        score2:in natural range 0 to 999999; --score mobspawner
        bx:in natural range 0 to 81; --x boundary spawner
        by:in natural range 0 to 61; --y boundary spawner
        we:out std_logic; --we to tilemap
        enable:in std_logic; --from render
        outadrr : out STD_LOGIC_VECTOR(9 downto 0); --output tile to revise size
        XW:out natural range 0 to 81; --which xtile to output
        YW:out natural range 0 to 61; --which ytile to output
        countmob:in natural range 0 to 100; --mobs alive
        counttraps:in natural range 0 to 100; --traps on
        level: in natural range 0 to 99; -- n of level
        pause: in std_logic; -- game paused?
        addrtrap:in natural range 0 to 127;
        addrmob1:in natural range 0 to 127;
        addrmob2:in natural range 0 to 127;    
        switch: in std_logic);
end component;

component Animator is
  Port (   clk: in STD_LOGIC;
           enable: in STD_LOGIC;
           move: in STD_LOGIC;
           spriteID: in STD_LOGIC_VECTOR(7 downto 0);
           coordX_in: in INTEGER range 0 to 80;
           coordY_in: in INTEGER range 0 to 60;
           command_in: in STD_LOGIC_VECTOR(3 downto 0);
           enW: in STD_LOGIC;
           tile: out STD_LOGIC_VECTOR(18 downto 0);
           coordX_out: out INTEGER range 0 to 80;
           coordY_out: out INTEGER range 0 to 60 );
end component;

component connInterface is
    Port ( sys_clk : in STD_LOGIC; --will be transmitted
           start : in STD_LOGIC; --trigger from the top module
           ena_rx : in STD_LOGIC := '0'; --other side is transmitting
           channel_rx_up : in STD_LOGIC := '0'; --listening pin (7 downto 4)
           channel_rx_down : in STD_LOGIC := '0'; --listening pin (3 downto 0)
           butts_rx : out STD_LOGIC_VECTOR (7 downto 0) := (others=>'0'); --received
           ena_tx : out STD_LOGIC := '0'; --to inform other board
           channel_tx_up : out STD_LOGIC := '0'; --transmitting pin (7 downto 4)
           channel_tx_down : out STD_LOGIC := '0'; --transmitting pin (3 downto 0)
           butts_tx : in STD_LOGIC_VECTOR (7 downto 0) := (others=>'0')); --to send
end component;

--component booting is
--generic (
--    DATA_ID_NUM_BITS : NATURAL := 8;
--    PACKET_IN_NUM_BITS : NATURAL := 32;
--    MAPS_NUM : NATURAL := 128;
--    MAP_NUM_TILES_X : NATURAL := 80;
--    MAP_NUM_TILES_Y : NATURAL := 60;
--    TILE_NUM_BITS : NATURAL := 19;
--    SPRITES_NUM : NATURAL := 128;
--    SPRITE_NUM_PIXELS : NATURAL := 16*16;
--    PIXEL_NUM_BITS : NATURAL := 6
--);
--port (
--    clock : in STD_LOGIC;
--    mode : in STD_LOGIC;
--    ch_rx0 : in STD_LOGIC;
--    ch_rx1 : in STD_LOGIC;
--    ch_tx0 : out STD_LOGIC;
--    ch_tx1 : out STD_LOGIC;
--    tmp_rand : out INTEGER range 0 to MAPS_NUM-1;
--    final_rand : in INTEGER range 0 to MAPS_NUM-1;
--    Xmap : out INTEGER range 0 to MAP_NUM_TILES_X-1;
--    Ymap : out INTEGER range 0 to MAP_NUM_TILES_Y-1;
--    tile_out : out STD_LOGIC_VECTOR (TILE_NUM_BITS-1 downto 0);
--    pixel_out : out STD_LOGIC_VECTOR (PIXEL_NUM_BITS-1 downto 0);
--    fetching_map : out STD_LOGIC;
--    fetching_sprites : out STD_LOGIC;
--    fetch_complete : out STD_LOGIC;
    
--    --AXI Data Fetcher
--    fetch_start : out STD_LOGIC; --set to 1 to start transfer
--    data_type : out STD_LOGIC; --0 for map, 1 for all sprites
--    data_id : out STD_LOGIC_VECTOR (DATA_ID_NUM_BITS-1 downto 0); --map index
--    packet_in : in STD_LOGIC_VECTOR (PACKET_IN_NUM_BITS-1 downto 0); --32-bit tile/pixel (discard useless bits)
--    fetching : in STD_LOGIC; --is 1 while packets are being tranfered
    
--    led0 : out STD_LOGIC;
--    led1 : out STD_LOGIC;
--    led2 : out STD_LOGIC;
--    led3 : out STD_LOGIC
--);
--end component;


signal pbus : STD_LOGIC_VECTOR (15 downto 0) := (others => '0');
signal h_clk,c_clk,clk5,r_ena,addr_ch,new_tile : STD_LOGIC := '0';
signal w_ena,we_anim,we_boot,we_hud : STD_LOGIC := '0';
signal buttr_ena,f_ena,h_ena : STD_LOGIC := '0';
signal en_tx,en_rx,conn_ena : STD_LOGIC := '0';
signal butt_ena,ai_ena,c_enaP,c_enaM,c_ena : STD_LOGIC := '0';
signal ena_l,ena_pl,p_pos,mv,anim_ena,cmd_ena : STD_LOGIC := '0';
signal check,ready,canput,pause,lev_up : STD_LOGIC := '0';

signal tile_in,tile_out,tilein_a,tilein_b : STD_LOGIC_VECTOR (TILE_WIDTH-1 downto 0) := (others=>'0');
signal tilein_h : STD_LOGIC_VECTOR (9 downto 0) := (others=>'0');

signal screenX : INTEGER range 0 to 900 := 0; --screen coordinates
signal screenY : INTEGER range 0 to 530 := 0; --screen coordinates

signal x_w,x_r,x_w_anim,x_w_boot,x_w_hud,bndX : INTEGER range 0 to 80 := 0;
signal y_w,y_r,y_w_anim,y_w_boot,y_w_hud,bndY : INTEGER range 0 to 60 := 0;
signal x_in,x_out,addrX,x_anim,findX,mtX,chX : INTEGER range 0 to 80 := 23;
signal y_in,y_out,addrY,y_anim,findY,mtY,chY : INTEGER range 0 to 60 := 15;
signal tmpdir,testDir,dir,tmpdirP,dir_pl : STD_LOGIC_VECTOR (3 downto 0);
signal btt,bttR : STD_LOGIC_VECTOR (3 downto 0);
signal sign_rx,sign_tx,butts_tx : STD_LOGIC_VECTOR (7 downto 0) := (others=>'0');
signal cmd_pl : STD_LOGIC_VECTOR (2 downto 0) := (others=>'0');
signal cmd_anim : STD_LOGIC_VECTOR (3 downto 0) := (others=>'0');
signal lp_in,lp_out : INTEGER range 0 to 7 := 5;
signal lev : INTEGER range 0 to 31 := 1;
signal mob0,mob1 : INTEGER range 0 to 127 := 0;
signal id_anim : STD_LOGIC_VECTOR (7 downto 0) := (others=>'0');
signal random, tmprand : INTEGER range 0 to 255 := 67;
signal mobs_alive : INTEGER range 0 to 63 := 0;
signal score_r,score_m: INTEGER range 0 to 999999 := 0; --runner, mob spawner

signal fetching,fetch_complete,data_type,fetch_start : STD_LOGIC := '0';
signal packet_in : STD_LOGIC_VECTOR (31 downto 0) := (others=>'0');
signal data_id : STD_LOGIC_VECTOR (7 downto 0) := (others=>'0');

begin

  rend : renderer_v2 port map(--boundary_X=>bnd_x, boundary_Y=>bnd_y, 
         tile_id=>tile_out, level=>lev,
         this_x=>bndX, this_Y=>bndY, mode=>sw(0), canput=>canput,
         clk=>h_clk,screen_X=>screenX, screen_Y=>screenY, pixel_clk=>clk5, 
         output_enable=>r_ena, pixel_bus=>pbus, addr_X=>addrX, addr_Y=>addrY);

  ps_clock : clk_gen port map(clk_in=>clk, clk_out=>clk5);--, sys_clk_out=>open);

  output : vga_connector port map(pixel_bus=>pbus, clock=>clk5, h_cnt=>screenX, v_cnt=>screenY,
          vga_r=>vga_r, vga_g=>vga_g, vga_b=>vga_b, vga_hs=>vga_hs, vga_vs=>vga_vs,
          render_enable=>r_ena, fetch_complete=>fetch_complete);

  tm : tilemanager port map(clock=>h_clk, we=>w_ena, x_r=>x_r, y_r=>y_r, 
       x_w=>x_w, y_w=>y_w, in_tile=>tile_in, out_tile=>tile_out);
       
  ec : entityController port map(clock=>h_clk, enable=>ena_l, enable_mob=>ai_ena,
       player_pos=>p_pos, enable_pl=>ena_pl, command_pl=>cmd_pl, lifepoints=>lp_in, 
       dir_pl=>dir_pl, findX=>findX, findY=>findY, isFree=>check, ready=>ready,
       dirToTest=>tmpdir, c_enable=>c_enaM, testX=>mtX, testY=>mtY, move=>mv, 
       command_anim=>cmd_anim, id_out=>id_anim, x_out=>x_anim, y_out=>y_anim,
       anim_enable=>cmd_ena, numMobs=>mobs_alive, random=>random, 
       mob0=>mob0, mob1=>mob1, score_mob=>score_m, score_run=>score_r);


  collision : collisionChecker port map(clock=>h_clk, enable=>c_ena, level_up=>lev_up,
              dir=>testDir, x_in=>x_in, y_in=>y_in, x_out=>x_out, y_out=>y_out, 
              tile=> tile_out, check=>check, ready=>ready, heroX=>chX, heroY=>chY);
         
  bp : buttProc port map(buttons(7 downto 4)=>jd_n, buttons(3 downto 0)=>jd_p, 
       clock=>h_clk, enable=>butt_ena, enable_rev=>buttr_ena, mode=>sw(0), 
       isFree=>check, ready=>ready, dirToTest=>tmpdirP, coll_ena=>c_enaP, 
       direction=>btt, dir_rev=>bttR, canput=>canput, command=>cmd_pl, damage=>lp_in,
       butt_in=>sign_rx, butt_out=>butts_tx);
--       butt_in(3 downto 0)=>btn, butt_in(7 downto 4)=>"0000", butt_out=>butts_tx);

  anim : Animator port map(clk=>h_clk, enable=>cmd_ena, move=>mv,
      spriteID=>id_anim, coordX_in=>x_anim, coordY_in=>y_anim, command_in=>cmd_anim,
      enW=>anim_ena, tile=>tilein_a, coordX_out=>x_w_anim, coordY_out=>y_w_anim);
      
  hud : hud_manager port map(tclk=>h_clk, xp=>chX, yp=>chY, lifep=>lp_out, manap=>6,
        score1=>score_r, score2=>score_m, bx=>findX, by=>findY, we=>we_hud, enable=>h_ena,
        outadrr=>tilein_h, XW=>x_w_hud, YW=>y_w_hud, countmob=>mobs_alive, counttraps=>2,
        level=>lev, pause=>pause, addrtrap=>49, addrmob1=>mob0, addrmob2=>mob1, switch=>sw(0));

      
  conn : connInterface port map(sys_clk=>h_clk, start=>conn_ena,
       channel_rx_up=>jb_p(1), channel_rx_down=>jb_p(2), butts_rx=>sign_rx,
       channel_tx_up=>jc_p(1), channel_tx_down=>jc_p(2), butts_tx=>sign_tx,
       ena_rx=>jb_p(0), ena_tx=>jc_p(0));

--  boot : booting port map (clock=>h_clk, mode=>sw(0), ch_rx0=>jb_n(0), ch_rx1=>jb_n(2), 
--        ch_tx0=>jc_n(0), ch_tx1=>jc_n(2), tmp_rand=>tmprand, final_rand=>random,
--        Xmap=>x_w_boot, Ymap=>y_w_boot, tile_out=>tilein_b, fetching_map=>we_boot,
--        fetch_complete=>fetch_complete, fetch_start=>fetch_start, data_type=>data_type,
--        data_id=>data_id, packet_in=>packet_in, fetching=>fetching); --, pixel_out=>pixel, fetching_sprites=>f_sprites);
  
  half_clk : process(clk)
  variable cnt : INTEGER range 0 to 63 := 0;
  begin --would be computed by ps 
    if rising_edge(clk) then
      h_clk <= not h_clk;
      if cnt=0 then
        c_clk <= not c_clk;
      end if;
      cnt:=(cnt+1) mod 10;
    end if;
  end process;

  scheduling : process(h_clk)
  variable cnt : NATURAL := 0;
--  variable LIMIT : INTEGER range 0 to 4801 := 0;
  begin
    if falling_edge(h_clk) then
--      if fetch_start='1' then
--        if data_type='1' then --sprites
--          LIMIT:=34048;
--        else --map
--          LIMIT:=4800;
--        end if;
--        if cnt=0 then
--          packet_in(31 downto 19) <= (others=>'0'); --at first send the hero
--          packet_in(18 downto 0) <= "0000111000000000000";
--          fetching <= '1'; --sending
--        elsif cnt<LIMIT then
--          packet_in <= (others=>'0');
--          fetching <= '1'; --sending
--        else
--          fetching <= '0'; --stop
--        end if;
--        cnt:=cnt+1; --increment while sending
--      else
--        cnt:=0; --stop otherwise
--      end if;
    
      if cnt=3 then
        p_pos<='1'; --put the player
      elsif cnt=5 then
        p_pos<='0';
        we_boot<='1';
        x_w_boot<=findX; y_w_boot<=findY;
        tilein_b<="0000111000000000000";
      elsif cnt=6 then
        we_boot<='1';
        x_w_boot<=findX+1; y_w_boot<=findY;
        tilein_b<="0000111100000000000"; 
      elsif cnt=7 then
        we_boot<='1';
        x_w_boot<=findX; y_w_boot<=findY+1;
        tilein_b<="0001000000000000000"; 
      elsif cnt=8 then
        we_boot<='1';
        x_w_boot<=findX+1; y_w_boot<=findY+1;
        tilein_b<="0001000100000000000";
      elsif cnt>8 and cnt<40 then
        we_boot <= '1';
        if cnt=18 then
          tilein_b <= "0000010000000000000"; --mountain
        else
          tilein_b <= "0000010100000000000"; --wall
        end if;
        x_w_boot <= cnt; y_w_boot <= 20;
      elsif cnt=40 then
        we_boot<='1';
        x_w_boot<=70; y_w_boot<=40;
        tilein_b<="0110000000000000000"; --portal
      elsif cnt=41 then
        we_boot<='1';
        x_w_boot<=71; y_w_boot<=40;
        tilein_b<="0110000001000000000"; --hflip
      elsif cnt=42 then
        we_boot<='1';
        x_w_boot<=70; y_w_boot<=41;
        tilein_b<="0110000010000000000"; --vflip
      elsif cnt=43 then
        we_boot<='1';
        x_w_boot<=71; y_w_boot<=41;
        tilein_b<="0110000011000000000"; --both
      elsif cnt=70 then
        we_boot<='0';
      end if;
    end if;
    if rising_edge(h_clk) then
      case screenY is
        when 10 =>
--          if screenX=10 then
          butt_ena <= '1'; --check for pressing
--          end if;
--        when 18 => 
--          if screenX=10 then
--            ai_ena <= '1'; --computes the direction
--          end if;
        when 20 =>
          conn_ena <= '1'; --send out
        when 21 =>
          buttr_ena <= '1'; --analyzes what has been received
        when 485 =>
          h_ena <= '1';
        when 487 =>
          f_ena <= '1';
        when 490 to 520 =>
          anim_ena<='1';
          if screenY>492 then
            ena_l <= '1'; --enable logics          
            if (screenX mod 80)=5 then
              ai_ena <= '1'; --computes a mob
            elsif (screenX mod 80)=70 then
              ai_ena <= '0'; --leave time to reload
            end if;
          end if;
        when others => --disable all
          butt_ena <= '0';
          buttr_ena <= '0';
          ai_ena <= '0';
          anim_ena<='0';
          f_ena <= '0';
          ena_l <= '0';
          h_ena <= '0';
          conn_ena <= '0';
      end case;
      if pause='1' then
        ai_ena <= '0';
        anim_ena<='0';
        f_ena <= '0';
        ena_l <= '0';
      end if;
      if tmprand/=0 and random=0 then --ready to send data
        conn_ena<='1';
      else
        conn_ena<='0'; --data has been sent or not yet ready
      end if;
      if lev_up='1' then
        cnt:=0; --reinit there
      elsif cnt<1000 then
        cnt := cnt + 1;
      end if;
    end if;
  end process;
  
  process(butt_ena)
  begin
    if cmd_anim="0001" then
      led(2)<='1';
    elsif cmd_anim(2 downto 0)="110" then
      led(1)<='1';
    elsif cmd_anim(2 downto 0)="011" then
      led(0)<='1';
    end if;
    if btn(1)='1' then
      led<="0000";
    end if;
  end process;
  
  tm_r_mux : process(addrX,addrY,x_out,y_out)
  begin
    if butt_ena='1' or ai_ena='1' or buttR_ena='1' then --when needed to check for collisions
      x_r <= x_out; y_r <= y_out; --used by collision
    else
      x_r <= addrX; y_r <= addrY; --used by renderer
    end if;
  end process;
  
  tm_w_mux : process(x_w_anim,y_w_anim,x_w_boot,y_w_boot,x_w_hud,y_w_hud,tilein_a,tilein_b,tilein_h,we_boot,we_hud)
  begin
    if anim_ena='1' then --anim time
      --w_ena<=we_anim;
      w_ena<='1';
      x_w<=x_w_anim;
      y_w<=y_w_anim;
      tile_in<=tilein_a;
    elsif h_ena='1' then --hud time
      w_ena<=we_hud;
      x_w<=x_w_hud;
      y_w<=y_w_hud;
      tile_in(18 downto 9)<=tilein_h;
      tile_in(8 downto 0)<=(others=>'0');
    else --while booting
      w_ena<=we_boot;
      x_w<=x_w_boot;
      y_w<=y_w_boot;
      tile_in<=tilein_b;
    end if;
  end process;
  
  
  bnd_mux : process(x_anim,y_anim,findX,findY)
  begin
    if sw(0)='1' then --define correct boundary
      bndX<=x_anim; bndY<=y_anim;
    else
      bndX<=findX; bndY<=findY;
    end if;
  end process;
  
  coll_mux : process(x_anim,y_anim,findX,findY,tmpdirP,mtX,mtY,tmpdir,c_enaP,c_enaM)
  variable cx,cy,rx,ry : INTEGER range 0 to 80;
  begin
    if sw(0)='1' then --set the correct reference
      cx:=x_anim; cy:=y_anim;
      rx:=findX; ry:=findY;
    else
      cx:=findX; cy:=findY;
      rx:=x_anim; ry:=y_anim;
    end if;
    if butt_ena='1' then --player values
      x_in<=cX; y_in<=cY;
      testDir<=tmpdirP; --check these values
      c_ena<=c_enaP;
    elsif buttR_ena='1' then --use reverse value
      x_in<=rX; y_in<=rY;
      testDir<=tmpdirP; --check these values
      c_ena<=c_enaP;
    else --mob values
      x_in<=mtX; y_in<=mtY;
      testDir<=tmpdir; --check these values
      c_ena<=c_enaM;
    end if;
  end process;
  
--  conn_mux : process(butts_tx,tmprand)
--  begin
--    if fetch_complete='1' then
--      sign_tx<=butts_tx; --send out buttons 
--    elsif random=0 then --for initialization only
--      sign_tx<=std_logic_vector(to_unsigned(tmprand,8)); --send rand number
--    end if;
--  end process;
  
  pl_dir : process(btt,bttR)
  begin
    if sw(0)='1' then --use correct vector
      dir_pl <= btt;
    else
      dir_pl <= bttR;
    end if;
  end process;
  
  pl_enable : process(butt_ena,buttR_ena)
  begin
    ena_pl <= butt_ena or buttR_ena; --when at least one
  end process;
  
  f_anim : process(f_ena)
  variable step : INTEGER range 0 to 15 := 0;
  variable dir_f : STD_LOGIC_VECTOR (3 downto 0);
  begin
    if rising_edge(f_ena) then
      if sw(0)='0' then --use correct vector
        dir_f := btt;
      else
        dir_f := bttR;
      end if;
      
--      foldX<=findX; foldY<=findY;
      if step=0 then --do it only once
        if dir_f(3)='1' then --right
          findX <= (findX + 1) mod 80;
        elsif dir_f(1)='1' then --left
          findX <= (findX - 1) mod 80;
        end if;
        if dir_f(2)='1' then --up
          findY <= (findY - 1) mod 60;
        elsif dir_f(0)='1' then --down
          findY <= (findY + 1) mod 60;
        end if;
      end if;
      step:=(step+1) mod 5;
    end if;
  end process;
  
  hud_decoder : process(h_ena)
  begin
    if rising_edge(h_ena) then --safe trigger
      lp_out<=to_integer(unsigned(cmd_anim));
      chX<=x_anim; chY<=y_anim; --save once they're ready
  --    mob0<=to_integer(unsigned(id_anim(3 downto 0)));
  --    mob1<=to_integer(unsigned(id_anim(7 downto 4)));
    end if;
  end process;
  
  pause_manager : process (h_ena)
  variable pause_ext,pause_int,check_t,check_r : STD_LOGIC := '0'; --external value
  variable cnt : INTEGER range 0 to 31 := 0;
  begin
    if rising_edge(h_ena) then --safe trigger
      if butts_tx(6)='1' and pause_ext='0' and cnt=0 then
        pause_int := not pause_int;
      end if;
      if sign_rx(6)='1' and pause_int='0' and cnt=0 then
        pause_ext := not pause_ext;
      end if;
      cnt:=(cnt+1) mod 16; --avoid pause disabling
      pause <= pause_int or pause_ext;
    end if;
    
--    if pause='0' then
--      pause_int:=butts_tx(6); --set from internal value
--      check_t:=butts_tx(6);
--    else
--      if butts_tx(6)='0' and check_t='1' then --to avoid long pressing
--        check_t:='0';
--      elsif  pause_ext='0' then --not pause on other side
--        pause_int:='0';
--      end if;
--    end if;
--    if pause='0' then
--      pause_ext:=butts_rx(6);
--      check_r:=butts_rx(6);
--    else
--      if butts_rx(6)='0' and check='1' then
--        check_r:='0';
--      elsif  pause_int='0' then
--        pause_ext:='0';
--      end if;
--    end if;
  end process;
  
  lev_man : process(lev_up)
  begin
    if rising_edge(lev_up) then
      lev <= lev+1; --increase level
    end if;
  end process;
  
--  rand_man : process(h_clk)
--  begin
  
--  end process;

end Behavioral;
