----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/28/2017 09:18:35 AM
-- Design Name: 
-- Module Name: buttReceiver - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- Used to filter the input and rule it following the clock 
-- Each pressed button is saved into a buffer and then this 
-- buffer is sent out, the clock can be an enabler
----------------------------------------------------------------------------------
-- moving directions (3 downto 0)
--   2
-- 1   3
--   0
-- right - up -  left - down
----------------------------------------------------------------------------------
-- other actions (5 downto 4)
-- normal attack - special attack
-- put trap - put mob
-- 6 is pause 7 is not connected (right now)
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity buttProc is
    Port ( buttons : in STD_LOGIC_VECTOR (7 downto 0); --from board
           clock : in STD_LOGIC; --may be used as enabler (not system clock)
           enable : in STD_LOGIC;
           enable_rev : in STD_LOGIC;
           butt_out : out STD_LOGIC_VECTOR (7 downto 0); --send to other board 
           butt_in : in STD_LOGIC_VECTOR (7 downto 0); --from other board
           mode : in STD_LOGIC;
           isFree : in STD_LOGIC;
           ready : in STD_LOGIC;
           dirToTest : out STD_LOGIC_VECTOR (3 downto 0) := (others=>'0');
           coll_ena : out STD_LOGIC := '0';
           direction : out STD_LOGIC_VECTOR (3 downto 0) := (others=>'0');
           dir_rev : out STD_LOGIC_VECTOR (3 downto 0) := (others=>'0');
           canput : out STD_LOGIC := '0'; --to color the target of the mob-spawner
           command : out STD_LOGIC_VECTOR (2 downto 0) := (others=>'0');
           damage : out INTEGER range 0 to 7 := 0);
end buttProc;

architecture Behavioral of buttProc is
signal butt_pure : STD_LOGIC_VECTOR (7 downto 0) := (others=>'0');
begin
  receive : process(enable,enable_rev)
  variable buff : STD_LOGIC_VECTOR (7 downto 0) := (others=>'0');
  begin
  
    for i in 0 to 7 loop
      if buttons(i)='1' then
        --buff(i):='1';
        buff(i):='0'; --pmod sends reversed
      else
        --buff(i):='0';
        buff(i):='1';
      end if;
    end loop;
    
    if buff(3)='1' and buff(1)='1' then
      buff(3):='0'; buff(1):='0'; --kill contrasting signals
    end if;
    
    if buff(2)='1' and buff(0)='1' then
      buff(2):='0'; buff(0):='0';
    end if;
    
    if enable='1' then
      butt_pure <= buff; --send it to processing
      butt_out <= buff; --send to other board
    elsif enable_rev='1' then
      butt_pure <= butt_in; --use received buttons
--      butt_pure <= not butt_in; --debug
    end if;
  end process;

  computing : process (clock)
  variable cnt : INTEGER range 0 to 3 := 0;
  variable cmd : STD_LOGIC_VECTOR (2 downto 0) := (others=>'0');
  variable currdir : STD_LOGIC_VECTOR (3 downto 0);
  begin
    
    if enable='0' and enable_rev='0' then --reinit
      cnt := 0;
      cmd := "000";
    elsif falling_edge(clock) then --one of the 2 is on
      if cnt=1 then --leave time to load data
        coll_ena <= '1'; --start checking
      end if;
      --enable uses direct input, reverse uses received input, thus reversing the mode
      -- 1 is runner 0 is mob_spawner
      cmd:=(others=>'0'); --initial
      if (mode and enable)='1' or (not mode and enable_rev)='1' then
        dirToTest <= butt_pure(3 downto 0); --check this
        if ready='1' and cnt=2 then --value has been computed
          if isFree='1' then
            if butt_pure(3 downto 0)="0000" then
              cmd := "000"; --do not move anyway
            else
              cmd := "010"; --can move
            end if;
          else
            cmd := "000";
          end if;
          if butt_pure(5)='1' then --normal attack (default)
            cmd := "011";
            damage <= 1;
          elsif butt_pure(4)='1' and isFree='1' then --long attack 
            cmd := "110"; --shot
          end if;
          currdir := butt_pure(3 downto 0); --always send
          coll_ena <= '0'; --no more need to use it
        elsif cnt<2 then 
          currdir:=(others=>'1'); --pending
        end if;
      else --if (mode and enable)='1' or (not mode and enable_rev)='1' then
        dirToTest <= "1111"; --check current position
        if ready='1' and cnt=2 then --value has been computed
          if isFree='1' then
            canput <= '1'; --target will be green
            if butt_pure(5)='1' then --put trap (default)
              cmd := "001";
            elsif butt_pure(4)='1' then --put mob 
              cmd := "001"; --needs to discriminate
            else
              cmd := "000"; --do nothing
            end if;
          else
            canput <= '0'; --target will be red
          end if;
          coll_ena <= '0'; --no more need to use it
          currdir := butt_pure(3 downto 0); --can move
        elsif cnt<2 then
          currdir:=(others=>'1'); --pending
        end if;
      end if;
      if enable='1' then --local part
        direction <= currdir; --send out
      else --received part
        dir_rev <= currdir;
      end if;
      if cnt<3 and ready='1' then --keeping trace of 3 steps
        cnt := cnt + 1;
      end if;
    end if;
    command<=cmd; --send out the strongest command
  end process;

end Behavioral;