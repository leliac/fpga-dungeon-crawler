#include <stdio.h>
#include <stdint.h>
#include <math.h>

#define SPRITES_FILE_NAME "sprites.bin"
#define SPRITES_NUM 128
#define SPRITE_NUM_PIXELS 16*16
#define PIXEL_NUM_BITS 6

int main() {
	int i, j;
	FILE* f;
	uint32_t pixel;

	f = fopen(SPRITES_FILE_NAME, "wb");
	for(i = 0; i < SPRITES_NUM; ++i) {
		for(j = 0; j < SPRITE_NUM_PIXELS; ++j) {
			pixel = 3*pow(4,i%3);
			fwrite(&pixel, sizeof(pixel), 1, f);
		}
	}
	fclose(f);

	f = fopen(SPRITES_FILE_NAME, "rb");
	for(i = 0; i < SPRITES_NUM; ++i) {
		for(j = 0; j < SPRITE_NUM_PIXELS; ++j) {
			fread(&pixel, sizeof(pixel), 1, f);
			printf("sprite %3d, pixel %3d -> %2d\n", i, j, pixel);
		}
	}
	fclose(f);

	return 0;
}
