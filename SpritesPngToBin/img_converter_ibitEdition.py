import png
import sys
import os

#png only
#install python 2.7
#install pypng (pip install pypng, or download it and follow instructions)
with open("final_result.txt", "w") as f:
	f.write('(')
	
for arg in os.listdir(os.getcwd()):
	img=png.Reader(arg)
	pixel_array=list(img.asRGBA()[2])
	newarray=[]
	for line in pixel_array:
		temp_arr=line
		new_line=[]
		for pixel in temp_arr:
			new_line.append(int(pixel/64))
		newarray.append(new_line)
	
    bin_arr=[]
    for line in newarray:
		bin_line=[]
		for i in range(0, 63, 4):
			print str(line[i])+" "+str(line[i+1])+" "+str(line[i+2])
			if(line[i] == 3 or line[i+1]==3 or line[i+2]==3):
				print "I'm a one"
				bin_line.append('1')
			else:
				bin_line.append('0')
		bin_arr.append(bin_line)
	
    with open("final_result.txt", 'a') as f:
		for line in bin_arr:
			for val in line:
				f.write('"'+val+'"'+',')
			f.write('\n')