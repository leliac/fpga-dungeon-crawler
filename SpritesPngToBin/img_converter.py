import png
import sys
import os

cwd=os.listdir(os.getcwd())
if "final_result.txt" in cwd:
    cwd.remove("final_result.txt")
cwd.sort(key= lambda x: int(x.strip(".png")))

with open("final_result.txt", "w") as f:
    f.write('(')
j=0
for arg in cwd:
    if ".png" not in arg:
        continue
    img=png.Reader(arg)
    pixel_array=list(img.asRGBA()[2])
    newarray=[]
    for line in pixel_array:
        temp_arr=line
        new_line=[]
        for pixel in temp_arr:
            new_line.append(int(pixel/64))
        newarray.append(new_line)
    
    bin_arr=[]
    for line in newarray:
        bin_line=[]
        for i in range(0, 63, 4):
            red='{0:02b}'.format(line[i])
            green='{0:02b}'.format(line[i+1])
            blue='{0:02b}'.format(line[i+2])
            bin_line.append(green+blue+red)
        bin_arr.append(bin_line)
    
    #with open(arg.strip(".png")+".txt", 'w') as f:
    with open("final_result.txt", 'a') as f:
        for line in bin_arr:
            for val in line:
                f.write(str(j)+'=>"'+val+'",')
                j+
            f.write('\n')
    