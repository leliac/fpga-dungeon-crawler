#include <stdio.h>
#include <stdint.h>

#define SPRITES_TXT_FILE_NAME "sprites.txt"
#define SPRITES_BIN_FILE_NAME "sprites.bin"
#define SPRITES_NUM 128
#define SPRITE_NUM_PIXELS 16*16

int main() {
	int i, j;
	FILE *fTxt, *fBin;
	uint32_t pixel;

	fTxt = fopen(SPRITES_TXT_FILE_NAME, "r");
	fBin = fopen(SPRITES_BIN_FILE_NAME, "wb");
	for(i = 0; i < SPRITES_NUM*SPRITE_NUM_PIXELS; ++i) {
		fscanf(fTxt, "%d", &pixel);
		fwrite(&pixel, sizeof(pixel), 1, fBin);
	}
	fclose(fTxt);
	fclose(fBin);

	fBin = fopen(SPRITES_BIN_FILE_NAME, "rb");
	for(i = 0; i < SPRITES_NUM; ++i) {
		for(j = 0; j < SPRITE_NUM_PIXELS; ++j) {
			fread(&pixel, sizeof(pixel), 1, fBin);
			printf("sprite %3d, pixel %3d -> %2d\n", i, j, pixel);
		}
	}
	fclose(fBin);

	return 0;
}
