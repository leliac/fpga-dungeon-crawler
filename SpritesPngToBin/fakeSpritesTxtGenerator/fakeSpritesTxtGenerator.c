#include <stdio.h>
#include <stdint.h>

#define SPRITES_TXT_FILE_NAME "sprites.txt"
#define SPRITES_NUM 128
#define SPRITE_NUM_PIXELS 16*16
#define PIXEL_MAX_VALUE 64

int main() {
	int i, j;
	FILE* f;
	int pixel;

	f = fopen(SPRITES_TXT_FILE_NAME, "w");
	for(i = 0; i < SPRITES_NUM; ++i) {
		for(j = 0; j < SPRITE_NUM_PIXELS; ++j) {
			pixel = (i+j)%PIXEL_MAX_VALUE;
			fprintf(f, "%d ", pixel);
		}
	}
	fclose(f);

	f = fopen(SPRITES_TXT_FILE_NAME, "r");
	for(i = 0; i < SPRITES_NUM; ++i) {
		for(j = 0; j < SPRITE_NUM_PIXELS; ++j) {
			fscanf(f, "%d", &pixel);
			printf("sprite %3d, pixel %3d -> %2d\n", i, j, pixel);
		}
	}
	fclose(f);

	return 0;
}
