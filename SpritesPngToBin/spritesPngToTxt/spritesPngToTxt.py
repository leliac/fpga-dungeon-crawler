import png
import sys
import os

cwd=os.listdir(os.getcwd())
for val in cwd:
    if ".png" not in val:
        cwd.remove(val)
cwd.sort(key= lambda x: int(x.strip(".png")))

for arg in cwd:
    if ".png" not in arg:
        continue
    img=png.Reader(arg)
    pixel_array=list(img.asRGBA()[2])
    newarray=[]
    for line in pixel_array:
        temp_arr=line
        new_line=[]
        for pixel in temp_arr:
            new_line.append(int(pixel/64))
        newarray.append(new_line)

    bin_arr=[]
    for line in newarray:
        bin_line=[]
        for i in range(0, 63, 4):
            red='{0:02b}'.format(line[i])
            green='{0:02b}'.format(line[i+1])
            blue='{0:02b}'.format(line[i+2])
            bin_line.append(int((green+blue+red), 2))
        bin_arr.append(bin_line)

    #with open(arg.strip(".png")+".txt", 'w') as f:
    with open("sprites.txt", 'a') as f:
        for line in bin_arr:
            for val in line:
                f.write(str(val)+" ")
            f.write('\n')
