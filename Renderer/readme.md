Possibile issues with v2 design:

It requires a custom package, with this types defined:

type pixel_row_t is array (0 to 15) of std_logic_vector(5 downto 0);
type sprite_t is array (0 to 15) of pixel_row;

This model doesn't support transparencies. 
However, with the current graphic target it shouldn't be an issue.
An overhaul would be required, if that was to change.

Also, a function will be written to read the pixels, and return them one by one.
this is also needed in the package for this code to work


REGOLE PER L'UTILIZZO DEGLI OFFSET:

    tile(9 downto 8) => Offset direction: indica la direzione (inteso come retta) lungo quale si posiziona la tile
        - "00" antidiagonale (/)
        - "01" orrizontale   (-)
        - "10" verticale     (|)
        - "11" diagonale     (\)
        
    tile(7 downto 4) => Offset: indica lo scostamento in pixel
        - "00" antidiagonale:   offset indica lo scostamento verticale 'y'. Quello orizontale sarà 15-y   => y_off = offset; x_off = 15-y_off;
        - "01" orizzontale:     l'offset indica lo scostamento orizzontale                                => y_off = 0; x_off = offset;
        - "10" verticale:       l'offset indica lo scostamento verticale                                  => y_off = offset; x_off = 0;
        - "11" diagonale:       l'offset indica sia lo scostamento orizzontale, che quello verticale      => y_off = offset; x_off = offset;
    
    Nella notazione, x_off rappresenta sempre uno scostamento verso destra, mentre y_off uno scostamento verso il basso.
    
Tutti i movimenti nell'animatore sono fatti utilizzando queste 4 direzioni. Il verso lo decide l'animatore, incrementando o decrementando l'offset.