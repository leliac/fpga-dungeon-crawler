----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 20.04.2017 11:02:05
-- Design Name: 
-- Module Name: tilemap - Behavioral
-- Target Devices: Zybo
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


use IEEE.NUMERIC_STD.ALL;


----------
--boundary_X: received from the boundary writer x coord of the leftmost upper tile
--boundary_Y: received from the boundary writer y coord of the leftmost upper tile
--tile_id: value stored in the tilemap at addr_X, addr_Y
--addr_X/addr_Y: coords that are sent to the tilemap data queue to retrieve tile_id
entity tilemap is
    Port ( boundary_X : in INTEGER;
           boundary_Y : in INTEGER;
           tile_id : in STD_LOGIC_VECTOR(11 downto 0); --size subject to change
           tile_changed:in STD_LOGIC;
           addr_X : out INTEGER;
           addr_Y : out INTEGER;
           pixel_bus : out STD_LOGIC_VECTOR(15 downto 0);
           fr_clk: in STD_LOGIC;
           enable_bit : in STD_LOGIC;
           sprite_addr: out STD_LOGIC_VECTOR(7 downto 0); --goes to the module that returns the sprite data (8bit since sprite address is 8 bit) 
           sprite_data: in STD_LOGIC_VECTOR(15 downto 0));
end tilemap;

architecture Behavioral of tilemap is
--declare distributed rams (tileline, pixel line)
type tileram is array (0 to 39) of std_logic_vector(11 downto 0);
type pixelram is array (0 to 639) of std_logic_vector(5 downto 0);
signal tiles_1, tiles_2: tileram:=(others=>(others=>'0'));
signal pixels_1, pixels_2: pixelram:=(others=>(others=>'0'));



attribute ram_style:string;
attribute ram_style of tiles_1: signal is "distributed";
attribute ram_style of tiles_2: signal is "distributed";
attribute ram_style of pixels_1: signal is "block";
attribute ram_style of pixels_2: signal is "block";

constant TILES_IN_LINE: INTEGER := 40;
constant LINES_IN_FRAME: INTEGER := 27;
constant LINES_IN_HUD: INTEGER := 30-lines_in_frame;

signal out_row:STD_LOGIC_VECTOR(1 downto 0);--00 no out, 01 out1, 10 out2;
signal tile_row:STD_LOGIC_VECTOR(1 downto 0);--same as before


begin
    write_tile_rows: process
    variable hcount: INTEGER := -1;
    variable vcount : INTEGER :=0;
    variable current_tile_line, current_pixel_line: STD_LOGIC :='0';
    begin
        
        
        wait until rising_edge(fr_clk);
        if(enable_bit = '1') then
            -----------
            --read tileline, and save it to an internal register.
            -----------

            if (hcount < TILES_IN_LINE) then
               hcount:=hcount+1;
               addr_X<=hcount;
               addr_Y<=vcount;
               --delay
               wait until tile_changed'event;--should work, as long as tile_id isn't the same
               --saves to ram
               if(current_tile_line = '0') then
                    tiles_1(hcount)<=tile_id;
               else
                    tiles_2(hcount)<=tile_id;
               end if;
               
            end if;
            --change tile line
            current_tile_line := not current_tile_line; 
            if(tile_row = "00" or tile_row= "01") then
              tile_row <= "10";
            else
              tile_row <= "01";
            end if;
            
            
        end if;
            -----------
        
    end process;
    
       
    write_pixel_rows: process
    begin
        wait until (tile_row="01" or tile_row="10");
        if(tile_row="01") then
        --read from array1
        end if;
    end process;
    
    out_pixel_row : process(fr_clk)--trigger is wrong, need to readjust when I'm sure
    begin
    end process;  
    
    
end Behavioral;
