--------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/22/2017 07:59:37 PM
-- Design Name: 
-- Module Name: collisionChecker - collisionChecker
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
--------------------------------------------------------------------------------
-- 
-- Direction Bus (4 bits):
-- right - top - left - bottom
--
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;
-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
entity collisionChecker is
    Port ( clock : in STD_LOGIC;
           x_in : in NATURAL;
           y_in : in NATURAL;
           dir : in STD_LOGIC_VECTOR (3 downto 0);
           x_out : out NATURAL; -- sent to
           y_out : out NATURAL; -- tilemap
           tile : in STD_LOGIC_VECTOR (7 downto 0);
           check : out STD_LOGIC := '0'; --1 if free
           ready : out STD_LOGIC := '1'); --1 if the result can be used
end collisionChecker;
architecture Behavioral of collisionChecker is
-- Screen resolution
constant maxX : integer := 80;
constant maxY : integer := 60;

begin

  process (clock)
  variable chk,stable,wait_for_tm : STD_LOGIC := '0';
  variable stage : INTEGER range 0 to 3 := 3;
  variable x,y,x_1,y_1,x_2,y_2,x_3,y_3,xtmp,ytmp : INTEGER range 0 to 83;
  variable dirtmp : STD_LOGIC_VECTOR (3 downto 0);
  variable diag : STD_LOGIC; --1 if diagonal move
  begin
    
    if rising_edge(clock) then
      --to initialize
      if stage=3 or xtmp/=x_in or ytmp/=y_in or dirtmp/=dir then
        stable:='0'; --computing
        stage:=0; wait_for_tm:='0';
        xtmp:=x_in; ytmp:=y_in; --to intercept changes
        dirtmp:=dir;
    
        case dir is
          when "0000" => --idle
            chk := '1';
            stable := '1'; --found it
          when "0001" => --bottom
            x_1 := x_in;
            y_1 := y_in + 2;
            x_2 := x_in + 1;
            y_2 := y_in + 2;
            diag := '0';
          when "0010" => -- left
            x_1 := x_in - 1;
            y_1 := y_in;
            x_2 := x_in - 1;
            y_2 := y_in + 1;
            diag := '0';
          when "0011" => --bottom left
            x_1 := x_in - 1;
            y_1 := y_in + 1;
            x_2 := x_in - 1;
            y_2 := y_in + 2;
            x_3 := x_in;
            y_3 := y_in + 2;
            diag := '1';
          when "0100" => -- top
            x_1 := x_in;
            y_1 := y_in - 1;
            x_2 := x_in + 1;
            y_2 := y_in - 1;
            diag := '0';
          when "1100" => --top right
            x_1 := x_in + 1;
            y_1 := y_in - 1;
            x_2 := x_in + 2;
            y_2 := y_in - 1;
            x_3 := x_in + 2;
            y_3 := y_in;
            diag := '1';
          when "1000" => --right
            x_1 := x_in + 2;
            y_1 := y_in;
            x_2 := x_in + 2;
            y_2 := y_in + 1;
            diag := '0';
          when "1001" => --bottom right
            x_1 := x_in + 2;
            y_1 := y_in + 1;
            x_2 := x_in + 2;
            y_2 := y_in + 2;
            x_3 := x_in + 1;
            y_3 := y_in + 2;
            diag := '1';
          when "0110" => --top left
            x_1 := x_in;
            y_1 := y_in - 1;
            x_2 := x_in - 1;
            y_2 := y_in - 1;
            x_3 := x_in - 1;
            y_3 := y_in;
            diag := '1';
          when others => null;
        end case;
      
      end if;
      
      if stable='0' then --on pending operation
        
        if wait_for_tm='1' then --still need to check
          if tile="00000000" then --received tile is free
            if stage=0 then --check second in any case
              stage:=1;
            elsif stage=1 then
              if diag='1' then
                stage:=2; --need to check third
              else
                chk:='1'; --it's enough
                stable := '1'; --found it
              end if;
            else --stage=2
              chk:='1'; --it's enough
              stable := '1'; --found it
            end if;
          else --occupied cell
            chk := '0';
            stable := '1'; --found it
          end if;
          wait_for_tm := '0'; --reinit
        end if;
        
        if stage=0 then --compute current cell
          x:=x_1; y:=y_1;
        elsif stage=1 then
          x:=x_2; y:=y_2;
        elsif stage=2 then
          x:=x_3; y:=y_3;
        end if;
          
        if x>=0 and x<maxX and y>=0 and y<maxY then --current cell is inside the screen
          x_out <= x;
          y_out <= y;
          wait_for_tm := '1'; --check result on next rising edge
        else
          chk := '0';
          stable := '1'; --found it
        end if;
        check <= chk; --send out
        ready <= stable;
      end if;
    end if;
  end process;
end Behavioral;