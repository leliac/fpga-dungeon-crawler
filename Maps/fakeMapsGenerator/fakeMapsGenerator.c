#include <stdio.h>
#include <stdint.h>
#include <math.h>

#define MAPS_FILE_NAME "maps.bin"
#define MAPS_NUM 128
#define MAP_NUM_TILES 80*60
#define TILE_NUM_BITS 19
#define SPRITES_NUM 8

//tile = j%SPRITES_NUM*pow(2,24);

int main() {
	int i, j, k;
	FILE* f;
	uint32_t tile;

	f = fopen(MAPS_FILE_NAME, "wb");
	for(i = 0; i < MAPS_NUM; ++i) {
		for(j = 0; j < 60; ++j) {
			for(k = 0; k < 80; ++k) {
				if(j == 0 || j == 59 || k == 0 || k == 79) {
					tile = 2;
				} else if(j == 29 || j == 30 || k == 39 || k == 40) {
					tile = 4;
				} else {
					tile = 6;
				}
				fwrite(&tile, sizeof(tile), 1, f);
			}
		}
	}
	fclose(f);

	f = fopen(MAPS_FILE_NAME, "rb");
	for(i = 0; i < MAPS_NUM; ++i) {
		for(j = 0; j < MAP_NUM_TILES; ++j) {
			fread(&tile, sizeof(tile), 1, f);
			printf("map %3d, tile %4d -> sprite %3d\n", i, j, tile);
		}
	}
	fclose(f);

	return 0;
}
