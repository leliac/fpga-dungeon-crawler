----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/28/2017 09:18:35 AM
-- Design Name: 
-- Module Name: buttReceiver - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- Used to filter the input and rule it following the clock 
-- Each pressed button is saved into a buffer and then this 
-- buffer is sent out, the clock can be an enabler
----------------------------------------------------------------------------------
-- moving directions (3 downto 0)
--   2
-- 1   3
--   0
-- right - up -  left - down
----------------------------------------------------------------------------------
-- other actions (5 downto 4)
-- normal attack - special attack
-- put trap - put mob
-- 6 is pause 7 is not connected (right now)
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity buttReceiver is
    Port ( buttons : in STD_LOGIC_VECTOR (7 downto 0);
           clock : in STD_LOGIC; --may be used as enabler (not system clock)
           moves : out STD_LOGIC_VECTOR (7 downto 0) := (others=>'0'));
end buttReceiver;
architecture Behavioral of buttReceiver is
begin
  receive : process(buttons, clock)
  variable buff : STD_LOGIC_VECTOR (7 downto 0) := (others=>'0');
  begin
  
    for i in 0 to 7 loop
      if buttons(i)='1' then
      buff(i):='1'; end if;
    end loop;
    
    if buff(3)='1' and buff(1)='1' then
      buff(3):='0'; buff(1):='0'; --kill contrasting signals
    end if;
    
    if buff(2)='1' and buff(0)='1' then
      buff(2):='0'; buff(0):='0';
    end if;
    
    if rising_edge(clock) then
      moves <= buff;
      buff := (others=>'0');
    end if;
  end process;
end Behavioral;