----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/14/2017 06:09:36 PM
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top is
    Port ( sw : in STD_LOGIC_VECTOR (3 downto 0);
           btn : in STD_LOGIC_VECTOR (3 downto 0);
           clk : in STD_LOGIC;
           led : out STD_LOGIC_VECTOR (3 downto 0);
           vga_r : out STD_LOGIC_VECTOR (4 downto 0);
           vga_g : out STD_LOGIC_VECTOR (5 downto 0);
           vga_b : out STD_LOGIC_VECTOR (4 downto 0);
           vga_hs : out STD_LOGIC;
           vga_vs : out STD_LOGIC);
end top;

architecture Behavioral of top is

component vga_connector is
Port ( pixel_bus : in STD_LOGIC_VECTOR (15 downto 0);
       clock : in STD_LOGIC;
       reset : in STD_LOGIC;
       h_cnt : in NATURAL;
       v_cnt : in NATURAL;
       vga_r : out STD_LOGIC_VECTOR (4 downto 0);
       vga_g : out STD_LOGIC_VECTOR (5 downto 0);
       vga_b : out STD_LOGIC_VECTOR (4 downto 0);
       vga_hs : out STD_LOGIC;
       vga_vs : out STD_LOGIC);
end component;

component bus_gen is
    Port ( switch : in STD_LOGIC_VECTOR (3 downto 0);
           pixel_bus : out STD_LOGIC_VECTOR (15 downto 0));
end component;

component clk_wiz_0 is
  Port ( clk_in1 : in STD_LOGIC;
         reset : in STD_LOGIC;
         locked : out STD_LOGIC;
         clk_out1 : out STD_LOGIC);
end component;

component pos_counter is
    Port ( clock : in STD_LOGIC;
           reset : in STD_LOGIC;
           h_cnt : out NATURAL;
           v_cnt : out NATURAL);
end component;

component circle is
    Port ( clock : in STD_LOGIC;
           button : in STD_LOGIC_VECTOR (3 downto 0);
           x_pos : in NATURAL;
           y_pos : in NATURAL;
           bus_in : in STD_LOGIC_VECTOR (15 downto 0);
           bus_out : out STD_LOGIC_VECTOR (15 downto 0);
           reset : out STD_LOGIC);
end component;

signal pbus_i : STD_LOGIC_VECTOR (15 downto 0) := (others => '0');
signal pbus : STD_LOGIC_VECTOR (15 downto 0) := (others => '0');
signal rst : STD_LOGIC;
signal clk5 : STD_LOGIC := '0';
signal x : NATURAL;
signal y : NATURAL;

begin
    
    --live feedback of modified switches
    feedback : process(sw)
    begin
      led <= sw;
    end process;
    
    color : bus_gen port map(switch=>sw, pixel_bus=>pbus_i);
    
    position : pos_counter port map(clock=>clk5, reset=>'0', h_cnt=>x, v_cnt=>y);
    
    mv : circle port map(clock=>clk5, button=> btn, x_pos=>x, y_pos=>y,
    bus_in=>pbus_i, bus_out=>pbus, reset=>rst);
       
    vga_clock : clk_wiz_0 port map(clk_in1=>clk, reset=>'0', locked=>open, clk_out1=>clk5);
       
    output : vga_connector port map(pixel_bus=>pbus, clock=>clk5, h_cnt=>x, v_cnt=>y,
    reset=>'0', vga_r=>vga_r, vga_g=>vga_g, vga_b=>vga_b, vga_hs=>vga_hs, vga_vs=>vga_vs);

end Behavioral;
