----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/14/2017 04:18:52 PM
-- Design Name: 
-- Module Name: top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity bus_gen is
    Port ( switch : in STD_LOGIC_VECTOR (3 downto 0);
           pixel_bus : out STD_LOGIC_VECTOR (15 downto 0));
end bus_gen;

architecture Behavioral of bus_gen is
 
begin
    
    out_bus : process(switch)
    
    begin
        pixel_bus(3 downto 0) <= (others => switch(0)); -- set all to the switch value
        pixel_bus(7 downto 4) <= (others => switch(1));
        pixel_bus(11 downto 8) <= (others => switch(2));
        pixel_bus(15 downto 12) <= (others => switch(3));
    end process;

end Behavioral;
