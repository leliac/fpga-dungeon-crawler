----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/18/2017 03:15:57 PM
-- Design Name: 
-- Module Name: pos_counter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values


-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity pos_counter is
    Port ( clock : in STD_LOGIC;
           reset : in STD_LOGIC;
           h_cnt : out NATURAL;
           v_cnt : out NATURAL);
end pos_counter;

architecture Behavioral of pos_counter is

begin

  cnt : process(clock)
  variable x : NATURAL := 0; 
  variable y : NATURAL := 0;
  begin
    h_cnt <= x;
    v_cnt <= y;
    if reset='1' then
      x:=0; y:=0;
    else
      if clock'event and clock='1' then
        --counters block
        x := x+1;          
        if x = 800 then --very end of row
          x := 0;
          y := y + 1;
        end if;          
        if y = 525 then --very end of screen
          y := 0;
        end if;
      end if;
    end if;
  end process;

end Behavioral;
