----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/18/2017 03:47:01 PM
-- Design Name: 
-- Module Name: circle - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity circle is
    Port ( clock : in STD_LOGIC;
           button : in STD_LOGIC_VECTOR (3 downto 0);
           x_pos : in NATURAL;
           y_pos : in NATURAL;
           bus_in : in STD_LOGIC_VECTOR (15 downto 0);
           bus_out : out STD_LOGIC_VECTOR (15 downto 0);
           reset : out STD_LOGIC);
end circle;

architecture Behavioral of circle is
signal x : INTEGER := 22;
signal y : INTEGER := 22;
signal VSYNC : STD_LOGIC := '1';
begin

  v_clock : process(clock)
  begin
    if y_pos > 489 and y_pos < 492 then  -- send sync during the proper period
      VSYNC <= '0';  --vga_vs <= VSYNC; 
    else
      VSYNC <= '1';  --vga_vs <= VSYNC;
    end if;
  end process;

  position : process (clock)
  begin
  if clock'event and clock='1' then
    if button(0)='1' then
      --x <= x - 1; 
      reset <= '1'; 
    end if;
    if button(1)='1' then
      --y <= y + 1; 
      reset <= '1'; 
    end if;
    if button(2)='1' then
      --y <= y - 1; 
      reset <= '1'; 
    end if;
    if button(3)='1' then
      --x <= x + 1; 
      reset <= '1'; 
    end if;
    if (button(0) or button(1) or button(2) or button(3))='0' then
      reset <= '0';
    end if;
  end if;
  end process;

  is_circle : process(clock)
  
  begin 
  
    if clock'event and clock='1' then
      if x_pos>2 and y_pos>2 and x_pos<638 and y_pos<478 and  
      (x_pos-x)*(x_pos-x)+(y_pos-y)*(y_pos-y)>20*20
      then bus_out <= not bus_in;
      else bus_out <= bus_in;
      end if;
--      if x_pos=x and y_pos=y then
--        bus_out <= (others=>'0');
--      end if;
    end if;

  end process;

  movement : process(VSYNC)
  variable dx : INTEGER;
  variable dy : INTEGER;
  begin
    if VSYNC'event and VSYNC='0' then
      if x=618 then dx:=-1; end if;
      if y=458 then dy:=-1; end if;
      if x=22 then dx:=1; end if;
      if y=22 then dy:=1; end if;
      x <= x + dx; y <= y + dy; --increase on each frame
      report integer'image(x);
      report integer'image(y);
    end if;
  end process;

end Behavioral;
