#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Paragraph
DRAM (Dynamic random access memory)
\end_layout

\begin_layout Standard
A type of RAM that stores each bit of data in a separate capacitor within
 an integrated circuit.
 A capacitor can be either charged or discharged, representing the two values
 of a bit.
\end_layout

\begin_layout Paragraph
SDRAM (Synchronous dynamic random access memory)
\end_layout

\begin_layout Standard
Any DRAM where the operation of its external pin interface is coordinated
 by an externally supplied clock signal.
\end_layout

\begin_layout Standard
DRAM uses an asynchronous interface, in which control inputs have a direct
 effect on internal functions.
 SDRAM, on the other hand, has a synchronous interface, in which changes
 on the control inputs are recognised after a rising edge of the clock input.
\end_layout

\begin_layout Standard
Incoming commands can be pipelined to improve performance, with previously
 started operations completing while new commands are received.
\end_layout

\begin_layout Standard
The memory is divided into several equally sized but independent sections
 called banks, allowing to execute commands on each bank simultaneously
 and speed access in an interleaved fashion.
 This allows SDRAMs to achieve greater concurrency and higher data transfer
 rates than DRAMs.
\end_layout

\begin_layout Paragraph
DDR (Double date rate)
\end_layout

\begin_layout Standard
An operating mode of a computer bus in which data transfers occur on both
 the rising and the falling edge of the clock signal.
\end_layout

\begin_layout Paragraph
DDR3 SDRAM (Double data rate type 3 synchronous dynamic random access memory)
\end_layout

\begin_layout Itemize
A type of SDRAM (synchronous dynamic random access memory) with a high bandwidth
 (DDR) interface
\end_layout

\begin_layout Itemize
in use since 2007
\end_layout

\begin_layout Itemize
higher-speed successor to DDR and DDR2 SDRAM and predecessor to DDR4 SDRAM
\end_layout

\begin_layout Itemize
neither forward nor backward compatible with earlier types of RAM because
 of different signaling voltages, timings and other factors
\end_layout

\begin_layout Itemize
it's a DRAM interface specification.
 The actual DRAM arrays that store the data are similar to earlier types,
 with similar performance
\end_layout

\begin_layout Itemize
primary benefit over DDR2 SDRAM is the ability to transfer data at twice
 the rate (8 times the speed of its internal memory arrays) enabling higher
 bandwidth
\end_layout

\begin_layout Itemize
bandwidth is given by memoryClockRate x 4 (for bus clock multiplier) x 2
 (for data rate) x bitsPerTransfer / 8 (for number of bits per byte) bytes/s
\end_layout

\begin_layout Itemize
the DDR3 standard permits DRAM chip capacities of up to 8 Gb
\end_layout

\end_body
\end_document
