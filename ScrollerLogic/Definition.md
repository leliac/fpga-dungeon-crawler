Input:
frame clock (vsync signal)
PlayerPositionX
PlayerPositionY
tilemap

Output:
boundaryTopRight
boundaryTopLeft
boundaryBotRight
boundaryBotLeft

Internal logic:
add to position the values to keep it centered.
If position is at 0 on any axis (map limit), let player move in that direction without scrolling.

