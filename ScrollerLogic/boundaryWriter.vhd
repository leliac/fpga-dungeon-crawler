    ----------------------------------------------------------------------------------
    -- Company: 
    -- Engineer: GTKplusplus
    -- 
    -- Create Date: 20.04.2017 10:20:18
    -- Design Name: 
    -- Module Name: boundaryWriter - Behavioral
    -- Project Name: 
    -- Target Devices: Zybo
    -- 
     
     
    ----------------------------------------------------------------------------------
    
    
    library IEEE;
    use IEEE.STD_LOGIC_1164.ALL;
    use IEEE.NUMERIC_STD.ALL;
    
    entity boundaryWriter is
        Port ( fr_clk : in STD_LOGIC;
               ch_x : in INTEGER;
               ch_y : in INTEGER;
               boundary_X : out INTEGER;
               boundary_Y : out INTEGER);
    end boundaryWriter;
    
    
    architecture Behavioral of boundaryWriter is
    
    constant LeftX : natural := 20;
    constant RightX: natural := 60;
    constant TopY: natural :=15;
    constant BotY: natural :=45;
    constant centralRightX: natural :=(LeftX + RightX)/2;
    constant centralBotY: natural :=(TopY+BotY)/2;
    
    begin
        boundary_write : process(fr_clk)
            begin
            
            if(rising_edge(fr_clk)) then
                -----------------------------------------------------------
                --if the character/cursor position is such that scrolling would go out of bonds, if fixes the point of view to the boundarys.
                --the returned boundary is the top-left tile of the view.
                -----------------------------------------------------------
                --
                -----------------------------------------------------------
                --math:
                --if(ch_x/y < (width/height of view/2) then fixes
                --if(ch_x/y > (right/bottom boundary- (width/height of view/2) then fixes
                ----------------------------------------------------------
                if( ch_x < LeftX) then
                    boundary_X <= 0;
                elsif(ch_x > RightX) then
                    boundary_X <= centralRightX;
                else
                    boundary_X <= (ch_x-LeftX);
                end if;
                if( ch_y < TopY) then
                    boundary_Y <= 0;
                elsif(ch_y > BotY) then
                    boundary_Y <= centralBotY;
                else
                    boundary_Y <= (ch_y-TopY);
                end if;
            end if;
            end process;
    end Behavioral;
