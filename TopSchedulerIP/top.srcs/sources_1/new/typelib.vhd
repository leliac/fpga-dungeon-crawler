----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04.06.2017 00:20:06
-- Design Name: 
-- Module Name: typelib - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
package typelib is
    type mob is record
    x : NATURAL range 0 to 80;
    y : NATURAL range 0 to 60;
    life : NATURAL range 0 to 5;
    spriteID : STD_LOGIC_VECTOR (7 downto 0);
    bulx : NATURAL range 0 to 80;
    buly : NATURAL range 0 to 60;
    buldir: std_logic_vector (3 downto 0);
--    idle: std_logic;
end record mob;
constant EmptyEntity : mob := (x => 80,
                              y => 60,
                              life => 0,
                              spriteID => (others=>'0'),
                              bulx => 80,
                              buly => 60,
                              buldir => "0000");
--                              idle => '0');
procedure randMob(variable SEED1,SEED2:inout POSITIVE;variable X:out INTEGER);
end typelib;                              

package body typelib is
procedure randMob(variable SEED1,SEED2:inout POSITIVE;variable X:out INTEGER) is
        -- Description:
        --        See function declaration in IEEE Std 1076.2-1996
        -- Notes:
        --        a) Returns 0.0 on error
        --
        variable Z, K: INTEGER;
        variable TSEED1 : INTEGER := INTEGER'(SEED1);
        variable TSEED2 : INTEGER := INTEGER'(SEED2);
    begin
        -- Compute new seed values and pseudo-random number
        TSEED1 := (SEED1+SEED2)*97;
        TSEED2 := abs((SEED1*SEED2)-73);
        Z := 37*(abs(TSEED1-TSEED2)+(TSEED1*TSEED2))/32;
        -- Get output values
        SEED1 := POSITIVE'(TSEED1);
        SEED2 := POSITIVE'(TSEED2);
        X :=  (abs(Z) mod 8)/2;
    end randMob;
end typelib;