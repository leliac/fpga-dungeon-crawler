----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/22/2017 11:01:42 AM
-- Design Name: 
-- Module Name: tilemap - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
entity tilemanager is
    Port ( clock : in STD_LOGIC;
           we : in STD_LOGIC;
           x_r : in NATURAL range 0 to 80;
           y_r : in NATURAL range 0 to 60;
           x_w : in NATURAL range 0 to 80;
           y_w : in NATURAL range 0 to 60;
           in_tile : in STD_LOGIC_VECTOR(18 downto 0);
           out_tile : out STD_LOGIC_VECTOR(18 downto 0));
end tilemanager;
architecture Behavioral of tilemanager is
-- channels for reading and writing are separed --
-- both actions run in different instances to avoid overlapping --
-- the matrix 80x60 is linearized into an array --
-- array_index := x + 80*y -- 
constant TILE_WIDTH : INTEGER := 19;
subtype TILE is STD_LOGIC_VECTOR(TILE_WIDTH-1 downto 0);
--type TILEMAP is array(4799 downto 0) of TILE;
type TILEMAP is array(4959 downto 0) of TILE;

--function initTile(x : natural) return tilemap is
--variable data : tilemap;
--variable tile : std_logic_vector(7 downto 0) := (others=>'0');
--begin
--  for i in 0 to 4799 loop
--    data(i) := tile;
--    tile := std_logic_vector(unsigned(tile) + 1);
--  end loop;
--  return data;
--end function;

signal tm : TILEMAP := (others=>(others=>'0'));
--signal tm : TILEMAP := initTile(0);
attribute ram_style: string;
attribute ram_style of tm : signal is "block";

begin
reading : process(clock)
begin
-- read on clock 0 --
-- use the coordinates for reading and send the tile in output --
  if falling_edge(clock) then
    out_tile <= tm(x_r + y_r*80);
  end if;
end process;
writing : process(clock)
begin
-- write on clock 1 --
-- use the coordinates for writing and override the tile --
  if rising_edge(clock) and we='1' then
    tm(x_w + y_w*80) <= in_tile;
  end if;
end process;
end Behavioral;