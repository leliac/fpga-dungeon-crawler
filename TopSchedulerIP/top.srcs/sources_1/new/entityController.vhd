----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04/29/2017 12:38:46 PM
-- Design Name: 
-- Module Name: checkDir - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- computes the best direction to reach the object and outputs it to be checked
-- by the collisionChecker, while pending the direction is 1111 
-- The clever AI adjusts the direction until it can move, after some iterations
-- it stops (that means the mob cannot move in any direction)
-- The dumb one finds wether it can reach the target using the same direction and
-- stops if it can not or if the target is too far (30 steps)
-- Each time the collision checker is called it must be passed the coordinates and 
-- the direction computed here, then should wait for a result to continue
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.typelib.ALL;
use IEEE.NUMERIC_STD.ALL;

entity entityController is
    Port ( clock : in STD_LOGIC;
           enable : in STD_LOGIC; --used to track frames
           enable_mob : in STD_LOGIC;
           enable_pl : in STD_LOGIC;
           player_pos : in STD_LOGIC; --flag to use find coord
           
           command_pl : in std_logic_vector (2 downto 0); --input from player
           dir_pl : in STD_LOGIC_VECTOR (3 downto 0) := (others=>'0');
           findX : in INTEGER range 0 to 80;
           findY : in INTEGER range 0 to 60;
           lifepoints : in INTEGER range 0 to 7 := 0; --to create or damage
           
           random : in INTEGER range 0 to 127;
           mob0 : out INTEGER range 0 to 127;
           mob1 : out INTEGER range 0 to 127;
           score_mob : out INTEGER range 0 to 999999;
           score_run : out INTEGER range 0 to 999999;
--           life_pl : out INTEGER range 0 to 7 := 5;
           
           isFree : in STD_LOGIC; --received from collisionChecker
           ready : in STD_LOGIC;
           dirToTest :out STD_LOGIC_VECTOR (3 downto 0) := (others=>'0');
           c_enable : out STD_LOGIC := '0';
           testX : out INTEGER range 0 to 80; --to pass to collisionChecker
           testY : out INTEGER range 0 to 60;
           numMobs : out INTEGER range 0 to 63;
           anim_enable : out STD_LOGIC := '0'; --to inform a command is arriving
           id_out : out STD_LOGIC_VECTOR (7 downto 0); --new sprites
           x_out : out INTEGER range 0 to 80;
           y_out : out INTEGER range 0 to 60;
           command_anim : out STD_LOGIC_VECTOR (3 downto 0) := (others=>'0'); --to animator
           move : out STD_LOGIC);
end entityController;
--           mob_out: out mob;
--           mobs_alive: out natural range 0 to 100;
--           traps_on:out natural range 0 to 99;
--           --index_out: out NATURAL range 0 to 101;
--           damage: in NATURAL range 0 to 3;
--           level: in natural range 0 to 99;
--           dir: in std_logic_vector (3 downto 0));

-- moving directions
--   2
-- 1   3
--   0
-- right - up -  left - down
---------------------------------
-- mob type
-- cleverness - attack type - random
-- 100 is clever short attack
-- 000 is dumb short attack
-- 010 is dumb long attack
-- 001 is ramdomly moving short
-- 011 is ramdomly moving long
-- 110 not implemented but may be too strong this way
-- 1x1 may be avoided
---------------------------------
architecture Behavioral of entityController is
function computeDir(targetX,targetY,thisX,thisY : INTEGER)
return STD_LOGIC_VECTOR is
variable dir : STD_LOGIC_VECTOR (3 downto 0); 
begin
  if thisX<targetX then
    if thisY<targetY then
      dir := "1001"; --go down-right
    elsif thisY>targetY then
      dir := "1100"; --go up-right
    else --if thisY=targetY
      dir := "1000"; --go right
    end if;
  elsif thisX>targetX then
    if thisY<targetY then
      dir := "0011"; --go down-left
    elsif thisY>targetY then
      dir := "0110"; --go up-left
    else --if thisY=targetY
      dir := "0010"; --go left
    end if;
  else --if thisX=targetX
    if thisY<targetY then
      dir := "0001"; --go down
    elsif thisY>targetY then
      dir := "0100"; --go up
    else --if thisY=targetY
      dir := "0000"; --found
    end if;
  end if; 
  return dir;
end function;

constant NUM_MOBS : INTEGER := 10;
--constant NUM_GEN : INTEGER := 25;
--sprite IDs
constant FIRSTMOB : INTEGER := 30; --the rat
constant HERO : STD_LOGIC_VECTOR (7 downto 0) := "00001110"; --14
constant RAT : STD_LOGIC_VECTOR (7 downto 0) := "00011110"; --30
constant GOBLIN : STD_LOGIC_VECTOR (7 downto 0) := "00101110"; --46
constant SKELETON : STD_LOGIC_VECTOR (7 downto 0) := "00111110"; --62
constant GHOST : STD_LOGIC_VECTOR (7 downto 0) := "01001110"; --78
constant BULLET : STD_LOGIC_VECTOR (7 downto 0) := "01011110"; --94
constant ATTACK : STD_LOGIC_VECTOR (7 downto 0) := "10101010"; --used to state if mob can attack
--..--
signal ai_ena : STD_LOGIC := '0';
signal ind : INTEGER range 0 to NUM_MOBS+1:= 0;
signal damage_mob : NATURAL range 0 to 3;
signal dir_mob : STD_LOGIC_VECTOR (3 downto 0) := (others=>'0');
signal command_mob : STD_LOGIC_VECTOR (2 downto 0) := (others=>'0');
signal speedCnt : INTEGER range 0 to 127 := 0;

type mobarray is array(0 to NUM_MOBS) of mob;
signal ea : mobarray := (others=>EmptyEntity);
signal trapon:INTEGER range 0 to NUM_MOBS:=0;

impure function canMove(id : STD_LOGIC_VECTOR (7 downto 0)) return BOOLEAN is
begin --should set speed on multiples of 5 depending on the id
  if id=ATTACK and speedCnt=120 then --attack only once per window time
    return true;
  elsif id=GHOST and (speedCnt mod 50)=0 then --slowest
    return true;
  elsif (id=GOBLIN or id=SKELETON) and (speedCnt mod 40)=0 then --medium
    return true;
  elsif id=RAT and (speedCnt mod 30)=0 then --fastest
    return true;
  elsif id=HERO and (speedCnt mod 10)=0 then --player
    return true;
  elsif (id=BULLET) and (speedCnt mod 3)=0 then --bullets only
    return true;
  end if;
  return false;
end function;

impure function getIndex(x : in INTEGER range 0 to 80; y : in INTEGER range 0 to 60; choice : std_logic) return INTEGER is
variable out_data : INTEGER range 0 to NUM_MOBS := NUM_MOBS;
begin
    for i in 0 to NUM_MOBS loop
        if i=NUM_MOBS or --missed 
        (((x=ea(i).x or x=ea(i).x+1 or x=ea(i).x-1) and (y=ea(i).y or y=ea(i).y+1 or y=ea(i).y-1)) --close to
        and ea(i).life/=0 and choice='0') --still alive
        or (ea(i).life=0 and choice='1' and i/=0)  then --first dead
            out_data := i;
            exit;
        end if;
    end loop;
return out_data;
end function;

function freshShot(ent : mob) return BOOLEAN is
variable x,y : INTEGER range 0 to 80 := 0; 
begin
  if ent.buldir(3)='1' then --right
    x := ent.x + 3;
  elsif ent.buldir(1)='1' then --left
    x := ent.x - 2;
  else
    x := ent.x;
  end if;
  if ent.buldir(2)='1' then --up
    y := ent.y - 2;
  elsif ent.buldir(0)='1' then --down
    y := ent.y + 3;
  else
    y := ent.y;
  end if;

  if ent.bulx=x and ent.buly=y then
    return true; --shot started now
  end if;
  return false;
end function;

begin

  frm_cnt : process(enable)
  begin --counting frames to set speed
    if rising_edge(enable) then
      speedCnt <= (speedCnt+1) mod 128;
    end if;
  end process;
  
  starting : process(clock) --selecting the mob for the ia
  variable first : BOOLEAN := true;
  variable done : BOOLEAN := false;
  begin
    if rising_edge(clock) then
      if enable='0' then
        ind<=0;
        ai_ena <= '0'; --eventually stop the ai
        done:=false;
      elsif enable_mob='0' then
        first := true;
        if ind<NUM_MOBS then --only for mobs
          ai_ena<='0'; --keep for hero's bullet
        end if;
      elsif not done then
        if first then
          for i in 1 to NUM_MOBS loop --find the first to compute
            if i+ind>=NUM_MOBS then
              ind<=NUM_MOBS; --set the end
              done:=true; --no more mobs
              exit;
            end if;
            if (canMove(ea(i+ind).spriteID) or --time to move 
            (canMove(BULLET) and ea(i+ind).buldir/="0000")) --bullet only 
            and ea(i+ind).life>0 then --but still alive
              ind<=i+ind;
              exit;
            end if;
          end loop;
        else --leave the time to load the data 
          ai_ena <= '1'; --start the ai
        end if;
        first:=false;
--      elsif ind<NUM_MOBS+2 then
--        ind<=ind+1; --bullet of player just once
      else --mobs are done
--        if ind<NUM_MOBS then --only for mobs
        ai_ena<='1'; --keep for hero's bullet
--        end if;
      end if;
    end if;
  end process;

  adjust : process(clock)
  variable computing,rand,first,toSend : BOOLEAN := true;
  variable x,y,tmpX,tmpY : INTEGER range -81 to 161;
  variable thisType : STD_LOGIC_VECTOR (2 downto 0) := (others=>'0');
  variable tmpdir,bestdir : STD_LOGIC_VECTOR (3 downto 0) := (others=>'1'); --to init
  variable cnt : INTEGER range 0 to 31 := 0;
  variable currMob : mob;
  begin
    
    if ai_ena='0' then
      --update/init variables
      if ind>0 and ind<NUM_MOBS then --a real mob
        currMob := ea(ind);
        if thisType(2)='1' then --clever Ai
          x:=ea(0).x;
          y:=ea(0).y; 
          tmpX:=x; tmpY:=y;
        elsif thisType(2)='0' then --dumb Ai
          x:=currMob.x;
          y:=currMob.y;
        end if;
        if canMove(currMob.spriteID) then --not here for the bullet only
          computing := true; --needs to compute
        else
          computing := false; --no need to compute
          command_mob<="000"; --send this as default
        end if;
        rand := false;
        first := true; --track first computation of this ai
        bestdir := computeDir(ea(0).x,ea(0).y,currMob.x,currMob.y);
        tmpdir := bestdir; --first to try
        cnt := 0;
        command_mob<="000"; --empty command while disabled
        --set thisType based on currMob.spriteID
        if currMob.spriteID=GOBLIN then
          thisType:="000"; --dumb 
        elsif currMob.spriteID=RAT then
          thisType:="001"; --random
        elsif currMob.spriteID=GHOST then
          thisType:="011"; --shooting
        elsif currMob.spriteID=SKELETON then
          thisType:="100"; --clever
        end if;
      elsif ind=NUM_MOBS then
        currMob:=ea(0); --to compute bullet of hero
        computing:=false; --treated as last mob
      else
        currMob:=EmptyEntity; --can never move
      end if;
    elsif falling_edge(clock) then --only work when needed
      if not computing and currMob.buldir/="0000" and canMove(BULLET) and --try the bullet if there 
      not (canMove(currMob.spriteID) and freshShot(currMob)) then --avoid moving the first time it shot
        dirToTest<=currMob.buldir;
        testX <= currMob.bulx; testY <= currMob.buly;
        command_mob<="000"; --stop transmitting while computing the bullet
        if ready='1' then
          if first then
            c_enable<='0';
            cnt:=0; 
            first:=false;
          elsif cnt<3 then
            if cnt=2 then
              if isFree='1' then
                command_mob<="111"; --move bullet
              else
                command_mob<="110"; --hit bullet
              end if;
            end if;
            c_enable<='1';
            cnt:=cnt+1;
          else
            c_enable<='0'; --leave as idle
            command_mob<="000"; --send idle
          end if;
        end if;
      elsif (abs(currMob.x-ea(0).x)<=2 and abs(currMob.y-ea(0).y)<=2) and thisType(1)='0' and ind>0 and ind<NUM_MOBS then
        --dir_mob <= (others=>'0'); --stop while found
        dir_mob <= bestdir; --send attack direction
        c_enable <= '0'; --prevent from starting
        command_mob<="011"; --short range attack
        damage_mob<=1;
        computing:=false;
      elsif computing and ready='1' then
        command_mob<="000"; --stop transmitting while computing
        if thisType(2)='1' then --clever Ai
          if first then --first time passes and computes data
            c_enable <= '0'; --stop the collision checker
            first := false; --the signals are ready
            if isFree='0' and cnt<10 then --cannot move
              if cnt/=0 then --change from the second time on
                if (bestdir(3) xor bestdir(2) xor bestdir(1) xor bestdir(0))='0' then --diagonal movements
                  if (cnt mod 3)=1 then
                    x := tmpX; y := currMob.y; --first try to adjust horizontally
                  elsif (cnt mod 3)=2 then
                    x := currMob.x; y := tmpY; --then vertically
                  elsif (cnt mod 3)=0 then
                    if (cnt mod 2)=0 then --change diagonal direction
                      x := 2*currMob.x-ea(0).x; y := ea(0).y;
                    else
                      x := ea(0).x; y := 2*currMob.y-ea(0).y;
                    end if;
                    tmpX:=x; tmpY:=y; --save temporary target position
                  end if;
                else --pure movement
                  if (cnt mod 3)=1 then
                    if tmpY=currMob.y then --horizontal move
                      y := tmpY + 20; --try diagonally on one side
                    else --vertical move
                      x := tmpX + 20; --try diagonally on one side
                    end if;
                  elsif (cnt mod 3)=2 then
                    if tmpY=currMob.y then
                      y := tmpY - 20; --try the other
                    else
                      x := tmpX - 20; --try the other
                    end if;                
                  elsif (cnt mod 3)=0 then
                    if (cnt mod 2)=0 then --change pure direction
                      if ea(0).y=currMob.y then
                        x := currMob.x; y := ea(0).y+ 20;
                      else
                        x := ea(0).x + 20; y := currMob.y;
                      end if;
                    else
                      if ea(0).y=currMob.y then
                        x := currMob.x; y := ea(0).y - 20;
                      else
                        x := ea(0).x - 20; y := currMob.y;
                      end if;                  
                    end if;
                    tmpX:=x; tmpY:=y; --save temporary target position
                  end if;
                end if;
              end if;
              tmpdir := computeDir(x,y,currMob.x,currMob.y);
              dir_mob <= (others=>'1'); --correct direction still not found
              cnt := cnt + 1;
            else
              if cnt=10 then
                dir_mob <= (others=>'0'); --stop trying, there's no way out
                command_mob<="000"; --idle
              else
                dir_mob <= tmpdir; --now it's correct
                command_mob<="010"; --move
              end if;
              computing := false; --found result
              first:=true; --used by bullet
              c_enable <= '0'; --no more needed
            end if;
            dirToTest <= tmpdir; --send this
            testX <= currMob.x; testY <= currMob.y; --no change here          
          else --after having computed start all
            first := true; --for next time
            c_enable <= '1'; --start the collision checker
          end if;          
        elsif thisType(2)='0' then --dumb Ai
          if first then
            c_enable <= '0'; --let time to load the data
            first := false;
            if currMob.buldir/="0000" then
              rand:=true; --when bullet is there move randomly
--              tmpdir:=(others=>'0');
            end if;
            if not rand then
              if isFree='1' and cnt<15 then --still need to compute path
                if tmpdir(3)='1' then --update the position
                  x := x + 1;
                elsif tmpdir(1)='1' then
                  x := x - 1;
                end if;
                if tmpdir(2)='1' then --update the position
                  y := y - 1;
                elsif tmpdir(0)='1' then
                  y := y + 1;
                end if;
                tmpdir := computeDir(ea(0).x,ea(0).y,x,y);
                if abs(x-ea(0).x)<=2 and abs(y-ea(0).y)<=2 then
                  dir_mob <= bestdir; --can reach it, pass initial direction
                  if thisType(1)='1' then
                    if currMob.buldir="0000" then --can shot
                      command_mob<="110"; --shot
                      damage_mob<=1;
                      computing := false; --found result
                    else
                      rand:=true; --if cannot shot move randomly
                    end if;
                  else
                    command_mob<="010"; --move
                    computing := false; --found result
                  end if;
                  first:=true; --used by bullet
                else
                  dir_mob <= (others=>'1'); --still pending
                end if;
              elsif cnt/=0 then --always need to compute the first time
                tmpdir := (others=>'0');
                if thisType(0)='1' then
                  rand := true; --compute random dir
                else
                  dir_mob <= (others=>'0'); --there's an obstacle, stop
                  command_mob <= "000"; --idle
                  computing := false; --found result
                  first:=true; --used by bullet
                end if;
              end if;
            else --compute random dir
              if tmpdir="0000" then
                tmpdir := bestdir; --first try the best
                cnt:=0; --reinit there
              end if;
              x:=currMob.x; y:=currMob.y; --restore position
              if isFree='0' and cnt<5 then
                tmpdir := tmpdir(2 downto 0) & tmpdir(3); --rotate
                --cnt := cnt+1;
              else
                if cnt=5 then
                  dir_mob <= (others=>'0'); --stop trying, there's no way out
                  command_mob <= "000"; --idle
                else
                  dir_mob <= tmpdir; --now it's correct
                  command_mob<="010"; --move
                end if;
              computing := false; --found result
              first:=true; --used by bullet
              c_enable <= '0'; --no more needed
              rand := false;          
              end if;
            end if;
            cnt := cnt + 1;
            dirToTest <= tmpdir; --send this
            testX <= x; testY <= y; --pass updated
          else
            first := true; --for next time
            c_enable <= '1'; --stop the collision checker
          end if;
        end if;
      end if;
    end if;
  end process;
    
  inout_command:process (clock)
  variable pointer,pnt1,alive_mobs : INTEGER range 0 to NUM_MOBS := 0;
  variable damage: NATURAL range 0 to 7 := 0;
  variable q_pointer,s_pointer,inc_p,inc_m : NATURAL range 0 to 3 := 0;
  variable cmd : STD_LOGIC_VECTOR (2 downto 0) := (others=>'0');
  variable dir,q0,q1 : STD_LOGIC_VECTOR (3 downto 0) := (others=>'0');
  variable done,isBullet : BOOLEAN := false;
  variable x,y : INTEGER range 0 to 80;
  variable seed1,seed2 : integer;
  variable res : INTEGER range 0 to 3;
  variable score_r,score_m: INTEGER range 0 to 999999 := 0; --runner, mob spawner
  type stack is array (0 to 1) of INTEGER range 0 to 126; 
  variable mStack : stack := (others=>0); --should output for HUD
  begin
  
    if rising_edge(clock) then
      if player_pos='1' then --on level change
        for i in 1 to NUM_MOBS-1 loop
          ea(i).life<=0; --reinitialize
        end loop;
        alive_mobs:=0;
--        ea<=(others=>EmptyEntity);
        ea(0).x<=findX; --use these
        ea(0).y<=findY; --use these
        ea(0).spriteID<=HERO; --may put correct
        ea(0).life<=5;
        seed1:=random/2+13;
        seed1:=random*2-23;
        randMob(seed1,seed2,res);
        mStack(0):=16*(res mod 4)+FIRSTMOB;
        randMob(seed1,seed2,res);
        mStack(1):=16*(res mod 4)+FIRSTMOB;
        s_pointer:=0;
      elsif (enable_mob or enable_pl)='0' then --both 0
        cmd:=(others=>'0');
        done:=false;
        if ind<NUM_MOBS then --reset this only for mobs
          isBullet:=false;
        end if;
--        q0:=(others=>'0'); --reinit queue
--        q1:=(others=>'0');
        --pnt1:=0;
        x_out<=ea(0).x; --always output while disabled
        y_out<=ea(0).y;
        mob0<=mStack(s_pointer);
        mob1<=mStack((s_pointer+1)mod 2);
--        life_pl<=ea(0).life;
        command_anim<=std_logic_vector(to_unsigned(ea(0).life,4)); --hero's life
--        if ea(0).life>0 then
--          command_anim<=std_logic_vector(to_unsigned(ea(0).life/4+1,4)); --hero's life
--        else
--          command_anim<=(others=>'0'); --greater life on 5 hearts (here put 0)
--        end if;
        numMobs<=alive_mobs; --reuse this
      elsif enable_mob='1' then
        if ind>0 and ind<=NUM_MOBS then
          if ind<NUM_MOBS then --computing hero's bullet
            pointer := ind; --ind has already advanced
          else
            pointer:=0;
          end if;
          dir:=dir_mob;
          cmd:=command_mob;
--          cmd:=(others=>'0');
          damage:=damage_mob;
          if (cmd="111" or cmd="110") and not isBullet then --bullet reopens calc (only once)
            done:=false; --may be dangerous
          elsif ea(pointer).life=0 then --dead moving or attacking
            cmd:="000"; --just to be sure
          end if;
          if (cmd="011" and not canMove(ATTACK)) then
            cmd:="000"; --avoid too frequent attack
          end if;
        else
          cmd:=(others=>'0');
        end if;
      elsif enable_pl='1' then
        pointer := 0; --player
        dir:=dir_pl;
        if (cmd/="001" and canmove(HERO)) or ((cmd="001")and speedCnt=50) then
          cmd:=command_pl; --send command only when can be done
        end if;
        if command_pl="110" and ea(0).buldir/="0000" then
          cmd := (others=>'0'); --avoid shooting if cannot do so
        end if;
        damage:=lifepoints;
      end if;
      
      if cmd="110" or cmd="111" then --is a bullet
        id_out <= BULLET; --send out first
        x_out <=ea(pointer).bulx;
        y_out <=ea(pointer).buly;
      else
        id_out <= ea(pointer).spriteID; --send out first
        x_out <=ea(pointer).x;
        y_out <=ea(pointer).y;
      end if;
      
      if cmd="111" or (cmd="110" and ea(pointer).buldir/="0000") then --bullet move or hit
        dir := ea(pointer).buldir; --use this
        if cmd="110" then --hit
          inc_p:=2; --to hit outside the bullet
        else
          inc_p:=1; --used for plus
        end if;
        inc_m:=1; --used for minus
        if dir(3)='1' then --right
          x := ea(pointer).bulx + inc_p;
        elsif dir(1)='1' then --left
          x := ea(pointer).bulx - inc_m;
        else
          x := ea(pointer).bulx;
        end if;
        if dir(2)='1' then --up
          y := ea(pointer).buly - inc_m;
        elsif dir(0)='1' then --down
          y := ea(pointer).buly + inc_p;
        else
          y := ea(pointer).buly;
        end if;
      else --not the bullet
        if dir="0000" and (cmd="110" or cmd="011") then
          dir:=dir_mob(1 downto 0) & dir_mob(3 downto 2); --last used direction on attack/shot
          if dir="0000" then
            dir:="0100"; --default if still not enough
          end if;
        end if;
        if cmd/="010" then --not movement
          if cmd/="110" then --not bullet start
            inc_p:=2; --1 of distance (avoid hitting itself)
            inc_m:=2;
          else
            inc_p:=2; --immediately after
            inc_m:=1;
          end if;
        else
          inc_p:=1;
          inc_m:=1;
        end if;
        if dir(3)='1' then --right
          x := ea(pointer).x + inc_p;
        elsif dir(1)='1' then --left
          x := ea(pointer).x - inc_m;
        else
          x := ea(pointer).x;
        end if;
        if dir(2)='1' then --up
          y := ea(pointer).y - inc_m;
        elsif dir(0)='1' then --down
          y := ea(pointer).y + inc_p;
        else
          y := ea(pointer).y;
        end if;
      end if;
      if not done then --do it only once
        pnt1 := getIndex(x,y,'0'); --to not repeat calc                  
        move<='0'; --default
        q0:=(others=>'0'); --reinit queue
        q1:=(others=>'0');
        done:=true; --set as default
        q_pointer:=0; --restart queue 
        case cmd is
          when "000" =>
            done:=false; --nothing has been done
          when "001" => -- Create
            pointer:=getIndex(0,0,'1'); --get the first free
            if pointer<NUM_MOBS then --still space left
              ea(pointer).x <= findX;
              ea(pointer).y <= findY;
              ea(pointer).life <= 2; --may change for each mob
              randMob(seed1,seed2,res);
              ea(pointer).spriteID <= std_logic_vector(to_unsigned(mStack(s_pointer),8)); --pick it
              id_out <= std_logic_vector(to_unsigned(mStack(s_pointer),8)); --send out new
              x_out <=findX;
              y_out <=findY;
              mStack(s_pointer):=16*(res mod 4)+FIRSTMOB; --substitute just used
              s_pointer:=(s_pointer+1) mod 2; --advance for next time
              q0 := "0001";
              alive_mobs:=alive_mobs+1;
            end if;
          when "011" => -- attack
            --pointer := getIndex(x,y,'0');
            --pointer:=pnt1;
            if pnt1<NUM_MOBS then --no mob
              if pnt1=0 then
                score_m := score_m + 10;
              end if;
              q0 := dir(1) & "110"; --send also the flip
              if(ea(pnt1).life-damage>0) then --reduce life
                ea(pnt1).life <= ea(pnt1).life - damage; --first animation will be attack
                q1:="0011"; --damage
                --pnt1:=pointer; --will affect this one
              else --die
                q1 := "0100"; --send as second in the queue
                --pnt1:=pointer; --will affect this one
                ea(pnt1).life<=0;
                if pnt1=0 then
                --GAME OVER
                else
                  alive_mobs:=alive_mobs-1; --destroy
                  score_r := score_r + 10;
                end if;
--                ea(pointer).idle<='1'; --it's animating
              end if;
            end if;
          when "110" => --shot
            isBullet:=true; --inform the next part
            if ea(pointer).buldir="0000" then --first shot
              ea(pointer).bulx<=x; --put the bullet in first position
              ea(pointer).buly<=y;
              ea(pointer).buldir<=dir;
--              x_out <=x; --override here
--              y_out <=y;
--              q0:="0001"; --like create
              q1 := dir(1) & "011"; --like attack (after bullet)
              pnt1:=pointer;
            else --hit
              ea(pointer).buldir<="0000"; --remove bullet
              q0 := "0100"; --destroy the bullet
              --pnt1 := getIndex(x,y,'0');
              x:=ea(pointer).bulx; --use these now
              y:=ea(pointer).buly;
              if pnt1<NUM_MOBS then --no mob
                if pnt1=0 then
                  score_m := score_m + 10;
                end if;
                if(ea(pnt1).life-1>0) then --reduce life (1 for hit)
                  ea(pnt1).life <= ea(pnt1).life - 1; --first animation will be attack
                else --die
                  q1 := "0100"; --send as second in the queue
                  ea(pnt1).life<=0;
                  if pnt1=0 then
                  --GAME OVER
                  else
                    alive_mobs:=alive_mobs-1; --destroy
                    score_r := score_r + 10;
                  end if;
                end if;
              end if;
            end if;
          when "010" => --move
            ea(pointer).x <= x;
            ea(pointer).y <= y;
            move<='1';
            q0 := dir;
          when "111" => --move bullet
            isBullet:=true;
            ea(pointer).bulx <= x; --advance
            ea(pointer).buly <= y;
            move<='1';
            q0 := ea(pointer).buldir;
          when others => null;
        end case;
      end if;
--      cmd:="000"; --has been used
      if q0="0000" then
        q_pointer:=2; --do not transmit if empty
      end if;
      if q1="0000" and q_pointer>1 then --empty but other has been transmitted
        q_pointer:=3; --do not transmit if empty
      end if;
      if q_pointer=0 then
--        if isBullet then
--          id_out <= BULLET; --send this out
--          x_out <=x;
--          y_out <=y;
--        end if;
        command_anim<=q0; --send to animator
        q_pointer:=1; --advance for next time
        anim_enable<='1';
      elsif q_pointer=1 then
        anim_enable<='0'; --pause for a cycle
        q_pointer:=2; --advance for next time
      elsif q_pointer=2 then
        command_anim<=q1; --send to animator
        id_out <= ea(pnt1).spriteID; --send this out
        x_out <=ea(pnt1).x;
        y_out <=ea(pnt1).y;
        q_pointer:=3; --advance for next time
        anim_enable<='1';
      else
        anim_enable<='0'; --end of tranmission
      end if;
      score_mob<=score_m; score_run<=score_r;
    end if;
  end process;  

end Behavioral;
