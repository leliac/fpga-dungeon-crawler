----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04.05.2017 13:11:39
-- Design Name: 
-- Module Name: pixels_p - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use std.textio.all;
use ieee.std_logic_textio.all;
-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
package pixels_p is
    type sprites_arr is array(0 to 126*256) of STD_LOGIC_VECTOR(5 downto 0);
    type effects_arr is array(0 to 16*256) of STD_LOGIC_VECTOR(5 downto 0);
    type sprites_arr_bin is array(0 to 16*256) of STD_LOGIC;
    type color_arr is array(0 to 63) of STD_LOGIC_VECTOR(15 downto 0);
    impure function initSprite(x : natural) return sprites_arr;
    impure function initEffect(x : natural) return effects_arr;
    impure function initBinSprite(x : natural) return sprites_arr_bin;
    impure function initCol(x : natural) return color_arr;
    constant palette : color_arr := initCol(0);
    attribute rom_style:string;
    attribute rom_style of palette: constant is "block";
            
--    function get_pixel(x, y: integer;
--                       sprite: sprite_t) return std_logic_vector;
    function b6to16(pixel_in : STD_LOGIC_VECTOR(5 downto 0)) return STD_LOGIC_VECTOR;
    
end pixels_p;
package body pixels_p is
        
    function b6to16(pixel_in : STD_LOGIC_VECTOR(5 downto 0)) return STD_LOGIC_VECTOR is
    variable pixel_bus:STD_LOGIC_VECTOR(15 downto 0);
    begin
      pixel_bus := palette(to_integer(unsigned(pixel_in)));
      return pixel_bus;
    end function b6to16;
    
    impure function initSprite(x : natural) return sprites_arr is
    variable data : sprites_arr := (others=>(others=>'0'));
    FILE sprites_file : TEXT is in "sprites.mem";
    variable buff : LINE;
    variable ind : INTEGER range 0 to 256*126 := 0;
    begin
      while not endfile(sprites_file) and ind<256*126 loop
        readline(sprites_file,buff);
        read(buff,data(ind));
        ind := ind + 1;
      end loop;
      return data;
    end function;
    
    impure function initEffect(x : natural) return effects_arr is
    variable data : effects_arr := (others=>(others=>'0'));
    FILE effects_file : TEXT is in "effects.mem";
    variable buff : LINE;
    variable ind : INTEGER range 0 to 256*16 := 0;
    begin
      while not endfile(effects_file) and ind<256*16 loop
        readline(effects_file,buff);
        read(buff,data(ind));
        ind := ind + 1;
      end loop;
      return data;
    end function;

    
    impure function initBinSprite(x : natural) return sprites_arr_bin is
    variable data : sprites_arr_bin := (others=>'0');
    FILE sprites_file : TEXT is in "sprites_bin.mem";
    variable buff : LINE;
    variable ind : INTEGER range 0 to 256*16 := 0;
    begin
      while not endfile(sprites_file) and ind<256*16 loop
        readline(sprites_file,buff);
        read(buff,data(ind));
        ind := ind + 1;
      end loop;
      return data;
    end function;
    
    impure function initCol(x : natural) return color_arr is
    variable data : color_arr := (others=>(others=>'0'));
    FILE pal_file : TEXT is in "color.mem";
    variable buff : LINE;
    variable ind : INTEGER range 0 to 64 := 0;
    begin
      while not endfile(pal_file) and ind<64 loop
        readline(pal_file,buff);
        read(buff,data(ind));
        ind := ind + 1;
      end loop;
      return data;
    end function;
        

    
end package body;
