----------------------------------------------------------------------------------
-- University: Politecnico di Torino 
-- Authors: Gabriele Cazzato, Stefano Negri Merlo, Gabriele Monaco, Alexandru Valentin Onica, Gabriele Ponzio, Roberto Stagi
-- 
-- Create Date: 05/22/2017 03:16:37 PM
-- Design Name: TOP 
-- Module Name: Booting
-- Project Name: Computer Architecture Project 2017
-- Target Devices: Digilent ZYBO
-- Tool Versions: Vivado 2017.1
-- Description: 
-- This module is used at boot time and every time a new map needs to be loaded.
-- At boot time, the board sends a signal to the other one
-- and waits for a signal from it (these signals depend on the mode).
-- While waiting, a counter is increased, which is used as a random map selector
-- after being multiplied by the one obtained from the other board (to share the same constant).
-- This calculation is done in the top module and one of the two numbers (the lowest) will be 1.
-- The module then asks the AXI Data Fetcher to send all sprites
-- stored in DDR3 memory, which are received pixel by pixel.
-- When the transfer is over, the AXI Data Fetcher is invoked again to transfer
-- the randomly selected map, which is received tile by tile.
-- The module then enters an idle state, which is abandoned when a new map transfer is needed.
--
-- Dependencies: 
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity booting is
generic (
    DATA_ID_NUM_BITS : NATURAL := 8;
    PACKET_IN_NUM_BITS : NATURAL := 32;
    MAPS_NUM : NATURAL := 127;
    MAP_NUM_TILES_X : NATURAL := 80;
    MAP_NUM_TILES_Y : NATURAL := 60;
    TILE_NUM_BITS : NATURAL := 19;
    SPRITES_NUM : NATURAL := 127;
    SPRITE_NUM_PIXELS : NATURAL := 16*16;
    PIXEL_NUM_BITS : NATURAL := 6
);
port (
    clock : in STD_LOGIC;
    mode : in STD_LOGIC;
    ch_rx0 : in STD_LOGIC;
    ch_rx1 : in STD_LOGIC;
    ch_tx0 : out STD_LOGIC;
    ch_tx1 : out STD_LOGIC;
    tmp_rand : out INTEGER range 0 to MAPS_NUM-1;
    final_rand : in INTEGER range 0 to MAPS_NUM-1;
    Xmap : out INTEGER range 0 to MAP_NUM_TILES_X-1;
    Ymap : out INTEGER range 0 to MAP_NUM_TILES_Y-1;
    tile_out : out STD_LOGIC_VECTOR (TILE_NUM_BITS-1 downto 0);
    player_pos : out STD_LOGIC; --signal that the player position has been found
    pixel_out : out STD_LOGIC_VECTOR (PIXEL_NUM_BITS-1 downto 0);
    fetching_map : out STD_LOGIC;
    fetching_sprites : out STD_LOGIC;
    fetch_complete : out STD_LOGIC;
    
    --AXI Data Fetcher
    fetch_start : out STD_LOGIC; --set to 1 to start transfer
    data_type : out STD_LOGIC; --0 for map, 1 for all sprites
    data_id : out STD_LOGIC_VECTOR (DATA_ID_NUM_BITS-1 downto 0); --map index
    packet_in : in STD_LOGIC_VECTOR (PACKET_IN_NUM_BITS-1 downto 0); --32-bit tile/pixel (discard useless bits)
    fetching : in STD_LOGIC; --is 1 while packets are being tranfered
    
    led : out STD_LOGIC_VECTOR (3 downto 0)
);
end booting;

architecture Behavioral of booting is
constant DEBUG : STD_LOGIC := '1';

constant \MAP\ : STD_LOGIC := '0';
constant SPRITES : STD_LOGIC := '1';
constant HERO : STD_LOGIC_VECTOR (9 downto 0) := "0000111000"; --to find initial hero position in map

constant CONNECTING : NATURAL := 0;
constant CONNECTED : NATURAL := 1;
constant SPRITES_TRNSF_INIT : NATURAL := 2;
constant SPRITES_TRNSF : NATURAL := 3;
constant MAP_TRNSF_INIT : NATURAL := 4;
constant MAP_TRNSF : NATURAL := 5;
constant IDLE : NATURAL := 6;
constant SLEEP : NATURAL := 7;

--constant pattern : STD_LOGIC_VECTOR (3 downto 0) := "1110";

begin
    initialization : process(clock)
    variable cnt : NATURAL := 0;
    variable rand : NATURAL range 0 to MAPS_NUM-1 := 0;
    variable state : NATURAL range 0 to 7 := CONNECTING;
--    variable pnt_rx, pnt_tx : NATURAL range 0 to 3 := 0; --indexes in the pattern
--    variable transm : NATURAL range 0 to 20 := 0; --used to slow transmission

    begin
        if rising_edge(clock) then
            case ( state ) is
            when CONNECTING =>
                data_type <= '0';
                data_id <= (others => '0');
                fetch_start <= '0';
                fetching_sprites <= '0';
                fetching_map <= '0';
                fetch_complete <= '0';
                pixel_out <= (others => '0');
                tile_out <= (others => '0');
                tmp_rand <= 0; --still waiting
                led <= "0000";
                cnt := cnt+1; --advancing
                
                if mode = '1' then
                    ch_tx0 <= '0'; --send empty
                    ch_tx1 <= '1'; --send communication
                    if ch_rx0 = '1' and ch_rx1 = '0' then --other board is trying to communicate
                        state := CONNECTED;
                    end if;
                else
                    ch_tx0 <= '1'; --send communication
                    ch_tx1 <= '0'; --send empty
                    if ch_rx0 = '0' and ch_rx1 = '1' then --other board is trying to communicate
                        state := CONNECTED;
                    end if;      
                end if;
            when CONNECTED =>
                led(0) <= '1'; --debug: connection established
                if final_rand = 0 then --connection established
                    tmp_rand <= (cnt mod MAPS_NUM) + 1;
                    led(1) <= '1'; --debug: computed random
                else
                    state := SPRITES_TRNSF_INIT;
                    led(2) <= '1'; --debug: received as well
                    ch_tx0 <= '0'; --send empty
                    ch_tx1 <= '0'; --upon connection established                    
                end if;
            when SPRITES_TRNSF_INIT =>
                rand := final_rand - 1; --to use 0 as idle the constant is summed by 1
                cnt := 0;
                data_type <= SPRITES;
                fetch_start <= '1';
                state := SPRITES_TRNSF;
                --led(0) <= '1'; --debug: sprites transfer init
            when SPRITES_TRNSF =>
                if fetching = '1' and cnt < SPRITES_NUM*SPRITE_NUM_PIXELS then
                    fetch_start <= '0';
                    fetching_sprites <= '1';
                    pixel_out <= packet_in(PIXEL_NUM_BITS-1 downto 0); --selecting significant bits only
                    cnt := cnt+1;
                elsif cnt >= SPRITES_NUM*SPRITE_NUM_PIXELS then
                    fetching_sprites <= '0';
                    pixel_out <= (others => '0');
                    cnt := 0;
                    state := SLEEP;
                    --led(1) <= '1'; --debug: sprites transfer complete
                end if;
            when SLEEP => --TODO: try deleting state or reducing sleep time
                if (cnt >= 100) then
                    state := MAP_TRNSF_INIT;
                end if;
                cnt := cnt+1;
            when MAP_TRNSF_INIT =>
                cnt := 0;
                data_type <= \MAP\;
                data_id <= std_logic_vector(to_unsigned(rand, data_id'length));
                fetch_start <= '1'; --start map transfer
                state := MAP_TRNSF;
                --led(2) <= '1'; --debug: map transfer init
            when MAP_TRNSF =>
                if fetching = '1' and cnt < MAP_NUM_TILES_X*MAP_NUM_TILES_Y then
                    fetch_start <= '0';
                    Xmap <= cnt mod MAP_NUM_TILES_X;
                    Ymap <= cnt / MAP_NUM_TILES_X;
                    tile_out(8 downto 0) <= (others=>'0'); --blank portion
                    tile_out(18 downto 9) <= packet_in(9 downto 0); --selecting significant bits only
                    if packet_in(9 downto 0)=HERO then
                      player_pos <= '1'; --found it
                      led(3)<='1';
                    else
                      player_pos <= '0';
                    end if;
                    fetching_map <= '1';
                    cnt := cnt+1;
                elsif cnt >= MAP_NUM_TILES_X*MAP_NUM_TILES_Y then
                    fetching_map <= '0';
                    data_id <= (others => '0');
                    tile_out <= (others => '0');
                    state := IDLE;
                    --led(3) <= '1'; --debug: map transfer complete
                end if;
            when IDLE =>
                fetch_complete <= '1';
                if rand /= (final_rand-1) and rand /= 0 then
                    fetch_complete <= '0';
                    state := MAP_TRNSF_INIT; --re-init map transfer if random constant changes
                end if;
            end case;
        end if;
    end process;
end Behavioral;