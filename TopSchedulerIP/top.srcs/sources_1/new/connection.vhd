----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/04/2017 05:23:31 PM
-- Design Name: 
-- Module Name: connInterface - Behavioral
-- Project Name: 
-- Target Devices: 
-- Description: 
-- This module uses 5 physical pins as input and 5 as output:
-- [one is for the clock] and the others are for the buttons bus
-- (double), [a 4 bit bus to send additional information] and the
-- 2 enables (may use different connectors).
-- For stability reasons the clock used is always the system clock
-- (won't be a problem since both boards work at the same frequency)
-- The bits are sent in series at each rising edge of the
-- clock attached, while receiving they are saved in a vector
-- all computations of these data (only regarding inputs) are
-- to be done on board. Regarding button bus the rx/tx is done
-- by 2 pins, splitting the bus in 2 halves
-- To signal for communications the pins ena_{tx,rx} are used
-- 1 means the communication is active, 0 is off (both directions)
-- The buses are transmitted reversed (damned downto reference)
-- but on receiving they are flipped again
-- The transmission is done 3 times consecutively, at receiption
-- the signal is received 3 times and the best option is selected
-- with majority vote (bit by bit) 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;
entity connInterface is
    Port ( sys_clk : in STD_LOGIC; --will be transmitted
           start : in STD_LOGIC; --trigger from the top module
           ena_rx : in STD_LOGIC := '0'; --other side is transmitting
           channel_rx_up : in STD_LOGIC := '0'; --listening pin (7 downto 4)
           channel_rx_down : in STD_LOGIC := '0'; --listening pin (3 downto 0)
           butts_rx : out STD_LOGIC_VECTOR (7 downto 0) := (others=>'0'); --received
           ena_tx : out STD_LOGIC := '0'; --to inform other board
           channel_tx_up : out STD_LOGIC := '0'; --transmitting pin (7 downto 4)
           channel_tx_down : out STD_LOGIC := '0'; --transmitting pin (3 downto 0)
           butts_tx : in STD_LOGIC_VECTOR (7 downto 0) := (others=>'0')); --to send
end connInterface;
architecture Behavioral of connInterface is
type buses is array (0 to 2) of STD_LOGIC_VECTOR (3 downto 0);
function findCorrect(test : buses) return STD_LOGIC_VECTOR is
variable final : STD_LOGIC_VECTOR (3 downto 0);
begin
  for i in 0 to 3 loop
    final(i) := (test(0)(i) and test(1)(i)) or (test(1)(i) and test(2)(i)) or (test(0)(i) and test(2)(i));
  end loop;
  if final="1111" then --avoid returning wrong result
    final:=(others=>'0');
  end if;
  return final;
end function;
begin
  
  transmitting : process(sys_clk)
  variable cnt : INTEGER range 0 to 6:= 5;
  variable phase,sil_cnt : INTEGER range 0 to 3 := 0;
  variable silence : STD_LOGIC := '0';
  begin
    if start='0' then --init
      cnt := 0; --restart
      phase := 0;
      silence := '0';
      sil_cnt:=0;
    elsif rising_edge(sys_clk) then --can start
      if silence='0' then --speak
        if cnt<4 then --transmission not ended 
          ena_tx<='1'; --warn on the other side
          channel_tx_up <= butts_tx(cnt+4); --send correct bit
          channel_tx_down <= butts_tx(cnt); --send correct bit
          cnt := cnt + 1;
          if phase<2 and cnt=4 then --still need to send
            cnt := 0; --send it 3 times
            phase := phase + 1;
          end if;
        elsif cnt=4 then
          ena_tx<='0'; --warn on the other side
          channel_tx_up <= '0';
          channel_tx_down <= '0';
          cnt := cnt + 1;
        end if;
        silence:='1';
        sil_cnt:=0; --init silence
      else --stop talking
        --ena_tx<='0'; --should not switch this off
        channel_tx_up <= '0';
        channel_tx_down <= '0';
        if sil_cnt=3 then
          silence:='0'; --make silence last longer
        end if;
        sil_cnt:=sil_cnt+1;
      end if;
    end if;
  end process;
  
  receiving : process(sys_clk)
  variable cnt : INTEGER range 0 to 6 := 0;
  variable butts_up,butts_down : buses;
  variable phase,sil_cnt : INTEGER range 0 to 3 := 0;
  variable silence : STD_LOGIC := '0';
  begin
    if ena_rx='0' then
      cnt := 0; --restart 
      phase := 0;
      silence:='0';
    elsif rising_edge(sys_clk) then --sample the input with own system clock
      if silence='0' then --receive
        if cnt<4 then --active connection
          butts_up(phase)(cnt) := channel_rx_up;
          butts_down(phase)(cnt) := channel_rx_down;
          cnt := cnt + 1;
        end if;
        if cnt=4 then
          if phase<2 then
            cnt := 0; --ready to receive it 3 times
            phase := phase + 1;
          else
            if ena_rx='1' then
              phase := phase + 1;
              butts_rx <= findCorrect(butts_up) & findCorrect(butts_down);
              cnt := cnt + 1;
            end if;
          end if;
        end if;
        silence:='1';
        sil_cnt:=0; --init silence
      else --neglect silence
        if sil_cnt=3 then
          silence:='0'; --make silence last longer
        end if;
        sil_cnt:=sil_cnt+1;
      end if;
    end if;
  end process;
end Behavioral;