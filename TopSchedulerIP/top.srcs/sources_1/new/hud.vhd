----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 23.05.2017 11:14:53
-- Design Name: 
-- Module Name: hud_manager - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;
-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
entity hud_manager is
  Port (
        tclk:in std_logic; 
        xp:in natural range 0 to 81; --x runner
        yp:in natural range 0 to 61; --y runner
        lifep:in natural range 0 to 7; --life runner
        manap: in natural range 0 to 11; --mana runner
        score1: in natural range 0 to 999999; --score runner
        score2:in natural range 0 to 999999; --score mobspawner
        bx:in natural range 0 to 81; --x boundary spawner
        by:in natural range 0 to 61; --y boundary spawner
        we:out std_logic; --we to tilemap
        enable:in std_logic; --from render
        outadrr : out STD_LOGIC_VECTOR(9 downto 0); --output tile to revise size
        XW:out natural range 0 to 81; --which xtile to output
        YW:out natural range 0 to 61; --which ytile to output
        countmob:in natural range 0 to 100; --mobs alive
        counttraps:in natural range 0 to 100; --traps on
        level: in natural range 0 to 99; -- n of level
        pause: in std_logic; -- game paused?
        addrtrap:in natural range 0 to 127;
        addrmob1:in natural range 0 to 127;
        addrmob2:in natural range 0 to 127;    
        switch: in std_logic);
end hud_manager;
architecture Behavioral of hud_manager is
procedure number_assembler(x:in integer range 0 to 9;tob:in std_logic; flip:out std_logic_vector(1 downto 0);y:out integer) is
begin
flip:="00";
    case x is
        when 0 => y:=0;
            if tob='1' then
                flip:="10";
            end if;
        when 1 => y:=1;
        when 2 => y:=2;
            if tob='1' then
                flip:="11";
            end if;
        when 3=> y:=2;
            if tob='1' then
                flip:="10";
            end if;
        when 4=>
            if tob='0' then
                y:=0; flip:="10";
            else
                y:=1;
            end if;
       when 5=> y:=2; 
            if tob='0' then
                flip:="01";
            else
                flip:="10";
            end if;
       when 6=> 
            if tob='0' then
                y:=3; flip:="01";
            else
                y:=4;
            end if;
       when 7=>
            if tob='0' then
                y:=3;
            else
                y:=1;
            end if;
       when 8=> y:=4;
       when 9=> 
            if tob='0' then
                y:=4;
             else
                y:=1;
             end if;
    end case;
end procedure;
procedure direction_choice(dir:out natural range 0 to 8) is
--variable DeltaX:integer range -81 to 81:= bx+20-xp;
--variable DeltaY:integer range -61 to 61:=by+15-yp;
--variable tg: integer range -6100 to 6100;
--variable invtg:integer range -8100 to 8100;
-- from left dir (=0) clockwise, 8=no dir
begin 
  if bx<xp then
    if by<yp then
      dir := 5; --go down-right
    elsif by>yp then
      dir := 3; --go up-right
    else --if by=yp
      dir := 4; --go right
    end if;
  elsif bx>xp then
    if by<yp then
      dir := 7; --go down-left
    elsif by>yp then
      dir := 1; --go up-left
    else --if by=yp
      dir := 0; --go left
    end if;
  else --if bx=xp
    if by<yp then
      dir := 6; --go down
    elsif by>yp then
      dir := 2; --go up
    else --if by=yp
      --dir := "0000"; --found
    end if;
  end if; 

end procedure;
constant ft: integer range 0 to 128:=98 ; --first tile, to define
constant numt: integer:=(ft+27); --first num tile
begin
    hud_modifier:process(tclk)
    variable i:integer range 0 to 4; --position of num tile
    variable j: integer range 0 to 250; -- position of face tile
    variable k: integer range 0 to 250; -- position of face tile
    variable tmpnum:integer range 0 to 9; --num to convert
    variable count: natural range 0 to 330:=0; --count
    variable old_life: natural range 0 to 7:=lifep; 
    variable counter1: natural range 0 to 63:=0; --counts 5 cycles-0.5 sec to show broken heart and smiling sterpone in mob spawner
    variable old_mobs: natural range 0 to 101:=0;
    variable counter2: natural range 0 to 63:=63; --as counter1 but for dangersign and direction indicatore for mob spawner's compass
    variable ot: std_logic_vector (9 downto 0):= (others =>'0'); --assign at the end to signal.. useless?
    variable lvl_old: natural range 0 to 99:=0;
    variable timesmile: natural range 0 to 63:=0; --smile when sterpone passes level or when montuschi makes dmg
    begin
    if falling_edge(tclk) then
    if enable='1' then
        ot:=(others=>'0');
       if count<160 then
            we<='1';
            XW<=count mod 80;
            YW<=(count/80)+60;
        if switch='1' then --runner
            if old_mobs<countmob then
                counter2:=0;
            end if; 
            case count is
                when 41 => 
                    if lvl_old<level then
                        if timesmile<60 then               --half second
                            j:=ft+8;
                            ot(9 downto 2):=std_logic_vector(to_unsigned(j,8)); --address smiling sterpone;
                            timesmile:=timesmile+1;
                        else
                            lvl_old:=level;
                            timesmile:=0;
                        end if;
                    else
                        j:=ft+6; -- not smiling sterpone
                        ot(9 downto 2):=std_logic_vector(to_unsigned(j,8));
                    end if; 
                    
                when 42 =>-- ot <= --address  right top pg;
                     ot(9 downto 2):=std_logic_vector(to_unsigned(j,8)); --address smiling sterpone;
                     ot(0):='1';
                when 81 => --ot <= --address left bottom pg;
                     ot(9 downto 2):=std_logic_vector(to_unsigned(j+1,8));                
                when 82 =>-- ot <= --address right bottom pg;
                     ot(9 downto 2):=std_logic_vector(to_unsigned(j+1,8));
                     ot(0):='1';
                     
                when 44 to 48 => 
                    if count<lifep+44 then
                        ot(9 downto 2):=std_logic_vector(to_unsigned(ft,8)); --address heart
                    end if;
                    if old_life>lifep and count>lifep+43 and count<old_life+44 then
                        if counter1<60 then               --half second
                            ot(9 downto 2):=std_logic_vector(to_unsigned(ft+1,8)); --address broken heart;
                            counter1:=counter1+1;
                        else
                            old_life:=lifep;
                            counter1:=0;
                        end if;
                    end if;            
                    if old_life=0 then  
                        ot(9 downto 2):=std_logic_vector(to_unsigned(ft+43,8)); --black tile
                    end if;
                when 84 to 88 => 
                    if count<=83+manap/2 and manap mod 2 =0 then
                        ot(9 downto 2):=std_logic_vector(to_unsigned(ft+2,8)); --    ot <= full mana tile;
                    elsif count=84+manap/2 and manap mod 2=1 then
                        ot(9 downto 2):=std_logic_vector(to_unsigned(ft+3,8)); 
                    else
                        ot(9 downto 2):=std_logic_vector(to_unsigned(ft+42,8));--black tile
                    end if;
                when 52=> --mobs alive top
                    tmpnum:=(countmob/100) mod 10;
                    number_assembler(countmob/100, '0',ot(1 downto 0),i);
                    ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                when 53=> --mobs alive top
                    tmpnum:=(countmob/10)mod 10;
                    number_assembler(tmpnum, '0',ot(1 downto 0),i);
                    ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                when 54=> --mobs alive top
                    tmpnum:=countmob mod 10;
                    number_assembler(tmpnum, '0',ot(1 downto 0),i);
                    ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                when 92=> --mobs alive bottom
                    tmpnum:=(countmob/100) mod 10;
                    number_assembler(tmpnum, '1',ot(1 downto 0),i);
                    ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                when 93=> --mobs alive bottom
                    tmpnum:=(countmob/10) mod 10;
                    number_assembler(tmpnum, '1',ot(1 downto 0),i);
                    ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                when 94=> --mobs alive bottom
                    tmpnum:=countmob mod 10;
                    number_assembler(tmpnum, '1',ot(1 downto 0),i);
                    ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));                                
                
                when 56 => 
                    if counter2 <60 then
                        ot(9 downto 2):=std_logic_vector(to_unsigned(ft+4,8)); -- ot<= dangertop no flip;
                     else
                        ot(9 downto 2):=std_logic_vector(to_unsigned(ft+10,8)); --montuschi face
                     end if; 
                when 57 => 
                    if counter2 <60 then
                        ot(9 downto 2):=std_logic_vector(to_unsigned(ft+4,8));
                        ot(0):='1';-- ot<= dangertop horizontal flip;
                    else
                        ot(9 downto 2):=std_logic_vector(to_unsigned(ft+10,8)); --montuschi face
                        ot(0):='1';
                    end if;            
                when 96 => 
                    if counter2 <60 then
                        ot(9 downto 2):=std_logic_vector(to_unsigned(ft+5,8));--ot<= dangerbottom no flip;
                    else
                        ot(9 downto 2):=std_logic_vector(to_unsigned(ft+11,8)); --montuschi face
                    end if;            
                when 97 => 
                    if counter2 <60 then
                        ot(9 downto 2):=std_logic_vector(to_unsigned(ft+5,8));
                        ot(0):='1';    -- ot<=danger bottom horizontal flip;
                        counter2:=counter2+1;
                     else
                        ot(9 downto 2):=std_logic_vector(to_unsigned(ft+11,8)); --montuschi face
                        ot(0):='1';
                        old_mobs:=countmob;
                    end if;
                
                when 18 to 21 => 
                    if pause='1' then
                        ot(9 downto 2):=std_logic_vector(to_unsigned(ft+33+count-18,8)); --pause tiles          
                    else
                        ot(9 downto 2):=std_logic_vector(to_unsigned(ft+42,8)); --black tile           
                    end if;
                when 138 to 141 => 
                    if pause='1' then
                         ot(9 downto 2):=std_logic_vector(to_unsigned(ft+33+count-138,8)); --pause tiles          
                    else
                         ot(9 downto 2):=std_logic_vector(to_unsigned(ft+42,8)); --black tile           
                    end if;
               
                when 62 =>  --score uppart
                    tmpnum:=(score1/1000000) mod 10;
                    number_assembler(tmpnum, '0',ot(1 downto 0),i);
                    ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                when 63=>  --score uppart
                    tmpnum:=(score1/100000) mod 10;
                    number_assembler(tmpnum, '0',ot(1 downto 0),i);
                    ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                when 64=>  --score uppart
                    tmpnum:=(score1/10000) mod 10;
                    number_assembler(tmpnum, '0',ot(1 downto 0),i);
                    ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                when 65=>  --score uppart
                    tmpnum:=(score1/1000) mod 10;
                    number_assembler(tmpnum, '0',ot(1 downto 0),i);
                    ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                when 66=>  --score uppart
                    tmpnum:=(score1/100) mod 10;
                    number_assembler(tmpnum, '0',ot(1 downto 0),i);
                    ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                when 67=>  --score uppart
                    tmpnum:=(score1/10) mod 10;
                    number_assembler(tmpnum, '0',ot(1 downto 0),i);
                    ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                when 68=>  --score uppart
                    tmpnum:=(score1) mod 10;
                    number_assembler(tmpnum, '0',ot(1 downto 0),i);
                    ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));                                                                                                                                                                                    
                when 102 =>  --score lower part
                    tmpnum:=(score1/1000000) mod 10;
                    number_assembler(tmpnum, '1',ot(1 downto 0),i);
                    ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                when 103=>  --score uppart
                    tmpnum:=(score1/100000) mod 10;
                    number_assembler(tmpnum, '1',ot(1 downto 0),i);
                    ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                when 104=>  --score uppart
                    tmpnum:=(score1/10000) mod 10;
                    number_assembler(tmpnum, '1',ot(1 downto 0),i);
                    ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                when 105=>  --score uppart
                    tmpnum:=(score1/1000) mod 10;
                    number_assembler(tmpnum, '1',ot(1 downto 0),i);
                    ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                when 106=>  --score uppart
                    tmpnum:=(score1/100) mod 10;
                    number_assembler(tmpnum, '1',ot(1 downto 0),i);
                    ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                when 107=>  --score uppart
                    tmpnum:=(score1/10) mod 10;
                    number_assembler(tmpnum, '1',ot(1 downto 0),i);
                    ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                when 108=>  --score uppart
                    tmpnum:=(score1) mod 10;
                    number_assembler(tmpnum, '1',ot(1 downto 0),i);
                    ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                
                when 148 to 151=>
                    ot(9 downto 2):=std_logic_vector(to_unsigned(ft+37+count-148,8));  --score written 4 tiles
                
                when 74 => ot(9 downto 2):=std_logic_vector(to_unsigned(numt+1,8)); ot(0):='1';-- L top part --Lvl
                when 114 | 116 => ot(9 downto 2):=std_logic_vector(to_unsigned(numt+3,8)); ot(1 downto 0):="11";-- L low part and little l Lvl
                when 115 => ot(9 downto 2):=std_logic_vector(to_unsigned(numt+5,8)); -- v Lvl
                
                when 77 => --lvl number top
                    tmpnum:=level/10;
                    number_assembler(tmpnum, '0',ot(1 downto 0),i);
                    ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                when 78 => --Lvl bottom
                    tmpnum:=level mod 10 ;
                    number_assembler(tmpnum, '0',ot(1 downto 0),i);
                    ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                when 117 => --Lvl bottom
                     tmpnum:=level/10;
                     number_assembler(tmpnum, '1',ot(1 downto 0),i);
                     ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                when 118 => --Lvl bottom
                      tmpnum:=level mod 10 ;
                      number_assembler(tmpnum, '1',ot(1 downto 0),i);
                      ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                
                when others => ot(9 downto 2):=std_logic_vector(to_unsigned(ft+42,8)); --black tile
             end case;
        else --mob spawner
        direction_choice(counter2);
             case count is
                 when 41 => 
                     if old_life>lifep then
                         if timesmile<60 then               --half second
                             k:=ft+12;
                             ot(9 downto 2):=std_logic_vector(to_unsigned(k,8)); --address glasses montuschi;
                             timesmile:=timesmile+1;
                         else
                             old_life:=lifep;
                             timesmile:=0;
                         end if;
                     else
                         k:=ft+10; -- not smiling sterpone
                         ot(9 downto 2):=std_logic_vector(to_unsigned(k,8));
                     end if; 
                 when 42 =>-- ot <= --address  right top pg;
                      ot(9 downto 2):=std_logic_vector(to_unsigned(k,8)); --address glasses montuschi;
                      ot(0):='1';
                 when 81 => --ot <= --address left bottom pg;
                      ot(9 downto 2):=std_logic_vector(to_unsigned(k+1,8));                
                 when 82 =>-- ot <= --address right bottom pg;
                      ot(9 downto 2):=std_logic_vector(to_unsigned(k+1,8));
                      ot(0):='1';
                      
                 when 7 | 14=>  ot(9 downto 2):=std_logic_vector(to_unsigned(ft+41,8)); ot(1):='1'; --arrows
                 when 8 | 15=>  ot(9 downto 2):=std_logic_vector(to_unsigned(ft+41,8)); ot(1 downto 0):="11";
                 when 127 | 134=>  ot(9 downto 2):=std_logic_vector(to_unsigned(ft+41,8));
                 when 128 | 135=>  ot(9 downto 2):=std_logic_vector(to_unsigned(ft+41,8)); ot(0):='1';
                when 44 => --mobs alive top
                    tmpnum:=(countmob/100) mod 10;
                    number_assembler(tmpnum, '0',ot(1 downto 0),i);
                    ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                when 45=> --mobs alive top
                    tmpnum:=(countmob/10) mod 10;
                    number_assembler(tmpnum, '0',ot(1 downto 0),i);
                    ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                when 46=> --mobs alive top
                    tmpnum:=(countmob) mod 10;
                    number_assembler(tmpnum, '0',ot(1 downto 0),i);
                    ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));                                            
                when 84 => --mobs alive bottom
                    tmpnum:=(countmob/100) mod 10;
                    number_assembler(tmpnum, '1',ot(1 downto 0),i);
                    ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                when 85=> --mobs alive top
                    tmpnum:=(countmob/10) mod 10;
                    number_assembler(tmpnum, '1',ot(1 downto 0),i);
                    ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                when 86=> --mobs alive top
                    tmpnum:=(countmob) mod 10;
                    number_assembler(tmpnum, '1',ot(1 downto 0),i);
                    ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));  
                    
                 when 47 => ot(9 downto 2):=std_logic_vector(to_unsigned(addrmob1,8)); --tile mobs in queque
                 when 48 => ot(9 downto 2):=std_logic_vector(to_unsigned(addrmob1+1,8)); 
                 when 87 => ot(9 downto 2):=std_logic_vector(to_unsigned(addrmob1+2,8)); 
                 when 88 => ot(9 downto 2):=std_logic_vector(to_unsigned(addrmob1+3,8));
                 when 49 => ot(9 downto 2):=std_logic_vector(to_unsigned(addrmob2,8)); 
                 when 50 => ot(9 downto 2):=std_logic_vector(to_unsigned(addrmob2+1,8)); 
                 when 89 => ot(9 downto 2):=std_logic_vector(to_unsigned(addrmob2+2,8)); 
                 when 90 => ot(9 downto 2):=std_logic_vector(to_unsigned(addrmob2+3,8));                                                                       
 
                 when 52=> --traps on
                     number_assembler(counttraps/10, '0',ot(1 downto 0),i);
                     ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                 when 53=> --traps on
                     number_assembler(counttraps mod 10, '0',ot(1 downto 0),i);
                         ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                 when 92=> --traps on
                     number_assembler(counttraps/10, '1',ot(1 downto 0),i);
                             ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));                                              
                 when 93=> --traps on
                     number_assembler(counttraps mod 10, '1',ot(1 downto 0),i);
                     ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8)); 
                     
                 when 54 => ot(9 downto 2):=std_logic_vector(to_unsigned(addrtrap,8));  --trap in queque
                 when 55 => ot(9 downto 2):=std_logic_vector(to_unsigned(addrtrap+1,8)); 
                 when 94 => ot(9 downto 2):=std_logic_vector(to_unsigned(addrtrap+2,8)); 
                 when 95 => ot(9 downto 2):=std_logic_vector(to_unsigned(addrtrap+3,8)); 
                 
                when 58 =>  --score uppart
                     tmpnum:=(score2/1000000) mod 10;
                     number_assembler(tmpnum, '0',ot(1 downto 0),i);
                     ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                 when 59=>  --score uppart
                     tmpnum:=(score2/100000) mod 10;
                     number_assembler(tmpnum, '0',ot(1 downto 0),i);
                     ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                 when 60=>  --score uppart
                     tmpnum:=(score2/10000) mod 10;
                     number_assembler(tmpnum, '0',ot(1 downto 0),i);
                     ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                 when 61=>  --score uppart
                     tmpnum:=(score2/1000) mod 10;
                     number_assembler(tmpnum, '0',ot(1 downto 0),i);
                     ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                 when 62=>  --score uppart
                     tmpnum:=(score2/100) mod 10;
                     number_assembler(tmpnum, '0',ot(1 downto 0),i);
                     ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                 when 63=>  --score uppart
                     tmpnum:=(score2/10)mod 10;
                     number_assembler(tmpnum, '0',ot(1 downto 0),i);
                     ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                 when 64=>  --score uppart
                     tmpnum:=(score2) mod 10;
                     number_assembler(tmpnum, '0',ot(1 downto 0),i);
                     ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));                                                                                                                                                                                    
                 when 98 =>  --score lower part
                     tmpnum:=(score2/1000000) mod 10;
                     number_assembler(tmpnum, '1',ot(1 downto 0),i);
                     ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                 when 99=>  --score lower part
                     tmpnum:=(score2/100000) mod 10;
                     number_assembler(tmpnum, '1',ot(1 downto 0),i);
                     ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                 when 100=>  --score lower part
                     tmpnum:=(score2/10000) mod 10;
                     number_assembler(tmpnum, '1',ot(1 downto 0),i);
                     ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                 when 101=>  --score lower part
                     tmpnum:=(score2/1000) mod 10;
                     number_assembler(tmpnum, '1',ot(1 downto 0),i);
                     ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                 when 102=>  --score lower part
                     tmpnum:=(score2/100) mod 10;
                     number_assembler(tmpnum, '1',ot(1 downto 0),i);
                     ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                 when 103=>  --score lower part
                     tmpnum:=(score2/10)mod 10;
                     number_assembler(tmpnum, '1',ot(1 downto 0),i);
                     ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                 when 104=>  --score lower part
                     tmpnum:=(score2) mod 10;
                     number_assembler(tmpnum, '1',ot(1 downto 0),i);
                     ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                 when 143 to 146=>
                     ot(9 downto 2):=std_logic_vector(to_unsigned(ft+37+count-143,8));  --score written 4 tiles                    
                                 
                 when 18 to 21 => 
                      if pause='1' then
                          ot(9 downto 2):=std_logic_vector(to_unsigned(ft+33+count-18,8)); --pause tiles          
                      else
                          ot(9 downto 2):=std_logic_vector(to_unsigned(ft+42,8)); --black tile           
                      end if;
                 when 138 to 141 => 
                      if pause='1' then
                          ot(9 downto 2):=std_logic_vector(to_unsigned(ft+33+count-138,8)); --pause tiles          
                      else
                          ot(9 downto 2):=std_logic_vector(to_unsigned(ft+42,8)); --black tile           
                      end if;                
                 when 67 => 
                      if lvl_old<level or old_life<lifep then
                          if counter1<60 then               --half second
                              j:=ft+8;
                              ot(9 downto 2):=std_logic_vector(to_unsigned(j,8)); --address smiling sterpone;
                              counter1:=counter1+1;
                          else
                              lvl_old:=level;
                              old_life:=lifep;
                              counter1:=0;
                          end if;
                      else
                          j:=ft+6; -- not smiling sterpone
                          ot(9 downto 2):=std_logic_vector(to_unsigned(j,8));
                      end if; 
                      
                  when 68 =>-- ot <= --address  right top pg;
                       ot(9 downto 2):=std_logic_vector(to_unsigned(j,8)); --address smiling sterpone;
                       ot(0):='1';
                  when 107 => --ot <= --address left bottom pg;
                       ot(9 downto 2):=std_logic_vector(to_unsigned(j+1,8));                
                  when 108 =>-- ot <= --address right bottom pg;
                       ot(9 downto 2):=std_logic_vector(to_unsigned(j+1,8));
                       ot(0):='1';
                  
                  when 69 to 70=>  --runner life top
                        if lifep <4 then
                            ot(9 downto 2):=std_logic_vector(to_unsigned(ft+24,8));
                        else
                        ot(9 downto 2):=std_logic_vector(to_unsigned(ft+24+lifep-3,8));
                            end if;
                        if count=70 then ot(0):='1'; end if;
                            
                  when 109 to 110=> --runner life bottom
                        if lifep >2 then
                          ot(9 downto 2):=std_logic_vector(to_unsigned(ft+23,8));
                          elsif lifep/=0 then
                          ot(9 downto 2):=std_logic_vector(to_unsigned(ft+21+lifep-1,8));
                          else
                          ot(9 downto 2):=std_logic_vector(to_unsigned(ft+21,8));
                          end if;
                          if count=110 then ot(0):='1';  end if;                          
                  
                  when 32=>         --left top diag compass
                        if counter2=1 then
                            ot(9 downto 2):=std_logic_vector(to_unsigned(ft+14+1,8));
                        else ot(9 downto 2):=std_logic_vector(to_unsigned(ft+14+4,8));
                        end if;
                  when 33=>         --left top compass
                        if counter2=2 then
                          ot(9 downto 2):=std_logic_vector(to_unsigned(ft+14+2,8));
                        else ot(9 downto 2):=std_logic_vector(to_unsigned(ft+14+5,8));
                        end if;
                  when 34=>  ot(0):='1';      --right top compass
                        if counter2=2 then
                            ot(9 downto 2):=std_logic_vector(to_unsigned(ft+14+2,8));
                        else ot(9 downto 2):=std_logic_vector(to_unsigned(ft+14+5,8));
                        end if;
                  when 35=>  ot(0):='1';       --right top diag compass
                        if counter2=3 then
                          ot(9 downto 2):=std_logic_vector(to_unsigned(ft+14+1,8));
                        else ot(9 downto 2):=std_logic_vector(to_unsigned(ft+14+4,8));
                        end if;
                  when 72=> --top left compass
                        if counter2=0 then
                            ot(9 downto 2):=std_logic_vector(to_unsigned(ft+14+3,8));
                        else ot(9 downto 2):=std_logic_vector(to_unsigned(ft+14+6,8));
                        end if;
                  when 73 => --top left centre compass
                        ot(9 downto 2):=std_logic_vector(to_unsigned(ft+14,8));
                  when 74 => --top right centre compass
                        ot(9 downto 2):=std_logic_vector(to_unsigned(ft+14,8));
                        ot(0):='1';
                  when 75 => ot(0):='1'; --top right compass
                        if counter2=4 then
                            ot(9 downto 2):=std_logic_vector(to_unsigned(ft+14+3,8));
                        else ot(9 downto 2):=std_logic_vector(to_unsigned(ft+14+6,8));
                        end if;
                  when  112=> ot(1):='1'; --low left compass
                        if counter2=0 then
                          ot(9 downto 2):=std_logic_vector(to_unsigned(ft+14+3,8));
                        else ot(9 downto 2):=std_logic_vector(to_unsigned(ft+14+6,8));
                        end if;                          
                  when 113 => --low left centre compass
                        ot(9 downto 2):=std_logic_vector(to_unsigned(ft+14,8));
                        ot(1):='1';
                  when 114 => --low right centre compass
                        ot(9 downto 2):=std_logic_vector(to_unsigned(ft+14,8));
                        ot(1 downto 0):="11";
                  when 115 => ot(1 downto 0):="11"; --low right compass
                        if counter2=4 then
                          ot(9 downto 2):=std_logic_vector(to_unsigned(ft+14+3,8));
                        else ot(9 downto 2):=std_logic_vector(to_unsigned(ft+14+6,8));
                        end if; 
                  when 152=>  ot(1):='1';       --left bottom diag compass
                        if counter2=7 then
                            ot(9 downto 2):=std_logic_vector(to_unsigned(ft+14+1,8));
                        else ot(9 downto 2):=std_logic_vector(to_unsigned(ft+14+4,8));
                        end if;
                  when 153=>  ot(1):='1';       --left bottom compass
                        if counter2=6 then
                          ot(9 downto 2):=std_logic_vector(to_unsigned(ft+14+2,8));
                        else ot(9 downto 2):=std_logic_vector(to_unsigned(ft+14+5,8));
                        end if;
                  when 154=>  ot(1 downto 0):="11";       --right bottom compass
                         if counter2=6 then
                            ot(9 downto 2):=std_logic_vector(to_unsigned(ft+14+2,8));
                        else ot(9 downto 2):=std_logic_vector(to_unsigned(ft+14+5,8));
                        end if;
                  when 155=>  ot(1 downto 0):="11";       --right bottom diag compass
                        if counter2=5 then
                          ot(9 downto 2):=std_logic_vector(to_unsigned(ft+14+1,8));
                        else ot(9 downto 2):=std_logic_vector(to_unsigned(ft+14+4,8));
                        end if;                        
                  when 36 | 38 => ot(9 downto 2):=std_logic_vector(to_unsigned(numt+3,8)); ot(1 downto 0):="11";-- 2 little l Lvl
                  when 37 => ot(9 downto 2):=std_logic_vector(to_unsigned(numt+5,8)); -- v Lvl     
                  when 77 => --lvl number top
                      tmpnum:=level/10;
                      number_assembler(tmpnum, '0',ot(1 downto 0),i);
                      ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                  when 78 => --Lvl bottom
                      tmpnum:=level mod 10 ;
                      number_assembler(tmpnum, '0',ot(1 downto 0),i);
                      ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                  when 117 => --Lvl bottom
                       tmpnum:=level/10;
                       number_assembler(tmpnum, '1',ot(1 downto 0),i);
                       ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8));
                  when 118 => --Lvl bottom
                        tmpnum:=level mod 10 ;
                        number_assembler(tmpnum, '1',ot(1 downto 0),i);
                        ot(9 downto 2):=std_logic_vector(to_unsigned(numt+i,8)); 
                                                                                                                                                                                                                                          
                  when others => ot(9 downto 2):=std_logic_vector(to_unsigned(ft+42,8)); --black tile                                        
              end case;
        end if;    
        outadrr<=ot;
        else
        we<='0';    
        end if;
        if count< 160 then
        count:=count+1;
        end if;
     else
        count:=0;
     end if;
    end if;
     end process;
end Behavioral;