----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 05/10/2017 07:13:06 PM
-- Design Name: 
-- Module Name: test - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity test is
end test;

architecture Behavioral of test is

component top is
Port ( clk : in STD_LOGIC;
           btn : in STD_LOGIC_VECTOR (3 downto 0); --test
           jd_p : in STD_LOGIC_VECTOR (3 downto 0); --dir 
           jd_n : in STD_LOGIC_VECTOR (3 downto 0); --others
           sw : in STD_LOGIC_VECTOR (2 downto 0); --mode
           led : out STD_LOGIC_VECTOR (3 downto 0);
           jb_p : in STD_LOGIC_VECTOR (3 downto 0);
           jc_p : out STD_LOGIC_VECTOR (3 downto 0);
           vga_r : out STD_LOGIC_VECTOR (4 downto 0);
           vga_g : out STD_LOGIC_VECTOR (5 downto 0);
           vga_b : out STD_LOGIC_VECTOR (4 downto 0);
           vga_hs : out STD_LOGIC;
           vga_vs : out STD_LOGIC);
end component;

signal clk,vga_hs : STD_LOGIC;
signal butt,led : STD_LOGIC_VECTOR (3 downto 0) := "0000";
signal jd_p,jd_n : STD_LOGIC_VECTOR (3 downto 0) := "0000";
signal jb_p,jc_p : STD_LOGIC_VECTOR (3 downto 0) := "0000";
signal sw : STD_LOGIC_VECTOR (2 downto 0) := (others=>'0');
begin

  sim : top port map(clk=>clk, btn=>butt, sw=>sw, vga_hs=>vga_hs, jd_p=>jd_p,
        jd_n=>jd_n, led=>led, jb_p=>jb_p, jc_p=>jc_p);
  
  clocking : process
  constant t : time := 1 ns;
  begin
    clk<='1';
    wait for t;
    clk<='0';
    wait for t;
  end process;
  
  process(vga_hs)
  variable cnt : NATURAL := 0;
  begin
    if rising_edge(vga_hs) and cnt = 10 then
      butt <= butt(2 downto 0) & butt(3);
    end if;
    if cnt < 1000 then
      cnt := cnt + 1;
    end if;
  end process;

end Behavioral;
