Input: system_clock, write_coordinates, read_coordinates, tile_to_write
Output: tile_to_read
Internal logic: works in parallel with r/w on an array representing the tilemap
(4800 of 6bit vectors), doing the job in different clock states


TileMap DATA (tot 19bit):

    Tile = spriteID & VFlip & HFlip & VOffset & HOffset & Offset & Effect
    
    7bit spriteID        (18 downto 12)
    1bit VFlip           (11)
    1bit HFlip           (10)
    1bit VOffset         (9)
    1bit HOffset         (8)
    4bit Offset          (7 downto 4)
    4bit Effect          (3 downto 0)

Più compatto:

    Tile = SpriteID & VFlip & HFlip & OffsetData & Effect
    
    7bit spriteID    (18 downto 12)
    1bit VFlip       (11)
    1bit HFlip       (10)
    6bit OffsetData  (9 downto 4) [ VOffset & HOffset & Offset ]
    4bit Effect      (3 downto 0)